#include "DriveBackupRestore.h"

DriveBackupRestore::DriveBackupRestore ()
{
	m_bUseDiagnostics = true;
	m_bBackupdone = false; // Just a flag so just to make sure that we don't do a restore without first doing a backup.
}

DriveBackupRestore::~DriveBackupRestore ()
{
}

void DriveBackupRestore::SetDiagnostics (Diagnostics *pdiag)
{
	m_pdiag = pdiag;
}

void DriveBackupRestore::SetHWND (HWND hwnd)
{
	m_hwnd = hwnd;
}

unsigned long long DriveBackupRestore::GetFreeDiskSpace ()
{
	unsigned long long lLimit = 0;
	unsigned long long lFree = 0;
	unsigned long long lSize = 0;
	unsigned long long lTotal = 0;
	
	char szAppData[SIZE_STRING];
	ZeroMemory (szAppData, SIZE_STRING);

	if (GetEnvironmentVariable ("APPDATA", szAppData, SIZE_STRING) == 0) {
		return 0;
	}

	char szDrive[SIZE_NAME];
	ZeroMemory (szDrive, SIZE_NAME);
	
	strncpy_s (szDrive, SIZE_NAME, szAppData, 1);
	strcat_s (szDrive, SIZE_NAME, ":\\");

	if (GetDiskFreeSpaceEx (szDrive, (PULARGE_INTEGER) &lFree, (PULARGE_INTEGER) &lLimit, (PULARGE_INTEGER) &lTotal) == 0) {
		//m_pdiag->OutputInt ("GetDiskFreeSpace failed: Error: ", GetLastError ());
	}

	return lFree;
}

bool DriveBackupRestore::BackupDrive (char *szDriveletter)
{
	//OutputText ("This is some text which is produced by the Drive backup class.");

	//CreateDirectoryPath ("D:\\Temp\\Directory1\\Directory2\\Directory3\\MyFile.txt");

	//GetFullFolderSize (szDriveletter);

	if (PrepareBackupPath() == false) {
		return false;
	}

	// Now list all the files in the source
	m_dlFilelist.Clear ();

	// Add a colon to the drive letter specified
	char szDrive[SIZE_NAME];
	ZeroMemory (szDrive, SIZE_NAME);
	strcpy_s (szDrive, SIZE_NAME, szDriveletter);
	strcat_s (szDrive, SIZE_NAME, ":");

	m_lbackupsize = 0;
	ListAllFiles (szDrive);

	// Once we get here all the files should be listed, and we will be able to check if
	// the backup destination has enough free disk space to contain the backup
	// m_lbackupsize will be incremented to include the total size of all files
	m_lbackupsize += 100000000; // Add 100MB to the backup size so that if the user only has just enough space we
	// don't kill their system by using up every last byte;

	if (m_lbackupsize >= GetFreeDiskSpace ()) {
		MessageBox (NULL, "This computer does not have enough free disk space to contain the backup. Please free some disk space and try again.", "Not enough disk space", MB_OK | MB_ICONEXCLAMATION);
		return false;
	}

	// Now go through each entry that was found and back it up
	SingleEntry *pEntry;
	int i = 0;
	char szSourcepath[SIZE_STRING];
	char szDestpath[SIZE_STRING];
	int icopyerrors = 0;
	int ipercent = 0;
	
	for (i=0;i<m_dlFilelist.GetNumItems ();i++)
	{
		pEntry = (SingleEntry *) m_dlFilelist.GetItem(i);

		ZeroMemory (szSourcepath, SIZE_STRING);
		ZeroMemory (szDestpath, SIZE_STRING);

		strcpy_s (szSourcepath, SIZE_STRING, pEntry->szFilePath);
		strcpy_s (szDestpath, SIZE_STRING, pEntry->szFilePath);

		Replacestring (szDestpath, szDrive, m_szBackuppath);

		OutputText ("Source Path: ", szSourcepath);
		OutputText ("Dest Path: ", szDestpath);


		// Now copy the file, but first we need to create the destination directory structure
		CreateDirectoryPath (szDestpath);

		if (CopyFile (szSourcepath, szDestpath, FALSE) == FALSE) {
			icopyerrors++;
		}

		ipercent = (i*100) / m_dlFilelist.GetNumItems ();

		PostMessage (m_hwnd, WM_UICOMMAND, PROGRESS_BACKUP, ipercent);
	}

	OutputInt ("Number of Backup Errors: ", icopyerrors);

	if (icopyerrors == 0) {
		m_bBackupdone = true;
		return true;		
	} else {
		m_bBackupdone = false;
		return false;
	}
}

bool DriveBackupRestore::RestoreDrive (char *szOrigdrive, char *szDestdriveletter)
{

	// Ensure we don't restore unless a backup has been done first
	if (m_bBackupdone == false) {
		return false;
	}

	// Add a colon to the drive letter specified
	char szDrive[SIZE_NAME];
	ZeroMemory (szDrive, SIZE_NAME);
	strcpy_s (szDrive, SIZE_NAME, szOrigdrive);
	strcat_s (szDrive, SIZE_NAME, ":");

	char szDestdrv[SIZE_NAME];
	ZeroMemory (szDestdrv, SIZE_NAME);
	strcpy_s (szDestdrv, SIZE_NAME, szDestdriveletter);
	strcat_s (szDestdrv, SIZE_NAME, ":");

	// Now go through each entry that was found and restore it
	SingleEntry *pEntry;
	int i = 0;
	char szSourcepath[SIZE_STRING];
	char szDestpath[SIZE_STRING];
	int icopyerrors = 0;
	int ipercent = 0;

	for (i=0;i<m_dlFilelist.GetNumItems ();i++)
	{
		pEntry = (SingleEntry *) m_dlFilelist.GetItem(i);

		ZeroMemory (szSourcepath, SIZE_STRING);
		ZeroMemory (szDestpath, SIZE_STRING);

		strcpy_s (szSourcepath, SIZE_STRING, pEntry->szFilePath);
		strcpy_s (szDestpath, SIZE_STRING, pEntry->szFilePath);

		Replacestring (szSourcepath, szDrive, m_szBackuppath);
		Replacestring (szDestpath, szDrive, szDestdrv);

		OutputText ("Source Path: ", szSourcepath);
		OutputText ("Dest Path: ", szDestpath);


		// Now copy the file, but first we need to create the destination directory structure
		CreateDirectoryPath (szDestpath);

		if (CopyFile (szSourcepath, szDestpath, FALSE) == FALSE) {
			icopyerrors++;
		}

		ipercent = (i*100) / m_dlFilelist.GetNumItems ();

		PostMessage (m_hwnd, WM_UICOMMAND, PROGRESS_RESTORE, ipercent);
	}

	OutputInt ("Number of Restore Errors: ", icopyerrors);

	if (icopyerrors == 0) {
		return true;		
	} else {
		return false;
	}
}

bool DriveBackupRestore::DeleteRestore (char *szDriveletter)
{
	// Add a colon to the drive letter specified
	char szDrive[SIZE_NAME];
	ZeroMemory (szDrive, SIZE_NAME);
	strcpy_s (szDrive, SIZE_NAME, szDriveletter);
	strcat_s (szDrive, SIZE_NAME, ":");

	// Now go through each entry that was found and delete it
	SingleEntry *pEntry;
	int i = 0;
	char szSourcepath[SIZE_STRING];
	char szDestpath[SIZE_STRING];
	int icopyerrors = 0;

	for (i=0;i<m_dlFilelist.GetNumItems ();i++)
	{
		pEntry = (SingleEntry *) m_dlFilelist.GetItem(i);

		ZeroMemory (szSourcepath, SIZE_STRING);
		ZeroMemory (szDestpath, SIZE_STRING);

		strcpy_s (szSourcepath, SIZE_STRING, pEntry->szFilePath);
		strcpy_s (szDestpath, SIZE_STRING, pEntry->szFilePath);

		Replacestring (szSourcepath, szDrive, m_szBackuppath);

		OutputText ("Source Path: ", szSourcepath);
		OutputText ("Dest Path: ", szDestpath);


		// Now copy the file, but first we need to create the destination directory structure		
		if (DeleteFile (szSourcepath) == FALSE) {
			icopyerrors++;
		}
	}

	OutputInt ("Number of Restore Errors: ", icopyerrors);

	if (icopyerrors == 0) {
		char szDirectory[SIZE_STRING];
		ZeroMemory (szDirectory, SIZE_STRING);
		strcpy_s (szDirectory, SIZE_STRING, m_szBackuppath);
		strcat_s (szDirectory, SIZE_STRING, "\0");
		
		SHFILEOPSTRUCT shFileop;
		shFileop.hwnd = m_hwnd;
		shFileop.pFrom = szDirectory;
		shFileop.wFunc = FO_DELETE;
		shFileop.pTo = szDirectory;
		shFileop.fFlags = FOF_NOCONFIRMATION;

		SHFileOperation (&shFileop);
		
		return true;		
	} else {
		return false;
	}
}

void DriveBackupRestore::CreateDirectoryPath (char *szFilepath)
{
	// This function will create any directories that are specified in the filepath parameter
	DynList dlEntries;
	SingleEntry currentEntry;
	SingleEntry *pEntry;

	int c = 0;
	
	for (c=0;c<strlen (szFilepath);c++) {
		if (strncmp (szFilepath+c, "\\", 1) == 0) {
			//MessageBox (NULL, "Found a backslash", "Found", MB_OK);

			ZeroMemory (currentEntry.szFilePath, SIZE_STRING);
			strncpy_s (currentEntry.szFilePath, SIZE_STRING, szFilepath, c);

			dlEntries.AddItem (&currentEntry, sizeof (SingleEntry), false);
		}
	}

	for (c=0;c<dlEntries.GetNumItems ();c++) {
		
		pEntry = (SingleEntry *) dlEntries.GetItem (c);

		//MessageBox (NULL, pEntry->szFilePath, "Split Path", MB_OK);
		_mkdir(pEntry->szFilePath);

	}

	dlEntries.Clear ();
}

bool DriveBackupRestore::GetTimeString (char *szTimestring, int destsize)
{
	SYSTEMTIME systemtime;
	GetLocalTime (&systemtime);

	int iDay = systemtime.wDay;
	int iMonth = systemtime.wMonth;
	int iYear = systemtime.wYear;
	int iHour = systemtime.wHour;
	int iMinute = systemtime.wMinute;
	int iSecond = systemtime.wSecond;

	char szString[SIZE_STRING];
	ZeroMemory (szString, SIZE_STRING);

	sprintf_s (szString, SIZE_STRING, "%i%i%i%i%i%i", iDay, iMonth, iYear, iHour, iMinute, iSecond);

	ZeroMemory (szTimestring, destsize);
	strcpy_s (szTimestring, destsize, szString);

	return true;
}

bool DriveBackupRestore::PrepareBackupPath ()
{
	int iRes = 0;
	char szAppData[SIZE_STRING];
	char szCompanyAppData[SIZE_STRING];
	char szProgramAppData[SIZE_STRING];
	char szTempScriptsDir[SIZE_STRING];
	char szTimedir[SIZE_STRING];

	ZeroMemory (szAppData, SIZE_STRING);
	ZeroMemory (szCompanyAppData, SIZE_STRING);
	ZeroMemory (szProgramAppData, SIZE_STRING);
	ZeroMemory (szTempScriptsDir, SIZE_STRING);
	ZeroMemory (szTimedir, SIZE_STRING);

	if (GetEnvironmentVariable ("APPDATA", szAppData, SIZE_STRING) == 0) {
		return false;
	}

	strcpy_s (szCompanyAppData, SIZE_STRING, szAppData);
	strcat_s (szCompanyAppData, SIZE_STRING, "\\CedeSoft");
	_mkdir (szCompanyAppData);

	strcpy_s (szProgramAppData, SIZE_STRING, szCompanyAppData);
	strcat_s (szProgramAppData, SIZE_STRING, "\\CedeDrive");
	_mkdir (szProgramAppData);

	strcpy_s (szTempScriptsDir, SIZE_STRING, szProgramAppData);
	strcat_s (szTempScriptsDir, SIZE_STRING, "\\Backups");
	_mkdir (szTempScriptsDir);

	GetTimeString (szTimedir, SIZE_STRING);
	strcat_s (szTempScriptsDir, SIZE_STRING, "\\");
	strcat_s (szTempScriptsDir, SIZE_STRING, szTimedir);
	_mkdir (szTempScriptsDir);

	// Set our member variable which is used by the other functions
	ZeroMemory (m_szBackuppath, SIZE_STRING);
	strcpy_s (m_szBackuppath, SIZE_STRING, szTempScriptsDir);
	
	return true;
}

bool DriveBackupRestore::Replacestring (char *szStringtoreplace, char *szStringtosearchfor, char *szReplacement)
{
	char szFull[SIZE_STRING];
	char szSearch[SIZE_STRING];
	char szTemp[SIZE_STRING];
	int c = 0;

	char szFinal[SIZE_STRING];

	if (strlen (szStringtoreplace) > 0) {
		if (strlen (szStringtoreplace) >= strlen (szStringtosearchfor)) {
			
			ZeroMemory (szFull, SIZE_STRING);
			strcpy_s (szFull, SIZE_STRING, szStringtoreplace);

			ZeroMemory (szSearch, SIZE_STRING);
			strcpy_s (szSearch, SIZE_STRING, szStringtosearchfor);

			_strupr (szFull);
			_strupr (szSearch);

			for (c=0;c<(strlen (szFull)-strlen (szSearch));c++) {
				
				ZeroMemory (szTemp, SIZE_STRING);
				strncpy_s (szTemp, SIZE_STRING, szFull+c, strlen (szSearch));

				if (strcmp (szTemp, szSearch) == 0) {
					// We found the string

					ZeroMemory (szFinal, SIZE_STRING);
					
					// Copy the first part (before the found string) into the final string
					if (c>0) {
						strncpy_s (szFinal, SIZE_STRING, szStringtoreplace, c);
					}

					// Now append the replacement
					strcat_s (szFinal, SIZE_STRING, szReplacement);

					// now append the rest of the source string (after the found string)
					strcat_s (szFinal, SIZE_STRING, szStringtoreplace+c+strlen(szStringtosearchfor));

					ZeroMemory (szStringtoreplace, SIZE_STRING);
					strcpy_s (szStringtoreplace, SIZE_STRING, szFinal);

					return true;
				}

			}

			// if we got here, then we didn't find anything
			return false;

		} else {
			return false;
		}
	} else {
		return false;
	}

	return false;
}

void DriveBackupRestore::ListAllFiles (char *szSourcePath) {
	struct _finddata_t c_file;
	long hFile;

	char szSource[SIZE_STRING];
	char szFilter[SIZE_STRING];
	char szFullPath[SIZE_STRING];
	char szDirectoryPath[SIZE_STRING];
	CHAR szFullFilePath[SIZE_STRING];

	//BOOL bExists;
	//DynStringList dlSourceFiles;
	//pPathList->Clear ();

	ZeroMemory (szSource, SIZE_STRING);
	ZeroMemory (szFilter, SIZE_STRING);
	ZeroMemory (szFullPath, SIZE_STRING);
	
	if (strncmp (szSourcePath+strlen(szSourcePath)-1, "\\", 1) == 0) {
		strncpy_s (szSource, SIZE_STRING, szSourcePath, strlen(szSourcePath)-1);
	} else {
		strcpy_s (szSource, SIZE_STRING, szSourcePath);
	}

	strcpy (szFilter, "\\*.*");
	
	strcat (szFullPath, szSource);
	strcat (szFullPath, szFilter);
	
	//OutputText ("SourcePath: ", szSourcePath);

	// Build the list of files in the source path
	if( (hFile = _findfirst(szFullPath, &c_file )) == -1L ) {
		OutputText ("No Files.");
	}		
	else
	{
		//Sleep (8);
		if ((c_file.attrib & _A_SUBDIR) != 16) {
			//dlSourceFiles.AddItem (c_file.name);
			//OutputText (c_file.name, ": AFILE" );

			ZeroMemory (szFullFilePath, SIZE_STRING);
			strcpy_s (szFullFilePath, SIZE_STRING, szSourcePath);
			strcat_s (szFullFilePath, SIZE_STRING, "\\");
			strcat_s (szFullFilePath, SIZE_STRING, c_file.name);
			//c_file.
			
			//if (IsExtensionMatched (c_file.name, ".pst") == true) {
				OutputText (szFullFilePath);

				SingleEntry entry;
				strcpy_s (entry.szFilePath, SIZE_STRING, szFullFilePath);
				entry.lFileSize = c_file.size;
				entry.lModifiedTime = c_file.time_write;

				m_lbackupsize+= c_file.size;
				m_dlFilelist.AddItem ((SingleEntry *) &entry, sizeof (SingleEntry), false);
			//}
			
			//m_lTotalfoldersize+=c_file.size;

		} else {
			if (strcmp (c_file.name, ".") != 0 && strcmp (c_file.name, "..") != 0) {
				//OutputText (c_file.name, ": ADIRECTORY");

				ZeroMemory (szDirectoryPath, SIZE_STRING);
				strcpy_s (szDirectoryPath, SIZE_STRING, szSourcePath);
				strcat_s (szDirectoryPath, SIZE_STRING, "\\");
				strcat_s (szDirectoryPath, SIZE_STRING, c_file.name);
				
				ListAllFiles (szDirectoryPath);
			}
		}
		while( _findnext(hFile, &c_file ) == 0 )
		{
			//Sleep (8);
			if ((c_file.attrib & _A_SUBDIR) != 16) {			
				//OutputText (c_file.name, ": AFILE");

				ZeroMemory (szFullFilePath, SIZE_STRING);
				strcpy_s (szFullFilePath, SIZE_STRING, szSourcePath);
				strcat_s (szFullFilePath, SIZE_STRING, "\\");
				strcat_s (szFullFilePath, SIZE_STRING, c_file.name);
				
				//if (IsExtensionMatched (c_file.name, ".pst") == true) {
					OutputText (szFullFilePath);

					SingleEntry entry;
					strcpy_s (entry.szFilePath, SIZE_STRING, szFullFilePath);
					entry.lFileSize = c_file.size;
					entry.lModifiedTime = c_file.time_write;
					
					m_lbackupsize+= c_file.size;
					m_dlFilelist.AddItem ((SingleEntry *) &entry, sizeof (SingleEntry), false);
				//}
				//m_lTotalfoldersize+=c_file.size;
			} else {
				if (strcmp (c_file.name, ".") != 0 && strcmp (c_file.name, "..") != 0) {
					//OutputText (c_file.name, ": ADIRECTORY");

					ZeroMemory (szDirectoryPath, SIZE_STRING);
					strcpy_s (szDirectoryPath, SIZE_STRING, szSourcePath);
					strcat_s (szDirectoryPath, SIZE_STRING, "\\");
					strcat_s (szDirectoryPath, SIZE_STRING, c_file.name);
					ListAllFiles (szDirectoryPath);
				}
			}
		}
		_findclose( hFile );
	}
	
	//if (dlSourceFiles.GetNumItems () > 0) {
	//	dlSourceFiles.Clear ();
	//}
}


void DriveBackupRestore::OutputInt (LPCSTR lpszText, int iValue)
{
	if (m_bUseDiagnostics == true) {
		m_pdiag->OutputInt (lpszText, iValue);
	}
}

void DriveBackupRestore::OutputText (LPCSTR lpszText)
{
	if (m_bUseDiagnostics == true) {
		m_pdiag->OutputText (lpszText);
	}
}

void DriveBackupRestore::OutputText (LPCSTR lpszName, LPCSTR lpszValue)
{
	if (m_bUseDiagnostics == true) {
		m_pdiag->OutputText (lpszName, lpszValue);
	}
}