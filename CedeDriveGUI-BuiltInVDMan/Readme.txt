This version of the CedeDrive GUI has the Virtual Drive management built in to the GUI. This is a backup
purely for reference, as the latest version of the CedeDrive GUI now uses the CedeDrive Service to manage
the mounting and unmounting of encrypted virtual drives. 

The problem with having the Virtual Drive management built in to the GUI was that the GUI always needed
admin rights to mount or unmount drives, therefore this functionality was moved over to the cede drive
service where it will run as localsystem, and communicate with the GUI using named pipes.
