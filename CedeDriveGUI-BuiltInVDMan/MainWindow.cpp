// This work is dedicated to the Lord our God. King of Heaven, Lord of Heaven's Armies.

#include "MainWindow.h"

MainWindow::MainWindow ()
{	
	m_pwnd = (MainWindow *) this;
	m_wrongpasswordcount = 0;
}

MainWindow::~MainWindow ()
{
}

void MainWindow::Initialise (HWND hWnd, bool bAlreadyrunning, LPSTR lpCmdline)
{	
	m_bAlreadyrunning = bAlreadyrunning; // Flag to inform us that an instance of this app is already running
	SetParentHWND (hWnd);
	//m_hwnd = hWnd;
	SetBgColor (GetSysColor (COLOR_BTNFACE));
	SetCaption (TEXT ("CedeDrive v2.5"));
	SetWindowStyle (FS_STYLESTANDARD);
	CreateAppWindow ("CRYPTWindowClass", 70, 0, 658, 434, false);
	m_uihandler.SetWindowProperties (0, 70, 0, 343, RGB (200, 200, 200));
	SetWindowPosition (FS_CENTER);
	//Show ();
}

void MainWindow::SetDiagnostics (Diagnostics *pdiag)
{
	m_pdiag = pdiag;

	m_vdmanager.SetDiagnostics (pdiag);
	m_addwindow.SetDiagnostics (pdiag);
	m_drivebackup.SetDiagnostics (pdiag);
	m_convertwindow.SetDiagnostics (pdiag);
}

void MainWindow::OnCreate (HWND hWnd)
{
	m_hwnd = hWnd;
	g_hWnd = hWnd; // Need to do this otherwise the control creation wrapper functions don't work.

	RegisterIPCEvent("CedeDriveCoreEvent");

	if (m_bAlreadyrunning == true) {
		ShareConfig ();
		PostQuitMessage (0);
		return;
	}

	//if (ReadSharedInfo () == false) {
		//MessageBox (NULL, "Error reading shared info", "Info", MB_OK);
	//}

	m_drivebackup.SetHWND (m_hwnd);

	//m_header.SetBitmapResources (IDB_HEADER);
	//m_header.SetBitmapProperties (0, 0, 238, 64);
	//m_header.SetProperties (hWnd, CID_HEADER, 3, 3, 238, 64);
	//m_uihandler.AddDirectControl (&m_header);
	
	m_pdiag->SetParentHWND (hWnd);
	m_vdmanager.SetHWND (hWnd);

	m_mainmenu.CreateMainMenu (hWnd);
	m_mainmenu.CreateMainPopupMenu ();

	


	/*
	SingleDriveInfo diskinfo;
	diskinfo.lDiskSizeMegs = 100;
	strcpy_s (diskinfo.szDescription, SIZE_STRING, "This is my drive description");
	strcpy_s (diskinfo.szDriveLetter, SIZE_NAME, "Z");
	strcpy_s (diskinfo.szName, SIZE_STRING, "My test data");
	strcpy_s (diskinfo.szPath, SIZE_STRING, "D:\\Disk1.dat");

	SingleDriveInfo diskinfo2;
	diskinfo2.lDiskSizeMegs = 100;
	strcpy_s (diskinfo2.szDescription, SIZE_STRING, "This is my drive description");
	strcpy_s (diskinfo2.szDriveLetter, SIZE_NAME, "Y");
	strcpy_s (diskinfo2.szName, SIZE_STRING, "My test data");
	strcpy_s (diskinfo2.szPath, SIZE_STRING, "D:\\Disk2.dat");
	*/

	//m_vdmanager.AllocateVirtualDisk (diskinfo);
	
	//m_vdmanager.MountVirtualDisk (diskinfo);
	//m_vdmanager.MountVirtualDisk (diskinfo2);

	// Prepare the configuration file path, used for loading the drive list, and saving the drive list
	PrepareConfigPath();

	m_header.SetBitmapResources (IDB_MAINSPLASH);
	m_header.SetBitmapProperties (0, 0, 202, 387);
	m_header.SetProperties (hWnd, CID_ADDBANNER, 0, 0, 202, 387);
	m_uihandler.AddDirectControl (&m_header);

	// Create the list view control
	m_hwndlistview = CreateWindow (WC_LISTVIEW, "", WS_CHILD | WS_VISIBLE | LVS_REPORT | LVS_EDITLABELS, 202, 0, 449, 314, hWnd, (HMENU) ID_LISTVIEW, GetModuleHandle (NULL), NULL);

	// Create the info panel
	int yoffset = -23;
	m_infodrivename = CreateLabel("Drive Name: ",	218, 345+yoffset, 170, 13, ID_INFODRIVENAME);
	m_infodisksize = CreateLabel("Disk Size: ",			395, 345+yoffset, 106, 13, ID_INFODISKSIZE);
	m_infodriveletter = CreateLabel("Drive Letter: ",	524, 345+yoffset, 79, 13, ID_INFODRIVELETTER);
	m_infodescription = CreateLabel("Description: ",	218, 365+yoffset, 395, 13, ID_INFODESCRIPTION);
	m_infopath = CreateLabel("File path: ",				218, 386+yoffset, 395, 13, ID_INFOPATH);

	// Prepare the list view control
	PrepareListView();
	SetWindowLong (m_hwndlistview, GWL_STYLE, WS_CHILD | WS_VISIBLE | LVS_ICON | LVS_EDITLABELS);
	
	SingleDriveInfo tempDrive;
	SetInfoPanel (tempDrive, false);

	
	if (m_memshared.ReadSharedMemory ("CedeDriveLauncher", 1024000) == true) {
		
		m_bstandalonemode = true;

		OutputText ("CedeDriveLauncher Memory read ok. Standalone mode activated.");

		m_dlnewdrives.Clear ();
		m_dlnewdrives.FromMemoryBuffer (&m_memshared); // The shared memory we're expecting will be a dynlist

		// The launcher is now not needed, so tell it to quit
		BroadcastLauncherQuit ();

	} else {
		OutputText ("CedeDriveLauncher shared memory not found. Running in normal mode.");
	}

	
	//CheckStandaloneMode ();
	
	if (m_bstandalonemode == true) {
		
		m_passwindow.Initialise (hWnd, 0);
		m_passwindow.SetEncryptMode (false);
		m_passwindow.SetDriveName ("Removable Drive");
		m_passwindow.Show ();		

	} else {
		if (LoadConfig () == false) {
			m_passwindow.Initialise (hWnd, 0);
			m_passwindow.SetEncryptMode (true);
			m_passwindow.Show ();		
		} else {
			m_passwindow.Initialise (hWnd, 0);
			m_passwindow.SetEncryptMode (false);
			m_passwindow.Show ();		
		}
	}	
}

void MainWindow::ShareConfig ()
{
	//m_dlDrivelist

	if (LoadConfig () == true) {
		
		m_memshared.StartSharing ("CedeDriveLauncher", 1024000);			
		m_dlDrivelist.ToMemoryBuffer (&m_memshared);
		
		BroadcastIPCEvent (0, 0); // Tell the already running instance to read our shared memory

		// Now stop sharing and quit
		m_memshared.StopSharing ();
	}
}

void MainWindow::BroadcastIPCEvent (WPARAM wParam, LPARAM lParam)
{
	if (g_ipcmessageregistered == true) {
		SendMessage (HWND_BROADCAST, g_ipcmessage, wParam, lParam);
	}			
}

void MainWindow::BroadcastLauncherQuit ()
{
	// Tell the cededrive launcher to quit, as we have finished with it
	g_quitmessage = RegisterWindowMessage ("CedeDriveLaunchQuit");
	SendMessage (HWND_BROADCAST, g_quitmessage, 0, 0);
}

/*
bool MainWindow::CheckStandaloneMode ()
{
	char szModulefilename[SIZE_STRING];
	ZeroMemory (szModulefilename, SIZE_STRING);
	GetModuleFileName (NULL, szModulefilename, SIZE_STRING);	

	char szPathonly[SIZE_STRING];
	ZeroMemory (szPathonly, SIZE_STRING);
	GetPathOnly (szModulefilename, szPathonly);
	strcat_s (szPathonly, SIZE_STRING, "EncryptedDisk.evd");

	char szFreeDriveletter[SIZE_NAME];
	ZeroMemory (szFreeDriveletter, SIZE_NAME);

	if (FileExists (szPathonly) == true) {
		m_bstandalonemode = true;

		if (GetFreeDriveLetter (szFreeDriveletter) == false) {
			// no available drive letter on this system, abort!
			MessageBox (NULL, "There are no available drive letters available on this system. Cannot mount encrypted disk.", "Insufficient drive letters", MB_OK | MB_ICONEXCLAMATION);
			PostQuitMessage (0);
			return false;
		}

		// if we got here then we have an available drive letter to use for later on
		// but we need to add a colon to it.
		strcat_s (szFreeDriveletter, SIZE_NAME, ":");
		
		ZeroMemory (m_localmodedrive.szName, SIZE_STRING);
		strcpy_s (m_localmodedrive.szName, SIZE_STRING, "Standalone Encrypted Drive"); //m_localmodedrive
		
		ZeroMemory (m_localmodedrive.szDescription, SIZE_STRING);
		strcpy_s (m_localmodedrive.szDescription, SIZE_STRING, "Standalone Encrypted disk for use with removable drives.");

		ZeroMemory (m_localmodedrive.szPath, SIZE_STRING);
		strcpy_s (m_localmodedrive.szPath, SIZE_STRING, szPathonly);

		ZeroMemory (m_localmodedrive.szDriveLetter, SIZE_NAME);
		strcpy_s (m_localmodedrive.szDriveLetter, SIZE_NAME, szFreeDriveletter);
		
		m_localmodedrive.bMountStartup = true;

		// Need to specify a disk size in megs!!!!
		unsigned long vdsize = m_vdmanager.GetVirtualDiskSizeMegs (szPathonly);
		m_localmodedrive.lDiskSizeMegs = vdsize;

		OutputText ("STANDALONE MODE! Local EVD file found.");
		OutputInt ("Standalone mode disk size: ", vdsize);

	} else {
		m_bstandalonemode = false;
	}

	return true;
}
*/

void MainWindow::GetPathOnly (char *szInpath, char *szOutpath)
{
	char szCurchar[SIZE_NAME];
	int seploc = 0;

	for (int c=strlen(szInpath);c>0;c--) {
		
		ZeroMemory (szCurchar, SIZE_NAME);
		strncpy_s (szCurchar, SIZE_NAME, szInpath+c, 1);

		if (strcmp (szCurchar, "\\") == 0) {
			seploc = c+1;
			break;
		}
	}

	strncpy_s (szOutpath, SIZE_STRING, szInpath, seploc);
}

void MainWindow::SetInfoPanel (SingleDriveInfo driveinfo, bool showinfo)
{
	char szText[SIZE_STRING];

	if (showinfo == true) {

		ZeroMemory (szText, SIZE_STRING);
		sprintf_s (szText, "Drive Name: %s", driveinfo.szName);
		SetDlgItemText(m_hwnd, ID_INFODRIVENAME, szText);

		ZeroMemory (szText, SIZE_STRING);
		sprintf_s (szText, SIZE_STRING, "Disk Size: %i MB", driveinfo.lDiskSizeMegs);
		SetDlgItemText(m_hwnd, ID_INFODISKSIZE, szText);

		ZeroMemory (szText, SIZE_STRING);
		sprintf_s (szText, "Drive Letter: %s", driveinfo.szDriveLetter);
		SetDlgItemText(m_hwnd, ID_INFODRIVELETTER, szText);

		ZeroMemory (szText, SIZE_STRING);
		sprintf_s (szText, "Description: %s", driveinfo.szDescription);
		SetDlgItemText(m_hwnd, ID_INFODESCRIPTION, szText);

		ZeroMemory (szText, SIZE_STRING);
		sprintf_s (szText, "File path: %s", driveinfo.szPath);
		SetDlgItemText(m_hwnd, ID_INFOPATH, szText);

	} else {
		
		SetDlgItemText(m_hwnd, ID_INFODRIVENAME, "");
		SetDlgItemText(m_hwnd, ID_INFODISKSIZE, "");
		SetDlgItemText(m_hwnd, ID_INFODRIVELETTER, "");
		
		if (m_dlDrivelist.GetNumItems () > 0) {
			SetDlgItemText(m_hwnd, ID_INFODESCRIPTION, "Click on a drive to see more information...");
		} else {
			SetDlgItemText(m_hwnd, ID_INFODESCRIPTION, "You have no encrypted drives defined. Click File->New to create a new drive...");
		}
		
		SetDlgItemText(m_hwnd, ID_INFOPATH, "");

	}

}

void MainWindow::AddTrayIcon ()
{
	m_hSystrayicon = LoadIcon (GetModuleHandle (NULL), MAKEINTRESOURCE (ID_TRAYICON));

	m_nid.cbSize = sizeof (m_nid);
	m_nid.hWnd = m_hwnd;
	m_nid.uID = IDC_SYSTRAYICON;
	m_nid.uFlags = NIF_ICON | NIF_MESSAGE | NIF_TIP;
	m_nid.uCallbackMessage = WM_SYSTRAY;
	m_nid.hIcon = m_hSystrayicon;

	strcpy (m_nid.szTip, "CedeDrive Virtual Drive Encryption");

	// Now actually create the system tray icon.
	Shell_NotifyIcon (NIM_ADD, &m_nid);
}

void MainWindow::DeleteTrayIcon ()
{
	m_nid.cbSize = sizeof (m_nid);
	m_nid.hWnd = m_hwnd;
	m_nid.uID = IDC_SYSTRAYICON;
	m_nid.uFlags = NIF_ICON | NIF_MESSAGE | NIF_TIP;
	m_nid.uCallbackMessage = WM_SYSTRAY;
	m_nid.hIcon = m_hSystrayicon;

	strcpy (m_nid.szTip, "CedeDrive Virtual Drive Encryption");

	// Now actually create the system tray icon.
	Shell_NotifyIcon (NIM_DELETE, &m_nid);
}

void MainWindow::PrepareConfigPath ()
{
	int iRes = 0;
	char szAppData[SIZE_STRING];
	char szCompanyAppData[SIZE_STRING];
	char szProgramAppData[SIZE_STRING];
	char szInfoAppData[SIZE_STRING];

	ZeroMemory (szAppData, SIZE_STRING);
	ZeroMemory (szCompanyAppData, SIZE_STRING);
	ZeroMemory (szProgramAppData, SIZE_STRING);
	ZeroMemory (szInfoAppData, SIZE_STRING);

	if (GetEnvironmentVariable ("APPDATA", szAppData, SIZE_STRING) == 0) {
		// retrieving environment variable failed.
		//m_diag.OutputText ("Failed to retrieve APPDATA environment variable!");
		return;
	}

	strcpy_s (szCompanyAppData, SIZE_STRING, szAppData);
	strcat_s (szCompanyAppData, SIZE_STRING, "\\CedeSoft");
	_mkdir (szCompanyAppData);

	strcpy_s (szProgramAppData, SIZE_STRING, szCompanyAppData);
	strcat_s (szProgramAppData, SIZE_STRING, "\\CedeDrive");
	_mkdir (szProgramAppData);

	strcpy_s (szInfoAppData, SIZE_STRING, szProgramAppData);
	strcat_s (szInfoAppData, SIZE_STRING, "\\DriveList.dat");

	// Set our member variable which is used by the other functions
	ZeroMemory (m_szConfigpath, SIZE_STRING);
	strcpy_s (m_szConfigpath, SIZE_STRING, szInfoAppData);
	
	return;
}

void MainWindow::DeleteDrive (SingleDriveInfo *pinfo, bool bDeletehostfile)
{
	DynList dlTemp;

	SingleDriveInfo *ptemp;
	SingleDriveInfo tempDrive;

	for (int d=0;d<m_dlDrivelist.GetNumItems ();d++)
	{
		ptemp = (SingleDriveInfo *) m_dlDrivelist.GetItem (d);
		memcpy (&tempDrive, (SingleDriveInfo *) ptemp, sizeof (SingleDriveInfo));

		if (strcmp (ptemp->szPath, pinfo->szPath) != 0) {
			dlTemp.AddItem (&tempDrive, sizeof (SingleDriveInfo), false);
		} 

	}

	m_dlDrivelist.Clear();

	for (int t=0;t<dlTemp.GetNumItems();t++) {
		ptemp = (SingleDriveInfo *) dlTemp.GetItem (t);
		memcpy (&tempDrive, (SingleDriveInfo *) ptemp, sizeof (SingleDriveInfo));

		m_dlDrivelist.AddItem (&tempDrive, sizeof (SingleDriveInfo), false);
	}

	if (bDeletehostfile == true) {
		// Now actually delete the file hosting the virtual disk
		DeleteFile (pinfo->szPath);
	}
}

bool MainWindow::FileExists (char *FileName)
{
	// Check if the file exists		
	struct _finddata_t c_file;
	long hFile;

	// Build the list of files in the source path
	if( (hFile = _findfirst(FileName, &c_file )) == -1L ) {
		return false;
	} else {
		return true;
	}

	return false;
}

void MainWindow::SaveConfig ()
{
	int numdrives = 0;
	int encpasssize = 0;
	int d = 0;
	int tempsize = 0;
	MemoryBuffer memSer;

	SingleDriveInfo *pinfo;

	//m_memEncpassword
	// Calculate the size of the serialised buffer
	
	int sersize = (sizeof (SingleDriveInfo) * m_dlDrivelist.GetNumItems ());
	sersize += sizeof (int) + m_memEncpassword.GetSize(); // Account for the size of the encryption password
	sersize += sizeof (int); // Plus additional room to store the number of contacts

	memSer.SetSize (sersize);

	numdrives = m_dlDrivelist.GetNumItems ();
	encpasssize = m_memEncpassword.GetSize();

	// Add the size of the encryption password to the buffer
	memSer.Append (&encpasssize, sizeof (int));
	memSer.Append (m_memEncpassword.GetBuffer(), encpasssize);

	// Add the number of drives to the buffer
	memSer.Append (&numdrives, sizeof (int));

	for (d=0;d<m_dlDrivelist.GetNumItems ();d++) {
		
		pinfo = (SingleDriveInfo *) m_dlDrivelist.GetItem (d);

		memSer.Append (&pinfo->bMountStartup, sizeof (bool));
		memSer.Append (&pinfo->lDiskSizeMegs, sizeof (long));
		
		tempsize = strlen (pinfo->szDescription);
		memSer.Append (&tempsize, sizeof (int));
		memSer.Append ((char *) pinfo->szDescription, tempsize);

		tempsize = strlen (pinfo->szDriveLetter);
		memSer.Append (&tempsize, sizeof (int));
		memSer.Append ((char *) pinfo->szDriveLetter, tempsize);

		tempsize = strlen (pinfo->szName);
		memSer.Append (&tempsize, sizeof (int));
		memSer.Append ((char *) pinfo->szName, tempsize);

		tempsize = strlen (pinfo->szPath);
		memSer.Append (&tempsize, sizeof (int));
		memSer.Append ((char *) pinfo->szPath, tempsize);

	}

	if (strlen (m_szConfigpath) > 0) {
		memSer.SaveToFile (m_szConfigpath); // Save the config to disk
	}
}

bool MainWindow::LoadConfig ()
{

	int numdrives = 0;
	int encpasssize = 0;
	int d = 0;
	int ipointer = 0;
	int tempsize = 0;
	SingleDriveInfo tempdrive;
	
	MemoryBuffer memSer;
	
	if (strlen (m_szConfigpath) == 0) {
		return false;
	}

	if (FileExists (m_szConfigpath) == false) {
		return false;
	}

	memSer.ReadFromFile (m_szConfigpath);

	memcpy (&encpasssize, (BYTE *) memSer.GetBuffer()+ipointer, sizeof (int));
	ipointer += sizeof (int);

	if (encpasssize > 0) {
		m_memEncpassword.SetSize (encpasssize);
		m_memEncpassword.Write ((BYTE *) memSer.GetBuffer()+ipointer, 0, encpasssize);
	}

	ipointer+= encpasssize;

	// Get the number of drives
	memcpy (&numdrives, (BYTE *) memSer.GetBuffer()+ipointer, sizeof (int));
	ipointer += sizeof (int);

	for (d=0;d<numdrives;d++) {
		
		memcpy (&tempdrive.bMountStartup, (BYTE *) memSer.GetBuffer()+ipointer, sizeof (bool));
		ipointer += sizeof (bool);

		memcpy (&tempdrive.lDiskSizeMegs, (BYTE *) memSer.GetBuffer()+ipointer, sizeof (long));
		ipointer += sizeof (long);

		// The Description field
		memcpy (&tempsize, (BYTE *) memSer.GetBuffer()+ipointer, sizeof (int));
		ipointer += sizeof (int);

		if (tempsize > 0) {
			ZeroMemory (tempdrive.szDescription, SIZE_STRING);
			strncpy_s (tempdrive.szDescription, SIZE_STRING, (char *) memSer.GetBuffer()+ipointer, tempsize);
		} else {
			ZeroMemory (tempdrive.szDescription, SIZE_STRING);
		}
		ipointer+=tempsize;

		// The Drive Letter
		memcpy (&tempsize, (BYTE *) memSer.GetBuffer()+ipointer, sizeof (int));
		ipointer += sizeof (int);

		if (tempsize > 0) {
			ZeroMemory (tempdrive.szDriveLetter, SIZE_NAME);
			strncpy_s (tempdrive.szDriveLetter, SIZE_NAME, (char *) memSer.GetBuffer()+ipointer, tempsize);
		} else {
			ZeroMemory (tempdrive.szDriveLetter, SIZE_NAME);
		}
		ipointer+=tempsize;

		// The Name Field
		memcpy (&tempsize, (BYTE *) memSer.GetBuffer()+ipointer, sizeof (int));
		ipointer += sizeof (int);

		if (tempsize > 0) {
			ZeroMemory (tempdrive.szName, SIZE_STRING);
			strncpy_s (tempdrive.szName, SIZE_STRING, (char *) memSer.GetBuffer()+ipointer, tempsize);
		} else {
			ZeroMemory (tempdrive.szName, SIZE_STRING);
		}
		ipointer+=tempsize;


		// The Path Field
		memcpy (&tempsize, (BYTE *) memSer.GetBuffer()+ipointer, sizeof (int));
		ipointer += sizeof (int);

		if (tempsize > 0) {
			ZeroMemory (tempdrive.szPath, SIZE_STRING);
			strncpy_s (tempdrive.szPath, SIZE_STRING, (char *) memSer.GetBuffer()+ipointer, tempsize);
		} else {
			ZeroMemory (tempdrive.szPath, SIZE_STRING);
		}
		ipointer+=tempsize;


		// Now add this temp drive to the list of drives
		m_dlDrivelist.AddItem (&tempdrive, sizeof (SingleDriveInfo), false);
	}

	return true;

}

void MainWindow::PrepareListView ()
{
	// Create the image lists
    HICON hiconSmall;     // icon for list-view items
	HICON hiconLarge;     // icon for list-view items	

	// Large icon image list
	m_hLarge = ImageList_Create(32, 32, ILC_COLOR32 , 1, 1); 
    // Small Icon image list
	m_hSmall = ImageList_Create(16, 16, ILC_COLOR32 , 1, 1); 

    hiconSmall = LoadIcon(GetModuleHandle (NULL), MAKEINTRESOURCE(IDI_ICON));
	hiconLarge = LoadIcon(GetModuleHandle (NULL), MAKEINTRESOURCE(IDI_ICON));	

    ImageList_AddIcon(m_hLarge, hiconLarge);	
    ImageList_AddIcon(m_hSmall, hiconSmall);
	
	DestroyIcon(hiconLarge);
	DestroyIcon(hiconSmall); 
	
    ListView_SetImageList(m_hwndlistview, m_hLarge, LVSIL_NORMAL); 
    ListView_SetImageList(m_hwndlistview, m_hSmall, LVSIL_SMALL); 


	// Add columns to the list view
	char szColname[SIZE_NAME];
	ZeroMemory (szColname, SIZE_NAME);
	strcpy_s (szColname, SIZE_NAME, "Drive Name");

	char szColname2[SIZE_NAME];
	ZeroMemory (szColname2, SIZE_NAME);
	strcpy_s (szColname2, SIZE_NAME, "Drive Size");

	char szColname3[SIZE_NAME];
	ZeroMemory (szColname3, SIZE_NAME);
	strcpy_s (szColname3, SIZE_NAME, "Drive Letter");

	LVCOLUMN lvc;
	int iCol = 0;

	lvc.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM; 
	
    lvc.iSubItem = 0;
    lvc.pszText = szColname;	
    lvc.cx = 100;     // width of column in pixels
	lvc.fmt = LVCFMT_LEFT;  // left-aligned column
	
	ListView_InsertColumn(m_hwndlistview, 0, &lvc);

	lvc.iSubItem = 1;
	lvc.pszText = szColname2;	
	ListView_InsertColumn(m_hwndlistview, 1, &lvc);

	lvc.iSubItem = 2;
	lvc.pszText = szColname3;	
	ListView_InsertColumn(m_hwndlistview, 2, &lvc);
}

bool MainWindow::IsDriveAccessible(SingleDriveInfo driveinfo)
{
	MemoryBuffer memData;

	char szString[SIZE_STRING];
	ZeroMemory (szString, SIZE_STRING);
	strcpy_s (szString, SIZE_STRING, "This is a test file which is saved to determine if a drive is accessible");

	memData.SetSize (strlen (szString));
	memData.Write (szString, 0, strlen (szString));

	char szFilename[SIZE_STRING];
	ZeroMemory (szFilename, SIZE_STRING);
	strcpy_s (szFilename, SIZE_STRING, driveinfo.szDriveLetter);
	strcat_s (szFilename, SIZE_STRING, "\\_CedeSoft_CedeDrive_Test_DataWrite.txt");

	OutputText ("Checking accessibility: ", szFilename);

	if (memData.SaveToFile (szFilename) == true) {
		DeleteFile (szFilename);
		return true;
	} else {
		return false;
	}

}

void MainWindow::VerifyPassword ()
{
	//MessageBox (NULL, "Password to be verified", "Test", MB_OK);
	char szEnctext[SIZE_STRING];
	ZeroMemory (szEnctext, SIZE_STRING);
	strcpy_s (szEnctext, SIZE_STRING, "Unto You I lift up my eyes, O You who dwell in the heavens. Behold, as the eyes of servants look to the hand of their masters, As the eyes of a maid to the hand of her mistress, So our eyes look to the LORD our God, Until He has mercy on us. Have mercy on us, O LORD, have mercy on us! For we are exceedingly filled with contempt. Our soul is exceedingly filled With the scorn of those who are at ease, With the contempt of the proud. (Psalms 123).");

	char szPassword[SIZE_STRING];
	ZeroMemory (szPassword, SIZE_STRING);

	if (m_bstandalonemode == true) {
		strcpy_s (szPassword, SIZE_STRING, m_passwindow.GetLastPassword ());

		//m_dlDrivelist.Clear ();
		SingleDriveInfo *pdrive;
		for (int d=0;d<m_dlnewdrives.GetNumItems ();d++) {
			pdrive = (SingleDriveInfo *) m_dlnewdrives.GetItem (d);
			pdrive->bUsingseperatepassword = true;
			strcpy_s (pdrive->szPassword, SIZE_STRING, szPassword);

			m_dlDrivelist.AddItem (pdrive, sizeof (SingleDriveInfo), false);
		}

		//m_dlDrivelist.AddItem (&m_localmodedrive, sizeof (SingleDriveInfo), false);

		// Accept whatever password is supplied
		m_vdmanager.Initialise (szPassword);
		RefreshListView();

		AddTrayIcon();

		// Now mount all drives set to mount at startup only
		MountAllDrives(true);

		// Now check if the new drives are accessible, if they are writeable then it means the
		// password was correct, otherwise it will not be possible to write to the drive
		// because it will appear to have an unrecognised filesystem.
		/*
		int iTesterrors = 0;
		SingleDriveInfo tdrive;
		for (int i=0;i<m_dlnewdrives.GetNumItems ();i++)
		{
			pdrive = (SingleDriveInfo *) m_dlnewdrives.GetItem (i);
			ZeroMemory (&tdrive, sizeof (SingleDriveInfo));
			memcpy (&tdrive, pdrive, sizeof (SingleDriveInfo));

			if (IsDriveAccessible (tdrive) == false) {
				
				m_vdmanager.UnmountVirtualDrive (tdrive);
				DeleteDrive (&tdrive, false);
				iTesterrors++;
			}
		}

		if (iTesterrors > 0) {
			if (iTesterrors == 1) {
				MessageBox (NULL, "Your virtual drive is not accessible using the password you supplied.", "Access Denied", MB_OK | MB_ICONEXCLAMATION);
			} else {
				MessageBox (NULL, "Your virtual drives are not accessible using the password you supplied.", "Access Denied", MB_OK | MB_ICONEXCLAMATION);			
			}

			m_passwindow.Initialise (m_hwnd, 0);
			m_passwindow.SetEncryptMode (false);
			m_passwindow.SetDriveName ("Removable Drive");
			m_passwindow.Show ();
		}
		*/

		return;
	}

	if (m_passwindow.m_bEncryptmode == true) {
		// This is the first time a user has entered a password, so we must save it
		
		strcpy_s (szPassword, SIZE_STRING, m_passwindow.GetLastPassword ());

		m_memEncpassword.SetSize (strlen(szEnctext));
		m_memEncpassword.Write ((char *) szEnctext, 0, strlen (szEnctext));

		m_enc.EncryptBuffer (&m_memEncpassword, szPassword, true);

		SaveConfig (); // Save the configuration file which will have the Biblical text above encrypted with the users password.

		m_vdmanager.Initialise (szPassword);

		AddTrayIcon();
	} else {
	
		strcpy_s (szPassword, SIZE_STRING, m_passwindow.GetLastPassword ());

		// Now take a copy of the encrypted buffer from the config file. We need to attempt decryption using the users password supplied in order to validate the password.
		MemoryBuffer memTemp;
		memTemp.SetSize (m_memEncpassword.GetSize());
		memTemp.Write ((BYTE *) m_memEncpassword.GetBuffer(), 0, m_memEncpassword.GetSize());

		// Now decrypt the temporary buffer using the supplied password
		m_enc.EncryptBuffer (&memTemp, szPassword, false);

		// Now check the result of the decryption against what we expect. - The Biblical Psalm.
		char szDecrypted[SIZE_STRING];
		ZeroMemory (szDecrypted, SIZE_STRING);
		strncpy_s (szDecrypted, SIZE_STRING, (char *) memTemp.GetBuffer(), memTemp.GetSize());		

		// Did it match?
		if (strcmp (szDecrypted, szEnctext) == 0) {
			// Password ok
			m_vdmanager.Initialise (szPassword);
			RefreshListView();

			AddTrayIcon();

			// Now mount all drives set to mount at startup only
			MountAllDrives(true);
		} else {
			m_wrongpasswordcount++;

			MessageBox (NULL, "Access Denied. Incorrect password.", "Incorrect Password", MB_ICONEXCLAMATION | MB_OK);
			
			if (m_wrongpasswordcount > 3) {
				MessageBox (NULL, "You have entered the Incorrect password over 3 times. Goodbye.", "Maximum attempts reached", MB_ICONEXCLAMATION | MB_OK);
				PostQuitMessage(0);
			} else {
				// Show the password window again						
				m_passwindow.Initialise (m_hwnd, 0);
				m_passwindow.SetEncryptMode (false);
				m_passwindow.Show ();
			}
		}
	}
}

void MainWindow::RefreshListView ()
{
	// Now add a few items to the list view
	LVITEM lvI;
	int index = 0;	
	SingleDriveInfo* pinfo;

	ListView_DeleteAllItems (m_hwndlistview);

	lvI.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM | LVIF_STATE; 
	lvI.state = 0; 
	lvI.stateMask = 0; 	

	for (index=0;index < m_dlDrivelist.GetNumItems ();index++) {
		pinfo = (SingleDriveInfo *) m_dlDrivelist.GetItem (index);

		lvI.iItem = index;						
		lvI.iImage = 0; // Set the image number in the image list for other images				
		lvI.iSubItem = 0;
		lvI.lParam = 0;
		lvI.pszText = LPSTR_TEXTCALLBACK; 							  
		ListView_InsertItem(m_hwndlistview, &lvI);
	}
}

int MainWindow::LV_GetFirstSelectedIndex (HWND hwndlv)
{
	int iNumitems = ListView_GetItemCount (hwndlv);
	int index = 0;
	LVITEM lvi;

	//m_pdiag->OutputInt ("ListView Item count is: ", iNumitems);

	for (index=0;index<iNumitems;index++) {
		lvi.mask = LVIF_STATE;
		lvi.stateMask = LVIS_SELECTED;
		lvi.iItem = index;
		lvi.iSubItem = 0;

		ListView_GetItem (hwndlv, &lvi);

		//m_pdiag->OutputText ("Item Text: ", lvi.pszText);
		if (lvi.state == LVIS_SELECTED) {
			return index;
		}
	}

	return -1;
}

void MainWindow::OnSysTray (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	UINT uMouseMsg;

	uMouseMsg = (UINT) lParam;
	if (uMouseMsg == WM_RBUTTONDOWN) {
		POINT mousepoints;
		GetCursorPos (&mousepoints);
		SetForegroundWindow (m_nid.hWnd); // Without this, you can't dismiss the popup menu without clicking an option (apparently a bug in windows)
		TrackPopupMenuEx (m_mainmenu.m_hTrayMenu, TPM_LEFTALIGN | TPM_LEFTBUTTON, mousepoints.x, mousepoints.y, hWnd, NULL);
	}

	if (uMouseMsg == WM_LBUTTONDBLCLK) {
		Show ();
	}
}

void MainWindow::OnCryptEvent (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	// Notify the communications object of an encryption event message.
	
}

void MainWindow::OnNotify (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	switch (((LPNMHDR) lParam)->code)
	{
		case NM_CLICK:
		{
			int index = LV_GetFirstSelectedIndex(m_hwndlistview);


			if (index != -1) {

				SingleDriveInfo tempDrive;
				SingleDriveInfo *pinfo = (SingleDriveInfo *) m_dlDrivelist.GetItem (index);					
				memcpy (&tempDrive, (SingleDriveInfo *) pinfo, sizeof (SingleDriveInfo));
				SetInfoPanel (tempDrive, true);
			} else {
				SingleDriveInfo tempDrive;
				SetInfoPanel (tempDrive, false);
			}
		}
		break;
		case NM_RCLICK:
		{
				POINT mousepoints;
				GetCursorPos (&mousepoints);
				//SetForegroundWindow (hWnd); // Without this, you can't dismiss the popup menu without clicking an option (apparently a bug in windows)
				TrackPopupMenuEx (m_mainmenu.m_hPopupMenu, TPM_LEFTALIGN | TPM_LEFTBUTTON, mousepoints.x, mousepoints.y, hWnd, NULL);
		}
		break;
		case NM_DBLCLK:
		{
			OutputText ("Double Click on ListView!");
			

			int iCuritem = LV_GetFirstSelectedIndex (m_hwndlistview);

			if (iCuritem != -1) { // If something was selected
			
				//pinfo = (SingleFileInfo *) m_dlListviewfiles.GetItem (iCuritem);

				/*
				if (pinfo->bIsDir == true) {
					m_dlFolderhistory.AddItem (m_szCurrentfolder, SIZE_STRING, false);
					strcat_s (m_szCurrentfolder, SIZE_STRING, "\\");
					strcat_s (m_szCurrentfolder, SIZE_STRING, pinfo->szName);
					RefreshListView (m_szCurrentfolder);
				}
				*/
			}

		}
		break;
		case LVN_GETDISPINFO:
		{
			NMLVDISPINFO *lpinfo;		

			lpinfo = (NMLVDISPINFO *) lParam;
			
			//pfinfo = (SingleFileInfo *) m_dlListviewfiles.GetItem (lpinfo->item.iItem);
			SingleDriveInfo *pdinfo;

			pdinfo = (SingleDriveInfo *) m_dlDrivelist.GetItem (lpinfo->item.iItem);


			switch (lpinfo->item.iSubItem)
			{
				case 0:
				{
					lpinfo->item.pszText = pdinfo->szName;
				}
				break;

				case 1:
				{
					char szSize[SIZE_STRING];
					ZeroMemory (szSize, SIZE_STRING);
					ltoa (pdinfo->lDiskSizeMegs, szSize, 10);

					lpinfo->item.pszText = szSize;
				}
				break;

				case 2:
				{
					lpinfo->item.pszText = pdinfo->szDriveLetter;
				}
				break;
			}

		}
		break;
	}

	return;	
}

/*
#define IDM_TRAY_EXIT				712
#define IDM_TRAY_SHOW			713
#define IDM_TRAY_MOUNTALL	714
#define IDM_TRAY_UNMOUNTALL	715
*/

void MainWindow::MountAllDrives (bool startuponly)
{
	bool bError = false;

	for (int d=0;d<m_dlDrivelist.GetNumItems();d++) {
		
		SingleDriveInfo tempDrive;
		SingleDriveInfo *pinfo = (SingleDriveInfo *) m_dlDrivelist.GetItem (d);					
		memcpy (&tempDrive, (SingleDriveInfo *) pinfo, sizeof (SingleDriveInfo));

		if (startuponly == true) {
			if (tempDrive.bMountStartup == true) {
				if (m_vdmanager.MountVirtualDisk (tempDrive) != 0) {
					bError = true;
				}
			}
		} else {
			if (m_vdmanager.MountVirtualDisk (tempDrive) != 0) {
				bError = true;
			}
		}

		
	}

	if (bError == false) {
		// Show success message
		if (startuponly == false) {
			MessageBox (NULL, "All encrypted drives mounted successfully.", "Mount successful", MB_OK | MB_ICONINFORMATION);
		}
	} else {
		MessageBox (NULL, "There was a problem mounting one or more of your encrypted drives.", "Mounting error", MB_OK | MB_ICONEXCLAMATION);
	}
}

void MainWindow::PrepareCedeCryptPath ()
{
	ZeroMemory (m_szCedeCryptPath, SIZE_STRING);
	//m_registry.WriteString ("Software\\CedeSoft\\CedeCrypt", "ShellExtensionPath", szModulepath);
	if (m_registry.DoesValueExistCU ("Software\\CedeSoft\\CedeCrypt", "ShellExtensionPath", REG_SZ) == true) {
		//m_bCedeCryptPathfound = true;
		strcpy_s (m_szCedeCryptPath, SIZE_STRING, m_registry.ReadStringCU ("Software\\CedeSoft\\CedeCrypt", "ShellExtensionPath"));

		MessageBox (NULL,m_szCedeCryptPath, "Caption", MB_OK);
	} else {
		//m_bCedeCryptPathfound = false;
		MessageBox (NULL, "Reg Key NOT found!", "Caption", MB_OK);
	}
}

unsigned long long MainWindow::GetFreeDiskSpace (char *szDriveletter)
{
	unsigned long long lLimit = 0;
	unsigned long long lFree = 0;
	unsigned long long lSize = 0;
	unsigned long long lTotal = 0;
	

	char szDrive[SIZE_NAME];
	ZeroMemory (szDrive, SIZE_NAME);
	strcpy_s (szDrive, SIZE_NAME, szDriveletter);
	strcat_s (szDrive, SIZE_NAME, ":\\");

	//double fAmount = 0;
	//double fAvail = 0;
	//int iPixelprogress = 0;
	//int iPercent = 0;

	//lSize = 3000000000;

	//char szVolumename[SIZE_STRING];
	//ZeroMemory (szVolumename, SIZE_STRING);

	//DWORD dwSizevolname = SIZE_STRING;

	//GetVolumeInformation (szDrive, szVolumename, dwSizevolname, NULL, NULL, NULL, NULL, NULL);

	if (GetDiskFreeSpaceEx (szDrive, (PULARGE_INTEGER) &lFree, (PULARGE_INTEGER) &lLimit, (PULARGE_INTEGER) &lTotal) == 0) {
		//m_pdiag->OutputInt ("GetDiskFreeSpace failed: Error: ", GetLastError ());
	}

	return lFree;
}

DWORD WINAPI MainWindow::ConvertDriveProc (PVOID pParam)
{
	// The worker thread in which the drive conversion takes place

	MainWindow *pmainwnd = (MainWindow *) pParam;
	pmainwnd->ConvertRemovableDrive ();

	//PostMessage (pmainwnd->m_hwnd, WM_UICOMMAND, CID_PSTSCANCOMPLETE, 0);

	return 0;
}

void MainWindow::StartDriveConverter ()
{
	// Create the separate thread which converts the drive
	DWORD dwThreadID;
	m_hConvertthread = CreateThread (NULL, 0, ConvertDriveProc, (MainWindow *) this, 0, &dwThreadID);
}

bool MainWindow::IsDriveConverted (char *szDriveletter)
{
	char szEvdpath[SIZE_STRING];
	ZeroMemory (szEvdpath, SIZE_STRING);

	strcpy_s (szEvdpath, SIZE_STRING, szDriveletter);
	strcat_s (szEvdpath, SIZE_STRING, ":\\EncryptedDisk.evd");

	if (FileExists (szEvdpath) == true) {
		return true;
	} else {
		return false;
	}
}

void MainWindow::ConvertRemovableDrive ()
{
	m_convertwindow.SetButtonEnabled (false);
	m_convertwindow.SetInputVisible (false);
	m_convertwindow.SetStagesVisible (true);
	m_convertwindow.InvalidateWindow ();

	char szDriveletter[SIZE_NAME];
	ZeroMemory (szDriveletter, SIZE_NAME);
	strncpy_s (szDriveletter, SIZE_NAME, m_convertdriveinfo.szDriveLetter, 1);

	if (IsDriveConverted (szDriveletter) == true) {
		m_convertwindow.InvalidateWindow ();
		m_convertwindow.Reset ();
		m_convertwindow.SetStagesVisible (false);
		m_convertwindow.SetInputVisible (true);
		m_convertwindow.SetButtonEnabled (true);
		MessageBox (NULL, "This removable storage device has already been converted into an encrypted device. The conversion process will now abort.", "Already encrypted.", MB_OK | MB_ICONINFORMATION);
		return;
	}

	char szFreeDriveletter[SIZE_NAME];
	ZeroMemory (szFreeDriveletter, SIZE_NAME);
	
	char szFreeDrivelettercolon[SIZE_NAME];
	ZeroMemory (szFreeDrivelettercolon, SIZE_NAME);

	if (GetFreeDriveLetter (szFreeDriveletter) == false) {
		// no available drive letter on this system, abort!
		m_convertwindow.InvalidateWindow ();
		m_convertwindow.Reset ();
		m_convertwindow.SetStagesVisible (false);
		m_convertwindow.SetInputVisible (true);
		m_convertwindow.SetButtonEnabled (true);

		MessageBox (NULL, "There are no available drive letters available on this system. Conversion cannot continue.", "Insufficient drive letters", MB_OK | MB_ICONEXCLAMATION);
		return;
	}

	// if we got here then we have an available drive letter to use for later on
	// but we need to add a colon to it.
	strcpy_s (szFreeDrivelettercolon, SIZE_NAME, szFreeDriveletter);
	strcat_s (szFreeDrivelettercolon, SIZE_NAME, ":");
	
	m_convertwindow.SetStage (STAGE_BACKUP, STATUS_INPROGRESS);
	if (m_drivebackup.BackupDrive (szDriveletter) == true) {

		m_convertwindow.SetStage (STAGE_BACKUP, STATUS_SUCCESS);
		m_convertwindow.SetStageProgress (STAGE_BACKUP, 100);
		m_convertwindow.SetStage (STAGE_FORMAT, STATUS_INPROGRESS);
		m_convertwindow.InvalidateWindow ();

		if (m_driveformat.QuickFormatDrive (szDriveletter) == false) {

			m_convertwindow.SetStage (STAGE_FORMAT, STATUS_FAILED);
			char szReason[SIZE_STRING];
			m_driveformat.GetErrorReason (szReason, SIZE_STRING);
			MessageBox (NULL, szReason, "Format Failed.", MB_OK);
			m_convertwindow.SetButtonMode (false);
			m_convertwindow.SetButtonEnabled (true);
			m_convertwindow.SetStagesComplete (false);

		} else {
			//MessageBox (NULL, "Format was successful", "Format", MB_OK);
			m_convertwindow.SetStage (STAGE_FORMAT, STATUS_SUCCESS);
			m_convertwindow.SetStage (STAGE_CREATE, STATUS_INPROGRESS);
			m_convertwindow.InvalidateWindow ();
			
			// Get the amount of free disk space on the target drive
			unsigned long long lFree = GetFreeDiskSpace (szDriveletter);
			double fFreemegs = (double) lFree / (1024*1024);

			OutputInt ("Free Disk Space (MB): ", fFreemegs);

			// Take 10 megs off just to be safe
			if (fFreemegs > 10) {
				fFreemegs-=10;
			}

			OutputInt ("Useable Free Disk Space (MB): ", fFreemegs);

			if (fFreemegs > 0) {
				
				SingleDriveInfo tempdrive;
				tempdrive.lDiskSizeMegs = fFreemegs;
				
				tempdrive.bUsingseperatepassword = true;
				ZeroMemory (tempdrive.szPassword, SIZE_STRING);
				strcpy_s (tempdrive.szPassword, SIZE_STRING, m_szConvertpassword);

				ZeroMemory (tempdrive.szPath, SIZE_STRING);
				strcpy_s (tempdrive.szPath, SIZE_STRING, szDriveletter);
				strcat_s (tempdrive.szPath, SIZE_STRING, ":\\EncryptedDisk.evd");

				tempdrive.bFormatdriveaftermount = true;
				ZeroMemory (tempdrive.szVolumename, SIZE_STRING);
				strcpy_s (tempdrive.szVolumename, SIZE_STRING, "Encrypted Memory Stick");

				if (m_vdmanager.AllocateStandaloneVirtualDisk (tempdrive) == true) {					
					m_convertwindow.InvalidateWindow ();
					m_convertwindow.SetStage (STAGE_CREATE, STATUS_SUCCESS);
					m_convertwindow.SetStageProgress (STAGE_CREATE, 100);
					m_convertwindow.SetStage (STAGE_RESTORE, STATUS_INPROGRESS);

					//MessageBox (NULL, "Virtual Disk Allocated ok.", "Allocation ok.", MB_OK);
					
					ZeroMemory (tempdrive.szDriveLetter, SIZE_NAME);
					strcpy_s (tempdrive.szDriveLetter, SIZE_NAME, szFreeDrivelettercolon); // Need to enumerate available drive letters!!!
					
					memcpy (&m_converttempdrive, &tempdrive, sizeof (SingleDriveInfo));

					// Ask the main thread to mount our temporary drive, then suspend the thread
					//PostMessage (m_hwnd, WM_UICOMMAND, CID_CONVERTMOUNTREQUEST, 0);
					//SuspendThread (m_hConvertthread);
					int ires = m_vdmanager.MountVirtualDisk (tempdrive);

					char szMessage[SIZE_STRING];
					ZeroMemory (szMessage, SIZE_STRING);

					if (ires == 0) {

						if (m_drivebackup.RestoreDrive (szDriveletter, szFreeDriveletter) == true) {
							m_convertwindow.InvalidateWindow ();
							m_convertwindow.SetStage (STAGE_RESTORE, STATUS_SUCCESS);
							m_convertwindow.SetStageProgress (STAGE_RESTORE, 100);
							m_convertwindow.SetStage (STAGE_COMPLETION, STATUS_INPROGRESS);

							MemoryBuffer memLauncher;
							if (memLauncher.ReadFromResource (MAKEINTRESOURCE(IDB_LAUNCHERAPP)) == 0) {
								
								// Prepare the save path for the launcher

								char szLauncherpath[SIZE_STRING];
								ZeroMemory (szLauncherpath, SIZE_STRING);
								strcpy_s (szLauncherpath, SIZE_STRING, szDriveletter);
								strcat_s (szLauncherpath, SIZE_STRING, ":\\CedeDriveLaunch.exe");
								
								if (memLauncher.SaveToFile (szLauncherpath) == true) {
									
									
									m_drivebackup.DeleteRestore (szDriveletter);
									m_vdmanager.UnmountVirtualDrive (tempdrive);
									m_convertwindow.InvalidateWindow ();
									m_convertwindow.SetStage (STAGE_COMPLETION, STATUS_SUCCESS);
									m_convertwindow.SetButtonMode (false);
									m_convertwindow.SetButtonEnabled (true);
									m_convertwindow.SetStagesComplete (true);
									
									MessageBox (NULL, "Removable storage converted successfully. You may now use this drive in any PC. \n\nRemember to run the CedeDriveLaunch.exe application in order to mount your encrypted drive.", "Conversion successful", MB_OK | MB_ICONINFORMATION);
								} else {
									m_convertwindow.SetButtonMode (false);
									m_convertwindow.SetButtonEnabled (true);
									m_convertwindow.SetStagesComplete (false);
									MessageBox (NULL, "Unable to save launcer to removable storage! Conversion failed.", "Unable to save launcher", MB_OK | MB_ICONEXCLAMATION);
								}

							} else {
								m_convertwindow.SetButtonMode (false);
								m_convertwindow.SetButtonEnabled (true);
								m_convertwindow.SetStagesComplete (false);
								MessageBox (NULL, "Unable to read launcher resource! Conversion failed.", "Launcher Resource Error", MB_OK | MB_ICONEXCLAMATION);	
							}
							
							//MessageBox (NULL, "Conversion complete.", "Success", MB_OK);
						} else {
							m_convertwindow.SetButtonMode (false);
							m_convertwindow.SetButtonEnabled (true);
							m_convertwindow.SetStagesComplete (false);
							m_convertwindow.SetStage (STAGE_RESTORE, STATUS_FAILED);
							MessageBox (NULL, "Restoration failed.", "Failed.", MB_OK);
						}
						//sprintf_s (szMessage, "Encrypted Drive '%s' was mounted successfully as drive %s.", tempdrive.szName, tempdrive.szDriveLetter);
						//MessageBox (NULL, szMessage, "Mount Successful", MB_OK | MB_ICONINFORMATION);

					} else {						
						m_convertwindow.SetButtonMode (false);
						m_convertwindow.SetButtonEnabled (true);
						m_convertwindow.SetStagesComplete (false);
						m_convertwindow.SetStage (STAGE_RESTORE, STATUS_FAILED);

						m_vdmanager.GetMountError (szMessage);						
						MessageBox (NULL, szMessage, "Mount Failed", MB_OK | MB_ICONEXCLAMATION);						
					}


				} else {
					m_convertwindow.SetButtonMode (false);
					m_convertwindow.SetButtonEnabled (true);
					m_convertwindow.SetStagesComplete (false);
					m_convertwindow.SetStage (STAGE_CREATE, STATUS_FAILED);
					MessageBox (NULL, "Allocation Failed.", "Allocation Failed.", MB_OK);
				}

			} else {
				m_convertwindow.SetButtonMode (false);
				m_convertwindow.SetButtonEnabled (true);
				m_convertwindow.SetStagesComplete (false);
				m_convertwindow.SetStage (STAGE_CREATE, STATUS_FAILED);
				MessageBox (NULL, "Not enough free space for encrypted disk.", "No Free space.", MB_OK);
			}


			/*
			if (m_drivebackup.RestoreDrive ("H") == true) {

				MessageBox (NULL, "Restoration Successful", "Drive Restore", MB_OK);

				if (m_drivebackup.DeleteRestore ("H") == true) {
					MessageBox (NULL, "Cleanup Successful", "Drive Restore", MB_OK);
				} else {
					MessageBox (NULL, "Cleanup Failed.", "Drive Restore", MB_OK);
				}

				
			} else {
				MessageBox (NULL, "Restoration Failed.", "Drive Restore", MB_OK);
			}
			*/

		}
	} else {
		m_convertwindow.SetButtonMode (false);
		m_convertwindow.SetButtonEnabled (true);
		m_convertwindow.SetStage (STAGE_BACKUP, STATUS_FAILED);
	}
}

int MainWindow::DriveLetterToNumber (char *szDriveletter)
{
	if (strcmp (szDriveletter, "A") == 0) {return 0;}
	if (strcmp (szDriveletter, "B") == 0) {return 1;}
	if (strcmp (szDriveletter, "C") == 0) {return 2;}
	if (strcmp (szDriveletter, "D") == 0) {return 3;}
	if (strcmp (szDriveletter, "E") == 0) {return 4;}
	if (strcmp (szDriveletter, "F") == 0) {return 5;}
	if (strcmp (szDriveletter, "G") == 0) {return 6;}
	if (strcmp (szDriveletter, "H") == 0) {return 7;}
	if (strcmp (szDriveletter, "I") == 0) {return 8;}
	if (strcmp (szDriveletter, "J") == 0) {return 9;}
	if (strcmp (szDriveletter, "K") == 0) {return 10;}
	if (strcmp (szDriveletter, "L") == 0) {return 11;}
	if (strcmp (szDriveletter, "M") == 0) {return 12;}
	if (strcmp (szDriveletter, "N") == 0) {return 13;}
	if (strcmp (szDriveletter, "O") == 0) {return 14;}
	if (strcmp (szDriveletter, "P") == 0) {return 15;}
	if (strcmp (szDriveletter, "Q") == 0) {return 16;}
	if (strcmp (szDriveletter, "R") == 0) {return 17;}
	if (strcmp (szDriveletter, "S") == 0) {return 18;}
	if (strcmp (szDriveletter, "T") == 0) {return 19;}
	if (strcmp (szDriveletter, "U") == 0) {return 20;}
	if (strcmp (szDriveletter, "V") == 0) {return 21;}
	if (strcmp (szDriveletter, "W") == 0) {return 22;}
	if (strcmp (szDriveletter, "X") == 0) {return 23;}
	if (strcmp (szDriveletter, "Y") == 0) {return 24;}
	if (strcmp (szDriveletter, "Z") == 0) {return 25;}
	
	return -1;
}

void MainWindow::NumberToDriveLetter (char *szOutdrive, int iNumber)
{
	ZeroMemory (szOutdrive, SIZE_NAME);

	if (iNumber == 0) {strcpy_s (szOutdrive, SIZE_NAME, "A");}
	if (iNumber == 1) {strcpy_s (szOutdrive, SIZE_NAME, "B");}
	if (iNumber == 2) {strcpy_s (szOutdrive, SIZE_NAME, "C");}
	if (iNumber == 3) {strcpy_s (szOutdrive, SIZE_NAME, "D");}
	if (iNumber == 4) {strcpy_s (szOutdrive, SIZE_NAME, "E");}
	if (iNumber == 5) {strcpy_s (szOutdrive, SIZE_NAME, "F");}
	if (iNumber == 6) {strcpy_s (szOutdrive, SIZE_NAME, "G");}
	if (iNumber == 7) {strcpy_s (szOutdrive, SIZE_NAME, "H");}
	if (iNumber == 8) {strcpy_s (szOutdrive, SIZE_NAME, "I");}
	if (iNumber == 9) {strcpy_s (szOutdrive, SIZE_NAME, "J");}
	if (iNumber == 10) {strcpy_s (szOutdrive, SIZE_NAME, "K");}
	if (iNumber == 11) {strcpy_s (szOutdrive, SIZE_NAME, "L");}
	if (iNumber == 12) {strcpy_s (szOutdrive, SIZE_NAME, "M");}
	if (iNumber == 13) {strcpy_s (szOutdrive, SIZE_NAME, "N");}
	if (iNumber == 14) {strcpy_s (szOutdrive, SIZE_NAME, "O");}
	if (iNumber == 15) {strcpy_s (szOutdrive, SIZE_NAME, "P");}
	if (iNumber == 16) {strcpy_s (szOutdrive, SIZE_NAME, "Q");}
	if (iNumber == 17) {strcpy_s (szOutdrive, SIZE_NAME, "R");}
	if (iNumber == 18) {strcpy_s (szOutdrive, SIZE_NAME, "S");}
	if (iNumber == 19) {strcpy_s (szOutdrive, SIZE_NAME, "T");}
	if (iNumber == 20) {strcpy_s (szOutdrive, SIZE_NAME, "U");}
	if (iNumber == 21) {strcpy_s (szOutdrive, SIZE_NAME, "V");}
	if (iNumber == 22) {strcpy_s (szOutdrive, SIZE_NAME, "W");}
	if (iNumber == 23) {strcpy_s (szOutdrive, SIZE_NAME, "X");}
	if (iNumber == 24) {strcpy_s (szOutdrive, SIZE_NAME, "Y");}
	if (iNumber == 25) {strcpy_s (szOutdrive, SIZE_NAME, "Z");}
}

bool MainWindow::GetFreeDriveLetter (char *szOutdriveletter)
{
	char szDrives[SIZE_STRING];
	ZeroMemory (szDrives, SIZE_STRING);

	char szCurDrive[SIZE_STRING];
	char szLetter[SIZE_NAME];
	char szAvaildrive[SIZE_NAME];

	int a = 0;
	int l = 0;
	int len = 0;

	bool bAvaildrives[26];

	for (a=0;a<26;a++) {
		bAvaildrives[a] = true;
	}

	len = GetLogicalDriveStrings (SIZE_STRING, szDrives);
	
	for (a=0;a<len;a++) {
		if (szDrives[a] == 0) {

			ZeroMemory (szCurDrive, SIZE_STRING);
			strncpy (szCurDrive, szDrives+l, a-l);			
			l = a+1;

			ZeroMemory (szLetter, SIZE_NAME);
			strncpy_s (szLetter, SIZE_NAME, szCurDrive, 1);

			bAvaildrives[DriveLetterToNumber(szLetter)] = false;
			OutputText ("Drive: ", szCurDrive);
		}
	}

	// Ensure drive A,  B and C are not chosen
	bAvaildrives[0] = false;
	bAvaildrives[1] = false;
	bAvaildrives[2] = false;


	for (a=0;a<26;a++) {

		if (bAvaildrives[a] == true) {
			
			ZeroMemory (szAvaildrive, SIZE_NAME);
			NumberToDriveLetter (szAvaildrive, a);

			OutputText("Available Drive: ", szAvaildrive);

			ZeroMemory (szOutdriveletter, SIZE_NAME);
			strcpy_s (szOutdriveletter, SIZE_NAME, szAvaildrive);

			return true;
		}		
	}

	return false;
}

int MainWindow::MountTemporaryDrive (SingleDriveInfo driveinfo)
{	
	int ires = m_vdmanager.MountVirtualDisk (driveinfo);

	char szMessage[SIZE_STRING];
	ZeroMemory (szMessage, SIZE_STRING);

	if (ires != 0) {
		
		m_vdmanager.GetMountError (szMessage);		
		MessageBox (NULL, szMessage, "Mount Failed", MB_OK | MB_ICONEXCLAMATION);
	}

	return ires;
}

void MainWindow::OnCommand (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	switch (wParam)
	{
		case IDM_CONVERT_DRIVE:
		{
			//MessageBox (NULL, "This will convert a memory stick...", "Test", MB_OK);
			
			/*
			// Code to format a drive.
			if (m_driveformat.QuickFormatDrive ("G") == false) {

				char szReason[SIZE_STRING];
				m_driveformat.GetErrorReason (szReason, SIZE_STRING);

				MessageBox (NULL, szReason, "Format Failed.", MB_OK);

			} else {
				MessageBox (NULL, "Format was successful", "Format", MB_OK);
			}
			*/
			
			//char szDriveletter[SIZE_NAME];
			//ZeroMemory (szDriveletter, SIZE_NAME);
			
			//if (GetFreeDriveLetter (szDriveletter) == true) {
				
			//}
			
			SendMessage (HWND_BROADCAST, WM_DEVICECHANGE, 0, 0);

			m_convertwindow.Initialise (m_hwnd, 0);
			m_convertwindow.SetButtonMode (true);
			m_convertwindow.Reset ();
			m_convertwindow.Show();
			m_convertwindow.SetStagesVisible (false);
			m_convertwindow.SetInputVisible (true);

		}
		break;
		case IDM_HELP_ABOUT:
		{
			// Show the about window
			MessageBox (NULL, "CedeDrive v2.5\n\nVirtual Encrypted Drive Management\n\n(c) 2005-2009 CedeSoft Ltd\n\nAll rights reserved.\n\nWarning: This computer program is protected by copyright law and international treaties. Unauthorised reproduction or distribution of this program, or any portion of it, may result in severe civil and criminal penalties, and will be prosecuted to the maximum extent possible under the law.", "About CedeDrive", MB_OK | MB_ICONINFORMATION);
		}
		break;
		case IDM_FILE_EXIT:
		{
			m_vdmanager.UnmountAllVirtualDrives ();			
			DeleteTrayIcon();
			PostQuitMessage (0);
		}
		break;
		case IDM_TRAY_EXIT:
		{
			m_vdmanager.UnmountAllVirtualDrives ();			
			DeleteTrayIcon();
			PostQuitMessage (0);
		}
		break;
		case IDM_TRAY_SHOW:
		{
			Show();
		}
		break;
		case IDM_TRAY_UNMOUNTALL:
		{
			m_vdmanager.UnmountAllVirtualDrives ();
			MessageBox (NULL, "All encrypted drives unmounted.", "Drives unmounted", MB_OK | MB_ICONINFORMATION);
		}
		break;
		case IDM_TRAY_MOUNTALL:
		{
			MountAllDrives(false);
		}
		break;
		case IDM_ADD_DRIVE:
		{
			m_addwindow.Initialise (hWnd, 0);
			m_addwindow.Show();
		}
		break;
		case IDM_TEST_OPTION:
			{
				if (m_driveformat.ConvertDriveToNTFS ("F") == true) {
					MessageBox (NULL, "Convert succeeded.", "Info", MB_OK);
				} else {
					MessageBox (NULL, "Convert failed.", "Info", MB_OK);
				}
			}
			break;

		case IDM_POPUP_MOUNT:
			{
				int index = LV_GetFirstSelectedIndex(m_hwndlistview);
				
				if (index != -1) {

					SingleDriveInfo tempDrive;
					SingleDriveInfo *pinfo = (SingleDriveInfo *) m_dlDrivelist.GetItem (index);					
					memcpy (&tempDrive, (SingleDriveInfo *) pinfo, sizeof (SingleDriveInfo));

					int ires = m_vdmanager.MountVirtualDisk (tempDrive);

					char szMessage[SIZE_STRING];
					ZeroMemory (szMessage, SIZE_STRING);

					if (ires == 0) {

						sprintf_s (szMessage, "Encrypted Drive '%s' was mounted successfully as drive %s.", pinfo->szName, pinfo->szDriveLetter);
						MessageBox (NULL, szMessage, "Mount Successful", MB_OK | MB_ICONINFORMATION);

					} else {
						m_vdmanager.GetMountError (szMessage);						
						MessageBox (NULL, szMessage, "Mount Failed", MB_OK | MB_ICONEXCLAMATION);
					}

				}
			}
			break;

		case IDM_POPUP_UNMOUNT:
			{
				int index = LV_GetFirstSelectedIndex(m_hwndlistview);
				
				if (index != -1) {

					SingleDriveInfo tempDrive;
					SingleDriveInfo *pinfo = (SingleDriveInfo *) m_dlDrivelist.GetItem (index);					
					memcpy (&tempDrive, (SingleDriveInfo *) pinfo, sizeof (SingleDriveInfo));
				
					m_vdmanager.UnmountVirtualDrive (tempDrive);

					char szMessage[SIZE_STRING];
					ZeroMemory (szMessage, SIZE_STRING);

					sprintf_s (szMessage, "Encrypted Drive '%s' (%s) was unmounted.", pinfo->szName, pinfo->szDriveLetter);
					MessageBox (NULL, szMessage, "Drive unmounted", MB_OK | MB_ICONINFORMATION);
				}
			}
			break;

		case IDM_POPUP_ADDDRIVE:
			{
				m_addwindow.Initialise (hWnd, 0);
				m_addwindow.Show();
			}
			break;

		case IDM_POPUP_DELDRIVE:
			{
				int index = LV_GetFirstSelectedIndex(m_hwndlistview);
				
				if (index != -1) {
					SingleDriveInfo tempDrive;
					SingleDriveInfo *pinfo = (SingleDriveInfo *) m_dlDrivelist.GetItem (index);
					memcpy (&tempDrive, (SingleDriveInfo *) pinfo, sizeof (SingleDriveInfo));

					char szMessage[SIZE_STRING];
					ZeroMemory (szMessage, SIZE_STRING);
					sprintf_s (szMessage, "Are you sure you want to delete the encrypted drive '%s'? If there is any data on this virtual disk, it will be permanently lost.", pinfo->szName);

					if (MessageBox (NULL, szMessage, "Are you sure?", MB_YESNO | MB_ICONQUESTION) == IDYES) {
						// Unmount the drive
						m_vdmanager.UnmountVirtualDrive (tempDrive);
						
						// Delete the drive
						DeleteDrive(pinfo, true);
						RefreshListView();
						SaveConfig();
					}
				}
			}
			break;
	}
}

void MainWindow::OnUICommand (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	int controlID = wParam;

	switch (controlID) {
		case PROGRESS_BACKUP:
			{
				m_convertwindow.SetStageProgress (STAGE_BACKUP, (int) lParam);
			}
			break;
		case PROGRESS_CREATE:
			{
				m_convertwindow.SetStageProgress (STAGE_CREATE, (int) lParam);
			}
			break;
		case PROGRESS_RESTORE:
			{
				m_convertwindow.SetStageProgress (STAGE_RESTORE, (int) lParam);
			}
			break;
		case VD_ALLOCATIONCOMPLETE:
			{
				m_progress.Hide();
				MessageBox (NULL, "Encrypted virtual drive created successfully.", "Encrypted disk created.", MB_OK | MB_ICONINFORMATION);
			}
			break;
		case VD_ALLOCATIONPROGRESS:
			{
				m_progress.SetProgressLabel (m_vdmanager.m_szProgressLabel);
				m_progress.SetProgressValue (m_vdmanager.m_allocationpercent);
			}
			break;
		case CC_QUITNOW:
			{
				m_vdmanager.UnmountAllVirtualDrives ();
				DeleteTrayIcon();
				PostQuitMessage(0);				
			}
			break;
		case CC_UIDRIVEADDED:
			{
				// The Add drive UI has completed
				SingleDriveInfo driveinfo = m_addwindow.GetDriveInfo ();
				m_dlDrivelist.AddItem (&driveinfo, sizeof (SingleDriveInfo), false);
				RefreshListView();
				SaveConfig ();


				// Now create the virtual disk
				SingleDriveInfo diskinfo = m_addwindow.GetDriveInfo ();

				m_progress.SetProgressMax (100);
				m_progress.SetProgressValue (0);
				m_progress.SetProgressLabel ("Creating encrypted virtual drive...");
				
				m_progress.Initialise (hWnd, 0);
				m_progress.Show();

				m_vdmanager.AllocateVirtualDisk (diskinfo);

			}
			break;
		case CID_PASSWORDOK:
			{
				VerifyPassword();
			}
			break;
		case CID_PASSWORDCANCEL:
			{
				OutputText ("Password Request Cancelled.");
				MessageBox (hWnd, "WARNING: If you do not provide a password you will not have access to your encrypted drives.", "Password Warning", MB_OK | MB_ICONEXCLAMATION);
				PostQuitMessage(0);
			}
			break;

		case CID_BEGINCONVERT:
			{
				m_convertdriveinfo = m_convertwindow.GetSelectedDrive ();
				m_convertwindow.GetPassword (m_szConvertpassword);

				OutputText ("Selected Drive: ", m_convertdriveinfo.szDriveLetter);

				StartDriveConverter ();

				/*
				char szDriveletter[SIZE_NAME];
				ZeroMemory (szDriveletter, SIZE_NAME);
			
				if (GetFreeDriveLetter (szDriveletter) == true) {
					OutputText ("Free Drive: ", szDriveletter);
				}
				*/
			}
			break;
		case CID_CONVERTMOUNTREQUEST:
			{
				if (MountTemporaryDrive (m_converttempdrive) == 0) {
					ResumeThread (m_hConvertthread);
				}
			}
			break;
	}
}

void MainWindow::OnIPCEvent (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	//MessageBox (NULL, "IPC Event Received By cededrive core!", "Event", MB_OK);
	OutputText ("IPC Event received from launcher. Checking shared memory.....");

	if (m_memshared.ReadSharedMemory ("CedeDriveLauncher", 1024000) == true) {
		
		m_bstandalonemode = true;

		OutputText ("CedeDriveLauncher Memory read ok. Standalone mode activated.");

		m_dlnewdrives.Clear ();
		m_dlnewdrives.FromMemoryBuffer (&m_memshared); // The shared memory we're expecting will be a dynlist

		// The launcher is now not needed, so tell it to quit
		BroadcastLauncherQuit ();

		m_passwindow.Initialise (hWnd, 0);
		m_passwindow.SetEncryptMode (false);
		m_passwindow.Show ();

	} else {
		OutputText ("CedeDriveLauncher shared memory not found. Running in normal mode.");
	}
}

LRESULT MainWindow::OnClose (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	Hide();
	return 0;
}

void MainWindow::OnDestroy (HWND hWnd)
{
	//MessageBox (NULL, "Being destroyed!", "Test", MB_OK);
	m_vdmanager.UnmountAllVirtualDrives ();
	DeleteTrayIcon();

	PostQuitMessage (0);
}

void MainWindow::OnTimer (WPARAM wParam)
{
	m_uihandler.NotifyTimer (wParam);
}

void MainWindow::OnPaint (HWND hWnd)
{
	m_uihandler.PaintControls (hWnd);	
}

void MainWindow::OnMouseMove (HWND hWnd, int mouseXPos, int mouseYPos)
{		
	m_uihandler.NotifyMouseMove (mouseXPos, mouseYPos);
}

void MainWindow::OnLButtonDown (HWND hWnd)
{
	m_uihandler.NotifyMouseDown ();
}

void MainWindow::OnLButtonDblClick (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	m_pdiag->OutputText ("Window Double Clicked.");
}

void MainWindow::OnLButtonUp (HWND hWnd)
{
	m_uihandler.NotifyMouseUp ();
}

void MainWindow::OutputInt (LPCSTR lpszText, int iValue)
{
	m_pdiag->OutputInt (lpszText, iValue);
}

void MainWindow::OutputText (LPCSTR lpszText)
{
	m_pdiag->OutputText (lpszText);
}

void MainWindow::OutputText (LPCSTR lpszName, LPCSTR lpszValue)
{
	m_pdiag->OutputText (lpszName, lpszValue);
}
