// application icons
#define IDI_ICON 			1
#define IDI_ICONSMALL		2

// Function Specific Identifiers
#define	FS_CENTER			100
#define FS_BOTTOMRIGHT		101
#define FS_STYLESTANDARD	102
#define FS_STYLEBLANK		103
#define FS_TOPLEFT			104

#define STAGE_BACKUP		200
#define STAGE_FORMAT		201
#define STAGE_CREATE		202
#define STAGE_RESTORE		203
#define STAGE_COMPLETION	204

#define STATUS_INPROGRESS	205
#define STATUS_SUCCESS		206
#define STATUS_FAILED		207
#define STATUS_NOTSTARTED	208

#define PROGRESS_BACKUP	1000
#define PROGRESS_CREATE		1001
#define PROGRESS_RESTORE	1002

// Custom control message identifiers
#define WM_UICOMMAND			4000
#define WM_UIHIGHLIGHT			4001
#define WM_UINOHIGHLIGHT		4002
#define WM_MESSAGINGEVENT		4005
#define WM_UISCROLL				4006
#define CC_QUITNOW				4007
#define CC_UIDRIVEADDED		4008
#define VD_ALLOCATIONCOMPLETE	4009
#define VD_ALLOCATIONPROGRESS	4010
#define WM_SYSTRAY				4011
#define CM_IPCEVENT				4012

// Bitmap identifiers
#define IDB_STDBTNBASE		200
#define IDB_STDBTNHIGH		201
#define IDB_STDBTNDOWN		202
#define IDB_DIAGHEADER		243
#define IDB_VSCROLLBASE		296
#define IDB_ADDSPLASH		297
#define IDB_MAINSPLASH		298
#define ID_TRAYICON			299
#define IDC_SYSTRAYICON	300
#define IDB_CONVERTSPLASH	301
#define IDB_TICKICON		302
#define IDB_CROSSICON		303
#define IDB_PASSBG			304
#define IDB_LAUNCHERAPP		305

//Custom Control identifiers
#define CID_TEST			400
#define CID_HEADER			413
#define CID_MULTISCROLL		414
#define ID_LISTVIEW		415
#define CID_CONVERTSPLASH	416
#define CID_STATIC			417
#define CID_CONVERTHEADERLABEL		418
#define CID_CONVERTSUBHEADERLABEL	419
#define CID_CONVERTBACKUPLABEL		420
#define CID_CONVERTFORMATLABEL		421
#define CID_CONVERTCREATELABEL		422
#define CID_CONVERTRESTORELABEL		423
#define CID_CONVERTCOMPLETELABEL	424

// Control resources for the add drive window
#define CID_ADDBANNER		416
#define CID_LABEL				417
#define CID_TXTDRIVENAME	418
#define CID_TXTDRIVEDESCRIPTION	419
#define CID_TXTDRIVEPATH	420
#define CID_BTNBROWSEPATH	421
#define CID_TXTDISKSIZE		422
#define CID_TXTDRIVELETTER	423
#define CID_OPTMOUNTSTARTUP	424
#define CID_BTNADDOK			425
#define CID_BTNADDCANCEL		426
#define CID_BEGINCONVERT		427
#define CID_CONVERTMOUNTREQUEST	428
#define CID_PASSHEADERLABEL		429
#define CID_PASSEDITLABEL1		430
#define CID_PASSEDITLABEL2		431
#define CID_PASSBOTTOMHEADER		432

// Control resources for the password window
#define ID_LBLSTATIC				428
#define ID_EDITPASSWORD		429
#define ID_LBLSTATIC2				430
#define ID_EDITPASSWORD2		431
#define ID_LBLSTATIC3				432
#define ID_BTNPASSOK				433
#define ID_BTNPASSCANCEL		434
#define CID_PASSWORDOK       435
#define CID_PASSWORDCANCEL    436
#define ID_DRIVELIST		437
#define ID_CONVPASSWORD1	438
#define ID_CONVPASSWORD2	439
#define ID_CONVENCRYPTBUTTON	440


// Control resources for the progress window
#define ID_ENCPROGRESS		437
#define ID_LBLOPERATION		438

// Control resources for the info panel
#define ID_INFODRIVENAME	439
#define ID_INFODISKSIZE		440
#define ID_INFODRIVELETTER	441
#define ID_INFODESCRIPTION	442
#define ID_INFOPATH					443

// Windows control identifiers
#define ID_DIAGLIST			701
#define IDM_FILE_EXIT		703
#define IDM_HELP_ABOUT		704
#define IDM_TOOLS_OPTIONS	705
#define IDM_ADD_DRIVE		706
#define IDM_TEST_OPTION		707
#define IDM_POPUP_ADDDRIVE	708
#define IDM_POPUP_DELDRIVE	709
#define IDM_POPUP_MOUNT		710
#define IDM_POPUP_UNMOUNT	711
#define IDM_TRAY_EXIT				712
#define IDM_TRAY_SHOW			713
#define IDM_TRAY_MOUNTALL	714
#define IDM_TRAY_UNMOUNTALL	715
#define IDM_CONVERT_DRIVE	716

// Custom application identifiers
// and messages
#define CRYPT_MSG				903
#define CRYPT_PROGRESS1			904
#define CRYPT_PROGRESS2			905
#define CRYPT_PROGRESSSINGLE	906
#define CRYPT_COMPLETE			907

// Internal buffer sizes
#define SIZE_NAME			64
#define SIZE_STRING			1024
#define SIZE_LARGESTRING	10000
#define SIZE_INTEGER		32

// Application timer messages

// Network control identifiers

