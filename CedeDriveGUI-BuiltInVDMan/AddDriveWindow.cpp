// This work is dedicated to the Lord our God. King of Heaven, Lord of Heaven's Armies.

#include "AddDriveWindow.h"

AddDriveWindow::AddDriveWindow ()
{
	m_bUseDiagnostics = true;
	ZeroMemory (m_szClassname, SIZE_STRING);
	m_ID = 0;
	m_bMountstartup = false;
}

AddDriveWindow::~AddDriveWindow ()
{

}

void AddDriveWindow::SetDiagnostics (Diagnostics *pdiag)
{
	m_pdiag = pdiag;
}

void AddDriveWindow::Initialise (HWND hWnd, unsigned int uID)
{	
	// Make the ID global
	m_ID = uID;
	m_parenthwnd = hWnd;
	//m_hwnd = hWnd;

	// Temporary string value for uID
	char szID[SIZE_STRING];
	ZeroMemory (szID, SIZE_STRING);

	SetParentHWND (hWnd);
	SetBgColor (GetSysColor(COLOR_BTNFACE));
	SetCaption (TEXT ("Add Encrypted Drive"));
	SetWindowStyle (FS_STYLESTANDARD);

	// Create the class name
	strcat_s (m_szClassname, SIZE_STRING, "ADDDRIVEWindowClass");
	sprintf_s (szID, SIZE_STRING, "%d", uID);
	strcat_s (m_szClassname, SIZE_STRING, szID);

	CreateAppWindow (m_szClassname, 70, 0, 658, 434, true);
	m_uihandler.SetWindowProperties (0, 0, 300, 0, RGB (230, 230, 240));
	SetWindowPosition (FS_CENTER);
	Show ();
}

void AddDriveWindow::OnDestroy (HWND hWnd)
{			
	Hide ();
}

void AddDriveWindow::OnCreate (HWND hWnd)
{				
	// Make this window handle global
	m_hwnd = hWnd;
	g_hWnd = hWnd; // Need to do this otherwise the control creation wrapper functions don't work.

	/*
	#define CID_ADDBANNER		416
	#define CID_LABEL				417
	#define CID_TXTDRIVENAME	418
	#define CID_TXTDRIVEDESCRIPTION	419
	#define CID_TXTDRIVEPATH	420
	#define CID_BTNBROWSEPATH	421
	#define CID_TXTDISKSIZE		422
	#define CID_TXTDRIVELETTER	423
	#define CID_OPTMOUNTSTARTUP	424
	#define CID_BTNADDOK			425
	#define CID_BTNADDCANCEL		426


	HWND m_hwndlabel;
	HWND m_hwnddrivename;
	HWND m_hwnddrivedescription;
	HWND m_hwnddrivepath;
	HWND m_hwndbrowsepath;
	HWND m_hwnddisksize;
	HWND m_hwnddriveletter;
	HWND m_hwndmountstartup;
	HWND m_hwndaddok;
	HWND m_hwndaddcancel;

	*/

	// Labels
	m_hwndlabel = CreateLabel("Encrypted Drive Name", 233, 26, 114, 13, CID_LABEL);
	m_hwndlabel = CreateLabel("Encrypted Drive Description", 233, 78, 144, 13, CID_LABEL);
	m_hwndlabel = CreateLabel("Encrypted Drive File", 233, 237, 114, 13, CID_LABEL);
	m_hwndlabel = CreateLabel("Disk Size", 233, 285, 114, 13, CID_LABEL);
	m_hwndlabel = CreateLabel("Drive Letter", 379, 285, 114, 13, CID_LABEL);
	m_hwndlabel = CreateLabel("MB", 332, 304, 23, 13, CID_LABEL);

	// Text Boxes
	m_hwnddrivename = CreateTextBox(235, 46, 374, 20, CID_TXTDRIVENAME);
	m_hwnddrivedescription = CreateTextBox(235, 96, 374, 89, CID_TXTDRIVEDESCRIPTION);
	m_hwnddrivepath = CreateTextBox(235, 253, 299, 20, CID_TXTDRIVEPATH);
	m_hwnddisksize = CreateTextBox(235, 301, 90, 20, CID_TXTDISKSIZE);
	m_hwnddriveletter = CreateTextBox(382, 301, 90, 20, CID_TXTDRIVELETTER);

	// Buttons
	m_hwndaddok = CreateButton ("Ok", 442, 362, 91, 27, CID_BTNADDOK);
	m_hwndaddcancel = CreateButton ("Cancel", 539, 362, 91, 27, CID_BTNADDCANCEL);
	m_hwndbrowsepath = CreateButton ("Browse", 539, 253, 70, 20, CID_BTNBROWSEPATH);

	// Checkboxes
	m_hwndmountstartup = CreateCheckBox("Mount on startup", 503, 304, 106, 17, CID_OPTMOUNTSTARTUP);

	// Header bitmap
	m_header.SetBitmapResources (IDB_ADDSPLASH);
	m_header.SetBitmapProperties (0, 0, 202, 409);
	m_header.SetProperties (hWnd, CID_ADDBANNER, 0, 0, 202, 409);
	m_uihandler.AddDirectControl (&m_header);
}

bool AddDriveWindow::ValidateTextField (int ControlID, char *szControlname)
{
	char szText[SIZE_STRING];
	ZeroMemory (szText, SIZE_STRING);

	GetDlgItemText (m_hwnd, ControlID, szText, SIZE_STRING);

	if (strlen(szText) > 1) {
		return true;
	} else {
		char szMessage[SIZE_STRING];
		ZeroMemory (szMessage, SIZE_STRING);
		sprintf_s (szMessage, SIZE_STRING, "Please enter some valid information for %s", szControlname);
		MessageBox (NULL, szMessage, "Input Problem", MB_OK | MB_ICONEXCLAMATION);
		return false;
	}
}

bool AddDriveWindow::ValidateInput ()
{
	if (ValidateTextField(CID_TXTDRIVENAME, "Drive Name") == false) {return false;}
	if (ValidateTextField(CID_TXTDRIVEDESCRIPTION, "Drive Description") == false) {return false;}
	if (ValidateTextField(CID_TXTDRIVEPATH, "Encrypted Drive File") == false) {return false;}
	if (ValidateTextField(CID_TXTDISKSIZE, "Disk Size") == false) {return false;}
	if (ValidateTextField(CID_TXTDRIVELETTER, "Drive Letter") == false) {return false;}

	return true; // If we got here then all of the validation succeeded.
}

bool AddDriveWindow::SaveSingleFile ()
{
	ZeroMemory (m_szOutputfile, SIZE_STRING);

	char szFileonly[SIZE_STRING];
	ZeroMemory (szFileonly, SIZE_STRING);

	char szPathonly[SIZE_STRING];
	ZeroMemory (szPathonly, SIZE_STRING);

	strcpy_s (szFileonly, SIZE_STRING, "EncryptedDisk.evd");	
	strcpy_s (m_szOutputfile, SIZE_STRING, "EncryptedDisk.evd");
	
	OPENFILENAME ofn;
	
	ZeroMemory(&ofn, sizeof(ofn));
	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = m_hwnd;
	ofn.lpstrFile = m_szOutputfile;
	//ofn.lpstrFile[0] = '\0';
	ofn.nMaxFile = SIZE_STRING;

	ofn.lpstrFilter = "Encrypted Virtual Drives (*.evd)\0*.evd\0All Files (*.*)\0*.*\0";
	
	ofn.nFilterIndex = 1;
	ofn.lpstrFileTitle = szFileonly;
	ofn.nMaxFileTitle = SIZE_STRING;
	ofn.lpstrInitialDir = szPathonly;	
	ofn.Flags = OFN_OVERWRITEPROMPT;
	
	if (GetSaveFileName (&ofn) != 0) {
		return true;
	} else {
		return false;
	}

}

void AddDriveWindow::OutputInt (LPCSTR lpszText, int iValue)
{
	if (m_bUseDiagnostics == true) {
		m_pdiag->OutputInt (lpszText, iValue);
	}
}

void AddDriveWindow::OutputText (LPCSTR lpszText)
{
	if (m_bUseDiagnostics == true) {
		m_pdiag->OutputText (lpszText);
	}
}

void AddDriveWindow::OutputText (LPCSTR lpszName, LPCSTR lpszValue)
{
	if (m_bUseDiagnostics == true) {
		m_pdiag->OutputText (lpszName, lpszValue);
	}
}

SingleDriveInfo AddDriveWindow::GetDriveInfo ()
{
	return m_driveinfo;
}

void AddDriveWindow::SetDriveInfo ()
{
	GetDlgItemText (m_hwnd, CID_TXTDRIVENAME, m_driveinfo.szName, SIZE_STRING);
	GetDlgItemText (m_hwnd, CID_TXTDRIVEDESCRIPTION, m_driveinfo.szDescription, SIZE_STRING);
	GetDlgItemText (m_hwnd, CID_TXTDRIVEPATH, m_driveinfo.szPath, SIZE_STRING);
	
	char szDisksize[SIZE_STRING];
	ZeroMemory (szDisksize, SIZE_STRING);
	GetDlgItemText (m_hwnd, CID_TXTDISKSIZE, szDisksize, SIZE_STRING);
	m_driveinfo.lDiskSizeMegs = atol (szDisksize);
	
	GetDlgItemText (m_hwnd, CID_TXTDRIVELETTER, m_driveinfo.szDriveLetter, SIZE_NAME);

	m_driveinfo.bMountStartup = m_bMountstartup;

}

void AddDriveWindow::OnCommand (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	switch (LOWORD (wParam)) {
		case CID_BTNADDOK:
			{
				if (ValidateInput() == true) {
					SetDriveInfo();
					PostMessage (m_parenthwnd, WM_UICOMMAND, CC_UIDRIVEADDED, 0);
					Hide();
				}
			}
			break;
		case CID_BTNADDCANCEL:
			{
				Hide();
			}
			break;
		case CID_BTNBROWSEPATH:
			{
				if (SaveSingleFile() == true) {
					SetDlgItemText(m_hwnd, CID_TXTDRIVEPATH, m_szOutputfile);
				}
			}
			break;
		case CID_OPTMOUNTSTARTUP:
			{
				if (m_bMountstartup == true) {
					m_bMountstartup = false;
					SetCheck(m_hwndmountstartup, false);
				} else {
					m_bMountstartup = true;				
					SetCheck(m_hwndmountstartup, true);
				}
			}
			break;
	}
}

void AddDriveWindow::OnUIScroll (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	
}

void AddDriveWindow::OnUICommand (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	switch (wParam)
	{
		
	}
}

void AddDriveWindow::OnTimer (WPARAM wParam)
{	
	m_uihandler.NotifyTimer (wParam);	
}

void AddDriveWindow::OnPaint (HWND hWnd)
{
	m_uihandler.PaintControls (hWnd);	
}

void AddDriveWindow::OnMouseMove (HWND hWnd, int mouseXPos, int mouseYPos)
{	
	m_uihandler.NotifyMouseMove (mouseXPos, mouseYPos);
}

void AddDriveWindow::OnLButtonDown (HWND hWnd)
{
	m_uihandler.NotifyMouseDown ();
}

void AddDriveWindow::OnLButtonUp (HWND hWnd)
{
	m_uihandler.NotifyMouseUp ();
}