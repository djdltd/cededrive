// This work is dedicated to the Lord our God. King of Heaven, Lord of Heaven's Armies.

#include "ProgressWindow.h"

ProgressWindow::ProgressWindow ()
{
	m_bUseDiagnostics = true;
	ZeroMemory (m_szClassname, SIZE_STRING);
	m_ID = 0;
}

ProgressWindow::~ProgressWindow ()
{

}

void ProgressWindow::SetDiagnostics (Diagnostics *pdiag)
{
	m_pdiag = pdiag;
}

void ProgressWindow::Initialise (HWND hWnd, unsigned int uID)
{	
	// Make the ID global
	m_ID = uID;
	m_parenthwnd = hWnd;
	//m_hwnd = hWnd;

	// Temporary string value for uID
	char szID[SIZE_STRING];
	ZeroMemory (szID, SIZE_STRING);

	SetParentHWND (hWnd);
	SetBgColor (GetSysColor(COLOR_BTNFACE));
	SetCaption (TEXT ("Progress"));
	SetWindowStyle (FS_STYLESTANDARD);

	// Create the class name
	strcat_s (m_szClassname, SIZE_STRING, "PROGRESSWindowClass");
	sprintf_s (szID, SIZE_STRING, "%d", uID);
	strcat_s (m_szClassname, SIZE_STRING, szID);

	CreateAppWindow (m_szClassname, 70, 0, 650, 100, true);
	m_uihandler.SetWindowProperties (0, 0, 300, 0, RGB (230, 230, 240));
	SetWindowPosition (FS_CENTER);
	Show ();
}

void ProgressWindow::OnDestroy (HWND hWnd)
{			
	Hide ();
}

void ProgressWindow::OnCreate (HWND hWnd)
{				
	// Make this window handle global
	m_hwnd = hWnd;
	
	HFONT hfDefault = (HFONT) GetStockObject (DEFAULT_GUI_FONT);

	m_lbloperation = CreateWindow ("static", "Cipher in progress...", WS_CHILD | WS_VISIBLE, 12, 10, 620, 21, hWnd, (HMENU) ID_LBLOPERATION, GetModuleHandle (NULL), NULL);
	SendMessage (m_lbloperation, WM_SETFONT, (WPARAM) hfDefault, MAKELPARAM (FALSE, 0));

	m_hwndprogress  = CreateWindowEx(0L, PROGRESS_CLASS, "", WS_CHILD | WS_VISIBLE, 12, 32, 620, 22, hWnd, (HMENU) ID_ENCPROGRESS, GetModuleHandle (NULL), NULL);

}

void ProgressWindow::SetProgressMax (int iMax)
{
	SendMessage(m_hwndprogress, PBM_SETRANGE, 0L, MAKELONG (0, iMax));
	SendMessage (m_hwndprogress, PBM_SETSTEP, 1, 0L);
}

void ProgressWindow::SetProgressValue (int iValue)
{
	SendMessage (m_hwndprogress, PBM_SETPOS, iValue, 0L);
}

void ProgressWindow::SetProgressLabel (char *szText)
{
	SetDlgItemText(m_hwnd, ID_LBLOPERATION, szText);
}

void ProgressWindow::OutputInt (LPCSTR lpszText, int iValue)
{
	if (m_bUseDiagnostics == true) {
		m_pdiag->OutputInt (lpszText, iValue);
	}
}

void ProgressWindow::OutputText (LPCSTR lpszText)
{
	if (m_bUseDiagnostics == true) {
		m_pdiag->OutputText (lpszText);
	}
}

void ProgressWindow::OutputText (LPCSTR lpszName, LPCSTR lpszValue)
{
	if (m_bUseDiagnostics == true) {
		m_pdiag->OutputText (lpszName, lpszValue);
	}
}

void ProgressWindow::OnCommand (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	switch (LOWORD (wParam)) {

	}
}

void ProgressWindow::OnUIScroll (HWND hWnd, WPARAM wParam, LPARAM lParam)
{

}

void ProgressWindow::OnUICommand (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	switch (wParam)
	{
		
	}
}

void ProgressWindow::OnTimer (WPARAM wParam)
{	
	m_uihandler.NotifyTimer (wParam);	
}

void ProgressWindow::OnPaint (HWND hWnd)
{
	m_uihandler.PaintControls (hWnd);	
}

void ProgressWindow::OnMouseMove (HWND hWnd, int mouseXPos, int mouseYPos)
{	
	m_uihandler.NotifyMouseMove (mouseXPos, mouseYPos);
}

void ProgressWindow::OnLButtonDown (HWND hWnd)
{
	m_uihandler.NotifyMouseDown ();
}

void ProgressWindow::OnLButtonUp (HWND hWnd)
{
	m_uihandler.NotifyMouseUp ();
}