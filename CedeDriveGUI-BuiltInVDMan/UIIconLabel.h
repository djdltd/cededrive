#pragma once
#include "UIControl.h"

class UIIconLabel : public UIControl
{
	public:
		UIIconLabel ();
		~UIIconLabel ();
		void Paint (HDC hdc);
		void SetIconResources (unsigned int wRes1, unsigned int wRes2);
		void SetIconProperties (int Width, int Height);
		void NotifyMouseUp ();
		void SetVisible (bool bVisible);
		void NotifyMouseMove (int iXPos, int iYPos);
		void SetVisibleIcon (int iIcon);
		unsigned long GetSize ();
		void SetColor (COLORREF color);
		void SetFontStyle (int fontstyle);
		void InvalidateIconLabel ();


	private:
		///////////////// METHOD DEFINITIONS ///////////////////

		// Banner bitmap properties
		// The repeating properties. Defines the xStart position and Width of the section of the bitmap
		// which can be repeated if the desired size is above the base size.
		int m_rptxStart;
		int m_rptxWidth;
		bool m_bVisible;
		int m_ivisibleicon;
};
