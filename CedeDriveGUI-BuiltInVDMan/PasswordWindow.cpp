// This work is dedicated to the Lord our God. King of Heaven, Lord of Heaven's Armies.

#include "PasswordWindow.h"

PasswordWindow::PasswordWindow ()
{
	m_bUseDiagnostics = true;
	ZeroMemory (m_szClassname, SIZE_STRING);
	m_ID = 0;
	m_bSessionmode = false;
	m_bInitialised = false;
	m_bEncryptmode = false;
}

PasswordWindow::~PasswordWindow ()
{

}

void PasswordWindow::Initialise (HWND hWnd, unsigned int uID)
{	
	// Make the ID global
	m_ID = uID;
	m_parenthwnd = hWnd;
	//m_hwnd = hWnd;

	// Temporary string value for uID
	char szID[SIZE_STRING];
	ZeroMemory (szID, SIZE_STRING);

	SetParentHWND (hWnd);
	SetBgColor (GetSysColor (COLOR_BTNFACE));
	SetCaption (TEXT ("Encrypted Drives Password"));
	SetWindowStyle (FS_STYLESTANDARD);

	// Create the class name
	strcat_s (m_szClassname, SIZE_STRING, "PASSWindowClass");
	sprintf_s (szID, SIZE_STRING, "%d", uID);
	strcat_s (m_szClassname, SIZE_STRING, szID);

	if (m_bInitialised == false) {
		m_bInitialised = true;
		CreateAppWindow (m_szClassname, 70, 0, 423, 270, true);
		m_uihandler.SetWindowProperties (0, 0, 423, 196, RGB (0, 230, 0));
	}
	
	SetWindowPosition (FS_CENTER);
	SetAlwaysOnTop (true);
	Show ();
}

char *PasswordWindow::GetLastPassword ()
{
	return (char *) m_szPassword;
}

void PasswordWindow::OnDestroy (HWND hWnd)
{			
	Hide ();
}

void PasswordWindow::OnCreate (HWND hWnd)
{				
	// Make this window handle global
	m_hwnd = hWnd;	
	g_hWnd = hWnd;

	m_passbg.SetBitmapResources (IDB_PASSBG);
	m_passbg.SetBitmapProperties (0, 0, 423, 196);
	m_passbg.SetProperties (hWnd, IDB_PASSBG, 0, 0, 423, 196);
	m_uihandler.AddDirectControl (&m_passbg);

	// The password header label
	m_headerlabel.SetTextProperties ("Arial", 130, 0, 0, 0, "Enter Cededrive Password", RGB (51, 100, 191));
	m_headerlabel.SetProperties (m_hwnd, CID_PASSHEADERLABEL, 20, 14, 200, 20);
	m_uihandler.AddDirectControl (&m_headerlabel);

	m_subheader.SetTextProperties ("Arial", 90, 0, 0, 0, "Enter your password to mount your encrypted drives", RGB (0, 0, 0));
	m_subheader.SetProperties (m_hwnd, CID_LABEL, 20, 35, 400, 20);
	m_uihandler.AddDirectControl (&m_subheader);

	m_bottomheader.SetTextProperties ("Arial", 90, 0, 0, 0, "Encrypted drives: All startup", RGB (0, 0, 0));
	m_bottomheader.SetProperties (m_hwnd, CID_PASSBOTTOMHEADER, 105, 148, 400, 20);
	m_uihandler.AddDirectControl (&m_bottomheader);

	HFONT hfDefault = (HFONT) GetStockObject (DEFAULT_GUI_FONT);
	
	m_editpass1.SetTextProperties ("Arial", 90, 0, 0, 0, "Local\\All Encrypted Drives", RGB (100, 100, 100));
	m_editpass1.SetProperties (m_hwnd, CID_PASSEDITLABEL1, 105, 89, 190, 19);
	m_uihandler.AddDirectControl (&m_editpass1);

	m_editpass2.SetTextProperties ("Arial", 90, 2, 0, 0, "Password", RGB (100, 100, 100));
	m_editpass2.SetProperties (m_hwnd, CID_PASSEDITLABEL2, 105, 118, 190, 19);
	m_editpass2.SetTextHoverCursor (true);
	m_uihandler.AddDirectControl (&m_editpass2);

	// Password edit box
	m_hwndeditpass = CreateWindow ("edit", "", WS_CHILD | WS_VISIBLE | ES_PASSWORD | ES_LEFT , 105, 88, 190, 19, hWnd, (HMENU) ID_EDITPASSWORD, GetModuleHandle (NULL), NULL);
	SendMessage (m_hwndeditpass, WM_SETFONT, (WPARAM) hfDefault, MAKELPARAM (FALSE, 0));
	ShowWindow (m_hwndeditpass, SW_HIDE);

	m_hwndeditpass2 = CreateWindow ("edit", "", WS_CHILD | WS_VISIBLE | ES_PASSWORD | ES_LEFT , 105, 117, 190, 19, hWnd, (HMENU) ID_EDITPASSWORD2, GetModuleHandle (NULL), NULL);
	SendMessage (m_hwndeditpass2, WM_SETFONT, (WPARAM) hfDefault, MAKELPARAM (FALSE, 0));
	ShowWindow (m_hwndeditpass2, SW_HIDE);

	// The ok button
	m_hwndbtnpassok = CreateWindow ("button", "Ok", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 257, 204, 73, 26, hWnd, (HMENU) ID_BTNPASSOK, GetModuleHandle (NULL), NULL);
	SendMessage (m_hwndbtnpassok, WM_SETFONT, (WPARAM) hfDefault, MAKELPARAM (FALSE, 0));

	// The cancel button
	m_hwndbtnpasscancel = CreateWindow ("button", "Cancel", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 336, 204, 73, 26, hWnd, (HMENU) ID_BTNPASSCANCEL, GetModuleHandle (NULL), NULL);
	SendMessage (m_hwndbtnpasscancel, WM_SETFONT, (WPARAM) hfDefault, MAKELPARAM (FALSE, 0));

}

void PasswordWindow::SetSecondFocus ()
{
	SetFocus (m_hwndeditpass2);
}

void PasswordWindow::SetDriveName (char *szName)
{
	// This will set the labels specifying the context at which the password
	// dialog applies e.g. All Encrypted drives or Removable Encrypted Drive

	char szName1[SIZE_STRING];
	char szName2[SIZE_STRING];
	ZeroMemory (szName1, SIZE_STRING);
	ZeroMemory (szName2, SIZE_STRING);

	//m_editpass1.SetTextProperties ("Arial", 90, 0, 0, 0, "Local\\All Encrypted Drives", RGB (100, 100, 100));
	//m_bottomheader.SetTextProperties ("Arial", 90, 0, 0, 0, "Encrypted drives: All startup", RGB (0, 0, 0));

	strcpy_s (szName1, SIZE_STRING, "Local\\");
	strcpy_s (szName2, SIZE_STRING, "Encrypted drives: ");

	strcat_s (szName1, SIZE_STRING, szName);
	strcat_s (szName2, SIZE_STRING, szName);

	m_editpass1.SetTextCaption (szName1);
	m_bottomheader.SetTextCaption (szName2);

}

void PasswordWindow::SetEncryptMode (bool bEncrypt)
{
	bool bEnc = bEncrypt;
	//bool bEnc = true;
		
	m_bEncryptmode = bEnc;

	if (bEnc == true) {
				
		m_headerlabel.SetTextCaption ("Create your password");
		m_subheader.SetTextCaption ("Create a password which will be used to encrypt your drives");
		m_editpass1.SetTextCaption ("Enter new password");
		m_editpass1.SetTextHoverCursor (true);
		
		m_editpass2.SetTextCaption ("Confirm new password");
		m_editpass2.SetTextHoverCursor (true);

	} else {
		m_headerlabel.SetTextCaption ("Enter Cededrive Password");
		m_subheader.SetTextCaption ("Enter your password to mount your encrypted drives");
		m_editpass1.SetTextCaption ("Local\\All Encrypted Drives");
		m_editpass1.SetTextHoverCursor (false);

		m_editpass2.SetTextCaption ("Password");
		m_editpass2.SetTextHoverCursor (true);
	}

}

bool PasswordWindow::GetSessionMode ()
{
	return m_bSessionmode;
}

void PasswordWindow::ParseUserInput ()
{
	// Every time a user presses a key on the user input text box
	// on the conversation window, we need to know what the last 2 characters
	// were so we can determine if the enter key was pressed. Which will
	// trigger the process chat input function.
	char szInput[SIZE_STRING];
	ZeroMemory (szInput, SIZE_STRING);
	
	GetDlgItemText (m_hwnd, ID_EDITPASSWORD, szInput, SIZE_STRING);

	BYTE bChar1;
	BYTE bChar2;
	
	memcpy (&bChar1, szInput+strlen(szInput)-2, 1);
	memcpy (&bChar2, szInput+strlen(szInput)-1, 1);
	
	if (bChar1 == 13 && bChar2 == 10) {
		//OutputText ("Enter was pressed.");
		//ProcessChatInput (szInput);

		//SetDlgItemText (m_hwnd, ID_EDITPASSWORD, "");
		PostMessage (m_hwnd, WM_COMMAND, ID_BTNPASSOK, 0);
	}
}

void PasswordWindow::ParseUserInput2 ()
{
	// Every time a user presses a key on the user input text box
	// on the conversation window, we need to know what the last 2 characters
	// were so we can determine if the enter key was pressed. Which will
	// trigger the process chat input function.
	char szInput[SIZE_STRING];
	ZeroMemory (szInput, SIZE_STRING);
	
	GetDlgItemText (m_hwnd, ID_EDITPASSWORD2, szInput, SIZE_STRING);

	BYTE bChar1;
	BYTE bChar2;
	
	memcpy (&bChar1, szInput+strlen(szInput)-2, 1);
	memcpy (&bChar2, szInput+strlen(szInput)-1, 1);
	
	if (bChar1 == 13 && bChar2 == 10) {
		//OutputText ("Enter was pressed.");
		//ProcessChatInput (szInput);

		//SetDlgItemText (m_hwnd, ID_EDITPASSWORD, "");
		PostMessage (m_hwnd, WM_COMMAND, ID_BTNPASSOK, 0);
	}
}

void PasswordWindow::CheckPassword ()
{
	ZeroMemory (m_szPassword, SIZE_NAME);
	GetDlgItemText (m_hwnd, ID_EDITPASSWORD2, m_szPassword, SIZE_NAME);
	SetDlgItemText (m_hwnd, ID_EDITPASSWORD2, "");
	

	char szPassword2[SIZE_NAME];
	ZeroMemory (szPassword2, SIZE_NAME);
	GetDlgItemText (m_hwnd, ID_EDITPASSWORD, szPassword2, SIZE_NAME);
	SetDlgItemText (m_hwnd, ID_EDITPASSWORD, "");

	if (strlen (m_szPassword) < 5 || strlen (m_szPassword) > 40) {
		MessageBox (m_hwnd, "Your password must be greater than 4 characters and less than 40 characters.", "Password Input", MB_OK | MB_ICONEXCLAMATION);
	} else {
		if (m_bEncryptmode == true) {
			
			if (strcmp (m_szPassword, szPassword2) == 0) {
				PostMessage (m_parenthwnd, WM_UICOMMAND, (WPARAM) CID_PASSWORDOK, 0);
				Hide ();				
			} else {
				MessageBox (m_hwnd, "The passwords you have entered do not match. Please retype your password.", "Password Input", MB_OK | MB_ICONEXCLAMATION);			
				SetFocus (m_hwndeditpass);
			}

		} else {
			PostMessage (m_parenthwnd, WM_UICOMMAND, (WPARAM) CID_PASSWORDOK, 0);
			Hide ();				
		}
	}
}

void PasswordWindow::ClearPassword ()
{
	ZeroMemory (m_szPassword, SIZE_NAME);
}

void PasswordWindow::OnCommand (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	
	switch (LOWORD (wParam)) {
		case ID_BTNPASSOK:
		{
			CheckPassword ();
		}
		break;
		case ID_BTNPASSCANCEL:
		{
			ZeroMemory (m_szPassword, SIZE_NAME);
			PostMessage (m_parenthwnd, WM_UICOMMAND, (WPARAM) CID_PASSWORDCANCEL, 0);
			Hide ();
		}
		break;

		case ID_EDITPASSWORD:
		{
			
			switch (HIWORD (wParam)) {
				case EN_CHANGE:
				{
					ParseUserInput ();
				}
				break;
			}
		}
		break;
		
		case ID_EDITPASSWORD2:
		{			
			switch (HIWORD (wParam)) {
				case EN_CHANGE:
				{
					ParseUserInput2 ();
				}
				break;
			}
		}
		break;
		case IDOK:
		{
			MessageBox (NULL, "Enter", "Info", MB_OK);
		}
		break;
	}
}

void PasswordWindow::OnUIScroll (HWND hWnd, WPARAM wParam, LPARAM lParam)
{

}

void PasswordWindow::OnUICommand (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	switch (wParam)
	{
		case CID_PASSHEADERLABEL:
		{
			//MessageBox (NULL, "Label clicked!", "Info", MB_OK);
		}
		break;

		case CID_PASSEDITLABEL1:
		{
			if (m_editpass1.IsTextHoverEnabled () == true) {
				m_editpass1.SetVisible (false);
				ShowWindow (m_hwndeditpass, SW_SHOW);
				SetFocus (m_hwndeditpass);
			}
		}
		break;
		case CID_PASSEDITLABEL2:
		{
			if (m_editpass2.IsTextHoverEnabled () == true) {
				m_editpass2.SetVisible (false);
				ShowWindow (m_hwndeditpass2, SW_SHOW);
				SetFocus (m_hwndeditpass2);				
			}
		}
		break;
	}
}

void PasswordWindow::OnTimer (WPARAM wParam)
{	
	m_uihandler.NotifyTimer (wParam);	
}

void PasswordWindow::OnPaint (HWND hWnd)
{
	// Paint the box to the screen
	//PAINTSTRUCT ps;
	//BITMAP bm;
	//HDC hdc = BeginPaint (hWnd, &ps);

	//HDC hdcMem = CreateCompatibleDC (hdc);
	//HBITMAP hbmOld = (HBITMAP) SelectObject (hdcMem, hbmBanner);

	//GetObject (hbmBanner, sizeof (bm), &bm);
	//
	//BitBlt (hdc, 0, 0, 423, 196, hdcMem, 0, 0, SRCCOPY);

	//SelectObject (hdcMem, hbmOld);
	//DeleteDC (hdcMem);
	//DeleteDC (hdc);
	//DeleteObject (hbmOld);
	//EndPaint (hWnd, &ps);
	//MessageBox (NULL, "Password paint called.", "Paint",MB_OK);
	m_uihandler.PaintControls (hWnd);
}

void PasswordWindow::OnMouseMove (HWND hWnd, int mouseXPos, int mouseYPos)
{	
	m_uihandler.NotifyMouseMove (mouseXPos, mouseYPos);
}

void PasswordWindow::OnLButtonDown (HWND hWnd)
{
	m_uihandler.NotifyMouseDown ();
}

void PasswordWindow::OnLButtonUp (HWND hWnd)
{
	m_uihandler.NotifyMouseUp ();
}