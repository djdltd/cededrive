#pragma once
#include <windows.h>
#include <commctrl.h>
#include "resource.h"
#include "Diagnostics.h"

class MenuHandler {
	
	public:
		MenuHandler ();
		~MenuHandler ();
		void CreateMainMenu (HWND hwndParent);		
		void CreateMainPopupMenu ();
		void SetDiagnostics (Diagnostics *pdiag);		

		HMENU m_hPopupMenu;
		HMENU m_hTrayMenu;
		bool m_bdeployedfromlauncher;

	private:
		// Diagnostics window pointer
		Diagnostics *m_pdiag;
};