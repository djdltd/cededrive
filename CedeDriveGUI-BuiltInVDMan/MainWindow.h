#include <windows.h>
#include <io.h>
#include <direct.h>
#include <commctrl.h>
#include "UIWindow.h"
#include "UIHandler.h"
#include "ControlCreator.h"
#include "UIPicButton.h"
#include "UIBanner.h"
#include "Diagnostics.h"
#include "UILabel.h"
#include "MenuHandler.h"
#include "Stack.h"
#include "DynList.h"
#include "VirtualDriveManager.h"
#include "AddDriveWindow.h"
#include "PasswordWindow.h"
#include "StandardEncryption.h"
#include "ProgressWindow.h"
#include "RegistryHandler.h"
#include "DriveFormatter.h"
#include "DriveBackupRestore.h"
#include "ConvertDriveWindow.h"

class MainWindow : public UIWindow
{
	public:
		MainWindow ();
		~MainWindow ();
		
		void Initialise (HWND hWnd, bool bAlreadyrunning, LPSTR lpCmdline);
		void SetDiagnostics (Diagnostics *pdiag);
	
		void OutputInt (LPCSTR lpszText, int iValue);
		void OutputText (LPCSTR lpszText);
		void OutputText (LPCSTR lpszName, LPCSTR lpszValue);
		void SaveConfig ();
		bool LoadConfig ();
		bool FileExists (char *FileName);
		void DeleteDrive (SingleDriveInfo *pinfo, bool bDeletehostfile);
		void AddTrayIcon ();
		void DeleteTrayIcon ();		

		void SetInfoPanel (SingleDriveInfo driveinfo, bool showinfo);
		void PrepareCedeCryptPath ();
		void PrepareListView ();
		void RefreshListView ();
		void PrepareConfigPath ();
		int LV_GetFirstSelectedIndex (HWND hwndlv);
		void VerifyPassword ();
		void MountAllDrives (bool startuponly);
		unsigned long long GetFreeDiskSpace (char *szDrive);
		void ConvertRemovableDrive ();
		bool GetFreeDriveLetter (char *szOutdriveletter);
		int DriveLetterToNumber (char *szDriveletter);
		void NumberToDriveLetter (char *szOutdrive, int iNumber);
		void StartDriveConverter ();
		bool IsDriveConverted (char *szDriveletter);
		static MainWindow* m_pwnd;
		
		bool IsDriveAccessible(SingleDriveInfo driveinfo);
		static DWORD WINAPI ConvertDriveProc (PVOID pParam);
		int MountTemporaryDrive (SingleDriveInfo driveinfo);
		void GetPathOnly (char *szInpath, char *szOutpath);
		
		//bool CheckStandaloneMode ();
		HANDLE m_hConvertthread;
		void BroadcastLauncherQuit ();
		void ShareConfig ();
		void BroadcastIPCEvent (WPARAM wParam, LPARAM lParam);
	private:
		// Private Member Variables & objects
		
		// The UI Handler required for multiple handling of custom controls.
		UIHandler m_uihandler;

		// The Control Creater required for fast creation of controls
		ControlCreator m_ccontrols;		

		//A test stack
		Stack m_stack;

		// Global hwnd
		HWND m_hwnd;

		// The header bitmap image
		UIBanner m_header;

		// The info panel control handles
		HWND m_infodrivename;
		HWND m_infodisksize;
		HWND m_infodriveletter;
		HWND m_infodescription;
		HWND m_infopath;

		// The main menu class
		MenuHandler m_mainmenu;

		// Pointer to the global diagnostics window
		Diagnostics *m_pdiag;

		// Virtual Drive Manager
		VirtualDriveManager m_vdmanager;
		
		// Add new virtual drive window
		AddDriveWindow m_addwindow;

		// The progress window when allocating new drives
		ProgressWindow m_progress;

		// The tray icon
		NOTIFYICONDATA m_nid;
		HICON m_hSystrayicon;

		// The Password Window
		PasswordWindow m_passwindow;

		// Standard Encryption class
		StandardEncryption m_enc;

		RegistryHandler m_registry;
		char m_szCedeCryptPath[SIZE_STRING];

		// Control handles
		HWND m_hwndlistview;

		// Wrong password count
		int m_wrongpasswordcount;

		// The convert drive window
		ConvertDriveWindow m_convertwindow;

		// The Drive Formatter class for formatting memory sticks
		DriveFormatter m_driveformat;
		// The Drive backup and restore class
		DriveBackupRestore m_drivebackup;
		// The singledrive info structure storing the
		// selected drive
		SingleDriveInfo m_convertdriveinfo;
		// The password used to convert the drive
		// when doing one click conversion
		SingleDriveInfo m_converttempdrive;
		char m_szConvertpassword[SIZE_STRING];

		char m_szConfigpath[SIZE_STRING];
	
		MemoryBuffer m_memEncpassword; // A piece of encrypted data encrypted using the users password.
		
		DynList m_dlnewdrives;
		//SingleDriveInfo m_localmodedrive;
		bool m_bstandalonemode;

		// The list of all the mounted drives
		DynList m_dlDrivelist;
		HIMAGELIST m_hLarge;   // image list for icon view 
		HIMAGELIST m_hSmall;   // image list for other views 
		
		// Shared Memory IPC
		MemoryBuffer m_memshared;
		UINT g_quitmessage; // Global quit message so that the cededrive process can tell the launcher to quit when we're done with it

		// Temporary input output buffers
		//MemoryBuffer m_inBuffer;
		//MemoryBuffer m_outBuffer;
		bool m_bAlreadyrunning;

		// event notification from base class
		void OnCreate (HWND hWnd);
		void OnNotify (HWND hWnd, WPARAM wParam, LPARAM lParam);
		void OnCommand (HWND hWnd, WPARAM wParam, LPARAM lParam);
		void OnUICommand (HWND hWnd, WPARAM wParam, LPARAM lParam);		
		void OnPaint (HWND hWnd);
		void OnTimer (WPARAM wParam);
		void OnMouseMove (HWND hWnd, int mouseXPos, int mouseYPos);
		void OnLButtonDown (HWND hWnd);
		void OnLButtonDblClick (HWND hWnd, WPARAM wParam, LPARAM lParam);				
		void OnCryptEvent (HWND hWnd, WPARAM wParam, LPARAM lParam);
		void OnSysTray (HWND hWnd, WPARAM wParam, LPARAM lParam);
		LRESULT OnClose (HWND hWnd, WPARAM wParam, LPARAM lParam);
		void OnLButtonUp (HWND hWnd);
		void OnDestroy (HWND hWnd);
		void OnIPCEvent (HWND hWnd, WPARAM wParam, LPARAM lParam);

};
