// This work is dedicated to the Lord our God. King of Heaven, Lord of Heaven's Armies.

#include "ConvertDriveWindow.h"

ConvertDriveWindow::ConvertDriveWindow ()
{
	m_bUseDiagnostics = true;
	ZeroMemory (m_szClassname, SIZE_STRING);
	m_ID = 0;
}

ConvertDriveWindow::~ConvertDriveWindow ()
{

}

void ConvertDriveWindow::SetDiagnostics (Diagnostics *pdiag)
{
	m_pdiag = pdiag;
}

void ConvertDriveWindow::Initialise (HWND hWnd, unsigned int uID)
{	
	// Make the ID global
	m_ID = uID;
	m_parenthwnd = hWnd;
	//m_hwnd = hWnd;

	// Temporary string value for uID
	char szID[SIZE_STRING];
	ZeroMemory (szID, SIZE_STRING);

	SetParentHWND (hWnd);
	SetBgColor (GetSysColor (COLOR_BTNFACE));
	SetCaption (TEXT ("Convert Removable Storage"));
	SetWindowStyle (FS_STYLESTANDARD);

	// Create the class name
	strcat_s (m_szClassname, SIZE_STRING, "ConvertWindowClass");
	sprintf_s (szID, SIZE_STRING, "%d", uID);
	strcat_s (m_szClassname, SIZE_STRING, szID);

	CreateAppWindow (m_szClassname, 70, 0, 690, 510, true);
	m_uihandler.SetWindowProperties (0, 0, 300, 0, RGB (230, 230, 240));
	SetWindowPosition (FS_CENTER);
	Show ();
}

void ConvertDriveWindow::OnDestroy (HWND hWnd)
{			
	Hide ();
}

LRESULT ConvertDriveWindow::OnClose (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	Hide();
	return 0;
}

void ConvertDriveWindow::OnCreate (HWND hWnd)
{				
	// Make this window handle global
	m_hwnd = hWnd;
	g_hWnd = hWnd;

	m_header.SetBitmapResources (IDB_CONVERTSPLASH);
	m_header.SetBitmapProperties (0, 0, 215, 426);
	m_header.SetProperties (hWnd, CID_CONVERTSPLASH, 0, 0, 215, 426);
	m_uihandler.AddDirectControl (&m_header);

	
	m_whiterect.SetProperties (m_hwnd, CID_STATIC, 216, 0, 475, 426);
	m_uihandler.AddDirectControl (&m_whiterect);

	m_headerlabel.SetTextProperties ("Arial", 130, 0, 0, 0, "Before we begin", RGB (51, 100, 191));
	m_headerlabel.SetProperties (m_hwnd, CID_CONVERTHEADERLABEL, 250, 22, 350, 20);
	m_uihandler.AddDirectControl (&m_headerlabel); 

	m_subheaderlabel.SetTextProperties ("Arial", 90, 0, 0, 0, "Before encrypting your removable storage, some information is", RGB (0, 0, 0));
	m_subheaderlabel.SetProperties (m_hwnd, CID_CONVERTSUBHEADERLABEL, 250, 52, 400, 20);
	m_uihandler.AddDirectControl (&m_subheaderlabel);

	m_subheaderlabel2.SetTextProperties ("Arial", 90, 0, 0, 0, "required from you...", RGB (0, 0, 0));
	m_subheaderlabel2.SetProperties (m_hwnd, CID_CONVERTSUBHEADERLABEL, 250, 68, 400, 20);
	m_uihandler.AddDirectControl (&m_subheaderlabel2);
	

	m_drivelabel.SetTextProperties ("Arial", 90, 0, 0, 0, "Select the drive to encrypt", RGB (0, 0, 0));
	m_drivelabel.SetProperties (m_hwnd, CID_STATIC, 325, 110, 200, 20);
	m_uihandler.AddDirectControl (&m_drivelabel);

	m_password1label.SetTextProperties ("Arial", 90, 0, 0, 0, "Create a password for this drive", RGB (0, 0, 0));
	m_password1label.SetProperties (m_hwnd, CID_STATIC, 325, 180, 200, 20);
	m_uihandler.AddDirectControl (&m_password1label);

	m_password2label.SetTextProperties ("Arial", 90, 0, 0, 0, "Confirm password", RGB (0, 0, 0));
	m_password2label.SetProperties (m_hwnd, CID_STATIC, 325, 240, 200, 20);
	m_uihandler.AddDirectControl (&m_password2label);

	// Create the combo box that will be used to make status changes.
	HFONT hfDefault = (HFONT) GetStockObject (DEFAULT_GUI_FONT);
	m_hwnddriveselection = CreateWindow ("ComboBox", NULL, WS_CHILD | WS_VISIBLE | WS_BORDER | CBS_DROPDOWN, 325, 130, 210, 19, hWnd, (HMENU) ID_DRIVELIST, GetModuleHandle (NULL), NULL);
	SendMessage (m_hwnddriveselection, WM_SETFONT, (WPARAM) hfDefault, MAKELPARAM (FALSE, 0));

	// Create the 2 password edit boxes
	m_hwndpassword1 = CreateWindow ("edit", "", WS_CHILD | WS_VISIBLE | WS_BORDER | ES_PASSWORD | ES_LEFT , 325, 200, 210, 19, hWnd, (HMENU) ID_CONVPASSWORD1, GetModuleHandle (NULL), NULL);
	SendMessage (m_hwndpassword1, WM_SETFONT, (WPARAM) hfDefault, MAKELPARAM (FALSE, 0));

	m_hwndpassword2 = CreateWindow ("edit", "", WS_CHILD | WS_VISIBLE | WS_BORDER | ES_PASSWORD | ES_LEFT , 325, 260, 210, 19, hWnd, (HMENU) ID_CONVPASSWORD2, GetModuleHandle (NULL), NULL);
	SendMessage (m_hwndpassword2, WM_SETFONT, (WPARAM) hfDefault, MAKELPARAM (FALSE, 0));

	// The labels indicating which part of the process
	// the conversion process is at
	int labely = 140;

	m_lblbackup.SetIconResources (IDB_TICKICON, IDB_CROSSICON);
	m_lblbackup.SetIconProperties (20, 20);
	m_lblbackup.SetTextProperties ("Arial", 90, 1, 0, 0, "Backing up your files", RGB (0, 0, 0));
	m_lblbackup.SetProperties (m_hwnd, CID_CONVERTBACKUPLABEL, 350, labely, 200, 20);
	m_uihandler.AddDirectControl (&m_lblbackup);

	m_lblformat.SetIconResources (IDB_TICKICON, IDB_CROSSICON);
	m_lblformat.SetIconProperties (20, 20);
	m_lblformat.SetTextProperties ("Arial", 90, 1, 0, 0, "Formatting removable storage", RGB (0, 0, 0));
	m_lblformat.SetProperties (m_hwnd, CID_CONVERTFORMATLABEL, 350, labely+30, 200, 20);
	m_uihandler.AddDirectControl (&m_lblformat);

	m_lblcreate.SetIconResources (IDB_TICKICON, IDB_CROSSICON);
	m_lblcreate.SetIconProperties (20, 20);
	m_lblcreate.SetTextProperties ("Arial", 90, 1, 0, 0, "Creating encrypted drive", RGB (0, 0, 0));
	m_lblcreate.SetProperties (m_hwnd, CID_CONVERTCREATELABEL, 350, labely+60, 200, 20);
	m_uihandler.AddDirectControl (&m_lblcreate);

	m_lblrestore.SetIconResources (IDB_TICKICON, IDB_CROSSICON);
	m_lblrestore.SetIconProperties (20, 20);
	m_lblrestore.SetTextProperties ("Arial", 90, 1, 0, 0, "Restoring your files", RGB (0, 0, 0));
	m_lblrestore.SetProperties (m_hwnd, CID_CONVERTRESTORELABEL, 350, labely+90, 200, 20);
	m_uihandler.AddDirectControl (&m_lblrestore);

	m_lblcompleting.SetIconResources (IDB_TICKICON, IDB_CROSSICON);
	m_lblcompleting.SetIconProperties (20, 20);
	m_lblcompleting.SetTextProperties ("Arial", 90, 1, 0, 0, "Completing conversion", RGB (0, 0, 0));
	m_lblcompleting.SetProperties (m_hwnd, CID_CONVERTCOMPLETELABEL, 350, labely+120, 200, 20);
	m_uihandler.AddDirectControl (&m_lblcompleting);

	
	SetStage (STAGE_BACKUP, STATUS_NOTSTARTED);
	SetStage (STAGE_FORMAT, STATUS_NOTSTARTED);
	SetStage (STAGE_CREATE, STATUS_NOTSTARTED);
	SetStage (STAGE_RESTORE, STATUS_NOTSTARTED);
	SetStage (STAGE_COMPLETION, STATUS_NOTSTARTED);

	SetInputVisible (false);
	SetStagesVisible (false);	

	// Get a list of the available drives on this system
	GetAvailableDrives ();
	PopulateAvailDrives ();

	// Create the button to convert the drive
	m_hwndencryptbutton = CreateButton ("Encrypt", 590, 440, 80, 25, ID_CONVENCRYPTBUTTON);

}

void ConvertDriveWindow::InvalidateWindow ()
{
	RECT rect;

	rect.left = 300;
	rect.top = 100;
	rect.bottom = 650;
	rect.right = 650;

	InvalidateRect (m_hwnd, &rect, false);
}

void ConvertDriveWindow::InvalidateHeader ()
{
	//// 200 left, 10 top, 110 bottom, 650 right
	RECT rect;

	rect.left = 200;
	rect.top = 10;
	rect.bottom = 110;
	rect.right = 650;
}

void ConvertDriveWindow::Reset ()
{
	SetStage (STAGE_BACKUP, STATUS_NOTSTARTED);
	SetStage (STAGE_FORMAT, STATUS_NOTSTARTED);
	SetStage (STAGE_CREATE, STATUS_NOTSTARTED);
	SetStage (STAGE_RESTORE, STATUS_NOTSTARTED);
	SetStage (STAGE_COMPLETION, STATUS_NOTSTARTED);

	SetInputVisible (true);
	SetStagesVisible (false);
}

void ConvertDriveWindow::SetButtonMode (bool bEncrypt)
{
	if (bEncrypt == true) {
		SetDlgItemText (m_hwnd, ID_CONVENCRYPTBUTTON, "Encrypt");
		m_bbuttonmode = true;
	} else {
		SetDlgItemText (m_hwnd, ID_CONVENCRYPTBUTTON, "Finish");
		m_bbuttonmode = false;
	}
}

void ConvertDriveWindow::SetButtonEnabled (bool bEnabled)
{
	EnableWindow (m_hwndencryptbutton, bEnabled);
}

void ConvertDriveWindow::SetStageProgress (int Stage, int Percent)
{
	char szLabel[SIZE_STRING];
	ZeroMemory (szLabel, SIZE_STRING);

	switch (Stage)
	{
		case STAGE_BACKUP:
			{
				sprintf_s (szLabel, SIZE_STRING, "Backing up your files (%i %%)", Percent);
				m_lblbackup.SetTextCaption (szLabel);
			}
			break;
		case STAGE_FORMAT:
			{
				sprintf_s (szLabel, SIZE_STRING, "Formatting removable storage (%i %%)", Percent);
				m_lblformat.SetTextCaption (szLabel);
			}
			break;
		case STAGE_CREATE:
			{
				sprintf_s (szLabel, SIZE_STRING, "Creating encrypted drive (%i %%)", Percent);
				m_lblcreate.SetTextCaption (szLabel);
			}
			break;
		case STAGE_RESTORE:
			{
				sprintf_s (szLabel, SIZE_STRING, "Restoring your files (%i %%)", Percent);
				m_lblrestore.SetTextCaption (szLabel);
			}
			break;
		case STAGE_COMPLETION:
			{
				sprintf_s (szLabel, SIZE_STRING, "Completing conversion (%i %%)", Percent);
				m_lblcompleting.SetTextCaption (szLabel);
			}
			break;
	}

	InvalidateWindow ();
	InvalidateHeader ();
}

SingleDriveInfo ConvertDriveWindow::GetSelectedDrive ()
{
	SingleDriveInfo driveinfo;
	SingleDriveInfo *pinfo;

	// Get the index of the selected item
	int iIndex = SendMessage (m_hwnddriveselection, CB_GETCURSEL, 0, 0);

	pinfo = (SingleDriveInfo *) m_dlAvaildrives.GetItem (iIndex);

	memcpy (&driveinfo, pinfo, sizeof (SingleDriveInfo));

	return driveinfo;
}

void ConvertDriveWindow::GetPassword (char *szOutpassword)
{
	char szPassword[SIZE_STRING];
	ZeroMemory (szPassword, SIZE_STRING);

	GetDlgItemText (m_hwnd, ID_CONVPASSWORD1, szPassword, SIZE_STRING);

	ZeroMemory (szOutpassword, SIZE_STRING);
	strcpy_s (szOutpassword, SIZE_STRING, szPassword);
}

bool ConvertDriveWindow::IsPasswordOk ()
{
	char szPass1[SIZE_STRING];
	char szPass2[SIZE_STRING];
	ZeroMemory (szPass1, SIZE_STRING);
	ZeroMemory (szPass2, SIZE_STRING);

	GetDlgItemText (m_hwnd, ID_CONVPASSWORD1, szPass1, SIZE_STRING);
	GetDlgItemText (m_hwnd, ID_CONVPASSWORD2, szPass2, SIZE_STRING);

	if (strlen (szPass1) < 5) {
		MessageBox (NULL, "Please enter a password of 5 characters or more.", "Password too short", MB_OK | MB_ICONINFORMATION);
		return false;
	}

	if (strcmp (szPass1, szPass2) == 0) {
		return true;
	} else {
		MessageBox (NULL, "The passwords you have entered do not match. Please try again.", "Password Input", MB_OK | MB_ICONEXCLAMATION);
		return false;
	}

	
}

void ConvertDriveWindow::GetAvailableDrives ()
{
	// Function to scan the computer for all removable drives
	// for presenting to the user when deciding which drive to convert
	// to an encrypted disk.

	m_dlAvaildrives.Clear ();
	SingleDriveInfo driveinfo;
	char szVolumename[SIZE_STRING];
	char szDrives[SIZE_STRING];
	ZeroMemory (szDrives, SIZE_STRING);

	char szCurDrive[SIZE_NAME];
	
	int a = 0;
	int l = 0;
	int len = 0;
	int count = 0;	

	len = GetLogicalDriveStrings (SIZE_STRING, szDrives);
	DWORD dwSizevolname = SIZE_STRING;

	for (a=0;a<len;a++) {
		if (szDrives[a] == 0) {
			ZeroMemory (szCurDrive, SIZE_NAME);
			strncpy (szCurDrive, szDrives+l, a-l);
						
			if (strcmp (szCurDrive, "A:\\") != 0 && strcmp (szCurDrive, "B:\\") != 0) {				
				if (GetDriveType (szCurDrive) == DRIVE_REMOVABLE) {
					
					ZeroMemory (driveinfo.szDriveLetter, SIZE_NAME);
					ZeroMemory (driveinfo.szVolumename, SIZE_STRING);

					m_pdiag->OutputText (szCurDrive);
						
					ZeroMemory (szVolumename, SIZE_STRING);	
					GetVolumeInformation (szCurDrive, szVolumename, dwSizevolname, NULL, NULL, NULL, NULL, NULL);

					strcpy_s (driveinfo.szDriveLetter, SIZE_NAME, szCurDrive);
					strcpy_s (driveinfo.szVolumename, SIZE_STRING, szVolumename);

					m_dlAvaildrives.AddItem (&driveinfo, sizeof (SingleDriveInfo), false);

					ZeroMemory (driveinfo.szDriveLetter, SIZE_NAME);
					ZeroMemory (driveinfo.szVolumename, SIZE_STRING);

					count++;
				}				
			}
			l = a+1;
		}
	}	
}

void ConvertDriveWindow::PopulateAvailDrives ()
{
	SingleDriveInfo *pinfo;
	
	char szDrivename[SIZE_STRING];
	
	SendMessage (m_hwnddriveselection,CB_RESETCONTENT, 0, 0);

	for (int a=0;a<m_dlAvaildrives.GetNumItems ();a++) {
		pinfo = (SingleDriveInfo *) m_dlAvaildrives.GetItem (a);

		ZeroMemory (szDrivename, SIZE_STRING);
		strcpy_s (szDrivename, SIZE_STRING, pinfo->szDriveLetter);
		
		if (strlen (pinfo->szVolumename) > 0) {
			strcat_s (szDrivename, SIZE_STRING, " (");
			strcat_s (szDrivename, SIZE_STRING, pinfo->szVolumename);
			strcat_s (szDrivename, SIZE_STRING, ")");
		}
		
		SendMessage (m_hwnddriveselection, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) szDrivename);
	}

	if (m_dlAvaildrives.GetNumItems () == 0) {
		SendMessage (m_hwnddriveselection, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "No removable drives found.");
	} 
	
	
	SendMessage (m_hwnddriveselection, CB_SETCURSEL, 0, 0); // Set the current selection to the first item in the list

}

void ConvertDriveWindow::SetStagesComplete (bool bSuccess)
{
	if (bSuccess == true) {
		m_headerlabel.SetTextCaption ("Converted successfully");
		m_subheaderlabel.SetTextCaption ("Your removable storage was converted successfully to an ");
		m_subheaderlabel2.SetTextCaption ("encrypted storage device for use on any Windows PC.");
	} else {
		m_headerlabel.SetTextCaption ("There was a problem");
		m_subheaderlabel.SetTextCaption ("There was a problem converting your removable drive");
		m_subheaderlabel2.SetTextCaption ("to an encrypted storage device.");
	}

	InvalidateHeader ();
}

void ConvertDriveWindow::SetStagesVisible (bool visible)
{
	m_lblbackup.SetVisible (visible);
	m_lblformat.SetVisible (visible);
	m_lblcreate.SetVisible (visible);
	m_lblrestore.SetVisible (visible);
	m_lblcompleting.SetVisible (visible);

	if (visible == true) {
		m_headerlabel.SetTextCaption ("Encrypting your removable storage...");
		m_subheaderlabel.SetTextCaption ("That's all the information we need from you. Your removable storage");
		m_subheaderlabel2.SetTextCaption ("is being converted into an encrypted device.");
	}

	InvalidateHeader ();
}



void ConvertDriveWindow::SetInputVisible (bool visible)
{
	m_drivelabel.SetVisible (visible);
	m_password1label.SetVisible (visible);
	m_password2label.SetVisible (visible);
	
	int ivis;
	if (visible == true) {
		ivis = SW_SHOW;
	} else {
		ivis = SW_HIDE;
	}

	ShowWindow (m_hwnddriveselection, ivis);
	ShowWindow (m_hwndpassword1, ivis);
	ShowWindow (m_hwndpassword2, ivis);
	
	if (visible == true) {
		m_headerlabel.SetTextCaption ("Before we begin");
		m_subheaderlabel.SetTextCaption ("Before encrypting your removable storage, some information is");
		m_subheaderlabel2.SetTextCaption ("required from you...");
	}

	InvalidateHeader ();
}

void ConvertDriveWindow::SetStage (int iStage, int iStatus)
{
	if (iStatus == STATUS_INPROGRESS)
	{
		if (iStage == STAGE_BACKUP) {
			m_lblbackup.SetColor (RGB (0, 0, 0));
			m_lblbackup.SetFontStyle (1);
			m_lblbackup.SetVisibleIcon (0);
		}

		if (iStage == STAGE_FORMAT) {		
			m_lblformat.SetColor (RGB (0, 0, 0));
			m_lblformat.SetFontStyle (1);
			m_lblformat.SetVisibleIcon (0);
		}

		if (iStage == STAGE_CREATE) {	
			m_lblcreate.SetColor (RGB (0, 0, 0));
			m_lblcreate.SetFontStyle (1);
			m_lblcreate.SetVisibleIcon (0);
		}

		if (iStage == STAGE_RESTORE) {
			m_lblrestore.SetColor (RGB (0, 0, 0));
			m_lblrestore.SetFontStyle (1);
			m_lblrestore.SetVisibleIcon (0);
		}

		if (iStage == STAGE_COMPLETION) {
			m_lblcompleting.SetColor (RGB (0, 0, 0));
			m_lblcompleting.SetFontStyle (1);
			m_lblcompleting.SetVisibleIcon (0);
		}
	}

	if (iStatus == STATUS_SUCCESS)
	{
		if (iStage == STAGE_BACKUP) {
			m_lblbackup.SetColor (RGB (100, 100, 100));
			m_lblbackup.SetFontStyle (0);
			m_lblbackup.SetVisibleIcon (1);
		}

		if (iStage == STAGE_FORMAT) {		
			m_lblformat.SetColor (RGB (100, 100, 100));
			m_lblformat.SetFontStyle (0);
			m_lblformat.SetVisibleIcon (1);
		}

		if (iStage == STAGE_CREATE) {	
			m_lblcreate.SetColor (RGB (100, 100, 100));
			m_lblcreate.SetFontStyle (0);
			m_lblcreate.SetVisibleIcon (1);
		}

		if (iStage == STAGE_RESTORE) {
			m_lblrestore.SetColor (RGB (100, 100, 100));
			m_lblrestore.SetFontStyle (0);
			m_lblrestore.SetVisibleIcon (1);
		}

		if (iStage == STAGE_COMPLETION) {
			m_lblcompleting.SetColor (RGB (100, 100, 100));
			m_lblcompleting.SetFontStyle (0);
			m_lblcompleting.SetVisibleIcon (1);
		}
	}

	if (iStatus == STATUS_FAILED)
	{
		if (iStage == STAGE_BACKUP) {
			m_lblbackup.SetColor (RGB (100, 100, 100));
			m_lblbackup.SetFontStyle (0);
			m_lblbackup.SetVisibleIcon (2);
		}

		if (iStage == STAGE_FORMAT) {		
			m_lblformat.SetColor (RGB (100, 100, 100));
			m_lblformat.SetFontStyle (0);
			m_lblformat.SetVisibleIcon (2);
		}

		if (iStage == STAGE_CREATE) {	
			m_lblcreate.SetColor (RGB (100, 100, 100));
			m_lblcreate.SetFontStyle (0);
			m_lblcreate.SetVisibleIcon (2);
		}

		if (iStage == STAGE_RESTORE) {
			m_lblrestore.SetColor (RGB (100, 100, 100));
			m_lblrestore.SetFontStyle (0);
			m_lblrestore.SetVisibleIcon (2);
		}

		if (iStage == STAGE_COMPLETION) {
			m_lblcompleting.SetColor (RGB (100, 100, 100));
			m_lblcompleting.SetFontStyle (0);
			m_lblcompleting.SetVisibleIcon (2);
		}
	}

	if (iStatus == STATUS_NOTSTARTED)
	{
		if (iStage == STAGE_BACKUP) {
			m_lblbackup.SetColor (RGB (100, 100, 100));
			m_lblbackup.SetFontStyle (0);
			m_lblbackup.SetVisibleIcon (0);
		}

		if (iStage == STAGE_FORMAT) {		
			m_lblformat.SetColor (RGB (100, 100, 100));
			m_lblformat.SetFontStyle (0);
			m_lblformat.SetVisibleIcon (0);
		}

		if (iStage == STAGE_CREATE) {	
			m_lblcreate.SetColor (RGB (100, 100, 100));
			m_lblcreate.SetFontStyle (0);
			m_lblcreate.SetVisibleIcon (0);
		}

		if (iStage == STAGE_RESTORE) {
			m_lblrestore.SetColor (RGB (100, 100, 100));
			m_lblrestore.SetFontStyle (0);
			m_lblrestore.SetVisibleIcon (0);
		}

		if (iStage == STAGE_COMPLETION) {
			m_lblcompleting.SetColor (RGB (100, 100, 100));
			m_lblcompleting.SetFontStyle (0);
			m_lblcompleting.SetVisibleIcon (0);
		}
	}

	InvalidateWindow ();
	InvalidateHeader ();
}

void ConvertDriveWindow::OutputInt (LPCSTR lpszText, int iValue)
{
	if (m_bUseDiagnostics == true) {
		m_pdiag->OutputInt (lpszText, iValue);
	}
}

void ConvertDriveWindow::OutputText (LPCSTR lpszText)
{
	if (m_bUseDiagnostics == true) {
		m_pdiag->OutputText (lpszText);
	}
}

void ConvertDriveWindow::OutputText (LPCSTR lpszName, LPCSTR lpszValue)
{
	if (m_bUseDiagnostics == true) {
		m_pdiag->OutputText (lpszName, lpszValue);
	}
}

void ConvertDriveWindow::OnCommand (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	switch (LOWORD (wParam)) {
		case ID_CONVENCRYPTBUTTON:
		{
			if (m_bbuttonmode == true) {
				if (IsPasswordOk () == true) {
					SendMessage (g_ParentHWND, WM_UICOMMAND, CID_BEGINCONVERT, 0);	
				}
			} else {
				Hide ();
			}
		}
		break;
	}
}

void ConvertDriveWindow::OnUIScroll (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	
}

void ConvertDriveWindow::OnUICommand (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	switch (wParam)
	{
		
	}
}

void ConvertDriveWindow::OnTimer (WPARAM wParam)
{	
	m_uihandler.NotifyTimer (wParam);	
}

void ConvertDriveWindow::OnPaint (HWND hWnd)
{
	m_uihandler.PaintControls (hWnd);	
}

void ConvertDriveWindow::OnMouseMove (HWND hWnd, int mouseXPos, int mouseYPos)
{	
	m_uihandler.NotifyMouseMove (mouseXPos, mouseYPos);
}

void ConvertDriveWindow::OnLButtonDown (HWND hWnd)
{
	m_uihandler.NotifyMouseDown ();
}

void ConvertDriveWindow::OnLButtonUp (HWND hWnd)
{
	m_uihandler.NotifyMouseUp ();
}