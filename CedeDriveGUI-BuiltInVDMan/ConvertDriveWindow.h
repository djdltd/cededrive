#pragma once
#include <windows.h>
#include <io.h>
#include <commctrl.h>
#include "UIWindow.h"
#include "Diagnostics.h"
#include "UIHandler.h"
#include "UIBanner.h"
#include "MultiContent.h"
#include "UIRect.h"
#include "UIPicButton.h"
#include "UIIconLabel.h"
#include "DynList.h"
#include "SingleDriveInfo.h"


class ConvertDriveWindow : public UIWindow
{
	public:
		ConvertDriveWindow ();
		~ConvertDriveWindow ();
		void SetDiagnostics (Diagnostics *pdiag);
		
		void Initialise (HWND hWnd, unsigned int uID);
		void OutputInt (LPCSTR lpszText, int iValue);
		void OutputText (LPCSTR lpszText);
		void OutputText (LPCSTR lpszName, LPCSTR lpszValue);
		void SetStagesVisible (bool visible);
		void SetInputVisible (bool visible);
		void GetAvailableDrives ();
		void PopulateAvailDrives ();
		SingleDriveInfo GetSelectedDrive ();
		void GetPassword (char *szOutpassword);
		void SetStageProgress (int Stage, int Percent);
		void SetButtonMode (bool bEncrypt);
		void SetStage (int iStage, int iStatus);
		void SetButtonEnabled (bool bEnabled);
		void Reset ();
		void InvalidateWindow ();
		void InvalidateHeader ();
		bool IsPasswordOk ();
		void SetStagesComplete (bool bSuccess);
	private:
		// Private Member Variables & objects

		// The UI Handler required for multiple handling of custom controls.
		UIHandler m_uihandler;
		
		// ID ofthis window - required for window class registration purposes
		unsigned int m_ID;

		// Global Window Handle
		HWND m_hwnd;
		HWND m_parenthwnd;

		// The header bitmap image
		UIBanner m_header;
		UIRect m_whiterect;
		UILabel m_headerlabel;
		UILabel m_subheaderlabel;
		UILabel m_subheaderlabel2;
		
		UILabel m_drivelabel;
		UILabel m_password1label;
		UILabel m_password2label;
		HWND m_hwnddriveselection;
		HWND m_hwndpassword1; //ID_CONVPASSWORD1
		HWND m_hwndpassword2; //ID_CONVPASSWORD2
		HWND m_hwndencryptbutton;
		bool m_bbuttonmode;

		// Controls used during the conversion process
		UIIconLabel m_lblbackup;
		UIIconLabel m_lblformat;
		UIIconLabel m_lblcreate;
		UIIconLabel	m_lblrestore;
		UIIconLabel m_lblcompleting;
		
		// The success or failure icons of each
		// part of the process
		UIBanner m_tickbackup;
		UIBanner m_crossbackup;

		UIBanner m_tickformat;
		UIBanner m_crossformat;
		
		UIBanner m_tickcreate;
		UIBanner m_crosscreate;
		
		UIBanner m_tickrestore;
		UIBanner m_crossrestore;
		
		UIBanner m_tickcompleting;
		UIBanner m_crosscompleting;

		// The list of drives on the system available to
		// convert
		DynList m_dlAvaildrives;

		// Flag indicating if we're using diagnostics
		bool m_bUseDiagnostics;
		Diagnostics *m_pdiag;

		// Registered class name
		// We need a different class name for every instance of
		// this window. This class name
		// Is created by the Initialise routine
		// with a uID value suffixed to the end
		char m_szClassname[SIZE_STRING];

		// event notification from base class
		void OnDestroy (HWND hWnd);
		void OnCreate (HWND hWnd);		
		void OnCommand (HWND hWnd, WPARAM wParam, LPARAM lParam);
		void OnUICommand (HWND hWnd, WPARAM wParam, LPARAM lParam);		
		void OnUIScroll (HWND hWnd, WPARAM wParam, LPARAM lParam);
		void OnPaint (HWND hWnd);
		void OnTimer (WPARAM wParam);
		void OnMouseMove (HWND hWnd, int mouseXPos, int mouseYPos);
		LRESULT OnClose (HWND hWnd, WPARAM wParam, LPARAM lParam);
		void OnLButtonDown (HWND hWnd);
		void OnLButtonUp (HWND hWnd);
};
