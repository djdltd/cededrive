#pragma once
#include <windows.h>
#include <io.h>
#include <commctrl.h>
#include "UIWindow.h"
#include "Diagnostics.h"
#include "UIHandler.h"
#include "UIBanner.h"
#include "MultiContent.h"
#include "UIRect.h"
#include "UIPicButton.h"
#include "SingleDriveInfo.h"

class AddDriveWindow : public UIWindow
{
	public:
		AddDriveWindow ();
		~AddDriveWindow ();
		void SetDiagnostics (Diagnostics *pdiag);
		
		void Initialise (HWND hWnd, unsigned int uID);
		void OutputInt (LPCSTR lpszText, int iValue);
		void OutputText (LPCSTR lpszText);
		void OutputText (LPCSTR lpszName, LPCSTR lpszValue);
		SingleDriveInfo GetDriveInfo ();
		void SetDriveInfo ();

	private:
		// Private Member Variables & objects
		bool ValidateTextField (int ControlID, char *szControlname);
		bool ValidateInput ();
		bool SaveSingleFile ();

		// The UI Handler required for multiple handling of custom controls.
		UIHandler m_uihandler;
		
		// ID ofthis window - required for window class registration purposes
		unsigned int m_ID;

		// Global Window Handle
		HWND m_hwnd;
		HWND m_parenthwnd;

		// The header bitmap image
		UIBanner m_header;		
		
	/*
	#define CID_ADDBANNER		416
	#define CID_LABEL				417
	#define CID_TXTDRIVENAME	418
	#define CID_TXTDRIVEDESCRIPTION	419
	#define CID_TXTDRIVEPATH	420
	#define CID_BTNBROWSEPATH	421
	#define CID_TXTDISKSIZE		422
	#define CID_TXTDRIVELETTER	423
	#define CID_OPTMOUNTSTARTUP	424
	#define CID_BTNADDOK			425
	#define CID_BTNADDCANCEL		426
	*/

		HWND m_hwndlabel;
		HWND m_hwnddrivename;
		HWND m_hwnddrivedescription;
		HWND m_hwnddrivepath;
		HWND m_hwndbrowsepath;
		HWND m_hwnddisksize;
		HWND m_hwnddriveletter;
		HWND m_hwndmountstartup;
		HWND m_hwndaddok;
		HWND m_hwndaddcancel;

		// The member drive info object - set once the user has
		// filled in all the fields.
		SingleDriveInfo m_driveinfo;

		// Flag indicating if we're using diagnostics
		bool m_bUseDiagnostics;
		Diagnostics *m_pdiag;

		char m_szOutputfile[SIZE_STRING];
		bool m_bMountstartup;
		// Registered class name
		// We need a different class name for every instance of
		// this window. This class name
		// Is created by the Initialise routine
		// with a uID value suffixed to the end
		char m_szClassname[SIZE_STRING];

		// event notification from base class
		void OnDestroy (HWND hWnd);
		void OnCreate (HWND hWnd);		
		void OnCommand (HWND hWnd, WPARAM wParam, LPARAM lParam);
		void OnUICommand (HWND hWnd, WPARAM wParam, LPARAM lParam);		
		void OnUIScroll (HWND hWnd, WPARAM wParam, LPARAM lParam);
		void OnPaint (HWND hWnd);
		void OnTimer (WPARAM wParam);
		void OnMouseMove (HWND hWnd, int mouseXPos, int mouseYPos);
		void OnLButtonDown (HWND hWnd);
		void OnLButtonUp (HWND hWnd);
};
