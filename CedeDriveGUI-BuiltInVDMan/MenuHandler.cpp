#include "MenuHandler.h"

MenuHandler::MenuHandler ()
{
	// IMPORTANT!!!! Only set this when building this application for embedding into the cededrive
	// Launcher! - Be sure to set this back to false when doing a main app build!!! Otherwise
	// This will hide 2 of the main menu options.
		
#ifdef _FORLAUNCH
	m_bdeployedfromlauncher  = true;
#else
	m_bdeployedfromlauncher  = false;
#endif

}

MenuHandler::~MenuHandler ()
{
	
}

void MenuHandler::SetDiagnostics (Diagnostics *pdiag)
{
	m_pdiag = pdiag;
}

void MenuHandler::CreateMainMenu (HWND hwndParent)
{
	HMENU hMenu, hSubMenu, hSubMenu2, hSubMenu3, hSubMenu4;
	
	hMenu = CreateMenu ();
		
	hSubMenu = CreatePopupMenu ();
	
	AppendMenu (hSubMenu, MF_STRING, IDM_TEST_OPTION, "T&est Option");
	
	if (m_bdeployedfromlauncher == false) {
		AppendMenu (hSubMenu, MF_STRING, IDM_CONVERT_DRIVE, "E&ncrypt Removable Storage");
		AppendMenu (hSubMenu, MF_STRING, IDM_ADD_DRIVE, "N&ew Encrypted Drive");
	}
	AppendMenu (hSubMenu, MF_STRING, IDM_FILE_EXIT, "E&xit");
	AppendMenu (hMenu, MF_STRING | MF_POPUP, (UINT) hSubMenu, "&File");

	hSubMenu2 = CreatePopupMenu ();
	//AppendMenu (hSubMenu2, MF_STRING, IDM_TOOLS_OPTIONS, "O&ptions");
	//AppendMenu (hMenu, MF_STRING | MF_POPUP, (UINT) hSubMenu2, "&Tools");
	
	hSubMenu4 = CreatePopupMenu ();
	AppendMenu (hSubMenu4, MF_STRING, IDM_HELP_ABOUT, "&About");
	AppendMenu (hMenu, MF_STRING | MF_POPUP, (UINT) hSubMenu4, "&Help");

	SetMenu (hwndParent, hMenu);
}

void MenuHandler::CreateMainPopupMenu ()
{
	m_hPopupMenu = CreatePopupMenu ();

	if (m_bdeployedfromlauncher == false) {
		AppendMenu (m_hPopupMenu, MF_STRING, IDM_POPUP_ADDDRIVE, "New Encrypted Drive...");
		AppendMenu (m_hPopupMenu, MF_STRING, IDM_POPUP_DELDRIVE, "Delete Encrypted Drive");
	}
	AppendMenu (m_hPopupMenu, MF_MENUBREAK, NULL, NULL);
	AppendMenu (m_hPopupMenu, MF_STRING, IDM_POPUP_MOUNT, "Mount Drive");
	AppendMenu (m_hPopupMenu, MF_STRING, IDM_POPUP_UNMOUNT, "Unmount Drive");
	
	m_hTrayMenu = CreatePopupMenu();
	AppendMenu (m_hTrayMenu, MF_STRING, IDM_TRAY_SHOW, "Encrypted Drive Manager");
	AppendMenu (m_hTrayMenu, MF_STRING, IDM_TRAY_MOUNTALL, "Mount All Drives");
	AppendMenu (m_hTrayMenu, MF_STRING, IDM_TRAY_UNMOUNTALL, "Unmount All Drives");
	AppendMenu (m_hTrayMenu, MF_STRING, IDM_TRAY_EXIT, "Exit");

}