#pragma once
#include "resource.h"
#include <io.h>
#include <stdio.h>
#include <windows.h>
#include <direct.h>

class DriveFormatter {
	
	public:
		DriveFormatter ();
		~DriveFormatter ();
		
		bool QuickFormatDrive (char *szDriveLetter);
		void GetErrorReason (char *szReason, int reasonlen);
		void SetTrialMode (bool bTrial);
		bool ConvertDriveToNTFS (char *szDriveLetter);
	private:

		bool MakeScript (char *szDriveLetter);
		bool PrepareCommandLine ();
		bool PrepareConvertCommandLine (char *szDriveletter);
		bool PrepareScriptPath ();
		int FIOOpenAppendFile (LPCSTR strFilename);
		void FIOWriteFileLine(LPCSTR strFileline);
		void FIOClose ();
		void ShowInt (int iInttoShow);

		BOOL m_bFileOpen;
		FILE *stream;
		int m_resultcode;
		char m_szScriptpath[SIZE_STRING];
		char m_szCommandline[SIZE_STRING];
		bool m_bTrialmode;
};