#pragma once
#include "resource.h"
#include <io.h>
#include <stdio.h>
#include <windows.h>
#include <direct.h>
#include "MemoryBuffer.h"

class SecureDelete {
	
	public:
		SecureDelete ();
		~SecureDelete ();
		
		void GetErrorReason (char *szReason, int reasonlen);
		bool SecureDeleteFile (char *szFilepath);
	private:

		bool PrepareCommandLine (char *szFilepath);
		bool FileExists (char *FileName);

		BOOL m_bFileOpen;
		FILE *stream;
		int m_resultcode;
		char m_szCommandline[SIZE_STRING];

};