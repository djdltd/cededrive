// This work is dedicated to the Lord our God. King of Heaven, Lord of Heaven's Armies.

#include "SecureMovePopup.h"

SecureMovePopup::SecureMovePopup ()
{
	m_bUseDiagnostics = true;
	ZeroMemory (m_szClassname, SIZE_STRING);
	m_ID = 0;
}

SecureMovePopup::~SecureMovePopup ()
{

}

void SecureMovePopup::SetDiagnostics (Diagnostics *pdiag)
{
	m_pdiag = pdiag;
	
}

void SecureMovePopup::Initialise (HWND hWnd, unsigned int uID)
{	
	// Make the ID global
	m_ID = uID;
	m_parenthwnd = hWnd;
	//m_hwnd = hWnd;

	// Temporary string value for uID
	char szID[SIZE_STRING];
	ZeroMemory (szID, SIZE_STRING);

	SetParentHWND (hWnd);
	SetBgColor (GetSysColor (COLOR_BTNFACE));
	SetCaption (TEXT ("Secure Move In Progress..."));
	SetWindowStyle (FS_STYLESTANDARD);

	// Create the class name
	strcat_s (m_szClassname, SIZE_STRING, "SecureMovePopupWindow");
	sprintf_s (szID, SIZE_STRING, "%d", uID);
	strcat_s (m_szClassname, SIZE_STRING, szID);

	CreateAppWindow (m_szClassname, 70, 0, 300, 150, true);
	m_uihandler.SetWindowProperties (0, 0, 0, 0, RGB (230, 230, 240));
	SetWindowPosition (FS_CENTER);
	SetAlwaysOnTop (true);
	//Show ();
}

void SecureMovePopup::OnDestroy (HWND hWnd)
{			
	Hide ();
}

void SecureMovePopup::OnClose (HWND hWnd)
{
	Hide ();
}

void SecureMovePopup::OnCreate (HWND hWnd)
{				
	// Make this window handle global
	m_hwnd = hWnd;
	g_hWnd = hWnd; // Need to do this otherwise control wrappers don't work.

	CreateLabel ("Secure move is in progress...", 20, 20, 300, 20, IDC_STATIC);	
	CreateLabel ("After your files have been copied, they will be permanently shredded using DOD standards for secure file deletion.", 20, 60, 270, 50, IDC_STATIC);
	
}

void SecureMovePopup::OutputInt (LPCSTR lpszText, int iValue)
{
	if (m_bUseDiagnostics == true) {
		m_pdiag->OutputInt (lpszText, iValue);
	}
}

void SecureMovePopup::OutputText (LPCSTR lpszText)
{
	if (m_bUseDiagnostics == true) {
		m_pdiag->OutputText (lpszText);
	}
}

void SecureMovePopup::OutputText (LPCSTR lpszName, LPCSTR lpszValue)
{
	if (m_bUseDiagnostics == true) {
		m_pdiag->OutputText (lpszName, lpszValue);
	}
}

void SecureMovePopup::OnCommand (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	switch (LOWORD (wParam)) {

	}
}

void SecureMovePopup::OnUIScroll (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	
}

void SecureMovePopup::OnUICommand (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	switch (wParam)
	{
		
	}
}

void SecureMovePopup::OnTimer (WPARAM wParam)
{	
	m_uihandler.NotifyTimer (wParam);	
}

void SecureMovePopup::OnPaint (HWND hWnd)
{
	m_uihandler.PaintControls (hWnd);	
}

void SecureMovePopup::OnMouseMove (HWND hWnd, int mouseXPos, int mouseYPos)
{	
	m_uihandler.NotifyMouseMove (mouseXPos, mouseYPos);
}

void SecureMovePopup::OnLButtonDown (HWND hWnd)
{
	m_uihandler.NotifyMouseDown ();
}

void SecureMovePopup::OnLButtonUp (HWND hWnd)
{
	m_uihandler.NotifyMouseUp ();
}