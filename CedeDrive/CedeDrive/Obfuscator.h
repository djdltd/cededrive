#pragma once
#include <windows.h>
#include "resource.h"
#include "Obfuscator.h"

class Obfuscator {
	
	public:
		Obfuscator ();
		~Obfuscator ();
		void GetMessage (char *szOutmessage);
		void GetMessage2 (char *szOutmessage);
	private:

		void InitialiseMessage ();
};