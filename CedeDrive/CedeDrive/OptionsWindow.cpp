// This work is dedicated to the Lord our God. King of Heaven, Lord of Heaven's Armies.

#include "OptionsWindow.h"

OptionsWindow::OptionsWindow ()
{
	m_bUseDiagnostics = true;
	ZeroMemory (m_szClassname, SIZE_STRING);
	m_ID = 0;
}

OptionsWindow::~OptionsWindow ()
{

}

void OptionsWindow::SetDiagnostics (Diagnostics *pdiag)
{
	m_pdiag = pdiag;
	m_mainconv.SetDiagnostics (pdiag);
}

void OptionsWindow::Initialise (HWND hWnd, unsigned int uID)
{	
	// Make the ID global
	m_ID = uID;
	m_parenthwnd = hWnd;
	//m_hwnd = hWnd;

	// Temporary string value for uID
	char szID[SIZE_STRING];
	ZeroMemory (szID, SIZE_STRING);

	SetParentHWND (hWnd);
	SetBgColor (GetSysColor (COLOR_BTNFACE));
	SetCaption (TEXT ("CedeDrive Options"));
	SetWindowStyle (FS_STYLESTANDARD);

	// Create the class name
	strcat_s (m_szClassname, SIZE_STRING, "CedeDriveOptionsWindow");
	sprintf_s (szID, SIZE_STRING, "%d", uID);
	strcat_s (m_szClassname, SIZE_STRING, szID);

	// Clear the multicontent control
	m_mainconv.Clear ();

	CreateAppWindow (m_szClassname, 70, 0, 500, 350, true);
	m_uihandler.SetWindowProperties (0, 0, 1, 0, RGB (230, 230, 240));
	SetWindowPosition (FS_CENTER);
	Show ();

	PopulateSettings ();
}

void OptionsWindow::PopulateSettings ()
{
	SetCheck (m_hwndchkusefullscreenpassword, m_registry.GetSetting_UsingSecurePassWindow ());
	SetCheck (m_hwndchkdisplayconsole, m_registry.GetSetting_ShowMainWindowOnStartup ());

	m_busingsecurepasswindow = m_registry.GetSetting_UsingSecurePassWindow ();
	m_bshowmainwindowonstartup = m_registry.GetSetting_ShowMainWindowOnStartup ();
}

void OptionsWindow::OnDestroy (HWND hWnd)
{			
	Hide ();
}

void OptionsWindow::OnCreate (HWND hWnd)
{				
	// Make this window handle global
	m_hwnd = hWnd;
	g_hWnd = hWnd; // Need to do this otherwise the control creation wrapper functions don't work.
	
	m_whiterect.SetProperties (m_hwnd, CID_STATIC, 0, 0, 500, 60);
	m_uihandler.AddDirectControl (&m_whiterect);

	m_headerlabel.SetTextProperties ("Arial", 130, 0, 0, 0, "CedeDrive Options", RGB (51, 100, 191));
	m_headerlabel.SetProperties (m_hwnd, CID_CONVERTHEADERLABEL, 20, 22, 350, 20);
	m_uihandler.AddDirectControl (&m_headerlabel); 

	m_hwndaddok = CreateButton ("Ok", 292, 282, 91, 27, CID_BTNOPTIONSOK);
	m_hwndaddcancel = CreateButton ("Cancel", 390, 282, 91, 27, CID_BTNOPTIONSCANCEL);
	m_hwndgroupbox = CreateGroupBox ("General Options", 18, 80, 460, 190, CID_STATIC);

	m_hwndchkusefullscreenpassword = CreateCheckBox ("Use secure password screen when resuming from Standby/Hibernate", 40, 110, 400, 21, CID_CHKOPT_USEFULLPASSWINDOW);
	m_hwndchkdisplayconsole = CreateCheckBox ("Always show the CedeDrive main window on startup", 40, 150, 400, 21, CID_CHKOPT_SHOWCONSOLE);
}

LRESULT OptionsWindow::OnClose (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	Hide();
	return 0;
}

void OptionsWindow::OutputInt (LPCSTR lpszText, int iValue)
{
	if (m_bUseDiagnostics == true) {
		m_pdiag->OutputInt (lpszText, iValue);
	}
}

void OptionsWindow::OutputText (LPCSTR lpszText)
{
	if (m_bUseDiagnostics == true) {
		m_pdiag->OutputText (lpszText);
	}
}

void OptionsWindow::OutputText (LPCSTR lpszName, LPCSTR lpszValue)
{
	if (m_bUseDiagnostics == true) {
		m_pdiag->OutputText (lpszName, lpszValue);
	}
}

void OptionsWindow::OnCommand (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	
	switch (LOWORD (wParam)) {

		case CID_CHKOPT_USEFULLPASSWINDOW:
			{
				if (m_busingsecurepasswindow == true) {
					m_busingsecurepasswindow = false;
					SetCheck (m_hwndchkusefullscreenpassword, false);
					m_registry.SetSetting_UsingSecurePassWindow (false);
				} else {
					m_busingsecurepasswindow = true;
					SetCheck (m_hwndchkusefullscreenpassword, true);
					m_registry.SetSetting_UsingSecurePassWindow (true);
				}
			}
			break;

		case CID_CHKOPT_SHOWCONSOLE:
			{
				if (m_bshowmainwindowonstartup == true) {
					m_bshowmainwindowonstartup = false;
					SetCheck (m_hwndchkdisplayconsole, false);
					m_registry.SetSetting_ShowMainWindowOnStartup (false);
				} else {
					m_bshowmainwindowonstartup = true;
					SetCheck (m_hwndchkdisplayconsole, true);
					m_registry.SetSetting_ShowMainWindowOnStartup (true);
				}
			}
			break;

		case CID_BTNOPTIONSOK:
			{
				Hide();
			}
			break;

		case CID_BTNOPTIONSCANCEL:
			{
				Hide ();
			}
			break;
	}
	
}

void OptionsWindow::OnUIScroll (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	m_mainconv.NotifyUIScroll (hWnd, wParam, lParam);
}

void OptionsWindow::OnUICommand (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	/*
	switch (wParam)
	{
		
	}
	*/
}

void OptionsWindow::OnTimer (WPARAM wParam)
{	
	m_uihandler.NotifyTimer (wParam);	
}

void OptionsWindow::OnPaint (HWND hWnd)
{
	m_uihandler.PaintControls (hWnd);	
}

void OptionsWindow::OnMouseMove (HWND hWnd, int mouseXPos, int mouseYPos)
{	
	m_uihandler.NotifyMouseMove (mouseXPos, mouseYPos);
}

void OptionsWindow::OnLButtonDown (HWND hWnd)
{
	m_uihandler.NotifyMouseDown ();
}

void OptionsWindow::OnLButtonUp (HWND hWnd)
{
	m_uihandler.NotifyMouseUp ();
}