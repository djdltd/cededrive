#include "resource.h"
#include "Diagnostics.h"
#include "SingleDriveInfo.h"
#include "StandardEncryption.h"
#include "ServicePipeClient.h"
#include "InstructionResult.h"

#define DISK_SIZE 100
#define MAX_DRIVES	30
#define DISCONNECTTIMER		121

class VirtualDriveManager {
	
	public:
		VirtualDriveManager ();
		~VirtualDriveManager ();		


		static DWORD WINAPI ThreadProc_AllocateDiskSpace (PVOID pParam);
		void DoThread_AllocateDiskSpace ();
		void NotifyTimer (WPARAM wParam);

		bool FileExists (char *FileName);

		// External useable methods
		void SetHWND (HWND hWnd);
		void SetDiagnostics (Diagnostics *pdiag);
		bool IsDriveMounted (SingleDriveInfo driveinfo);
		bool IsDriveLetterFree (char *szDriveletter);
		bool GetFreeDriveLetter (char *szOutdriveletter);
		int DriveLetterToNumber (char *szDriveletter);
		void NumberToDriveLetter (char *szOutdrive, int iNumber);
		void AllocateVirtualDisk (SingleDriveInfo driveinfo, MemoryBuffer *memFileheader);
		bool AllocateStandaloneVirtualDisk (SingleDriveInfo driveinfo, MemoryBuffer *memFileheader);
		void Initialise (char *szEncryptPassword);
		void ForgetPassword ();
		int GetFreeDriveResource ();
		bool DoesDriveExist (char *szDriveLetter);
		void GetMountError (char *szOutmessage);
		unsigned long GetVirtualDiskSizeMegs (char *szPath);
		

		int MountVirtualDisk (SingleDriveInfo driveinfo);
		void UnmountVirtualDrive (SingleDriveInfo driveinfo);
		void UnmountAllVirtualDrives ();
		bool IsServiceRunning ();
		void SetTrialMode (bool bTrialmode);
		unsigned long GetSectorMultiple (unsigned long lValue);
		void OutputInt (LPCSTR lpszText, int iValue);
		void OutputText (LPCSTR lpszText);
		void OutputText (LPCSTR lpszName, LPCSTR lpszValue);

		// Virtual Drive vars - these have to be public because they are accessed from
		// static methods through a static pointer.
		HANDLE m_hAllocateFile; // File handle used when allocating virtual disks.
		
		// Allocation progress variables
		char m_szProgressLabel[SIZE_STRING];
		int m_allocationpercent;
		int m_currentvalue;
		int m_maxvalues;
		
		// Virtual Drive resources - these are limited in number and represent
		// the maximum number of virtual drives that can be mounted at once
		// I'll limit these to 30

		HANDLE m_hFileResource[MAX_DRIVES];
		bool m_bResourceBusy[MAX_DRIVES];
		SingleDriveInfo m_Drivelist[MAX_DRIVES];
		MemoryBuffer m_memIndividualcipher[MAX_DRIVES];

		StandardEncryption m_enc;
		char m_szPassword[SIZE_STRING];
		MemoryBuffer m_memCipher;
		char m_szMounterrormessage[SIZE_STRING];

		static VirtualDriveManager* m_pinstance;
		HWND m_hwnd;

		SingleDriveInfo currentAllocationInfo;
		MemoryBuffer memcurrentfileheader;
		bool m_binitialised;



		void RemoveFromList (DynList *dlList, SingleDriveInfo driveinfo);
		DynList m_dlmounteddrives;
		bool IsDriveHostingVirtualDisk (char *szSingledriveletter);
	private:
		
		

		bool m_diagnosticsset;
		Diagnostics *m_pdiag;
		int m_bminutespassed;

		

		bool m_bTrialmode;
		ServicePipeClient m_vdclient;
};