#include "ServicePipeClient.h"

ServicePipeClient::ServicePipeClient ()
{
}

ServicePipeClient::~ServicePipeClient ()
{
}

InstructionResult ServicePipeClient::RequestMountDrive (SingleDriveInfo driveinfo)
{
	return MakeRequest (DRIVE_MOUNT, driveinfo);
}

InstructionResult ServicePipeClient::RequestUnmountDrive (SingleDriveInfo driveinfo)
{
	return MakeRequest (DRIVE_UNMOUNT, driveinfo);
}

InstructionResult ServicePipeClient::RequestUnmountAllDrives ()
{
	SingleDriveInfo driveinfo;
	ZeroMemory (&driveinfo, sizeof (SingleDriveInfo));

	return MakeRequest (DRIVE_UNMOUNTALL, driveinfo);
}

InstructionResult ServicePipeClient::RequestPing ()
{
	SingleDriveInfo driveinfo;
	ZeroMemory (&driveinfo, sizeof (SingleDriveInfo));

	return MakeRequest (SERVICEPING, driveinfo);
}

InstructionResult ServicePipeClient::MakeRequest (unsigned int iInstruction, SingleDriveInfo driveinfo)
{
	HANDLE hPipe; 
	//TCHAR chBuf[BUFSIZE]; 
	BOOL fSuccess; 
	DWORD cbRead, cbWritten, dwMode; 
	LPTSTR lpszPipename = TEXT("\\\\.\\pipe\\CedeDriveServicePipe"); 
	InstructionResult iResult;

	// Create a temporary drive list structure for transmission across
	// the named pipe
	SingleDriveInfo drive1;	
	ZeroMemory (&drive1, sizeof (SingleDriveInfo));

	drive1 = driveinfo;
	drive1.iInstruction = iInstruction;

	DynList dlDrivelist;
	dlDrivelist.AddItem (&drive1, sizeof (SingleDriveInfo), false);

	// Serialise to memory buffer
	MemoryBuffer memList;
	dlDrivelist.ToMemoryBuffer (&memList);
 
	// Try to open a named pipe; wait for it, if necessary.  
	while (1) 
	{ 
		hPipe = CreateFile(lpszPipename, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);
		 
		// Break if the pipe handle is valid. 
		if (hPipe != INVALID_HANDLE_VALUE) 
		 break; 
		 
		// Exit if an error other than ERROR_PIPE_BUSY occurs. 
		if (GetLastError() != ERROR_PIPE_BUSY) 
		{
			iResult.bSuccess = false;
			iResult.iResult = 20;
			ZeroMemory (iResult.szDescription, SIZE_STRING);
			strcpy_s (iResult.szDescription, SIZE_STRING, "Unable to open named pipe.");
			return iResult;
		}

		// All pipe instances are busy, so wait for 20 seconds. 
		if (!WaitNamedPipe(lpszPipename, 20000)) 
		{ 
			iResult.bSuccess = false;
			iResult.iResult = 20;
			ZeroMemory (iResult.szDescription, SIZE_STRING);
			strcpy_s (iResult.szDescription, SIZE_STRING, "Unable to open named pipe.");
			return iResult;
		} 
	} 
 
	// The pipe connected; change to message-read mode. 
	dwMode = PIPE_READMODE_MESSAGE; 
	fSuccess = SetNamedPipeHandleState(hPipe, &dwMode, NULL, NULL);
	if (!fSuccess) 
	{
	  iResult.bSuccess = false;
	  iResult.iResult = 20;
	  ZeroMemory (iResult.szDescription, SIZE_STRING);
	  strcpy_s (iResult.szDescription, SIZE_STRING, "SetNamedPipeHandleState Failed.");
	  return iResult;
	}

	fSuccess = WriteFile (hPipe, memList.GetBuffer(), memList.GetSize(), &cbWritten, NULL);

	if (!fSuccess) 
	{
	  iResult.bSuccess = false;
	  iResult.iResult = 20;
	  ZeroMemory (iResult.szDescription, SIZE_STRING);
	  strcpy_s (iResult.szDescription, SIZE_STRING, "WriteFile Failed. Unable to write request to pipe.");
	  return iResult;
	}
 
	memList.Clear ();
	dlDrivelist.Clear ();

	do 
	{ 
		// Read from the pipe. 
		MemoryBuffer memResult;
		memResult.SetSize (RESULTBUF);

		fSuccess = ReadFile(hPipe, memResult.GetBuffer (), RESULTBUF, &cbRead, NULL);
		if (! fSuccess && GetLastError() != ERROR_MORE_DATA) 
		 break; 

		MemoryBuffer memAnswer;
		memAnswer.SetSize (cbRead);
		memAnswer.Write (memResult.GetBuffer (), 0, cbRead);
		memResult.Clear ();

		InstructionResult irResult;
		memcpy (&irResult, memAnswer.GetBuffer (), sizeof (InstructionResult));	
		iResult = irResult;

	} while (!fSuccess);  // repeat loop if ERROR_MORE_DATA 
		
	CloseHandle(hPipe);


	return iResult;
}