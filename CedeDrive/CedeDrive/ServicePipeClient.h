#include "resource.h"
#include "Diagnostics.h"
#include "SingleDriveInfo.h"
#include "StandardEncryption.h"
#include "InstructionResult.h"

#define BUFSIZE 512
#define RESULTBUF		10000
#define DRIVE_MOUNT					100
#define DRIVE_UNMOUNT			101
#define DRIVE_UNMOUNTALL		102
#define SERVICEPING					103

class ServicePipeClient {
	
	public:
		ServicePipeClient ();
		~ServicePipeClient ();		
		InstructionResult RequestMountDrive (SingleDriveInfo driveinfo);
		InstructionResult RequestUnmountDrive (SingleDriveInfo driveinfo);
		InstructionResult MakeRequest (unsigned int iInstruction, SingleDriveInfo driveinfo);
		InstructionResult RequestUnmountAllDrives ();
		InstructionResult RequestPing ();
	private:
		
		
};
