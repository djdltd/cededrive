#include "Obfuscator.h"

Obfuscator::Obfuscator ()
{

}

Obfuscator::~Obfuscator ()
{
	
}

void Obfuscator::InitialiseMessage ()
{

}

void Obfuscator::GetMessage (char *szOutmessage)
{
	const int exlen = 448;
	int expectedarray[exlen];
	expectedarray[0] = 132;
	expectedarray[1] = 158;
	expectedarray[2] = 165;
	expectedarray[3] = 161;
	expectedarray[4] = 83;
	expectedarray[5] = 141;
	expectedarray[6] = 164;
	expectedarray[7] = 171;
	expectedarray[8] = 87;
	expectedarray[9] = 129;
	expectedarray[10] = 89;
	expectedarray[11] = 166;
	expectedarray[12] = 164;
	expectedarray[13] = 162;
	expectedarray[14] = 177;
	expectedarray[15] = 94;
	expectedarray[16] = 180;
	expectedarray[17] = 176;
	expectedarray[18] = 97;
	expectedarray[19] = 175;
	expectedarray[20] = 188;
	expectedarray[21] = 100;
	expectedarray[22] = 170;
	expectedarray[23] = 191;
	expectedarray[24] = 172;
	expectedarray[25] = 187;
	expectedarray[26] = 117;
	expectedarray[27] = 106;
	expectedarray[28] = 154;
	expectedarray[29] = 108;
	expectedarray[30] = 166;
	expectedarray[31] = 189;
	expectedarray[32] = 196;
	expectedarray[33] = 112;
	expectedarray[34] = 200;
	expectedarray[35] = 186;
	expectedarray[36] = 194;
	expectedarray[37] = 116;
	expectedarray[38] = 185;
	expectedarray[39] = 205;
	expectedarray[40] = 188;
	expectedarray[41] = 196;
	expectedarray[42] = 197;
	expectedarray[43] = 122;
	expectedarray[44] = 196;
	expectedarray[45] = 202;
	expectedarray[46] = 125;
	expectedarray[47] = 210;
	expectedarray[48] = 199;
	expectedarray[49] = 197;
	expectedarray[50] = 129;
	expectedarray[51] = 202;
	expectedarray[52] = 200;
	expectedarray[53] = 197;
	expectedarray[54] = 219;
	expectedarray[55] = 203;
	expectedarray[56] = 213;
	expectedarray[57] = 219;
	expectedarray[58] = 151;
	expectedarray[59] = 138;
	expectedarray[60] = 173;
	expectedarray[61] = 209;
	expectedarray[62] = 213;
	expectedarray[63] = 221;
	expectedarray[64] = 219;
	expectedarray[65] = 212;
	expectedarray[66] = 157;
	expectedarray[67] = 146;
	expectedarray[68] = 212;
	expectedarray[69] = 231;
	expectedarray[70] = 149;
	expectedarray[71] = 234;
	expectedarray[72] = 223;
	expectedarray[73] = 221;
	expectedarray[74] = 153;
	expectedarray[75] = 223;
	expectedarray[76] = 244;
	expectedarray[77] = 225;
	expectedarray[78] = 240;
	expectedarray[79] = 158;
	expectedarray[80] = 238;
	expectedarray[81] = 230;
	expectedarray[82] = 161;
	expectedarray[83] = 245;
	expectedarray[84] = 232;
	expectedarray[85] = 246;
	expectedarray[86] = 251;
	expectedarray[87] = 231;
	expectedarray[88] = 245;
	expectedarray[89] = 252;
	expectedarray[90] = 252;
	expectedarray[91] = 170;
	expectedarray[92] = 247;
	expectedarray[93] = 251;
	expectedarray[94] = 252;
	expectedarray[95] = 249;
	expectedarray[96] = 175;
	expectedarray[97] = 260;
	expectedarray[98] = 256;
	expectedarray[99] = 178;
	expectedarray[100] = 263;
	expectedarray[101] = 252;
	expectedarray[102] = 250;
	expectedarray[103] = 182;
	expectedarray[104] = 255;
	expectedarray[105] = 249;
	expectedarray[106] = 263;
	expectedarray[107] = 254;
	expectedarray[108] = 187;
	expectedarray[109] = 267;
	expectedarray[110] = 259;
	expectedarray[111] = 190;
	expectedarray[112] = 275;
	expectedarray[113] = 264;
	expectedarray[114] = 262;
	expectedarray[115] = 267;
	expectedarray[116] = 277;
	expectedarray[117] = 196;
	expectedarray[118] = 274;
	expectedarray[119] = 263;
	expectedarray[120] = 282;
	expectedarray[121] = 284;
	expectedarray[122] = 270;
	expectedarray[123] = 284;
	expectedarray[124] = 286;
	expectedarray[125] = 216;
	expectedarray[126] = 205;
	expectedarray[127] = 239;
	expectedarray[128] = 290;
	expectedarray[129] = 208;
	expectedarray[130] = 293;
	expectedarray[131] = 282;
	expectedarray[132] = 280;
	expectedarray[133] = 212;
	expectedarray[134] = 282;
	expectedarray[135] = 303;
	expectedarray[136] = 284;
	expectedarray[137] = 299;
	expectedarray[138] = 217;
	expectedarray[139] = 297;
	expectedarray[140] = 289;
	expectedarray[141] = 220;
	expectedarray[142] = 286;
	expectedarray[143] = 222;
	expectedarray[144] = 300;
	expectedarray[145] = 289;
	expectedarray[146] = 298;
	expectedarray[147] = 294;
	expectedarray[148] = 227;
	expectedarray[149] = 312;
	expectedarray[150] = 308;
	expectedarray[151] = 230;
	expectedarray[152] = 315;
	expectedarray[153] = 304;
	expectedarray[154] = 302;
	expectedarray[155] = 234;
	expectedarray[156] = 307;
	expectedarray[157] = 301;
	expectedarray[158] = 315;
	expectedarray[159] = 306;
	expectedarray[160] = 239;
	expectedarray[161] = 319;
	expectedarray[162] = 311;
	expectedarray[163] = 242;
	expectedarray[164] = 315;
	expectedarray[165] = 313;
	expectedarray[166] = 327;
	expectedarray[167] = 246;
	expectedarray[168] = 324;
	expectedarray[169] = 321;
	expectedarray[170] = 332;
	expectedarray[171] = 334;
	expectedarray[172] = 333;
	expectedarray[173] = 321;
	expectedarray[174] = 336;
	expectedarray[175] = 337;
	expectedarray[176] = 267;
	expectedarray[177] = 256;
	expectedarray[178] = 308;
	expectedarray[179] = 337;
	expectedarray[180] = 259;
	expectedarray[181] = 339;
	expectedarray[182] = 346;
	expectedarray[183] = 344;
	expectedarray[184] = 263;
	expectedarray[185] = 333;
	expectedarray[186] = 354;
	expectedarray[187] = 335;
	expectedarray[188] = 350;
	expectedarray[189] = 268;
	expectedarray[190] = 345;
	expectedarray[191] = 349;
	expectedarray[192] = 350;
	expectedarray[193] = 347;
	expectedarray[194] = 273;
	expectedarray[195] = 358;
	expectedarray[196] = 354;
	expectedarray[197] = 276;
	expectedarray[198] = 361;
	expectedarray[199] = 350;
	expectedarray[200] = 348;
	expectedarray[201] = 280;
	expectedarray[202] = 325;
	expectedarray[203] = 329;
	expectedarray[204] = 333;
	expectedarray[205] = 320;
	expectedarray[206] = 285;
	expectedarray[207] = 365;
	expectedarray[208] = 372;
	expectedarray[209] = 370;
	expectedarray[210] = 289;
	expectedarray[211] = 329;
	expectedarray[212] = 370;
	expectedarray[213] = 360;
	expectedarray[214] = 305;
	expectedarray[215] = 294;
	expectedarray[216] = 348;
	expectedarray[217] = 374;
	expectedarray[218] = 381;
	expectedarray[219] = 371;
	expectedarray[220] = 375;
	expectedarray[221] = 300;
	expectedarray[222] = 341;
	expectedarray[223] = 371;
	expectedarray[224] = 303;
	expectedarray[225] = 376;
	expectedarray[226] = 370;
	expectedarray[227] = 389;
	expectedarray[228] = 307;
	expectedarray[229] = 385;
	expectedarray[230] = 378;
	expectedarray[231] = 392;
	expectedarray[232] = 378;
	expectedarray[233] = 401;
	expectedarray[234] = 313;
	expectedarray[235] = 393;
	expectedarray[236] = 393;
	expectedarray[237] = 316;
	expectedarray[238] = 402;
	expectedarray[239] = 401;
	expectedarray[240] = 333;
	expectedarray[241] = 320;
	expectedarray[242] = 361;
	expectedarray[243] = 387;
	expectedarray[244] = 409;
	expectedarray[245] = 393;
	expectedarray[246] = 325;
	expectedarray[247] = 403;
	expectedarray[248] = 396;
	expectedarray[249] = 410;
	expectedarray[250] = 396;
	expectedarray[251] = 419;
	expectedarray[252] = 331;
	expectedarray[253] = 411;
	expectedarray[254] = 411;
	expectedarray[255] = 334;
	expectedarray[256] = 420;
	expectedarray[257] = 419;
	expectedarray[258] = 349;
	expectedarray[259] = 338;
	expectedarray[260] = 386;
	expectedarray[261] = 340;
	expectedarray[262] = 385;
	expectedarray[263] = 389;
	expectedarray[264] = 393;
	expectedarray[265] = 380;
	expectedarray[266] = 357;
	expectedarray[267] = 346;
	expectedarray[268] = 419;
	expectedarray[269] = 413;
	expectedarray[270] = 435;
	expectedarray[271] = 419;
	expectedarray[272] = 351;
	expectedarray[273] = 429;
	expectedarray[274] = 422;
	expectedarray[275] = 436;
	expectedarray[276] = 422;
	expectedarray[277] = 445;
	expectedarray[278] = 357;
	expectedarray[279] = 437;
	expectedarray[280] = 437;
	expectedarray[281] = 360;
	expectedarray[282] = 446;
	expectedarray[283] = 445;
	expectedarray[284] = 364;
	expectedarray[285] = 364;
	expectedarray[286] = 403;
	expectedarray[287] = 445;
	expectedarray[288] = 449;
	expectedarray[289] = 368;
	expectedarray[290] = 456;
	expectedarray[291] = 439;
	expectedarray[292] = 371;
	expectedarray[293] = 437;
	expectedarray[294] = 455;
	expectedarray[295] = 443;
	expectedarray[296] = 375;
	expectedarray[297] = 445;
	expectedarray[298] = 465;
	expectedarray[299] = 445;
	expectedarray[300] = 448;
	expectedarray[301] = 449;
	expectedarray[302] = 449;
	expectedarray[303] = 455;
	expectedarray[304] = 461;
	expectedarray[305] = 455;
	expectedarray[306] = 461;
	expectedarray[307] = 475;
	expectedarray[308] = 387;
	expectedarray[309] = 458;
	expectedarray[310] = 462;
	expectedarray[311] = 466;
	expectedarray[312] = 467;
	expectedarray[313] = 461;
	expectedarray[314] = 461;
	expectedarray[315] = 394;
	expectedarray[316] = 482;
	expectedarray[317] = 469;
	expectedarray[318] = 481;
	expectedarray[319] = 470;
	expectedarray[320] = 399;
	expectedarray[321] = 467;
	expectedarray[322] = 480;
	expectedarray[323] = 480;
	expectedarray[324] = 487;
	expectedarray[325] = 473;
	expectedarray[326] = 482;
	expectedarray[327] = 486;
	expectedarray[328] = 491;
	expectedarray[329] = 422;
	expectedarray[330] = 409;
	expectedarray[331] = 457;
	expectedarray[332] = 496;
	expectedarray[333] = 494;
	expectedarray[334] = 413;
	expectedarray[335] = 497;
	expectedarray[336] = 494;
	expectedarray[337] = 501;
	expectedarray[338] = 493;
	expectedarray[339] = 418;
	expectedarray[340] = 492;
	expectedarray[341] = 503;
	expectedarray[342] = 421;
	expectedarray[343] = 491;
	expectedarray[344] = 511;
	expectedarray[345] = 491;
	expectedarray[346] = 494;
	expectedarray[347] = 495;
	expectedarray[348] = 495;
	expectedarray[349] = 501;
	expectedarray[350] = 507;
	expectedarray[351] = 501;
	expectedarray[352] = 507;
	expectedarray[353] = 521;
	expectedarray[354] = 433;
	expectedarray[355] = 504;
	expectedarray[356] = 508;
	expectedarray[357] = 512;
	expectedarray[358] = 513;
	expectedarray[359] = 507;
	expectedarray[360] = 507;
	expectedarray[361] = 440;
	expectedarray[362] = 496;
	expectedarray[363] = 515;
	expectedarray[364] = 527;
	expectedarray[365] = 516;
	expectedarray[366] = 445;
	expectedarray[367] = 530;
	expectedarray[368] = 519;
	expectedarray[369] = 517;
	expectedarray[370] = 449;
	expectedarray[371] = 533;
	expectedarray[372] = 518;
	expectedarray[373] = 531;
	expectedarray[374] = 535;
	expectedarray[375] = 532;
	expectedarray[376] = 455;
	expectedarray[377] = 535;
	expectedarray[378] = 527;
	expectedarray[379] = 458;
	expectedarray[380] = 543;
	expectedarray[381] = 532;
	expectedarray[382] = 540;
	expectedarray[383] = 545;
	expectedarray[384] = 532;
	expectedarray[385] = 464;
	expectedarray[386] = 552;
	expectedarray[387] = 538;
	expectedarray[388] = 546;
	expectedarray[389] = 468;
	expectedarray[390] = 534;
	expectedarray[391] = 552;
	expectedarray[392] = 540;
	expectedarray[393] = 472;
	expectedarray[394] = 538;
	expectedarray[395] = 558;
	expectedarray[396] = 475;
	expectedarray[397] = 545;
	expectedarray[398] = 542;
	expectedarray[399] = 561;
	expectedarray[400] = 548;
	expectedarray[401] = 492;
	expectedarray[402] = 481;
	expectedarray[403] = 537;
	expectedarray[404] = 556;
	expectedarray[405] = 568;
	expectedarray[406] = 557;
	expectedarray[407] = 486;
	expectedarray[408] = 571;
	expectedarray[409] = 560;
	expectedarray[410] = 558;
	expectedarray[411] = 490;
	expectedarray[412] = 558;
	expectedarray[413] = 571;
	expectedarray[414] = 571;
	expectedarray[415] = 578;
	expectedarray[416] = 564;
	expectedarray[417] = 573;
	expectedarray[418] = 577;
	expectedarray[419] = 582;
	expectedarray[420] = 499;
	expectedarray[421] = 579;
	expectedarray[422] = 571;
	expectedarray[423] = 502;
	expectedarray[424] = 587;
	expectedarray[425] = 576;
	expectedarray[426] = 574;
	expectedarray[427] = 506;
	expectedarray[428] = 587;
	expectedarray[429] = 590;
	expectedarray[430] = 588;
	expectedarray[431] = 595;
	expectedarray[432] = 579;
	expectedarray[433] = 526;
	expectedarray[434] = 513;
	expectedarray[435] = 522;
	expectedarray[436] = 563;
	expectedarray[437] = 599;
	expectedarray[438] = 582;
	expectedarray[439] = 594;
	expectedarray[440] = 596;
	expectedarray[441] = 603;
	expectedarray[442] = 521;
	expectedarray[443] = 539;
	expectedarray[444] = 541;
	expectedarray[445] = 543;
	expectedarray[446] = 534;
	expectedarray[447] = 540;


	char szMessage[SIZE_STRING];
	ZeroMemory (szMessage, SIZE_STRING);

	char szChar[SIZE_NAME];
	int dec = 0;

	for (int i=0;i<exlen;i++) {
		ZeroMemory (szChar, SIZE_NAME);		
		dec = expectedarray[i] - i - 47;
		szMessage[i] = dec;
	}

	ZeroMemory (szOutmessage, SIZE_STRING);
	strcpy_s (szOutmessage, SIZE_STRING, szMessage);

	// Delete the temporary message we've decoded
	SecureZeroMemory (szMessage, SIZE_STRING);
}

void Obfuscator::GetMessage2 (char *szOutmessage)
{
	const int exlen = 824;
	int expectedarray[exlen];
    expectedarray[0] = 132;
    expectedarray[1] = 158;
    expectedarray[2] = 165;
    expectedarray[3] = 161;
    expectedarray[4] = 83;
    expectedarray[5] = 141;
    expectedarray[6] = 164;
    expectedarray[7] = 171;
    expectedarray[8] = 87;
    expectedarray[9] = 129;
    expectedarray[10] = 89;
    expectedarray[11] = 166;
    expectedarray[12] = 164;
    expectedarray[13] = 162;
    expectedarray[14] = 177;
    expectedarray[15] = 94;
    expectedarray[16] = 180;
    expectedarray[17] = 176;
    expectedarray[18] = 97;
    expectedarray[19] = 175;
    expectedarray[20] = 188;
    expectedarray[21] = 100;
    expectedarray[22] = 170;
    expectedarray[23] = 191;
    expectedarray[24] = 172;
    expectedarray[25] = 187;
    expectedarray[26] = 117;
    expectedarray[27] = 106;
    expectedarray[28] = 154;
    expectedarray[29] = 108;
    expectedarray[30] = 166;
    expectedarray[31] = 189;
    expectedarray[32] = 196;
    expectedarray[33] = 112;
    expectedarray[34] = 200;
    expectedarray[35] = 186;
    expectedarray[36] = 194;
    expectedarray[37] = 116;
    expectedarray[38] = 185;
    expectedarray[39] = 205;
    expectedarray[40] = 188;
    expectedarray[41] = 196;
    expectedarray[42] = 197;
    expectedarray[43] = 122;
    expectedarray[44] = 196;
    expectedarray[45] = 202;
    expectedarray[46] = 125;
    expectedarray[47] = 210;
    expectedarray[48] = 199;
    expectedarray[49] = 197;
    expectedarray[50] = 129;
    expectedarray[51] = 202;
    expectedarray[52] = 200;
    expectedarray[53] = 197;
    expectedarray[54] = 219;
    expectedarray[55] = 203;
    expectedarray[56] = 213;
    expectedarray[57] = 219;
    expectedarray[58] = 151;
    expectedarray[59] = 138;
    expectedarray[60] = 173;
    expectedarray[61] = 209;
    expectedarray[62] = 213;
    expectedarray[63] = 221;
    expectedarray[64] = 219;
    expectedarray[65] = 212;
    expectedarray[66] = 157;
    expectedarray[67] = 146;
    expectedarray[68] = 212;
    expectedarray[69] = 231;
    expectedarray[70] = 149;
    expectedarray[71] = 234;
    expectedarray[72] = 223;
    expectedarray[73] = 221;
    expectedarray[74] = 153;
    expectedarray[75] = 223;
    expectedarray[76] = 244;
    expectedarray[77] = 225;
    expectedarray[78] = 240;
    expectedarray[79] = 158;
    expectedarray[80] = 238;
    expectedarray[81] = 230;
    expectedarray[82] = 161;
    expectedarray[83] = 245;
    expectedarray[84] = 232;
    expectedarray[85] = 246;
    expectedarray[86] = 251;
    expectedarray[87] = 231;
    expectedarray[88] = 245;
    expectedarray[89] = 252;
    expectedarray[90] = 252;
    expectedarray[91] = 170;
    expectedarray[92] = 247;
    expectedarray[93] = 251;
    expectedarray[94] = 252;
    expectedarray[95] = 249;
    expectedarray[96] = 175;
    expectedarray[97] = 260;
    expectedarray[98] = 256;
    expectedarray[99] = 178;
    expectedarray[100] = 263;
    expectedarray[101] = 252;
    expectedarray[102] = 250;
    expectedarray[103] = 182;
    expectedarray[104] = 255;
    expectedarray[105] = 249;
    expectedarray[106] = 263;
    expectedarray[107] = 254;
    expectedarray[108] = 187;
    expectedarray[109] = 267;
    expectedarray[110] = 259;
    expectedarray[111] = 190;
    expectedarray[112] = 275;
    expectedarray[113] = 264;
    expectedarray[114] = 262;
    expectedarray[115] = 267;
    expectedarray[116] = 277;
    expectedarray[117] = 196;
    expectedarray[118] = 274;
    expectedarray[119] = 263;
    expectedarray[120] = 282;
    expectedarray[121] = 284;
    expectedarray[122] = 270;
    expectedarray[123] = 284;
    expectedarray[124] = 286;
    expectedarray[125] = 216;
    expectedarray[126] = 205;
    expectedarray[127] = 239;
    expectedarray[128] = 290;
    expectedarray[129] = 208;
    expectedarray[130] = 293;
    expectedarray[131] = 282;
    expectedarray[132] = 280;
    expectedarray[133] = 212;
    expectedarray[134] = 282;
    expectedarray[135] = 303;
    expectedarray[136] = 284;
    expectedarray[137] = 299;
    expectedarray[138] = 217;
    expectedarray[139] = 297;
    expectedarray[140] = 289;
    expectedarray[141] = 220;
    expectedarray[142] = 286;
    expectedarray[143] = 222;
    expectedarray[144] = 300;
    expectedarray[145] = 289;
    expectedarray[146] = 298;
    expectedarray[147] = 294;
    expectedarray[148] = 227;
    expectedarray[149] = 312;
    expectedarray[150] = 308;
    expectedarray[151] = 230;
    expectedarray[152] = 315;
    expectedarray[153] = 304;
    expectedarray[154] = 302;
    expectedarray[155] = 234;
    expectedarray[156] = 307;
    expectedarray[157] = 301;
    expectedarray[158] = 315;
    expectedarray[159] = 306;
    expectedarray[160] = 239;
    expectedarray[161] = 319;
    expectedarray[162] = 311;
    expectedarray[163] = 242;
    expectedarray[164] = 315;
    expectedarray[165] = 313;
    expectedarray[166] = 327;
    expectedarray[167] = 246;
    expectedarray[168] = 324;
    expectedarray[169] = 321;
    expectedarray[170] = 332;
    expectedarray[171] = 334;
    expectedarray[172] = 333;
    expectedarray[173] = 321;
    expectedarray[174] = 336;
    expectedarray[175] = 337;
    expectedarray[176] = 267;
    expectedarray[177] = 256;
    expectedarray[178] = 308;
    expectedarray[179] = 337;
    expectedarray[180] = 259;
    expectedarray[181] = 339;
    expectedarray[182] = 346;
    expectedarray[183] = 344;
    expectedarray[184] = 263;
    expectedarray[185] = 333;
    expectedarray[186] = 354;
    expectedarray[187] = 335;
    expectedarray[188] = 350;
    expectedarray[189] = 268;
    expectedarray[190] = 345;
    expectedarray[191] = 349;
    expectedarray[192] = 350;
    expectedarray[193] = 347;
    expectedarray[194] = 273;
    expectedarray[195] = 358;
    expectedarray[196] = 354;
    expectedarray[197] = 276;
    expectedarray[198] = 361;
    expectedarray[199] = 350;
    expectedarray[200] = 348;
    expectedarray[201] = 280;
    expectedarray[202] = 325;
    expectedarray[203] = 329;
    expectedarray[204] = 333;
    expectedarray[205] = 320;
    expectedarray[206] = 285;
    expectedarray[207] = 365;
    expectedarray[208] = 372;
    expectedarray[209] = 370;
    expectedarray[210] = 289;
    expectedarray[211] = 329;
    expectedarray[212] = 370;
    expectedarray[213] = 360;
    expectedarray[214] = 305;
    expectedarray[215] = 294;
    expectedarray[216] = 348;
    expectedarray[217] = 374;
    expectedarray[218] = 381;
    expectedarray[219] = 371;
    expectedarray[220] = 375;
    expectedarray[221] = 300;
    expectedarray[222] = 341;
    expectedarray[223] = 371;
    expectedarray[224] = 303;
    expectedarray[225] = 376;
    expectedarray[226] = 370;
    expectedarray[227] = 389;
    expectedarray[228] = 307;
    expectedarray[229] = 385;
    expectedarray[230] = 378;
    expectedarray[231] = 392;
    expectedarray[232] = 378;
    expectedarray[233] = 401;
    expectedarray[234] = 313;
    expectedarray[235] = 393;
    expectedarray[236] = 393;
    expectedarray[237] = 316;
    expectedarray[238] = 402;
    expectedarray[239] = 401;
    expectedarray[240] = 333;
    expectedarray[241] = 320;
    expectedarray[242] = 361;
    expectedarray[243] = 387;
    expectedarray[244] = 409;
    expectedarray[245] = 393;
    expectedarray[246] = 325;
    expectedarray[247] = 403;
    expectedarray[248] = 396;
    expectedarray[249] = 410;
    expectedarray[250] = 396;
    expectedarray[251] = 419;
    expectedarray[252] = 331;
    expectedarray[253] = 411;
    expectedarray[254] = 411;
    expectedarray[255] = 334;
    expectedarray[256] = 420;
    expectedarray[257] = 419;
    expectedarray[258] = 349;
    expectedarray[259] = 338;
    expectedarray[260] = 386;
    expectedarray[261] = 340;
    expectedarray[262] = 385;
    expectedarray[263] = 389;
    expectedarray[264] = 393;
    expectedarray[265] = 380;
    expectedarray[266] = 357;
    expectedarray[267] = 346;
    expectedarray[268] = 419;
    expectedarray[269] = 413;
    expectedarray[270] = 435;
    expectedarray[271] = 419;
    expectedarray[272] = 351;
    expectedarray[273] = 429;
    expectedarray[274] = 422;
    expectedarray[275] = 436;
    expectedarray[276] = 422;
    expectedarray[277] = 445;
    expectedarray[278] = 357;
    expectedarray[279] = 437;
    expectedarray[280] = 437;
    expectedarray[281] = 360;
    expectedarray[282] = 446;
    expectedarray[283] = 445;
    expectedarray[284] = 364;
    expectedarray[285] = 364;
    expectedarray[286] = 403;
    expectedarray[287] = 445;
    expectedarray[288] = 449;
    expectedarray[289] = 368;
    expectedarray[290] = 456;
    expectedarray[291] = 439;
    expectedarray[292] = 371;
    expectedarray[293] = 437;
    expectedarray[294] = 455;
    expectedarray[295] = 443;
    expectedarray[296] = 375;
    expectedarray[297] = 445;
    expectedarray[298] = 465;
    expectedarray[299] = 445;
    expectedarray[300] = 448;
    expectedarray[301] = 449;
    expectedarray[302] = 449;
    expectedarray[303] = 455;
    expectedarray[304] = 461;
    expectedarray[305] = 455;
    expectedarray[306] = 461;
    expectedarray[307] = 475;
    expectedarray[308] = 387;
    expectedarray[309] = 458;
    expectedarray[310] = 462;
    expectedarray[311] = 466;
    expectedarray[312] = 467;
    expectedarray[313] = 461;
    expectedarray[314] = 461;
    expectedarray[315] = 394;
    expectedarray[316] = 482;
    expectedarray[317] = 469;
    expectedarray[318] = 481;
    expectedarray[319] = 470;
    expectedarray[320] = 399;
    expectedarray[321] = 467;
    expectedarray[322] = 480;
    expectedarray[323] = 480;
    expectedarray[324] = 487;
    expectedarray[325] = 473;
    expectedarray[326] = 482;
    expectedarray[327] = 486;
    expectedarray[328] = 491;
    expectedarray[329] = 422;
    expectedarray[330] = 409;
    expectedarray[331] = 457;
    expectedarray[332] = 496;
    expectedarray[333] = 494;
    expectedarray[334] = 413;
    expectedarray[335] = 497;
    expectedarray[336] = 494;
    expectedarray[337] = 501;
    expectedarray[338] = 493;
    expectedarray[339] = 418;
    expectedarray[340] = 492;
    expectedarray[341] = 503;
    expectedarray[342] = 421;
    expectedarray[343] = 491;
    expectedarray[344] = 511;
    expectedarray[345] = 491;
    expectedarray[346] = 494;
    expectedarray[347] = 495;
    expectedarray[348] = 495;
    expectedarray[349] = 501;
    expectedarray[350] = 507;
    expectedarray[351] = 501;
    expectedarray[352] = 507;
    expectedarray[353] = 521;
    expectedarray[354] = 433;
    expectedarray[355] = 504;
    expectedarray[356] = 508;
    expectedarray[357] = 512;
    expectedarray[358] = 513;
    expectedarray[359] = 507;
    expectedarray[360] = 507;
    expectedarray[361] = 440;
    expectedarray[362] = 496;
    expectedarray[363] = 515;
    expectedarray[364] = 527;
    expectedarray[365] = 516;
    expectedarray[366] = 445;
    expectedarray[367] = 530;
    expectedarray[368] = 519;
    expectedarray[369] = 517;
    expectedarray[370] = 449;
    expectedarray[371] = 533;
    expectedarray[372] = 518;
    expectedarray[373] = 531;
    expectedarray[374] = 535;
    expectedarray[375] = 532;
    expectedarray[376] = 455;
    expectedarray[377] = 535;
    expectedarray[378] = 527;
    expectedarray[379] = 458;
    expectedarray[380] = 543;
    expectedarray[381] = 532;
    expectedarray[382] = 540;
    expectedarray[383] = 545;
    expectedarray[384] = 532;
    expectedarray[385] = 464;
    expectedarray[386] = 552;
    expectedarray[387] = 538;
    expectedarray[388] = 546;
    expectedarray[389] = 468;
    expectedarray[390] = 534;
    expectedarray[391] = 552;
    expectedarray[392] = 540;
    expectedarray[393] = 472;
    expectedarray[394] = 538;
    expectedarray[395] = 558;
    expectedarray[396] = 475;
    expectedarray[397] = 545;
    expectedarray[398] = 542;
    expectedarray[399] = 561;
    expectedarray[400] = 548;
    expectedarray[401] = 492;
    expectedarray[402] = 481;
    expectedarray[403] = 537;
    expectedarray[404] = 556;
    expectedarray[405] = 568;
    expectedarray[406] = 557;
    expectedarray[407] = 486;
    expectedarray[408] = 571;
    expectedarray[409] = 560;
    expectedarray[410] = 558;
    expectedarray[411] = 490;
    expectedarray[412] = 558;
    expectedarray[413] = 571;
    expectedarray[414] = 571;
    expectedarray[415] = 578;
    expectedarray[416] = 564;
    expectedarray[417] = 573;
    expectedarray[418] = 577;
    expectedarray[419] = 582;
    expectedarray[420] = 499;
    expectedarray[421] = 579;
    expectedarray[422] = 571;
    expectedarray[423] = 502;
    expectedarray[424] = 587;
    expectedarray[425] = 576;
    expectedarray[426] = 574;
    expectedarray[427] = 506;
    expectedarray[428] = 587;
    expectedarray[429] = 590;
    expectedarray[430] = 588;
    expectedarray[431] = 595;
    expectedarray[432] = 579;
    expectedarray[433] = 526;
    expectedarray[434] = 513;
    expectedarray[435] = 522;
    expectedarray[436] = 563;
    expectedarray[437] = 599;
    expectedarray[438] = 582;
    expectedarray[439] = 594;
    expectedarray[440] = 596;
    expectedarray[441] = 603;
    expectedarray[442] = 521;
    expectedarray[443] = 539;
    expectedarray[444] = 541;
    expectedarray[445] = 543;
    expectedarray[446] = 534;
    expectedarray[447] = 540;
    expectedarray[448] = 527;
    expectedarray[449] = 561;
    expectedarray[450] = 529;
    expectedarray[451] = 607;
    expectedarray[452] = 600;
    expectedarray[453] = 615;
    expectedarray[454] = 616;
    expectedarray[455] = 599;
    expectedarray[456] = 606;
    expectedarray[457] = 605;
    expectedarray[458] = 537;
    expectedarray[459] = 622;
    expectedarray[460] = 618;
    expectedarray[461] = 540;
    expectedarray[462] = 618;
    expectedarray[463] = 631;
    expectedarray[464] = 543;
    expectedarray[465] = 614;
    expectedarray[466] = 614;
    expectedarray[467] = 622;
    expectedarray[468] = 623;
    expectedarray[469] = 627;
    expectedarray[470] = 636;
    expectedarray[471] = 550;
    expectedarray[472] = 631;
    expectedarray[473] = 634;
    expectedarray[474] = 632;
    expectedarray[475] = 625;
    expectedarray[476] = 637;
    expectedarray[477] = 621;
    expectedarray[478] = 634;
    expectedarray[479] = 635;
    expectedarray[480] = 628;
    expectedarray[481] = 642;
    expectedarray[482] = 644;
    expectedarray[483] = 588;
    expectedarray[484] = 563;
    expectedarray[485] = 615;
    expectedarray[486] = 644;
    expectedarray[487] = 566;
    expectedarray[488] = 656;
    expectedarray[489] = 647;
    expectedarray[490] = 654;
    expectedarray[491] = 577;
    expectedarray[492] = 657;
    expectedarray[493] = 641;
    expectedarray[494] = 573;
    expectedarray[495] = 656;
    expectedarray[496] = 644;
    expectedarray[497] = 643;
    expectedarray[498] = 656;
    expectedarray[499] = 664;
    expectedarray[500] = 648;
    expectedarray[501] = 662;
    expectedarray[502] = 650;
    expectedarray[503] = 650;
    expectedarray[504] = 583;
    expectedarray[505] = 668;
    expectedarray[506] = 657;
    expectedarray[507] = 659;
    expectedarray[508] = 670;
    expectedarray[509] = 588;
    expectedarray[510] = 666;
    expectedarray[511] = 659;
    expectedarray[512] = 674;
    expectedarray[513] = 675;
    expectedarray[514] = 658;
    expectedarray[515] = 665;
    expectedarray[516] = 664;
    expectedarray[517] = 596;
    expectedarray[518] = 670;
    expectedarray[519] = 676;
    expectedarray[520] = 599;
    expectedarray[521] = 665;
    expectedarray[522] = 679;
    expectedarray[523] = 602;
    expectedarray[524] = 668;
    expectedarray[525] = 688;
    expectedarray[526] = 689;
    expectedarray[527] = 675;
    expectedarray[528] = 684;
    expectedarray[529] = 688;
    expectedarray[530] = 693;
    expectedarray[531] = 610;
    expectedarray[532] = 695;
    expectedarray[533] = 691;
    expectedarray[534] = 613;
    expectedarray[535] = 696;
    expectedarray[536] = 684;
    expectedarray[537] = 683;
    expectedarray[538] = 696;
    expectedarray[539] = 704;
    expectedarray[540] = 688;
    expectedarray[541] = 702;
    expectedarray[542] = 621;
    expectedarray[543] = 706;
    expectedarray[544] = 695;
    expectedarray[545] = 693;
    expectedarray[546] = 625;
    expectedarray[547] = 694;
    expectedarray[548] = 709;
    expectedarray[549] = 701;
    expectedarray[550] = 715;
    expectedarray[551] = 699;
    expectedarray[552] = 631;
    expectedarray[553] = 712;
    expectedarray[554] = 698;
    expectedarray[555] = 717;
    expectedarray[556] = 718;
    expectedarray[557] = 723;
    expectedarray[558] = 716;
    expectedarray[559] = 720;
    expectedarray[560] = 707;
    expectedarray[561] = 654;
    expectedarray[562] = 641;
    expectedarray[563] = 697;
    expectedarray[564] = 712;
    expectedarray[565] = 720;
    expectedarray[566] = 721;
    expectedarray[567] = 646;
    expectedarray[568] = 715;
    expectedarray[569] = 727;
    expectedarray[570] = 727;
    expectedarray[571] = 719;
    expectedarray[572] = 665;
    expectedarray[573] = 652;
    expectedarray[574] = 686;
    expectedarray[575] = 730;
    expectedarray[576] = 739;
    expectedarray[577] = 728;
    expectedarray[578] = 736;
    expectedarray[579] = 743;
    expectedarray[580] = 730;
    expectedarray[581] = 732;
    expectedarray[582] = 661;
    expectedarray[583] = 746;
    expectedarray[584] = 735;
    expectedarray[585] = 729;
    expectedarray[586] = 749;
    expectedarray[587] = 666;
    expectedarray[588] = 754;
    expectedarray[589] = 733;
    expectedarray[590] = 752;
    expectedarray[591] = 670;
    expectedarray[592] = 755;
    expectedarray[593] = 744;
    expectedarray[594] = 742;
    expectedarray[595] = 674;
    expectedarray[596] = 744;
    expectedarray[597] = 741;
    expectedarray[598] = 760;
    expectedarray[599] = 767;
    expectedarray[600] = 679;
    expectedarray[601] = 746;
    expectedarray[602] = 754;
    expectedarray[603] = 766;
    expectedarray[604] = 695;
    expectedarray[605] = 684;
    expectedarray[606] = 750;
    expectedarray[607] = 764;
    expectedarray[608] = 755;
    expectedarray[609] = 688;
    expectedarray[610] = 773;
    expectedarray[611] = 762;
    expectedarray[612] = 764;
    expectedarray[613] = 775;
    expectedarray[614] = 693;
    expectedarray[615] = 778;
    expectedarray[616] = 764;
    expectedarray[617] = 784;
    expectedarray[618] = 781;
    expectedarray[619] = 698;
    expectedarray[620] = 767;
    expectedarray[621] = 779;
    expectedarray[622] = 770;
    expectedarray[623] = 785;
    expectedarray[624] = 781;
    expectedarray[625] = 711;
    expectedarray[626] = 789;
    expectedarray[627] = 706;
    expectedarray[628] = 776;
    expectedarray[629] = 794;
    expectedarray[630] = 778;
    expectedarray[631] = 788;
    expectedarray[632] = 711;
    expectedarray[633] = 784;
    expectedarray[634] = 782;
    expectedarray[635] = 790;
    expectedarray[636] = 795;
    expectedarray[637] = 716;
    expectedarray[638] = 806;
    expectedarray[639] = 797;
    expectedarray[640] = 804;
    expectedarray[641] = 720;
    expectedarray[642] = 792;
    expectedarray[643] = 791;
    expectedarray[644] = 807;
    expectedarray[645] = 724;
    expectedarray[646] = 790;
    expectedarray[647] = 793;
    expectedarray[648] = 794;
    expectedarray[649] = 797;
    expectedarray[650] = 812;
    expectedarray[651] = 813;
    expectedarray[652] = 731;
    expectedarray[653] = 816;
    expectedarray[654] = 812;
    expectedarray[655] = 734;
    expectedarray[656] = 819;
    expectedarray[657] = 808;
    expectedarray[658] = 806;
    expectedarray[659] = 738;
    expectedarray[660] = 819;
    expectedarray[661] = 805;
    expectedarray[662] = 824;
    expectedarray[663] = 825;
    expectedarray[664] = 830;
    expectedarray[665] = 823;
    expectedarray[666] = 827;
    expectedarray[667] = 814;
    expectedarray[668] = 747;
    expectedarray[669] = 814;
    expectedarray[670] = 818;
    expectedarray[671] = 817;
    expectedarray[672] = 816;
    expectedarray[673] = 837;
    expectedarray[674] = 836;
    expectedarray[675] = 823;
    expectedarray[676] = 755;
    expectedarray[677] = 845;
    expectedarray[678] = 836;
    expectedarray[679] = 843;
    expectedarray[680] = 759;
    expectedarray[681] = 847;
    expectedarray[682] = 834;
    expectedarray[683] = 838;
    expectedarray[684] = 839;
    expectedarray[685] = 764;
    expectedarray[686] = 848;
    expectedarray[687] = 850;
    expectedarray[688] = 840;
    expectedarray[689] = 844;
    expectedarray[690] = 845;
    expectedarray[691] = 770;
    expectedarray[692] = 849;
    expectedarray[693] = 841;
    expectedarray[694] = 842;
    expectedarray[695] = 842;
    expectedarray[696] = 775;
    expectedarray[697] = 860;
    expectedarray[698] = 856;
    expectedarray[699] = 778;
    expectedarray[700] = 847;
    expectedarray[701] = 859;
    expectedarray[702] = 781;
    expectedarray[703] = 847;
    expectedarray[704] = 783;
    expectedarray[705] = 818;
    expectedarray[706] = 867;
    expectedarray[707] = 871;
    expectedarray[708] = 871;
    expectedarray[709] = 857;
    expectedarray[710] = 789;
    expectedarray[711] = 828;
    expectedarray[712] = 870;
    expectedarray[713] = 874;
    expectedarray[714] = 860;
    expectedarray[715] = 863;
    expectedarray[716] = 795;
    expectedarray[717] = 861;
    expectedarray[718] = 881;
    expectedarray[719] = 882;
    expectedarray[720] = 868;
    expectedarray[721] = 877;
    expectedarray[722] = 881;
    expectedarray[723] = 886;
    expectedarray[724] = 803;
    expectedarray[725] = 883;
    expectedarray[726] = 883;
    expectedarray[727] = 806;
    expectedarray[728] = 891;
    expectedarray[729] = 880;
    expectedarray[730] = 878;
    expectedarray[731] = 810;
    expectedarray[732] = 880;
    expectedarray[733] = 890;
    expectedarray[734] = 880;
    expectedarray[735] = 896;
    expectedarray[736] = 904;
    expectedarray[737] = 896;
    expectedarray[738] = 901;
    expectedarray[739] = 891;
    expectedarray[740] = 898;
    expectedarray[741] = 898;
    expectedarray[742] = 835;
    expectedarray[743] = 822;
    expectedarray[744] = 874;
    expectedarray[745] = 903;
    expectedarray[746] = 825;
    expectedarray[747] = 865;
    expectedarray[748] = 906;
    expectedarray[749] = 907;
    expectedarray[750] = 897;
    expectedarray[751] = 830;
    expectedarray[752] = 875;
    expectedarray[753] = 917;
    expectedarray[754] = 900;
    expectedarray[755] = 909;
    expectedarray[756] = 849;
    expectedarray[757] = 836;
    expectedarray[758] = 892;
    expectedarray[759] = 910;
    expectedarray[760] = 928;
    expectedarray[761] = 840;
    expectedarray[762] = 919;
    expectedarray[763] = 921;
    expectedarray[764] = 927;
    expectedarray[765] = 844;
    expectedarray[766] = 919;
    expectedarray[767] = 931;
    expectedarray[768] = 930;
    expectedarray[769] = 932;
    expectedarray[770] = 849;
    expectedarray[771] = 921;
    expectedarray[772] = 924;
    expectedarray[773] = 938;
    expectedarray[774] = 922;
    expectedarray[775] = 854;
    expectedarray[776] = 940;
    expectedarray[777] = 936;
    expectedarray[778] = 857;
    expectedarray[779] = 936;
    expectedarray[780] = 938;
    expectedarray[781] = 947;
    expectedarray[782] = 861;
    expectedarray[783] = 927;
    expectedarray[784] = 941;
    expectedarray[785] = 932;
    expectedarray[786] = 865;
    expectedarray[787] = 949;
    expectedarray[788] = 932;
    expectedarray[789] = 954;
    expectedarray[790] = 938;
    expectedarray[791] = 870;
    expectedarray[792] = 960;
    expectedarray[793] = 951;
    expectedarray[794] = 958;
    expectedarray[795] = 956;
    expectedarray[796] = 958;
    expectedarray[797] = 945;
    expectedarray[798] = 953;
    expectedarray[799] = 948;
    expectedarray[800] = 879;
    expectedarray[801] = 945;
    expectedarray[802] = 881;
    expectedarray[803] = 969;
    expectedarray[804] = 955;
    expectedarray[805] = 963;
    expectedarray[806] = 961;
    expectedarray[807] = 955;
    expectedarray[808] = 887;
    expectedarray[809] = 964;
    expectedarray[810] = 968;
    expectedarray[811] = 955;
    expectedarray[812] = 959;
    expectedarray[813] = 892;
    expectedarray[814] = 972;
    expectedarray[815] = 964;
    expectedarray[816] = 895;
    expectedarray[817] = 968;
    expectedarray[818] = 962;
    expectedarray[819] = 981;
    expectedarray[820] = 982;
    expectedarray[821] = 976;
    expectedarray[822] = 970;
    expectedarray[823] = 916;


	char szMessage[SIZE_STRING];
	ZeroMemory (szMessage, SIZE_STRING);

	char szChar[SIZE_NAME];
	int dec = 0;

	for (int i=0;i<exlen;i++) {
		ZeroMemory (szChar, SIZE_NAME);		
		dec = expectedarray[i] - i - 47;
		szMessage[i] = dec;
	}

	ZeroMemory (szOutmessage, SIZE_STRING);
	strcpy_s (szOutmessage, SIZE_STRING, szMessage);

	// Delete the temporary message we've decoded
	SecureZeroMemory (szMessage, SIZE_STRING);
}