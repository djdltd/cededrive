#include <windows.h>

#pragma once

#define SIZE_STRING		1024
#define SIZE_INTEGER	32
#define SIZE_NAME		64

class SingleEntry {
	
	public:
		SingleEntry ();
		~SingleEntry ();		
		
		char szFilePath[SIZE_STRING];
		unsigned long lFileSize;
		time_t lModifiedTime;

	private:
		
};
