// This work is dedicated to the Lord our God. King of Heaven, Lord of Heaven's Armies.

#include "AddDriveWindow.h"

AddDriveWindow::AddDriveWindow ()
{
	m_bUseDiagnostics = true;
	ZeroMemory (m_szClassname, SIZE_STRING);
	m_ID = 0;
	m_bMountstartup = false;
}

AddDriveWindow::~AddDriveWindow ()
{

}

void AddDriveWindow::SetDiagnostics (Diagnostics *pdiag)
{
	m_pdiag = pdiag;
}

void AddDriveWindow::Initialise (HWND hWnd, unsigned int uID)
{	
	// Make the ID global
	m_ID = uID;
	m_parenthwnd = hWnd;
	//m_hwnd = hWnd;

	// Temporary string value for uID
	char szID[SIZE_STRING];
	ZeroMemory (szID, SIZE_STRING);

	SetParentHWND (hWnd);
	SetBgColor (GetSysColor(COLOR_BTNFACE));
	SetCaption (TEXT ("Add Encrypted Drive"));
	SetWindowStyle (FS_STYLESTANDARD);

	// Create the class name
	strcat_s (m_szClassname, SIZE_STRING, "ADDDRIVEWindowClass");
	sprintf_s (szID, SIZE_STRING, "%d", uID);
	strcat_s (m_szClassname, SIZE_STRING, szID);

	CreateAppWindow (m_szClassname, 70, 0, 658, 434, true);
	m_uihandler.SetWindowProperties (0, 0, 300, 0, RGB (230, 230, 240));
	SetWindowPosition (FS_CENTER);
	Show ();
}

void AddDriveWindow::OnDestroy (HWND hWnd)
{			
	Hide ();
}

void AddDriveWindow::OnCreate (HWND hWnd)
{				
	// Make this window handle global
	m_hwnd = hWnd;
	g_hWnd = hWnd; // Need to do this otherwise the control creation wrapper functions don't work.

	/*
	#define CID_ADDBANNER		416
	#define CID_LABEL				417
	#define CID_TXTDRIVENAME	418
	#define CID_TXTDRIVEDESCRIPTION	419
	#define CID_TXTDRIVEPATH	420
	#define CID_BTNBROWSEPATH	421
	#define CID_TXTDISKSIZE		422
	#define CID_TXTDRIVELETTER	423
	#define CID_OPTMOUNTSTARTUP	424
	#define CID_BTNADDOK			425
	#define CID_BTNADDCANCEL		426


	HWND m_hwndlabel;
	HWND m_hwnddrivename;
	HWND m_hwnddrivedescription;
	HWND m_hwnddrivepath;
	HWND m_hwndbrowsepath;
	HWND m_hwnddisksize;
	HWND m_hwnddriveletter;
	HWND m_hwndmountstartup;
	HWND m_hwndaddok;
	HWND m_hwndaddcancel;

	*/

	HFONT hfDefault = (HFONT) GetStockObject (DEFAULT_GUI_FONT);

	m_whiterect.SetProperties (m_hwnd, CID_STATIC, 202, 0, 456, 344);
	m_uihandler.AddDirectControl (&m_whiterect);

	// Labels
	m_headerlabel.SetTextProperties ("Arial", 130, 0, 0, 0, "Create a new encrypted virtual drive...", RGB (51, 100, 191));
	m_headerlabel.SetProperties (m_hwnd, CID_CONVERTHEADERLABEL, 233, 22, 350, 20);
	m_uihandler.AddDirectControl (&m_headerlabel); 

	COLORREF labelcolor = RGB (90, 90, 90);

	m_label1.SetTextProperties ("Arial", 90, 0, 0, 0, "Encrypted Drive Name", labelcolor);
	m_label1.SetProperties (m_hwnd, CID_STATIC, 235, 70, 114, 13);
	m_uihandler.AddDirectControl (&m_label1);

	m_label2.SetTextProperties ("Arial", 90, 0, 0, 0, "Encrypted Drive Description", labelcolor);
	m_label2.SetProperties (m_hwnd, CID_STATIC, 235, 122, 144, 13);
	m_uihandler.AddDirectControl (&m_label2);

	m_label3.SetTextProperties ("Arial", 90, 0, 0, 0, "Encrypted Drive File", labelcolor);
	m_label3.SetProperties (m_hwnd, CID_STATIC, 235, 236, 114, 13);
	m_uihandler.AddDirectControl (&m_label3);

	m_label4.SetTextProperties ("Arial", 90, 0, 0, 0, "Disk Size", labelcolor);
	m_label4.SetProperties (m_hwnd, CID_STATIC, 235, 284, 114, 13);
	m_uihandler.AddDirectControl (&m_label4);

	m_label5.SetTextProperties ("Arial", 90, 0, 0, 0, "Drive Letter", labelcolor);
	m_label5.SetProperties (m_hwnd, CID_STATIC, 381, 284, 114, 13);
	m_uihandler.AddDirectControl (&m_label5);

	m_label7.SetTextProperties ("Arial", 90, 0, 0, 0, "MB", labelcolor);
	m_label7.SetProperties (m_hwnd, CID_STATIC, 332, 304, 23, 13);
	m_uihandler.AddDirectControl (&m_label7);

	//m_hwndlabel = CreateLabel("Encrypted Drive Name", 233, 70, 114, 13, CID_LABEL);
	//m_hwndlabel = CreateLabel("Encrypted Drive Description", 233, 122, 144, 13, CID_LABEL);
	//m_hwndlabel = CreateLabel("Encrypted Drive File", 233, 237, 114, 13, CID_LABEL);
	//m_hwndlabel = CreateLabel("Disk Size", 233, 285, 114, 13, CID_LABEL);
	//m_hwndlabel = CreateLabel("Drive Letter", 379, 285, 114, 13, CID_LABEL);
	//m_hwndlabel = CreateLabel("MB", 332, 304, 23, 13, CID_LABEL);

	// Combo Boxes
	m_hwnddriveselection = CreateWindow ("ComboBox", NULL, WS_CHILD | WS_VISIBLE | WS_BORDER | CBS_DROPDOWN, 382, 301, 90, 20, hWnd, (HMENU) IDC_AVAILNEWDRIVES, GetModuleHandle (NULL), NULL);
	SendMessage (m_hwnddriveselection, WM_SETFONT, (WPARAM) hfDefault, MAKELPARAM (FALSE, 0));

	// Text Boxes
	m_hwnddrivename = CreateTextBox(235, 90, 374, 20, CID_TXTDRIVENAME);
	m_hwnddrivedescription = CreateTextBox(235, 140, 374, 45, CID_TXTDRIVEDESCRIPTION);
	m_hwnddrivepath = CreateTextBox(235, 253, 299, 20, CID_TXTDRIVEPATH);
	m_hwnddisksize = CreateTextBox(235, 301, 90, 20, CID_TXTDISKSIZE);
	//m_hwnddriveletter = CreateTextBox(382, 301, 90, 20, CID_TXTDRIVELETTER);

	// Buttons
	m_hwndaddok = CreateButton ("Ok", 442, 362, 91, 27, CID_BTNADDOK);
	m_hwndaddcancel = CreateButton ("Cancel", 539, 362, 91, 27, CID_BTNADDCANCEL);
	m_hwndbrowsepath = CreateButton ("Browse", 539, 253, 70, 20, CID_BTNBROWSEPATH);

	// Checkboxes
	m_label8.SetTextProperties ("Arial", 90, 0, 0, 0, "Mount at startup", RGB (0, 0, 0));
	m_label8.SetProperties (m_hwnd, CID_STATIC, 523, 304, 150, 13);
	m_uihandler.AddDirectControl (&m_label8);

	m_hwndmountstartup = CreateCheckBox("", 503, 304, 15, 15, CID_OPTMOUNTSTARTUP);

	m_label9.SetTextProperties ("Arial", 90, 0, 0, 0, "Create a recovery key just incase I forget my password", RGB (0, 0, 0));
	m_label9.SetProperties (m_hwnd, CID_STATIC, 255, 190, 400, 13);
	m_uihandler.AddDirectControl (&m_label9);

	m_hwndcreaterecovery = CreateCheckBox ("", 235, 190, 15, 15, CID_CHKCREATERECOVERY);

	// Header bitmap
	m_header.SetBitmapResources (IDB_ADDSPLASH);
	m_header.SetBitmapProperties (0, 0, 202, 409);
	m_header.SetProperties (hWnd, CID_ADDBANNER, 0, 0, 202, 409);
	m_uihandler.AddDirectControl (&m_header);

	SetCheck (m_hwndcreaterecovery, true);
	m_bCreaterecovery = true;
}

int AddDriveWindow::DriveLetterToNumber (char *szDriveletter)
{
	if (strcmp (szDriveletter, "A") == 0) {return 0;}
	if (strcmp (szDriveletter, "B") == 0) {return 1;}
	if (strcmp (szDriveletter, "C") == 0) {return 2;}
	if (strcmp (szDriveletter, "D") == 0) {return 3;}
	if (strcmp (szDriveletter, "E") == 0) {return 4;}
	if (strcmp (szDriveletter, "F") == 0) {return 5;}
	if (strcmp (szDriveletter, "G") == 0) {return 6;}
	if (strcmp (szDriveletter, "H") == 0) {return 7;}
	if (strcmp (szDriveletter, "I") == 0) {return 8;}
	if (strcmp (szDriveletter, "J") == 0) {return 9;}
	if (strcmp (szDriveletter, "K") == 0) {return 10;}
	if (strcmp (szDriveletter, "L") == 0) {return 11;}
	if (strcmp (szDriveletter, "M") == 0) {return 12;}
	if (strcmp (szDriveletter, "N") == 0) {return 13;}
	if (strcmp (szDriveletter, "O") == 0) {return 14;}
	if (strcmp (szDriveletter, "P") == 0) {return 15;}
	if (strcmp (szDriveletter, "Q") == 0) {return 16;}
	if (strcmp (szDriveletter, "R") == 0) {return 17;}
	if (strcmp (szDriveletter, "S") == 0) {return 18;}
	if (strcmp (szDriveletter, "T") == 0) {return 19;}
	if (strcmp (szDriveletter, "U") == 0) {return 20;}
	if (strcmp (szDriveletter, "V") == 0) {return 21;}
	if (strcmp (szDriveletter, "W") == 0) {return 22;}
	if (strcmp (szDriveletter, "X") == 0) {return 23;}
	if (strcmp (szDriveletter, "Y") == 0) {return 24;}
	if (strcmp (szDriveletter, "Z") == 0) {return 25;}
	
	return -1;
}

void AddDriveWindow::NumberToDriveLetter (char *szOutdrive, int iNumber)
{
	ZeroMemory (szOutdrive, SIZE_NAME);

	if (iNumber == 0) {strcpy_s (szOutdrive, SIZE_NAME, "A");}
	if (iNumber == 1) {strcpy_s (szOutdrive, SIZE_NAME, "B");}
	if (iNumber == 2) {strcpy_s (szOutdrive, SIZE_NAME, "C");}
	if (iNumber == 3) {strcpy_s (szOutdrive, SIZE_NAME, "D");}
	if (iNumber == 4) {strcpy_s (szOutdrive, SIZE_NAME, "E");}
	if (iNumber == 5) {strcpy_s (szOutdrive, SIZE_NAME, "F");}
	if (iNumber == 6) {strcpy_s (szOutdrive, SIZE_NAME, "G");}
	if (iNumber == 7) {strcpy_s (szOutdrive, SIZE_NAME, "H");}
	if (iNumber == 8) {strcpy_s (szOutdrive, SIZE_NAME, "I");}
	if (iNumber == 9) {strcpy_s (szOutdrive, SIZE_NAME, "J");}
	if (iNumber == 10) {strcpy_s (szOutdrive, SIZE_NAME, "K");}
	if (iNumber == 11) {strcpy_s (szOutdrive, SIZE_NAME, "L");}
	if (iNumber == 12) {strcpy_s (szOutdrive, SIZE_NAME, "M");}
	if (iNumber == 13) {strcpy_s (szOutdrive, SIZE_NAME, "N");}
	if (iNumber == 14) {strcpy_s (szOutdrive, SIZE_NAME, "O");}
	if (iNumber == 15) {strcpy_s (szOutdrive, SIZE_NAME, "P");}
	if (iNumber == 16) {strcpy_s (szOutdrive, SIZE_NAME, "Q");}
	if (iNumber == 17) {strcpy_s (szOutdrive, SIZE_NAME, "R");}
	if (iNumber == 18) {strcpy_s (szOutdrive, SIZE_NAME, "S");}
	if (iNumber == 19) {strcpy_s (szOutdrive, SIZE_NAME, "T");}
	if (iNumber == 20) {strcpy_s (szOutdrive, SIZE_NAME, "U");}
	if (iNumber == 21) {strcpy_s (szOutdrive, SIZE_NAME, "V");}
	if (iNumber == 22) {strcpy_s (szOutdrive, SIZE_NAME, "W");}
	if (iNumber == 23) {strcpy_s (szOutdrive, SIZE_NAME, "X");}
	if (iNumber == 24) {strcpy_s (szOutdrive, SIZE_NAME, "Y");}
	if (iNumber == 25) {strcpy_s (szOutdrive, SIZE_NAME, "Z");}
}

bool AddDriveWindow::DoesDriveLetterExist (DynList *dlDrivelist, char *szDriveletter)
{
	//	SingleDriveInfo *pinfo = (SingleDriveInfo *) m_dlDrivelist.GetItem (index);
	for (unsigned int a=0;a<dlDrivelist->GetNumItems ();a++) {
		
		SingleDriveInfo *pinfo = (SingleDriveInfo *) dlDrivelist->GetItem (a);

		if (strcmp (pinfo->szDriveLetter, szDriveletter) == 0) {
			return true;
		}
	}

	return false;
}

bool AddDriveWindow::GetFreeDriveLetters (DynList *dlCurrentdrivelist)
{
	SendMessage (m_hwnddriveselection,CB_RESETCONTENT, 0, 0);

	char szDrives[SIZE_STRING];
	ZeroMemory (szDrives, SIZE_STRING);

	char szCurDrive[SIZE_STRING];
	char szLetter[SIZE_NAME];
	char szAvaildrive[SIZE_NAME];

	int a = 0;
	int l = 0;
	int len = 0;

	bool bAvaildrives[26];

	for (a=0;a<26;a++) {
		bAvaildrives[a] = true;
	}

	len = GetLogicalDriveStrings (SIZE_STRING, szDrives);
	
	for (a=0;a<len;a++) {
		if (szDrives[a] == 0) {

			ZeroMemory (szCurDrive, SIZE_STRING);
			strncpy_s (szCurDrive, SIZE_STRING, szDrives+l, a-l);			
			l = a+1;

			ZeroMemory (szLetter, SIZE_NAME);
			strncpy_s (szLetter, SIZE_NAME, szCurDrive, 1);

			bAvaildrives[DriveLetterToNumber(szLetter)] = false;
			OutputText ("Drive: ", szCurDrive);
		}
	}

	// Ensure drive A,  B and C are not chosen
	bAvaildrives[0] = false;
	bAvaildrives[1] = false;
	bAvaildrives[2] = false;


	for (a=0;a<26;a++) {

		if (bAvaildrives[a] == true) {
			
			ZeroMemory (szAvaildrive, SIZE_NAME);
			NumberToDriveLetter (szAvaildrive, a);

			strcat_s (szAvaildrive, SIZE_NAME, ":");
			//OutputText("Available Drive: ", szAvaildrive);

			if (DoesDriveLetterExist (dlCurrentdrivelist, szAvaildrive) == false) {
				SendMessage (m_hwnddriveselection, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) szAvaildrive);
			}

			//return true;
		}	
	}

	SendMessage (m_hwnddriveselection, CB_SETCURSEL, 0, 0); // Set the current selection to the first item in the list

	return true;
}

bool AddDriveWindow::PrePopulateFromExistingFile (char *szFilename, DynList *dlExistingdrives)
{
	m_baddexistingmode = true;

	// First alter the GUI to be in add existing mode...
	m_headerlabel.SetTextCaption ("Add an existing encrypted drive...");
	ShowWindow (m_hwndcreaterecovery, SW_HIDE);
	m_label9.SetVisible (false);
	m_label9.InvalidateControl (true);

	InvalidateRect (m_hwnd, NULL, false);

	GetFreeDriveLetters(dlExistingdrives);

	// Now open the existing EVD file specified

	EVDFileHeader myHeader;

	if (myHeader.SetFromFile (szFilename) == true) {
		myHeader.Deserialise ();

		// Get the drive size
		unsigned long drivesize = myHeader.GetUserSpecifiedDriveSize ();
		char szDrivesize[SIZE_NAME];
		ZeroMemory (szDrivesize, SIZE_NAME);
		ltoa (drivesize, szDrivesize, 10);

		// Get the name
		char szName[SIZE_STRING];
		ZeroMemory (szName, SIZE_STRING);
		myHeader.GetName (szName);

		// Get the description
		char szDescription[SIZE_STRING];
		ZeroMemory (szDescription, SIZE_STRING);
		myHeader.GetDescription (szDescription);


		SetDlgItemText (m_hwnd, CID_TXTDRIVENAME, szName);
		SetDlgItemText (m_hwnd, CID_TXTDRIVEDESCRIPTION, szDescription);
		SetDlgItemText (m_hwnd, CID_TXTDRIVEPATH, szFilename);
		SetDlgItemText (m_hwnd, CID_TXTDISKSIZE, szDrivesize);
		SetCheck(m_hwndmountstartup, true);
		m_bMountstartup = true;

		return true;
	} else {
	
		char szError[SIZE_STRING];
		ZeroMemory (szError, SIZE_STRING);
		myHeader.GetLastErrorMessage (szError);

		MessageBox (m_hwnd, szError, "Problem opening EVD file", MB_OK | MB_ICONEXCLAMATION);

		return false;
	}
}

bool AddDriveWindow::PrePopulate (DynList *dlExistingdrives)
{
	m_baddexistingmode = false;
	m_headerlabel.SetTextCaption ("Create a new encrypted virtual drive...");
	m_label9.SetVisible (true);
	ShowWindow (m_hwndcreaterecovery, SW_SHOW);
	InvalidateRect (m_hwnd, NULL, false);

	GetFreeDriveLetters(dlExistingdrives);

	// Get the current username
	char szUsername[SIZE_STRING];
	ZeroMemory (szUsername, SIZE_STRING);
	DWORD dwSize = SIZE_STRING;

	char szAutodrivename[SIZE_STRING];
	ZeroMemory (szAutodrivename, SIZE_STRING);

	int inumexisting = dlExistingdrives->GetNumItems ();
	inumexisting++;

	char szNum[SIZE_STRING];
	ZeroMemory (szNum, SIZE_STRING);
	_itoa_s (inumexisting, szNum, SIZE_STRING, 10);

	if (GetUserName (szUsername, &dwSize) != 0) {
		// Success
		strcpy_s (szAutodrivename, SIZE_STRING, szUsername);
		strcat_s (szAutodrivename, SIZE_STRING, "'s Secure Data");
		strcat_s (szAutodrivename, SIZE_STRING, " (");
		strcat_s (szAutodrivename, SIZE_STRING, szNum);
		strcat_s (szAutodrivename, SIZE_STRING, ")");

	} else {
		// Failure
		strcpy_s (szAutodrivename, SIZE_STRING, "My Secure Data");
		strcat_s (szAutodrivename, SIZE_STRING, " (");
		strcat_s (szAutodrivename, SIZE_STRING, szNum);
		strcat_s (szAutodrivename, SIZE_STRING, ")");
	}

	// The automatically generated description
	char szAutodescription[SIZE_STRING];
	ZeroMemory (szAutodescription, SIZE_STRING);
	strcpy_s (szAutodescription, SIZE_STRING, "This is an encrypted drive containing my secure data.");

	// Now populate the location using My Documents
	int Result;
	char pPath[SIZE_STRING];
	ZeroMemory (pPath, SIZE_STRING);
	LPITEMIDLIST pIDL = NULL;

	Result = SHGetSpecialFolderLocation( (HWND)100, CSIDL_COMMON_DOCUMENTS, &pIDL );

	if ( Result == 0 )
	{
		Result = SHGetPathFromIDList(pIDL, pPath);
	}
	




	strcat_s (pPath, SIZE_STRING, "\\");
	strcat_s (pPath, SIZE_STRING, szUsername);
	strcat_s (pPath, SIZE_STRING, "EncryptedDisk");
	strcat_s (pPath, SIZE_STRING, szNum);
	strcat_s (pPath, SIZE_STRING, ".evd");

	/*
	GetDlgItemText (m_hwnd, CID_TXTDRIVENAME, m_driveinfo.szName, SIZE_STRING);
	GetDlgItemText (m_hwnd, CID_TXTDRIVEDESCRIPTION, m_driveinfo.szDescription, SIZE_STRING);
	GetDlgItemText (m_hwnd, CID_TXTDRIVEPATH, m_driveinfo.szPath, SIZE_STRING);
	*/

	SetDlgItemText (m_hwnd, CID_TXTDRIVENAME, szAutodrivename);
	SetDlgItemText (m_hwnd, CID_TXTDRIVEDESCRIPTION, szAutodescription);
	SetDlgItemText (m_hwnd, CID_TXTDRIVEPATH, pPath);
	SetDlgItemText (m_hwnd, CID_TXTDISKSIZE, "100");
	SetCheck(m_hwndmountstartup, true);
	m_bMountstartup = true;

	return true;
}

bool AddDriveWindow::ValidateTextField (int ControlID, char *szControlname)
{
	char szText[SIZE_STRING];
	ZeroMemory (szText, SIZE_STRING);

	GetDlgItemText (m_hwnd, ControlID, szText, SIZE_STRING);

	if (strlen(szText) > 1) {
		return true;
	} else {
		char szMessage[SIZE_STRING];
		ZeroMemory (szMessage, SIZE_STRING);
		sprintf_s (szMessage, SIZE_STRING, "Please enter some valid information for %s", szControlname);
		MessageBox (NULL, szMessage, "Input Problem", MB_OK | MB_ICONEXCLAMATION);
		return false;
	}
}

bool AddDriveWindow::ValidateInput ()
{
	if (ValidateTextField(CID_TXTDRIVENAME, "Drive Name") == false) {return false;}
	if (ValidateTextField(CID_TXTDRIVEDESCRIPTION, "Drive Description") == false) {return false;}
	if (ValidateTextField(CID_TXTDRIVEPATH, "Encrypted Drive File") == false) {return false;}
	if (ValidateTextField(CID_TXTDISKSIZE, "Disk Size") == false) {return false;}
	if (ValidateTextField(IDC_AVAILNEWDRIVES, "Drive Letter") == false) {return false;}

	return true; // If we got here then all of the validation succeeded.
}

bool AddDriveWindow::SaveSingleFile ()
{
	ZeroMemory (m_szOutputfile, SIZE_STRING);

	char szFileonly[SIZE_STRING];
	ZeroMemory (szFileonly, SIZE_STRING);

	char szPathonly[SIZE_STRING];
	ZeroMemory (szPathonly, SIZE_STRING);

	strcpy_s (szFileonly, SIZE_STRING, "EncryptedDisk.evd");	
	strcpy_s (m_szOutputfile, SIZE_STRING, "EncryptedDisk.evd");
	
	OPENFILENAME ofn;
	
	ZeroMemory(&ofn, sizeof(ofn));
	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = m_hwnd;
	ofn.lpstrFile = m_szOutputfile;
	//ofn.lpstrFile[0] = '\0';
	ofn.nMaxFile = SIZE_STRING;

	ofn.lpstrFilter = "Encrypted Virtual Drives (*.evd)\0*.evd\0All Files (*.*)\0*.*\0";
	
	ofn.nFilterIndex = 1;
	ofn.lpstrFileTitle = szFileonly;
	ofn.nMaxFileTitle = SIZE_STRING;
	ofn.lpstrInitialDir = szPathonly;	
	ofn.Flags = OFN_OVERWRITEPROMPT;
	
	if (GetSaveFileName (&ofn) != 0) {
		return true;
	} else {
		return false;
	}

}

void AddDriveWindow::OutputInt (LPCSTR lpszText, int iValue)
{
	if (m_bUseDiagnostics == true) {
		m_pdiag->OutputInt (lpszText, iValue);
	}
}

void AddDriveWindow::OutputText (LPCSTR lpszText)
{
	if (m_bUseDiagnostics == true) {
		m_pdiag->OutputText (lpszText);
	}
}

void AddDriveWindow::OutputText (LPCSTR lpszName, LPCSTR lpszValue)
{
	if (m_bUseDiagnostics == true) {
		m_pdiag->OutputText (lpszName, lpszValue);
	}
}

SingleDriveInfo AddDriveWindow::GetDriveInfo ()
{
	return m_driveinfo;
}

bool AddDriveWindow::GetRecoveryChoice ()
{
	return m_bCreaterecovery;
}

void AddDriveWindow::SetDriveInfo ()
{
	GetDlgItemText (m_hwnd, CID_TXTDRIVENAME, m_driveinfo.szName, SIZE_STRING);
	GetDlgItemText (m_hwnd, CID_TXTDRIVEDESCRIPTION, m_driveinfo.szDescription, SIZE_STRING);
	GetDlgItemText (m_hwnd, CID_TXTDRIVEPATH, m_driveinfo.szPath, SIZE_STRING);
	
	char szDisksize[SIZE_STRING];
	ZeroMemory (szDisksize, SIZE_STRING);
	GetDlgItemText (m_hwnd, CID_TXTDISKSIZE, szDisksize, SIZE_STRING);
	m_driveinfo.lDiskSizeMegs = atol (szDisksize);
	
	GetDlgItemText (m_hwnd, IDC_AVAILNEWDRIVES, m_driveinfo.szDriveLetter, SIZE_NAME);

	m_driveinfo.bMountStartup = m_bMountstartup;

}

void AddDriveWindow::OnCommand (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	switch (LOWORD (wParam)) {
		case CID_BTNADDOK:
			{
				if (ValidateInput() == true) {
					SetDriveInfo();

					if (m_baddexistingmode == true) {
						PostMessage (m_parenthwnd, WM_UICOMMAND, CC_UIEXISTINGDRIVEADDED, 0);
					} else {
						PostMessage (m_parenthwnd, WM_UICOMMAND, CC_UIDRIVEADDED, 0);
					}
					
					Hide();
				}
			}
			break;
		case CID_BTNADDCANCEL:
			{
				Hide();
			}
			break;
		case CID_BTNBROWSEPATH:
			{
				if (SaveSingleFile() == true) {
					SetDlgItemText(m_hwnd, CID_TXTDRIVEPATH, m_szOutputfile);
				}
			}
			break;
		case CID_OPTMOUNTSTARTUP:
			{
				if (m_bMountstartup == true) {
					m_bMountstartup = false;
					SetCheck(m_hwndmountstartup, false);
				} else {
					m_bMountstartup = true;
					SetCheck(m_hwndmountstartup, true);
				}
			}
			break;
		case CID_CHKCREATERECOVERY:
			{
				if (m_bCreaterecovery == true) {
					m_bCreaterecovery = false;
					SetCheck(m_hwndcreaterecovery, false);
				} else {
					m_bCreaterecovery = true;				
					SetCheck(m_hwndcreaterecovery, true);
				}
			}
			break;
	}
}

void AddDriveWindow::OnUIScroll (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	
}

void AddDriveWindow::OnUICommand (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	/*
	switch (wParam)
	{
		
	}
	*/
}

void AddDriveWindow::OnTimer (WPARAM wParam)
{	
	m_uihandler.NotifyTimer (wParam);	
}

void AddDriveWindow::OnPaint (HWND hWnd)
{
	m_uihandler.PaintControls (hWnd);	
}

void AddDriveWindow::OnMouseMove (HWND hWnd, int mouseXPos, int mouseYPos)
{	
	m_uihandler.NotifyMouseMove (mouseXPos, mouseYPos);
}

void AddDriveWindow::OnLButtonDown (HWND hWnd)
{
	m_uihandler.NotifyMouseDown ();
}

void AddDriveWindow::OnLButtonUp (HWND hWnd)
{
	m_uihandler.NotifyMouseUp ();
}