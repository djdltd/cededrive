// This work is dedicated to the Lord our God. King of Heaven, Lord of Heaven's Armies.

#include "SecureFileManager.h"

SecureFileManager::SecureFileManager ()
{
	m_bUseDiagnostics = true;
	ZeroMemory (m_szClassname, SIZE_STRING);
	m_ID = 0;
}

SecureFileManager::~SecureFileManager ()
{

}

void SecureFileManager::SetDiagnostics (Diagnostics *pdiag)
{
	m_pdiag = pdiag;
	
}

void SecureFileManager::Initialise (HWND hWnd, unsigned int uID)
{	
	// Make the ID global
	m_ID = uID;
	m_parenthwnd = hWnd;
	//m_hwnd = hWnd;

	

	// Temporary string value for uID
	char szID[SIZE_STRING];
	ZeroMemory (szID, SIZE_STRING);



	SetParentHWND (hWnd);
	SetBgColor (GetSysColor (COLOR_BTNFACE));
	SetCaption (TEXT ("Secure File Manager"));
	SetWindowStyle (FS_SIZEABLE);



	// Create the class name
	strcat_s (m_szClassname, SIZE_STRING, "SecureFileManagerWindow");
	sprintf_s (szID, SIZE_STRING, "%d", uID);
	strcat_s (m_szClassname, SIZE_STRING, szID);



	CreateAppWindow (m_szClassname, 70, 0, 910, 700, true);
	m_uihandler.SetWindowProperties (0, 0, 0, 0, RGB (230, 230, 240));
	SetWindowPosition (FS_CENTER);
	Show ();


}

void SecureFileManager::OnDestroy (HWND hWnd)
{			
	Hide ();
}

void SecureFileManager::OnCreate (HWND hWnd)
{				
	// Make this window handle global
	m_hwnd = hWnd;
	g_hWnd = hWnd; // Need to do this otherwise control wrappers don't work.



	// Create the toolbars
	m_toolbarsource.CreateToolbar (&m_uihandler, hWnd, 7, 30, CID_SOURCETOOLBAR);
	m_toolbardest.SetDeleteButtonVisible (false);
	m_toolbardest.CreateToolbar (&m_uihandler, hWnd, 427, 30, CID_DESTTOOLBAR);



	// Create the text box which displays the path
	m_hwndtxtsource = CreateReadonlyTextBox (57, 70, 330, 19, ID_TXTSOURCEPATH);
	m_hwndtxtdest = CreateReadonlyTextBox (477, 70, 330, 19, ID_TXTDESTPATH);



	// Create the drop down boxes used to show the available drives
	HFONT hfDefault = (HFONT) GetStockObject (DEFAULT_GUI_FONT);
	m_hwnddrivesource = CreateWindow ("ComboBox", NULL, WS_CHILD | WS_VISIBLE | WS_BORDER | CBS_DROPDOWN, 7, 70, 49, 19, hWnd, (HMENU) ID_DRIVELISTSOURCE, GetModuleHandle (NULL), NULL);
	SendMessage (m_hwnddrivesource, WM_SETFONT, (WPARAM) hfDefault, MAKELPARAM (FALSE, 0));



	m_hwnddrivedest = CreateWindow ("ComboBox", NULL, WS_CHILD | WS_VISIBLE | WS_BORDER | CBS_DROPDOWN, 427, 70, 49, 19, hWnd, (HMENU) ID_DRIVELISTDEST, GetModuleHandle (NULL), NULL);
	SendMessage (m_hwnddrivedest, WM_SETFONT, (WPARAM) hfDefault, MAKELPARAM (FALSE, 0));



	// Create the list view controls
	m_hwndlistsource = CreateWindow (WC_LISTVIEW, "", WS_CHILD | WS_VISIBLE | LVS_REPORT | LVS_EDITLABELS, 7, 90, 380, 400, hWnd, (HMENU) ID_LISTVIEWSOURCE, GetModuleHandle (NULL), NULL);
	m_hwndlistdest = CreateWindow (WC_LISTVIEW, "", WS_CHILD | WS_VISIBLE | LVS_REPORT | LVS_EDITLABELS, 427, 90, 380, 400, hWnd, (HMENU) ID_LISTVIEWDEST, GetModuleHandle (NULL), NULL);
	


	// The move button
	m_btnMove.SetBitmapResources (true, IDB_BTNEXTRACTNORM, IDB_BTNEXTRACTHIGH, IDB_BTNEXTRACTDOWN);
	m_btnMove.SetBitmapProperties (35, 32);
	m_btnMove.SetProperties (hWnd, ID_SECUREMOVE, 390, 200, 35, 32);
	m_btnMove.SetHighlightProperties (true, false);	
	m_uihandler.AddDirectControl (&m_btnMove);


	// Progress bar at the bottom of the window
	m_hwndprogress  = CreateWindowEx(0L, PROGRESS_CLASS, "", WS_CHILD | WS_VISIBLE, 60, 500, 400, 22, hWnd, (HMENU) ID_COPYPROGRESS, GetModuleHandle (NULL), NULL);
	


	// Create the status label
	m_hwndlblstatus = CreateLabel ("Securely Move files to your Encrypted Virtual Drives ensuring the source data is securely deleted.", 60, 525, 400, 19, ID_LBLSTATUS);


	// Create the bitmap labels
	m_header.SetBitmapResources (IDB_HEADER);
	m_header.SetBitmapProperties (0, 0, 2600, 27);
	m_header.SetProperties (hWnd, CID_HEADER, 0, 0, 2600, 27);
	m_uihandler.AddDirectControl (&m_header);



	m_sourcelabel.SetBitmapResources (IDB_SOURCEFOLDERLABEL);
	m_sourcelabel.SetBitmapProperties (0, 0, 90, 32);
	m_sourcelabel.SetProperties (hWnd, CID_HEADER, 7, 500, 90, 32);
	m_uihandler.AddDirectControl (&m_sourcelabel);



	m_destlabel.SetBitmapResources (IDB_DESTFOLDERLABEL);
	m_destlabel.SetBitmapProperties (0, 0, 90, 32);
	m_destlabel.SetProperties (hWnd, CID_HEADER, 800, 500, 90, 32);
	m_uihandler.AddDirectControl (&m_destlabel);



	// Prepare both list view controls
	PrepareListView (m_hwndlistsource);
	PrepareListView (m_hwndlistdest);



	ZeroMemory (m_szCurrentsourcefolder, SIZE_STRING);
	ZeroMemory (m_szCurrentdestfolder, SIZE_STRING);



	// Get and populate the available drives for the drop down lists
	GetAvailableDrives ();



	char szHomedrive[SIZE_NAME];
	ZeroMemory (szHomedrive, SIZE_NAME);
	GetEnvironmentVariable ("HOMEDRIVE", szHomedrive, SIZE_NAME);



	char szSystemdrive[SIZE_NAME];
	ZeroMemory (szSystemdrive, SIZE_NAME);
	GetEnvironmentVariable ("SystemDrive", szSystemdrive, SIZE_NAME);



	if (strlen(szHomedrive) > 0) {
		RefreshSourceList (szHomedrive);
		RefreshDestList (szHomedrive);



		PopulateAvailDrives (m_hwnddrivesource, szHomedrive);
		PopulateAvailDrives (m_hwnddrivedest, szHomedrive);

	} else {
		RefreshSourceList (szSystemdrive);
		RefreshDestList (szSystemdrive);



		PopulateAvailDrives (m_hwnddrivesource, szSystemdrive);
		PopulateAvailDrives (m_hwnddrivedest, szSystemdrive);
	}



	m_movepopup.Initialise (hWnd, 0);

	m_movepopup.SetDiagnostics (m_pdiag);

	m_fileshredder.SetDiagnostics (m_pdiag);

	m_fileshredder.InitRandom ();


	SetTimer (hWnd, IDT_SECUREMOVEWELCOME, 900, NULL);
}

void SecureFileManager::PrepareListView (HWND listviewhwnd)
{
	// Create the image lists
    HICON hiconSmall;     // icon for list-view items
	HICON hiconLarge;     // icon for list-view items
	HICON hiconFolder;	  // icon for folder items
	HICON hiconLargefolder; // icon for large folder items;
	HICON hiconLargePlainfile; // icon for plain file
	HICON hiconSmallPlainfile; // icon for plain file

	// Large icon image list
	m_hLarge = ImageList_Create(32, 32, ILC_COLOR32 , 1, 1); 
    // Small Icon image list
	m_hSmall = ImageList_Create(16, 16, ILC_COLOR32 , 1, 1); 

    hiconSmall = LoadIcon(GetModuleHandle (NULL), MAKEINTRESOURCE(ID_LVICONSMALL));
	hiconLarge = LoadIcon(GetModuleHandle (NULL), MAKEINTRESOURCE(ID_LVICONLARGE));
	hiconFolder = LoadIcon (GetModuleHandle (NULL), MAKEINTRESOURCE(ID_LVICONFOLDER));
	hiconLargefolder = LoadIcon (GetModuleHandle (NULL), MAKEINTRESOURCE (ID_LVICONLARGEFOLDER));
	hiconLargePlainfile = LoadIcon (GetModuleHandle (NULL), MAKEINTRESOURCE (ID_LVICONPLAINFILE));
	hiconSmallPlainfile = LoadIcon (GetModuleHandle (NULL), MAKEINTRESOURCE (ID_LVICONPLAINFILE));

    ImageList_AddIcon(m_hLarge, hiconLarge);
	ImageList_AddIcon(m_hLarge, hiconLargefolder);   	
	ImageList_AddIcon(m_hLarge, hiconLargePlainfile);
	
	ImageList_AddIcon(m_hSmall, hiconSmall);
	ImageList_AddIcon(m_hSmall, hiconFolder);	
	ImageList_AddIcon(m_hSmall, hiconSmallPlainfile);
	
	DestroyIcon(hiconLarge);
	DestroyIcon(hiconLargePlainfile);
	DestroyIcon(hiconSmall); 
	DestroyIcon(hiconSmallPlainfile);
	
    ListView_SetImageList(listviewhwnd, m_hLarge, LVSIL_NORMAL); 
    ListView_SetImageList(listviewhwnd, m_hSmall, LVSIL_SMALL); 

	// Add columns to the list view
	char szColname[SIZE_NAME];
	ZeroMemory (szColname, SIZE_NAME);
	strcpy_s (szColname, SIZE_NAME, "File Name");

	char szColname2[SIZE_NAME];
	ZeroMemory (szColname2, SIZE_NAME);
	strcpy_s (szColname2, SIZE_NAME, "File Size");

	char szColname3[SIZE_NAME];
	ZeroMemory (szColname3, SIZE_NAME);
	strcpy_s (szColname3, SIZE_NAME, "Date Modified");

	LVCOLUMN lvc;
	int iCol = 0;

	lvc.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM; 
	
    lvc.iSubItem = 0;
    lvc.pszText = szColname;	
    lvc.cx = 200;     // width of column in pixels
	lvc.fmt = LVCFMT_LEFT;  // left-aligned column
	
	ListView_InsertColumn(listviewhwnd, 0, &lvc);

	lvc.iSubItem = 1;
	lvc.pszText = szColname2;	
	lvc.cx = 100;     // width of column in pixels
	ListView_InsertColumn(listviewhwnd, 1, &lvc);

	lvc.iSubItem = 2;
	lvc.pszText = szColname3;	
	lvc.cx = 100;     // width of column in pixels
	ListView_InsertColumn(listviewhwnd, 2, &lvc);
}

void SecureFileManager::ChangeSourceDrive ()
{
	char szDrive[SIZE_NAME];
	ZeroMemory (szDrive, SIZE_NAME);

	GetDlgItemText (m_hwnd, ID_DRIVELISTSOURCE, szDrive, SIZE_NAME);

	RefreshSourceList (szDrive);
}

void SecureFileManager::ChangeDestDrive ()
{
	char szDrive[SIZE_NAME];
	ZeroMemory (szDrive, SIZE_NAME);

	GetDlgItemText (m_hwnd, ID_DRIVELISTDEST, szDrive, SIZE_NAME);

	RefreshDestList (szDrive);	
}

void SecureFileManager::RefreshSourceList (char *szDir)
{
	RefreshListView (szDir, m_hwndlistsource, &m_dlsourcefiles, ID_TXTSOURCEPATH, 2);

	//ZeroMemory (m_szCurrentsourcefolder, SIZE_STRING);
	strcpy_s (m_szCurrentsourcefolder, SIZE_STRING, szDir);
}

void SecureFileManager::RefreshDestList (char *szDir)
{
	RefreshListView (szDir, m_hwndlistdest, &m_dldestfiles, ID_TXTDESTPATH, 0);

	//ZeroMemory (m_szCurrentdestfolder, SIZE_STRING);
	strcpy_s (m_szCurrentdestfolder, SIZE_STRING, szDir);
}

void SecureFileManager::RefreshCurrentSource ()
{
	RefreshSourceList (m_szCurrentsourcefolder);
}

void SecureFileManager::RefreshCurrentDest ()
{
	RefreshDestList (m_szCurrentdestfolder);
}

void SecureFileManager::RefreshListView (char *szDir, HWND listviewhwnd, DynList *dlFilelist, int PathControlID, int fileiconid)
{
	
	// Now add a few items to the list view
	LVITEM lvI;
	int index = 0;
	SingleFileInfo *pinfo;

	ListView_DeleteAllItems (listviewhwnd);
	dlFilelist->Clear ();

	SetDlgItemText (m_hwnd, PathControlID, szDir);

	m_Dirscanner.ListAllRootFilesEx (szDir, dlFilelist);

	lvI.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM | LVIF_STATE; 
	lvI.state = 0; 
	lvI.stateMask = 0; 	

	for (index=0;index < dlFilelist->GetNumItems ();index++) {
		pinfo = (SingleFileInfo *) dlFilelist->GetItem (index);

		lvI.iItem = index;
		if (pinfo->bIsDir == true) {
			lvI.iImage = 1;
		} else {
			lvI.iImage = fileiconid;
		}
		
		lvI.iSubItem = 0;
		lvI.lParam = 0;
		lvI.pszText = LPSTR_TEXTCALLBACK;
		ListView_InsertItem(listviewhwnd, &lvI);
	}
}

void SecureFileManager::OutputInt (LPCSTR lpszText, int iValue)
{
	if (m_bUseDiagnostics == true) {
		m_pdiag->OutputInt (lpszText, iValue);
	}
}

void SecureFileManager::OutputText (LPCSTR lpszText)
{
	if (m_bUseDiagnostics == true) {
		m_pdiag->OutputText (lpszText);
	}
}

void SecureFileManager::OutputText (LPCSTR lpszName, LPCSTR lpszValue)
{
	if (m_bUseDiagnostics == true) {
		m_pdiag->OutputText (lpszName, lpszValue);
	}
}

LRESULT SecureFileManager::OnClose (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	Hide();
	return 0;
}

void SecureFileManager::OnNotify (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	switch (wParam) {
		
		/////////////////////////////////
		/// DESTINATION LIST VIEW
		/////////////////////////////////
		case ID_LISTVIEWDEST:
		{
			switch (((LPNMHDR) lParam)->code)
			{
				case NM_DBLCLK:
				{
					//m_pdiag->OutputText ("Double Click on ListView!");

					SingleFileInfo *pinfo;

					int iCuritem = LV_GetFirstSelectedIndex (m_hwndlistdest);

					if (iCuritem != -1) { // If something was selected
					
						pinfo = (SingleFileInfo *) m_dldestfiles.GetItem (iCuritem);

						if (pinfo->bIsDir == true) {

							m_dlDestfolderhistory.AddItem (m_szCurrentdestfolder, SIZE_STRING, false);

							strcat_s (m_szCurrentdestfolder, SIZE_STRING, "\\");
							strcat_s (m_szCurrentdestfolder, SIZE_STRING, pinfo->szName);

							RefreshDestList (m_szCurrentdestfolder);

						}
					}

				}
				break;
				case LVN_GETDISPINFO:
				{
					NMLVDISPINFO *lpinfo;
					SingleFileInfo *pfinfo;

					lpinfo = (NMLVDISPINFO *) lParam;

					pfinfo = (SingleFileInfo *) m_dldestfiles.GetItem (lpinfo->item.iItem);

					switch (lpinfo->item.iSubItem)
					{
						case 0:
						{
							lpinfo->item.pszText = pfinfo->szName;
						}
						break;

						case 1:
						{
							lpinfo->item.pszText = pfinfo->szFilesize;
						}
						break;

						case 2:
						{
							lpinfo->item.pszText = pfinfo->szDatemodified;
						}
						break;
					}

				}
				break;
			}		
		}
		break;

		/////////////////////////////////
		/// SOURCE LIST VIEW
		/////////////////////////////////
		case ID_LISTVIEWSOURCE:
			{
				switch (((LPNMHDR) lParam)->code)
				{
					case NM_DBLCLK:
					{
						//m_pdiag->OutputText ("Double Click on ListView!");

						SingleFileInfo *pinfo;

						int iCuritem = LV_GetFirstSelectedIndex (m_hwndlistsource);

						if (iCuritem != -1) { // If something was selected
						
							pinfo = (SingleFileInfo *) m_dlsourcefiles.GetItem (iCuritem);

							if (pinfo->bIsDir == true) {

								m_dlSourcefolderhistory.AddItem (m_szCurrentsourcefolder, SIZE_STRING, false);

								strcat_s (m_szCurrentsourcefolder, SIZE_STRING, "\\");
								strcat_s (m_szCurrentsourcefolder, SIZE_STRING, pinfo->szName);

								RefreshSourceList (m_szCurrentsourcefolder);

							}
						}

					}
					break;
					case LVN_GETDISPINFO:
					{
						NMLVDISPINFO *lpinfo;
						SingleFileInfo *pfinfo;

						lpinfo = (NMLVDISPINFO *) lParam;

						pfinfo = (SingleFileInfo *) m_dlsourcefiles.GetItem (lpinfo->item.iItem);

						switch (lpinfo->item.iSubItem)
						{
							case 0:
							{
								lpinfo->item.pszText = pfinfo->szName;
							}
							break;

							case 1:
							{
								lpinfo->item.pszText = pfinfo->szFilesize;
							}
							break;

							case 2:
							{
								lpinfo->item.pszText = pfinfo->szDatemodified;
							}
							break;
						}

					}
					break;
				}		
			}
			break;
	}

	
}

void SecureFileManager::OnCommand (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	switch (LOWORD (wParam)) {
		case ID_DRIVELISTSOURCE:
		{
			switch (HIWORD (wParam)) {
				case CBN_SELENDOK:
				{
					SetTimer (m_hwnd, IDT_DRIVESOURCECHANGE, 100, NULL);
					//ChangeSourceDrive ();
				}
				break;
			}
		}
		break;

		case ID_DRIVELISTDEST:
		{
			switch (HIWORD (wParam)) {
				case CBN_SELENDOK:
				{
					SetTimer (m_hwnd, IDT_DRIVEDESTCHANGE, 100, NULL);
					//ChangeDestDrive ();
				}
				break;
			}
		}
		break;

	}
}

void SecureFileManager::OnSize (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	int newwidth = LOWORD(lParam);
	int newheight = HIWORD (lParam);

	MoveWindow (m_hwndlistsource, 7, 90, (newwidth/2)-20, newheight-160, TRUE);
	m_btnMove.SetPosition ((newwidth/2)-10, (newheight/2));
	MoveWindow (m_hwndlistdest, (newwidth/2)+28, 90, (newwidth/2)-40, newheight-160, TRUE);
	
	MoveWindow (m_hwndtxtsource, 57, 69, (newwidth/2)-70, 20, TRUE);
	MoveWindow (m_hwndtxtdest, (newwidth/2)+78, 69, (newwidth/2)-90, 20, TRUE);
	
	MoveWindow (m_hwnddrivesource, 7, 69, 49, 19, TRUE);
	MoveWindow (m_hwnddrivedest, (newwidth/2)+28, 69, 49, 19, TRUE);
	

	RECT rectstatus;
	rectstatus.left = 110;
	rectstatus.right = 110+(newwidth-220);
	rectstatus.top = newheight-33;
	rectstatus.bottom = (newheight-33)+35;

	InvalidateRect (m_hwnd, &rectstatus, TRUE);

	MoveWindow (m_hwndprogress, 110, newheight-57, (newwidth-220), 22, TRUE);
	MoveWindow (m_hwndlblstatus, 110, newheight-33, (newwidth-220), 35, TRUE);

	m_toolbardest.SetPosition ((newwidth/2)+28, 30);

	m_sourcelabel.SetPosition (7, newheight-61);
	m_destlabel.SetPosition (newwidth-100, newheight-61);

}

void SecureFileManager::OnUIScroll (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	
}

void SecureFileManager::OnUICommand (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	if (wParam == CID_BTNPARENT+CID_SOURCETOOLBAR) {

		char szlastsourceitem[SIZE_STRING];
		ZeroMemory (szlastsourceitem, SIZE_STRING);

		if (PopLastItem (szlastsourceitem, &m_dlSourcefolderhistory) == true) {
			ZeroMemory (m_szCurrentsourcefolder, SIZE_STRING);
			strcpy_s (m_szCurrentsourcefolder, SIZE_STRING, szlastsourceitem);

			RefreshSourceList (m_szCurrentsourcefolder);
		}
	}

	if (wParam == CID_BTNPARENT+CID_DESTTOOLBAR) {
		
		char szlastdestitem[SIZE_STRING];
		ZeroMemory (szlastdestitem, SIZE_STRING);

		if (PopLastItem (szlastdestitem, &m_dlDestfolderhistory) == true) {
			ZeroMemory (m_szCurrentdestfolder, SIZE_STRING);
			strcpy_s (m_szCurrentdestfolder, SIZE_STRING, szlastdestitem);

			RefreshDestList (m_szCurrentdestfolder);
		}

	}

	if (wParam == ID_SECUREMOVE) {		
		if (LV_GetSelectedCount (m_hwndlistsource) > 0) {
			DoScanandCopy ();
		} else {
			MessageBox (m_hwnd, "Please select some files or folders from the list of files on the left hand side.", "No files or folders selected", MB_OK | MB_ICONINFORMATION);
		}
	}


	if (wParam == CID_BTNICONVIEW+CID_SOURCETOOLBAR) {
		SetWindowLong (m_hwndlistsource, GWL_STYLE, WS_CHILD | WS_VISIBLE | LVS_ICON | LVS_EDITLABELS);
		return;
	}

	if (wParam == CID_BTNLISTVIEW+CID_SOURCETOOLBAR) {
		SetWindowLong (m_hwndlistsource, GWL_STYLE, WS_CHILD | WS_VISIBLE | LVS_LIST | LVS_EDITLABELS);
		return;
	}

	if (wParam == CID_BTNDETAILVIEW+CID_SOURCETOOLBAR) {
		SetWindowLong (m_hwndlistsource, GWL_STYLE, WS_CHILD | WS_VISIBLE | LVS_REPORT | LVS_EDITLABELS);
		return;
	}

	if (wParam == CID_BTNDELETE+CID_SOURCETOOLBAR) {
		
		if (LV_GetSelectedCount (m_hwndlistsource) > 0) {
			//DoScanandCopy ();
			if (MessageBox (m_hwnd, "The files you have selected will be securely shredded and deleted making recovery impossible.\n\nAre you sure you wish to proceed?", "Are you sure?", MB_YESNO | MB_ICONQUESTION) == IDYES) {
				DoShredOnly();
			}
			
		} else {
			MessageBox (m_hwnd, "Please select some files or folders from the list of files on the left hand side.", "No files or folders selected", MB_OK | MB_ICONINFORMATION);
		}
		return;

	}

	if (wParam == CID_BTNICONVIEW+CID_DESTTOOLBAR) {
		SetWindowLong (m_hwndlistdest, GWL_STYLE, WS_CHILD | WS_VISIBLE | LVS_ICON | LVS_EDITLABELS);
		return;
	}

	if (wParam == CID_BTNLISTVIEW+CID_DESTTOOLBAR) {
		SetWindowLong (m_hwndlistdest, GWL_STYLE, WS_CHILD | WS_VISIBLE | LVS_LIST | LVS_EDITLABELS);
		return;
	}

	if (wParam == CID_BTNDETAILVIEW+CID_DESTTOOLBAR) {
		SetWindowLong (m_hwndlistdest, GWL_STYLE, WS_CHILD | WS_VISIBLE | LVS_REPORT | LVS_EDITLABELS);
		return;
	}


}

void SecureFileManager::OnTimer (WPARAM wParam)
{	
	m_uihandler.NotifyTimer (wParam);	

	switch (wParam) {
		case IDT_DRIVESOURCECHANGE:
		{
			ChangeSourceDrive ();
			KillTimer (m_hwnd, IDT_DRIVESOURCECHANGE);
		}
		break;
		case IDT_DRIVEDESTCHANGE:
		{
			ChangeDestDrive ();
			KillTimer (m_hwnd, IDT_DRIVEDESTCHANGE);
		}
		break;
		case IDT_SECUREMOVEWELCOME:
		{
			KillTimer (m_hwnd, IDT_SECUREMOVEWELCOME);
			MessageBox (m_hwnd, "Welcome to Secure Move\n\nSecure Move allows you to securely move files and folders to your Encrypted Drives whilst ensuring the original files and folders are securely deleted using DOD standards for secure file deletion.\n\nThis ensures any attempt to recover your original data will be impossible even if using third party recovery tools", "Secure Move Information", MB_OK | MB_ICONINFORMATION);
		}
		break;
	}
}

void SecureFileManager::OnPaint (HWND hWnd)
{
	m_uihandler.PaintControls (hWnd);	
}

void SecureFileManager::OnMouseMove (HWND hWnd, int mouseXPos, int mouseYPos)
{	
	m_uihandler.NotifyMouseMove (mouseXPos, mouseYPos);
}

void SecureFileManager::OnLButtonDown (HWND hWnd)
{
	m_uihandler.NotifyMouseDown ();
}

void SecureFileManager::OnLButtonUp (HWND hWnd)
{
	m_uihandler.NotifyMouseUp ();
}

void SecureFileManager::UpdateStatusLabel (char *szCaption)
{
	SetDlgItemText (m_hwnd, ID_LBLSTATUS, szCaption);
}

// Cross thread accessor functions
HWND SecureFileManager::GetHWNDSourceList ()
{
	return m_hwndlistsource;
}

DynList *SecureFileManager::GetSourceFileList ()
{
	return &m_dlsourcefiles;
}

void SecureFileManager::EnableControls(bool bEnabled)
{
	EnableWindow (m_hwndlistsource, bEnabled);
	EnableWindow (m_hwndlistdest, bEnabled);
	m_btnMove.SetEnabled (bEnabled);
	m_toolbarsource.SetEnabled (bEnabled);
	m_toolbardest.SetEnabled (bEnabled);

	if (bEnabled == false) {
		m_movepopup.Show ();
	} else {
		m_movepopup.Hide ();
		SendMessage (m_hwndprogress, PBM_SETPOS, 0, 0L);
	}
}

DWORD WINAPI SecureFileManager::ShredonlyProc (PVOID pParam)
{
	// The worker thread in which the scanning and file copy takes place	
	SecureFileManager *pmainwnd = (SecureFileManager *) pParam;
	
	unsigned long lnumerrors = 0;
	// Disable all the controls
	pmainwnd->EnableControls (false);

	pmainwnd->LV_GetAllSelectedFiles (pmainwnd->GetHWNDSourceList (), pmainwnd->GetSourceFileList ());
	lnumerrors = pmainwnd->ShredAll ();

	if (lnumerrors == 0) {
		// Only remove the source folders and files if there were no errors
		pmainwnd->LV_DeleteAllSelectedFiles (pmainwnd->GetHWNDSourceList (), pmainwnd->GetSourceFileList());
	}

	pmainwnd->EnableControls (true);
	pmainwnd->RefreshCurrentSource ();
	pmainwnd->RefreshCurrentDest ();

	return 0;
}

DWORD WINAPI SecureFileManager::ScanandCopyProc (PVOID pParam)
{
	// The worker thread in which the scanning and file copy takes place	
	SecureFileManager *pmainwnd = (SecureFileManager *) pParam;
	
	unsigned long lnumerrors = 0;
	// Disable all the controls
	pmainwnd->EnableControls (false);

	pmainwnd->LV_GetAllSelectedFiles (pmainwnd->GetHWNDSourceList (), pmainwnd->GetSourceFileList ());
	lnumerrors = pmainwnd->CopyAll ();

	if (lnumerrors == 0) {
		// Only remove the source folders and files if there were no errors
		pmainwnd->LV_DeleteAllSelectedFiles (pmainwnd->GetHWNDSourceList (), pmainwnd->GetSourceFileList());
	}

	pmainwnd->EnableControls (true);
	pmainwnd->RefreshCurrentSource ();
	pmainwnd->RefreshCurrentDest ();

	return 0;
}

void SecureFileManager::DoScanandCopy ()
{
	// Creates a separate thread for the scanning and copying of the files, and then the shredding of the source files
	DWORD dwThreadID;
	m_hCopythread = CreateThread (NULL, 0, ScanandCopyProc, (SecureFileManager *) this, 0, &dwThreadID);
}

void SecureFileManager::DoShredOnly ()
{
	// Creates a separate thread for the shredding of the files, and then the shredding of the source files
	DWORD dwThreadID;
	m_hCopythread = CreateThread (NULL, 0, ShredonlyProc, (SecureFileManager *) this, 0, &dwThreadID);
}

int SecureFileManager::ShredAll ()
{
	// Go through each resultant file in the result file list and copy each one
	// The dlResultfiles is populated by the LV_GetAllSelected function because this recursively scans for files in any dirs
	// that have been selected so by the time we get here we have a nice big list of all files to be coped including all their source paths

	long f = 0;
	SingleFileInfo *pinfo;
	unsigned long lErrors = 0;

	for (f=0;f<m_dlResultfiles.GetNumItems();f++)  {
		pinfo = (SingleFileInfo *) m_dlResultfiles.GetItem (f);
		
		if (ShredSingleFile (pinfo->szFilepath, pinfo->lFilesize) == false) {
			lErrors++;
		}		
	}

	UpdateStatusLabel ("File shredding has completed.");

	// When we're done, clean up
	m_dlResultfiles.Clear ();

	return lErrors;
}

int SecureFileManager::CopyAll ()
{
	// Go through each resultant file in the result file list and copy each one
	// The dlResultfiles is populated by the LV_GetAllSelected function because this recursively scans for files in any dirs
	// that have been selected so by the time we get here we have a nice big list of all files to be coped including all their source paths

	long f = 0;
	SingleFileInfo *pinfo;
	unsigned long lErrors = 0;

	for (f=0;f<m_dlResultfiles.GetNumItems();f++)  {
		pinfo = (SingleFileInfo *) m_dlResultfiles.GetItem (f);

		if (CopySingleFile (pinfo->szFilepath, m_szCurrentsourcefolder, m_szCurrentdestfolder, pinfo->lFilesize) == false) {
			lErrors++;
		}		
	}

	UpdateStatusLabel ("File copy has completed.");

	// When we're done, clean up
	m_dlResultfiles.Clear ();

	return lErrors;
}

void SecureFileManager::UpdateCopyProgress (unsigned long long bytestransferred)
{
	unsigned long long lltotal = m_lltotalbytescopied+bytestransferred;
	

	int percentprogress = (lltotal * 39500) / m_llsizeallfiles;

	OutputInt ("Percentprogress: ", percentprogress);
	OutputInt ("total bytes copied: ", m_lltotalbytescopied);
	OutputInt ("size all fiels: ", m_llsizeallfiles);

	SendMessage (m_hwndprogress, PBM_SETPOS, percentprogress, 0L);
}

DWORD CALLBACK SecureFileManager::CopyProgressRoutine (LARGE_INTEGER TotalFileSize, LARGE_INTEGER TotalBytesTransferred, LARGE_INTEGER StreamSize, LARGE_INTEGER StreamBytesTransferred, DWORD dwStreamNumber, DWORD dwCallbackReason, HANDLE hSourceFile, HANDLE hDestinationFile, LPVOID lpData)
{
	// Call back function called by windows whenever the copy file function updates the progress
	// of the file copy. We can then use the information supplied to update the progress bar.

	SecureFileManager *pmainwnd = (SecureFileManager *) lpData;

	unsigned long long lTotalsize = TotalFileSize.QuadPart;
	unsigned long long lBytescopied = TotalBytesTransferred.QuadPart;
	char szProgresscaption[SIZE_STRING];

	if (lTotalsize > 0) {

		// 13723 / 100 * 75

		// 10123 / 13723 * 100
		// progress / totalsize * pixelsize;

		//int pixprogress = (lBytescopied * 430) / lTotalsize;
		//int percentprogress = (lBytescopied * 100) / lTotalsize;

		//ZeroMemory (szProgresscaption, SIZE_STRING);
		//sprintf_s (szProgresscaption, SIZE_STRING, "Moving...%d%%", percentprogress);

		//pmainwnd->m_pdiag->OutputInt ("Pix progress: ", pixprogress);

		pmainwnd->UpdateCopyProgress (lBytescopied);
	}

	return 0;
}

bool SecureFileManager::ShredSingleFile (char *szSourcefile, unsigned long lFilesize)
{
	char szMessage[SIZE_STRING];
	ZeroMemory (szMessage, SIZE_STRING);
	strcpy_s (szMessage, SIZE_STRING, "Shredding file ");
	strcat_s (szMessage, SIZE_STRING, szSourcefile);
	strcat_s (szMessage, SIZE_STRING, "...");
	UpdateStatusLabel (szMessage);
	
	//m_fileshredder.SecureDelete (szSourcefile);
	m_securedelete.SecureDeleteFile (szSourcefile);
	
	UpdateCopyProgress (lFilesize);
	
	m_lltotalbytescopied+=lFilesize;

	return true;
}

bool SecureFileManager::FileExists (char *FileName)
{
	// Check if the file exists		
	struct _finddata_t c_file;
	long hFile;

	// Build the list of files in the source path
	if( (hFile = _findfirst(FileName, &c_file )) == -1L ) {
		return false;
	} else {
		return true;
	}

	return false;
}

bool SecureFileManager::CopySingleFile (char *szSourcefile, char *szRootpath, char *szDestpath, unsigned long lFilesize)
{
	// Root path is expected to be the path the copy started from e.g. C:\Program Files
	// Source path is expected to be something like C:\Program Files\Somedirectory\Somefile.txt
	// Dest path is expected to be something like D:\Data\SomeDirectory

	char szStrippedpath[SIZE_STRING];
	ZeroMemory (szStrippedpath, SIZE_STRING);

	m_Dirscanner.StripStartPath (szRootpath, szSourcefile, szStrippedpath);

	char szDestfile[SIZE_STRING];
	ZeroMemory (szDestfile, SIZE_STRING);
	strcpy_s (szDestfile, SIZE_STRING, m_szCurrentdestfolder);
	strcat_s (szDestfile, SIZE_STRING, szStrippedpath);

	char szDestfolder[SIZE_STRING];
	ZeroMemory (szDestfolder, SIZE_STRING);

	char szDestfileonly[SIZE_STRING];
	ZeroMemory (szDestfileonly, SIZE_STRING);
	m_Dirscanner.GetPathOnly (szDestfile, szDestfolder, szDestfileonly, "\\");
	
	// Dest file is now our destination file we need to copy to
	OutputText ("Dest File: ", szDestfile);

	// Destfolder is the folder that needs to be created on the destination before the file can be copied - the path needs to exist first!
	OutputText ("Dest Folder: ", szDestfolder);

	m_Dirscanner.CreateFolderEx (szDestfolder);

	// Now the destination folder exists we now need to actually copy the file using CopyFileEx
	LPBOOL bCancel = false;
	bool bResult = false;

	char szMessage[SIZE_STRING];
	ZeroMemory (szMessage, SIZE_STRING);
	strcpy_s (szMessage, SIZE_STRING, "Copying ");
	strcat_s (szMessage, SIZE_STRING, szSourcefile);
	strcat_s (szMessage, SIZE_STRING, "...");
	UpdateStatusLabel (szMessage);

	if (CopyFileEx (szSourcefile, szDestfile, CopyProgressRoutine, (SecureFileManager *) this, bCancel, NULL) != 0) {
		bResult = true;
		bool bFiledeleted = false;

		for (int d=0;d<5;d++) {

			if (bFiledeleted == false) {

				ZeroMemory (szMessage, SIZE_STRING);
				strcpy_s (szMessage, SIZE_STRING, "Shredding file ");
				strcat_s (szMessage, SIZE_STRING, szSourcefile);
				strcat_s (szMessage, SIZE_STRING, "...");
				UpdateStatusLabel (szMessage);
		
				//m_fileshredder.SecureDelete (szSourcefile);
				m_securedelete.SecureDeleteFile (szSourcefile);
		
				// Make sure the file does not exist, if it does then loop around trying to delete it again after waiting 1 second
				if (FileExists (szSourcefile) == false) {
					bFiledeleted = true;
				} else {
					Sleep (1000);
				}
			}
		}

	} else {		
		//pmainwnd->m_Singlefilecopylasterror = GetLastError ();		
		bResult = false;
	}

	m_lltotalbytescopied+=lFilesize;

	return bResult;
	//OutputText ("Strippedpath: ", szStrippedpath);
}

bool SecureFileManager::PopLastItem (char *szDestitem, DynList *dlHistory)
{
	// This function will retrieve the last item from the dynlist
	// and remove the last item from the dynlist. Treating a DynList as a kind
	// of stack

	char szItem[SIZE_STRING];
	ZeroMemory (szItem, SIZE_STRING);

	char szCuritem[SIZE_STRING];
	
	int i = 0;
	DynList dlTempitems;

	if (dlHistory->GetNumItems () > 0) {
		strcpy_s (szItem, SIZE_STRING, (char *) dlHistory->GetItem (dlHistory->GetNumItems ()-1));


		// Now remove the last item from the list
		for (i=0;i<dlHistory->GetNumItems ()-1;i++)
		{
			ZeroMemory (szCuritem, SIZE_STRING);
			strcpy_s (szCuritem, SIZE_STRING, (char *) dlHistory->GetItem (i));
			dlTempitems.AddItem (szCuritem, SIZE_STRING, false);			
		}

		dlHistory->Clear ();

		for (i=0;i<dlTempitems.GetNumItems ();i++) {
			ZeroMemory (szCuritem, SIZE_STRING);
			strcpy_s (szCuritem, SIZE_STRING, (char *) dlTempitems.GetItem (i));
			dlHistory->AddItem (szCuritem, SIZE_STRING, false);
		}

		dlTempitems.Clear ();

		ZeroMemory (szDestitem, SIZE_STRING);
		strcpy_s (szDestitem, SIZE_STRING, szItem);	

		return true;
	} else {
		return false;
	}
}

void SecureFileManager::DeletePath (char *szPath)
{
	char szSource[SIZE_STRING];
	ZeroMemory (szSource, SIZE_STRING);
	strcpy_s (szSource, SIZE_STRING, szPath);

	SHFILEOPSTRUCT Fileop;
	Fileop.hwnd = m_hwnd;
	Fileop.wFunc = FO_DELETE;
	Fileop.pFrom = szSource;
	Fileop.pTo = NULL;
	Fileop.fFlags = FOF_NOCONFIRMATION | FOF_NOERRORUI;

	SHFileOperation (&Fileop);
}



/////////////////////////////////////////
// LIST VIEW HELPER FUNCTIONS
/////////////////////////////////////////

int SecureFileManager::LV_GetSelectedCount (HWND hwndlv)
{
	int iNumitems = ListView_GetItemCount (hwndlv);
	int index = 0;
	int count = 0;
	LVITEM lvi;

	//m_pdiag->OutputInt ("ListView Item count is: ", iNumitems);

	for (index=0;index<iNumitems;index++) {
		lvi.mask = LVIF_STATE;
		lvi.stateMask = LVIS_SELECTED;
		lvi.iItem = index;
		lvi.iSubItem = 0;

		ListView_GetItem (hwndlv, &lvi);

		//m_pdiag->OutputText ("Item Text: ", lvi.pszText);
		if (lvi.state == LVIS_SELECTED) {
			count++;
		}
	}

	return count;
}

void SecureFileManager::LV_DeleteAllSelectedFiles (HWND hwndlv, DynList *dlFilelist)
{
	SingleFileInfo *pinfo;
	int iNumitems = ListView_GetItemCount (hwndlv);
	int index = 0;
	LVITEM lvi;	
	int r = 0;
	
	char szFilepath[SIZE_STRING];
	char szCurfilepath[SIZE_STRING];

	for (index=0;index<iNumitems;index++) {
		lvi.mask = LVIF_STATE;
		lvi.stateMask = LVIS_SELECTED;
		lvi.iItem = index;
		lvi.iSubItem = 0;

		ListView_GetItem (hwndlv, &lvi);

		if (lvi.state == LVIS_SELECTED) {
			pinfo = (SingleFileInfo *) dlFilelist->GetItem (index);

			DeletePath (pinfo->szFilepath);

			//if (m_Dirscanner.IsFolder (pinfo->szFilepath) == true) {
			//	m_Dirscanner.EmptyDirectory (pinfo->szFilepath);
			//	
			//	SetFileAttributes (pinfo->szFilepath, FILE_ATTRIBUTE_NORMAL);

			//	for (int a=0;a<10;a++) {
			//		RemoveDirectory (pinfo->szFilepath);
			//		Sleep (100);
			//	}
			//} else {
			//	
			//	SetFileAttributes (pinfo->szFilepath, FILE_ATTRIBUTE_NORMAL);

			//	for (int a=0;a<10;a++) {
			//		DeleteFile (pinfo->szFilepath);
			//		Sleep (100);
			//	}
			//	
			//}
		}
	}
}

void SecureFileManager::LV_GetAllSelectedFiles (HWND hwndlv, DynList *dlFilelist)
{
	SingleFileInfo *pinfo;
	SingleFileInfo *psubinfo;
	int iNumitems = ListView_GetItemCount (hwndlv);
	int index = 0;
	LVITEM lvi;
	DynList dlAllfiles;
	int r = 0;
	
	char szFilepath[SIZE_STRING];
	char szCurfilepath[SIZE_STRING];

	//m_wndenc.ClearFileInfo ();
	//m_pdiag->OutputInt ("ListView Item count is: ", iNumitems);
	
	m_dlResultfiles.Clear (); // Clear the final list containing our output;
	m_llsizeallfiles = 0;
	m_lltotalbytescopied = 0;


	SendMessage(m_hwndprogress, PBM_SETRANGE, 0L, MAKELONG (0, 39500));
	SendMessage (m_hwndprogress, PBM_SETSTEP, 1, 0L);
	SendMessage (m_hwndprogress, PBM_SETPOS, 0, 0L);

	for (index=0;index<iNumitems;index++) {
		lvi.mask = LVIF_STATE;
		lvi.stateMask = LVIS_SELECTED;
		lvi.iItem = index;
		lvi.iSubItem = 0;

		ListView_GetItem (hwndlv, &lvi);

		//m_pdiag->OutputText ("Item Text: ", lvi.pszText);
		if (lvi.state == LVIS_SELECTED) {
			pinfo = (SingleFileInfo *) dlFilelist->GetItem (index);

			if (m_Dirscanner.IsFolder (pinfo->szFilepath) == true) {
				dlAllfiles.Clear ();
				m_Dirscanner.ListAllFiles (pinfo->szFilepath, &dlAllfiles);
				

				// Now list out all the recursively scanned files
				for (r=0;r<dlAllfiles.GetNumItems ();r++) {
					
					psubinfo = (SingleFileInfo *) dlAllfiles.GetItem (r);
					
					// Add the item to the encryption window list
					//m_wndenc.AddFileInfo (psubinfo->szFilepath, pinfo->szFilepath, psubinfo->lFilesize, true);
					//OutputText (psubinfo->szFilepath);

					m_dlResultfiles.AddItem (psubinfo, sizeof (SingleFileInfo), false);
					m_llsizeallfiles+=psubinfo->lFilesize;

					//CopySingleFile (psubinfo->szFilepath, m_szCurrentsourcefolder, m_szCurrentdestfolder);
				}

			} else {
				//m_wndenc.AddFileInfo (pinfo->szFilepath, pinfo->lFilesize, false);
				//OutputText (pinfo->szFilepath);

				m_dlResultfiles.AddItem (pinfo, sizeof (SingleFileInfo), false);
				m_llsizeallfiles+=pinfo->lFilesize;

				//CopySingleFile (pinfo->szFilepath, m_szCurrentsourcefolder, m_szCurrentdestfolder);
			}
		}
	}

	dlAllfiles.Clear ();
}

int SecureFileManager::LV_GetFirstSelectedIndex (HWND hwndlv)
{
	int iNumitems = ListView_GetItemCount (hwndlv);
	int index = 0;
	LVITEM lvi;

	//m_pdiag->OutputInt ("ListView Item count is: ", iNumitems);

	for (index=0;index<iNumitems;index++) {
		lvi.mask = LVIF_STATE;
		lvi.stateMask = LVIS_SELECTED;
		lvi.iItem = index;
		lvi.iSubItem = 0;

		ListView_GetItem (hwndlv, &lvi);

		//m_pdiag->OutputText ("Item Text: ", lvi.pszText);
		if (lvi.state == LVIS_SELECTED) {
			return index;
		}
	}

	return -1;
}

//////////////////////////////////////
///// Available Drive Population
//////////////////////////////////////

void SecureFileManager::GetAvailableDrives ()
{
	// Function to scan the computer for all removable drives
	// for presenting to the user when deciding which drive to convert
	// to an encrypted disk.

	m_dlAvaildrives.Clear ();
	SingleFileInfo driveinfo;
	
	char szDrives[SIZE_STRING];
	ZeroMemory (szDrives, SIZE_STRING);

	char szCurDrive[SIZE_NAME];
	
	int a = 0;
	int l = 0;
	int len = 0;
	int count = 0;	

	len = GetLogicalDriveStrings (SIZE_STRING, szDrives);
	DWORD dwSizevolname = SIZE_STRING;

	for (a=0;a<len;a++) {
		if (szDrives[a] == 0) {
			ZeroMemory (szCurDrive, SIZE_NAME);
			strncpy (szCurDrive, szDrives+l, a-l-1);

			ZeroMemory (driveinfo.szRootdir, SIZE_STRING);			
			strcpy_s (driveinfo.szRootdir, SIZE_NAME, szCurDrive);
			m_dlAvaildrives.AddItem (&driveinfo, sizeof (SingleFileInfo), false);
			ZeroMemory (driveinfo.szRootdir, SIZE_NAME);			
			
			count++;
			l = a+1;
		}
	}	
}

void SecureFileManager::PopulateAvailDrives (HWND hwnddrivelist, char *szSelecteddrive)
{
	SingleFileInfo *pinfo;
	
	char szDrivename[SIZE_STRING];
	
	SendMessage (hwnddrivelist,CB_RESETCONTENT, 0, 0);

	char szSeldrive[SIZE_NAME];
	ZeroMemory (szSeldrive, SIZE_NAME);
	strcpy_s (szSeldrive, SIZE_NAME, szSelecteddrive);	

	int defaultindex = 0;

	for (int a=0;a<m_dlAvaildrives.GetNumItems ();a++) {
		pinfo = (SingleFileInfo *) m_dlAvaildrives.GetItem (a);

		ZeroMemory (szDrivename, SIZE_STRING);
		strcpy_s (szDrivename, SIZE_STRING, pinfo->szRootdir);

		if (strcmp (szDrivename, szSeldrive) == 0) {
			defaultindex = a;
		}

		SendMessage (hwnddrivelist, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) szDrivename);
	}

	if (m_dlAvaildrives.GetNumItems () == 0) {
		SendMessage (hwnddrivelist, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "--");
	}
		
	SendMessage (hwnddrivelist, CB_SETCURSEL, defaultindex, 0); // Set the current selection to the first item in the list

}