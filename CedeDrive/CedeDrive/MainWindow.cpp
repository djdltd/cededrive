// This work is dedicated to the Lord our God. King of Heaven, Lord of Heaven's Armies.

/*
Version Information

Version 2.75
	- When suspending or hibernating your computer, all the encrypted drives are dismounted when suspending, and all session key caches
		are securely erased from memory using a secure overwrite of the area in memory. When the computer it resumed, the password prompt
		is displayed allowing the user to remount their drives. Thus increasing CedeDrive security.
	
	- More secure management of session keys - removed the unnecessary creation of encrypted session keys in the client gui, only the service should know about
		the encrypted session key.

	- When the password window is displayed when the computer starts up, if the password window loses focus, then focus is automatically
	  regained. This ensures the cededrive password window is always shown on the screen when other applications are starting up on the computer.

	- Fixed an issue whereby when unmounting removable drives, the Icon would still be present in the drive list window. Now when unmounting removable drives
	  The icon is also removed from the drive list window.

    - When deleting virtual drives, the user is now given an additional prompt asking them if they want to delete the host file containing the data. Previously
	  this was done automatically.

	- Additional options have been provided to allow the user to choose which Explorer folders they would like to redirect to their encrypted drives. My Documents,
	  My Music, My Pictures and My Video can be redirected by right clicking on an Encrypted Drive in the CedeDrive Console Window and choosing Set as My Documents,
	  Set as My Pictures, etc...The changes take effect the next time the user logs into the computer (or explorer.exe restarted).

  -	  When the CedeDrive Manager has been quit, all encrypted drives are unmounted. This was done to increase CedeDrive security.

  -   Added a new "Explore" option when right clicking on Encrypted Drives from the CedeDrive Manager Window. Clicking Explore will cause Windows Explorer to
	 browse directly to the contents of the encrypted drive.

Version 2.76

  - Fixed an issue whereby the drive was not dismounting when going into Standby or Hibernate on Windows Vista/Windows 7.

Version 2.77

	- Added a full screen password window which is displayed when resuming from hibernate or standby
	- Added a "forgot your password?" option in the standard password window which starts off the recovery process

Version 2.78
	
	- On the CedeDriveService, changed the way formatting is done. Formatting of new virtual drives now calls the windows format command by first creating a batch file script.
	- When unmounting drives, stopped the saving of the config file as this was corrupting the config file storing the password.
	- When creating new virtual drives, the name of the volume is now set to the same name as the drive when formatting.
	- When performing a removable conversion, the convert window is now always activated with the message boxes using the window handle of the convert window instead of null.
	- When an error occurs in the CedeDrive Manager service, it wasn't closing the handle to the file resource if it was a file related error. This would result in Error 32 when trying to remount the drive.
	- When mounting a drive that requests a drive letter which is not available, a new drive letter is automatically assigned to prevent drive letter clashing, and thus causing an Error 32.


Version 2.79

	- When doing a redirect of My Documents, and user then deletes drive we need to undo the redirection!!! - DONE
	- New Virtual Drives will default to public documents.
	- When users pull out memory sticks, need to autodetect this and unmount the drive. - DONE
	- When unmounting all drives, the removable icons also need to disappear - DONE
	- When plugging in a memory stick on a computer with cededrive installed, then there is no need to run the mount program manually. - DONE

Version 2.7.10

	- When mounting drives a write test is done to ensure the drive is accessible. If the drive is accessible the drive is displayed using windows explorer
	  but if the drive is not accessible then CedeDrive asks the user if they want to enter their password again.

	- When doing a secure move, and shredding the files - a check is now done to make sure the file is gone, if it is not then the shredding is repeated a maximum of 5 times.

	- Add existing drive feature, you can now add EVD files to CedeDrive directly just incase CedeDrive has been reinstalled or Configuration has been reset and you need to
	  remount encrypted drives that you might have created in the past.

	- Reset Configuration Feature. This can be found under the Tools Menu. This is useful if users want to reset CedeDrive to factory defaults, the same behaviour as when CedeDrive is first installed.

Version 2.7.11

	- Fixed an issue whereby when changing the name of the recovery key the .dat extension would not be appended to the file name causing files to be generated without
	  an extension.

	- Fixed an issue whereby Windows Explorer would open the drive multiple times after mounting.

	- Ensured that when starting the recovery process, all drives are unmounted first to avoid a File Sharing Violation.

	- When autopopulating the drive details for new encrypted drives, the drive name was not generating a unique name, thus causing recovery key names to be the same. Now
	  autopopulating the new drive details will have a number appended to the drive if you have more than one, and thus the recovery key created will also match the unique
	  name.

	- When right clicking on the console window, all unusable options are greyed out if a drive is not selected. In addition, when right clicking on a removable encrypted drive
	  then the delete option is greyed out.

	- When right clicking on a removable drive in the console window, the redirection options are also greyed out. This ensures users cannot redirect any windows folders
	  to removable drives as this not recommended.

	- A new Tools->Options window is now available which allows you to turn on and off the secure password window when resuming from hibernate/standby. You also have the option
	  of showing the main CedeDrive window on startup or turning this off.

	- A new MSI Bootstrapper has been created this now means we provide users with a single CedeDriveSetup.exe file which will launch the installation. There is now no need
	  to download an manually unzip a zip file and launch the correct MSI. The MSI Bootstrapper (CedeDriveSetup.exe) will automatically launch the correct MSI (32bit or 64bit)
	  depending on the operating system it is running from. This makes downloading and installation much easier for end users.

	- A CedeDriveLaunch bootstrapper has been created to account for the fact that after code signing executables you can no longer modify them without causing the digital
	  signature to become invalid. Therefore the CedeDriveLaunch executable is now a very simple launcher which is code signed and requests elevation if required which then
	  launched CDLDR (which is the old CedeDriveLaunch) which does the work of launching the removable version of cededrive, and checking copy protection, etc...

	- CedeDrive is now codesigned.

Version 2.7.12

	- Fixed a bug whereby the full screen password window could not be cleared if a removable drive was inserted. (This was because standalone mode was set to true and the verify password
	  method was not doing a full password check and returning early).

	- Added a service waiting dialog. This dialog will be displayed if the CedeDrive GUI starts up before the CedeDrive Manager Service. The CedeDrive GUI will wait for the manager service
	  to become available before mounting any drives at startup. This is useful particularly on slower computers whereby the services are still starting up when the desktop is loaded.

	- Fixed a bug whereby the removable conversion window would remain behind the main window during the conversion process.


Version 2.7.13

	- Properly fixed the problem whereby the convert window kept going behind the main window, and you had to keep bringing it back to the foreground.
*/

#include "MainWindow.h"

MainWindow::MainWindow ()
{	
	m_pwnd = (MainWindow *) this;
	m_wrongpasswordcount = 0;

	//http://www.cedesoft.com/FeatAct147770747.aspx?pk=124ABC123ABC123ABC123ABC123ABC&cm=AAAA&f=OCC&m=3
}

MainWindow::~MainWindow ()
{

}

void MainWindow::Initialise (HWND hWnd, bool bAlreadyrunning, LPSTR lpCmdline)
{	
	m_bAlreadyrunning = bAlreadyrunning; // Flag to inform us that an instance of this app is already running
	SetParentHWND (hWnd);
	//m_hwnd = hWnd;
	SetBgColor (GetSysColor (COLOR_BTNFACE));
	SetCaption (TEXT ("CedeDrive v2.7.13"));
	SetWindowStyle (FS_STYLESTANDARD);
	CreateAppWindow ("CRYPTWindowClass", 70, 0, 658, 434, false);
	m_uihandler.SetWindowProperties (0, 70, 0, 343, RGB (200, 200, 200));
	SetWindowPosition (FS_CENTER);
	//Show ();
}

void MainWindow::SetDiagnostics (Diagnostics *pdiag)
{
	m_pdiag = pdiag;

	m_vdmanager.SetDiagnostics (pdiag);
	m_addwindow.SetDiagnostics (pdiag);
	m_drivebackup.SetDiagnostics (pdiag);
	m_convertwindow.SetDiagnostics (pdiag);
	m_enc.SetOutputHWND (m_pdiag->GetListBoxHWND ());
}

void MainWindow::OnCreate (HWND hWnd)
{
	m_hwnd = hWnd;
	g_hWnd = hWnd; // Need to do this otherwise the control creation wrapper functions don't work.

	RegisterIPCEvent("CedeDriveCoreEvent");

	if (m_bAlreadyrunning == true) {
		ShareConfig ();
		PostQuitMessage (0);
		return;
	}

	// Set the global static pointer to this window
	m_pwnd = (MainWindow *) this;

	m_minuteselapsed = 0; // Reset the number of minutes elapsed to 0.

	SetTimer (hWnd, IDT_RUNNINGTIMER, 60000, NULL); // Trigger every minute

	//if (ReadSharedInfo () == false) {
		//MessageBox (NULL, "Error reading shared info", "Info", MB_OK);
	//}

	m_drivebackup.SetHWND (m_hwnd);

	//m_header.SetBitmapResources (IDB_HEADER);
	//m_header.SetBitmapProperties (0, 0, 238, 64);
	//m_header.SetProperties (hWnd, CID_HEADER, 3, 3, 238, 64);
	//m_uihandler.AddDirectControl (&m_header);
	
	m_pdiag->SetParentHWND (hWnd);
	m_vdmanager.SetHWND (hWnd);

	m_mainmenu.CreateMainMenu (hWnd);
	m_mainmenu.CreateMainPopupMenu ();

	m_openfoldertimeractive = false;


	/*
	SingleDriveInfo diskinfo;
	diskinfo.lDiskSizeMegs = 100;
	strcpy_s (diskinfo.szDescription, SIZE_STRING, "This is my drive description");
	strcpy_s (diskinfo.szDriveLetter, SIZE_NAME, "Z");
	strcpy_s (diskinfo.szName, SIZE_STRING, "My test data");
	strcpy_s (diskinfo.szPath, SIZE_STRING, "D:\\Disk1.dat");

	SingleDriveInfo diskinfo2;
	diskinfo2.lDiskSizeMegs = 100;
	strcpy_s (diskinfo2.szDescription, SIZE_STRING, "This is my drive description");
	strcpy_s (diskinfo2.szDriveLetter, SIZE_NAME, "Y");
	strcpy_s (diskinfo2.szName, SIZE_STRING, "My test data");
	strcpy_s (diskinfo2.szPath, SIZE_STRING, "D:\\Disk2.dat");
	*/

	//m_vdmanager.AllocateVirtualDisk (diskinfo);
	
	//m_vdmanager.MountVirtualDisk (diskinfo);
	//m_vdmanager.MountVirtualDisk (diskinfo2);

	// Prepare the configuration file path, used for loading the drive list, and saving the drive list
	PrepareConfigPath();

	m_header.SetBitmapResources (IDB_MAINSPLASH);
	m_header.SetBitmapProperties (0, 0, 202, 387);
	m_header.SetProperties (hWnd, CID_ADDBANNER, 0, 0, 202, 387);
	m_uihandler.AddDirectControl (&m_header);

	// Create the list view control
	m_hwndlistview = CreateWindow (WC_LISTVIEW, "", WS_CHILD | WS_VISIBLE | LVS_REPORT | LVS_EDITLABELS, 202, 0, 449, 314, hWnd, (HMENU) ID_LISTVIEW, GetModuleHandle (NULL), NULL);
	
	


	// Create the info panel
	int yoffset = -23;
	m_infodrivename = CreateLabel("Drive Name: ",	218, 345+yoffset, 170, 13, ID_INFODRIVENAME);
	m_infodisksize = CreateLabel("Disk Size: ",			395, 345+yoffset, 106, 13, ID_INFODISKSIZE);
	m_infodriveletter = CreateLabel("Drive Letter: ",	524, 345+yoffset, 79, 13, ID_INFODRIVELETTER);
	m_infodescription = CreateLabel("Description: ",	218, 365+yoffset, 395, 13, ID_INFODESCRIPTION);
	m_infopath = CreateLabel("File path: ",				218, 386+yoffset, 395, 13, ID_INFOPATH);

	// Prepare the list view control
	PrepareListView();
	SetWindowLong (m_hwndlistview, GWL_STYLE, WS_CHILD | WS_VISIBLE | LVS_ICON | LVS_EDITLABELS);
	ShowWindow (m_hwndlistview, SW_HIDE);



	m_whiterect.SetProperties (hWnd, CID_STATIC, 202, 0, 449, 314);
	m_uihandler.AddDirectControl (&m_whiterect);
	m_btnadddrive = CreateButton ("Create an Encrypted Drive...", 218, 100, 170, 40, ID_BTNCREATEDRIVE);
	m_btnaddexisting = CreateButton ("Add an Existing Drive...", 218, 160, 170, 40, ID_BTNADDEXISTINGDRIVE);
	m_btnconvertdrive = CreateButton ("Encrypt a Removable Drive...", 218, 220, 170, 40, ID_BTNCONVERTDRIVE);
	
	m_headerlabel.SetTextProperties ("Arial", 130, 0, 0, 0, "Getting started with CedeDrive...", RGB (51, 100, 191));
	m_headerlabel.SetProperties (hWnd, CID_CONVERTHEADERLABEL, 218, 22, 350, 20);
	m_uihandler.AddDirectControl (&m_headerlabel);

	m_lbladddrive.SetTextProperties ("Arial", 90, 0, 0, 0, "Start securing your files on your computer", RGB (40, 40, 40));
	m_lbladddrive.SetProperties (m_hwnd, CID_STATIC, 403, 103, 114, 13);
	m_uihandler.AddDirectControl (&m_lbladddrive);

	m_lbladddrive2.SetTextProperties ("Arial", 90, 0, 0, 0, "by creating encrypted drives...", RGB (40, 40, 40));
	m_lbladddrive2.SetProperties (m_hwnd, CID_STATIC, 403, 119, 114, 13);
	m_uihandler.AddDirectControl (&m_lbladddrive2);


	m_lbladdexistingdrive.SetTextProperties ("Arial", 90, 0, 0, 0, "Already familiar with CedeDrive? Add", RGB (40, 40, 40));
	m_lbladdexistingdrive.SetProperties (m_hwnd, CID_STATIC, 403, 163, 114, 13);
	m_uihandler.AddDirectControl (&m_lbladdexistingdrive);

	m_lbladdexistingdrive2.SetTextProperties ("Arial", 90, 0, 0, 0, "your existing encrypted drives...", RGB (40, 40, 40));
	m_lbladdexistingdrive2.SetProperties (m_hwnd, CID_STATIC, 403, 179, 114, 13);
	m_uihandler.AddDirectControl (&m_lbladdexistingdrive2);


	m_lblconvertdrive.SetTextProperties ("Arial", 90, 0, 0, 0, "Secure your portable drives by ", RGB (40, 40, 40));
	m_lblconvertdrive.SetProperties (m_hwnd, CID_STATIC, 403, 223, 114, 13);
	m_uihandler.AddDirectControl (&m_lblconvertdrive);

	m_lblconvertdrive2.SetTextProperties ("Arial", 90, 0, 0, 0, "converting them to encrypted devices...", RGB (40, 40, 40));
	m_lblconvertdrive2.SetProperties (m_hwnd, CID_STATIC, 403, 239, 114, 13);
	m_uihandler.AddDirectControl (&m_lblconvertdrive2);



	SingleDriveInfo tempDrive;
	SetInfoPanel (tempDrive, false);
	
	//if (m_memshared.ReadSharedMemory ("CedeDriveLauncher", 1024000) == true) {
	if (m_sharedconfig.ConfigExists () == true) {

		m_bstandalonemode = true;

		OutputText ("CedeDriveLauncher Shared config read ok. Standalone mode activated.");

		m_dlnewdrives.Clear ();
		m_sharedconfig.Read (&m_memshared);
		m_dlnewdrives.FromMemoryBuffer (&m_memshared); // The shared memory we're expecting will be a dynlist

		OutputInt ("m_dlnewdrives, number of items: ", m_dlnewdrives.GetNumItems ());

		// The launcher is now not needed, so tell it to quit
		BroadcastLauncherQuit ();

#ifdef _FORLAUNCH
		// Now check that this copy of CedeDrive is authorised to mount the requested
		// drives, just to make sure users haven't copied the executable.
		if (AuthoriseExecution () == false) {
			MessageBox (NULL, "Please run CedeDrive from the storage device that was originally encrypted. Running CedeDrive from another location is not authorised.", "Unauthorised Execution", MB_OK | MB_ICONEXCLAMATION);
			PostQuitMessage (0);
			return;
		}
#endif

	} else {
		OutputText ("CedeDriveLauncher shared memory not found. Running in normal mode.");
		
#ifdef _FORLAUNCH
		PostQuitMessage (0);
		return;
#endif
	}



	// License check
	m_bTrialmode = true;
	ZeroMemory (m_szUserlicense, SIZE_STRING);
	
#ifdef _FORLAUNCH

	char szFilename[SIZE_STRING];
	ZeroMemory (szFilename, SIZE_STRING);
	GetModuleFileName (NULL, szFilename, SIZE_STRING);

	MemoryBuffer memModule;
	memModule.ReadFromFile (szFilename);

	if (m_canvas.UserKeyPresent (&memModule) == true) {
		
		if (m_canvas.ReadUserKey (m_szUserlicense, &memModule) == true) {
			if (m_license.ValidateUserKey (m_szUserlicense) == true) {
				
				if (strcmp (m_license.GetUniqueProductID (), "11977474774712") == 0) {																
					if (m_license.GetTrialMode () == true) {
						m_bTrialmode = true;
					} else {
						m_pdiag->OutputText ("License Type: Full/Unlimited");
						sprintf_s (m_szWindowcomment, SIZE_STRING, "(Registered to %s)", m_license.GetOwner ());						
						
						m_bTrialmode = false;
					}															
				} else {
					m_bTrialmode = true;
				}

			} else {
				m_bTrialmode = true;
				m_pdiag->OutputText ("Embedded user license validation failed. Defaulting to trial...");				
			}
		} else {
			m_pdiag->OutputText ("Unable to read embedded user license key, defaulting to trial mode...");
			// If no license is present then default to a trial key
			m_bTrialmode = true;
		}

	} else {
		m_pdiag->OutputText ("No embedded license present, defaulting to trial mode...");
		// If no license is present then default to a trial key
		m_bTrialmode = true;
	}

	memModule.Clear ();

#else

	if (m_license.CheckDLLPath () == true) {

		if (m_license.UserKeyPresent () == false) { // Open up the CCrypt.DLL and check that the License key is physically there, byte by byte
			m_pdiag->OutputText ("No license present, defaulting to trial mode...");
			// If no license is present then default to a trial key
			m_bTrialmode = true;
		} else {
			
			m_license.LoadUserKey (m_szUserlicense); // Load the user key into the supplied string, but first decoding the bytes. The bytes are stored encoded.

			if (m_license.ValidateUserKey (m_szUserlicense) == true) { // This essentially deserializes the user key and retrieves each field from the key
				
				if (m_license.MachineMatch () == true) { // This keys that the unique machine id matches the one stored in the dll
					m_pdiag->OutputText ("License was valid...");
					if (strcmp (m_license.GetUniqueProductID (), "11977474774712") == 0) {
																
						if (m_license.GetTrialMode () == true) {
							m_bTrialmode = true;
						} else {
							m_pdiag->OutputText ("License Type: Full/Unlimited");
							sprintf_s (m_szWindowcomment, SIZE_STRING, "(Registered to %s)", m_license.GetOwner ());						
							
							m_bTrialmode = false;
						}															
					} else {
						m_bTrialmode = true;
					}
				} else {
					
					// Machine key was not valid, show license prompt.
					// Most likely the program dir was copied to another machine without
					// being properly licensed.
					m_pdiag->OutputText ("Machine key not valid, defaulting to trial mode...");
					m_bTrialmode = true;
				}

			} else {

				// License was not valid, show license prompt.
				// an attempt at license tampering was made, show license.
				m_pdiag->OutputText ("Possible license tampering, defaulting to trial mode...");
				m_bTrialmode = true;
			}
		}
	} else {
		m_bTrialmode = true;
	}

	
#endif

#ifndef _FORLAUNCH
	SetKeybHook (m_hwnd, KEYBSHORTCUT);
#endif

	if (m_bTrialmode == true) {
		sprintf_s (m_szWindowcomment, SIZE_STRING, "(Trial)");						
	}

	m_vdmanager.SetTrialMode (m_bTrialmode);
	m_driveformat.SetTrialMode (m_bTrialmode);
	m_drivebackup.SetTrialMode (m_bTrialmode);
	
	if (m_bTrialmode == false) {
		m_convertwindow.SetLicensingInfo (&m_productactivation, m_szUserlicense, m_license.GetMaxClients (), m_bTrialmode);
	}

	UpdateWindowCaption();
	
	if (m_bTrialmode == true) {
		MessageBox (NULL, "Thank you for trying CedeDrive, a secure virtual drive encryption solution for desktops, laptops and removable drives. For full purchase information please visit http://www.cedesoft.com.\n\nPlease note this trial version is limited to 1 virtual drive size of 100MB, and drives are disconnected after 20 minutes.", "Trial Version Information", MB_OK | MB_ICONINFORMATION);
	}

	if (m_bstandalonemode == true) {
		
		m_passwindow.Initialise (hWnd, 0);
		m_passwindow.SetEncryptMode (false);
		m_passwindow.SetDriveName ("Removable Drive");
		m_passwindow.Show ();
		ActivatePasswindow ();
		//m_passwindow.SetWindowFocus ();

	} else {
		if (LoadConfig () == false) {
			m_passwindow.Initialise (hWnd, 0);
			m_passwindow.SetEncryptMode (true);
			m_passwindow.Show ();
			ActivatePasswindow ();
			//m_passwindow.SetWindowFocus ();
		} else {
			m_passwindow.Initialise (hWnd, 0);
			m_passwindow.SetEncryptMode (false);
			m_passwindow.Show ();		
			ActivatePasswindow ();
			//m_passwindow.SetWindowFocus ();
		}
	}

	DecideWelcomeVisible ();

	// Refresh the list of system drives
	GetAllSystemDrives (&m_dlSystemDrives);

	//TestMethod ();
}

void MainWindow::DecideWelcomeVisible ()
{
	if (m_dlDrivelist.GetNumItems () > 0) {
		ShowWelcome (false);
	} else {
		ShowWelcome (true);
	}
}

void MainWindow::WaitForCedeDriveService ()
{
	// This will popup a modal dialog that will display a message to the user telling them that the GUI is waiting
	// for the cededrive manager service to become available. This will only happen if the GUI has been running for
	// under 15 minutes - e.g. the Computer has just booted up. After that time no waiting is done and it is assumed
	// the the cededrive manager service is running. If not then a proper error is presented to the user.

	// This accounts for computers that may be a bit slow on startup, and it's possible that the GUI could startup
	// before the CedeDrive Manager Service.

#ifndef _FORLAUNCH

	if (m_vdmanager.IsServiceRunning () == false) {
		if (m_minuteselapsed < 15) {
			ShowServiceWaitDialog ();
		}
	}

#endif
}

void MainWindow::ShowWelcome (bool bVisible)
{
	m_headerlabel.SetVisible (bVisible);
	m_whiterect.SetVisible (bVisible);	
	m_lbladddrive.SetVisible (bVisible);
	m_lbladddrive2.SetVisible (bVisible);
	m_lbladdexistingdrive.SetVisible (bVisible);
	m_lbladdexistingdrive2.SetVisible (bVisible);
	m_lblconvertdrive.SetVisible (bVisible);
	m_lblconvertdrive2.SetVisible (bVisible);

	if (bVisible == true) {
		ShowWindow (m_btnadddrive, SW_SHOW);
		ShowWindow (m_btnaddexisting, SW_SHOW);
		ShowWindow (m_btnconvertdrive, SW_SHOW);
		ShowWindow (m_hwndlistview, SW_HIDE);
	} else {
		ShowWindow (m_btnadddrive, SW_HIDE);
		ShowWindow (m_btnaddexisting, SW_HIDE);
		ShowWindow (m_btnconvertdrive, SW_HIDE);
		ShowWindow (m_hwndlistview, SW_SHOW);
	}
}

bool MainWindow::AuthoriseExecution ()
{
	// Checks to make sure this executable is actually authorised to run. It does this by checking
	// the encoded volume serial number that is written to the executable, and comparing this
	// to the volume serial of the drive letter that we are trying to mount. Therefore if a user is attempting
	// to use this executable to mount a drive it was not intended for, then it will fail. A form of copy
	// protection.

	char szDriveletter[SIZE_NAME];
	int authdrives = 0;

	char szFilename[SIZE_STRING];
	ZeroMemory (szFilename, SIZE_STRING);
	GetModuleFileName (NULL, szFilename, SIZE_STRING);

	MemoryBuffer memModule;
	memModule.ReadFromFile (szFilename);

	SingleDriveInfo *pdrive;
	for (int d=0;d<m_dlnewdrives.GetNumItems ();d++) {
		pdrive = (SingleDriveInfo *) m_dlnewdrives.GetItem (d);

		ZeroMemory (szDriveletter, SIZE_NAME); // The drive letter of the EVD file attempting to mount
		strncpy_s (szDriveletter, SIZE_NAME, pdrive->szPath, 3);

		if (m_canvas.IsVolumeSerialIdentical (szDriveletter, &memModule) == true) {
			authdrives++;
		}
		
		//m_dlDrivelist.AddItem (pdrive, sizeof (SingleDriveInfo), false);
	}

	memModule.Clear ();

	if (authdrives > 0) {
		return true;
	} else {
		return false;
	}
}

void MainWindow::ShareConfig ()
{
	//m_dlDrivelist

	if (LoadConfig () == true) {
		
		//m_memshared.StartSharing ("CedeDriveLauncher", 1024000);			
		m_dlDrivelist.ToMemoryBuffer (&m_memshared);
		m_sharedconfig.Save (&m_memshared);

		BroadcastIPCEvent (0, 0); // Tell the already running instance to read our shared memory

		// Now stop sharing and quit
		//m_memshared.StopSharing ();
	}
}

void MainWindow::BroadcastIPCEvent (WPARAM wParam, LPARAM lParam)
{
	if (g_ipcmessageregistered == true) {
		SendMessage (HWND_BROADCAST, g_ipcmessage, wParam, lParam);
	}			
}

void MainWindow::BroadcastLauncherQuit ()
{
	// Tell the cededrive launcher to quit, as we have finished with it
	g_quitmessage = RegisterWindowMessage ("CedeDriveLaunchQuit");
	SendMessage (HWND_BROADCAST, g_quitmessage, 0, 0);
}

/*
bool MainWindow::CheckStandaloneMode ()
{
	char szModulefilename[SIZE_STRING];
	ZeroMemory (szModulefilename, SIZE_STRING);
	GetModuleFileName (NULL, szModulefilename, SIZE_STRING);	

	char szPathonly[SIZE_STRING];
	ZeroMemory (szPathonly, SIZE_STRING);
	GetPathOnly (szModulefilename, szPathonly);
	strcat_s (szPathonly, SIZE_STRING, "EncryptedDisk.evd");

	char szFreeDriveletter[SIZE_NAME];
	ZeroMemory (szFreeDriveletter, SIZE_NAME);

	if (FileExists (szPathonly) == true) {
		m_bstandalonemode = true;

		if (GetFreeDriveLetter (szFreeDriveletter) == false) {
			// no available drive letter on this system, abort!
			MessageBox (NULL, "There are no available drive letters available on this system. Cannot mount encrypted disk.", "Insufficient drive letters", MB_OK | MB_ICONEXCLAMATION);
			PostQuitMessage (0);
			return false;
		}

		// if we got here then we have an available drive letter to use for later on
		// but we need to add a colon to it.
		strcat_s (szFreeDriveletter, SIZE_NAME, ":");
		
		ZeroMemory (m_localmodedrive.szName, SIZE_STRING);
		strcpy_s (m_localmodedrive.szName, SIZE_STRING, "Standalone Encrypted Drive"); //m_localmodedrive
		
		ZeroMemory (m_localmodedrive.szDescription, SIZE_STRING);
		strcpy_s (m_localmodedrive.szDescription, SIZE_STRING, "Standalone Encrypted disk for use with removable drives.");

		ZeroMemory (m_localmodedrive.szPath, SIZE_STRING);
		strcpy_s (m_localmodedrive.szPath, SIZE_STRING, szPathonly);

		ZeroMemory (m_localmodedrive.szDriveLetter, SIZE_NAME);
		strcpy_s (m_localmodedrive.szDriveLetter, SIZE_NAME, szFreeDriveletter);
		
		m_localmodedrive.bMountStartup = true;

		// Need to specify a disk size in megs!!!!
		unsigned long vdsize = m_vdmanager.GetVirtualDiskSizeMegs (szPathonly);
		m_localmodedrive.lDiskSizeMegs = vdsize;

		OutputText ("STANDALONE MODE! Local EVD file found.");
		OutputInt ("Standalone mode disk size: ", vdsize);

	} else {
		m_bstandalonemode = false;
	}

	return true;
}
*/

void MainWindow::GetPathOnly (char *szInpath, char *szOutpath)
{
	char szCurchar[SIZE_NAME];
	int seploc = 0;

	for (int c=strlen(szInpath);c>0;c--) {
		
		ZeroMemory (szCurchar, SIZE_NAME);
		strncpy_s (szCurchar, SIZE_NAME, szInpath+c, 1);

		if (strcmp (szCurchar, "\\") == 0) {
			seploc = c+1;
			break;
		}
	}

	strncpy_s (szOutpath, SIZE_STRING, szInpath, seploc);
}

void MainWindow::ShowLicensePrompt ()
{
	m_licensewindow.SetDiagnostics (m_pdiag);
	m_licensewindow.Initialise (m_hwnd, 0);
	m_licensewindow.SetAlwaysOnTop (true);
}

void MainWindow::UpdateWindowCaption ()
{
	ZeroMemory (m_szWindowcaption, SIZE_STRING);
	strcpy_s (m_szWindowcaption, SIZE_STRING, "CedeDrive v2.7.13 ");
	strcat_s (m_szWindowcaption, SIZE_STRING, m_szWindowcomment);
	SetTimer (m_hwnd, IDT_UPDATECAPTION, 2000, NULL);
}

void MainWindow::SetInfoPanel (SingleDriveInfo driveinfo, bool showinfo)
{
	char szText[SIZE_STRING];

	if (showinfo == true) {

		ZeroMemory (szText, SIZE_STRING);
		sprintf_s (szText, "Drive Name: %s", driveinfo.szName);
		SetDlgItemText(m_hwnd, ID_INFODRIVENAME, szText);

		ZeroMemory (szText, SIZE_STRING);
		sprintf_s (szText, SIZE_STRING, "Disk Size: %i MB", driveinfo.lDiskSizeMegs);
		SetDlgItemText(m_hwnd, ID_INFODISKSIZE, szText);

		ZeroMemory (szText, SIZE_STRING);
		sprintf_s (szText, "Drive Letter: %s", driveinfo.szDriveLetter);
		SetDlgItemText(m_hwnd, ID_INFODRIVELETTER, szText);

		ZeroMemory (szText, SIZE_STRING);
		sprintf_s (szText, "Description: %s", driveinfo.szDescription);
		SetDlgItemText(m_hwnd, ID_INFODESCRIPTION, szText);

		ZeroMemory (szText, SIZE_STRING);
		sprintf_s (szText, "File path: %s", driveinfo.szPath);
		SetDlgItemText(m_hwnd, ID_INFOPATH, szText);

	} else {
		
		SetDlgItemText(m_hwnd, ID_INFODRIVENAME, "");
		SetDlgItemText(m_hwnd, ID_INFODISKSIZE, "");
		SetDlgItemText(m_hwnd, ID_INFODRIVELETTER, "");
		
		if (m_dlDrivelist.GetNumItems () > 0) {
			SetDlgItemText(m_hwnd, ID_INFODESCRIPTION, "Click on a drive to see more information...");
		} else {
			SetDlgItemText(m_hwnd, ID_INFODESCRIPTION, "You have no encrypted drives defined. Click File->New to create a new drive...");
		}
		
		SetDlgItemText(m_hwnd, ID_INFOPATH, "");

	}

}

void MainWindow::AddTrayIcon ()
{
	m_hSystrayicon = LoadIcon (GetModuleHandle (NULL), MAKEINTRESOURCE (ID_TRAYICON));

	m_nid.cbSize = sizeof (m_nid);
	m_nid.hWnd = m_hwnd;
	m_nid.uID = IDC_SYSTRAYICON;
	m_nid.uFlags = NIF_ICON | NIF_MESSAGE | NIF_TIP;
	m_nid.uCallbackMessage = WM_SYSTRAY;
	m_nid.hIcon = m_hSystrayicon;

	strcpy (m_nid.szTip, "CedeDrive Virtual Drive Encryption");

	// Now actually create the system tray icon.
	Shell_NotifyIcon (NIM_ADD, &m_nid);
}

void MainWindow::DeleteTrayIcon ()
{
	m_nid.cbSize = sizeof (m_nid);
	m_nid.hWnd = m_hwnd;
	m_nid.uID = IDC_SYSTRAYICON;
	m_nid.uFlags = NIF_ICON | NIF_MESSAGE | NIF_TIP;
	m_nid.uCallbackMessage = WM_SYSTRAY;
	m_nid.hIcon = m_hSystrayicon;

	strcpy (m_nid.szTip, "CedeDrive Virtual Drive Encryption");

	// Now actually create the system tray icon.
	Shell_NotifyIcon (NIM_DELETE, &m_nid);
}

void MainWindow::PrepareConfigPath ()
{
	int iRes = 0;
	char szAppData[SIZE_STRING];
	char szCompanyAppData[SIZE_STRING];
	char szProgramAppData[SIZE_STRING];
	char szInfoAppData[SIZE_STRING];

	ZeroMemory (szAppData, SIZE_STRING);
	ZeroMemory (szCompanyAppData, SIZE_STRING);
	ZeroMemory (szProgramAppData, SIZE_STRING);
	ZeroMemory (szInfoAppData, SIZE_STRING);

	if (GetEnvironmentVariable ("APPDATA", szAppData, SIZE_STRING) == 0) {
		// retrieving environment variable failed.
		//m_diag.OutputText ("Failed to retrieve APPDATA environment variable!");
		return;
	}

	strcpy_s (szCompanyAppData, SIZE_STRING, szAppData);
	strcat_s (szCompanyAppData, SIZE_STRING, "\\CedeSoft");
	_mkdir (szCompanyAppData);

	strcpy_s (szProgramAppData, SIZE_STRING, szCompanyAppData);
	strcat_s (szProgramAppData, SIZE_STRING, "\\CedeDrive");
	_mkdir (szProgramAppData);

	strcpy_s (szInfoAppData, SIZE_STRING, szProgramAppData);
	strcat_s (szInfoAppData, SIZE_STRING, "\\DriveList.dat");

	// Set our member variable which is used by the other functions
	ZeroMemory (m_szConfigpath, SIZE_STRING);
	strcpy_s (m_szConfigpath, SIZE_STRING, szInfoAppData);
	
	return;
}

void MainWindow::DeleteDrive (SingleDriveInfo *pinfo, bool bDeletehostfile)
{
	DynList dlTemp;

	SingleDriveInfo *ptemp;
	SingleDriveInfo tempDrive;

	for (int d=0;d<m_dlDrivelist.GetNumItems ();d++)
	{
		ptemp = (SingleDriveInfo *) m_dlDrivelist.GetItem (d);
		memcpy (&tempDrive, (SingleDriveInfo *) ptemp, sizeof (SingleDriveInfo));

		if (strcmp (ptemp->szPath, pinfo->szPath) != 0) {
			dlTemp.AddItem (&tempDrive, sizeof (SingleDriveInfo), false);
		} 

	}

	m_dlDrivelist.Clear();

	for (int t=0;t<dlTemp.GetNumItems();t++) {
		ptemp = (SingleDriveInfo *) dlTemp.GetItem (t);
		memcpy (&tempDrive, (SingleDriveInfo *) ptemp, sizeof (SingleDriveInfo));

		m_dlDrivelist.AddItem (&tempDrive, sizeof (SingleDriveInfo), false);
	}

	if (bDeletehostfile == true) {
		// Now actually delete the file hosting the virtual disk
		DeleteFile (pinfo->szPath);
	}
}

bool MainWindow::DoesFileExist (char *szFilename)
{
   WIN32_FIND_DATA FindFileData;
   HANDLE hFind;

	hFind = FindFirstFile(szFilename, &FindFileData);

	if (hFind == INVALID_HANDLE_VALUE)
	{
		return false;
	} else {
		FindClose (hFind);
		return true;
	}
}

bool MainWindow::FileExistsEx (char *FileName)
{
    FILE* fp = NULL;

    //will not work if you do not have read permissions
    //to the file, but if you don't have read, it
    //may as well not exist to begin with.

    fp = fopen( FileName, "rb" );
    if( fp != NULL )
    {
        fclose( fp );
        return true;
    }

    return false;
}

bool MainWindow::FileExists (char *FileName)
{
	// Check if the file exists		
	struct _finddata_t c_file;
	long hFile;

	// Build the list of files in the source path
	if( (hFile = _findfirst(FileName, &c_file )) == -1L ) {
		return false;
	} else {
		return true;
	}

	return false;
}

void MainWindow::SaveConfig ()
{
	int numdrives = 0;
	int encpasssize = 0;
	int d = 0;
	int tempsize = 0;
	MemoryBuffer memSer;

	SingleDriveInfo *pinfo;

	//m_memEncpassword
	// Calculate the size of the serialised buffer
	
	int sersize = (sizeof (SingleDriveInfo) * m_dlDrivelist.GetNumItems ());
	sersize += sizeof (int) + m_memEncpassword.GetSize(); // Account for the size of the encryption password
	sersize += sizeof (int); // Plus additional room to store the number of contacts

	memSer.SetSize (sersize);

	numdrives = m_dlDrivelist.GetNumItems ();
	encpasssize = m_memEncpassword.GetSize();

	// Add the size of the encryption password to the buffer
	memSer.Append (&encpasssize, sizeof (int));
	memSer.Append (m_memEncpassword.GetBuffer(), encpasssize);

	// Add the number of drives to the buffer
	memSer.Append (&numdrives, sizeof (int));

	for (d=0;d<m_dlDrivelist.GetNumItems ();d++) {
		
		pinfo = (SingleDriveInfo *) m_dlDrivelist.GetItem (d);

		memSer.Append (&pinfo->bMountStartup, sizeof (bool));
		memSer.Append (&pinfo->lDiskSizeMegs, sizeof (long));
		
		tempsize = strlen (pinfo->szDescription);
		memSer.Append (&tempsize, sizeof (int));
		memSer.Append ((char *) pinfo->szDescription, tempsize);

		tempsize = strlen (pinfo->szDriveLetter);
		memSer.Append (&tempsize, sizeof (int));
		memSer.Append ((char *) pinfo->szDriveLetter, tempsize);

		tempsize = strlen (pinfo->szName);
		memSer.Append (&tempsize, sizeof (int));
		memSer.Append ((char *) pinfo->szName, tempsize);

		tempsize = strlen (pinfo->szPath);
		memSer.Append (&tempsize, sizeof (int));
		memSer.Append ((char *) pinfo->szPath, tempsize);

	}

	if (strlen (m_szConfigpath) > 0) {
		memSer.SaveToFile (m_szConfigpath); // Save the config to disk
	}
}

void MainWindow::ResetConfig ()
{
	if (MessageBox (NULL, "WARNING: Resetting the CedeDrive configuration settings will mean CedeDrive will no longer remember any of your encrypted drives, and you will have to configure a new password for use with CedeDrive. \n\nAfter the Configuration is reset back to defaults CedeDrive will function as it did when it was installed for the first time.\n\nAny encrypted drives you have created will not be deleted, but if you wish to use these encrypted drives you will need to add them to CedeDrive.\n\nDo you wish to proceed?", "Reset CedeDrive Configuration", MB_YESNO | MB_ICONINFORMATION) == IDYES) {
		
		if (DeleteFile (m_szConfigpath) == TRUE) {
			MessageBox (NULL, "CedeDrive configuration has been reset. CedeDrive will now quit. Once you restart CedeDrive you will have to setup a new password.", "Configuration Reset", MB_OK | MB_ICONINFORMATION);
			PostQuitMessage (0);
		}
	}
}

bool MainWindow::LoadConfig ()
{
	m_dlDrivelist.Clear ();


	int numdrives = 0;
	int encpasssize = 0;
	int d = 0;
	int ipointer = 0;
	int tempsize = 0;
	SingleDriveInfo tempdrive;
	
	MemoryBuffer memSer;
	
	if (strlen (m_szConfigpath) == 0) {
		return false;
	}

	if (FileExists (m_szConfigpath) == false) {
		return false;
	}

	memSer.ReadFromFile (m_szConfigpath);

	memcpy (&encpasssize, (BYTE *) memSer.GetBuffer()+ipointer, sizeof (int));
	ipointer += sizeof (int);

	if (encpasssize > 0) {
		m_memEncpassword.SetSize (encpasssize);
		m_memEncpassword.Write ((BYTE *) memSer.GetBuffer()+ipointer, 0, encpasssize);
	}

	ipointer+= encpasssize;

	// Get the number of drives
	memcpy (&numdrives, (BYTE *) memSer.GetBuffer()+ipointer, sizeof (int));
	ipointer += sizeof (int);

	for (d=0;d<numdrives;d++) {
		
		memcpy (&tempdrive.bMountStartup, (BYTE *) memSer.GetBuffer()+ipointer, sizeof (bool));
		ipointer += sizeof (bool);

		memcpy (&tempdrive.lDiskSizeMegs, (BYTE *) memSer.GetBuffer()+ipointer, sizeof (long));
		ipointer += sizeof (long);

		// The Description field
		memcpy (&tempsize, (BYTE *) memSer.GetBuffer()+ipointer, sizeof (int));
		ipointer += sizeof (int);

		if (tempsize > 0) {
			ZeroMemory (tempdrive.szDescription, SIZE_STRING);
			strncpy_s (tempdrive.szDescription, SIZE_STRING, (char *) memSer.GetBuffer()+ipointer, tempsize);
		} else {
			ZeroMemory (tempdrive.szDescription, SIZE_STRING);
		}
		ipointer+=tempsize;

		// The Drive Letter
		memcpy (&tempsize, (BYTE *) memSer.GetBuffer()+ipointer, sizeof (int));
		ipointer += sizeof (int);

		if (tempsize > 0) {
			ZeroMemory (tempdrive.szDriveLetter, SIZE_NAME);
			strncpy_s (tempdrive.szDriveLetter, SIZE_NAME, (char *) memSer.GetBuffer()+ipointer, tempsize);
		} else {
			ZeroMemory (tempdrive.szDriveLetter, SIZE_NAME);
		}
		ipointer+=tempsize;

		// The Name Field
		memcpy (&tempsize, (BYTE *) memSer.GetBuffer()+ipointer, sizeof (int));
		ipointer += sizeof (int);

		if (tempsize > 0) {
			ZeroMemory (tempdrive.szName, SIZE_STRING);
			strncpy_s (tempdrive.szName, SIZE_STRING, (char *) memSer.GetBuffer()+ipointer, tempsize);
		} else {
			ZeroMemory (tempdrive.szName, SIZE_STRING);
		}
		ipointer+=tempsize;


		// The Path Field
		memcpy (&tempsize, (BYTE *) memSer.GetBuffer()+ipointer, sizeof (int));
		ipointer += sizeof (int);

		if (tempsize > 0) {
			ZeroMemory (tempdrive.szPath, SIZE_STRING);
			strncpy_s (tempdrive.szPath, SIZE_STRING, (char *) memSer.GetBuffer()+ipointer, tempsize);
		} else {
			ZeroMemory (tempdrive.szPath, SIZE_STRING);
		}
		ipointer+=tempsize;


		// Now add this temp drive to the list of drives
		m_dlDrivelist.AddItem (&tempdrive, sizeof (SingleDriveInfo), false);
	}

	return true;

}

void MainWindow::PrepareListView ()
{
	// Create the image lists
    HICON hiconSmall;     // icon for list-view items
	HICON hiconLarge;     // icon for list-view items	

	// Large icon image list
	m_hLarge = ImageList_Create(32, 32, ILC_COLOR32 , 1, 1); 
    // Small Icon image list
	m_hSmall = ImageList_Create(16, 16, ILC_COLOR32 , 1, 1); 

    hiconSmall = LoadIcon(GetModuleHandle (NULL), MAKEINTRESOURCE(IDI_ICON));
	hiconLarge = LoadIcon(GetModuleHandle (NULL), MAKEINTRESOURCE(IDI_ICON));	

    ImageList_AddIcon(m_hLarge, hiconLarge);	
    ImageList_AddIcon(m_hSmall, hiconSmall);
	
	DestroyIcon(hiconLarge);
	DestroyIcon(hiconSmall); 
	
    ListView_SetImageList(m_hwndlistview, m_hLarge, LVSIL_NORMAL); 
    ListView_SetImageList(m_hwndlistview, m_hSmall, LVSIL_SMALL); 


	// Add columns to the list view
	char szColname[SIZE_NAME];
	ZeroMemory (szColname, SIZE_NAME);
	strcpy_s (szColname, SIZE_NAME, "Drive Name");

	char szColname2[SIZE_NAME];
	ZeroMemory (szColname2, SIZE_NAME);
	strcpy_s (szColname2, SIZE_NAME, "Drive Size");

	char szColname3[SIZE_NAME];
	ZeroMemory (szColname3, SIZE_NAME);
	strcpy_s (szColname3, SIZE_NAME, "Drive Letter");

	LVCOLUMN lvc;
	int iCol = 0;

	lvc.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM; 
	
    lvc.iSubItem = 0;
    lvc.pszText = szColname;	
    lvc.cx = 100;     // width of column in pixels
	lvc.fmt = LVCFMT_LEFT;  // left-aligned column
	
	ListView_InsertColumn(m_hwndlistview, 0, &lvc);

	lvc.iSubItem = 1;
	lvc.pszText = szColname2;	
	ListView_InsertColumn(m_hwndlistview, 1, &lvc);

	lvc.iSubItem = 2;
	lvc.pszText = szColname3;	
	ListView_InsertColumn(m_hwndlistview, 2, &lvc);
}

bool MainWindow::IsDriveAccessible(char *szLetter)
{
	MemoryBuffer memData;

	char szString[SIZE_STRING];
	ZeroMemory (szString, SIZE_STRING);
	strcpy_s (szString, SIZE_STRING, "This is a test file which is saved to determine if a drive is accessible");

	memData.SetSize (strlen (szString));
	memData.Write (szString, 0, strlen (szString));

	char szFilename[SIZE_STRING];
	ZeroMemory (szFilename, SIZE_STRING);
	strcpy_s (szFilename, SIZE_STRING, szLetter);
	strcat_s (szFilename, SIZE_STRING, ":\\_CedeSoft_CedeDrive_Test_DataWrite.txt");

	OutputText ("Checking accessibility: ", szFilename);

	if (memData.SaveToFile (szFilename) == true) {
		DeleteFile (szFilename);
		return true;
	} else {
		return false;
	}
}

void MainWindow::CheckToTerminateProcess ( DWORD processID, char *szQueryName)
{
	#ifndef _FORLAUNCH

    TCHAR szProcessName[MAX_PATH] = TEXT("<unknown>");
	bool bResult = false;

    // Get a handle to the process.
    HANDLE hProcess = OpenProcess( PROCESS_QUERY_INFORMATION |
                                   PROCESS_VM_READ | SYNCHRONIZE | PROCESS_TERMINATE,
                                   FALSE, processID );

    // Get the process name.

    if (NULL != hProcess )
    {
        HMODULE hMod;
        DWORD cbNeeded;

        if ( EnumProcessModules( hProcess, &hMod, sizeof(hMod), 
             &cbNeeded) )
        {
            GetModuleBaseName( hProcess, hMod, szProcessName, 
                               sizeof(szProcessName)/sizeof(TCHAR) );
        }
    }

    // Print the process name and identifier.
    //_tprintf( TEXT("%s  (PID: %u)\n"), szProcessName, processID );
	//MessageBox (NULL, szProcessName, "ProcessName", MB_OK);

	char szNamecopy[SIZE_STRING];
	ZeroMemory (szNamecopy, SIZE_STRING);
	strcpy_s (szNamecopy, SIZE_STRING, szQueryName);

	_strupr_s (szNamecopy, SIZE_STRING); // Convert it to uppercase.
	_strupr_s (szProcessName, MAX_PATH); // Convert it to uppercase.

	if (strcmp (szNamecopy, szProcessName) == 0) {
		TerminateProcess (hProcess, 0);
	}

    CloseHandle( hProcess );

	#endif
}

void MainWindow::CheckTaskManagerProcess ()
{
	#ifndef _FORLAUNCH
	// Get the list of process identifiers.
    bool bFound = false;
	DWORD aProcesses[1024], cbNeeded, cProcesses;
    unsigned int i;

    if ( !EnumProcesses( aProcesses, sizeof(aProcesses), &cbNeeded ) )
        return;

    // Calculate how many process identifiers were returned.
    cProcesses = cbNeeded / sizeof(DWORD);

    // Print the name and process identifier for each process.

	for ( i = 0; i < cProcesses; i++ ) {
		//PrintProcessNameAndID( aProcesses[i] );
		CheckToTerminateProcess (aProcesses[i], "taskmgr.exe");
	}
	#endif
}

void MainWindow::UnmountDriveLetter (char *szLetter)
{
	SingleDriveInfo *pinfo;
	SingleDriveInfo tempInfo;

	char szSuppliedletter[SIZE_NAME];
	char szCurletter[SIZE_NAME];

	ZeroMemory (szSuppliedletter, SIZE_NAME);
	strcpy_s (szSuppliedletter, SIZE_NAME, szLetter);
	_strupr_s (szSuppliedletter, SIZE_NAME);

	for (int i=0;i<m_dlDrivelist.GetNumItems ();i++) {
		pinfo = (SingleDriveInfo *) m_dlDrivelist.GetItem (i);

		ZeroMemory (szCurletter, SIZE_NAME);
		strncpy_s (szCurletter, SIZE_NAME, pinfo->szDriveLetter, 1);
		_strupr_s (szCurletter, SIZE_NAME);

		if (strcmp (szSuppliedletter, szCurletter) == 0) {
			ZeroMemory (&tempInfo, sizeof (SingleDriveInfo));
			memcpy (&tempInfo, pinfo, sizeof (SingleDriveInfo));
			
			m_vdmanager.UnmountVirtualDrive (tempInfo);

			if (pinfo->bUsingseperatepassword == true) {
				DeleteDrive (pinfo, false);
				RefreshListView ();
			}
		}
	}
}

bool MainWindow::IsDriveLetterCedeDrive (char *szLetter)
{
	SingleDriveInfo *pinfo;

	char szSuppliedletter[SIZE_NAME];
	char szCurletter[SIZE_NAME];

	ZeroMemory (szSuppliedletter, SIZE_NAME);
	strcpy_s (szSuppliedletter, SIZE_NAME, szLetter);
	_strupr_s (szSuppliedletter, SIZE_NAME);

	for (int i=0;i<m_dlDrivelist.GetNumItems ();i++) {
		pinfo = (SingleDriveInfo *) m_dlDrivelist.GetItem (i);

		ZeroMemory (szCurletter, SIZE_NAME);
		strncpy_s (szCurletter, SIZE_NAME, pinfo->szDriveLetter, 1);
		_strupr_s (szCurletter, SIZE_NAME);

		if (strcmp (szSuppliedletter, szCurletter) == 0) {
			return true;
		}
	}

	return false;
}

void MainWindow::VerifyDriveAccessibility (char *szLetter)
{
	// Single Letter only please in parameter
	if (IsDriveAccessible (szLetter) == false) {
		if (MessageBox (NULL, "The virtual drive does not appear to be fully accessible using the password supplied.\n\nCedeDrive performs a write test on newly mounted drives to determine if they are accessible using the password you've supplied. If your password is incorrect then Windows will not recognise the mounted drive. If this is a Removable Drive please ensure you have sufficient permissions to write to the drive.\n\nWould you like to unmount the drive and enter your password again?", "Unable to write to drive", MB_YESNO | MB_ICONINFORMATION) == IDYES) {
			UnmountDriveLetter (szLetter);

			m_passwindow.Initialise (m_hwnd, 0);
			m_passwindow.SetEncryptMode (false);
			m_passwindow.SetDriveName ("Encrypted Virtual Drive");
			m_passwindow.Show ();
		}
	} else {

		char szDrive[SIZE_NAME];
		ZeroMemory (szDrive, SIZE_NAME);
		strcpy_s (szDrive, SIZE_NAME, szLetter);
		strcat_s (szDrive, SIZE_NAME, ":");

		ZeroMemory (szTimedOpenDrive, SIZE_NAME);
		strcpy_s (szTimedOpenDrive, SIZE_NAME, szDrive);

		if (m_openfoldertimeractive == false) {
			m_openfoldertimeractive = true;
			SetTimer (m_hwnd, IDT_OPENFOLDERTIMER, 3500, NULL);		
		}

		
	}
}

void MainWindow::VerifyPassword (unsigned int origin)
{
	// Stop the timer that keeps trying to activate the window
	KillTimer (m_hwnd, IDT_ACTIVATEWINDOW);

	//MessageBox (NULL, "Verifying password..", "Password", MB_OK);

	Obfuscator encoder;

	//MessageBox (NULL, "Password to be verified", "Test", MB_OK);
	char szEnctext[SIZE_STRING];
	ZeroMemory (szEnctext, SIZE_STRING);
	//strcpy_s (szEnctext, SIZE_STRING, "Unto You I lift up my eyes, O You who dwell in the heavens. Behold, as the eyes of servants look to the hand of their masters, As the eyes of a maid to the hand of her mistress, So our eyes look to the LORD our God, Until He has mercy on us. Have mercy on us, O LORD, have mercy on us! For we are exceedingly filled with contempt. Our soul is exceedingly filled With the scorn of those who are at ease, With the contempt of the proud. (Psalms 123).");
	encoder.GetMessage (szEnctext);
	
	

	char szPassword[SIZE_STRING];
	ZeroMemory (szPassword, SIZE_STRING);

	if (m_bstandalonemode == true) {
		if (origin == ORIGIN_FULLPASSWINDOW) {
			strcpy_s (szPassword, SIZE_STRING, m_passwindowfull.GetLastPassword ());
		} else {
			strcpy_s (szPassword, SIZE_STRING, m_passwindow.GetLastPassword ());
		}
		

		//m_dlDrivelist.Clear ();
		SingleDriveInfo *pdrive;
		for (int d=0;d<m_dlnewdrives.GetNumItems ();d++) {
			pdrive = (SingleDriveInfo *) m_dlnewdrives.GetItem (d);
			pdrive->bUsingseperatepassword = true;
			strcpy_s (pdrive->szPassword, SIZE_STRING, szPassword);

			m_dlDrivelist.AddItem (pdrive, sizeof (SingleDriveInfo), false);
		}

		//m_dlDrivelist.AddItem (&m_localmodedrive, sizeof (SingleDriveInfo), false);

		// Accept whatever password is supplied
		m_vdmanager.Initialise (szPassword);
		RefreshListView();

		AddTrayIcon();

		// Now mount all drives set to mount at startup only
		MountAllDrives(true);

		

		return;
	}

	bool bEncryptmode = false;

	if (origin == ORIGIN_FULLPASSWINDOW) {
		bEncryptmode = m_passwindowfull.m_bEncryptmode;
	} else {
		bEncryptmode = m_passwindow.m_bEncryptmode;
	}

	if (bEncryptmode == true) {
		// This is the first time a user has entered a password, so we must save it
		
		if (origin == ORIGIN_FULLPASSWINDOW) {
			strcpy_s (szPassword, SIZE_STRING, m_passwindowfull.GetLastPassword ());
		} else {
			strcpy_s (szPassword, SIZE_STRING, m_passwindow.GetLastPassword ());
		}
		

		m_memEncpassword.SetSize (strlen(szEnctext));
		m_memEncpassword.Write ((char *) szEnctext, 0, strlen (szEnctext));

		//m_enc.EncryptBuffer (&m_memEncpassword, szPassword, true);
		// Use multilayered encryption to encrypt the text with the users password
		m_enc.MultiEncryptBuffer (&m_memEncpassword, szPassword, true, 2000);

		SaveConfig (); // Save the configuration file which will have the Biblical text above encrypted with the users password.

		m_vdmanager.Initialise (szPassword);

		AddTrayIcon();
	} else {
	
		if (origin == ORIGIN_FULLPASSWINDOW) {
			strcpy_s (szPassword, SIZE_STRING, m_passwindowfull.GetLastPassword ());
		} else {
			strcpy_s (szPassword, SIZE_STRING, m_passwindow.GetLastPassword ());
		}
		

		// Now take a copy of the encrypted buffer from the config file. We need to attempt decryption using the users password supplied in order to validate the password.
		MemoryBuffer memTemp;
		memTemp.SetSize (m_memEncpassword.GetSize());
		memTemp.Write ((BYTE *) m_memEncpassword.GetBuffer(), 0, m_memEncpassword.GetSize());

		// Now decrypt the temporary buffer using the supplied password
		// First check if the encrypted buffer is using the newer multi layered encryption, if not then use the older method of decryption
		// this is so that cededrive remains backward compatible with previous versions of the drivelist.dat file.
		if (m_enc.IsMultiLayeredSignaturePresent (&memTemp) == true) { 
			m_enc.MultiEncryptBuffer (&memTemp, szPassword, false, 2000); // Using 2000 layers of encryption.
		} else {
			m_enc.EncryptBuffer (&memTemp, szPassword, false);
		}

		
		// Now check the result of the decryption against what we expect. - The Biblical Psalm.
		char szDecrypted[SIZE_STRING];
		ZeroMemory (szDecrypted, SIZE_STRING);
		strncpy_s (szDecrypted, SIZE_STRING, (char *) memTemp.GetBuffer(), memTemp.GetSize());		

		// Did it match?
		if (strcmp (szDecrypted, szEnctext) == 0) {
			// Password ok
			m_vdmanager.Initialise (szPassword);
			RefreshListView();

			AddTrayIcon();

			// Now mount all drives set to mount at startup only
			MountAllDrives(true);

			m_wrongpasswordcount = 0;

			#ifndef _FORLAUNCH
			if (origin == ORIGIN_FULLPASSWINDOW)
			{
				EnableKeyboardShortcuts();
				KillTimer (m_hwnd, IDT_ACTIVATEFULLPASSWINDOW);
				KillTimer (m_hwnd, IDT_TASKMANAGERCHECK);

				m_passwindowfull.Hide ();
			}
			#endif
		} else {
			m_wrongpasswordcount++;

			if (origin == ORIGIN_FULLPASSWINDOW)
			{
				m_passwindowfull.ShowAccessDenied ();
			} else {
				MessageBox (NULL, "Access Denied. Incorrect password.", "Incorrect Password", MB_ICONEXCLAMATION | MB_OK);		
			}
			
			if (origin != ORIGIN_FULLPASSWINDOW) {
				if (m_wrongpasswordcount > 3) {
					MessageBox (NULL, "You have entered the Incorrect password over 3 times. Goodbye.", "Maximum attempts reached", MB_ICONEXCLAMATION | MB_OK);
					PostQuitMessage(0);
				} else {
					// Show the password window again
					if (origin == ORIGIN_FULLPASSWINDOW)
					{
						m_passwindowfull.Initialise (m_hwnd, 0);
						m_passwindowfull.SetEncryptMode (false);
						m_passwindowfull.Show ();
					} else {
						m_passwindow.Initialise (m_hwnd, 0);
						m_passwindow.SetEncryptMode (false);
						m_passwindow.Show ();
					}


					ActivatePasswindow ();
					//m_passwindow.SetWindowFocus ();
				}
			}
		}
	}

	// Securely erase the encrypted text we encrypt
	SecureZeroMemory (szEnctext, SIZE_STRING);

	if (m_dlDrivelist.GetNumItems () == 0) {
		Show ();
	} else {
		if (m_registry.GetSetting_ShowMainWindowOnStartup () == true) {
			Show ();
		}
	}
}

void MainWindow::RefreshListView ()
{
	// Now add a few items to the list view
	LVITEM lvI;
	int index = 0;	
	SingleDriveInfo* pinfo;

	ListView_DeleteAllItems (m_hwndlistview);

	lvI.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM | LVIF_STATE; 
	lvI.state = 0; 
	lvI.stateMask = 0; 	

	for (index=0;index < m_dlDrivelist.GetNumItems ();index++) {
		pinfo = (SingleDriveInfo *) m_dlDrivelist.GetItem (index);

		lvI.iItem = index;						
		lvI.iImage = 0; // Set the image number in the image list for other images				
		lvI.iSubItem = 0;
		lvI.lParam = 0;
		lvI.pszText = LPSTR_TEXTCALLBACK; 							  
		ListView_InsertItem(m_hwndlistview, &lvI);
	}

	DecideWelcomeVisible ();
}

int MainWindow::LV_GetFirstSelectedIndex (HWND hwndlv)
{
	int iNumitems = ListView_GetItemCount (hwndlv);
	int index = 0;
	LVITEM lvi;

	//m_pdiag->OutputInt ("ListView Item count is: ", iNumitems);

	for (index=0;index<iNumitems;index++) {
		lvi.mask = LVIF_STATE;
		lvi.stateMask = LVIS_SELECTED;
		lvi.iItem = index;
		lvi.iSubItem = 0;

		ListView_GetItem (hwndlv, &lvi);

		//m_pdiag->OutputText ("Item Text: ", lvi.pszText);
		if (lvi.state == LVIS_SELECTED) {
			return index;
		}
	}

	return -1;
}

void MainWindow::OnSysTray (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	UINT uMouseMsg;

	uMouseMsg = (UINT) lParam;
	if (uMouseMsg == WM_RBUTTONDOWN) {
		POINT mousepoints;
		GetCursorPos (&mousepoints);
		SetForegroundWindow (m_nid.hWnd); // Without this, you can't dismiss the popup menu without clicking an option (apparently a bug in windows)
		TrackPopupMenuEx (m_mainmenu.m_hTrayMenu, TPM_LEFTALIGN | TPM_LEFTBUTTON, mousepoints.x, mousepoints.y, hWnd, NULL);
	}

	if (uMouseMsg == WM_LBUTTONDBLCLK) {
		
		if (m_bTrialmode == true) {
			MessageBox (NULL, "Thank you for trying CedeDrive, a secure virtual drive encryption solution for desktops, laptops and removable drives. For full purchase information please visit http://www.cedesoft.com.\n\nPlease note this trial version is limited to 1 virtual drive size of 100MB, and drives are disconnected after 20 minutes.", "Trial Version Information", MB_OK | MB_ICONINFORMATION);
		}
		Show ();
	}
}

void MainWindow::OnCryptEvent (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	// Notify the communications object of an encryption event message.
	
}

void MainWindow::OnNotify (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	switch (((LPNMHDR) lParam)->code)
	{
		case NM_CLICK:
		{
			int index = LV_GetFirstSelectedIndex(m_hwndlistview);


			if (index != -1) {

				SingleDriveInfo tempDrive;
				SingleDriveInfo *pinfo = (SingleDriveInfo *) m_dlDrivelist.GetItem (index);					
				memcpy (&tempDrive, (SingleDriveInfo *) pinfo, sizeof (SingleDriveInfo));
				SetInfoPanel (tempDrive, true);
				m_mainmenu.SetAllDisabled (false);

				if (pinfo->bUsingseperatepassword == true) {
					m_mainmenu.SetDeleteDisabled (true);
					m_mainmenu.SetRedirectionDisabled (true);
				} else {
					m_mainmenu.SetDeleteDisabled (false);
					m_mainmenu.SetRedirectionDisabled (false);
				}

			} else {
				m_mainmenu.SetAllDisabled (true);
				SingleDriveInfo tempDrive;
				SetInfoPanel (tempDrive, false);
			}
		}
		break;
		case NM_RCLICK:
		{
				int index = LV_GetFirstSelectedIndex(m_hwndlistview);

				if (index != -1) {

					SingleDriveInfo tempDrive;
					SingleDriveInfo *pinfo = (SingleDriveInfo *) m_dlDrivelist.GetItem (index);					
					memcpy (&tempDrive, (SingleDriveInfo *) pinfo, sizeof (SingleDriveInfo));
					SetInfoPanel (tempDrive, true);

					m_mainmenu.SetAllDisabled (false);

					if (pinfo->bUsingseperatepassword == true) {
						m_mainmenu.SetDeleteDisabled (true);
						m_mainmenu.SetRedirectionDisabled (true);
					} else {
						m_mainmenu.SetDeleteDisabled (false);
						m_mainmenu.SetRedirectionDisabled (false);
					}

				} else {

					m_mainmenu.SetAllDisabled (true);

					SingleDriveInfo tempDrive;
					SetInfoPanel (tempDrive, false);
				}

				POINT mousepoints;
				GetCursorPos (&mousepoints);
				//SetForegroundWindow (hWnd); // Without this, you can't dismiss the popup menu without clicking an option (apparently a bug in windows)
				TrackPopupMenuEx (m_mainmenu.m_hPopupMenu, TPM_LEFTALIGN | TPM_LEFTBUTTON, mousepoints.x, mousepoints.y, hWnd, NULL);
		}
		break;
		case NM_DBLCLK:
		{
			OutputText ("Double Click on ListView!");
			

			int iCuritem = LV_GetFirstSelectedIndex (m_hwndlistview);

			if (iCuritem != -1) { // If something was selected
			
				//pinfo = (SingleFileInfo *) m_dlListviewfiles.GetItem (iCuritem);

				/*
				if (pinfo->bIsDir == true) {
					m_dlFolderhistory.AddItem (m_szCurrentfolder, SIZE_STRING, false);
					strcat_s (m_szCurrentfolder, SIZE_STRING, "\\");
					strcat_s (m_szCurrentfolder, SIZE_STRING, pinfo->szName);
					RefreshListView (m_szCurrentfolder);
				}
				*/
			}

		}
		break;
		case LVN_GETDISPINFO:
		{
			NMLVDISPINFO *lpinfo;		

			lpinfo = (NMLVDISPINFO *) lParam;
			
			//pfinfo = (SingleFileInfo *) m_dlListviewfiles.GetItem (lpinfo->item.iItem);
			SingleDriveInfo *pdinfo;

			pdinfo = (SingleDriveInfo *) m_dlDrivelist.GetItem (lpinfo->item.iItem);


			switch (lpinfo->item.iSubItem)
			{
				case 0:
				{
					lpinfo->item.pszText = pdinfo->szName;
				}
				break;

				case 1:
				{
					char szSize[SIZE_STRING];
					ZeroMemory (szSize, SIZE_STRING);
					ltoa (pdinfo->lDiskSizeMegs, szSize, 10);

					lpinfo->item.pszText = szSize;
				}
				break;

				case 2:
				{
					lpinfo->item.pszText = pdinfo->szDriveLetter;
				}
				break;
			}

		}
		break;
	}

	return;	
}

/*
#define IDM_TRAY_EXIT				712
#define IDM_TRAY_SHOW			713
#define IDM_TRAY_MOUNTALL	714
#define IDM_TRAY_UNMOUNTALL	715
*/

void MainWindow::MountAllDrives (bool startuponly)
{
	bool bError = false;
	char szDetailedError[SIZE_LARGESTRING];
	ZeroMemory (szDetailedError, SIZE_LARGESTRING);

	char szCurrentError[SIZE_STRING];
	char szCurrentDrive[SIZE_STRING];

	char szFullError[SIZE_LARGESTRING];	

	// If we need to wait for the service to startup then wait
	WaitForCedeDriveService ();

	for (int d=0;d<m_dlDrivelist.GetNumItems();d++) {
		
		SingleDriveInfo tempDrive;
		SingleDriveInfo *pinfo = (SingleDriveInfo *) m_dlDrivelist.GetItem (d);					
		memcpy (&tempDrive, (SingleDriveInfo *) pinfo, sizeof (SingleDriveInfo));

		if (startuponly == true) {
			if (tempDrive.bMountStartup == true) {
				if (m_vdmanager.MountVirtualDisk (tempDrive) != 0) {

					ZeroMemory (szCurrentError, SIZE_STRING);
					ZeroMemory (szCurrentDrive, SIZE_STRING);

					m_vdmanager.GetMountError (szCurrentError);									

					strcpy_s (szCurrentDrive, SIZE_STRING, tempDrive.szName);
					strcat_s (szCurrentDrive, SIZE_STRING, " (");
					strcat_s (szCurrentDrive, SIZE_STRING, tempDrive.szDriveLetter);
					strcat_s (szCurrentDrive, SIZE_STRING, ") : ");
				
					strcat_s (szDetailedError, SIZE_LARGESTRING, "\r\n");
					strcat_s (szDetailedError, SIZE_LARGESTRING, szCurrentDrive);
					strcat_s (szDetailedError, SIZE_LARGESTRING, "\r\n");
					strcat_s (szDetailedError, SIZE_LARGESTRING, szCurrentError);
					strcat_s (szDetailedError, SIZE_LARGESTRING, "\r\n");

					bError = true;
				} else {
					//ShellExecute (m_hwnd, "explore", tempDrive.szDriveLetter, NULL, NULL, SW_SHOWNORMAL);
				}
			}
		} else {

			if (m_vdmanager.MountVirtualDisk (tempDrive) != 0) {
				
				ZeroMemory (szCurrentError, SIZE_STRING);
				ZeroMemory (szCurrentDrive, SIZE_STRING);

				m_vdmanager.GetMountError (szCurrentError);			

				strcpy_s (szCurrentDrive, SIZE_STRING, tempDrive.szName);
				strcat_s (szCurrentDrive, SIZE_STRING, " (");
				strcat_s (szCurrentDrive, SIZE_STRING, tempDrive.szDriveLetter);
				strcat_s (szCurrentDrive, SIZE_STRING, ") : ");
				
				strcat_s (szDetailedError, SIZE_LARGESTRING, "\r\n");
				strcat_s (szDetailedError, SIZE_LARGESTRING, szCurrentDrive);
				strcat_s (szDetailedError, SIZE_LARGESTRING, "\r\n");
				strcat_s (szDetailedError, SIZE_LARGESTRING, szCurrentError);
				strcat_s (szDetailedError, SIZE_LARGESTRING, "\r\n");
				
				bError = true;
			} else {				
				//ShellExecute (m_hwnd, "explore", tempDrive.szDriveLetter, NULL, NULL, SW_SHOWNORMAL);
			}
		}		
	}

	if (bError == false) {
		// Show success message
		if (startuponly == false) {
			MessageBox (NULL, "All encrypted drives mounted successfully.", "Mount successful", MB_OK | MB_ICONINFORMATION);
		}
	} else {
		ZeroMemory (szFullError, SIZE_LARGESTRING);
		strcpy_s (szFullError, SIZE_LARGESTRING, "There was a problem mounting one or more of your encrypted drives. More information:");
		strcat_s (szFullError, SIZE_LARGESTRING, "\r\n");
		strcat_s (szFullError, SIZE_LARGESTRING, szDetailedError);

		MessageBox (NULL, szFullError, "Mounting error", MB_OK | MB_ICONEXCLAMATION);
	}
}

void MainWindow::PrepareCedeCryptPath ()
{
	ZeroMemory (m_szCedeCryptPath, SIZE_STRING);
	//m_registry.WriteString ("Software\\CedeSoft\\CedeCrypt", "ShellExtensionPath", szModulepath);
	if (m_registry.DoesValueExistCU ("Software\\CedeSoft\\CedeCrypt", "ShellExtensionPath", REG_SZ) == true) {
		//m_bCedeCryptPathfound = true;
		strcpy_s (m_szCedeCryptPath, SIZE_STRING, m_registry.ReadStringCU ("Software\\CedeSoft\\CedeCrypt", "ShellExtensionPath"));

		MessageBox (NULL,m_szCedeCryptPath, "Caption", MB_OK);
	} else {
		//m_bCedeCryptPathfound = false;
		MessageBox (NULL, "Reg Key NOT found!", "Caption", MB_OK);
	}
}

unsigned long long MainWindow::GetFreeDiskSpace (char *szDriveletter)
{
	unsigned long long lLimit = 0;
	unsigned long long lFree = 0;
	unsigned long long lSize = 0;
	unsigned long long lTotal = 0;
	

	char szDrive[SIZE_NAME];
	ZeroMemory (szDrive, SIZE_NAME);
	strcpy_s (szDrive, SIZE_NAME, szDriveletter);
	strcat_s (szDrive, SIZE_NAME, ":\\");

	//double fAmount = 0;
	//double fAvail = 0;
	//int iPixelprogress = 0;
	//int iPercent = 0;

	//lSize = 3000000000;

	//char szVolumename[SIZE_STRING];
	//ZeroMemory (szVolumename, SIZE_STRING);

	//DWORD dwSizevolname = SIZE_STRING;

	//GetVolumeInformation (szDrive, szVolumename, dwSizevolname, NULL, NULL, NULL, NULL, NULL);

	if (GetDiskFreeSpaceEx (szDrive, (PULARGE_INTEGER) &lFree, (PULARGE_INTEGER) &lLimit, (PULARGE_INTEGER) &lTotal) == 0) {
		//m_pdiag->OutputInt ("GetDiskFreeSpace failed: Error: ", GetLastError ());
	}

	return lFree;
}

DWORD WINAPI MainWindow::ConvertDriveProc (PVOID pParam)
{
	// The worker thread in which the drive conversion takes place

	MainWindow *pmainwnd = (MainWindow *) pParam;
	pmainwnd->ConvertRemovableDrive ();

	//PostMessage (pmainwnd->m_hwnd, WM_UICOMMAND, CID_PSTSCANCOMPLETE, 0);

	return 0;
}

void MainWindow::StartDriveConverter ()
{
	// Create the separate thread which converts the drive
	DWORD dwThreadID;
	m_hConvertthread = CreateThread (NULL, 0, ConvertDriveProc, (MainWindow *) this, 0, &dwThreadID);
}

bool MainWindow::IsDriveConverted (char *szDriveletter)
{
	char szEvdpath[SIZE_STRING];
	ZeroMemory (szEvdpath, SIZE_STRING);

	strcpy_s (szEvdpath, SIZE_STRING, szDriveletter);
	strcat_s (szEvdpath, SIZE_STRING, ":\\EncryptedDisk.evd");

	if (FileExists (szEvdpath) == true) {
		return true;
	} else {
		return false;
	}
}

void MainWindow::ConvertRemovableDrive ()
{
	m_convertwindow.SetButtonEnabled (false);
	m_convertwindow.SetInputVisible (false);
	m_convertwindow.SetStagesVisible (true);
	m_convertwindow.InvalidateWindow ();

	HWND converthwnd = m_convertwindow.GetHWND();

	SetActiveWindow (converthwnd);
	

	char szDriveletter[SIZE_NAME];
	ZeroMemory (szDriveletter, SIZE_NAME);
	strncpy_s (szDriveletter, SIZE_NAME, m_convertdriveinfo.szDriveLetter, 1);

	if (IsDriveConverted (szDriveletter) == true) {
		m_convertwindow.InvalidateWindow ();
		m_convertwindow.Reset ();
		m_convertwindow.SetStagesVisible (false);
		m_convertwindow.SetInputVisible (true);
		m_convertwindow.SetButtonEnabled (true);
		//MessageBox (converthwnd, "This removable storage device has already been converted into an encrypted device. The conversion process will now abort.", "Already encrypted.", MB_OK | MB_ICONINFORMATION);
		m_convertwindow.ShowMessage ("This removable storage device has already been converted into an encrypted device. The conversion process will now abort.", "Already encrypted.", true);
		return;
	}

	char szFreeDriveletter[SIZE_NAME];
	ZeroMemory (szFreeDriveletter, SIZE_NAME);
	
	char szFreeDrivelettercolon[SIZE_NAME];
	ZeroMemory (szFreeDrivelettercolon, SIZE_NAME);

	if (m_vdmanager.GetFreeDriveLetter (szFreeDriveletter) == false) {
		// no available drive letter on this system, abort!
		m_convertwindow.InvalidateWindow ();
		m_convertwindow.Reset ();
		m_convertwindow.SetStagesVisible (false);
		m_convertwindow.SetInputVisible (true);
		m_convertwindow.SetButtonEnabled (true);

		//MessageBox (converthwnd, "There are no available drive letters available on this system. Conversion cannot continue.", "Insufficient drive letters", MB_OK | MB_ICONEXCLAMATION);
		m_convertwindow.ShowMessage ("There are no available drive letters available on this system. Conversion cannot continue.", "Insufficient drive letters", true);
		return;
	}

	// Create the file header
	SingleDriveInfo drivenameinfo;
	ZeroMemory (drivenameinfo.szName, SIZE_STRING);
	strcpy_s (drivenameinfo.szName, SIZE_STRING, "Encrypted Removable Disk");

	ZeroMemory (drivenameinfo.szDescription, SIZE_STRING);
	strcpy_s (drivenameinfo.szDescription, SIZE_STRING, "Encrypted Removable Disk for use with Flash Drives and Portable Hard Drives.");

	char szUserpassword[SIZE_STRING];
	ZeroMemory (szUserpassword, SIZE_STRING);
	m_convertwindow.GetPassword (szUserpassword);

	EVDFileHeader fileheader;
	CreateEVDFileHeader(m_convertwindow.GetRecoveryChoice (), &drivenameinfo, szUserpassword, &fileheader, converthwnd);

	// if we got here then we have an available drive letter to use for later on
	// but we need to add a colon to it.
	strcpy_s (szFreeDrivelettercolon, SIZE_NAME, szFreeDriveletter);
	strcat_s (szFreeDrivelettercolon, SIZE_NAME, ":");
	
	m_convertwindow.SetStage (STAGE_BACKUP, STATUS_INPROGRESS);
	if (m_drivebackup.BackupDrive (szDriveletter) == true) {

		m_convertwindow.SetStage (STAGE_BACKUP, STATUS_SUCCESS);
		m_convertwindow.SetStageProgress (STAGE_BACKUP, 100);
		m_convertwindow.SetStage (STAGE_FORMAT, STATUS_INPROGRESS);
		m_convertwindow.InvalidateWindow ();

		//if (m_driveformat.QuickFormatDrive (szDriveletter) == false) {
		if (m_driveformat.ConvertDriveToNTFS (szDriveletter) == false) {

			m_convertwindow.SetStage (STAGE_FORMAT, STATUS_FAILED);
			char szReason[SIZE_STRING];
			m_driveformat.GetErrorReason (szReason, SIZE_STRING);
			MessageBox (converthwnd, szReason, "Format Failed.", MB_OK);
			m_convertwindow.SetButtonMode (false);
			m_convertwindow.SetButtonEnabled (true);
			m_convertwindow.SetStagesComplete (false);

		} else {
			//MessageBox (NULL, "Format was successful", "Format", MB_OK);
			m_convertwindow.SetStage (STAGE_FORMAT, STATUS_SUCCESS);
			m_convertwindow.SetStage (STAGE_CREATE, STATUS_INPROGRESS);
			m_convertwindow.InvalidateWindow ();
			
			// Get the amount of free disk space on the target drive
			unsigned long long lFree = GetFreeDiskSpace (szDriveletter);
			double fFreemegs = (double) lFree / (1024*1024);

			OutputInt ("Free Disk Space (MB): ", fFreemegs);

			// Take 10 megs off just to be safe
			if (fFreemegs > 10) {
				fFreemegs-=10;
			}

			if (m_bTrialmode == true) {
				fFreemegs = 100; // Set the converted drive to 100MB only if we are in trial mode.
			} 

			OutputInt ("Useable Free Disk Space (MB): ", fFreemegs);

			if (fFreemegs > 0) {
				
				SingleDriveInfo tempdrive;
				tempdrive.lDiskSizeMegs = fFreemegs;
				
				tempdrive.bUsingseperatepassword = true;
				ZeroMemory (tempdrive.szPassword, SIZE_STRING);
				strcpy_s (tempdrive.szPassword, SIZE_STRING, m_szConvertpassword);

				ZeroMemory (tempdrive.szPath, SIZE_STRING);
				strcpy_s (tempdrive.szPath, SIZE_STRING, szDriveletter);
				strcat_s (tempdrive.szPath, SIZE_STRING, ":\\EncryptedDisk.evd");

				tempdrive.bFormatdriveaftermount = true;
				ZeroMemory (tempdrive.szVolumename, SIZE_STRING);
				strcpy_s (tempdrive.szVolumename, SIZE_STRING, "Encrypted Memory Stick");



				if (m_vdmanager.AllocateStandaloneVirtualDisk (tempdrive, fileheader.GetEVDHeader ()) == true) {					
					m_convertwindow.InvalidateWindow ();
					m_convertwindow.SetStage (STAGE_CREATE, STATUS_SUCCESS);
					m_convertwindow.SetStageProgress (STAGE_CREATE, 100);
					m_convertwindow.SetStage (STAGE_RESTORE, STATUS_INPROGRESS);

					//MessageBox (NULL, "Virtual Disk Allocated ok.", "Allocation ok.", MB_OK);
					
					ZeroMemory (tempdrive.szDriveLetter, SIZE_NAME);
					strcpy_s (tempdrive.szDriveLetter, SIZE_NAME, szFreeDrivelettercolon); // Need to enumerate available drive letters!!!
					
					memcpy (&m_converttempdrive, &tempdrive, sizeof (SingleDriveInfo));

					// Ask the main thread to mount our temporary drive, then suspend the thread
					//PostMessage (m_hwnd, WM_UICOMMAND, CID_CONVERTMOUNTREQUEST, 0);
					//SuspendThread (m_hConvertthread);
					int ires = m_vdmanager.MountVirtualDisk (tempdrive);

					char szMessage[SIZE_STRING];
					ZeroMemory (szMessage, SIZE_STRING);

					if (ires == 0) {

						if (m_drivebackup.RestoreDrive (szDriveletter, szFreeDriveletter) == true) {
							m_convertwindow.InvalidateWindow ();
							m_convertwindow.SetStage (STAGE_RESTORE, STATUS_SUCCESS);
							m_convertwindow.SetStageProgress (STAGE_RESTORE, 100);
							m_convertwindow.SetStage (STAGE_COMPLETION, STATUS_INPROGRESS);

							MemoryBuffer memLauncher;
							MemoryBuffer memElevatedLauncher;

							if (memLauncher.ReadFromResource (MAKEINTRESOURCE(IDB_LAUNCHERAPP)) == 0) {
								
								if (memElevatedLauncher.ReadFromResource (MAKEINTRESOURCE (IDB_ELEVATEDLAUNCHER)) == 0) {
									// Now encode the removable drive volume serial into the launcher so it will only
									// run from that drive.
									char szProperDriveletter[SIZE_NAME];
									ZeroMemory (szProperDriveletter, SIZE_NAME);
									strcpy_s (szProperDriveletter, SIZE_NAME, szDriveletter);
									strcat_s (szProperDriveletter, SIZE_NAME, ":\\");

									m_canvas.WriteVolumeSerial (szProperDriveletter, &memLauncher);

									// If this is a full version of the product, then encode the license key into this
									// executable so that it will not run in trial mode.
									if (m_bTrialmode == false) {
										m_canvas.WriteUserKey (m_szUserlicense, &memLauncher);
									}

									// Prepare the save path for the launcher
									char szLauncherpath[SIZE_STRING];
									ZeroMemory (szLauncherpath, SIZE_STRING);
									strcpy_s (szLauncherpath, SIZE_STRING, szDriveletter);
									strcat_s (szLauncherpath, SIZE_STRING, ":\\cdldr");
								
									char szElevatedLaunchpath[SIZE_STRING];
									ZeroMemory (szElevatedLaunchpath, SIZE_STRING);
									strcpy_s (szElevatedLaunchpath, SIZE_STRING, szDriveletter);
									strcat_s (szElevatedLaunchpath, SIZE_STRING, ":\\CedeDriveLaunch.exe");

									if (memLauncher.SaveToFile (szLauncherpath) == true) {
										
										if (memElevatedLauncher.SaveToFile (szElevatedLaunchpath) == true) {
											m_drivebackup.DeleteRestore (szDriveletter);
											m_vdmanager.UnmountVirtualDrive (tempdrive);
											m_convertwindow.InvalidateWindow ();
											m_convertwindow.SetStage (STAGE_COMPLETION, STATUS_SUCCESS);
											m_convertwindow.SetButtonMode (false);
											m_convertwindow.SetButtonEnabled (true);
											m_convertwindow.SetStagesComplete (true);
									
											//MessageBox (converthwnd, "Removable storage converted successfully. You may now use this drive in any PC. \n\nRemember to run the CedeDriveLaunch.exe application in order to mount your encrypted drive.", "Conversion successful", MB_OK | MB_ICONINFORMATION);

											m_convertwindow.ShowMessage ("Removable storage converted successfully. You may now use this drive in any PC. \n\nRemember to run the CedeDriveLaunch.exe application in order to mount your encrypted drive.", "Conversion successful", true);
										} else {
											m_convertwindow.SetButtonMode (false);
											m_convertwindow.SetButtonEnabled (true);
											m_convertwindow.SetStagesComplete (false);
											//MessageBox (converthwnd, "Unable to save elevated launcher to removable storage! Conversion failed.", "Unable to save launcher", MB_OK | MB_ICONEXCLAMATION);

											m_convertwindow.ShowMessage ("Unable to save elevated launcher to removable storage! Conversion failed.", "Unable to save launcher", false);
										}									
										
									} else {
										m_convertwindow.SetButtonMode (false);
										m_convertwindow.SetButtonEnabled (true);
										m_convertwindow.SetStagesComplete (false);
										//MessageBox (converthwnd, "Unable to save launcher to removable storage! Conversion failed.", "Unable to save launcher", MB_OK | MB_ICONEXCLAMATION);

										m_convertwindow.ShowMessage ("Unable to save launcher to removable storage! Conversion failed.", "Unable to save launcher", false);
									}

								} else {
									m_convertwindow.SetButtonMode (false);
									m_convertwindow.SetButtonEnabled (true);
									m_convertwindow.SetStagesComplete (false);
									
									//MessageBox (converthwnd, "Unable to read elevated launcher resource! Conversion failed.", "Launcher Resource Error", MB_OK | MB_ICONEXCLAMATION);	

									m_convertwindow.ShowMessage ("Unable to read elevated launcher resource! Conversion failed.", "Launcher Resource Error", false);
								}



							} else {
								m_convertwindow.SetButtonMode (false);
								m_convertwindow.SetButtonEnabled (true);
								m_convertwindow.SetStagesComplete (false);
								//MessageBox (converthwnd, "Unable to read launcher resource! Conversion failed.", "Launcher Resource Error", MB_OK | MB_ICONEXCLAMATION);	

								m_convertwindow.ShowMessage ("Unable to read launcher resource! Conversion failed.", "Launcher Resource Error", false);
							}
							
							//MessageBox (NULL, "Conversion complete.", "Success", MB_OK);
						} else {
							m_convertwindow.SetButtonMode (false);
							m_convertwindow.SetButtonEnabled (true);
							m_convertwindow.SetStagesComplete (false);
							m_convertwindow.SetStage (STAGE_RESTORE, STATUS_FAILED);
							//MessageBox (converthwnd, "Restoration failed.", "Failed.", MB_OK);
							m_convertwindow.ShowMessage ("Restoration failed.", "Failed.", false);
						}
						//sprintf_s (szMessage, "Encrypted Drive '%s' was mounted successfully as drive %s.", tempdrive.szName, tempdrive.szDriveLetter);
						//MessageBox (NULL, szMessage, "Mount Successful", MB_OK | MB_ICONINFORMATION);

					} else {						
						m_convertwindow.SetButtonMode (false);
						m_convertwindow.SetButtonEnabled (true);
						m_convertwindow.SetStagesComplete (false);
						m_convertwindow.SetStage (STAGE_RESTORE, STATUS_FAILED);

						m_vdmanager.GetMountError (szMessage);						
						//MessageBox (converthwnd, szMessage, "Mount Failed", MB_OK | MB_ICONEXCLAMATION);						
						m_convertwindow.ShowMessage (szMessage, "Mount Failed", false);
					}


				} else {
					m_convertwindow.SetButtonMode (false);
					m_convertwindow.SetButtonEnabled (true);
					m_convertwindow.SetStagesComplete (false);
					m_convertwindow.SetStage (STAGE_CREATE, STATUS_FAILED);
					//MessageBox (converthwnd, "Allocation Failed.", "Allocation Failed.", MB_OK);
					m_convertwindow.ShowMessage ("Allocation Failed.", "Allocation Failed.", false);
				}

			} else {
				m_convertwindow.SetButtonMode (false);
				m_convertwindow.SetButtonEnabled (true);
				m_convertwindow.SetStagesComplete (false);
				m_convertwindow.SetStage (STAGE_CREATE, STATUS_FAILED);
				//MessageBox (converthwnd, "Not enough free space for encrypted disk.", "No Free space.", MB_OK);
				m_convertwindow.ShowMessage ("Not enough free space for encrypted disk.", "No Free space.", false);
			}


			/*
			if (m_drivebackup.RestoreDrive ("H") == true) {

				MessageBox (NULL, "Restoration Successful", "Drive Restore", MB_OK);

				if (m_drivebackup.DeleteRestore ("H") == true) {
					MessageBox (NULL, "Cleanup Successful", "Drive Restore", MB_OK);
				} else {
					MessageBox (NULL, "Cleanup Failed.", "Drive Restore", MB_OK);
				}

				
			} else {
				MessageBox (NULL, "Restoration Failed.", "Drive Restore", MB_OK);
			}
			*/

		}
	} else {
		m_convertwindow.SetButtonMode (false);
		m_convertwindow.SetButtonEnabled (true);
		m_convertwindow.SetStage (STAGE_BACKUP, STATUS_FAILED);
	}
}



int MainWindow::MountTemporaryDrive (SingleDriveInfo driveinfo)
{	
	// If we need to wait for the service to startup then wait
	WaitForCedeDriveService ();

	int ires = m_vdmanager.MountVirtualDisk (driveinfo);

	char szMessage[SIZE_STRING];
	ZeroMemory (szMessage, SIZE_STRING);

	if (ires != 0) {
		
		m_vdmanager.GetMountError (szMessage);		
		MessageBox (NULL, szMessage, "Mount Failed", MB_OK | MB_ICONEXCLAMATION);
	}

	return ires;
}

bool MainWindow::SaveRecoveryFile (char *szInitialname, char *szOutfilename, HWND windowhandle)
{
	char szFileonly[SIZE_STRING];
	ZeroMemory (szFileonly, SIZE_STRING);
	strcpy_s (szFileonly, SIZE_STRING, szInitialname);

	char szOutfile[SIZE_STRING];
	ZeroMemory (szOutfile, SIZE_STRING);

	ZeroMemory (szOutfile, SIZE_STRING);	
	strcpy_s (szOutfile, SIZE_STRING, szInitialname);

	OPENFILENAME ofn;
	
	ZeroMemory(&ofn, sizeof(ofn));
	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = windowhandle;
	ofn.lpstrFile = szOutfile;
	//ofn.lpstrFile[0] = '\0';
	ofn.nMaxFile = SIZE_STRING;
	ofn.lpstrFilter = "Recovery Key Files (*.dat)\0*.dat\0All Files (*.*)\0*.*\0";
	ofn.nFilterIndex = 0;
	ofn.lpstrFileTitle = szFileonly;
	ofn.nMaxFileTitle = SIZE_STRING;
	ofn.lpstrInitialDir = NULL;
	ofn.Flags = OFN_OVERWRITEPROMPT;
	
	if (GetSaveFileName (&ofn) != 0) {
		ZeroMemory (szOutfilename, SIZE_STRING);
		strcpy_s (szOutfilename, SIZE_STRING, szOutfile);

		// Check for the presence of a .dat or .DAT extension
		bool datextensionpresent = false;

		char szExtension[SIZE_NAME];
		ZeroMemory (szExtension, SIZE_NAME);
		strcpy_s (szExtension, SIZE_NAME, szOutfile+(strlen(szOutfile)-4));

		if (strcmp (szExtension, ".dat") == 0 || strcmp (szExtension, ".DAT") == 0)
		{
			datextensionpresent = true;
		}

		if (datextensionpresent == false) {
			strcat_s (szOutfilename, SIZE_STRING, ".dat");
		}

		return true;
	} else {
		return false;
	}
}

bool MainWindow::OpenSingleEVDFile (char *szOutfilename)
{
	
	OPENFILENAME ofn;
	ZeroMemory(&ofn, sizeof(ofn));

	char szInputfile[SIZE_STRING];
	char szInputfiletitle[SIZE_STRING];

	ZeroMemory (szInputfile, SIZE_STRING);
	ZeroMemory (szInputfiletitle, SIZE_STRING);

	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = m_hwnd;
	ofn.lpstrFile = szInputfile;
	ofn.lpstrFileTitle = szInputfiletitle;
	ofn.lpstrFile[0] = '\0';
	ofn.nMaxFile = SIZE_STRING;
	ofn.nMaxFileTitle = SIZE_STRING;

	ofn.lpstrFilter = "Encrypted Virtual Disks (*.evd)\0*.evd\0All Files (*.*)\0*.*\0";

	ofn.nFilterIndex = 1;	
	ofn.lpstrInitialDir = NULL;
	ofn.Flags = OFN_OVERWRITEPROMPT;
	
	if (GetOpenFileName (&ofn) != 0) {
		ZeroMemory (szOutfilename, SIZE_STRING);
		strcpy_s (szOutfilename, SIZE_STRING, szInputfile);
		return true;
	} else {
		return false;
	}
}

bool MainWindow::OpenSingleDATFile (char *szOutfilename)
{
	
	OPENFILENAME ofn;
	ZeroMemory(&ofn, sizeof(ofn));

	char szInputfile[SIZE_STRING];
	char szInputfiletitle[SIZE_STRING];

	ZeroMemory (szInputfile, SIZE_STRING);
	ZeroMemory (szInputfiletitle, SIZE_STRING);

	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = m_hwnd;
	ofn.lpstrFile = szInputfile;
	ofn.lpstrFileTitle = szInputfiletitle;
	ofn.lpstrFile[0] = '\0';
	ofn.nMaxFile = SIZE_STRING;
	ofn.nMaxFileTitle = SIZE_STRING;

	ofn.lpstrFilter = "Recovery Key Files (*.dat)\0*.dat\0All Files (*.*)\0*.*\0";

	ofn.nFilterIndex = 1;	
	ofn.lpstrInitialDir = NULL;
	ofn.Flags = OFN_OVERWRITEPROMPT;
	
	if (GetOpenFileName (&ofn) != 0) {
		ZeroMemory (szOutfilename, SIZE_STRING);
		strcpy_s (szOutfilename, SIZE_STRING, szInputfile);
		return true;
	} else {
		return false;
	}
}

unsigned long MainWindow::GetSectorMultiple (unsigned long lValue)
{
	// Takes a value and returns a value that is the nearest sector-aligned multiple
	// of the supplied value
	unsigned long currentMultiple = 1024;
	if (lValue > 0) {

		while (lValue > currentMultiple) {
			currentMultiple = currentMultiple * 2;
		}

		return currentMultiple;

	} else {
		return 0;
	}
}

bool MainWindow::Replacestring (char *szStringtoreplace, char *szStringtosearchfor, char *szReplacement)
{
	char szFull[SIZE_STRING];
	char szSearch[SIZE_STRING];
	char szTemp[SIZE_STRING];
	int c = 0;

	char szFinal[SIZE_STRING];

	if (strlen (szStringtoreplace) > 0) {
		if (strlen (szStringtoreplace) >= strlen (szStringtosearchfor)) {
			
			ZeroMemory (szFull, SIZE_STRING);
			strcpy_s (szFull, SIZE_STRING, szStringtoreplace);

			ZeroMemory (szSearch, SIZE_STRING);
			strcpy_s (szSearch, SIZE_STRING, szStringtosearchfor);

			_strupr (szFull);
			_strupr (szSearch);

			for (c=0;c<(strlen (szFull)-strlen (szSearch));c++) {
				
				ZeroMemory (szTemp, SIZE_STRING);
				strncpy_s (szTemp, SIZE_STRING, szFull+c, strlen (szSearch));

				if (strcmp (szTemp, szSearch) == 0) {
					// We found the string

					ZeroMemory (szFinal, SIZE_STRING);
					
					// Copy the first part (before the found string) into the final string
					if (c>0) {
						strncpy_s (szFinal, SIZE_STRING, szStringtoreplace, c);
					}

					// Now append the replacement
					strcat_s (szFinal, SIZE_STRING, szReplacement);

					// now append the rest of the source string (after the found string)
					strcat_s (szFinal, SIZE_STRING, szStringtoreplace+c+strlen(szStringtosearchfor));

					ZeroMemory (szStringtoreplace, SIZE_STRING);
					strcpy_s (szStringtoreplace, SIZE_STRING, szFinal);

					//return true;
				}

			}

			// if we got here, then we didn't find anything
			return false;

		} else {
			return false;
		}
	} else {
		return false;
	}

	return false;
}

void MainWindow::TestMethod ()
{
	//DisableKeyboardShortcuts();

	//m_passwindowfull.Initialise (m_hwnd, 0);
	//m_passwindowfull.SetEncryptMode (false);
	//m_passwindowfull.Show ();
	//CheckTaskManagerProcess ();

	/*
	if (m_vdmanager.IsDriveLetterFree ("C") == true) {
		MessageBox (NULL, "Drive C is free", "Free Drive", MB_OK);
	} else {
		MessageBox (NULL, "Drive C is UNAVAILABLE", "Unavailable", MB_OK);
	}

	if (m_vdmanager.IsDriveLetterFree ("O") == true) {
		MessageBox (NULL, "Drive O is free", "Free Drive", MB_OK);
	} else {
		MessageBox (NULL, "Drive O is UNAVAILABLE", "Unavailable", MB_OK);
	}
	*/


	/*
	if (m_vdmanager.DoesDriveExist ("C") == true) {
		MessageBox (NULL, "Drive C exists.", "DriveCheck", MB_OK);
	} else {
		MessageBox (NULL, "Drive C not found.", "DriveCheck", MB_OK);
	}

	if (m_vdmanager.DoesDriveExist ("E") == true) {
		MessageBox (NULL, "Drive E exists.", "DriveCheck", MB_OK);
	} else {
		MessageBox (NULL, "Drive E not found.", "DriveCheck", MB_OK);
	}

	if (m_vdmanager.DoesDriveExist ("F") == true) {
		MessageBox (NULL, "Drive F exists.", "DriveCheck", MB_OK);
	} else {
		MessageBox (NULL, "Drive F not found.", "DriveCheck", MB_OK);
	}

	*/


	//unsigned int drivetype = GetDriveType("E");

	//ShowInt (drivetype);

	/*
	if (DoesFileExist ("F:\\CedeDriveLaunch.exe") == true) {
		MessageBox (NULL, "File found", "Found", MB_OK);
	} else {
		MessageBox (NULL, "File not found", "Not found", MB_OK);	
	}
	*/

	
	MessageBox (NULL, "About to show wait dialog...", "Info", MB_OK);

	ShowServiceWaitDialog ();

	MessageBox (NULL, "Time to do something else...", "Info", MB_OK);

}



void MainWindow::Trimstring (char *szInput, char *szOutput)
{

	char szTemp[SIZE_NAME];
	char szResult[SIZE_STRING];
	ZeroMemory (szResult, SIZE_STRING);

	int i = 0;

	for (i=0;i<strlen(szInput);i++) {
		ZeroMemory (szTemp, SIZE_NAME);
		
		strncpy_s(szTemp, SIZE_STRING, szInput+i, 1);

		if (strcmp (szTemp, " ") != 0) {
			strcat_s (szResult, SIZE_STRING, szTemp);
		}
	}

	ZeroMemory (szOutput, SIZE_STRING);
	strcpy_s (szOutput, SIZE_STRING, szResult);

}


LRESULT MainWindow::OnPowerBroadcast(HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	//MessageBox (NULL, "Power Broadcast!", "Power", MB_OK);
	

	switch (wParam)
	{
		case PBT_APMSUSPEND:
		case PBT_APMQUERYSUSPEND:
		{
			m_bstandalonemode = false;
			//MessageBox (NULL, "Computer wants to suspend!", "Test", MB_OK);
			m_vdmanager.UnmountAllVirtualDrives ();
			DeleteRemovableDrives ();
			m_vdmanager.ForgetPassword ();
			m_passwindow.ClearPassword ();
			m_passwindowfull.ClearPassword();
			//MessageBox (NULL, "All encrypted drives unmounted.", "Drives unmounted", MB_OK | MB_ICONINFORMATION);
		}
		break;

		case PBT_APMRESUMESUSPEND:
		{
			//MessageBox (NULL, "Computer is resuming from suspend!", "test", MB_OK);

			#ifndef _FORLAUNCH

			if (m_registry.GetSetting_UsingSecurePassWindow () == true) {
				m_bstandalonemode = false;
				DisableKeyboardShortcuts();
				ActivateTaskManagerChecking();
				m_passwindowfull.Initialise (hWnd, 0);
				m_passwindowfull.SetEncryptMode (false);
				m_passwindowfull.Show ();
				ActivateFullPassWindow ();
			} else {
				m_passwindow.Initialise (hWnd, 0);
				m_passwindow.SetEncryptMode (false);
				m_passwindow.Show ();
			}
			#else
			m_passwindow.Initialise (hWnd, 0);
			m_passwindow.SetEncryptMode (false);
			m_passwindow.Show ();
			#endif
		}
		break;
	}

	return TRUE;
}

void MainWindow::StartPasswordRecovery ()
{
	if (MessageBox (NULL, "Before starting password recovery all of your encrypted drives will be unmounted. Please ensure you have saved any data to these drives before starting recovery. Do you wish to continue?", "Unmount all drives?", MB_YESNO | MB_ICONINFORMATION) == IDYES) {
		m_vdmanager.UnmountAllVirtualDrives ();
		DeleteRemovableDrives ();	
	} else {
		return;
	}

	MessageBox (NULL, "When recovering forgotten passwords from Encrypted Virtual Disks, you will need 2 files. The first file is the Encrypted Virtual Disk containing your encrypted data. The second is the recovery key that you saved when creating the encrypted virtual disk.\n\nYou will now be prompted locate the Encrypted Virtual Disk containing your encrypted data.", "Recovery Information", MB_OK | MB_ICONINFORMATION);

	char szEVDfilename[SIZE_STRING];
	char szDATfilename[SIZE_STRING];

	if (OpenSingleEVDFile (szEVDfilename) == true) {
				
		MessageBox (NULL, "Thank you. You will now be requested to locate the recovery key you saved when creating this Encrypted Virtual Drive.", "Recovery Key Information", MB_OK | MB_ICONINFORMATION);

		if (OpenSingleDATFile (szDATfilename) == true) {
					
			// We now need to load the file header from the evd file
			EVDFileHeader fileheader;

			if (fileheader.SetFromFile (szEVDfilename) == true) {

				//MessageBox (NULL, "Evd read ok", "Ok", MB_OK);
				fileheader.Deserialise ();
						
				// Load the users private key
				MemoryBuffer memPrivate;
				memPrivate.ReadFromFile (szDATfilename);

				char szDecryptedpassword[SIZE_STRING];
				ZeroMemory (szDecryptedpassword, SIZE_STRING);

				if (m_rsa.DecryptString (szDecryptedpassword, fileheader.GetUserKey (), &memPrivate) == true) {

					char szMsg[SIZE_STRING];
					ZeroMemory (szMsg, SIZE_STRING);
					sprintf_s (szMsg, SIZE_STRING, "Recovery of your password was successful. Your password for this encrypted drive is displayed below.\n\n%s\n\n", szDecryptedpassword);
					MessageBox (m_hwnd, szMsg, "Your Recovered Password", MB_OK | MB_ICONINFORMATION);

				} else {
					MessageBox (m_hwnd, "There was a problem recovering your password using the recovery key you supplied.\n\nPlease ensure you have supplied the correct recovery key for this Encrypted Virtual Disk.", "Unable to recover password", MB_OK | MB_ICONEXCLAMATION);
				}

			} else {
						
				char szError[SIZE_STRING];
				fileheader.GetLastErrorMessage (szError);
						
				MessageBox (m_hwnd, "There was a problem reading the requested information from this Encrypted Virtual Disk.", "Problem Reading File", MB_OK | MB_ICONEXCLAMATION);
				MessageBox (m_hwnd, szError, "Error Information", MB_OK | MB_ICONEXCLAMATION);
			}

		}

	}
}

void MainWindow::DeleteRemovableDrives ()
{
	SingleDriveInfo *pinfo;

	for (int d=0;d<m_dlDrivelist.GetNumItems ();d++) {
		pinfo = (SingleDriveInfo *) m_dlDrivelist.GetItem (d);

		if (pinfo->bUsingseperatepassword == true) {
			DeleteDrive (pinfo, false);
		}
	}

	RefreshListView();
}

void MainWindow::CheckAllDrivesValid ()
{
	SingleDriveInfo *pinfo;
	SingleDriveInfo tempDrive;

	char szHostingDrive[SIZE_NAME];

	for (int d=0;d<m_dlDrivelist.GetNumItems ();d++) {
		pinfo = (SingleDriveInfo *) m_dlDrivelist.GetItem (d);

		// Get the first letter of the drive hosting the evd
		ZeroMemory (szHostingDrive, SIZE_NAME);
		strncpy_s (szHostingDrive, SIZE_NAME, pinfo->szPath, 1);

		if (m_vdmanager.DoesDriveExist (szHostingDrive) == false) {
			
			memcpy (&tempDrive, (SingleDriveInfo *) pinfo, sizeof (SingleDriveInfo));	
			m_vdmanager.UnmountVirtualDrive (tempDrive);
			
			if (pinfo->bUsingseperatepassword == true) {
				DeleteDrive (pinfo, false);
			}
		}		
	}

	RefreshListView();
}

bool MainWindow::IsDriveAlreadyHostingEVD (char *szDrive)
{
	// Single Drive letter only expected in SzDrive - e.g. C
	SingleDriveInfo *pinfo;
	

	char szDrivecopy[SIZE_NAME];
	ZeroMemory (szDrivecopy, SIZE_NAME);
	strcpy_s (szDrivecopy, SIZE_NAME, szDrive);
	_strupr_s (szDrivecopy, SIZE_NAME);

	char szHostingDrive[SIZE_NAME];

	for (int d=0;d<m_dlDrivelist.GetNumItems ();d++) {
		pinfo = (SingleDriveInfo *) m_dlDrivelist.GetItem (d);

		// Get the first letter of the drive hosting the evd
		ZeroMemory (szHostingDrive, SIZE_NAME);
		strncpy_s (szHostingDrive, SIZE_NAME, pinfo->szPath, 1);
		
		_strupr_s (szHostingDrive, SIZE_NAME);

		if (strcmp (szHostingDrive, szDrivecopy) == 0) {
			return true;
		}
	}

	return false;
}

void MainWindow::UndoFolderRedirections ()
{			
	char szOldDocuments[SIZE_STRING];
	ZeroMemory (szOldDocuments, SIZE_STRING);

	char szOldMusic[SIZE_STRING];
	ZeroMemory (szOldMusic, SIZE_STRING);

	char szOldPictures[SIZE_STRING];
	ZeroMemory (szOldPictures, SIZE_STRING);

	char szOldVideo[SIZE_STRING];
	ZeroMemory (szOldVideo, SIZE_STRING);

	if (m_registry.DoesSettingExist ("OldMyDocuments", true) == true) {
		strcpy_s (szOldDocuments, SIZE_STRING, m_registry.ReadStringSetting ("OldMyDocuments"));
		m_registry.WriteStringCUEx ("Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\User Shell Folders", "Personal", szOldDocuments);
	} else {
		m_registry.WriteStringCUEx ("Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\User Shell Folders", "Personal", "%USERPROFILE%\\Documents");
	}

	if (m_registry.DoesSettingExist ("OldMyMusic", true) == true) {
		strcpy_s (szOldMusic, SIZE_STRING, m_registry.ReadStringSetting ("OldMyMusic"));
		m_registry.WriteStringCUEx ("Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\User Shell Folders", "My Music", szOldMusic);
	} else {
		m_registry.WriteStringCUEx ("Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\User Shell Folders", "My Music", "%USERPROFILE%\\Music");
	}


	if (m_registry.DoesSettingExist ("OldMyPictures", true) == true) {
		strcpy_s (szOldPictures, SIZE_STRING, m_registry.ReadStringSetting ("OldMyPictures"));
		m_registry.WriteStringCUEx ("Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\User Shell Folders", "My Pictures", szOldPictures);
	} else {
		m_registry.WriteStringCUEx ("Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\User Shell Folders", "My Pictures", "%USERPROFILE%\\Pictures");
	}


	if (m_registry.DoesSettingExist ("OldMyVideo", true) == true) {
		strcpy_s (szOldVideo, SIZE_STRING, m_registry.ReadStringSetting ("OldMyVideo"));
		m_registry.WriteStringCUEx ("Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\User Shell Folders", "My Video", szOldVideo);
	} else {
		m_registry.WriteStringCUEx ("Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\User Shell Folders", "My Video", "%USERPROFILE%\\Videos");
	}
}

bool MainWindow::CheckToRunRemovableLauncher (char *szLetter)
{
	char szPossiblepath[SIZE_STRING];

	ZeroMemory (szPossiblepath, SIZE_STRING);
	strcpy_s (szPossiblepath, SIZE_STRING, szLetter);
	strcat_s (szPossiblepath, SIZE_STRING, ":\\CedeDriveLaunch.exe");	


	if (DoesFileExist (szPossiblepath) == true) {
		// We found a launcher, run it!
		ShellExecute (m_hwnd, "open", szPossiblepath, NULL, NULL, SW_SHOWNORMAL);

		return true;
	}

	return false;
}

bool MainWindow::DoesSystemDriveExist (DynList *dlSourceList, char *szLetter)
{
	unsigned int a = 0;
	SingleDriveInfo *pinfo;

	for (a=0;a<dlSourceList->GetNumItems ();a++) {
		pinfo = (SingleDriveInfo *) dlSourceList->GetItem (a);

		if (strcmp (pinfo->szDriveLetter, szLetter) == 0) {
			return true;
		}
	}

	return false;
}

bool MainWindow::GetAddedSystemDrive (char *szOutLetter)
{
	// First we need to get all the current drives - which includes the new one
	DynList dlSystemDrives;
	SingleDriveInfo *pinfo;
	char szLetter[SIZE_NAME];

	GetAllSystemDrives (&dlSystemDrives);

	if (dlSystemDrives.GetNumItems () > m_dlSystemDrives.GetNumItems ()) {
		// The number of new drives must be greater than what we have already

		// Go through each new drive and find out which one is new

		for (int a=0;a<dlSystemDrives.GetNumItems ();a++) {
			
			pinfo = (SingleDriveInfo *) dlSystemDrives.GetItem (a);

			ZeroMemory (szLetter, SIZE_NAME);
			strcpy_s (szLetter, SIZE_NAME, pinfo->szDriveLetter);

			if (DoesSystemDriveExist (&m_dlSystemDrives, szLetter) == false) {
				// We could not find the current drive in the list we have
				// so this must be the new one
				ZeroMemory (szOutLetter, SIZE_NAME);
				strcpy_s (szOutLetter, SIZE_NAME, szLetter);

				dlSystemDrives.Clear ();

				
				return true; // We found the new drive
			}
		}
	}


	dlSystemDrives.Clear ();

	return false;
}

void MainWindow::GetAllSystemDrives (DynList *dlDestList)
{
	// First clear out the list we've been given
	dlDestList->Clear ();

	char szDrives[SIZE_STRING];
	ZeroMemory (szDrives, SIZE_STRING);

	char szCurDrive[SIZE_STRING];
	char szLetter[SIZE_NAME];
	char szAvaildrive[SIZE_NAME];

	int a = 0;
	int l = 0;
	int len = 0;

	SingleDriveInfo tempDrive;
	len = GetLogicalDriveStrings (SIZE_STRING, szDrives);

	for (a=0;a<len;a++) {
		if (szDrives[a] == 0) {

			ZeroMemory (szCurDrive, SIZE_STRING);
			strncpy (szCurDrive, szDrives+l, a-l);			
			l = a+1;

			ZeroMemory (szLetter, SIZE_NAME);
			strncpy_s (szLetter, SIZE_NAME, szCurDrive, 1);
			_strupr_s (szLetter, SIZE_NAME);

			ZeroMemory (tempDrive.szDriveLetter, SIZE_NAME);
			strcpy_s (tempDrive.szDriveLetter, SIZE_NAME, szLetter);

			dlDestList->AddItem (&tempDrive, sizeof (SingleDriveInfo), false);
		}
	}

}

void MainWindow::OnDeviceChange (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	//MessageBox (NULL, "A device has been removed or added.", "DeviceChange", MB_OK);
	switch (wParam)
	{
		case DBT_DEVICEARRIVAL:
		{
			//MessageBox (NULL, "A device has been added.", "DeviceChange", MB_OK);
			//CheckToRunRemovableLauncher ();
			

			char szNewDrive[SIZE_NAME];
			ZeroMemory (szNewDrive, SIZE_NAME);

			if (GetAddedSystemDrive (szNewDrive) == true) {
				//MessageBox (NULL, szNewDrive, "New Drive", MB_OK);
				CheckToRunRemovableLauncher (szNewDrive);

				// If the drive that's just arrive is a virtual drive then check the new drives are accessible, if they are writeable then it means the
				// password was correct, otherwise it will not be possible to write to the drive
				// because it will appear to have an unrecognised filesystem.

				if (IsDriveLetterCedeDrive (szNewDrive) == true) { // Check the drive is virtual and not physical
					
					VerifyDriveAccessibility (szNewDrive);
				}
			}

			// Refresh our list of system drives;
			GetAllSystemDrives (&m_dlSystemDrives);
		}
		break;

		case DBT_DEVICEREMOVECOMPLETE:
		{
			//MessageBox (NULL, "A device has been removed.", "DeviceChange", MB_OK);
			CheckAllDrivesValid ();

			GetAllSystemDrives (&m_dlSystemDrives);
		}
		break;
	}
}

void MainWindow::OnCommand (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	switch (wParam)
	{
		case IDM_TOOLSOPTIONS:
		{
			m_optionswindow.Initialise (m_hwnd, 0);
			m_optionswindow.Show ();
		}
		break;
		case IDM_SECURE_MOVE:
		{
			m_fileman.Initialise (hWnd, 0);
			
			m_fileman.SetDiagnostics (m_pdiag);
			
			m_fileman.Show ();
			
		}
		break;
		case IDM_SAVEDIAGNOSTICS:
		{
			m_pdiag->SaveDiagnostics (&m_dlDrivelist);
			MessageBox (hWnd, "CedeDrive Diagnostics have been saved to your Desktop. If you are experiencing any problems with CedeDrive please send this file to your Administrator.", "Diagnostics saved.", MB_OK | MB_ICONINFORMATION);
		}
		break;
		case ID_BTNCREATEDRIVE:
		{
			if (m_bTrialmode == true) {
				if (m_dlDrivelist.GetNumItems () > 0) {
					MessageBox (NULL, "Thank you for trying CedeDrive, a secure virtual drive encryption solution for desktops, laptops and removable drives. For full purchase information please visit http://www.cedesoft.com.\n\nPlease note this trial version is limited to 1 virtual drive. Please delete your existing drive if you would like to create another drive. To remove this limitation please visit our website for full purchase information.", "Trial Version Information", MB_OK | MB_ICONINFORMATION);
					break;
				} else {
					MessageBox (NULL, "Thank you for trying CedeDrive, a secure virtual drive encryption solution for desktops, laptops and removable drives. For full purchase information please visit http://www.cedesoft.com.\n\nPlease note this trial version is limited to 1 virtual drive size of 100MB, and drives are disconnected after 20 minutes.", "Trial Version Information", MB_OK | MB_ICONINFORMATION);
				}				
			}

			m_addwindow.Initialise (hWnd, 0);
			m_addwindow.PrePopulate (&m_dlDrivelist);
			m_addwindow.Show();
		}
		break;
		case ID_BTNADDEXISTINGDRIVE:
		{
			//bool MainWindow::OpenSingleEVDFile (char *szOutfilename)
			MessageBox (NULL, "When adding existing encrypted drives you will need to locate the Encrypted Drive File (EVD) on your hard disk.\n\nYou will now be prompted for the location of the Encrypted Drive File you wish to add.\n\n\nIMPORTANT: When adding existing drives it is very important that the password used to setup this instance of CedeDrive was the same password used to create the Encrypted Drive you are about to add. If the passwords do not match then Windows will not recognise the drive when it is mounted.", "Encrypted Drive File", MB_OK | MB_ICONINFORMATION);

			char szUserEVDFile[SIZE_STRING];
			ZeroMemory (szUserEVDFile, SIZE_STRING);

			if (OpenSingleEVDFile (szUserEVDFile) == true) {
				m_addwindow.Initialise (hWnd, 0);
				//m_addwindow.PrePopulate (&m_dlDrivelist);

				if (m_addwindow.PrePopulateFromExistingFile (szUserEVDFile, &m_dlDrivelist) == true) {
					m_addwindow.Show();
				}				
			}
		}
		break;
		case ID_BTNCONVERTDRIVE:
		{
			if (m_bTrialmode == true) {
				MessageBox (NULL, "Thank you for trying CedeDrive, a secure virtual drive encryption solution for desktops, laptops and removable drives. For full purchase information please visit http://www.cedesoft.com.\n\nPlease note when converting removable devices in trial mode, the Backup and Restore of your drive is disabled. An encrypted drive of 100MB will be created only, leaving all of your existing data in place. In the full version a complete backup of your removable device is performed and then your data is restored to the encrypted drive automatically. The encrypted drive size will also utilize the maximum available drive size. To remove these trial limitations please visit our website for full purchase information.", "Trial Version Information", MB_OK | MB_ICONINFORMATION);
			}

			m_convertwindow.Initialise (m_hwnd, 0);
			m_convertwindow.SetButtonMode (true);
			m_convertwindow.Reset ();
			m_convertwindow.Show();
			m_convertwindow.SetStagesVisible (false);
			m_convertwindow.SetInputVisible (true);
		}
		break;
		case IDM_RECOVERPASSWORD:
		{
			StartPasswordRecovery ();
		}
		break;
		case IDM_DEACTIVATELICENSE:
		{
			if (m_bTrialmode == false) {
				
				if (MessageBox (NULL, "Deactivating your license allows you to move your CedeDrive installation to another computer.\n\nAre you sure you wish to deactivate your CedeDrive license?", "Deactivation Request", MB_YESNO | MB_ICONQUESTION) == IDYES) {
				
					int iaret = m_productactivation.ActivateProductKey (m_szUserlicense, false);

					if (iaret == DEACTIVATE_SUCCESS) {
						
						m_license.ClearAllKeys ();
						m_license.DeleteLicenseFile ();
						m_bTrialmode = true;
						ZeroMemory (m_szWindowcomment, SIZE_STRING);
						sprintf_s (m_szWindowcomment, SIZE_STRING, "(Trial)");
						UpdateWindowCaption ();

						MessageBox (NULL, "Deactivation was successful. You can re-activate this product at any time.", "Product Activation", MB_OK | MB_ICONINFORMATION);
					}

					if (iaret == DEACTIVATE_FAILED) {
						MessageBox (NULL, "Deactivation failed. Please check your internet connection and try again.", "Product Activation", MB_OK | MB_ICONEXCLAMATION);
					}

					if (iaret == DEACTIVATE_NOTACTIVATED) {
						MessageBox (NULL, "This product has not been activated, and can be activated at any time.", "Product Activation", MB_OK | MB_ICONINFORMATION);
					}
				}

			} else {
				MessageBox (NULL, "This product is a trial version with no license associated to your installation. Deactivation is not necessary.", "Deactivation not necessary", MB_OK | MB_ICONINFORMATION);
			}
		}
		break;
		case IDM_ENTERLICENSE:
		{
			ShowLicensePrompt ();
		}
		break;
		case IDM_CONVERT_DRIVE:
		{
			//MessageBox (NULL, "This will convert a memory stick...", "Test", MB_OK);
			
			/*
			// Code to format a drive.
			if (m_driveformat.QuickFormatDrive ("G") == false) {

				char szReason[SIZE_STRING];
				m_driveformat.GetErrorReason (szReason, SIZE_STRING);

				MessageBox (NULL, szReason, "Format Failed.", MB_OK);

			} else {
				MessageBox (NULL, "Format was successful", "Format", MB_OK);
			}
			*/
			
			//char szDriveletter[SIZE_NAME];
			//ZeroMemory (szDriveletter, SIZE_NAME);
			
			//if (GetFreeDriveLetter (szDriveletter) == true) {
				
			//}
			
			//SendMessage (HWND_BROADCAST, WM_DEVICECHANGE, 0, 0);
			if (m_bTrialmode == true) {
				MessageBox (NULL, "Thank you for trying CedeDrive, a secure virtual drive encryption solution for desktops, laptops and removable drives. For full purchase information please visit http://www.cedesoft.com.\n\nPlease note when converting removable devices in trial mode, the Backup and Restore of your drive is disabled. An encrypted drive of 100MB will be created only, leaving all of your existing data in place. In the full version a complete backup of your removable device is performed and then your data is restored to the encrypted drive automatically. The encrypted drive size will also utilize the maximum available drive size. To remove these trial limitations please visit our website for full purchase information.", "Trial Version Information", MB_OK | MB_ICONINFORMATION);
			}

			m_convertwindow.Initialise (m_hwnd, 0);
			m_convertwindow.SetButtonMode (true);
			m_convertwindow.Reset ();
			m_convertwindow.Show();
			m_convertwindow.SetStagesVisible (false);
			m_convertwindow.SetInputVisible (true);

		}
		break;
		case IDM_HELP_ABOUT:
		{
			// Show the about window
			MessageBox (NULL, "CedeDrive v2.7.13\n\nVirtual Encrypted Drive Management\n\n(c) 2005-2010 CedeSoft Ltd\n\nAll rights reserved.\n\nWarning: This computer program is protected by copyright law and international treaties. Unauthorised reproduction or distribution of this program, or any portion of it, may result in severe civil and criminal penalties, and will be prosecuted to the maximum extent possible under the law.", "About CedeDrive", MB_OK | MB_ICONINFORMATION);
		}
		break;
		case IDM_FILE_EXIT:
		{
			//if (m_bTrialmode == true) {
				m_vdmanager.UnmountAllVirtualDrives ();
			//}
			
			DeleteTrayIcon();
			PostQuitMessage (0);
		}
		break;
		case IDM_TRAY_EXIT:
		{
			//if (m_bTrialmode == true) {
				m_vdmanager.UnmountAllVirtualDrives ();
			//}
			DeleteTrayIcon();
			PostQuitMessage (0);
		}
		break;
		case IDM_TRAY_SHOW:
		{
			if (m_bTrialmode == true) {
				MessageBox (NULL, "Thank you for trying CedeDrive, a secure virtual drive encryption solution for desktops, laptops and removable drives. For full purchase information please visit http://www.cedesoft.com.\n\nPlease note this trial version is limited to 1 virtual drive size of 100MB, and drives are disconnected after 20 minutes.", "Trial Version Information", MB_OK | MB_ICONINFORMATION);
			}
			Show();
		}
		break;
		case IDM_TRAY_UNMOUNTALL:
		{			
			m_vdmanager.UnmountAllVirtualDrives ();
			DeleteRemovableDrives ();
			MessageBox (NULL, "All encrypted drives unmounted.", "Drives unmounted", MB_OK | MB_ICONINFORMATION);
		}
		break;
		case IDM_TRAY_MOUNTALL:
		{
			// If we need to wait for the service to startup then wait
			WaitForCedeDriveService ();

			MountAllDrives(false);
		}
		break;
		case IDM_RESETCONFIG:
			{
				ResetConfig ();
			}
			break;
		case IDM_ADD_DRIVE:
		{
			if (m_bTrialmode == true) {
				if (m_dlDrivelist.GetNumItems () > 0) {
					MessageBox (NULL, "Thank you for trying CedeDrive, a secure virtual drive encryption solution for desktops, laptops and removable drives. For full purchase information please visit http://www.cedesoft.com.\n\nPlease note this trial version is limited to 1 virtual drive. Please delete your existing drive if you would like to create another drive. To remove this limitation please visit our website for full purchase information.", "Trial Version Information", MB_OK | MB_ICONINFORMATION);
					break;
				} else {
					MessageBox (NULL, "Thank you for trying CedeDrive, a secure virtual drive encryption solution for desktops, laptops and removable drives. For full purchase information please visit http://www.cedesoft.com.\n\nPlease note this trial version is limited to 1 virtual drive size of 100MB, and drives are disconnected after 20 minutes.", "Trial Version Information", MB_OK | MB_ICONINFORMATION);
				}				
			}

			m_addwindow.Initialise (hWnd, 0);
			m_addwindow.PrePopulate (&m_dlDrivelist);
			m_addwindow.Show();
		}
		break;
		case IDM_POPUP_ADDEXISTING:
		{
			//bool MainWindow::OpenSingleEVDFile (char *szOutfilename)
			MessageBox (NULL, "When adding existing encrypted drives you will need to locate the Encrypted Drive File (EVD) on your hard disk.\n\nYou will now be prompted for the location of the Encrypted Drive File you wish to add.\n\n\nIMPORTANT: When adding existing drives it is very important that the password used to setup this instance of CedeDrive was the same password used to create the Encrypted Drive you are about to add. If the passwords do not match then Windows will not recognise the drive when it is mounted.", "Encrypted Drive File", MB_OK | MB_ICONINFORMATION);

			char szUserEVDFile[SIZE_STRING];
			ZeroMemory (szUserEVDFile, SIZE_STRING);

			if (OpenSingleEVDFile (szUserEVDFile) == true) {
				m_addwindow.Initialise (hWnd, 0);
				//m_addwindow.PrePopulate (&m_dlDrivelist);

				if (m_addwindow.PrePopulateFromExistingFile (szUserEVDFile, &m_dlDrivelist) == true) {
					m_addwindow.Show();
				}				
			}
		}
		break;
		case IDM_TEST_OPTION:
			{
				//if (m_driveformat.ConvertDriveToNTFS ("F") == true) {
				//	MessageBox (NULL, "Convert succeeded.", "Info", MB_OK);
				//} else {
				//	MessageBox (NULL, "Convert failed.", "Info", MB_OK);
				//}

				//CanvasHandler canvas;
				//canvas.CreateCanvas ("C:\\Temp\\CedeDriveCanvas.dat");

				//OutputInt ("Sector Multiple of 345", GetSectorMultiple (345));
				//OutputInt ("Sector Multiple of 512", GetSectorMultiple (512));
				//OutputInt ("Sector Multiple of 768", GetSectorMultiple (768));
				//OutputInt ("Sector Multiple of 1024", GetSectorMultiple (1024));
				//OutputInt ("Sector Multiple of 1323", GetSectorMultiple (1323));
				//OutputInt ("Sector Multiple of 1766", GetSectorMultiple (1766));
				//OutputInt ("Sector Multiple of 2133", GetSectorMultiple (2133));
				//OutputInt ("Sector Multiple of 2899", GetSectorMultiple (2899));
				
				//PassWindowEx passwindow;
				//passwindow.Initialise (hWnd, 0);
				//passwindow.Show ();

				//m_passwindowex.Initialise (hWnd, 0);
				//m_passwindowex.Show ();
				
				/*
				SingleDriveInfo *pinfo;

				for (int a=0;a<m_vdmanager.m_dlmounteddrives.GetNumItems ();a++) {
					pinfo = (SingleDriveInfo *) m_vdmanager.m_dlmounteddrives.GetItem (a);

					MessageBox (NULL, pinfo->szName, "Name", MB_OK);

				}
				*/
				TestMethod ();
			}
			break;

		case IDM_POPUP_MOUNT:
			{
				int index = LV_GetFirstSelectedIndex(m_hwndlistview);
				
				if (index != -1) {

					// If we need to wait for the service to startup then wait
					WaitForCedeDriveService ();

					SingleDriveInfo tempDrive;
					SingleDriveInfo *pinfo = (SingleDriveInfo *) m_dlDrivelist.GetItem (index);					
					memcpy (&tempDrive, (SingleDriveInfo *) pinfo, sizeof (SingleDriveInfo));

					int ires = m_vdmanager.MountVirtualDisk (tempDrive);

					char szMessage[SIZE_STRING];
					ZeroMemory (szMessage, SIZE_STRING);

					if (ires == 0) {

						sprintf_s (szMessage, "Encrypted Drive '%s' was mounted successfully as drive %s.", pinfo->szName, pinfo->szDriveLetter);
						MessageBox (NULL, szMessage, "Mount Successful", MB_OK | MB_ICONINFORMATION);

					} else {
						m_vdmanager.GetMountError (szMessage);						
						MessageBox (NULL, szMessage, "Mount Failed", MB_OK | MB_ICONEXCLAMATION);
					}

				}
			}
			break;

		case IDM_POPUP_UNMOUNT:
			{
				int index = LV_GetFirstSelectedIndex(m_hwndlistview);
				
				if (index != -1) {

					// If we need to wait for the service to startup then wait
					WaitForCedeDriveService ();

					SingleDriveInfo tempDrive;
					SingleDriveInfo *pinfo = (SingleDriveInfo *) m_dlDrivelist.GetItem (index);					
					memcpy (&tempDrive, (SingleDriveInfo *) pinfo, sizeof (SingleDriveInfo));
				
					m_vdmanager.UnmountVirtualDrive (tempDrive);

					char szDriveLetter[SIZE_STRING];
					ZeroMemory (szDriveLetter, SIZE_STRING);

					char szDriveName[SIZE_STRING];
					ZeroMemory (szDriveName, SIZE_STRING);

					strcpy_s (szDriveLetter, pinfo->szDriveLetter);
					strcpy_s (szDriveName, pinfo->szName);

					if (pinfo->bUsingseperatepassword == true) {
						DeleteDrive(pinfo, false);
					} 
					
					RefreshListView();
					//SaveConfig(); - why were we saving the config here!!!

					char szMessage[SIZE_STRING];
					ZeroMemory (szMessage, SIZE_STRING);

					sprintf_s (szMessage, "Encrypted Drive '%s' (%s) was unmounted.", szDriveName, szDriveLetter);
					MessageBox (NULL, szMessage, "Drive unmounted", MB_OK | MB_ICONINFORMATION);
				}
			}
			break;

		case IDM_POPUP_REDIRECTDOCUMENTS:
			{
				int index = LV_GetFirstSelectedIndex(m_hwndlistview);
				
				if (index != -1) {

					SingleDriveInfo tempDrive;
					SingleDriveInfo *pinfo = (SingleDriveInfo *) m_dlDrivelist.GetItem (index);					
					memcpy (&tempDrive, (SingleDriveInfo *) pinfo, sizeof (SingleDriveInfo));
				
					char szOldSetting[SIZE_STRING];
					ZeroMemory (szOldSetting, SIZE_STRING);
					strcpy_s (szOldSetting, SIZE_STRING, m_registry.ReadStringCU ("Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\User Shell Folders", "Personal"));

					m_registry.WriteStringSetting ("OldMyDocuments", szOldSetting);

					if (MessageBox (NULL, "This will redirect My Documents to the selected drive. If you have files and folders under My Documents, it is recommended to move these files to your Encrypted Drive before redirecting My Documents to this encrypted drive. Do you wish to continue?", "Are you sure?", MB_YESNO | MB_ICONQUESTION) == IDYES) {
						m_registry.WriteStringCUEx ("Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\User Shell Folders", "Personal", tempDrive.szDriveLetter);

						MessageBox (NULL, "Folder redirection was successful. This change will take effect the next time you log into your computer", "Folder redirection success", MB_ICONINFORMATION);
					}
				}
			}
			break;

		case IDM_POPUP_REDIRECTMUSIC:
			{
				int index = LV_GetFirstSelectedIndex(m_hwndlistview);
				
				if (index != -1) {

					SingleDriveInfo tempDrive;
					SingleDriveInfo *pinfo = (SingleDriveInfo *) m_dlDrivelist.GetItem (index);					
					memcpy (&tempDrive, (SingleDriveInfo *) pinfo, sizeof (SingleDriveInfo));
				
					char szOldSetting[SIZE_STRING];
					ZeroMemory (szOldSetting, SIZE_STRING);
					strcpy_s (szOldSetting, SIZE_STRING, m_registry.ReadStringCU ("Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\User Shell Folders", "My Music"));

					m_registry.WriteStringSetting ("OldMyMusic", szOldSetting);

					if (MessageBox (NULL, "This will redirect My Music to the selected drive. If you have files and folders under My Music, it is recommended to move these files to your Encrypted Drive before redirecting My Music to this encrypted drive. Do you wish to continue?", "Are you sure?", MB_YESNO | MB_ICONQUESTION) == IDYES) {
						m_registry.WriteStringCUEx ("Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\User Shell Folders", "My Music", tempDrive.szDriveLetter);

						MessageBox (NULL, "Folder redirection was successful. This change will take effect the next time you log into your computer", "Folder redirection success", MB_ICONINFORMATION);
					}
				}
			}
			break;

		case IDM_POPUP_REDIRECTPICTURES:
			{
				int index = LV_GetFirstSelectedIndex(m_hwndlistview);
				
				if (index != -1) {

					SingleDriveInfo tempDrive;
					SingleDriveInfo *pinfo = (SingleDriveInfo *) m_dlDrivelist.GetItem (index);					
					memcpy (&tempDrive, (SingleDriveInfo *) pinfo, sizeof (SingleDriveInfo));
				
					char szOldSetting[SIZE_STRING];
					ZeroMemory (szOldSetting, SIZE_STRING);
					strcpy_s (szOldSetting, SIZE_STRING, m_registry.ReadStringCU ("Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\User Shell Folders", "My Pictures"));

					m_registry.WriteStringSetting ("OldMyPictures", szOldSetting);

					if (MessageBox (NULL, "This will redirect My Pictures to the selected drive. If you have files and folders under My Pictures, it is recommended to move these files to your Encrypted Drive before redirecting My Pictures to this encrypted drive. Do you wish to continue?", "Are you sure?", MB_YESNO | MB_ICONQUESTION) == IDYES) {
						m_registry.WriteStringCUEx ("Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\User Shell Folders", "My Pictures", tempDrive.szDriveLetter);
						
						MessageBox (NULL, "Folder redirection was successful. This change will take effect the next time you log into your computer", "Folder redirection success", MB_ICONINFORMATION);
					}
				}
			}
			break;


		case IDM_POPUP_REDIRECTVIDEO:
			{
				int index = LV_GetFirstSelectedIndex(m_hwndlistview);
				
				if (index != -1) {

					SingleDriveInfo tempDrive;
					SingleDriveInfo *pinfo = (SingleDriveInfo *) m_dlDrivelist.GetItem (index);					
					memcpy (&tempDrive, (SingleDriveInfo *) pinfo, sizeof (SingleDriveInfo));
				
					char szOldSetting[SIZE_STRING];
					ZeroMemory (szOldSetting, SIZE_STRING);
					strcpy_s (szOldSetting, SIZE_STRING, m_registry.ReadStringCU ("Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\User Shell Folders", "My Video"));

					m_registry.WriteStringSetting ("OldMyVideo", szOldSetting);

					if (MessageBox (NULL, "This will redirect My Video to the selected drive. If you have files and folders under My Video, it is recommended to move these files to your Encrypted Drive before redirecting My Video to this encrypted drive. Do you wish to continue?", "Are you sure?", MB_YESNO | MB_ICONQUESTION) == IDYES) {
						m_registry.WriteStringCUEx ("Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\User Shell Folders", "My Video", tempDrive.szDriveLetter);

						MessageBox (NULL, "Folder redirection was successful. This change will take effect the next time you log into your computer", "Folder redirection success", MB_ICONINFORMATION);
					}
				}
			}
			break;


		case IDM_POPUP_EXPLORE:
			{
				int index = LV_GetFirstSelectedIndex(m_hwndlistview);
				
				if (index != -1) {

					SingleDriveInfo tempDrive;
					SingleDriveInfo *pinfo = (SingleDriveInfo *) m_dlDrivelist.GetItem (index);					
					memcpy (&tempDrive, (SingleDriveInfo *) pinfo, sizeof (SingleDriveInfo));
				
					ShellExecute (m_hwnd, "explore", tempDrive.szDriveLetter, NULL, NULL, SW_SHOWNORMAL);
				}
			}
			break;

		case IDM_POPUP_UNDOREDIRECT:
			{
			
				if (MessageBox (NULL, "This will undo all folder redirections. My Documents, My Music, My Pictures and My Video will be set to their previous (default) locations. Do you wish to continue?", "Are you sure?", MB_YESNO | MB_ICONQUESTION) == IDYES) {
					UndoFolderRedirections ();

					MessageBox (NULL, "Folder redirection has been reverted back to Windows Defaults. This change will take effect the next time you log into your computer", "Folder redirection reset", MB_ICONINFORMATION);
				}

			}
			break;

		case IDM_POPUP_ADDDRIVE:
			{
				if (m_bTrialmode == true) {
					if (m_dlDrivelist.GetNumItems () > 0) {
						MessageBox (NULL, "Thank you for trying CedeDrive, a secure virtual drive encryption solution for desktops, laptops and removable drives. For full purchase information please visit http://www.cedesoft.com.\n\nPlease note this trial version is limited to 1 virtual drive. Please deleted your existing drive if you would like to create another drive. To remove this limitation please visit our website for full purchase information.", "Trial Version Information", MB_OK | MB_ICONINFORMATION);
						break;
					} else {
						MessageBox (NULL, "Thank you for trying CedeDrive, a secure virtual drive encryption solution for desktops, laptops and removable drives. For full purchase information please visit http://www.cedesoft.com.\n\nPlease note this trial version is limited to 1 virtual drive size of 100MB, and drives are disconnected after 20 minutes.", "Trial Version Information", MB_OK | MB_ICONINFORMATION);
					}				
				}
				m_addwindow.Initialise (hWnd, 0);
				m_addwindow.PrePopulate (&m_dlDrivelist);
				m_addwindow.Show();
			}
			break;

		case IDM_POPUP_DELDRIVE:
			{
				int index = LV_GetFirstSelectedIndex(m_hwndlistview);
				
				if (index != -1) {
					SingleDriveInfo tempDrive;
					SingleDriveInfo *pinfo = (SingleDriveInfo *) m_dlDrivelist.GetItem (index);
					memcpy (&tempDrive, (SingleDriveInfo *) pinfo, sizeof (SingleDriveInfo));

					char szMessage[SIZE_STRING];
					ZeroMemory (szMessage, SIZE_STRING);
					sprintf_s (szMessage, "Are you sure you want to delete the encrypted drive '%s'? All Folder Redirections will be reverted back to the Windows Defaults when deleting encrypted drives.", pinfo->szName);

					if (MessageBox (NULL, szMessage, "Are you sure?", MB_YESNO | MB_ICONQUESTION) == IDYES) {
						// Unmount the drive
						m_vdmanager.UnmountVirtualDrive (tempDrive);
						
						// Undo all folder redirections just incase this drive being deleted was used to host any windows special folders
						UndoFolderRedirections();

						// Delete the drive
						if (pinfo->bUsingseperatepassword == true) {
							DeleteDrive(pinfo, false);
						} else {
							if (MessageBox (NULL, "Do you also want to delete the file hosting the Encrypted Drive? If there is any data on this virtual disk, it will be permanently lost.", "Delete host file?", MB_YESNO | MB_ICONQUESTION) == IDYES) {
								DeleteDrive(pinfo, true);	
							} else {
								DeleteDrive (pinfo, false);
							}							
						}
						
						RefreshListView();
						SaveConfig();
					}
				}
			}
			break;
	}
}

void MainWindow::CreateEVDFileHeader (bool bEmbedUserRecovery, SingleDriveInfo *driveinfo, char *szDiskpassword, EVDFileHeader *pfileheader, HWND windowhandle)
{
	bool bRecoveryfilenameok = false;
	bool bRSAEncryptionok = false;
	MemoryBuffer memEncryptedpassword;

	char szChosenfilename[SIZE_STRING];
	ZeroMemory (szChosenfilename, SIZE_STRING);

	// Check if the user has chosen to create a recovery key
	if (bEmbedUserRecovery == true) {
		
		// We now need to ask the user where to save the file.
		MessageBox (windowhandle, "You will now be asked to save your recovery key for this drive. This recovery key will be needed if you forget your password for your encrypted drives.\n\nIMPORTANT: It is vital you store this key on a removable drive or flash drive which is not physically located with this computer. If this computer is a laptop computer, it is recommended that you save this recovery key on a flash drive and ensure you DO NOT keep it with your laptop. This recovery key must be stored in a safe location.", "Recovery key information", MB_OK | MB_ICONINFORMATION);
		
		char szInitialfilename[SIZE_STRING];
		ZeroMemory (szInitialfilename, SIZE_STRING);
		strcpy_s (szInitialfilename, SIZE_STRING, driveinfo->szName);
		strcat_s (szInitialfilename, SIZE_STRING, "-recoverykey.dat");

		if (SaveRecoveryFile (szInitialfilename, szChosenfilename, windowhandle) == true) {
			bRecoveryfilenameok = true;
		} else {
			MessageBox (windowhandle, "You have chosen not to save a recovery key. Your encrypted drive will still be created but if you forget your password to this drive recovery will be impossible.", "Recovery cancelled", MB_OK | MB_ICONEXCLAMATION);
		}
	}

	// Create the user recovery key
	if (bRecoveryfilenameok == true) {
		MemoryBuffer memPublic;
		MemoryBuffer memPrivate; // This will be saved to disk as the recovery key

		if (m_rsa.GenerateKeys (RSA_4096, &memPrivate, &memPublic) == true) {

			if (m_rsa.EncryptString (szDiskpassword, &memEncryptedpassword, &memPublic) == true) {
				
				// Discard the public key, we don't need this anymore
				memPublic.Clear ();
				
				// Set the recovery ok flag to true
				bRSAEncryptionok = true;

				// Save the private key, also known as the recovery key
				if (memPrivate.SaveToFile (szChosenfilename) == false) {
					MessageBox (windowhandle, "There was a problem saving the recovery key to the folder you specified. Your recovery key has not been saved. It is recommended that you re-create this encrypted drive and try again.", "Recovery Key Problem", MB_OK | MB_ICONEXCLAMATION);
				}

			} else {
				MessageBox (windowhandle, "There was a problem performing the RSA encryption necessary for recovery.", "Recovery Key Problem", MB_OK | MB_ICONEXCLAMATION);
				char szMsg[SIZE_STRING];
				m_rsa.GetErrorMessage (szMsg);
				MessageBox (windowhandle, szMsg, "Error information", MB_OK | MB_ICONEXCLAMATION);
			}

		} else {
			MessageBox (windowhandle, "There was a problem generating the RSA Public and Private keys necessary for recovery.", "Recovery Key Problem", MB_OK | MB_ICONEXCLAMATION);
			char szMsg[SIZE_STRING];
			m_rsa.GetErrorMessage (szMsg);
			MessageBox (windowhandle, szMsg, "Error information", MB_OK | MB_ICONEXCLAMATION);
		}
	}

	// Now create CedeSoft's manufacturer recovery key using the public key held as a resource.
	MemoryBuffer memMfpublic;
	MemoryBuffer memMfencryptedpassword;
	bool bMfkeyok = false;
	memMfpublic.ReadFromResource (MAKEINTRESOURCE (IDB_MFPUBLICKEY));
	
	if (memMfpublic.GetSize () > 0) {
		if (m_rsa.EncryptString (szDiskpassword, &memMfencryptedpassword, &memMfpublic) == true) {
			bMfkeyok = true;
		}
	}


	if (bRSAEncryptionok == true) {
		// Save the users recovery key into the file header
		pfileheader->SetUserKey (&memEncryptedpassword);
	}

	if (bMfkeyok == true) {
		// Save our cedesoft recovery key into the file header
		pfileheader->SetMFKey (&memMfencryptedpassword);
	}

	pfileheader->SetName (driveinfo->szName);
	pfileheader->SetDescription (driveinfo->szDescription);

	// Now serialise the file header
	pfileheader->Serialise ();
}

void MainWindow::OnDisabledShortcutPressed (HWND hWnd, WPARAM wParam, LPARAM lParam) 
{
	//Beep (1000, 10);
}

void MainWindow::OnUICommand (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	int controlID = wParam;

	switch (controlID) {
		case CID_FORGOTPASSWORD:
		{
			RefreshListView();
			AddTrayIcon();
			Show ();
			StartPasswordRecovery ();
		}
		break;

		case CID_LICENSEWINDOWOK:
		{
			//IG0KAB1H1-SB68MAFQA-GJR9VY2VS-I1EVUO2XI-FUD5VUQXF-ZNXOTIFTE-6VUSXHEHV-RNKDIFHQH-HXCUT6YA9-4O6U88P2J-A6MVCM0CO-PI0E
			bool bOkinit = true;
			ZeroMemory (m_szUserlicense, SIZE_STRING);
			ZeroMemory (m_szWindowcomment, SIZE_STRING);

			strcpy_s (m_szUserlicense, SIZE_STRING, m_licensewindow.GetEnteredLicense ());

			char szCleanlicensekey[SIZE_STRING];
			Trimstring (m_szUserlicense, szCleanlicensekey);

			ZeroMemory (m_szUserlicense, SIZE_STRING);
			strcpy_s (m_szUserlicense, SIZE_STRING, szCleanlicensekey);


			if (m_license.ValidateUserKey (m_szUserlicense) == true) {
				//MessageBox (NULL, "License is ok", "License", MB_OK);

				if (strcmp (m_license.GetUniqueProductID (), "11977474774712") == 0) {					

					if (m_license.GetTrialMode () == true) {
						m_licensewindow.Hide();					
						MessageBox (NULL, "The license key you have entered is for a trial product. Please enter a full version license key.", "License", MB_OK | MB_ICONEXCLAMATION);
					
					} else {

						int iaret = m_productactivation.ActivateProductKey (m_szUserlicense, true);

						m_licensewindow.SetAlwaysOnTop (false);
						if (iaret == ACTIVATE_SUCCESS) {

							m_pdiag->OutputText ("License Type: Full/Unlimited");
							m_license.DeployLicenseFile ();
							m_license.ClearAllKeys ();
							m_license.SaveMachineKey ();
							m_license.SaveUserKey (m_szUserlicense);
							m_bTrialmode = false;

							m_vdmanager.SetTrialMode (m_bTrialmode);
							m_driveformat.SetTrialMode (m_bTrialmode);
							m_drivebackup.SetTrialMode (m_bTrialmode);

							m_convertwindow.SetLicensingInfo (&m_productactivation, m_szUserlicense, m_license.GetMaxClients (), m_bTrialmode);
							
							MessageBox (NULL, "Your license key was accepted and activation was successful. Thank you for purchasing CedeDrive.\n\nPlease visit us at http://www.cedesoft.com for more information on our products and services.", "Product Activation", MB_OK | MB_ICONINFORMATION);
							//m_licensewindow.SetAlwaysOnTop (true);
							
							//EnableWindow (m_hwnd, true);
							m_licensewindow.Hide ();
							sprintf_s (m_szWindowcomment, SIZE_STRING, "(Registered to %s)", m_license.GetOwner ());
							UpdateWindowCaption ();							
						}

						if (iaret == ACTIVATE_FAILED) {
							MessageBox (NULL, "Activation failed. Please check your internet connection and try again.", "Product Activation", MB_OK | MB_ICONEXCLAMATION);
						}

						if (iaret == ACTIVATE_EXISTS) {
							MessageBox (NULL, "This license key has already been activated. You can purchase additional license keys from http://www.cedesoft.com. However if you wish to move your CedeDrive license to another computer you can deactivate your existing license, and reactivate it on the new computer.", "Product Activation", MB_OK | MB_ICONINFORMATION);
						}

						if (iaret == DEACTIVATE_SUCCESS) {
							MessageBox (NULL, "Deactivation was successful. You can re-activate this product at any time.", "Product Activation", MB_OK | MB_ICONINFORMATION);
						}

						if (iaret == DEACTIVATE_FAILED) {
							MessageBox (NULL, "Deactivation failed. Please check your internet connection and try again.", "Product Activation", MB_OK | MB_ICONEXCLAMATION);
						}

						if (iaret == DEACTIVATE_NOTACTIVATED) {
							MessageBox (NULL, "This product has not been activated, and can be activated at any time.", "Product Activation", MB_OK | MB_ICONINFORMATION);
						}

					}

				} else {
					m_licensewindow.SetAlwaysOnTop (false);
					MessageBox (NULL, "The license key you have entered is not for this CedeSoft product. Please enter a valid license key.", "License Invalid.", MB_OK | MB_ICONEXCLAMATION);
					m_licensewindow.SetAlwaysOnTop (true);
				}
								
			} else {
				m_licensewindow.SetAlwaysOnTop (false);
				MessageBox (NULL, "Invalid License Key.", "License", MB_OK);
				m_licensewindow.SetAlwaysOnTop (true);
			}
		}
		break;
		case PROGRESS_BACKUP:
			{
				m_convertwindow.SetStageProgress (STAGE_BACKUP, (int) lParam);
			}
			break;
		case PROGRESS_CREATE:
			{
				m_convertwindow.SetStageProgress (STAGE_CREATE, (int) lParam);
			}
			break;
		case PROGRESS_RESTORE:
			{
				m_convertwindow.SetStageProgress (STAGE_RESTORE, (int) lParam);
			}
			break;
		case VD_ALLOCATIONCOMPLETE:
			{
				
				// Get the last drive
				if (m_dlDrivelist.GetNumItems () > 0) {

					SingleDriveInfo *pdriveinfo = (SingleDriveInfo *) m_dlDrivelist.GetItem (m_dlDrivelist.GetNumItems ()-1);

					pdriveinfo->bFormatdriveaftermount = true;
					
					SingleDriveInfo tempDrive;
					memcpy (&tempDrive, pdriveinfo, sizeof (SingleDriveInfo));

					int ires = m_vdmanager.MountVirtualDisk (tempDrive);

					char szMessage[SIZE_STRING];
					ZeroMemory (szMessage, SIZE_STRING);

					if (ires != 0) {
						m_progress.Hide();
						m_vdmanager.GetMountError (szMessage);
						MessageBox (NULL, szMessage, "Mount Failed", MB_OK | MB_ICONEXCLAMATION);
					} else {
						
						char szReadmepath[SIZE_STRING];
						ZeroMemory (szReadmepath, SIZE_STRING);
						strcpy_s (szReadmepath, SIZE_STRING, tempDrive.szDriveLetter);
						strcat_s (szReadmepath, SIZE_STRING, "\\Readme.txt");

						MemoryBuffer memReadmefile;
						memReadmefile.ReadFromResource (MAKEINTRESOURCE (IDB_NEWDRIVEREADME));
						memReadmefile.SaveToFile (szReadmepath);

						m_progress.Hide();
						MessageBox (NULL, "Encrypted virtual drive created successfully.", "Encrypted disk created.", MB_OK | MB_ICONINFORMATION);


						//ShellExecute (m_hwnd, "explore", tempDrive.szDriveLetter, NULL, NULL, SW_SHOWNORMAL);
					}
					
				}
				
			}
			break;
		case VD_ALLOCATIONPROGRESS:
			{
				m_progress.SetProgressLabel (m_vdmanager.m_szProgressLabel);
				m_progress.SetProgressValue (m_vdmanager.m_allocationpercent);
			}
			break;
		case CC_QUITNOW:
			{
				//m_vdmanager.UnmountAllVirtualDrives ();
				DeleteTrayIcon();
				PostQuitMessage(0);				
			}
			break;
		case CC_UIDRIVEADDED:
			{
				// The Add drive UI has completed
				SingleDriveInfo driveinfo = m_addwindow.GetDriveInfo ();
				
				if (m_bTrialmode == true) {
					
					if (driveinfo.lDiskSizeMegs > 100) {
						MessageBox (NULL, "Thank you for trying CedeDrive, a secure virtual drive encryption solution for desktops, laptops and removable drives. For full purchase information please visit http://www.cedesoft.com.\n\nThe trial version is limited to virtual drive sizes of 100MB. Please create a smaller drive or visit our website to purchase the full version.", "Trial Version Limitation", MB_OK | MB_ICONINFORMATION);
						break;
					} 

					if (m_dlDrivelist.GetNumItems () > 0) {
						MessageBox (NULL, "Thank you for trying CedeDrive, a secure virtual drive encryption solution for desktops, laptops and removable drives. For full purchase information please visit http://www.cedesoft.com.\n\nThis trial version is limited to 1 virtual drive only. To create another virtual drive in this trial version, you must delete your existing drive.", "Trial Version Limitation", MB_OK | MB_ICONINFORMATION);
						break;
					}

					MessageBox (NULL, "Thank you for trying CedeDrive, a secure virtual drive encryption solution for desktops, laptops and removable drives. For full purchase information please visit http://www.cedesoft.com.", "Trial Version Information", MB_OK | MB_ICONINFORMATION);
				} 

				m_dlDrivelist.AddItem (&driveinfo, sizeof (SingleDriveInfo), false);
				RefreshListView();
				SaveConfig ();

				// Now create the virtual disk
				SingleDriveInfo diskinfo = m_addwindow.GetDriveInfo ();			

				// Create the file header for this new encrypted disk
				EVDFileHeader fileheader;
				CreateEVDFileHeader (m_addwindow.GetRecoveryChoice (), &diskinfo, m_passwindow.GetLastPassword (), &fileheader, m_hwnd);

				m_progress.SetProgressMax (100);
				m_progress.SetProgressValue (0);
				m_progress.SetProgressLabel ("Creating encrypted virtual drive...");
				
				m_progress.Initialise (hWnd, 0);
				m_progress.Show();

				// Save the header to a file just a test
				//fileheader.GetEVDHeader ()->SaveToFile ("D:\\Temp\\TestFileheader.dat");

				m_vdmanager.AllocateVirtualDisk (diskinfo, fileheader.GetEVDHeader ());

			}
			break;
		case CC_UIEXISTINGDRIVEADDED:
			{
				// The Add drive UI has completed
				SingleDriveInfo driveinfo = m_addwindow.GetDriveInfo ();
				
				if (m_bTrialmode == true) {
					
					if (driveinfo.lDiskSizeMegs > 100) {
						MessageBox (NULL, "Thank you for trying CedeDrive, a secure virtual drive encryption solution for desktops, laptops and removable drives. For full purchase information please visit http://www.cedesoft.com.\n\nThe trial version is limited to virtual drive sizes of 100MB. Please create a smaller drive or visit our website to purchase the full version.", "Trial Version Limitation", MB_OK | MB_ICONINFORMATION);
						break;
					} 

					if (m_dlDrivelist.GetNumItems () > 0) {
						MessageBox (NULL, "Thank you for trying CedeDrive, a secure virtual drive encryption solution for desktops, laptops and removable drives. For full purchase information please visit http://www.cedesoft.com.\n\nThis trial version is limited to 1 virtual drive only. To create another virtual drive in this trial version, you must delete your existing drive.", "Trial Version Limitation", MB_OK | MB_ICONINFORMATION);
						break;
					}

					MessageBox (NULL, "Thank you for trying CedeDrive, a secure virtual drive encryption solution for desktops, laptops and removable drives. For full purchase information please visit http://www.cedesoft.com.", "Trial Version Information", MB_OK | MB_ICONINFORMATION);
				} 

				m_dlDrivelist.AddItem (&driveinfo, sizeof (SingleDriveInfo), false);
				RefreshListView();
				SaveConfig ();
			}
			break;

		case CID_PASSWORDOK:
			{
				//MessageBox (NULL, "Calling verify password", "Info", MB_OK);
				VerifyPassword((unsigned int) lParam);
			}
			break;
		case CID_PASSWORDCANCEL:
			{
				OutputText ("Password Request Cancelled.");
				MessageBox (hWnd, "WARNING: If you do not provide a password you will not have access to your encrypted drives.", "Password Warning", MB_OK | MB_ICONEXCLAMATION);
				PostQuitMessage(0);
			}
			break;

		case CID_BEGINCONVERT:
			{
				m_convertdriveinfo = m_convertwindow.GetSelectedDrive ();
				m_convertwindow.GetPassword (m_szConvertpassword);

				OutputText ("Selected Drive: ", m_convertdriveinfo.szDriveLetter);

				StartDriveConverter ();

				/*
				char szDriveletter[SIZE_NAME];
				ZeroMemory (szDriveletter, SIZE_NAME);
			
				if (GetFreeDriveLetter (szDriveletter) == true) {
					OutputText ("Free Drive: ", szDriveletter);
				}
				*/
			}
			break;
		case CID_CONVERTMOUNTREQUEST:
			{
				if (MountTemporaryDrive (m_converttempdrive) == 0) {
					ResumeThread (m_hConvertthread);
				}
			}
			break;
	}
}

void MainWindow::OnIPCEvent (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	//MessageBox (NULL, "IPC Event Received By cededrive core!", "Event", MB_OK);
	OutputText ("IPC Event received from launcher. Checking shared config file.....");

	//if (m_memshared.ReadSharedMemory ("CedeDriveLauncher", 1024000) == true) {
	if (m_sharedconfig.ConfigExists () == true) {

		m_bstandalonemode = true;

		OutputText ("CedeDriveLauncher Memory read ok. Standalone mode activated.");

		m_dlnewdrives.Clear ();
		m_sharedconfig.Read (&m_memshared);
		m_dlnewdrives.FromMemoryBuffer (&m_memshared); // The shared memory we're expecting will be a dynlist

		// The launcher is now not needed, so tell it to quit
		BroadcastLauncherQuit ();

		m_passwindow.Initialise (hWnd, 0);
		m_passwindow.SetEncryptMode (false);
		m_passwindow.Show ();
		ActivatePasswindow ();
		//m_passwindow.SetWindowFocus ();

	} else {
		OutputText ("CedeDriveLauncher shared memory not found. Running in normal mode.");
	}
}

LRESULT MainWindow::OnClose (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	Hide();
	return 0;
}

void MainWindow::OnDestroy (HWND hWnd)
{
	//MessageBox (NULL, "Being destroyed!", "Test", MB_OK);
	//m_vdmanager.UnmountAllVirtualDrives ();
	DeleteTrayIcon();

	PostQuitMessage (0);
}

bool MainWindow::IsCedeDriveServiceRunning ()
{
	return m_vdmanager.IsServiceRunning ();
}

INT_PTR CALLBACK MainWindow::ServiceWaitDlgProc (HWND hWnd, UINT Message, WPARAM wParam, LPARAM lParam)
{
	int maxwaittime = 240; // Maximum wait time is 4 minutes before timeout - we can't wait forever man we got things to do!
	int currentwaittime = 0;

	switch (Message)
	{
		case WM_INITDIALOG:
		{
			SetTimer (hWnd, IDT_SERVICEWAITCHECK, 1000, NULL);
			currentwaittime = 0;
		}
		break;
		case WM_TIMER:
		{
			switch (wParam)
			{
				case IDT_SERVICEWAITCHECK:
				{
					currentwaittime++;

					if (m_pwnd->IsCedeDriveServiceRunning () == true) {
						EndDialog (hWnd, IDCANCEL);
					} else {
						if (currentwaittime > maxwaittime) {
							EndDialog (hWnd, IDCANCEL); // Timeout - we've waited long enough. Time to let the user know.
						}
					}
				}
				break;
			}
			break;
		}
		break;
		case WM_COMMAND:
		{
			switch (LOWORD (wParam))
			{
				case IDOK:
				{
				}
				break;
				//case IDCANCEL:
				//{
				//	EndDialog (hWnd, IDCANCEL);
				//}
				//break;
			}
			break;
		}
		break;
		default:
			return FALSE;
	}
	return TRUE;
}


BOOL MainWindow::ShowServiceWaitDialog ()
{
	int iRet = 0;

	iRet = DialogBox (GetModuleHandle (NULL), MAKEINTRESOURCE (IDD_SERVICEWAITDIALOG), m_hwnd, ServiceWaitDlgProc);
	return TRUE;
}


void MainWindow::ActivatePasswindow ()
{
	//SetTimer (m_hwnd, IDT_ACTIVATEWINDOW, 500, NULL);
}

void MainWindow::ActivateFullPassWindow ()
{
	m_activatewindowcount = 0;
	SetTimer (m_hwnd, IDT_ACTIVATEFULLPASSWINDOW, 500, NULL);
}

void MainWindow::ActivateTaskManagerChecking()
{
	SetTimer (m_hwnd, IDT_TASKMANAGERCHECK, 500, NULL);
}

void MainWindow::OnTimer (WPARAM wParam)
{
	m_uihandler.NotifyTimer (wParam);
	m_vdmanager.NotifyTimer (wParam);

	switch (wParam)
	{
		case IDT_RUNNINGTIMER:
		{
			m_minuteselapsed++;
		}
		break;
		case IDT_OPENFOLDERTIMER:
		{
			KillTimer (m_hwnd, IDT_OPENFOLDERTIMER);
			m_openfoldertimeractive = false;

			ShellExecute (m_hwnd, "explore", szTimedOpenDrive, NULL, NULL, SW_SHOWNORMAL);			
		}
		break;
		case IDT_UPDATECAPTION:
		{
			KillTimer (m_hwnd, IDT_UPDATECAPTION);
			SetCaption (m_szWindowcaption, true);
		}
		break;
		case IDT_ACTIVATEWINDOW:
		{
			//KillTimer (m_hwnd, IDT_ACTIVATEWINDOW);
			m_passwindow.SetWindowFocus ();
		}
		break;
		case IDT_ACTIVATEFULLPASSWINDOW:
		{
			m_activatewindowcount++;
			
			SetActiveWindow (m_hwnd);
			m_passwindowfull.SetWindowFocus ();
			//if (m_activatewindowcount > 4) {
				//KillTimer (m_hwnd, IDT_ACTIVATEFULLPASSWINDOW);
			//}
		}
		break;
		case IDT_TASKMANAGERCHECK:
		{
			CheckTaskManagerProcess ();
		}
		break;
	}
}

void MainWindow::OnPaint (HWND hWnd)
{
	m_uihandler.PaintControls (hWnd);	
}

void MainWindow::OnMouseMove (HWND hWnd, int mouseXPos, int mouseYPos)
{		
	m_uihandler.NotifyMouseMove (mouseXPos, mouseYPos);
}

void MainWindow::OnLButtonDown (HWND hWnd)
{
	m_uihandler.NotifyMouseDown ();
}

void MainWindow::OnLButtonDblClick (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	m_pdiag->OutputText ("Window Double Clicked.");
}

void MainWindow::OnLButtonUp (HWND hWnd)
{
	m_uihandler.NotifyMouseUp ();
}

void MainWindow::OutputInt (LPCSTR lpszText, int iValue)
{
	m_pdiag->OutputInt (lpszText, iValue);
}

void MainWindow::OutputText (LPCSTR lpszText)
{
	m_pdiag->OutputText (lpszText);
}

void MainWindow::OutputText (LPCSTR lpszName, LPCSTR lpszValue)
{
	m_pdiag->OutputText (lpszName, lpszValue);
}
