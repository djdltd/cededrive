#pragma once
#include <windows.h>
#include <io.h>
#include <commctrl.h>
#include "UIHandler.h"
#include "UILabel.h"
#include "UIBanner.h"
#include "UIWindow.h"


class PasswordWindow : public UIWindow
{
	public:
		PasswordWindow ();
		~PasswordWindow ();				
		void Initialise (HWND hWnd, unsigned int uID);
		char *GetLastPassword ();
		void ParseUserInput ();
		void ParseUserInput2 ();
		void CheckPassword ();		
		bool GetSessionMode ();
		void SetEncryptMode (bool bEncrypt);
		void SetDriveName (char *szName);
		void ClearPassword ();
		void SetSecondFocus ();

		static long FAR PASCAL SubProc(HWND hWnd, WORD wMessage, WORD wParam, LONG lParam);
		static long FAR PASCAL SubProc2(HWND hWnd, WORD wMessage, WORD wParam, LONG lParam);

		bool m_bEncryptmode; // Need to make this public so it can be accessed by the static functions through the class pointer
	private:
		// Private Member Variables & objects		
		static FARPROC lpfnOldWndProc;
		static FARPROC lpfnOldWndProc2;
		static PasswordWindow *ppwnd;

		// The UI Handler
		UIHandler m_uihandler;
		
		// ID ofthis window - required for window class registration purposes
		unsigned int m_ID;

		// Global Window Handle
		HWND m_hwnd;
		HWND m_parenthwnd;
		//HBITMAP hbmBanner;

		UILabel m_headerlabel;
		UILabel m_subheader;
		UILabel m_bottomheader;
		UILabel m_editpass1;
		UILabel m_editpass2;

		UIBanner m_passbg;

		bool m_bSessionmode;
		
		bool m_bInitialised;

		// Last entered password
		char m_szPassword[SIZE_NAME];

		// Control handles
		HWND m_hwndeditpass;
		HWND m_hwndeditpass2;
		HWND m_lblpass2info;
		HWND m_lblpass3info;
		HWND m_lbltitle;
		HWND m_hwndbtnpassok;
		HWND m_hwndbtnpasscancel;
		HWND m_hwndsessionlbl;

	
		// Flag indicating if we're using diagnostics
		bool m_bUseDiagnostics;

		// Registered class name
		// We need a different class name for every instance of
		// this window. This class name
		// Is created by the Initialise routine
		// with a uID value suffixed to the end
		char m_szClassname[SIZE_STRING];

		// event notification from base class
		void OnDestroy (HWND hWnd);
		void OnCreate (HWND hWnd);		
		void OnCommand (HWND hWnd, WPARAM wParam, LPARAM lParam);
		void OnUICommand (HWND hWnd, WPARAM wParam, LPARAM lParam);		
		void OnUIScroll (HWND hWnd, WPARAM wParam, LPARAM lParam);
		void OnPaint (HWND hWnd);
		void OnTimer (WPARAM wParam);
		void OnMouseMove (HWND hWnd, int mouseXPos, int mouseYPos);
		void OnLButtonDown (HWND hWnd);
		void OnLButtonUp (HWND hWnd);
};
