#include "MenuHandler.h"

MenuHandler::MenuHandler ()
{
	// IMPORTANT!!!! Only set this when building this application for embedding into the cededrive
	// Launcher! - Be sure to set this back to false when doing a main app build!!! Otherwise
	// This will hide 2 of the main menu options.
		
#ifdef _FORLAUNCH
	m_bdeployedfromlauncher  = true;
#else
	m_bdeployedfromlauncher  = false;
#endif

}

MenuHandler::~MenuHandler ()
{
	
}

void MenuHandler::SetDiagnostics (Diagnostics *pdiag)
{
	m_pdiag = pdiag;
}

void MenuHandler::CreateMainMenu (HWND hwndParent)
{
	HMENU hMenu, hSubMenu, hSubMenu2, hSubMenu3, hSubMenu4;
	
	hMenu = CreateMenu ();
		
	hSubMenu = CreatePopupMenu ();
	
	//AppendMenu (hSubMenu, MF_STRING, IDM_TEST_OPTION, "T&est Option");
	

	if (m_bdeployedfromlauncher == false) {
		AppendMenu (hSubMenu, MF_STRING, IDM_ENTERLICENSE, "Enter license key...");
		AppendMenu (hSubMenu, MF_STRING, IDM_DEACTIVATELICENSE, "Deactivate license");
		AppendMenu (hSubMenu, MF_STRING, IDM_RECOVERPASSWORD, "Recover forgotten password");
		AppendMenu (hSubMenu, MF_STRING, IDM_CONVERT_DRIVE, "E&ncrypt Removable Storage");
		AppendMenu (hSubMenu, MF_STRING, IDM_POPUP_ADDEXISTING, "&Add Existing Drive");
		AppendMenu (hSubMenu, MF_STRING, IDM_ADD_DRIVE, "N&ew Encrypted Drive");
	}
	AppendMenu (hSubMenu, MF_STRING, IDM_FILE_EXIT, "E&xit");
	AppendMenu (hMenu, MF_STRING | MF_POPUP, (UINT) hSubMenu, "&File");

	hSubMenu2 = CreatePopupMenu ();
	
	//AppendMenu (hSubMenu2, MF_STRING, IDM_TOOLS_OPTIONS, "O&ptions");
	AppendMenu (hSubMenu2, MF_STRING, IDM_TOOLSOPTIONS, "Options");
	AppendMenu (hSubMenu2, MF_STRING, IDM_SECURE_MOVE, "Secure Move");	
	AppendMenu (hSubMenu2, MF_STRING, IDM_RESETCONFIG, "Reset CedeDrive Configuration");	
	AppendMenu (hMenu, MF_STRING | MF_POPUP, (UINT) hSubMenu2, "&Tools");
	

	hSubMenu4 = CreatePopupMenu ();
	AppendMenu (hSubMenu4, MF_STRING, IDM_SAVEDIAGNOSTICS, "&Save Diagnostics");
	AppendMenu (hSubMenu4, MF_STRING, IDM_HELP_ABOUT, "&About");	
	AppendMenu (hMenu, MF_STRING | MF_POPUP, (UINT) hSubMenu4, "&Help");

	SetMenu (hwndParent, hMenu);
}

void MenuHandler::SetDeleteDisabled (bool disabled)
{
	if (disabled == true) {
		EnableMenuItem (m_hPopupMenu, IDM_POPUP_DELDRIVE, MF_GRAYED);
	} else {
		EnableMenuItem (m_hPopupMenu, IDM_POPUP_DELDRIVE, MF_ENABLED);
	}
	
}

void MenuHandler::SetAllDisabled (bool disabled)
{
	if (disabled == true) {
		EnableMenuItem (m_hPopupMenu, IDM_POPUP_DELDRIVE, MF_GRAYED);
		EnableMenuItem (m_hPopupMenu, IDM_POPUP_EXPLORE, MF_GRAYED);
		EnableMenuItem (m_hPopupMenu, IDM_POPUP_MOUNT, MF_GRAYED);
		EnableMenuItem (m_hPopupMenu, IDM_POPUP_UNMOUNT, MF_GRAYED);
		EnableMenuItem (m_hPopupMenu, IDM_POPUP_REDIRECTDOCUMENTS, MF_GRAYED);
		EnableMenuItem (m_hPopupMenu, IDM_POPUP_REDIRECTMUSIC, MF_GRAYED);
		EnableMenuItem (m_hPopupMenu, IDM_POPUP_REDIRECTPICTURES, MF_GRAYED);
		EnableMenuItem (m_hPopupMenu, IDM_POPUP_REDIRECTVIDEO, MF_GRAYED);

	} else {
		EnableMenuItem (m_hPopupMenu, IDM_POPUP_DELDRIVE, MF_ENABLED);
		EnableMenuItem (m_hPopupMenu, IDM_POPUP_EXPLORE, MF_ENABLED);
		EnableMenuItem (m_hPopupMenu, IDM_POPUP_MOUNT, MF_ENABLED);
		EnableMenuItem (m_hPopupMenu, IDM_POPUP_UNMOUNT, MF_ENABLED);
		EnableMenuItem (m_hPopupMenu, IDM_POPUP_REDIRECTDOCUMENTS, MF_ENABLED);
		EnableMenuItem (m_hPopupMenu, IDM_POPUP_REDIRECTMUSIC, MF_ENABLED);
		EnableMenuItem (m_hPopupMenu, IDM_POPUP_REDIRECTPICTURES, MF_ENABLED);
		EnableMenuItem (m_hPopupMenu, IDM_POPUP_REDIRECTVIDEO, MF_ENABLED);
	}
}

void MenuHandler::SetRedirectionDisabled (bool disabled)
{
	if (disabled == true) {
		EnableMenuItem (m_hPopupMenu, IDM_POPUP_REDIRECTDOCUMENTS, MF_GRAYED);
		EnableMenuItem (m_hPopupMenu, IDM_POPUP_REDIRECTMUSIC, MF_GRAYED);
		EnableMenuItem (m_hPopupMenu, IDM_POPUP_REDIRECTPICTURES, MF_GRAYED);
		EnableMenuItem (m_hPopupMenu, IDM_POPUP_REDIRECTVIDEO, MF_GRAYED);
	} else {
		EnableMenuItem (m_hPopupMenu, IDM_POPUP_REDIRECTDOCUMENTS, MF_ENABLED);
		EnableMenuItem (m_hPopupMenu, IDM_POPUP_REDIRECTMUSIC, MF_ENABLED);
		EnableMenuItem (m_hPopupMenu, IDM_POPUP_REDIRECTPICTURES, MF_ENABLED);
		EnableMenuItem (m_hPopupMenu, IDM_POPUP_REDIRECTVIDEO, MF_ENABLED);
	}
}

void MenuHandler::CreateMainPopupMenu ()
{
	m_hPopupMenu = CreatePopupMenu ();

	if (m_bdeployedfromlauncher == false) {
		AppendMenu (m_hPopupMenu, MF_STRING, IDM_POPUP_ADDDRIVE, "New Encrypted Drive...");
		AppendMenu (m_hPopupMenu, MF_STRING, IDM_POPUP_ADDEXISTING, "Add Existing Drive...");
		AppendMenu (m_hPopupMenu, MF_STRING, IDM_POPUP_DELDRIVE, "Delete Encrypted Drive");
	}
	AppendMenu (m_hPopupMenu, MF_MENUBREAK, NULL, NULL);
	AppendMenu (m_hPopupMenu, MF_STRING, IDM_POPUP_EXPLORE, "Explore");
	AppendMenu (m_hPopupMenu, MF_STRING, IDM_POPUP_MOUNT, "Mount Drive");
	AppendMenu (m_hPopupMenu, MF_STRING, IDM_POPUP_UNMOUNT, "Unmount Drive");
	AppendMenu (m_hPopupMenu, MF_STRING, IDM_POPUP_REDIRECTDOCUMENTS, "Set as My Documents");
	AppendMenu (m_hPopupMenu, MF_STRING, IDM_POPUP_REDIRECTMUSIC, "Set as My Music");
	AppendMenu (m_hPopupMenu, MF_STRING, IDM_POPUP_REDIRECTPICTURES, "Set as My Pictures");
	AppendMenu (m_hPopupMenu, MF_STRING, IDM_POPUP_REDIRECTVIDEO, "Set as My Video");
	AppendMenu (m_hPopupMenu, MF_MENUBREAK, NULL, NULL);
	AppendMenu (m_hPopupMenu, MF_STRING, IDM_POPUP_UNDOREDIRECT, "Undo all Folder Redirections");

	
	m_hTrayMenu = CreatePopupMenu();
	AppendMenu (m_hTrayMenu, MF_STRING, IDM_TRAY_SHOW, "Encrypted Drive Manager");
	AppendMenu (m_hTrayMenu, MF_STRING, IDM_TRAY_MOUNTALL, "Mount All Drives");
	AppendMenu (m_hTrayMenu, MF_STRING, IDM_TRAY_UNMOUNTALL, "Unmount All Drives");
	AppendMenu (m_hTrayMenu, MF_STRING, IDM_TRAY_EXIT, "Exit");

}