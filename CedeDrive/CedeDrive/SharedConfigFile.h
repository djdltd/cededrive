#pragma once
#include <windows.h>
#include <stdio.h>
#include <io.h>
#include <direct.h>
#include "MemoryBuffer.h"
#include "resource.h"

class SharedConfigFile {
	
	public:
		SharedConfigFile ();
		~SharedConfigFile ();
		bool PrepareConfigPath (bool bCreate);
		bool Save (MemoryBuffer *memBuffer);
		bool Read (MemoryBuffer *memBuffer);
		bool FileExists (char *FileName);
		bool ConfigExists ();
	private:

		char m_szConfigpath[SIZE_STRING];
};