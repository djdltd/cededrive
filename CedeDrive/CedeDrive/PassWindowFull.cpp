// This work is dedicated to the Lord our God. King of Heaven, Lord of Heaven's Armies.

#include "PassWindowFull.h"

PassWindowFull::PassWindowFull ()
{
	m_bUseDiagnostics = true;
	ZeroMemory (m_szClassname, SIZE_STRING);
	m_ID = 0;
		
	m_bSessionmode = false;
	m_bEncryptmode = false;
}

PassWindowFull::~PassWindowFull ()
{

}

void PassWindowFull::SetDiagnostics (Diagnostics *pdiag)
{
	m_pdiag = pdiag;
}

void PassWindowFull::Initialise (HWND hWnd, unsigned int uID)
{	
	// Make the ID global
	m_ID = uID;
	m_parenthwnd = hWnd;
	//m_hwnd = hWnd;

	// Temporary string value for uID
	char szID[SIZE_STRING];
	ZeroMemory (szID, SIZE_STRING);

	SetParentHWND (hWnd);
	SetBgColor (RGB (0, 0, 0));
	SetCaption (TEXT ("Password"));
	SetWindowStyle (FS_STYLEBLANK);

	// Create the class name
	strcat_s (m_szClassname, SIZE_STRING, "PassWindowFullWindowClass");
	sprintf_s (szID, SIZE_STRING, "%d", uID);
	strcat_s (m_szClassname, SIZE_STRING, szID);

	if (m_bInitialised == false) {
		m_bInitialised = true;

		GetScreenResolution();

		CreateAppWindow (m_szClassname, 70, 0, g_iScreenResX, g_iScreenResY, true);

		//ShowInt (g_iScreenResX);
		//ShowInt (g_iScreenResY);

		m_uihandler.SetWindowProperties (0, 0, g_iScreenResX, g_iScreenResY, RGB (0, 230, 0));
	}	

	SetWindowPosition (FS_CENTER);
	SetAlwaysOnTop (true);

	BOOL old;
	SystemParametersInfo(SPI_SETSCREENSAVEACTIVE, 1, &old, 0);

	SetFocus (m_hwndeditpass2);
	//Show ();
}

BOOL PassWindowFull::MySystemShutdown()
{
	HANDLE hToken; 
	TOKEN_PRIVILEGES tkp; 
		
	// Get a token for this process. 
	if (!OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken)) 
	return( FALSE ); 
		
	// Get the LUID for the shutdown privilege. 
	LookupPrivilegeValue(NULL, SE_SHUTDOWN_NAME,  &tkp.Privileges[0].Luid); 
		
	tkp.PrivilegeCount = 1;  // one privilege to set    
	tkp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED; 
		
	// Get the shutdown privilege for this process. 
	AdjustTokenPrivileges(hToken, FALSE, &tkp, 0, (PTOKEN_PRIVILEGES)NULL, 0); 
		
	if (GetLastError() != ERROR_SUCCESS) 
	return FALSE; 
		
	// Shut down and poweroff the system
	if (!ExitWindowsEx(EWX_REBOOT | EWX_FORCE, 0))
	return FALSE; 


	return TRUE;
}

void PassWindowFull::OnDestroy (HWND hWnd)
{			
	Hide ();
}

void PassWindowFull::SetWindowFocus ()
{
	//SetActiveWindow (m_hwnd);
	SetFocus (m_hwndeditpass2);
}

void PassWindowFull::SetSecondFocus ()
{
	m_editpass2.SetVisible (false);
	ShowWindow (m_hwndeditpass2, SW_SHOW);
	SetFocus (m_hwndeditpass2);
}

void PassWindowFull::OnCreate (HWND hWnd)
{				
	// Make this window handle global
	m_hwnd = hWnd;

	m_passbg.SetBitmapResources (IDB_FULLPASSBG);
	m_passbg.SetBitmapProperties (0, 0, 436, 305);
	m_passbg.SetProperties (hWnd, IDB_FULLPASSBG, (g_iScreenResX/2)-(436/2), (g_iScreenResY/2)-(305/2), 436, 305);
	m_uihandler.AddDirectControl (&m_passbg);

	// The password header label
	//m_headerlabel.SetTextProperties ("Arial", 130, 0, 0, 0, "Enter Cededrive Password", RGB (51, 100, 191));
	//m_headerlabel.SetProperties (m_hwnd, CID_PASSHEADERLABEL, 20, 14, 200, 20);
	//m_uihandler.AddDirectControl (&m_headerlabel);

	//m_subheader.SetTextProperties ("Arial", 90, 0, 0, 0, "Enter your password to mount your encrypted drives", RGB (0, 0, 0));
	//m_subheader.SetProperties (m_hwnd, CID_LABEL, 20, 35, 400, 20);
	//m_uihandler.AddDirectControl (&m_subheader);

	//m_bottomheader.SetTextProperties ("Arial", 90, 0, 0, 0, "Encrypted drives: All startup", RGB (0, 0, 0));
	//m_bottomheader.SetProperties (m_hwnd, CID_PASSBOTTOMHEADER, (g_iScreenResX/2)-90, (g_iScreenResY/2)+50, 400, 20);
	//m_uihandler.AddDirectControl (&m_bottomheader);
	
	//m_editpass1.SetTextProperties ("Arial", 90, 0, 0, 0, "Local\\All Encrypted Drives", RGB (100, 100, 100));
	//m_editpass1.SetProperties (m_hwnd, CID_PASSEDITLABEL1, (g_iScreenResX/2)-105,  (g_iScreenResY/2)-10, 190, 19);
	//m_uihandler.AddDirectControl (&m_editpass1);

	m_editpass2.SetTextProperties ("Arial", 90, 2, 0, 0, "Password", RGB (100, 100, 100));
	m_editpass2.SetProperties (m_hwnd, CID_PASSEDITLABEL2, (g_iScreenResX/2)-(105/2), (g_iScreenResY/2)+20, 190, 19);
	m_editpass2.SetTextHoverCursor (true);
	m_uihandler.AddDirectControl (&m_editpass2);

	HFONT hfDefault = (HFONT) GetStockObject (DEFAULT_GUI_FONT);
	
	m_hwndeditpass = CreateWindow ("edit", "", WS_CHILD | WS_VISIBLE | ES_PASSWORD | ES_LEFT , (g_iScreenResX/2)-95, (g_iScreenResY/2)-9, 190, 19, hWnd, (HMENU) ID_EDITPASSWORD, GetModuleHandle (NULL), NULL);
	SendMessage (m_hwndeditpass, WM_SETFONT, (WPARAM) hfDefault, MAKELPARAM (FALSE, 0));
	ShowWindow (m_hwndeditpass, SW_HIDE);

	m_hwndeditpass2 = CreateWindow ("edit", "", WS_CHILD | WS_VISIBLE | ES_PASSWORD | ES_LEFT , (g_iScreenResX/2)-30, (g_iScreenResY/2)-125, 190, 19, hWnd, (HMENU) ID_EDITPASSWORD2, GetModuleHandle (NULL), NULL);
	SendMessage (m_hwndeditpass2, WM_SETFONT, (WPARAM) hfDefault, MAKELPARAM (FALSE, 0));
	ShowWindow (m_hwndeditpass2, SW_HIDE);

	//g_iScreenResX
	//g_iScreenResY

	// The ok button
	//m_hwndbtnpassok = CreateWindow ("button", "Ok", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, g_iScreenResX-150, g_iScreenResY-60, 140, 50, hWnd, (HMENU) ID_BTNPASSOK, GetModuleHandle (NULL), NULL);
	//SendMessage (m_hwndbtnpassok, WM_SETFONT, (WPARAM) hfDefault, MAKELPARAM (FALSE, 0));

	// The cancel button
	//m_hwndbtnpasscancel = CreateWindow ("button", "Cancel", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, g_iScreenResX-300, g_iScreenResY-60, 140, 50, hWnd, (HMENU) ID_BTNPASSCANCEL, GetModuleHandle (NULL), NULL);
	//SendMessage (m_hwndbtnpasscancel, WM_SETFONT, (WPARAM) hfDefault, MAKELPARAM (FALSE, 0));


	lpfnOldWndProc = (LONG_PTR WINAPI) SetWindowLongPtr (m_hwndeditpass, GWLP_WNDPROC, (LONG_PTR) SubProc);
	lpfnOldWndProc2 = (LONG_PTR WINAPI) SetWindowLongPtr (m_hwndeditpass2, GWLP_WNDPROC, (LONG_PTR) SubProc2);

	ppwnd = (PassWindowFull *) this;

	SetFocus (m_hwndeditpass2);
}

void PassWindowFull::SetEncryptMode (bool bEncrypt)
{
	bool bEnc = bEncrypt;
	//bool bEnc = true;
		
	m_bEncryptmode = bEnc;

	if (bEnc == true) {
				
		m_headerlabel.SetTextCaption ("Create your password");
		m_subheader.SetTextCaption ("Create a password which will be used to encrypt your drives");
		m_editpass1.SetTextCaption ("Enter new password");
		m_editpass1.SetTextHoverCursor (true);
		
		m_editpass2.SetTextCaption ("Confirm new password");
		m_editpass2.SetTextHoverCursor (true);
		
		

	} else {
		m_headerlabel.SetTextCaption ("Enter Cededrive Password");
		m_subheader.SetTextCaption ("Enter your password to mount your encrypted drives");
		m_editpass1.SetTextCaption ("Local\\All Encrypted Drives");
		m_editpass1.SetTextHoverCursor (false);

		m_editpass2.SetTextCaption ("Password");
		m_editpass2.SetTextHoverCursor (true);
		m_editpass2.SetVisible (false);

		ShowWindow (m_hwndeditpass2, SW_SHOW);
		

		//SetFocus (m_hwndeditpass2);
	}

}

void PassWindowFull::ParseUserInput ()
{
	// Every time a user presses a key on the user input text box
	// on the conversation window, we need to know what the last 2 characters
	// were so we can determine if the enter key was pressed. Which will
	// trigger the process chat input function.
	char szInput[SIZE_STRING];
	ZeroMemory (szInput, SIZE_STRING);
	
	GetDlgItemText (m_hwnd, ID_EDITPASSWORD, szInput, SIZE_STRING);

	BYTE bChar1;
	BYTE bChar2;
	
	memcpy (&bChar1, szInput+strlen(szInput)-2, 1);
	memcpy (&bChar2, szInput+strlen(szInput)-1, 1);
	
	if (bChar1 == 13 && bChar2 == 10) {
		//OutputText ("Enter was pressed.");
		//ProcessChatInput (szInput);

		//SetDlgItemText (m_hwnd, ID_EDITPASSWORD, "");
		//PostMessage (m_hwnd, WM_COMMAND, ID_BTNPASSOK, 0);
		MessageBox (NULL, "enter was pressed.", "Info", MB_OK);
	}
}


LRESULT WINAPI PassWindowFull::SubProc(HWND hWnd, WORD wMessage, WPARAM wParam, LPARAM lParam)
{
	switch (wMessage)
    {			
        case WM_CHAR:
		{
			//Process this message to avoid message beeps.
			
			if ((wParam == VK_RETURN) || (wParam == VK_TAB)) {
				return 0;
			} else {
				return (CallWindowProc((WNDPROC) lpfnOldWndProc, hWnd, wMessage, wParam, lParam));
			}
			
		}
        break;
        case WM_KEYDOWN:
		{
			if ((wParam == VK_RETURN) || (wParam == VK_TAB)) {
				//PostMessage (ghDlg, WM_NEXTDLGCTL, 0, 0L);
				//MessageBox (hWnd, "enter pressed man!", "info", MB_OK);				
				ppwnd->SetSecondFocus ();
				return FALSE;
			}
		}
		break;		
	}
	return (CallWindowProc((WNDPROC) lpfnOldWndProc, hWnd, wMessage, wParam, lParam)); 
}

LRESULT WINAPI PassWindowFull::SubProc2(HWND hWnd, WORD wMessage, WPARAM wParam, LPARAM lParam)
{
	switch (wMessage)
    {	
		
        case WM_CHAR:
		{
			//Process this message to avoid message beeps.
			
			if ((wParam == VK_RETURN) || (wParam == VK_TAB)) {
				return 0;
			} else {
				return (CallWindowProc((WNDPROC) lpfnOldWndProc, hWnd, wMessage, wParam, lParam));
			}
		}
        break;
		
        case WM_KEYDOWN:
		{	
			if ((wParam == VK_RETURN) || (wParam == VK_TAB)) {
				//PostMessage (ghDlg, WM_NEXTDLGCTL, 0, 0L);
				//MessageBox (hWnd, "enter pressed on 2 man!", "info", MB_OK);				
				ppwnd->CheckPassword ();
				return FALSE;
			}

			if (wParam == VK_ESCAPE) {
				ppwnd->MySystemShutdown ();
				return FALSE;
			}
		}
		break;		
	}
	return (CallWindowProc((WNDPROC) lpfnOldWndProc2, hWnd, wMessage, wParam, lParam)); 
}

void PassWindowFull::ShowAccessDenied ()
{
	MessageBox (m_hwnd, "Access Denied. Incorrect password.", "Incorrect Password", MB_ICONEXCLAMATION | MB_OK);
}

void PassWindowFull::OutputInt (LPCSTR lpszText, int iValue)
{
	if (m_bUseDiagnostics == true) {
		m_pdiag->OutputInt (lpszText, iValue);
	}
}

void PassWindowFull::OutputText (LPCSTR lpszText)
{
	if (m_bUseDiagnostics == true) {
		m_pdiag->OutputText (lpszText);
	}
}

void PassWindowFull::OutputText (LPCSTR lpszName, LPCSTR lpszValue)
{
	if (m_bUseDiagnostics == true) {
		m_pdiag->OutputText (lpszName, lpszValue);
	}
}

void PassWindowFull::SetDriveName (char *szName)
{
	// This will set the labels specifying the context at which the password
	// dialog applies e.g. All Encrypted drives or Removable Encrypted Drive

	char szName1[SIZE_STRING];
	char szName2[SIZE_STRING];
	ZeroMemory (szName1, SIZE_STRING);
	ZeroMemory (szName2, SIZE_STRING);

	//m_editpass1.SetTextProperties ("Arial", 90, 0, 0, 0, "Local\\All Encrypted Drives", RGB (100, 100, 100));
	//m_bottomheader.SetTextProperties ("Arial", 90, 0, 0, 0, "Encrypted drives: All startup", RGB (0, 0, 0));

	strcpy_s (szName1, SIZE_STRING, "Local\\");
	strcpy_s (szName2, SIZE_STRING, "Encrypted drives: ");

	strcat_s (szName1, SIZE_STRING, szName);
	strcat_s (szName2, SIZE_STRING, szName);

	m_editpass1.SetTextCaption (szName1);
	m_bottomheader.SetTextCaption (szName2);

}

bool PassWindowFull::GetSessionMode ()
{
	return m_bSessionmode;
}


char *PassWindowFull::GetLastPassword ()
{
	return (char *) m_szPassword;
}

void PassWindowFull::ClearPassword ()
{
	SecureZeroMemory (m_szPassword, SIZE_NAME);
}

bool PassWindowFull::ContainsNumber (char *szPassword)
{
	// checks if the specified string contains a number

	if (strlen (szPassword) > 0) {
		// 48 to 57 - inclusive
		for (int a=0;a<strlen(szPassword);a++) {
			if (szPassword[a] >= 48 && szPassword[a] <= 57) {
				return true;
			}
		}
		return false;
	} else {
		return false;
	}
}

void PassWindowFull::CheckPassword ()
{
	ZeroMemory (m_szPassword, SIZE_NAME);
	GetDlgItemText (m_hwnd, ID_EDITPASSWORD2, m_szPassword, SIZE_NAME);
	SetDlgItemText (m_hwnd, ID_EDITPASSWORD2, "");
	

	char szPassword2[SIZE_NAME];
	ZeroMemory (szPassword2, SIZE_NAME);
	GetDlgItemText (m_hwnd, ID_EDITPASSWORD, szPassword2, SIZE_NAME);
	SetDlgItemText (m_hwnd, ID_EDITPASSWORD, "");

	if (strlen (m_szPassword) < 5 || strlen (m_szPassword) > 256) {
		MessageBox (m_hwnd, "Your password must be 5 characters or more.", "Password Input", MB_OK | MB_ICONEXCLAMATION);
	} else {
		if (m_bEncryptmode == true) {
			
			if (strlen (m_szPassword) >= 6 && ContainsNumber (m_szPassword) == true) {

				if (strcmp (m_szPassword, szPassword2) == 0) {
					PostMessage (m_parenthwnd, WM_UICOMMAND, (WPARAM) CID_PASSWORDOK, ORIGIN_FULLPASSWINDOW);
					Hide ();				
				} else {
					MessageBox (m_hwnd, "The passwords you have entered do not match. Please retype your password.", "Password Input", MB_OK | MB_ICONEXCLAMATION);			
					SetFocus (m_hwndeditpass);
				}
			
			} else {
				MessageBox (m_hwnd, "CedeDrive enforces strong passwords. Your password must be 6 characters or more, and must contain at least one number.", "Strong Password Policy", MB_OK | MB_ICONEXCLAMATION);
			}

		} else {
			PostMessage (m_parenthwnd, WM_UICOMMAND, (WPARAM) CID_PASSWORDOK, ORIGIN_FULLPASSWINDOW);				
		}
	}
}

void PassWindowFull::CancelPassword ()
{
	ZeroMemory (m_szPassword, SIZE_NAME);
	PostMessage (m_parenthwnd, WM_UICOMMAND, (WPARAM) CID_PASSWORDCANCEL, ORIGIN_FULLPASSWINDOW);
	Hide ();
}

void PassWindowFull::OnCommand (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	
	switch (LOWORD (wParam)) {
		case ID_BTNPASSOK:
		{
			CheckPassword ();
		}
		break;
		case ID_BTNPASSCANCEL:
		{
			CancelPassword ();
		}
		break;
		case IDOK:
		{
			MessageBox (NULL, "Enter", "Info", MB_OK);
		}
		break;
	}
}

void PassWindowFull::OnUIScroll (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	
}

void PassWindowFull::OnUICommand (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	switch (wParam)
	{
		case CID_PASSHEADERLABEL:
		{
			//MessageBox (NULL, "Label clicked!", "Info", MB_OK);
		}
		break;

		case CID_PASSEDITLABEL1:
		{
			if (m_editpass1.IsTextHoverEnabled () == true) {
				m_editpass1.SetVisible (false);
				ShowWindow (m_hwndeditpass, SW_SHOW);
				SetFocus (m_hwndeditpass);
			}
		}
		break;
		case CID_PASSEDITLABEL2:
		{
			if (m_editpass2.IsTextHoverEnabled () == true) {
				m_editpass2.SetVisible (false);
				ShowWindow (m_hwndeditpass2, SW_SHOW);
				SetFocus (m_hwndeditpass2);				
			}
		}
		break;
	}
}

void PassWindowFull::OnTimer (WPARAM wParam)
{	
	m_uihandler.NotifyTimer (wParam);	
}

void PassWindowFull::OnPaint (HWND hWnd)
{
	m_uihandler.PaintControls (hWnd);	
}

void PassWindowFull::OnMouseMove (HWND hWnd, int mouseXPos, int mouseYPos)
{	
	m_uihandler.NotifyMouseMove (mouseXPos, mouseYPos);
}

void PassWindowFull::OnLButtonDown (HWND hWnd)
{
	m_uihandler.NotifyMouseDown ();
}

void PassWindowFull::OnLButtonUp (HWND hWnd)
{
	m_uihandler.NotifyMouseUp ();
}