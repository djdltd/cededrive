#pragma once
#include <stdio.h>
#include <windows.h>
#include <wincrypt.h>
#include "resource.h"
#include "MemoryBuffer.h"

#define SIZE_STRING		1024
#define SIZE_INTEGER	32
#define SIZE_NAME			64

/*
// Supported Encryption Key Sizes

#define RSA_384		100
#define RSA_512		101
#define RSA_1024		102
#define RSA_2048		103
#define RSA_4096		104
#define RSA_8192		105

*/

class RSAEncryption {

	public:
		RSAEncryption ();
		~RSAEncryption ();
		bool GenerateKeys (unsigned int iStrength, MemoryBuffer *outPrivatekey, MemoryBuffer *outPublickey);
		bool EncryptUsingPublic (MemoryBuffer *memPlain, MemoryBuffer *memEncrypted, MemoryBuffer *memPublickey);
		bool DecryptUsingPrivate (MemoryBuffer *memEncrypted, MemoryBuffer *memDecrypted, MemoryBuffer *memPrivatekey);
		bool EncryptString (char *szInputString, MemoryBuffer *memEncrypted, MemoryBuffer *memPublickey);
		bool DecryptString (char *szOutputString, MemoryBuffer *memEncrypted, MemoryBuffer *memPrivatekey);

		void GetErrorMessage (char *szOutmessage);
		
	private:

		char szErrormessage[SIZE_STRING];
};
