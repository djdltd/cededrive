#include "ProductActivation.h"

ProductActivation::ProductActivation ()
{

}

ProductActivation::~ProductActivation ()
{

}

bool ProductActivation::Findmemstring (char *szString, MemoryBuffer *memBuffer)
{
	unsigned long b = 0;

	char szCurrent[SIZE_STRING];
	
	if (memBuffer->GetSize () >= strlen (szString)) {
		
		for (b=0;b<memBuffer->GetSize ()-strlen (szString);b++) {
			
			ZeroMemory (szCurrent, SIZE_STRING);
			memcpy (szCurrent, (char *) memBuffer->GetBuffer ()+b, strlen (szString));

			if (strcmp (szString, szCurrent) == 0) {
				return true;
			}
		}

		return false;

	} else {
		return false;
	}
}


unsigned int ProductActivation::RequestFeatureActivation (char *szProductKey, char *szFeaturecode, int iMaxactivations, bool bActivate)
{
	MemoryBuffer memResponse;
	memResponse.SetSize (50000);

	char szUrl[SIZE_STRING];
	ZeroMemory (szUrl, SIZE_STRING);
	strcpy_s (szUrl, SIZE_STRING, "http://www.cedesoft.com/FeatAct147770747.aspx?pk=");
	strcat_s (szUrl, SIZE_STRING, szProductKey);
	
	if (bActivate == true) {
		strcat_s (szUrl, SIZE_STRING, "&cm=AAAA");
	} else {
		strcat_s (szUrl, SIZE_STRING, "&cm=DDDD");
	}

	char szMax[SIZE_NAME];
	ZeroMemory (szMax, SIZE_NAME);
	sprintf_s (szMax, SIZE_NAME, "%i", iMaxactivations);

	strcat_s (szUrl, SIZE_STRING, "&f=");
	strcat_s (szUrl, SIZE_STRING, szFeaturecode);

	strcat_s (szUrl, SIZE_STRING, "&m=");
	strcat_s (szUrl, SIZE_STRING, szMax);
		

	if (g_inet.InternetFiletoMemoryBuffer (szUrl, &memResponse, memResponse.GetSize ()) == true) {
		
		//memResponse.SaveToFile ("C:\\temp\\dtemp\\response.dat");

		if (bActivate == true) {

			if (Findmemstring ("7777777.PSALMS118:8.777777714700014700000007777777", &memResponse) == true) {
				memResponse.Clear ();
				return ACTIVATE_SUCCESS;
			}

			if (Findmemstring ("7777777.PSALMS118:8.777777714800014800000007777777", &memResponse) == true) {
				memResponse.Clear ();
				return ACTIVATE_FAILED;
			}

			if (Findmemstring ("7777777.PSALMS118:8.777777714900014900000007777777", &memResponse) == true) {
				memResponse.Clear ();
				return ACTIVATE_EXISTS;
			}
		
		} else {
			
			if (Findmemstring ("7777777.PSALMS118:8.777777717700017700000007777777", &memResponse) == true) {
				memResponse.Clear ();
				return DEACTIVATE_SUCCESS;
			}

			if (Findmemstring ("7777777.PSALMS118:8.777777717800017800000007777777", &memResponse) == true) {
				memResponse.Clear ();
				return DEACTIVATE_FAILED;
			}

			if (Findmemstring ("7777777.PSALMS118:8.777777717900017900000007777777", &memResponse) == true) {
				memResponse.Clear ();
				return DEACTIVATE_NOTACTIVATED;
			}

		}


		memResponse.Clear ();
		return ACTIVATE_NOINTERNET;
	} else {
		//MessageBox (NULL, "Internet Communication failed.", "Failed.", MB_OK);
		memResponse.Clear ();
		return ACTIVATE_NOINTERNET;
	}

	memResponse.Clear ();
	return ACTIVATE_NOINTERNET;
}


unsigned int ProductActivation::ActivateProductKey (char *szProductKey, bool bActivate)
{
	MemoryBuffer memResponse;
	memResponse.SetSize (50000);

	char szUrl[SIZE_STRING];
	ZeroMemory (szUrl, SIZE_STRING);
	strcpy_s (szUrl, SIZE_STRING, "http://www.cedesoft.com/ProdAct147770747.aspx?pk=");
	strcat_s (szUrl, SIZE_STRING, szProductKey);
	
	if (bActivate == true) {
		strcat_s (szUrl, SIZE_STRING, "&cm=AAAA");
	} else {
		strcat_s (szUrl, SIZE_STRING, "&cm=DDDD");
	}
	

	if (g_inet.InternetFiletoMemoryBuffer (szUrl, &memResponse, memResponse.GetSize ()) == true) {
		
		//memResponse.SaveToFile ("C:\\temp\\dtemp\\response.dat");

		if (bActivate == true) {

			if (Findmemstring ("7777777.PSALMS118:8.777777714700014700000007777777", &memResponse) == true) {
				memResponse.Clear ();
				return ACTIVATE_SUCCESS;
			}

			if (Findmemstring ("7777777.PSALMS118:8.777777714800014800000007777777", &memResponse) == true) {
				memResponse.Clear ();
				return ACTIVATE_FAILED;
			}

			if (Findmemstring ("7777777.PSALMS118:8.777777714900014900000007777777", &memResponse) == true) {
				memResponse.Clear ();
				return ACTIVATE_EXISTS;
			}
		
		} else {
			
			if (Findmemstring ("7777777.PSALMS118:8.777777717700017700000007777777", &memResponse) == true) {
				memResponse.Clear ();
				return DEACTIVATE_SUCCESS;
			}

			if (Findmemstring ("7777777.PSALMS118:8.777777717800017800000007777777", &memResponse) == true) {
				memResponse.Clear ();
				return DEACTIVATE_FAILED;
			}

			if (Findmemstring ("7777777.PSALMS118:8.777777717900017900000007777777", &memResponse) == true) {
				memResponse.Clear ();
				return DEACTIVATE_NOTACTIVATED;
			}

		}


		memResponse.Clear ();
		return ACTIVATE_FAILED;
	} else {
		//MessageBox (NULL, "Internet Communication failed.", "Failed.", MB_OK);
		memResponse.Clear ();
		return ACTIVATE_FAILED;
	}

	memResponse.Clear ();
	return ACTIVATE_FAILED;
}
