// This work is dedicated to the Lord our God. King of Heaven, Lord of Heaven's Armies.

#include "PassWindowEx.h"

PassWindowEx::PassWindowEx ()
{
	m_bUseDiagnostics = true;
	ZeroMemory (m_szClassname, SIZE_STRING);
	m_ID = 0;
		
	m_bSessionmode = false;
	m_bEncryptmode = false;
}

PassWindowEx::~PassWindowEx ()
{

}

void PassWindowEx::SetDiagnostics (Diagnostics *pdiag)
{
	m_pdiag = pdiag;
}

void PassWindowEx::Initialise (HWND hWnd, unsigned int uID)
{	
	// Make the ID global
	m_ID = uID;
	m_parenthwnd = hWnd;
	//m_hwnd = hWnd;

	// Temporary string value for uID
	char szID[SIZE_STRING];
	ZeroMemory (szID, SIZE_STRING);

	SetParentHWND (hWnd);
	SetBgColor (GetSysColor (COLOR_BTNFACE));
	SetCaption (TEXT ("Password"));
	SetWindowStyle (FS_STYLESTANDARD);

	// Create the class name
	strcat_s (m_szClassname, SIZE_STRING, "PassWindowExWindowClass");
	sprintf_s (szID, SIZE_STRING, "%d", uID);
	strcat_s (m_szClassname, SIZE_STRING, szID);

	if (m_bInitialised == false) {
		m_bInitialised = true;
		CreateAppWindow (m_szClassname, 70, 0, 423, 270, true);
		m_uihandler.SetWindowProperties (0, 0, 423, 196, RGB (0, 230, 0));
	}	

	SetWindowPosition (FS_CENTER);
	SetAlwaysOnTop (true);
	//Show ();
}

void PassWindowEx::OnDestroy (HWND hWnd)
{			
	Hide ();
}

void PassWindowEx::SetWindowFocus ()
{
	SetActiveWindow (m_hwnd);
	SetFocus (m_hwndeditpass2);
}

void PassWindowEx::SetSecondFocus ()
{
	m_editpass2.SetVisible (false);
	ShowWindow (m_hwndeditpass2, SW_SHOW);
	SetFocus (m_hwndeditpass2);
}

void PassWindowEx::OnCreate (HWND hWnd)
{				
	// Make this window handle global
	m_hwnd = hWnd;

	m_passbg.SetBitmapResources (IDB_PASSBG);
	m_passbg.SetBitmapProperties (0, 0, 423, 196);
	m_passbg.SetProperties (hWnd, IDB_PASSBG, 0, 0, 423, 196);
	m_uihandler.AddDirectControl (&m_passbg);

	// The password header label
	m_headerlabel.SetTextProperties ("Arial", 130, 0, 0, 0, "Enter Cededrive Password", RGB (51, 100, 191));
	m_headerlabel.SetProperties (m_hwnd, CID_PASSHEADERLABEL, 20, 14, 200, 20);
	m_uihandler.AddDirectControl (&m_headerlabel);

	m_subheader.SetTextProperties ("Arial", 90, 0, 0, 0, "Enter your password to mount your encrypted drives", RGB (0, 0, 0));
	m_subheader.SetProperties (m_hwnd, CID_LABEL, 20, 35, 400, 20);
	m_uihandler.AddDirectControl (&m_subheader);

	m_bottomheader.SetTextProperties ("Arial", 90, 0, 0, 0, "Encrypted drives: All startup", RGB (0, 0, 0));
	m_bottomheader.SetProperties (m_hwnd, CID_PASSBOTTOMHEADER, 105, 148, 400, 20);
	m_uihandler.AddDirectControl (&m_bottomheader);
	
	m_editpass1.SetTextProperties ("Arial", 90, 0, 0, 0, "Local\\All Encrypted Drives", RGB (100, 100, 100));
	m_editpass1.SetProperties (m_hwnd, CID_PASSEDITLABEL1, 105, 89, 190, 19);
	m_uihandler.AddDirectControl (&m_editpass1);

	m_editpass2.SetTextProperties ("Arial", 90, 2, 0, 0, "Password", RGB (100, 100, 100));
	m_editpass2.SetProperties (m_hwnd, CID_PASSEDITLABEL2, 105, 118, 190, 19);
	m_editpass2.SetTextHoverCursor (true);
	m_uihandler.AddDirectControl (&m_editpass2);

	m_forgotpassword.SetProperties (m_hwnd, CID_FORGOTPASSWORD, 20, 210, 200, 30);
	m_forgotpassword.SetHyperlinkProperties ("Arial", 85, "Forgot your password?", "http://www.cedesoft.com");
	m_forgotpassword.SetCommandMode (true);
	m_uihandler.AddDirectControl (&m_forgotpassword);

	HFONT hfDefault = (HFONT) GetStockObject (DEFAULT_GUI_FONT);
	
	m_hwndeditpass = CreateWindow ("edit", "", WS_CHILD | WS_VISIBLE | ES_PASSWORD | ES_LEFT , 105, 88, 190, 19, hWnd, (HMENU) ID_EDITPASSWORD, GetModuleHandle (NULL), NULL);
	SendMessage (m_hwndeditpass, WM_SETFONT, (WPARAM) hfDefault, MAKELPARAM (FALSE, 0));
	ShowWindow (m_hwndeditpass, SW_HIDE);

	m_hwndeditpass2 = CreateWindow ("edit", "", WS_CHILD | WS_VISIBLE | ES_PASSWORD | ES_LEFT , 105, 117, 190, 19, hWnd, (HMENU) ID_EDITPASSWORD2, GetModuleHandle (NULL), NULL);
	SendMessage (m_hwndeditpass2, WM_SETFONT, (WPARAM) hfDefault, MAKELPARAM (FALSE, 0));
	ShowWindow (m_hwndeditpass2, SW_HIDE);

	

	// The ok button
	m_hwndbtnpassok = CreateWindow ("button", "Ok", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 257, 204, 73, 26, hWnd, (HMENU) ID_BTNPASSOK, GetModuleHandle (NULL), NULL);
	SendMessage (m_hwndbtnpassok, WM_SETFONT, (WPARAM) hfDefault, MAKELPARAM (FALSE, 0));

	// The cancel button
	m_hwndbtnpasscancel = CreateWindow ("button", "Cancel", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 336, 204, 73, 26, hWnd, (HMENU) ID_BTNPASSCANCEL, GetModuleHandle (NULL), NULL);
	SendMessage (m_hwndbtnpasscancel, WM_SETFONT, (WPARAM) hfDefault, MAKELPARAM (FALSE, 0));


	lpfnOldWndProc = (LONG_PTR WINAPI) SetWindowLongPtr (m_hwndeditpass, GWLP_WNDPROC, (LONG_PTR) SubProc);
	lpfnOldWndProc2 = (LONG_PTR WINAPI) SetWindowLongPtr (m_hwndeditpass2, GWLP_WNDPROC, (LONG_PTR) SubProc2);

	ppwnd = (PassWindowEx *) this;

}

void PassWindowEx::SetEncryptMode (bool bEncrypt)
{
	bool bEnc = bEncrypt;
	//bool bEnc = true;
		
	m_bEncryptmode = bEnc;

	if (bEnc == true) {
				
		m_headerlabel.SetTextCaption ("Create your password");
		m_subheader.SetTextCaption ("Create a password which will be used to encrypt your drives");
		m_editpass1.SetTextCaption ("Enter new password");
		m_editpass1.SetTextHoverCursor (true);
		
		m_editpass2.SetTextCaption ("Confirm new password");
		m_editpass2.SetTextHoverCursor (true);
		
		

	} else {
		m_headerlabel.SetTextCaption ("Enter Cededrive Password");
		m_subheader.SetTextCaption ("Enter your password to mount your encrypted drives");
		m_editpass1.SetTextCaption ("Local\\All Encrypted Drives");
		m_editpass1.SetTextHoverCursor (false);

		m_editpass2.SetTextCaption ("Password");
		m_editpass2.SetTextHoverCursor (true);
		m_editpass2.SetVisible (false);

		ShowWindow (m_hwndeditpass2, SW_SHOW);
		

		//SetFocus (m_hwndeditpass2);
	}

}

void PassWindowEx::ParseUserInput ()
{
	// Every time a user presses a key on the user input text box
	// on the conversation window, we need to know what the last 2 characters
	// were so we can determine if the enter key was pressed. Which will
	// trigger the process chat input function.
	char szInput[SIZE_STRING];
	ZeroMemory (szInput, SIZE_STRING);
	
	GetDlgItemText (m_hwnd, ID_EDITPASSWORD, szInput, SIZE_STRING);

	BYTE bChar1;
	BYTE bChar2;
	
	memcpy (&bChar1, szInput+strlen(szInput)-2, 1);
	memcpy (&bChar2, szInput+strlen(szInput)-1, 1);
	
	if (bChar1 == 13 && bChar2 == 10) {
		//OutputText ("Enter was pressed.");
		//ProcessChatInput (szInput);

		//SetDlgItemText (m_hwnd, ID_EDITPASSWORD, "");
		//PostMessage (m_hwnd, WM_COMMAND, ID_BTNPASSOK, 0);
		MessageBox (NULL, "enter was pressed.", "Info", MB_OK);
	}
}


LRESULT WINAPI PassWindowEx::SubProc(HWND hWnd, WORD wMessage, WPARAM wParam, LPARAM lParam)
{
	switch (wMessage)
    {			
        case WM_CHAR:
		{
			//Process this message to avoid message beeps.
			
			if ((wParam == VK_RETURN) || (wParam == VK_TAB)) {
				return 0;
			} else {
				return (CallWindowProc((WNDPROC) lpfnOldWndProc, hWnd, wMessage, wParam, lParam));
			}
			
		}
        break;
        case WM_KEYDOWN:
		{
			if ((wParam == VK_RETURN) || (wParam == VK_TAB)) {
				//PostMessage (ghDlg, WM_NEXTDLGCTL, 0, 0L);
				//MessageBox (hWnd, "enter pressed man!", "info", MB_OK);				
				ppwnd->SetSecondFocus ();
				return FALSE;
			}
		}
		break;		
	}
	return (CallWindowProc((WNDPROC) lpfnOldWndProc, hWnd, wMessage, wParam, lParam)); 
}

LRESULT WINAPI PassWindowEx::SubProc2(HWND hWnd, WORD wMessage, WPARAM wParam, LPARAM lParam)
{
	switch (wMessage)
    {	
		
        case WM_CHAR:
		{
			//Process this message to avoid message beeps.
			
			if ((wParam == VK_RETURN) || (wParam == VK_TAB)) {
				return 0;
			} else {
				return (CallWindowProc((WNDPROC) lpfnOldWndProc, hWnd, wMessage, wParam, lParam));
			}
		}
        break;
		
        case WM_KEYDOWN:
		{	
			if ((wParam == VK_RETURN) || (wParam == VK_TAB)) {
				//PostMessage (ghDlg, WM_NEXTDLGCTL, 0, 0L);
				//MessageBox (hWnd, "enter pressed on 2 man!", "info", MB_OK);				
				ppwnd->CheckPassword ();
				return FALSE;
			}
		}
		break;		
	}
	return (CallWindowProc((WNDPROC) lpfnOldWndProc2, hWnd, wMessage, wParam, lParam)); 
}

void PassWindowEx::OutputInt (LPCSTR lpszText, int iValue)
{
	if (m_bUseDiagnostics == true) {
		m_pdiag->OutputInt (lpszText, iValue);
	}
}

void PassWindowEx::OutputText (LPCSTR lpszText)
{
	if (m_bUseDiagnostics == true) {
		m_pdiag->OutputText (lpszText);
	}
}

void PassWindowEx::OutputText (LPCSTR lpszName, LPCSTR lpszValue)
{
	if (m_bUseDiagnostics == true) {
		m_pdiag->OutputText (lpszName, lpszValue);
	}
}

void PassWindowEx::SetDriveName (char *szName)
{
	// This will set the labels specifying the context at which the password
	// dialog applies e.g. All Encrypted drives or Removable Encrypted Drive

	char szName1[SIZE_STRING];
	char szName2[SIZE_STRING];
	ZeroMemory (szName1, SIZE_STRING);
	ZeroMemory (szName2, SIZE_STRING);

	//m_editpass1.SetTextProperties ("Arial", 90, 0, 0, 0, "Local\\All Encrypted Drives", RGB (100, 100, 100));
	//m_bottomheader.SetTextProperties ("Arial", 90, 0, 0, 0, "Encrypted drives: All startup", RGB (0, 0, 0));

	strcpy_s (szName1, SIZE_STRING, "Local\\");
	strcpy_s (szName2, SIZE_STRING, "Encrypted drives: ");

	strcat_s (szName1, SIZE_STRING, szName);
	strcat_s (szName2, SIZE_STRING, szName);

	m_editpass1.SetTextCaption (szName1);
	m_bottomheader.SetTextCaption (szName2);

}

bool PassWindowEx::GetSessionMode ()
{
	return m_bSessionmode;
}


char *PassWindowEx::GetLastPassword ()
{
	return (char *) m_szPassword;
}

void PassWindowEx::ClearPassword ()
{
	SecureZeroMemory (m_szPassword, SIZE_NAME);
}

bool PassWindowEx::ContainsNumber (char *szPassword)
{
	// checks if the specified string contains a number

	if (strlen (szPassword) > 0) {
		// 48 to 57 - inclusive
		for (int a=0;a<strlen(szPassword);a++) {
			if (szPassword[a] >= 48 && szPassword[a] <= 57) {
				return true;
			}
		}
		return false;
	} else {
		return false;
	}
}

void PassWindowEx::CheckPassword ()
{
	ZeroMemory (m_szPassword, SIZE_NAME);
	GetDlgItemText (m_hwnd, ID_EDITPASSWORD2, m_szPassword, SIZE_NAME);
	SetDlgItemText (m_hwnd, ID_EDITPASSWORD2, "");
	

	char szPassword2[SIZE_NAME];
	ZeroMemory (szPassword2, SIZE_NAME);
	GetDlgItemText (m_hwnd, ID_EDITPASSWORD, szPassword2, SIZE_NAME);
	SetDlgItemText (m_hwnd, ID_EDITPASSWORD, "");

	if (strlen (m_szPassword) < 5 || strlen (m_szPassword) > 256) {
		MessageBox (m_hwnd, "Your password must be 5 characters or more.", "Password Input", MB_OK | MB_ICONEXCLAMATION);
	} else {
		if (m_bEncryptmode == true) {
			
			if (strlen (m_szPassword) >= 6 && ContainsNumber (m_szPassword) == true) {

				if (strcmp (m_szPassword, szPassword2) == 0) {
					PostMessage (m_parenthwnd, WM_UICOMMAND, (WPARAM) CID_PASSWORDOK, 0);
					Hide ();				
				} else {
					MessageBox (m_hwnd, "The passwords you have entered do not match. Please retype your password.", "Password Input", MB_OK | MB_ICONEXCLAMATION);			
					SetFocus (m_hwndeditpass);
				}
			
			} else {
				MessageBox (m_hwnd, "CedeDrive enforces strong passwords. Your password must be 6 characters or more, and must contain at least one number.", "Strong Password Policy", MB_OK | MB_ICONEXCLAMATION);
			}

		} else {
			PostMessage (m_parenthwnd, WM_UICOMMAND, (WPARAM) CID_PASSWORDOK, 0);
			Hide ();				
		}
	}
}

void PassWindowEx::OnCommand (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	
	switch (LOWORD (wParam)) {
		case ID_BTNPASSOK:
		{
			CheckPassword ();
		}
		break;
		case ID_BTNPASSCANCEL:
		{
			ZeroMemory (m_szPassword, SIZE_NAME);
			PostMessage (m_parenthwnd, WM_UICOMMAND, (WPARAM) CID_PASSWORDCANCEL, 0);
			Hide ();
		}
		break;
		case IDOK:
		{
			MessageBox (NULL, "Enter", "Info", MB_OK);
		}
		break;
	}
}

void PassWindowEx::OnUIScroll (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	
}

void PassWindowEx::OnUICommand (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	switch (wParam)
	{
		case CID_PASSHEADERLABEL:
		{
			//MessageBox (NULL, "Label clicked!", "Info", MB_OK);
		}
		break;

		case CID_PASSEDITLABEL1:
		{
			if (m_editpass1.IsTextHoverEnabled () == true) {
				m_editpass1.SetVisible (false);
				ShowWindow (m_hwndeditpass, SW_SHOW);
				SetFocus (m_hwndeditpass);
			}
		}
		break;
		case CID_PASSEDITLABEL2:
		{
			if (m_editpass2.IsTextHoverEnabled () == true) {
				m_editpass2.SetVisible (false);
				ShowWindow (m_hwndeditpass2, SW_SHOW);
				SetFocus (m_hwndeditpass2);				
			}
		}
		break;
		case CID_FORGOTPASSWORD:
		{
			//MessageBox (m_hwnd, "Forgot Password", "Password", MB_OK);
			Hide ();
			PostMessage (m_parenthwnd, WM_UICOMMAND, (WPARAM) CID_FORGOTPASSWORD, 0);
		}
		break;
	}
}

void PassWindowEx::OnTimer (WPARAM wParam)
{	
	m_uihandler.NotifyTimer (wParam);	
}

void PassWindowEx::OnPaint (HWND hWnd)
{
	m_uihandler.PaintControls (hWnd);	
}

void PassWindowEx::OnMouseMove (HWND hWnd, int mouseXPos, int mouseYPos)
{	
	m_uihandler.NotifyMouseMove (mouseXPos, mouseYPos);
}

void PassWindowEx::OnLButtonDown (HWND hWnd)
{
	m_uihandler.NotifyMouseDown ();
}

void PassWindowEx::OnLButtonUp (HWND hWnd)
{
	m_uihandler.NotifyMouseUp ();
}