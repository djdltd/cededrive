#pragma once
#include "resource.h"
#include <io.h>
#include <stdio.h>
#include <windows.h>
#include <direct.h>
#include "InternetHandler.h"

class ProductActivation {
	
	public:
		ProductActivation ();
		~ProductActivation ();
		
		unsigned int ActivateProductKey (char *szProductKey, bool bActivate);
		unsigned int RequestFeatureActivation (char *szProductKey, char *szFeaturecode, int iMaxactivations, bool bActivate);
	private:
		bool Findmemstring (char *szString, MemoryBuffer *memBuffer);

		InternetHandler g_inet;
	
};