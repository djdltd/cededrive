#include "SecureDelete.h"

SecureDelete::SecureDelete ()
{
	m_bFileOpen = FALSE;
	m_resultcode = 0;
}

SecureDelete::~SecureDelete ()
{
}


bool SecureDelete::SecureDeleteFile (char *szFilepath)
{


	// Converts a drive to the NTFS filesystem by using the Windows command line too convert.exe (installed on Windows XP)
	// This is convert mechansim is used whenever a Windows XP host is detected as the Win32_Volume class used by the
	// format script is not available under windows xp.
	// Using result code of 93 and onwards, remember this class uses a global result code so the caller can just quickly
	// find out why anything failed if it did.

    STARTUPINFO si;
    PROCESS_INFORMATION pi;

    ZeroMemory( &si, sizeof(si) );
    si.cb = sizeof(si);
    ZeroMemory( &pi, sizeof(pi) );
    

	// This gets the location of Convert.exe and also sets the command line which will be called.	
	if (PrepareCommandLine (szFilepath) == false) {
		m_resultcode = 93;
		return false;
	}

	si.dwFlags = STARTF_USESHOWWINDOW;
	si.wShowWindow = SW_HIDE;

    // Start the child process. 
	if( !CreateProcess(NULL,   // No module name (use command line)
		m_szCommandline,        // Command line
        NULL,           // Process handle not inheritable
        NULL,           // Thread handle not inheritable
        FALSE,          // Set handle inheritance to FALSE
        0,              // No creation flags
        NULL,           // Use parent's environment block
        NULL,           // Use parent's starting directory 
        &si,            // Pointer to STARTUPINFO structure
        &pi )           // Pointer to PROCESS_INFORMATION structure
    ) 
    {
		m_resultcode = 94;
		return false;
    }

    // Wait until child process exits.
    WaitForSingleObject( pi.hProcess, INFINITE );

	// Now get the exit code of the Convert process		
	DWORD dwExitcode;
	GetExitCodeProcess (pi.hProcess, &dwExitcode);
	//m_resultcode = dwExitcode;
	//ShowInt (dwExitcode);

    // Close process and thread handles. 
    CloseHandle( pi.hProcess );
    CloseHandle( pi.hThread );
	
	return true;	
}

void SecureDelete::GetErrorReason (char *szReason, int reasonlen)
{
	ZeroMemory (szReason, reasonlen);
	if (m_resultcode == 93) {strcpy_s (szReason, reasonlen, "Prepare CommandLine Failed");}
	if (m_resultcode == 94) {strcpy_s (szReason, reasonlen, "CreateProcess Failed when attempting to launch secure delete command");}

}

bool SecureDelete::FileExists (char *FileName)
{
	// Check if the file exists		
	struct _finddata_t c_file;
	long hFile;

	// Build the list of files in the source path
	if( (hFile = _findfirst(FileName, &c_file )) == -1L ) {
		return false;
	} else {
		return true;
	}

	return false;
}

bool SecureDelete::PrepareCommandLine (char *szFilepath)
{
	char szTemp[SIZE_STRING];

	ZeroMemory (szTemp, SIZE_STRING);
	ZeroMemory (m_szCommandline	, SIZE_STRING);

	if (GetEnvironmentVariable ("TEMP", szTemp, SIZE_STRING) == 0) {
		return false;
	}

	strcpy_s (m_szCommandline, SIZE_STRING, szTemp);
	strcat_s (m_szCommandline, SIZE_STRING, "\\SecureDelete.exe"); // The /B parameter tells WScript to execute in batch mode thus supressing all message prompts or error message boxes.
	
	if (FileExists (m_szCommandline) == false) {
		// If the file does not exists then save out the secure delete app
		MemoryBuffer memApp;
		memApp.ReadFromResource (MAKEINTRESOURCE (IDB_SECUREDELETEAPP));
		memApp.SaveToFile (m_szCommandline);
		memApp.Clear ();
	}
	
	strcat_s (m_szCommandline, SIZE_STRING, " ");
	strcat_s (m_szCommandline, SIZE_STRING, "\"");
	strcat_s (m_szCommandline, SIZE_STRING, szFilepath);
	strcat_s (m_szCommandline, SIZE_STRING, "\"");

	//strcpy_s (m_szCommandline, SIZE_STRING, 
	return true;
}



