#pragma once
#define _WIN32_WINNT	0x0501
#include <windows.h>
#include <io.h>
#include <commctrl.h>
#include "UIWindow.h"
#include "Diagnostics.h"
#include "UIHandler.h"
#include "UIBanner.h"
#include "MultiContent.h"
#include "UIRect.h"
#include "UIToolbar.h"
#include "UIPicButton.h"
#include "DynList.h"
#include "DirScanner.h"
#include "SingleFileInfo.h"
#include "SecureMovePopup.h"
#include "FileShredder.h"
#include "SecureDelete.h"

class SecureFileManager : public UIWindow
{
	public:
		SecureFileManager ();
		~SecureFileManager ();
		void SetDiagnostics (Diagnostics *pdiag);
		void PrepareListView (HWND listviewhwnd);
		void RefreshListView (char *szDir, HWND listviewhwnd, DynList *dlFilelist, int PathControlID, int fileiconid);
		void RefreshDestList (char *szDir);
		void RefreshSourceList (char *szDir);
		int LV_GetFirstSelectedIndex (HWND hwndlv);
		bool CopySingleFile (char *szSourcefile, char *szRootpath, char *szDestpath, unsigned long lFilesize);
		bool ShredSingleFile (char *szSourcefile, unsigned long lFilesize);
		bool PopLastItem (char *szDestitem, DynList *dlHistory);
		int CopyAll ();
		int ShredAll ();
		void Initialise (HWND hWnd, unsigned int uID);
		void OutputInt (LPCSTR lpszText, int iValue);
		void OutputText (LPCSTR lpszText);
		void OutputText (LPCSTR lpszName, LPCSTR lpszValue);
		void LV_GetAllSelectedFiles (HWND hwndlv, DynList *dlFilelist);
		static DWORD CALLBACK CopyProgressRoutine (  LARGE_INTEGER TotalFileSize, LARGE_INTEGER TotalBytesTransferred, LARGE_INTEGER StreamSize, LARGE_INTEGER StreamBytesTransferred, DWORD dwStreamNumber, DWORD dwCallbackReason, HANDLE hSourceFile, HANDLE hDestinationFile, LPVOID lpData);
		void UpdateCopyProgress (unsigned long long bytestransferred);
		void DoScanandCopy ();
		static DWORD WINAPI ScanandCopyProc (PVOID pParam);
		static DWORD WINAPI ShredonlyProc (PVOID pParam);
		void DoShredOnly ();
		void EnableControls (bool bEnabled);
		void RefreshCurrentDest ();
		void RefreshCurrentSource ();
		void UpdateStatusLabel (char *szCaption);
		int LV_GetSelectedCount (HWND hwndlv);
		void DeletePath (char *szPath);
		bool FileExists (char *FileName);

		// Cross thread accessor functions
		HWND GetHWNDSourceList ();
		DynList *GetSourceFileList ();
		void GetAvailableDrives ();
		void PopulateAvailDrives (HWND hwnddrivelist, char *szSelecteddrive);
		void ChangeSourceDrive ();
		void ChangeDestDrive ();
		void LV_DeleteAllSelectedFiles (HWND hwndlv, DynList *dlFilelist);
		

	private:
		// Private Member Variables & objects

		// The UI Handler required for multiple handling of custom controls.
		UIHandler m_uihandler;
		
		// ID ofthis window - required for window class registration purposes
		unsigned int m_ID;

		// Global Window Handle
		HWND m_hwnd;
		HWND m_parenthwnd;
		HWND m_hwndlistsource;
		HWND m_hwndlistdest;
		HWND m_hwndprogress;
		HWND m_hwndlblstatus;

		HWND m_hwndtxtsource;
		HWND m_hwndtxtdest;

		HWND m_hwnddrivesource;
		HWND m_hwnddrivedest;

		UIToolbar m_toolbarsource;
		UIToolbar m_toolbardest;

		UIBanner m_sourcelabel;
		UIBanner m_destlabel;
		UIBanner m_header;

		UIPicButton m_btnMove;

		HIMAGELIST m_hLarge;   // image list for icon view 
		HIMAGELIST m_hSmall;   // image list for other views 
		
		// SecureMovePopup Window
		SecureMovePopup m_movepopup;

		// Recursive Directory Scanner class
		DirScanner m_Dirscanner;

		// The File Shredder
		FileShredder m_fileshredder;

		// Secure Deletion of Files by launching a separate process
		SecureDelete m_securedelete;


		// Source and Dest file lists
		DynList m_dlsourcefiles;
		DynList m_dldestfiles;

		DynList m_dlSourcefolderhistory;
		DynList m_dlDestfolderhistory;

		DynList m_dlAvaildrives; // List of available drives.

		char m_szCurrentsourcefolder[SIZE_STRING];
		char m_szCurrentdestfolder[SIZE_STRING];

		// The list containing all the files in all dirs to be copied and shredded.
		DynList m_dlResultfiles;
		unsigned long long m_llsizeallfiles; // The total size of all files to be copied - this helps with the progress bar.
		unsigned long long m_lltotalbytescopied;
		

		// The progress bar doesn't just show progress when a file is done, but it shows progress on a byte by byte basis for all files
		// for much more accurate representation of progress. That way the progress bar doesn't sit still when a real big file is being copied.
		HANDLE m_hCopythread; // The worker thread doing the scanning and copying

		// Flag indicating if we're using diagnostics
		bool m_bUseDiagnostics;
		Diagnostics *m_pdiag;

		// Registered class name
		// We need a different class name for every instance of
		// this window. This class name
		// Is created by the Initialise routine
		// with a uID value suffixed to the end
		char m_szClassname[SIZE_STRING];

		// event notification from base class
		void OnDestroy (HWND hWnd);
		void OnCreate (HWND hWnd);		
		LRESULT OnClose (HWND hWnd, WPARAM wParam, LPARAM lParam);
		void OnNotify (HWND hWnd, WPARAM wParam, LPARAM lParam);
		void OnCommand (HWND hWnd, WPARAM wParam, LPARAM lParam);
		void OnUICommand (HWND hWnd, WPARAM wParam, LPARAM lParam);		
		void OnUIScroll (HWND hWnd, WPARAM wParam, LPARAM lParam);
		void OnPaint (HWND hWnd);
		void OnTimer (WPARAM wParam);
		void OnMouseMove (HWND hWnd, int mouseXPos, int mouseYPos);
		void OnLButtonDown (HWND hWnd);
		void OnLButtonUp (HWND hWnd);
		void OnSize (HWND hWnd, WPARAM wParam, LPARAM lParam);
};
