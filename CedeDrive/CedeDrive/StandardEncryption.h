#pragma once
#include <stdio.h>
#include <windows.h>
#include <wincrypt.h>
#include "MemoryBuffer.h"
#include "Base64Coder.h"

#define SIZE_STRING		1024
#define SIZE_INTEGER	32
#define SIZE_NAME			64

#define ENCRYPT_ALGORITHM CALG_AES_256
//#define ENCRYPT_ALGORITHM CALG_RC4
#define ENCRYPT_BLOCK_SIZE 16
#define KEYLENGTH  0x01000000 // 256 bit key length
//#define KEYLENGTH  0x00800000 // 256 bit key length

#define CRYPTRES_FAILED			100
#define CRYPTRES_NOTENCRYPTED	101
#define CRYPTRES_ENCRYPTED		102

class StandardEncryption {
	
	public:
		StandardEncryption ();
		~StandardEncryption ();
		bool EncryptFile(PCHAR szSource, PCHAR szDestination, PCHAR szPassword, bool bEncrypt);
		bool EncryptFileEx (char *szSource, char *szDestination, char *szPassword, bool bEncrypt);
		bool EncryptFileEx2 (char *szSource, char *szDestination, char *szPassword, bool bEncrypt);
		bool IsMultiLayeredSignaturePresent (MemoryBuffer *memSource);
		bool MultiEncryptBuffer (MemoryBuffer *memSource, PCHAR szPassword, bool bEncrypt, int iNumlayers);
		bool EncryptBufferEx (MemoryBuffer *memSource, PCHAR szPassword, bool bEncrypt);
		bool EncryptBuffer (MemoryBuffer *memSource, PCHAR szPassword, bool bEncrypt);
		unsigned int IsFileEncrypted (char *szFilename);
		unsigned long GetFileSize (char *FileName);
		bool GetMD5Hash (char *szPassword, char *szOutbuf);
		void Base64Encode (char *szInput, char *szOutput);
		void Base64Decode (char *szInput, char *szOutput);
		void Base64Decode (char *szInput, MemoryBuffer *memOutput);
		void Base64Encode (MemoryBuffer *memInput, char *szOutput);
		void SetOutputHWND (HWND hwndOutput);
		void OutputText (LPCSTR lpszText);
		void OutputInt (LPCSTR lpszText, int iValue);
		void MyHandleError(char *s);

	private:
		bool m_bOutputwindowset;
		HWND m_hwnddiaglist;
		Base64Coder m_base64coder;

		unsigned long m_lMagicone;
		unsigned long m_lMagictwo;
		unsigned long m_lMagicthree;
		unsigned long m_lMagicfour;
};
