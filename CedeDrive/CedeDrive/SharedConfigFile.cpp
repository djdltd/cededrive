// A shared file class. For quickly sharing configuration information in a file. Written by Danny Draper (c) 2010.

#include "SharedConfigFile.h"

SharedConfigFile::SharedConfigFile ()
{
}

SharedConfigFile::~SharedConfigFile ()
{
}

bool SharedConfigFile::PrepareConfigPath (bool bCreate)
{
	char szAppData[SIZE_STRING];
	char szCompanyAppData[SIZE_STRING];
	char szProgramAppData[SIZE_STRING];
	
	ZeroMemory (szAppData, SIZE_STRING);
	ZeroMemory (szCompanyAppData, SIZE_STRING);
	ZeroMemory (szProgramAppData, SIZE_STRING);
	
	if (GetEnvironmentVariable ("APPDATA", szAppData, SIZE_STRING) == 0) {
		if (GetEnvironmentVariable ("TEMP", szAppData, SIZE_STRING) == 0) {
			return false;
		}
	}

	strcpy_s (szCompanyAppData, SIZE_STRING, szAppData);
	strcat_s (szCompanyAppData, SIZE_STRING, "\\CedeSoft");
	if (bCreate == true) {
		_mkdir (szCompanyAppData);
	}

	strcpy_s (szProgramAppData, SIZE_STRING, szCompanyAppData);
	strcat_s (szProgramAppData, SIZE_STRING, "\\CedeDrive");
	if (bCreate == true) {
		_mkdir (szProgramAppData);
	}

	// Set our member variable which is used by the other functions
	ZeroMemory (m_szConfigpath, SIZE_STRING);
	strcpy_s (m_szConfigpath, SIZE_STRING, szProgramAppData);
	
	return true;
}

bool SharedConfigFile::FileExists (char *FileName)
{
	// Check if the file exists		
	struct _finddata_t c_file;
	long hFile;

	// Build the list of files in the source path
	if( (hFile = _findfirst(FileName, &c_file )) == -1L ) {
		return false;
	} else {
		return true;
	}

	return false;
}

bool SharedConfigFile::ConfigExists ()
{
	char szFilename[SIZE_STRING];
	ZeroMemory (szFilename, SIZE_STRING);

	if (PrepareConfigPath (false) == false) {
		return false;
	}

	strcpy_s (szFilename, SIZE_STRING, m_szConfigpath);
	strcat_s (szFilename, SIZE_STRING, "\\SharedConfig.dat");

	if (FileExists (szFilename) == true) {
		return true;
	} else {
		return false;
	}
}

bool SharedConfigFile::Read (MemoryBuffer *memBuffer)
{
	if (PrepareConfigPath (false) == false) {
		return false;
	}
	
	char szFilename[SIZE_STRING];
	ZeroMemory (szFilename, SIZE_STRING);

	strcpy_s (szFilename, SIZE_STRING, m_szConfigpath);
	strcat_s (szFilename, SIZE_STRING, "\\SharedConfig.dat");

	if (memBuffer->ReadFromFile (szFilename) == true) {
		DeleteFile (szFilename);
		return true;
	} else {
		DeleteFile (szFilename);
		return false;
	}
}

bool SharedConfigFile::Save (MemoryBuffer *memBuffer)
{
	if (PrepareConfigPath (true) == false) {
		return false;
	}
	
	char szFilename[SIZE_STRING];
	ZeroMemory (szFilename, SIZE_STRING);

	strcpy_s (szFilename, SIZE_STRING, m_szConfigpath);
	strcat_s (szFilename, SIZE_STRING, "\\SharedConfig.dat");

	if (memBuffer->SaveToFile (szFilename) == false) {
		return false;
	}

	return true;
}

