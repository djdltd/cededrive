#pragma once
#include "resource.h"
#include <io.h>
#include <stdio.h>
#include <windows.h>
#include <direct.h>
#include "DynList.h"
#include "Diagnostics.h"
#include "SingleEntry.h"

class DriveBackupRestore {
	
	public:
		DriveBackupRestore ();
		~DriveBackupRestore ();

		void SetDiagnostics (Diagnostics *pdiag);

		void OutputInt (LPCSTR lpszText, int iValue);
		void OutputText (LPCSTR lpszText);
		void OutputText (LPCSTR lpszName, LPCSTR lpszValue);
		bool BackupDrive (char *szDriveletter);
		bool RestoreDrive (char *szOrigdrive, char *szDestdriveletter);
		bool DeleteRestore (char *szDriveletter);
		void ListAllFiles (char *szSourcePath);
		void CreateDirectoryPath (char *szFilepath);
		bool PrepareBackupPath ();
		bool Replacestring (char *szStringtoreplace, char *szStringtosearchfor, char *szReplacement);
		bool GetTimeString (char *szTimestring, int destsize);
		void SetHWND (HWND hwnd);
		unsigned long long GetFreeDiskSpace ();
		void SetTrialMode (bool bTrial);
	private:

		char m_szBackuppath[SIZE_STRING];
		DynList m_dlFilelist;
		DynList m_dlFolderlist;
		Diagnostics *m_pdiag;
		bool m_bUseDiagnostics;
		bool m_bBackupdone;
		unsigned long long m_lbackupsize;
		HWND m_hwnd;
		bool m_bTrialmode;

};