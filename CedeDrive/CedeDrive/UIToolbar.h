#pragma once
#include <windows.h>
#include <commctrl.h>
#include "resource.h"
#include "Diagnostics.h"
#include "UIHandler.h"

class UIToolbar {
	
	public:
		UIToolbar ();
		~UIToolbar ();

		void SetDiagnostics (Diagnostics *pdiag);
		void CreateToolbar (UIHandler *phandler, HWND hwndParent, int xpos, int ypos, int toolbarID);
		void SetPosition (int xpos, int ypos);
		void SetEnabled (bool bEnabled);
		void SetDeleteButtonVisible (bool bVisible);

	private:
	
		HWND m_hwnd;
		int m_xpos;
		int m_ypos;

		bool m_bDeletebuttonvisible;

		UIPicButton m_btnParent;
		UIPicButton m_btnDelete;
		UIPicButton m_btnIconview;
		UIPicButton m_btnListview;
		UIPicButton m_btnDetailview;
		UIPicButton m_btnAddfiles;
		UIPicButton m_btnExtractfiles;

		// Diagnostics window pointer
		Diagnostics *m_pdiag;

};