#include "UIToolbar.h"

UIToolbar::UIToolbar ()
{
	m_bDeletebuttonvisible = true;
}

UIToolbar::~UIToolbar ()
{
	
}

void UIToolbar::SetDiagnostics (Diagnostics *pdiag)
{
	m_pdiag = pdiag;
}

void UIToolbar::SetDeleteButtonVisible (bool bVisible)
{
	m_bDeletebuttonvisible = bVisible;	
}

void UIToolbar::SetPosition (int xpos, int ypos)
{
	int tbx = xpos;
	m_btnParent.SetPosition (tbx, ypos);

	tbx+=40;
	if (m_bDeletebuttonvisible == true) {
		m_btnDelete.SetPosition (tbx, ypos);
		tbx+=40;
	}
	m_btnIconview.SetPosition (tbx, ypos);
	tbx+=40;
	m_btnListview.SetPosition (tbx, ypos);
	tbx+=40;
	m_btnDetailview.SetPosition (tbx, ypos);
	tbx+=40;
	//m_btnAddfiles.SetPosition (xpos, ypos);
	//m_btnExtractfiles.SetPosition (xpos, ypos);

	int toolbarwidth = (5*40);
	int toolbarheight = 34;

	RECT toolbarrectold;
	toolbarrectold.left = m_xpos;
	toolbarrectold.right = m_xpos+toolbarwidth;
	toolbarrectold.top = m_ypos;
	toolbarrectold.bottom = m_ypos+toolbarheight;

	RECT toolbarrectnew;
	toolbarrectnew.left = xpos;
	toolbarrectnew.right = xpos+toolbarwidth;
	toolbarrectnew.top = ypos;
	toolbarrectnew.bottom = ypos+toolbarheight;

	//InvalidateRect (m_hwnd, &toolbarrectold, TRUE);
	//InvalidateRect (m_hwnd, &toolbarrectnew, TRUE);

	m_xpos = xpos;
	m_ypos = ypos;
}

void UIToolbar::SetEnabled (bool bEnabled) 
{
	m_btnParent.SetEnabled (bEnabled);
	if (m_bDeletebuttonvisible == true) {
		m_btnDelete.SetEnabled (bEnabled);
	}
	m_btnIconview.SetEnabled (bEnabled);
	m_btnListview.SetEnabled (bEnabled);
	m_btnDetailview.SetEnabled (bEnabled);
}

void UIToolbar::CreateToolbar (UIHandler *phandler, HWND hwndParent, int xpos, int ypos, int toolbarID)
{
	m_hwnd = hwndParent;

	int tby = ypos;
	int tbx = xpos;
	m_xpos = xpos;
	m_ypos = ypos;

//	#define CID_BTNPARENT					423
//#define CID_BTNDELETE					424
//#define CID_BTNICONVIEW					425
//#define CID_BTNLISTVIEW					426
//#define CID_BTNDETAILVIEW				427
//#define CID_BTNADDFILES					428
//#define CID_BTNEXTRACTFILES				429

	m_btnParent.SetBitmapResources (true, IDB_BTNPARENTNORM, IDB_BTNPARENTHIGH, IDB_BTNPARENTDOWN);
	m_btnParent.SetBitmapProperties (35, 32);
	m_btnParent.SetProperties (hwndParent, CID_BTNPARENT+toolbarID, tbx, tby, 35, 32);
	m_btnParent.SetHighlightProperties (true, false);	
	phandler->AddDirectControl (&m_btnParent);

	tbx+=40;
	
	if (m_bDeletebuttonvisible == true) {
		m_btnDelete.SetBitmapResources (true, IDB_BTNDELETENORM, IDB_BTNDELETEHIGH, IDB_BTNDELETEDOWN);
		m_btnDelete.SetBitmapProperties (35, 32);
		m_btnDelete.SetProperties (hwndParent, CID_BTNDELETE+toolbarID, tbx, tby, 35, 32);
		m_btnDelete.SetHighlightProperties (true, false);	
		phandler->AddDirectControl (&m_btnDelete);
		tbx+=40;
	}

	m_btnIconview.SetBitmapResources (true, IDB_BTNICONVIEWNORM, IDB_BTNICONVIEWHIGH, IDB_BTNICONVIEWDOWN);
	m_btnIconview.SetBitmapProperties (35, 32);
	m_btnIconview.SetProperties (hwndParent, CID_BTNICONVIEW+toolbarID, tbx, tby, 35, 32);
	m_btnIconview.SetHighlightProperties (true, false);	
	phandler->AddDirectControl (&m_btnIconview);

	tbx+=40;
	m_btnListview.SetBitmapResources (true, IDB_BTNLISTVIEWNORM, IDB_BTNLISTVIEWHIGH, IDB_BTNLISTVIEWDOWN);
	m_btnListview.SetBitmapProperties (35, 32);
	m_btnListview.SetProperties (hwndParent, CID_BTNLISTVIEW+toolbarID, tbx, tby, 35, 32);
	m_btnListview.SetHighlightProperties (true, false);	
	phandler->AddDirectControl (&m_btnListview);

	tbx+=40;
	m_btnDetailview.SetBitmapResources (true, IDB_BTNDETAILVIEWNORM, IDB_BTNDETAILVIEWHIGH, IDB_BTNDETAILVIEWDOWN);
	m_btnDetailview.SetBitmapProperties (35, 32);
	m_btnDetailview.SetProperties (hwndParent, CID_BTNDETAILVIEW+toolbarID, tbx, tby, 35, 32);
	m_btnDetailview.SetHighlightProperties (true, false);	
	phandler->AddDirectControl (&m_btnDetailview);

	/*
	tbx+=40;
	m_btnAddfiles.SetBitmapResources (true, IDB_BTNADDNORM, IDB_BTNADDHIGH, IDB_BTNADDDOWN);
	m_btnAddfiles.SetBitmapProperties (35, 32);
	m_btnAddfiles.SetProperties (hwndParent, CID_BTNADDFILES+toolbarID, tbx, tby, 35, 32);
	m_btnAddfiles.SetHighlightProperties (true, false);	
	phandler->AddDirectControl (&m_btnAddfiles);

	tbx+=40;
	m_btnExtractfiles.SetBitmapResources (true, IDB_BTNEXTRACTNORM, IDB_BTNEXTRACTHIGH, IDB_BTNEXTRACTDOWN);
	m_btnExtractfiles.SetBitmapProperties (35, 32);
	m_btnExtractfiles.SetProperties (hwndParent, CID_BTNEXTRACTFILES+toolbarID, tbx, tby, 35, 32);
	m_btnExtractfiles.SetHighlightProperties (true, false);	
	phandler->AddDirectControl (&m_btnExtractfiles);

	tbx+=40;
	*/
}
