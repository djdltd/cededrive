#include <windows.h>
#include <io.h>
#include <Dbt.h>

#ifndef _FORLAUNCH
#include "psapi.h"
#endif
#include <direct.h>
#include <commctrl.h>
#include "UIWindow.h"
#include "UIHandler.h"
#include "ControlCreator.h"
#include "UIPicButton.h"
#include "UIBanner.h"
#include "Diagnostics.h"
#include "UILabel.h"
#include "MenuHandler.h"
#include "Stack.h"
#include "DynList.h"
#include "VirtualDriveManager.h"
#include "AddDriveWindow.h"
#include "PasswordWindow.h"
#include "StandardEncryption.h"
#include "ProgressWindow.h"
#include "RegistryHandler.h"
#include "DriveFormatter.h"
#include "Licensing.h"
#include "LicenseWindow.h"
#include "DriveBackupRestore.h"
#include "ConvertDriveWindow.h"
#include "CanvasHandler.h"
#include "SharedConfigFile.h"
#include "ProductActivation.h"
#include "RSAEncryption.h"
#include "EVDFileHeader.h"
#include "PassWindowEx.h"
#include "PassWindowFull.h"
#include "Obfuscator.h"
#include "UIRect.h"
#include "SecureFileManager.h"
#include "SecureDelete.h"
#include "OptionsWindow.h"
#include "..\..\CedeDriveLHI\CedeDriveLHI\CedeDriveLHI.h"

class MainWindow : public UIWindow
{
	public:
		MainWindow ();
		~MainWindow ();
		
		void Initialise (HWND hWnd, bool bAlreadyrunning, LPSTR lpCmdline);
		void SetDiagnostics (Diagnostics *pdiag);
	
		void OutputInt (LPCSTR lpszText, int iValue);
		void OutputText (LPCSTR lpszText);
		void OutputText (LPCSTR lpszName, LPCSTR lpszValue);
		void SaveConfig ();
		bool LoadConfig ();
		bool FileExists (char *FileName);
		bool FileExistsEx (char *FileName);
		bool DoesFileExist (char *szFilename);
		void DeleteDrive (SingleDriveInfo *pinfo, bool bDeletehostfile);
		void AddTrayIcon ();
		void DeleteTrayIcon ();
		BOOL ShowServiceWaitDialog ();
		//static MainWindow *m_pwnd;
		static INT_PTR CALLBACK ServiceWaitDlgProc (HWND hWnd, UINT Message, WPARAM wParam, LPARAM lParam);

		void WaitForCedeDriveService ();
		void SetInfoPanel (SingleDriveInfo driveinfo, bool showinfo);
		void PrepareCedeCryptPath ();
		void PrepareListView ();
		void RefreshListView ();
		void PrepareConfigPath ();
		int LV_GetFirstSelectedIndex (HWND hwndlv);
		void VerifyPassword (unsigned int origin);
		void MountAllDrives (bool startuponly);
		unsigned long long GetFreeDiskSpace (char *szDrive);
		void ConvertRemovableDrive ();
		void DeleteRemovableDrives ();
		void UndoFolderRedirections ();
		void CheckAllDrivesValid ();
		bool CheckToRunRemovableLauncher (char *szLetter);
		bool IsDriveAlreadyHostingEVD (char *szDrive);
		void StartDriveConverter ();
		bool IsDriveConverted (char *szDriveletter);
		static MainWindow* m_pwnd;
		void ShowLicensePrompt ();
		bool DoesSystemDriveExist (DynList *dlSourceList, char *szLetter);
		void UpdateWindowCaption ();
		void ShowWelcome (bool bVisible);
		bool AuthoriseExecution ();
		bool IsCedeDriveServiceRunning ();
		void ActivateFullPassWindow ();
		void Trimstring (char *szInput, char *szOutput);
		void ActivateTaskManagerChecking();
		void StartPasswordRecovery();
		void GetAllSystemDrives (DynList *dlDestList);
		bool GetAddedSystemDrive (char *szOutLetter);
		bool Replacestring (char *szStringtoreplace, char *szStringtosearchfor, char *szReplacement);
		void UnmountDriveLetter (char *szLetter);
		bool IsDriveLetterCedeDrive (char *szLetter);

		bool IsDriveAccessible(char *szLetter);
		static DWORD WINAPI ConvertDriveProc (PVOID pParam);
		int MountTemporaryDrive (SingleDriveInfo driveinfo);
		void GetPathOnly (char *szInpath, char *szOutpath);
		
		//bool CheckStandaloneMode ();
		HANDLE m_hConvertthread;
		void CheckToTerminateProcess ( DWORD processID, char *szQueryName);
		void CheckTaskManagerProcess ();
		void BroadcastLauncherQuit ();
		void ShareConfig ();
		void BroadcastIPCEvent (WPARAM wParam, LPARAM lParam);
		bool SaveRecoveryFile (char *szInitialname, char *szOutfilename, HWND windowhandle);
		void CreateEVDFileHeader (bool bEmbedUserRecovery, SingleDriveInfo *driveinfo, char *szDiskpassword, EVDFileHeader *pfileheader, HWND windowhandle);
		unsigned long GetSectorMultiple (unsigned long lValue);
		bool OpenSingleEVDFile (char *szOutfilename);
		bool OpenSingleDATFile (char *szOutfilename);
		void ActivatePasswindow ();
		void DecideWelcomeVisible ();
		void VerifyDriveAccessibility (char *szLetter);
		void TestMethod ();
		void ResetConfig ();
	private:
		// Private Member Variables & objects
		
		// The UI Handler required for multiple handling of custom controls.
		UIHandler m_uihandler;

		// The Control Creater required for fast creation of controls
		ControlCreator m_ccontrols;		

		//A test stack
		Stack m_stack;

		// Global hwnd
		HWND m_hwnd;

		// The number CedeDrive has been running
		unsigned long m_minuteselapsed;

		// The header bitmap image
		UIBanner m_header;

		// Welcome panel
		UILabel m_headerlabel;
		UIRect m_whiterect;
		HWND m_btnadddrive;
		HWND m_btnaddexisting;
		HWND m_btnconvertdrive;
		UILabel m_lbladddrive;
		UILabel m_lbladddrive2;
		UILabel m_lbladdexistingdrive;
		UILabel m_lbladdexistingdrive2;
		UILabel m_lblconvertdrive;
		UILabel m_lblconvertdrive2;
		

		// The info panel control handles
		HWND m_infodrivename;
		HWND m_infodisksize;
		HWND m_infodriveletter;
		HWND m_infodescription;
		HWND m_infopath;

		// Contains a list of all the drives on the system
		// so if a drive gets added or removed then
		// we know which drive it is.
		DynList m_dlSystemDrives;

		// The main menu class
		MenuHandler m_mainmenu;

		// Pointer to the global diagnostics window
		Diagnostics *m_pdiag;

		// Test passwindow
		PassWindowEx m_passwindowex;

		PassWindowFull m_passwindowfull;

		char szTimedOpenDrive[SIZE_NAME];

		// Licensing classes
		Licensing m_license;
		LicenseWindow m_licensewindow;
		char m_szUserlicense[SIZE_STRING];
		char m_szWindowcomment[SIZE_STRING];
		char m_szWindowcaption[SIZE_STRING];
		bool m_bTrialmode;
		CanvasHandler m_canvas;
		ProductActivation m_productactivation;

		// Virtual Drive Manager
		VirtualDriveManager m_vdmanager;
		
		// Add new virtual drive window
		AddDriveWindow m_addwindow;

		// The progress window when allocating new drives
		ProgressWindow m_progress;

		// The tray icon
		NOTIFYICONDATA m_nid;
		HICON m_hSystrayicon;

		// The tools options window
		OptionsWindow m_optionswindow;

		// The Password Window
		PassWindowEx m_passwindow;

		// The Secure File Manager
		SecureFileManager m_fileman;

		// Standard Encryption class
		StandardEncryption m_enc;
		RSAEncryption m_rsa;

		RegistryHandler m_registry;
		char m_szCedeCryptPath[SIZE_STRING];

		// Control handles
		HWND m_hwndlistview;

		// Wrong password count
		int m_wrongpasswordcount;

		// Shell open folder timer active
		bool m_openfoldertimeractive;

		unsigned int m_activatewindowcount;

		// The convert drive window
		ConvertDriveWindow m_convertwindow;

		// The Drive Formatter class for formatting memory sticks
		DriveFormatter m_driveformat;
		// The Drive backup and restore class
		DriveBackupRestore m_drivebackup;
		// The singledrive info structure storing the
		// selected drive
		SingleDriveInfo m_convertdriveinfo;
		// The password used to convert the drive
		// when doing one click conversion
		SingleDriveInfo m_converttempdrive;
		char m_szConvertpassword[SIZE_STRING];

		char m_szConfigpath[SIZE_STRING];
	
		MemoryBuffer m_memEncpassword; // A piece of encrypted data encrypted using the users password.
		
		DynList m_dlnewdrives;
		//SingleDriveInfo m_localmodedrive;
		bool m_bstandalonemode;

		// The list of all the mounted drives
		DynList m_dlDrivelist;
		HIMAGELIST m_hLarge;   // image list for icon view 
		HIMAGELIST m_hSmall;   // image list for other views 
		
		// Shared Memory IPC
		MemoryBuffer m_memshared;
		UINT g_quitmessage; // Global quit message so that the cededrive process can tell the launcher to quit when we're done with it
		SharedConfigFile m_sharedconfig;

		// Temporary input output buffers
		//MemoryBuffer m_inBuffer;
		//MemoryBuffer m_outBuffer;
		bool m_bAlreadyrunning;

		// event notification from base class
		void OnCreate (HWND hWnd);
		void OnNotify (HWND hWnd, WPARAM wParam, LPARAM lParam);
		void OnCommand (HWND hWnd, WPARAM wParam, LPARAM lParam);
		void OnUICommand (HWND hWnd, WPARAM wParam, LPARAM lParam);		
		void OnPaint (HWND hWnd);
		void OnTimer (WPARAM wParam);
		void OnMouseMove (HWND hWnd, int mouseXPos, int mouseYPos);
		void OnLButtonDown (HWND hWnd);
		void OnLButtonDblClick (HWND hWnd, WPARAM wParam, LPARAM lParam);				
		void OnCryptEvent (HWND hWnd, WPARAM wParam, LPARAM lParam);
		void OnSysTray (HWND hWnd, WPARAM wParam, LPARAM lParam);
		LRESULT OnClose (HWND hWnd, WPARAM wParam, LPARAM lParam);
		void OnLButtonUp (HWND hWnd);
		void OnDisabledShortcutPressed (HWND hWnd, WPARAM wParam, LPARAM lParam);
		void OnDestroy (HWND hWnd);
		void OnIPCEvent (HWND hWnd, WPARAM wParam, LPARAM lParam);
		LRESULT OnPowerBroadcast(HWND hWnd, WPARAM wParam, LPARAM lParam);
		void OnDeviceChange (HWND hWnd, WPARAM wParam, LPARAM lParam);
};
