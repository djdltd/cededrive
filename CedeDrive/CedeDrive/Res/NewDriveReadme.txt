CEDEDRIVE VIRTUAL ENCRYPTED DRIVE



This is a virtual encrypted disk which has been created using CedeDrive.

Use this drive to store all of your data. All of the data stored on this drive is
secured using military grade AES-256 bit encryption.

This drive will be available after you have entered your CedeDrive password. If you 
have chosen to mount this drive on startup, this drive will be available everytime
you logon to your computer.


NOTE: If you have chosen to create a recovery key for this drive just incase you
forget your password, please remember to store this file in a location that is NOT
on this computer or physically located with this computer.



Enjoy peace of mind with your data secured with CedeDrive.

http://www.cedesoft.com
