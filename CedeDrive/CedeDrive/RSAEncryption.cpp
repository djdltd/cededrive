// A standard encryption class. Written by Danny Draper (c) 2008. Provides standard encryption using Crypto API algorithms
// RSA Encryption class for Public / Private key encryption.

#include "RSAEncryption.h"

RSAEncryption::RSAEncryption ()
{
	ZeroMemory (szErrormessage, SIZE_STRING);
}

RSAEncryption::~RSAEncryption ()
{
	
}

void RSAEncryption::GetErrorMessage (char *szOutmessage)
{
	ZeroMemory (szOutmessage, SIZE_STRING);
	strcpy_s (szOutmessage, SIZE_STRING, szErrormessage);
}

bool RSAEncryption::EncryptString (char *szInputString, MemoryBuffer *memEncrypted, MemoryBuffer *memPublickey)
{
	MemoryBuffer memPlain;
	
	if (strlen (szInputString) == 0) {
		sprintf_s (szErrormessage, SIZE_STRING, "Input string was a zero length string. Encryption aborted.");
		return false;
	}

	memPlain.SetSize (strlen (szInputString));
	memPlain.Write ((char *) szInputString, 0, strlen (szInputString));

	bool bRes = EncryptUsingPublic (&memPlain, memEncrypted, memPublickey);

	return bRes;
}

bool RSAEncryption::DecryptString (char *szOutputString, MemoryBuffer *memEncrypted, MemoryBuffer *memPrivatekey)
{
	ZeroMemory (szOutputString, SIZE_STRING);

	MemoryBuffer memDecrypted;

	bool bRes = DecryptUsingPrivate (memEncrypted, &memDecrypted, memPrivatekey);

	if (bRes == true) {

		if (memDecrypted.GetSize () <= SIZE_STRING) {
			memcpy (szOutputString, (char *) memDecrypted.GetBuffer (), memDecrypted.GetSize());			
			return true;
		} else {
			sprintf_s (szErrormessage, SIZE_STRING, "The length of the decrypted data exceeded the maximum string size.");
			return false;
		}

	} else {
		return false;
	}
}

bool RSAEncryption::DecryptUsingPrivate (MemoryBuffer *memEncrypted, MemoryBuffer *memDecrypted, MemoryBuffer *memPrivatekey)
{
	HCRYPTPROV hProvider;
	HCRYPTKEY hImportedPrivate;

	bool bRes = CryptAcquireContext (&hProvider, NULL, MS_ENHANCED_PROV, PROV_RSA_FULL, CRYPT_VERIFYCONTEXT);

	if (bRes == false) {
		sprintf_s (szErrormessage, SIZE_STRING, "CryptAcquireContext Failed - Error %i", GetLastError());
		return false;
	} 
	
	DWORD dwImportedKeyLength = memPrivatekey->GetSize ();
	bRes = CryptImportKey (hProvider, (BYTE *) memPrivatekey->GetBuffer (), dwImportedKeyLength, 0, NULL, &hImportedPrivate);
	if (bRes == false) {
		sprintf_s (szErrormessage, SIZE_STRING, "CryptImportKey Failed - Error %i", GetLastError());
		return false;
	} 
	
	DWORD dwDataBufferLen = memEncrypted->GetSize ();

	// Now actually encrypt the data
	bRes = CryptDecrypt (hImportedPrivate, 0, TRUE, NULL, (BYTE *) memEncrypted->GetBuffer (), &dwDataBufferLen);
	if (bRes == false) {
		sprintf_s (szErrormessage, SIZE_STRING, "CryptDecrypt Failed - Error %i", GetLastError());
		return false;
	} else {

		// Now copy the number of bytes actually decrypted into the decrypted buffer
		memDecrypted->SetSize (dwDataBufferLen);
		memDecrypted->Write (memEncrypted->GetBuffer (), 0, dwDataBufferLen);
	}

	// ******** END OF ENCRYPTION STUFF ***********
	bRes = CryptReleaseContext (hProvider, 0);
	if (bRes == false) {
		sprintf_s (szErrormessage, SIZE_STRING, "CryptReleaseContext Failed - Error %i", GetLastError());
	} 

	return true;
}

bool RSAEncryption::EncryptUsingPublic (MemoryBuffer *memPlain, MemoryBuffer *memEncrypted, MemoryBuffer *memPublickey)
{
	HCRYPTPROV hProvider;
	HCRYPTKEY hImportedPublic;

	bool bRes = CryptAcquireContext (&hProvider, NULL, MS_ENHANCED_PROV, PROV_RSA_FULL, CRYPT_VERIFYCONTEXT);

	if (bRes == false) {
		sprintf_s (szErrormessage, SIZE_STRING, "CryptAcquireContext Failed - Error %i", GetLastError());
		return false;
	} 	
	
	DWORD dwImportedKeyLength = memPublickey->GetSize ();
	bRes = CryptImportKey (hProvider, (BYTE *) memPublickey->GetBuffer (), dwImportedKeyLength, 0, NULL, &hImportedPublic);
	if (bRes == false) {
		sprintf_s (szErrormessage, SIZE_STRING, "CryptImportKey Failed - Error %i", GetLastError());	
		return false;
	} 


	DWORD dwDataBufferLen = memPlain->GetSize ();
	bRes = CryptEncrypt (hImportedPublic, 0, TRUE, NULL, NULL, &dwDataBufferLen, 0);
	if (bRes == false) {
		sprintf_s (szErrormessage, SIZE_STRING, "CryptEncrypt (sizeonly) Failed - Error %i", GetLastError());
		return false;
	} else {

		// Set the size of the destination buffer, and copy the source plaintext data to the encryption buffer

		memEncrypted->SetSize (dwDataBufferLen);
		memEncrypted->Write (memPlain->GetBuffer (), 0, memPlain->GetSize ());
		dwDataBufferLen = memPlain->GetSize (); // Important - this informs the function of how much data actually needs encrypting.

		// Now actually encrypt the data
		bRes = CryptEncrypt (hImportedPublic, 0, TRUE, NULL, (BYTE *) memEncrypted->GetBuffer (), &dwDataBufferLen, memEncrypted->GetSize ());
		if (bRes == false) {
			sprintf_s (szErrormessage, SIZE_STRING, "CryptEncrypt Failed - Error %i", GetLastError());
			return false;
		} 
	}

	// ******** END OF ENCRYPTION STUFF ***********
	bRes = CryptReleaseContext (hProvider, 0);
	if (bRes == false) {
		sprintf_s (szErrormessage, SIZE_STRING, "CryptReleaseContext Failed - Error %i", GetLastError());
	} 

	return true;
}

bool RSAEncryption::GenerateKeys (unsigned int iStrength, MemoryBuffer *outPrivatekey, MemoryBuffer *outPublickey)
{
	HCRYPTPROV hProvider;
	HCRYPTKEY hGeneratedKey;

	bool bRes = CryptAcquireContext (&hProvider, NULL, MS_ENHANCED_PROV, PROV_RSA_FULL, CRYPT_VERIFYCONTEXT);

	if (bRes == false) {		
		sprintf_s (szErrormessage, SIZE_STRING, "CryptAcquireContext Failed - Error %i", GetLastError());
		return false;
	} 

	// Now Generate an encryption key - Full RSA Exchange Key
	// 0x20000000 is a 8192 bit key - used for backdoor encryption of files and folders
	// 0x10000000 is a 4096 bit key
	// 0x08000000 is a 2048 bit key
	// 0x04000000 is a 1024 bit key
	// 0x02000000 is a 512 bit key
	// 0x01800000 is a 384 bit key - used for backdoor encryption of text only encryption

	bool bAlgchosen = false;

	if (iStrength == RSA_384) {
		bRes = CryptGenKey (hProvider, CALG_RSA_KEYX, CRYPT_EXPORTABLE | 0x01800000, &hGeneratedKey);
		bAlgchosen = true;
	}

	if (iStrength == RSA_512) {
		bRes = CryptGenKey (hProvider, CALG_RSA_KEYX, CRYPT_EXPORTABLE | 0x02000000, &hGeneratedKey);
		bAlgchosen = true;	
	}

	if (iStrength == RSA_1024) {
		bRes = CryptGenKey (hProvider, CALG_RSA_KEYX, CRYPT_EXPORTABLE | 0x04000000, &hGeneratedKey);
		bAlgchosen = true;	
	}

	if (iStrength == RSA_2048) {
		bRes = CryptGenKey (hProvider, CALG_RSA_KEYX, CRYPT_EXPORTABLE | 0x08000000, &hGeneratedKey);
		bAlgchosen = true;	
	}

	if (iStrength == RSA_4096) {
		bRes = CryptGenKey (hProvider, CALG_RSA_KEYX, CRYPT_EXPORTABLE | 0x10000000, &hGeneratedKey);
		bAlgchosen = true;	
	}

	if (iStrength == RSA_8192) {
		bRes = CryptGenKey (hProvider, CALG_RSA_KEYX, CRYPT_EXPORTABLE | 0x20000000, &hGeneratedKey);
		bAlgchosen = true;	
	}

	if (bAlgchosen == false) {
		// Default is 512 bit key
		bRes = CryptGenKey (hProvider, CALG_RSA_KEYX, CRYPT_EXPORTABLE | 0x02000000, &hGeneratedKey);
	}
	
	if (bRes == false) {		
		sprintf_s (szErrormessage, SIZE_STRING, "CryptGenKey Failed - Error %i", GetLastError());
		return false;
	} 

	// Now export the PUBLIC key only
	DWORD dwPublicKeyLen = 0;
	bRes = CryptExportKey (hGeneratedKey, 0, PUBLICKEYBLOB, NULL, NULL, &dwPublicKeyLen); // first get the size of the buffer we need
	if (bRes == false) {		
		sprintf_s (szErrormessage, SIZE_STRING, "CryptExportKey PUBLIC (sizeonly) Failed - Error %i", GetLastError());
		return false;
	} else {
		
		outPublickey->SetSize (dwPublicKeyLen);

		// Now actually get the public key
		bRes = CryptExportKey (hGeneratedKey, 0, PUBLICKEYBLOB, NULL, (BYTE *) outPublickey->GetBuffer (), &dwPublicKeyLen);
		if (bRes == false) {
			sprintf_s (szErrormessage, SIZE_STRING, "CryptExportKey PUBLIC Failed - Error %i", GetLastError());
			return false;
		}
	}

	// Now export the PRIVATE key
	DWORD dwPrivateKeyLen = 0;
	bRes = CryptExportKey (hGeneratedKey, 0, PRIVATEKEYBLOB, NULL, NULL, &dwPrivateKeyLen); // first get the size of the buffer we need
	if (bRes == false) {
		sprintf_s (szErrormessage, SIZE_STRING, "CryptExportKey PRIVATE (sizeonly) Failed - Error %i", GetLastError());
		return false;
	} else {
		
		outPrivatekey->SetSize (dwPrivateKeyLen);
		// Now actually get the private key
		bRes = CryptExportKey (hGeneratedKey, 0, PRIVATEKEYBLOB, 0, (BYTE *) outPrivatekey->GetBuffer (), &dwPrivateKeyLen);
		if (bRes == false) {
			sprintf_s (szErrormessage, SIZE_STRING, "CryptExportKey PRIVATE Failed - Error %i", GetLastError());
			return false;
		}
	}

	// ******** END OF ENCRYPTION STUFF ***********
	bRes = CryptReleaseContext (hProvider, 0);
	if (bRes == false) {
		sprintf_s (szErrormessage, SIZE_STRING, "CryptReleaseContext Failed - Error %i", GetLastError());
	}

	return true;
}