#include "DriveFormatter.h"

DriveFormatter::DriveFormatter ()
{
	m_bFileOpen = FALSE;
	m_resultcode = 0;
}

DriveFormatter::~DriveFormatter ()
{
}

void DriveFormatter::SetTrialMode (bool bTrial)
{
	m_bTrialmode = bTrial;
}

bool DriveFormatter::ConvertDriveToNTFS (char *szDriveLetter)
{
	if (m_bTrialmode == true) {
		return true; // Don't do it if we are in trial mode
	}

	// Converts a drive to the NTFS filesystem by using the Windows command line too convert.exe (installed on Windows XP)
	// This is convert mechansim is used whenever a Windows XP host is detected as the Win32_Volume class used by the
	// format script is not available under windows xp.
	// Using result code of 93 and onwards, remember this class uses a global result code so the caller can just quickly
	// find out why anything failed if it did.

    STARTUPINFO si;
    PROCESS_INFORMATION pi;

    ZeroMemory( &si, sizeof(si) );
    si.cb = sizeof(si);
    ZeroMemory( &pi, sizeof(pi) );
    

	// This gets the location of Convert.exe and also sets the command line which will be called.	
	if (PrepareConvertCommandLine(szDriveLetter) == false) {
		m_resultcode = 93;
		return false;
	}

    // Start the child process. 
	if( !CreateProcess(NULL,   // No module name (use command line)
		m_szCommandline,        // Command line
        NULL,           // Process handle not inheritable
        NULL,           // Thread handle not inheritable
        FALSE,          // Set handle inheritance to FALSE
        0,              // No creation flags
        NULL,           // Use parent's environment block
        NULL,           // Use parent's starting directory 
        &si,            // Pointer to STARTUPINFO structure
        &pi )           // Pointer to PROCESS_INFORMATION structure
    ) 
    {
		m_resultcode = 94;
		return false;
    }

    // Wait until child process exits.
    WaitForSingleObject( pi.hProcess, INFINITE );

	// Now get the exit code of the Convert process		
	DWORD dwExitcode;
	GetExitCodeProcess (pi.hProcess, &dwExitcode);
	//m_resultcode = dwExitcode;
	//ShowInt (dwExitcode);

    // Close process and thread handles. 
    CloseHandle( pi.hProcess );
    CloseHandle( pi.hThread );
	
	return true;	
}

bool DriveFormatter::QuickFormatDrive(char *szDriveLetter) 
{
	if (m_bTrialmode == true) {
		return true; // Don't do it if we are in trial mode
	}

	// Quick Format a drive specified by the drive letter. Returns true if successful, false if not. If the format failed,
	// the reason for failure can be obtained by calling GetErrorReason(). The QuickFormat works by creating a VBScript in 
	// a temporary directory which uses WMI to actually perform the format. We then look at the exit code of the WScript
	// command to determine if the format was successful or not.

    STARTUPINFO si;
    PROCESS_INFORMATION pi;

    ZeroMemory( &si, sizeof(si) );
    si.cb = sizeof(si);
    ZeroMemory( &pi, sizeof(pi) );
    
	if (MakeScript(szDriveLetter) == false) {
		m_resultcode = 90;
		return false;
	}

	// This gets the location of WScript.exe and also sets the location of the script to execute
	// this is then placed in m_szCommandline as a string which contains the command we execute to launch
	// the VBScript.
	if (PrepareCommandLine() == false) {
		m_resultcode = 91;
		return false;
	}

    // Start the child process. 
	if( !CreateProcess(NULL,   // No module name (use command line)
		m_szCommandline,        // Command line
        NULL,           // Process handle not inheritable
        NULL,           // Thread handle not inheritable
        FALSE,          // Set handle inheritance to FALSE
        0,              // No creation flags
        NULL,           // Use parent's environment block
        NULL,           // Use parent's starting directory 
        &si,            // Pointer to STARTUPINFO structure
        &pi )           // Pointer to PROCESS_INFORMATION structure
    ) 
    {
		m_resultcode = 92;
		return false;
    }

    // Wait until child process exits.
    WaitForSingleObject( pi.hProcess, INFINITE );

	// Now get the exit code of the WScript process, this will tell us
	// if the script was successful or not as we use the exit code to
	// return the WMI result code.
	DWORD dwExitcode;
	GetExitCodeProcess (pi.hProcess, &dwExitcode);
	m_resultcode = dwExitcode;

	// Now Delete the script, we don't need it anymore
	DeleteFile (m_szScriptpath);

    // Close process and thread handles. 
    CloseHandle( pi.hProcess );
    CloseHandle( pi.hThread );

	if (dwExitcode == 70) {
		return true;
	} else {
		return false;
	}	
}

void DriveFormatter::ShowInt (int iInttoShow) {
	char szMsg[255];
	ZeroMemory (szMsg, 255);
	
	sprintf_s (szMsg, 255, "Value of integer: %d", iInttoShow);
	MessageBox (NULL, szMsg, "ShowInt", MB_OK);
}

void DriveFormatter::GetErrorReason (char *szReason, int reasonlen)
{
	ZeroMemory (szReason, reasonlen);
	if (m_resultcode == 0) {strcpy_s (szReason, reasonlen, "Format Script Failure");}
	if (m_resultcode == 70) {strcpy_s (szReason, reasonlen, "Format was successful");}
	if (m_resultcode == 71) {strcpy_s (szReason, reasonlen, "Unsupported file system");}
	if (m_resultcode == 72) {strcpy_s (szReason, reasonlen, "Incompatible media in drive");}
	if (m_resultcode == 73) {strcpy_s (szReason, reasonlen, "Access denied");}
	if (m_resultcode == 74) {strcpy_s (szReason, reasonlen, "Call canceled");}
	if (m_resultcode == 75) {strcpy_s (szReason, reasonlen, "Call cancellation request too late");}
	if (m_resultcode == 76) {strcpy_s (szReason, reasonlen, "Volume write protected");}
	if (m_resultcode == 77) {strcpy_s (szReason, reasonlen, "Volume lock failed");}
	if (m_resultcode == 78) {strcpy_s (szReason, reasonlen, "Unable to quick format");}
	if (m_resultcode == 79) {strcpy_s (szReason, reasonlen, "Input/Output (I/O) error");}
	if (m_resultcode == 80) {strcpy_s (szReason, reasonlen, "Invalid volume label");}
	if (m_resultcode == 81) {strcpy_s (szReason, reasonlen, "No media in drive");}
	if (m_resultcode == 82) {strcpy_s (szReason, reasonlen, "Volume is too small");}
	if (m_resultcode == 83) {strcpy_s (szReason, reasonlen, "Volume is too large");}
	if (m_resultcode == 84) {strcpy_s (szReason, reasonlen, "Volume is not mounted");}
	if (m_resultcode == 85) {strcpy_s (szReason, reasonlen, "Cluster size is too small");}
	if (m_resultcode == 86) {strcpy_s (szReason, reasonlen, "Cluster size is too large");}
	if (m_resultcode == 87) {strcpy_s (szReason, reasonlen, "Cluster size is beyond 32 bits");}
	if (m_resultcode == 88) {strcpy_s (szReason, reasonlen, "Unknown error");}
	if (m_resultcode == 90) {strcpy_s (szReason, reasonlen, "MakeScript Failed");}
	if (m_resultcode == 91) {strcpy_s (szReason, reasonlen, "PrepareCommandLine Failed");}
	if (m_resultcode == 92) {strcpy_s (szReason, reasonlen, "CreateProcess Failed when attempting to launch script");}
	if (m_resultcode == 93) {strcpy_s (szReason, reasonlen, "Prepare NTFS Convert CommandLine Failed");}
	if (m_resultcode == 94) {strcpy_s (szReason, reasonlen, "CreateProcess Failed when attempting to launch convert command");}

}

bool DriveFormatter::MakeScript (char *szDriveLetter)
{
	// szDriveLetter must be a single letter, the :\ is added later.

	char szScriptline[SIZE_STRING];

	if (PrepareScriptPath() == false) {
		return false;
	}

	FIOOpenAppendFile (m_szScriptpath);

	FIOWriteFileLine ("On Error Resume Next\r\n");
	FIOWriteFileLine ("strComputer = \".\"\r\n");
	FIOWriteFileLine ("Set objWMIService = GetObject(\"winmgmts:\" & \"{impersonationLevel=impersonate}!\\\\\" & strComputer & \"\\root\\cimv2\")\r\n");
	
	ZeroMemory (szScriptline, SIZE_STRING);
	strcpy_s (szScriptline, SIZE_STRING, "Set colVolumes = objWMIService.ExecQuery (\"Select * from Win32_Volume Where Name = '");
	strcat_s (szScriptline, SIZE_STRING, szDriveLetter);
	strcat_s (szScriptline, SIZE_STRING, ":\\\\'\")\r\n");

	FIOWriteFileLine (szScriptline);	
	FIOWriteFileLine ("For Each objVolume in colVolumes\r\n");
	FIOWriteFileLine ("\terrResult = objVolume.Format(\"NTFS\", TRUE)\r\n");
	FIOWriteFileLine ("Next\r\n");

	FIOWriteFileLine ("returnCode = 0\r\n");
	FIOWriteFileLine ("If errResult = 0 Then returnCode = 70\r\n");
	FIOWriteFileLine ("If errResult = 1 Then returnCode = 71\r\n");
	FIOWriteFileLine ("If errResult = 2 Then returnCode = 72\r\n");
	FIOWriteFileLine ("If errResult = 3 Then returnCode = 73\r\n");
	FIOWriteFileLine ("If errResult = 4 Then returnCode = 74\r\n");
	FIOWriteFileLine ("If errResult = 5 Then returnCode = 75\r\n");
	FIOWriteFileLine ("If errResult = 6 Then returnCode = 76\r\n");
	FIOWriteFileLine ("If errResult = 7 Then returnCode = 77\r\n");
	FIOWriteFileLine ("If errResult = 8 Then returnCode = 78\r\n");
	FIOWriteFileLine ("If errResult = 9 Then returnCode = 79\r\n");
	FIOWriteFileLine ("If errResult = 10 Then returnCode = 80\r\n");
	FIOWriteFileLine ("If errResult = 11 Then returnCode = 81\r\n");
	FIOWriteFileLine ("If errResult = 12 Then returnCode = 82\r\n");
	FIOWriteFileLine ("If errResult = 13 Then returnCode = 83\r\n");
	FIOWriteFileLine ("If errResult = 14 Then returnCode = 84\r\n");
	FIOWriteFileLine ("If errResult = 15 Then returnCode = 85\r\n");
	FIOWriteFileLine ("If errResult = 16 Then returnCode = 86\r\n");
	FIOWriteFileLine ("If errResult = 17 Then returnCode = 87\r\n");
	FIOWriteFileLine ("If errResult = 18 Then returnCode = 88\r\n");

	FIOWriteFileLine ("WScript.Quit (returnCode)\r\n");

	FIOClose();

	return true;
}

bool DriveFormatter::PrepareCommandLine ()
{
	char szSystemRoot[SIZE_STRING];

	ZeroMemory (szSystemRoot, SIZE_STRING);
	ZeroMemory (m_szCommandline	, SIZE_STRING);

	if (GetEnvironmentVariable ("SystemRoot", szSystemRoot, SIZE_STRING) == 0) {
		return false;
	}

	strcpy_s (m_szCommandline, SIZE_STRING, szSystemRoot);
	//strcat_s (m_szCommandline, SIZE_STRING, "\\System32\\WScript.exe //B"); // The /B parameter tells WScript to execute in batch mode thus supressing all message prompts or error message boxes.
	strcat_s (m_szCommandline, SIZE_STRING, "\\System32\\WScript.exe"); // The /B parameter tells WScript to execute in batch mode thus supressing all message prompts or error message boxes.
	strcat_s (m_szCommandline, SIZE_STRING, " ");
	strcat_s (m_szCommandline, SIZE_STRING, "\"");
	strcat_s (m_szCommandline, SIZE_STRING, m_szScriptpath);
	strcat_s (m_szCommandline, SIZE_STRING, "\"");

	//strcpy_s (m_szCommandline, SIZE_STRING, 
	return true;
}

bool DriveFormatter::PrepareConvertCommandLine (char *szDriveletter)
{
	char szSystemRoot[SIZE_STRING];

	ZeroMemory (szSystemRoot, SIZE_STRING);
	ZeroMemory (m_szCommandline	, SIZE_STRING);

	if (GetEnvironmentVariable ("SystemRoot", szSystemRoot, SIZE_STRING) == 0) {
		return false;
	}

	strcpy_s (m_szCommandline, SIZE_STRING, szSystemRoot);	
	strcat_s (m_szCommandline, SIZE_STRING, "\\System32\\convert.exe");
	strcat_s (m_szCommandline, SIZE_STRING, " ");	
	strcat_s (m_szCommandline, SIZE_STRING, szDriveletter);	
	strcat_s (m_szCommandline, SIZE_STRING, ": /FS:NTFS");
	
	return true;
}

bool DriveFormatter::PrepareScriptPath ()
{
	int iRes = 0;
	char szAppData[SIZE_STRING];
	char szCompanyAppData[SIZE_STRING];
	char szProgramAppData[SIZE_STRING];
	char szTempScriptsDir[SIZE_STRING];
	char szInfoAppData[SIZE_STRING];

	ZeroMemory (szAppData, SIZE_STRING);
	ZeroMemory (szCompanyAppData, SIZE_STRING);
	ZeroMemory (szProgramAppData, SIZE_STRING);
	ZeroMemory (szTempScriptsDir, SIZE_STRING);
	ZeroMemory (szInfoAppData, SIZE_STRING);

	if (GetEnvironmentVariable ("APPDATA", szAppData, SIZE_STRING) == 0) {
		return false;
	}

	strcpy_s (szCompanyAppData, SIZE_STRING, szAppData);
	strcat_s (szCompanyAppData, SIZE_STRING, "\\CedeSoft");
	_mkdir (szCompanyAppData);

	strcpy_s (szProgramAppData, SIZE_STRING, szCompanyAppData);
	strcat_s (szProgramAppData, SIZE_STRING, "\\CedeDrive");
	_mkdir (szProgramAppData);

	strcpy_s (szTempScriptsDir, SIZE_STRING, szProgramAppData);
	strcat_s (szTempScriptsDir, SIZE_STRING, "\\TempScripts");
	_mkdir (szTempScriptsDir);

	strcpy_s (szInfoAppData, SIZE_STRING, szTempScriptsDir);
	strcat_s (szInfoAppData, SIZE_STRING, "\\formatscript.vbs");

	// Set our member variable which is used by the other functions
	ZeroMemory (m_szScriptpath, SIZE_STRING);
	strcpy_s (m_szScriptpath, SIZE_STRING, szInfoAppData);
	
	return true;
}

int DriveFormatter::FIOOpenAppendFile (LPCSTR strFilename)
{
	if( (stream = fopen(strFilename, "w+t" )) != NULL ) {
		m_bFileOpen = TRUE;
		return 0;
	} else {
		return -1;
	}
}

void DriveFormatter::FIOWriteFileLine(LPCSTR strFileline)
{
	if (m_bFileOpen == TRUE) {
		fwrite(strFileline, 1, strlen(strFileline), stream );
	} else {
		//MessageBox (NULL, "File is not open", "VBScript Launcher", MB_ICONEXCLAMATION | MB_OK);
	}
}

void DriveFormatter::FIOClose ()
{
	if (m_bFileOpen == TRUE) {
		fclose(stream);
	}
}