#pragma once
#include <windows.h>
#include <io.h>
#include <commctrl.h>
#include "UIWindow.h"
#include "Diagnostics.h"
#include "UIHandler.h"
#include "UIBanner.h"
#include "UIRect.h"
#include "UILabel.h"


class PassWindowFull : public UIWindow
{
	public:
		PassWindowFull ();
		~PassWindowFull ();
		void SetDiagnostics (Diagnostics *pdiag);
		
		void Initialise (HWND hWnd, unsigned int uID);
		void OutputInt (LPCSTR lpszText, int iValue);
		void OutputText (LPCSTR lpszText);
		void OutputText (LPCSTR lpszName, LPCSTR lpszValue);

		static LRESULT WINAPI SubProc(HWND hWnd, WORD wMessage, WPARAM wParam, LPARAM lParam);
		static LRESULT WINAPI SubProc2(HWND hWnd, WORD wMessage, WPARAM wParam, LPARAM lParam);
		void ParseUserInput ();
		void CheckPassword ();	
		void ClearPassword ();
		void SetEncryptMode (bool bEncrypt);
		bool GetSessionMode ();
		bool ContainsNumber (char *szPassword);
		void SetDriveName (char *szName);
		void SetSecondFocus ();
		char *GetLastPassword ();
		void SetWindowFocus ();		
		void CancelPassword ();
		void ShowAccessDenied();
		BOOL MySystemShutdown();

		bool m_bEncryptmode; // Need to make this public so it can be accessed by the static functions through the class pointer
		bool m_bSessionmode;
		bool m_bInitialised;
	private:
		static LONG_PTR WINAPI lpfnOldWndProc;
		static LONG_PTR WINAPI lpfnOldWndProc2;

		static PassWindowFull *ppwnd;
		// Private Member Variables & objects

		// The UI Handler required for multiple handling of custom controls.
		UIHandler m_uihandler;
		
		// ID ofthis window - required for window class registration purposes
		unsigned int m_ID;

		// Global Window Handle
		HWND m_hwnd;
		HWND m_parenthwnd;
		HWND m_hwndeditpass;
		HWND m_hwndeditpass2;
		HWND m_hwndbtnpassok;
		HWND m_hwndbtnpasscancel;

		UILabel m_headerlabel;
		UILabel m_subheader;
		UILabel m_bottomheader;
		UILabel m_editpass1;
		UILabel m_editpass2;

		// The header bitmap image
		UIBanner m_header;				
		UIBanner m_passbg;

		// Flag indicating if we're using diagnostics
		bool m_bUseDiagnostics;
		Diagnostics *m_pdiag;

		// Last entered password
		char m_szPassword[SIZE_NAME];

		// Registered class name
		// We need a different class name for every instance of
		// this window. This class name
		// Is created by the Initialise routine
		// with a uID value suffixed to the end
		char m_szClassname[SIZE_STRING];

		// event notification from base class
		void OnDestroy (HWND hWnd);
		void OnCreate (HWND hWnd);		
		void OnCommand (HWND hWnd, WPARAM wParam, LPARAM lParam);
		void OnUICommand (HWND hWnd, WPARAM wParam, LPARAM lParam);		
		void OnUIScroll (HWND hWnd, WPARAM wParam, LPARAM lParam);
		void OnPaint (HWND hWnd);
		void OnTimer (WPARAM wParam);
		void OnMouseMove (HWND hWnd, int mouseXPos, int mouseYPos);
		void OnLButtonDown (HWND hWnd);
		void OnLButtonUp (HWND hWnd);
};
