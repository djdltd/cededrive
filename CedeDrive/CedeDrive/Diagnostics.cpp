// This work is dedicated to the Lord our God. King of Heaven, Lord of Heaven's Armies.

#include "Diagnostics.h"

Diagnostics::Diagnostics ()
{
}

Diagnostics::~Diagnostics ()
{
	m_bShowdiagnostics = false;
}

void Diagnostics::ParseCommandLine (char *pszCmdline)
{
	//MessageBox (NULL, "Hello", "Info", MB_OK);
	//MessageBox (NULL, pszCmdline, "Command Line", MB_OK);
	char szExtendedAction[SIZE_STRING];
	ZeroMemory (szExtendedAction, SIZE_STRING);
	strncpy_s (szExtendedAction, SIZE_STRING, pszCmdline, 5);

	if (strcmp (szExtendedAction, "/diag") == 0) {
		m_bShowdiagnostics = true;
	}
}

HWND Diagnostics::GetListBoxHWND ()
{
	return m_hwnddiaglist;
}

void Diagnostics::Initialise (HWND hWnd, LPSTR lpCmdline)
{	
	SetParentHWND (hWnd);
	SetBgColor (RGB (200, 200, 200));
	SetParentHWND (hWnd);
	SetCaption (TEXT ("CedeDrive Diagnostics"));
	SetWindowStyle (FS_STYLESTANDARD);
	CreateAppWindow ("COMMDiagnosticsClass", 70, 0, 600, 350, true);
	//m_uihandler.SetWindowProperties (0, 70, 40, 443, RGB (200, 200, 200));		
	SetWindowPosition (FS_TOPLEFT);

	ParseCommandLine (lpCmdline);

	if (m_bShowdiagnostics == true) {
		Show ();
	}
}

void Diagnostics::OnCreate (HWND hWnd)
{			
	
	m_header.SetBitmapResources (IDB_DIAGHEADER);
	m_header.SetBitmapProperties (0, 0, 591, 41);
	m_header.SetProperties (hWnd, CID_HEADER, 1, 1, 591, 41);
	//m_uihandler.AddDirectControl (&m_header);
	
	HFONT hfDefault = (HFONT) GetStockObject (DEFAULT_GUI_FONT);
	m_hwnddiaglist = CreateWindow ("listbox", NULL, WS_CHILD | WS_VISIBLE | WS_HSCROLL | WS_VSCROLL, 1, 43, 591, 283, hWnd, (HMENU) ID_DIAGLIST, GetModuleHandle (NULL), NULL) ;
	SendMessage (m_hwnddiaglist, WM_SETFONT, (WPARAM) hfDefault, MAKELPARAM (FALSE, 0));
	OutputInt ("Diagnostics Ready: ", 0);
}

void Diagnostics::OutputInt (LPCSTR lpszText, int iValue)
{
	char szInteger[SIZE_INTEGER];
	ZeroMemory (szInteger, SIZE_INTEGER);
	sprintf_s (szInteger, SIZE_INTEGER, "%d", iValue);

	char szText[SIZE_STRING];
	ZeroMemory (szText, SIZE_STRING);
	strcpy_s (szText, SIZE_STRING, lpszText);

	strcat_s (szText, SIZE_STRING, szInteger);

	SendMessage (m_hwnddiaglist, LB_ADDSTRING, 0, (LPARAM) (LPCTSTR) &szText);

	int lCount = SendMessage (m_hwnddiaglist, LB_GETCOUNT, 0, 0);
	SendMessage (m_hwnddiaglist, LB_SETCURSEL, lCount-1, 0);
}

void Diagnostics::OutputText (LPCSTR lpszText)
{
	char szText[SIZE_STRING];
	ZeroMemory (szText, SIZE_STRING);
	strcpy_s (szText, SIZE_STRING, lpszText);
	SendMessage (m_hwnddiaglist, LB_ADDSTRING, 0, (LPARAM) (LPCTSTR) &szText);

	int lCount = SendMessage (m_hwnddiaglist, LB_GETCOUNT, 0, 0);
	SendMessage (m_hwnddiaglist, LB_SETCURSEL, lCount-1, 0);
}

void Diagnostics::OutputText (LPCSTR lpszName, LPCSTR lpszValue)
{
	char szText[SIZE_STRING*2];
	ZeroMemory (szText, SIZE_STRING*2);	
	strcat_s (szText, SIZE_STRING*2, lpszName);
	strcat_s (szText, SIZE_STRING*2, lpszValue);

	SendMessage (m_hwnddiaglist, LB_ADDSTRING, 0, (LPARAM) (LPCTSTR) &szText);

	int lCount = SendMessage (m_hwnddiaglist, LB_GETCOUNT, 0, 0);
	SendMessage (m_hwnddiaglist, LB_SETCURSEL, lCount-1, 0);
}

int Diagnostics::FIOOpenAppendFile (LPCSTR strFilename)
{
	if( (m_stream = fopen (strFilename, "a+t" )) != NULL ) {
		m_bFileOpen = true;
		return 0;
	} else {
		return -1;
	}
}

void Diagnostics::FIOWriteFileLine(LPCSTR strFileline)
{

	if (m_bFileOpen == true) {
		//strFileline += "\n";
		fwrite(strFileline, 1, strlen(strFileline), m_stream );
	}
}


void Diagnostics::FIOCloseOutFile()
{
	if (m_bFileOpen == true) {
		_fcloseall ();
		m_bFileOpen = false;
	}
}

void Diagnostics::SaveEventLogEntries ( )
{
    HANDLE h;
    EVENTLOGRECORD *pevlr; 
    BYTE bBuffer[BUFFER_SIZE]; 
    DWORD dwRead, dwNeeded, dwThisRecord; 
 
    // Open the Application event log. 
 
    h = OpenEventLog( NULL, "Application");   // source name
    if (h == NULL) 
    {
        MessageBox (NULL, "CedeDrive was unable to open the Application Event Log", "Unable to open event log", MB_OK);
        return;
    }
 
    pevlr = (EVENTLOGRECORD *) &bBuffer;
 
    // Get the record number of the oldest event log record.
    GetOldestEventLogRecord(h, &dwThisRecord);

    // Opening the event log positions the file pointer for this 
    // handle at the beginning of the log. Read the event log records 
    // sequentially until the last record has been read. 
	int numdisplayed = 0;


	for (int i=0;i<2000;i++) {
		if (ReadEventLog(h, EVENTLOG_FORWARDS_READ | EVENTLOG_SEQUENTIAL_READ, 0, pevlr, BUFFER_SIZE, &dwRead, &dwNeeded) != 0)
		{
			while (dwRead > 0)
			{ 

				char szEventsource[SIZE_STRING];
				ZeroMemory (szEventsource, SIZE_STRING);
				strcpy_s (szEventsource, SIZE_STRING, (LPSTR) ((LPBYTE) pevlr + sizeof(EVENTLOGRECORD)));


				//if (strcmp (szEventsource, "CedeDrive") == 0) {

					// Print the record number, event identifier, type, 
					// and source name.  
					char szEventInfo[SIZE_STRING];
					ZeroMemory (szEventInfo, SIZE_STRING);
					sprintf_s(szEventInfo, SIZE_STRING, "%03d  Event ID: 0x%08X  Event type: ", dwThisRecord++, pevlr->EventID); 

					

					switch(pevlr->EventType)
					{
						case EVENTLOG_ERROR_TYPE:
							strcat_s (szEventInfo, SIZE_STRING, "EVENTLOG_ERROR_TYPE\t  ");
							break;
						case EVENTLOG_WARNING_TYPE:
							strcat_s (szEventInfo, SIZE_STRING, "EVENTLOG_WARNING_TYPE\t  ");
							break;
						case EVENTLOG_INFORMATION_TYPE:
							strcat_s (szEventInfo, SIZE_STRING, "EVENTLOG_INFORMATION_TYPE  ");
							break;
						case EVENTLOG_AUDIT_SUCCESS:
							strcat_s (szEventInfo, SIZE_STRING, "EVENTLOG_AUDIT_SUCCESS\t  ");
							break;
						case EVENTLOG_AUDIT_FAILURE:
							strcat_s (szEventInfo, SIZE_STRING, "EVENTLOG_AUDIT_FAILURE\t  ");
							break;
						default:
							strcat_s (szEventInfo, SIZE_STRING, "Unknown ");
							break;
					}

					strcat_s (szEventInfo, SIZE_STRING, "Event Source: ");
					strcat_s (szEventInfo, SIZE_STRING, szEventsource);
		
					//printf("Event source: %s\n", szEventsource); 
					char szDescription[SIZE_LARGESTRING];
					ZeroMemory (szDescription, SIZE_LARGESTRING);
					sprintf_s (szDescription, SIZE_LARGESTRING, "Description: %s\n", (LPSTR) ((LPBYTE) pevlr+pevlr->StringOffset));

					FIOWriteFileLine (szEventInfo);
					FIOWriteFileLine ("\r\n");
					FIOWriteFileLine (szDescription);

					dwRead -= pevlr->Length; 
					pevlr = (EVENTLOGRECORD *) ((LPBYTE) pevlr + pevlr->Length); 

					numdisplayed++;

					//if (numdisplayed > 20) {
						//break;
					//}
				}
			//} 
	 
			pevlr = (EVENTLOGRECORD *) &bBuffer; 
					
		}
	}
 
    CloseEventLog(h); 
}

void Diagnostics::SaveDiagnostics (DynList *dlDrivelist)
{
	char szProfilepath[SIZE_STRING];
	ZeroMemory (szProfilepath, SIZE_STRING);

	GetEnvironmentVariable ("USERPROFILE", szProfilepath, SIZE_STRING);
	strcat_s (szProfilepath, SIZE_STRING, "\\Desktop\\CedeDrive-Diagnostics.log");

	FIOOpenAppendFile (szProfilepath);

	SaveEventLogEntries ();

	FIOWriteFileLine ("\r\n");
	FIOWriteFileLine ("\r\n");
	FIOWriteFileLine ("CedeDrive Drive List: ");
	FIOWriteFileLine ("\r\n");
	SingleDriveInfo *pinfo;

	char szSize[SIZE_NAME];	

	if (dlDrivelist->GetNumItems () == 0) {
		FIOWriteFileLine ("No drives have been created.");
		FIOWriteFileLine ("\r\n");
	}

	for (unsigned int a=0;a<dlDrivelist->GetNumItems ();a++) {
	
		pinfo = (SingleDriveInfo *) dlDrivelist->GetItem (a);

		FIOWriteFileLine ("Name: ");
		FIOWriteFileLine (pinfo->szName);
		FIOWriteFileLine ("\r\n");
		
		FIOWriteFileLine ("Description: ");
		FIOWriteFileLine (pinfo->szDescription);
		FIOWriteFileLine ("\r\n");

		FIOWriteFileLine ("Path: ");
		FIOWriteFileLine (pinfo->szPath);
		FIOWriteFileLine ("\r\n");

		ZeroMemory (szSize, SIZE_NAME);
		_ltoa_s (pinfo->lDiskSizeMegs, szSize, SIZE_NAME, 10);

		FIOWriteFileLine ("Disk Size: ");
		FIOWriteFileLine (szSize);
		FIOWriteFileLine ("\r\n");

		FIOWriteFileLine ("Mount at Startup: ");
		if (pinfo->bMountStartup == true) {
			FIOWriteFileLine ("YES");
		} else {
			FIOWriteFileLine ("NO");
		}
		FIOWriteFileLine ("\r\n");


		FIOWriteFileLine ("----------------------------------------------");
		FIOWriteFileLine ("\r\n");
		FIOWriteFileLine ("\r\n");

	}

	FIOCloseOutFile ();	
}

void Diagnostics::OnDestroy (HWND hWnd)
{
	//MessageBox (NULL, "Diagnostics being destroyed!", "Test", MB_OK);
	PostMessage (g_ParentHWND, WM_UICOMMAND, CC_QUITNOW, 0);
}

void Diagnostics::OnCommand (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	
}

void Diagnostics::OnUICommand (HWND hWnd, WPARAM wParam, LPARAM lParam)
{

}

void Diagnostics::OnTimer (WPARAM wParam)
{	
	m_uihandler.NotifyTimer (wParam);
}

void Diagnostics::OnPaint (HWND hWnd)
{
	m_uihandler.PaintControls (hWnd);	
}

void Diagnostics::OnMouseMove (HWND hWnd, int mouseXPos, int mouseYPos)
{	
	m_uihandler.NotifyMouseMove (mouseXPos, mouseYPos);
}

void Diagnostics::OnLButtonDown (HWND hWnd)
{
	m_uihandler.NotifyMouseDown ();
}

void Diagnostics::OnLButtonUp (HWND hWnd)
{
	m_uihandler.NotifyMouseUp ();
}