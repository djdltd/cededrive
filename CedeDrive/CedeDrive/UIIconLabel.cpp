#include "UIIconLabel.h"

UIIconLabel::UIIconLabel ()
{
	m_bVisible = true;
	m_ivisibleicon = 0;
}

UIIconLabel::~UIIconLabel ()
{

}

void UIIconLabel::SetVisible (bool bVisible)
{
	m_bVisible = bVisible;
	InvalidateControl (false);
}

void UIIconLabel::SetIconResources (unsigned int wRes1, unsigned int wRes2)
{
	// Load the bitmap resource. Only one resource is required for a Banner. Later on we
	// need to specify how the Banner is painted, and we need to specify which area of the banner
	// is designated as a repeating area. So, if the window gets larger or smaller an area of the banner
	// will be repeated if the base bitmap does not meet the required size.
	g_hbmBase = LoadBitmapResource (wRes1);
	g_hbmDown = LoadBitmapResource (wRes2);
}

void UIIconLabel::InvalidateIconLabel ()
{
	RECT Rect;

	Rect.left = g_xPos-g_iBitmapwidth-7;
	Rect.right = g_xPos+g_Width + g_iBitmapwidth;
	Rect.top = g_yPos - 5;
	Rect.bottom = g_yPos+g_Height + 5;

	InvalidateRect (g_hWnd, &Rect, FALSE);
}

void UIIconLabel::SetVisibleIcon (int iIcon)
{
	// 0 is none
	// 1 is primary
	// 2 is secondary
	m_ivisibleicon = iIcon;
	InvalidateIconLabel ();
}

unsigned long UIIconLabel::GetSize () {
	return sizeof (UIIconLabel);
}

void UIIconLabel::SetFontStyle (int fontstyle)
{
	g_FontStyle = fontstyle;
	InvalidateControl (true);
}

void UIIconLabel::SetColor (COLORREF color)
{
	g_textcolor = color;
	InvalidateControl (true);
}

void UIIconLabel::SetIconProperties (int Width, int Height)
{
	g_iBitmapheight = Height;
	g_iBitmapwidth = Width;
}

void UIIconLabel::Paint (HDC hdc)
{
	if (m_bVisible == false) {
		return;
	}

	if (m_ivisibleicon > 0) {
		// Paint the icon to the screen
		BITMAP bm;
		HDC hdcMem = CreateCompatibleDC (hdc);
		HBITMAP hbmOld;
		
		if (m_ivisibleicon == 1) {
			hbmOld = (HBITMAP) SelectObject (hdcMem, g_hbmBase);
			GetObject (g_hbmBase, sizeof (bm), &bm);
		}

		if (m_ivisibleicon == 2) {
			hbmOld = (HBITMAP) SelectObject (hdcMem, g_hbmDown);
			GetObject (g_hbmDown, sizeof (bm), &bm);
		}

		BitBlt (hdc, g_xPos-g_iBitmapwidth-7, g_yPos-2, g_iBitmapwidth, g_iBitmapheight, hdcMem, 0, 0, SRCCOPY);

		SelectObject (hdcMem, hbmOld);
		DeleteDC (hdcMem);
		DeleteObject (hbmOld);
	}

	PaintText (hdc);
}

void UIIconLabel::NotifyMouseMove (int iXPos, int iYPos)
{
	g_mouseXPos = iXPos;
	g_mouseYPos = iYPos;
}

void UIIconLabel::NotifyMouseUp()
{
	if (IsMouseRect () == true) {
		PostMessage (g_hWnd, WM_UICOMMAND, (WPARAM) g_controlID, g_mouseXPos);
	}
}
