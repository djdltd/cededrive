#include <windows.h>
#include "resource.h"
#include "UIWindow.h"
#include "MainWindow.h"
#include "Diagnostics.h"

////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////// GLOBAL VARIABLES
////////////////////////////////////////////////////////////////////////////////////////

MainWindow g_windowmain;
MainWindow* MainWindow::m_pwnd = NULL;
VirtualDriveManager* VirtualDriveManager::m_pinstance = NULL;

FARPROC PasswordWindow::lpfnOldWndProc = NULL;
FARPROC PasswordWindow::lpfnOldWndProc2 = NULL;
PasswordWindow *PasswordWindow::ppwnd = NULL;

LONG_PTR WINAPI PassWindowEx::lpfnOldWndProc = NULL;
LONG_PTR WINAPI PassWindowEx::lpfnOldWndProc2 = NULL;
PassWindowEx *PassWindowEx::ppwnd = NULL;

LONG_PTR WINAPI PassWindowFull::lpfnOldWndProc = NULL;
LONG_PTR WINAPI PassWindowFull::lpfnOldWndProc2 = NULL;
PassWindowFull *PassWindowFull::ppwnd = NULL;

LONG_PTR WINAPI ConvertDriveWindow::lpfnOldWndProc = NULL;
LONG_PTR WINAPI ConvertDriveWindow::lpfnOldWndProc2 = NULL;
ConvertDriveWindow *ConvertDriveWindow::ppwnd = NULL;

DirScanner *DirScanner::m_pdirscanner = NULL;

Diagnostics g_windowdiag;
const char g_mutexname [] = "CedeDriveEngine_Win32_Instance_Mutex";

////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////// FUNCTION PROTOTYPES
////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////// IMPLEMENTATION
////////////////////////////////////////////////////////////////////////////////////////

bool IsInstanceRunning ()
{
	// Create the named mutex
	HANDLE mutex = CreateMutex(NULL, FALSE, g_mutexname);
	if (mutex == NULL)
		return true;

	// Check that the mutex didn't already exist
	if (GetLastError() == ERROR_ALREADY_EXISTS)
		return true;

	return false;
}

int __stdcall WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) 
{
	bool bAlreadyrunning = IsInstanceRunning();
	HWND hWnd = NULL;
	
	MSG msg;

	// Ensure that the common control DLL is loaded
	InitCommonControls ();

	g_windowdiag.Initialise (hWnd, lpCmdLine);
	
	g_windowmain.SetDiagnostics (&g_windowdiag);
	g_windowmain.Initialise (hWnd, bAlreadyrunning, lpCmdLine);
	
	// Enter the message loop
	while (GetMessage (&msg, NULL, 0, 0))
	{
		TranslateMessage (&msg);
		DispatchMessage (&msg);
	}

	return 0;
}