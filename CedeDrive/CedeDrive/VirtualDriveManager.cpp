#include "VirtualDriveManager.h"

VirtualDriveManager::VirtualDriveManager ()
{
	m_diagnosticsset = false;
	m_binitialised = false;
	m_pinstance = (VirtualDriveManager *) this;
	m_bminutespassed = 0;
}

VirtualDriveManager::~VirtualDriveManager ()
{
}

void VirtualDriveManager::OutputInt (LPCSTR lpszText, int iValue)
{
	if (m_diagnosticsset == true) {
		m_pdiag->OutputInt (lpszText, iValue);
	}
}

void VirtualDriveManager::OutputText (LPCSTR lpszText)
{
	if (m_diagnosticsset == true) {
		m_pdiag->OutputText (lpszText);
	}
}

void VirtualDriveManager::OutputText (LPCSTR lpszName, LPCSTR lpszValue)
{
	if (m_diagnosticsset == true) {
		m_pdiag->OutputText (lpszName, lpszValue);
	}
}

void VirtualDriveManager::SetHWND (HWND hWnd)
{
	m_hwnd = hWnd;
}

void VirtualDriveManager::SetTrialMode (bool bTrialmode)
{
	m_bTrialmode = bTrialmode;
}

void VirtualDriveManager::NotifyTimer (WPARAM wParam)
{
	switch (wParam)
	{
		case DISCONNECTTIMER:
		{
			if (m_bminutespassed > 19) {
				UnmountAllVirtualDrives ();
				m_bminutespassed = 0;
				KillTimer (m_hwnd, DISCONNECTTIMER);
				MessageBox (NULL, "Thank you for trying CedeDrive, a secure virtual drive encryption solution for desktops, laptops and removable drives. For full purchase information please visit http://www.cedesoft.com.\n\n20 minutes has passed since mounting your virtual drive, and your virtual drive has been disconnected. To remove this limitation please visit our website for full purchase information.", "Trial Version Information", MB_OK | MB_ICONINFORMATION);
			} else {
				m_bminutespassed++;
			}
	
		}
		break;
	}
}

void VirtualDriveManager::SetDiagnostics (Diagnostics *pdiag)
{
	m_pdiag = pdiag;
	m_diagnosticsset = true;
}

void VirtualDriveManager::Initialise (char *szEncryptPassword)
{
	//if (m_binitialised == true) {
		//return;
	//}

	strcpy_s (m_szPassword, SIZE_STRING, szEncryptPassword);
	
	//m_memCipher.SetSize (1024*1024);

	//if (m_enc.EncryptBuffer (&m_memCipher, m_szPassword, true) == true) {
		//printf ("Encryption Successful - key has been created\n");
		//OutputText ("Encryption Successful - key has been created");
	//} else {
		//printf ("Encryption Failed!!!\n");
		//OutputText ("Encryption Failed.");
	//}

	m_binitialised = true;
}

void VirtualDriveManager::ForgetPassword ()
{
	SecureZeroMemory (m_szPassword, SIZE_STRING);
}

int VirtualDriveManager::DriveLetterToNumber (char *szDriveletter)
{
	if (strcmp (szDriveletter, "A") == 0) {return 0;}
	if (strcmp (szDriveletter, "B") == 0) {return 1;}
	if (strcmp (szDriveletter, "C") == 0) {return 2;}
	if (strcmp (szDriveletter, "D") == 0) {return 3;}
	if (strcmp (szDriveletter, "E") == 0) {return 4;}
	if (strcmp (szDriveletter, "F") == 0) {return 5;}
	if (strcmp (szDriveletter, "G") == 0) {return 6;}
	if (strcmp (szDriveletter, "H") == 0) {return 7;}
	if (strcmp (szDriveletter, "I") == 0) {return 8;}
	if (strcmp (szDriveletter, "J") == 0) {return 9;}
	if (strcmp (szDriveletter, "K") == 0) {return 10;}
	if (strcmp (szDriveletter, "L") == 0) {return 11;}
	if (strcmp (szDriveletter, "M") == 0) {return 12;}
	if (strcmp (szDriveletter, "N") == 0) {return 13;}
	if (strcmp (szDriveletter, "O") == 0) {return 14;}
	if (strcmp (szDriveletter, "P") == 0) {return 15;}
	if (strcmp (szDriveletter, "Q") == 0) {return 16;}
	if (strcmp (szDriveletter, "R") == 0) {return 17;}
	if (strcmp (szDriveletter, "S") == 0) {return 18;}
	if (strcmp (szDriveletter, "T") == 0) {return 19;}
	if (strcmp (szDriveletter, "U") == 0) {return 20;}
	if (strcmp (szDriveletter, "V") == 0) {return 21;}
	if (strcmp (szDriveletter, "W") == 0) {return 22;}
	if (strcmp (szDriveletter, "X") == 0) {return 23;}
	if (strcmp (szDriveletter, "Y") == 0) {return 24;}
	if (strcmp (szDriveletter, "Z") == 0) {return 25;}
	
	return -1;
}

void VirtualDriveManager::NumberToDriveLetter (char *szOutdrive, int iNumber)
{
	ZeroMemory (szOutdrive, SIZE_NAME);

	if (iNumber == 0) {strcpy_s (szOutdrive, SIZE_NAME, "A");}
	if (iNumber == 1) {strcpy_s (szOutdrive, SIZE_NAME, "B");}
	if (iNumber == 2) {strcpy_s (szOutdrive, SIZE_NAME, "C");}
	if (iNumber == 3) {strcpy_s (szOutdrive, SIZE_NAME, "D");}
	if (iNumber == 4) {strcpy_s (szOutdrive, SIZE_NAME, "E");}
	if (iNumber == 5) {strcpy_s (szOutdrive, SIZE_NAME, "F");}
	if (iNumber == 6) {strcpy_s (szOutdrive, SIZE_NAME, "G");}
	if (iNumber == 7) {strcpy_s (szOutdrive, SIZE_NAME, "H");}
	if (iNumber == 8) {strcpy_s (szOutdrive, SIZE_NAME, "I");}
	if (iNumber == 9) {strcpy_s (szOutdrive, SIZE_NAME, "J");}
	if (iNumber == 10) {strcpy_s (szOutdrive, SIZE_NAME, "K");}
	if (iNumber == 11) {strcpy_s (szOutdrive, SIZE_NAME, "L");}
	if (iNumber == 12) {strcpy_s (szOutdrive, SIZE_NAME, "M");}
	if (iNumber == 13) {strcpy_s (szOutdrive, SIZE_NAME, "N");}
	if (iNumber == 14) {strcpy_s (szOutdrive, SIZE_NAME, "O");}
	if (iNumber == 15) {strcpy_s (szOutdrive, SIZE_NAME, "P");}
	if (iNumber == 16) {strcpy_s (szOutdrive, SIZE_NAME, "Q");}
	if (iNumber == 17) {strcpy_s (szOutdrive, SIZE_NAME, "R");}
	if (iNumber == 18) {strcpy_s (szOutdrive, SIZE_NAME, "S");}
	if (iNumber == 19) {strcpy_s (szOutdrive, SIZE_NAME, "T");}
	if (iNumber == 20) {strcpy_s (szOutdrive, SIZE_NAME, "U");}
	if (iNumber == 21) {strcpy_s (szOutdrive, SIZE_NAME, "V");}
	if (iNumber == 22) {strcpy_s (szOutdrive, SIZE_NAME, "W");}
	if (iNumber == 23) {strcpy_s (szOutdrive, SIZE_NAME, "X");}
	if (iNumber == 24) {strcpy_s (szOutdrive, SIZE_NAME, "Y");}
	if (iNumber == 25) {strcpy_s (szOutdrive, SIZE_NAME, "Z");}
}

bool VirtualDriveManager::DoesDriveExist (char *szDriveLetter)
{
	char szDrives[SIZE_STRING];
	ZeroMemory (szDrives, SIZE_STRING);

	char szCurDrive[SIZE_STRING];
	char szLetter[SIZE_NAME];
	char szAvaildrive[SIZE_NAME];
	char szDriveLetterUpr[SIZE_NAME];
	ZeroMemory (szDriveLetterUpr, SIZE_NAME);
	strcpy_s (szDriveLetterUpr, SIZE_NAME, szDriveLetter);
	_strupr_s (szDriveLetterUpr, SIZE_NAME);

	int a = 0;
	int l = 0;
	int len = 0;

	len = GetLogicalDriveStrings (SIZE_STRING, szDrives);
	
	for (a=0;a<len;a++) {
		if (szDrives[a] == 0) {

			ZeroMemory (szCurDrive, SIZE_STRING);
			strncpy (szCurDrive, szDrives+l, a-l);			
			l = a+1;

			ZeroMemory (szLetter, SIZE_NAME);
			strncpy_s (szLetter, SIZE_NAME, szCurDrive, 1);
			_strupr_s (szLetter, SIZE_NAME);

			if (strcmp (szDriveLetterUpr, szLetter) == 0) {
				return true;
			}
		}
	}

	return false;
}



bool VirtualDriveManager::GetFreeDriveLetter (char *szOutdriveletter)
{
	char szDrives[SIZE_STRING];
	ZeroMemory (szDrives, SIZE_STRING);

	char szCurDrive[SIZE_STRING];
	char szLetter[SIZE_NAME];
	char szAvaildrive[SIZE_NAME];

	int a = 0;
	int l = 0;
	int len = 0;

	bool bAvaildrives[26];

	for (a=0;a<26;a++) {
		bAvaildrives[a] = true;
	}

	len = GetLogicalDriveStrings (SIZE_STRING, szDrives);
	
	for (a=0;a<len;a++) {
		if (szDrives[a] == 0) {

			ZeroMemory (szCurDrive, SIZE_STRING);
			strncpy (szCurDrive, szDrives+l, a-l);			
			l = a+1;

			ZeroMemory (szLetter, SIZE_NAME);
			strncpy_s (szLetter, SIZE_NAME, szCurDrive, 1);

			bAvaildrives[DriveLetterToNumber(szLetter)] = false;
			OutputText ("Drive: ", szCurDrive);
		}
	}

	// Ensure drive A,  B and C are not chosen
	bAvaildrives[0] = false;
	bAvaildrives[1] = false;
	bAvaildrives[2] = false;


	for (a=0;a<26;a++) {

		if (bAvaildrives[a] == true) {
			
			ZeroMemory (szAvaildrive, SIZE_NAME);
			NumberToDriveLetter (szAvaildrive, a);

			OutputText("Available Drive: ", szAvaildrive);

			ZeroMemory (szOutdriveletter, SIZE_NAME);
			strcpy_s (szOutdriveletter, SIZE_NAME, szAvaildrive);

			return true;
		}		
	}

	return false;
}

bool VirtualDriveManager::IsDriveLetterFree (char *szDriveletter)
{
	char szDrives[SIZE_STRING];
	ZeroMemory (szDrives, SIZE_STRING);

	char szCurDrive[SIZE_STRING];
	char szLetter[SIZE_NAME];
	char szAvaildrive[SIZE_NAME];

	int a = 0;
	int l = 0;
	int len = 0;

	bool bAvaildrives[26];

	for (a=0;a<26;a++) {
		bAvaildrives[a] = true;
	}

	len = GetLogicalDriveStrings (SIZE_STRING, szDrives);
	
	for (a=0;a<len;a++) {
		if (szDrives[a] == 0) {

			ZeroMemory (szCurDrive, SIZE_STRING);
			strncpy (szCurDrive, szDrives+l, a-l);			
			l = a+1;

			ZeroMemory (szLetter, SIZE_NAME);
			strncpy_s (szLetter, SIZE_NAME, szCurDrive, 1);

			bAvaildrives[DriveLetterToNumber(szLetter)] = false;
			//OutputText ("Drive: ", szCurDrive);
		}
	}

	// Ensure drive A,  B and C are not chosen
	bAvaildrives[0] = false;
	bAvaildrives[1] = false;
	bAvaildrives[2] = false;


	for (a=0;a<26;a++) {

		if (bAvaildrives[a] == true) {
			
			ZeroMemory (szAvaildrive, SIZE_NAME);
			NumberToDriveLetter (szAvaildrive, a);

			//OutputText("Available Drive: ", szAvaildrive);

			if (strcmp (szAvaildrive, szDriveletter) == 0) {
				return true;
			}
		}		
	}

	return false;
}

int VirtualDriveManager::MountVirtualDisk (SingleDriveInfo driveinfo)
{
	if (m_bTrialmode == true) {
		SetTimer (m_hwnd, DISCONNECTTIMER, 60000, NULL);
		if (driveinfo.lDiskSizeMegs > 100) {
			MessageBox (NULL, "Thank you for trying CedeDrive, a secure virtual drive encryption solution for desktops, laptops and removable drives. For full purchase information please visit http://www.cedesoft.com.\n\nPlease note this trial version is limited to 1 virtual drive size of 100MB, and drives are disconnected after 20 minutes.", "Trial Version Information", MB_OK | MB_ICONINFORMATION);
			strcpy_s (m_szMounterrormessage, SIZE_STRING, "This trial version is limited to Virtual Drives of 100MB.");
			return 32;
		} else {
			MessageBox (NULL, "Thank you for trying CedeDrive, a secure virtual drive encryption solution for desktops, laptops and removable drives. For full purchase information please visit http://www.cedesoft.com.\n\nPlease note this trial version is limited to 1 virtual drive size of 100MB, and drives are disconnected after 20 minutes.", "Trial Version Information", MB_OK | MB_ICONINFORMATION);
		}
	}

	SingleDriveInfo drivetomount;
	drivetomount = driveinfo;

	InstructionResult result;
	if (IsServiceRunning () == false) {
		strcpy_s (m_szMounterrormessage, SIZE_STRING, "The CedeDrive Manager Service does not appear to be running. Unable to mount drive.");
		return 30;
	}

	char szRequestedDriveLetter[SIZE_NAME];
	ZeroMemory (szRequestedDriveLetter, SIZE_NAME);
	strncpy_s (szRequestedDriveLetter, SIZE_NAME, drivetomount.szDriveLetter, 1);

	char szFreeDriveLetter[SIZE_NAME];
	ZeroMemory (szFreeDriveLetter, SIZE_NAME);

	char szNewDriveLetter[SIZE_NAME];

	// Check if the drive letter requested is actually free on the system.
	if (IsDriveLetterFree(szRequestedDriveLetter) == false) {
		// If the drive letter is not free, then we need to assign another drive letter
		if (GetFreeDriveLetter (szFreeDriveLetter) == false) {
			strcpy_s (m_szMounterrormessage, SIZE_STRING, "There are no free drive letters available to mount the requested drive. Please free some drive letters.");
			return 34;
		} else {
			ZeroMemory (szNewDriveLetter, SIZE_NAME);
			strncpy_s (szNewDriveLetter, SIZE_NAME, szFreeDriveLetter, 1);
			strcat_s (szNewDriveLetter, SIZE_NAME, ":");

			ZeroMemory (drivetomount.szDriveLetter, SIZE_NAME);
			strcpy_s (drivetomount.szDriveLetter, SIZE_NAME, szNewDriveLetter);
		}
	}

	ZeroMemory (m_szMounterrormessage, SIZE_STRING);
	
	//if (drivetomount.bUsingseperatepassword == false) {
		drivetomount.bUsingseperatepassword = true;
		ZeroMemory (drivetomount.szPassword, SIZE_STRING);
		strcpy_s (drivetomount.szPassword, SIZE_STRING, m_szPassword);
	//}
	
	result = m_vdclient.RequestMountDrive (drivetomount);

	if (result.iResult == 0) {
		m_dlmounteddrives.AddItem (&drivetomount, sizeof (SingleDriveInfo), false);
	}

	strcpy_s (m_szMounterrormessage, SIZE_STRING, result.szDescription);
	return result.iResult;
}

void VirtualDriveManager::UnmountVirtualDrive (SingleDriveInfo driveinfo)
{
	InstructionResult result;
	result = m_vdclient.RequestUnmountDrive (driveinfo);

	if (result.iResult == 0) {
		RemoveFromList (&m_dlmounteddrives, driveinfo);
	}
}

void VirtualDriveManager::RemoveFromList (DynList *dlList, SingleDriveInfo driveinfo)
{
	DynList dlTemp;

	SingleDriveInfo *pinfo;

	if (dlList->GetNumItems () > 0) {

		for (int a=0;a<dlList->GetNumItems ();a++) {
			pinfo = (SingleDriveInfo *) dlList->GetItem (a);

			if (strcmp (pinfo->szPath, driveinfo.szPath) != 0) {
				dlTemp.AddItem (pinfo, sizeof (SingleDriveInfo), false);
			}
		}

		dlList->Clear ();

		for (int b=0;b<dlTemp.GetNumItems ();b++) {
			pinfo = (SingleDriveInfo *) dlTemp.GetItem (b);

			dlList->AddItem (pinfo, sizeof (SingleDriveInfo), false);
		}

		dlTemp.Clear ();
	}
}

bool VirtualDriveManager::IsDriveHostingVirtualDisk (char *szSingledriveletter)
{
	SingleDriveInfo *pmounteddrive;
	char szCurdrive[SIZE_NAME];


	for (int a=0;a<m_dlmounteddrives.GetNumItems();a++) {
		ZeroMemory (szCurdrive, SIZE_NAME);
		pmounteddrive = (SingleDriveInfo *) m_dlmounteddrives.GetItem (a);
		strncpy_s (szCurdrive, SIZE_NAME, pmounteddrive->szPath, 1);

		if (strcmp (szCurdrive, szSingledriveletter) == 0) {
			return true;
		}
	}

	return false;
}

void VirtualDriveManager::UnmountAllVirtualDrives ()
{
	InstructionResult result;
	result = m_vdclient.RequestUnmountAllDrives ();

	if (result.iResult == 0) {
		m_dlmounteddrives.Clear ();
	}
}

bool VirtualDriveManager::IsServiceRunning ()
{
	InstructionResult result;
	result = m_vdclient.RequestPing ();

	if (result.bSuccess == true) {
		return true;
	} else {
		return false;
	}
}

void VirtualDriveManager::GetMountError (char *szOutmessage)
{
	ZeroMemory (szOutmessage, SIZE_STRING);
	strcpy_s (szOutmessage, SIZE_STRING, m_szMounterrormessage);
}

unsigned long VirtualDriveManager::GetSectorMultiple (unsigned long lValue)
{
	// Takes a value and returns a value that is the nearest sector-aligned multiple
	// of the supplied value
	unsigned long currentMultiple = 1024;
	if (lValue > 0) {

		while (lValue > currentMultiple) {
			currentMultiple = currentMultiple * 2;
		}

		return currentMultiple;

	} else {
		return 0;
	}
}


DWORD WINAPI VirtualDriveManager::ThreadProc_AllocateDiskSpace (PVOID pParam)
{
	VirtualDriveManager *pwnd = (VirtualDriveManager *) pParam;

	unsigned long long userspecifiedsize = (unsigned long long) pwnd->currentAllocationInfo.lDiskSizeMegs;

	unsigned long long DiskSize = (unsigned long long) userspecifiedsize * 1024 * 1024;
	//unsigned long long DiskSize = 10485760000;
    DWORD dw;

    LARGE_INTEGER currentSize;
	LARGE_INTEGER newPointerlocation;
    //currentSize.LowPart = GetFileSize(g_hFile,(LPDWORD)&currentSize.HighPart);
    
	GetFileSizeEx (pwnd->m_hAllocateFile, &currentSize);
	
	if(currentSize.QuadPart >= DiskSize)
    {
        // already allocated
        //SetFilePointer(g_hFile,currentSize.LowPart,&currentSize.HighPart,FILE_BEGIN);
		
		SetFilePointerEx (pwnd->m_hAllocateFile, currentSize, &newPointerlocation, FILE_BEGIN);

        SetEndOfFile(pwnd->m_hAllocateFile);
		PostMessage (pwnd->m_hwnd, WM_UICOMMAND, VD_ALLOCATIONCOMPLETE, 0);
		CloseHandle (pwnd->m_hAllocateFile);
        return 0;
    }
    
	pwnd->OutputText ("Allocating disk space...");
	
	// Take another copy of the file header
	MemoryBuffer memHeader;
	memHeader.SetSize (pwnd->GetSectorMultiple (pwnd->memcurrentfileheader.GetSize ()));
	memHeader.Write (pwnd->memcurrentfileheader.GetBuffer (), 0, pwnd->memcurrentfileheader.GetSize ());
	//memHeader.SaveToFile ("D:\\Temp\\Anothertestheader.dat");

	DWORD dwHeaderwritten;
	DWORD dwFileheadersize = memHeader.GetSize ();	

	// Write the file header to the file
	if(!WriteFile(pwnd->m_hAllocateFile, memHeader.GetBuffer (),dwFileheadersize,&dwHeaderwritten,NULL))
    {            
		pwnd->OutputInt ("Error writing file header, error: ", GetLastError ());
	} else {
		pwnd->OutputText ("File header written ok.");
	}

	memHeader.Clear ();
	

    const int ChunkSize = 1024 * 1024; // 1 MB
    char* ZeroBuffer = new char[ChunkSize];

    unsigned ChunksCount = (unsigned)(DiskSize / ChunkSize);
    for(unsigned i = 0;i<ChunksCount;++i)
    {        

		ZeroMemory (pwnd->m_szProgressLabel, SIZE_STRING);
		sprintf_s (pwnd->m_szProgressLabel, SIZE_STRING, "Creating encrypted virtual drive, %i %% complete (written chunk %i of %i)",  (i * 100 / ChunksCount), i, ChunksCount);
		
		pwnd->m_allocationpercent = (i * 100 / ChunksCount);
		pwnd->m_currentvalue = i;
		pwnd->m_maxvalues = ChunksCount;

		//pwnd->OutputInt ("Allocating: ", (i * 100 / ChunksCount));
		PostMessage (pwnd->m_hwnd, WM_UICOMMAND, VD_ALLOCATIONPROGRESS, 0);

        if(!WriteFile(pwnd->m_hAllocateFile,ZeroBuffer,ChunkSize,&dw,NULL))
        {            
			pwnd->OutputText ("Error!");
        }
   }

    delete[] ZeroBuffer;    
	pwnd->OutputText ("Allocation completed.");
	CloseHandle (pwnd->m_hAllocateFile);

	// Post a message to the main UI thread informing it that the Allocation has ended
	PostMessage (pwnd->m_hwnd, WM_UICOMMAND, VD_ALLOCATIONCOMPLETE, 0);

	return 0;
}

void VirtualDriveManager::DoThread_AllocateDiskSpace ()
{
	// Spawns the thread that actually does the disk allocating as this
	// will take some time. Especially on large virtual disks.
	HANDLE hThread;
	DWORD dwThreadID;

	hThread = CreateThread (NULL, 0, ThreadProc_AllocateDiskSpace, (void *) this, 0, &dwThreadID);
}

bool VirtualDriveManager::FileExists (char *FileName)
{
	// Check if the file exists		
	struct _finddata_t c_file;
	long hFile;

	// Build the list of files in the source path
	if( (hFile = _findfirst(FileName, &c_file )) == -1L ) {
		return false;
	} else {
		return true;
	}

	return false;
}

bool VirtualDriveManager::IsDriveMounted (SingleDriveInfo driveinfo)
{
	for (int i=0;i<MAX_DRIVES;i++) {
		if (m_bResourceBusy[i] == true) {
			if (strcmp (driveinfo.szPath, m_Drivelist[i].szPath) == 0) {
				return true;
			}
		}
	}

	return false;
}

int VirtualDriveManager::GetFreeDriveResource ()
{
	for (int i=0;i<MAX_DRIVES;i++) 
	{
		if (m_bResourceBusy[i] == false) {
			return i;
		}
	}

	// No free drive resource available - this must mean all the virtual drives
	// are currently in use.
	return -1;
}

unsigned long VirtualDriveManager::GetVirtualDiskSizeMegs (char *szPath)
{
	HANDLE hFile;
	hFile = CreateFile(szPath, GENERIC_READ | GENERIC_WRITE,FILE_SHARE_READ,0,OPEN_ALWAYS,FILE_FLAG_NO_BUFFERING | FILE_FLAG_RANDOM_ACCESS,0);

    if(hFile == INVALID_HANDLE_VALUE)
    {
		OutputText ("ERROR: can not create / open disk image file for getting file size");
        return 0;
    }

	LARGE_INTEGER currentSize;
	LARGE_INTEGER newPointerlocation;
    //currentSize.LowPart = GetFileSize(g_hFile,(LPDWORD)&currentSize.HighPart);
    
	GetFileSizeEx (hFile, &currentSize);

	int chunksize = 1024 * 1024;

	unsigned long megs = (currentSize.QuadPart / chunksize);

	CloseHandle (hFile);

	return megs;
}

void VirtualDriveManager::AllocateVirtualDisk (SingleDriveInfo driveinfo, MemoryBuffer *memFileheader)
{
	OutputText ("Allocating drive...");

	m_hAllocateFile = CreateFile(driveinfo.szPath, GENERIC_READ | GENERIC_WRITE,FILE_SHARE_READ,0,
        OPEN_ALWAYS,FILE_FLAG_NO_BUFFERING | FILE_FLAG_RANDOM_ACCESS,0);

    if(m_hAllocateFile == INVALID_HANDLE_VALUE)
    {
		OutputText ("ERROR: can not create / open disk image file for allocation");
        return;
    }

	currentAllocationInfo = driveinfo;

	// Copy the file header
	memcurrentfileheader.Clear ();
	memcurrentfileheader.SetSize (memFileheader->GetSize ());
	memcurrentfileheader.Write (memFileheader->GetBuffer (), 0, memFileheader->GetSize ());

    // allocate disk space
    //AllocateDiskSpace();
	DoThread_AllocateDiskSpace();
}

bool VirtualDriveManager::AllocateStandaloneVirtualDisk (SingleDriveInfo driveinfo, MemoryBuffer *memFileheader)
{


	OutputText ("Allocating drive...");

	m_hAllocateFile = CreateFile(driveinfo.szPath, GENERIC_READ | GENERIC_WRITE,FILE_SHARE_READ,0,
        OPEN_ALWAYS,FILE_FLAG_NO_BUFFERING | FILE_FLAG_RANDOM_ACCESS,0);

    if(m_hAllocateFile == INVALID_HANDLE_VALUE)
    {
		OutputText ("ERROR: can not create / open disk image file for allocation");
        return false;
    }

	unsigned long long userspecifiedsize = (unsigned long long) driveinfo.lDiskSizeMegs;

	unsigned long long DiskSize = (unsigned long long) userspecifiedsize * 1024 * 1024;
	//unsigned long long DiskSize = 10485760000;
    DWORD dw;

    LARGE_INTEGER currentSize;
	LARGE_INTEGER newPointerlocation;
    //currentSize.LowPart = GetFileSize(g_hFile,(LPDWORD)&currentSize.HighPart);
    
	GetFileSizeEx (m_hAllocateFile, &currentSize);
	
	if(currentSize.QuadPart >= DiskSize)
    {
        // already allocated
        //SetFilePointer(g_hFile,currentSize.LowPart,&currentSize.HighPart,FILE_BEGIN);
		
		SetFilePointerEx (m_hAllocateFile, currentSize, &newPointerlocation, FILE_BEGIN);

        SetEndOfFile(m_hAllocateFile);
		//PostMessage (m_hwnd, WM_UICOMMAND, VD_ALLOCATIONCOMPLETE, 0);
		CloseHandle (m_hAllocateFile);
        return true;
    }
    
	OutputText ("Allocating disk space...");

	// Take another copy of the file header
	MemoryBuffer memHeader;
	memHeader.SetSize (GetSectorMultiple (memFileheader->GetSize ()));
	memHeader.Write (memFileheader->GetBuffer (), 0, memFileheader->GetSize ());
	//memHeader.SaveToFile ("D:\\Temp\\Anothertestheader.dat");

	DWORD dwHeaderwritten;
	DWORD dwFileheadersize = memHeader.GetSize ();	

	// Write the file header to the file
	if(!WriteFile(m_hAllocateFile, memHeader.GetBuffer (),dwFileheadersize,&dwHeaderwritten,NULL))
    {            
		OutputInt ("Error writing standalone file header, error: ", GetLastError ());
	} else {
		OutputText ("Standalone File header written ok.");
	}

	memHeader.Clear ();

    const int ChunkSize = 1024 * 1024; // 1 MB
    char* ZeroBuffer = new char[ChunkSize];

    unsigned ChunksCount = (unsigned)(DiskSize / ChunkSize);
    for(unsigned i = 0;i<ChunksCount;++i)
    {        

		//ZeroMemory (pwnd->m_szProgressLabel, SIZE_STRING);
		//sprintf_s (pwnd->m_szProgressLabel, SIZE_STRING, "Creating encrypted virtual drive, %i %% complete (written chunk %i of %i)",  (i * 100 / ChunksCount), i, ChunksCount);
		
		m_allocationpercent = (i * 100 / ChunksCount);
		m_currentvalue = i;
		m_maxvalues = ChunksCount;

		//pwnd->OutputInt ("Allocating: ", (i * 100 / ChunksCount));
		//PostMessage (pwnd->m_hwnd, WM_UICOMMAND, VD_ALLOCATIONPROGRESS, 0);
		PostMessage (m_hwnd, WM_UICOMMAND, PROGRESS_CREATE, m_allocationpercent);

        if(!WriteFile(m_hAllocateFile,ZeroBuffer,ChunkSize,&dw,NULL))
        {            
			OutputText ("Error!");
        }
   }

    delete[] ZeroBuffer;    
	OutputText ("Allocation completed.");
	CloseHandle (m_hAllocateFile);

	// Post a message to the main UI thread informing it that the Allocation has ended
	//PostMessage (pwnd->m_hwnd, WM_UICOMMAND, VD_ALLOCATIONCOMPLETE, 0);

	return true;
}

