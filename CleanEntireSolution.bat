echo off

echo Cleaning...

erase Installer\CedeDriveSetup-32bit.exe
erase Installer\CedeDriveSetup-64bit.exe
erase Installer\32bit\CedeDrive.exe
erase Installer\32bit\CedeDriveSetup-32bit.exe
erase Installer\32bit\ServiceDeployment.exe
erase Installer\64bit\CedeDrive.exe
erase Installer\64bit\CedeDriveSetup-64bit.exe
erase Installer\64bit\ServiceDeployment.exe
erase Installer\CedeDriveLaunch.exe
erase Installer\CedeDriveSetup-32bit.msi
erase Installer\CedeDriveSetup-64bit.msi


erase Installer\32bit\CedeDriveInstaller32\CedeDriveInstaller\ServiceDeployment.exe
erase Installer\32bit\CedeDriveInstaller32\CedeDriveInstaller\CedeDrive.exe
erase Installer\32bit\CedeDriveInstaller32\CedeDriveInstaller\CedeDriveService.exe
erase Installer\32bit\CedeDriveInstaller32\CedeDriveInstaller\CedeDriveUserGuide.pdf
erase Installer\32bit\CedeDriveInstaller32\CedeDriveInstaller\VDSDKDll.dll
erase Installer\32bit\CedeDriveInstaller32\CedeDriveInstaller\VDCore.dll
erase Installer\32bit\CedeDriveInstaller32\CedeDriveInstaller\bin\Release\*.msi

erase Installer\64bit\CedeDriveInstaller64\CedeDriveInstaller64\ServiceDeployment.exe
erase Installer\64bit\CedeDriveInstaller64\CedeDriveInstaller64\CedeDrive.exe
erase Installer\64bit\CedeDriveInstaller64\CedeDriveInstaller64\CedeDriveService.exe
erase Installer\64bit\CedeDriveInstaller64\CedeDriveInstaller64\CedeDriveUserGuide.pdf
erase Installer\64bit\CedeDriveInstaller64\CedeDriveInstaller64\VDSDKDll.dll
erase Installer\64bit\CedeDriveInstaller64\CedeDriveInstaller64\VDCore.dll
erase Installer\64bit\CedeDriveInstaller64\CedeDriveInstaller64\bin\Release\*.msi


erase Installer\32bit\WiXProject\WixProject1\ServiceDeployment.exe
erase Installer\32bit\WiXProject\WixProject1\CedeDrive.exe
erase Installer\32bit\WiXProject\WixProject1\CedeDriveService.exe
erase Installer\32bit\WiXProject\WixProject1\CedeDriveUserGuide.pdf
erase Installer\32bit\WiXProject\WixProject1\VDSDKDll.dll
erase Installer\32bit\WiXProject\WixProject1\VDCore.dll
erase Installer\32bit\WiXProject\WixProject1\bin\Release\*.msi


erase Installer\64bit\WiXProject\WixProject1\ServiceDeployment.exe
erase Installer\64bit\WiXProject\WixProject1\CedeDrive.exe
erase Installer\64bit\WiXProject\WixProject1\CedeDriveService.exe
erase Installer\64bit\WiXProject\WixProject1\CedeDriveUserGuide.pdf
erase Installer\64bit\WiXProject\WixProject1\VDSDKDll.dll
erase Installer\64bit\WiXProject\WixProject1\VDCore.dll


erase "CedeDrive\CedeDrive\CedeDrive2010\Release\*.res"
erase "CedeDrive\CedeDrive\CedeDrive2010\Release\*.obj"
erase "CedeDrive\CedeDrive\CedeDrive2010\Release\*.exe"

erase "CedeDrive\CedeDrive\CedeDrive2010\x64\Release\*.res"
erase "CedeDrive\CedeDrive\CedeDrive2010\x64\Release\*.obj"
erase "CedeDrive\CedeDrive\CedeDrive2010\x64\Release\*.exe"

erase "CedeDrive\CedeDrive\CedeDrive2010\Release (For Launcher)\*.res"
erase "CedeDrive\CedeDrive\CedeDrive2010\Release (For Launcher)\*.obj"
erase "CedeDrive\CedeDrive\CedeDrive2010\Release (For Launcher)\*.exe"

erase "CedeDrive\CedeDrive\CedeDrive2010\x64\Release (For Launcher)\*.res"
erase "CedeDrive\CedeDrive\CedeDrive2010\x64\Release (For Launcher)\*.obj"
erase "CedeDrive\CedeDrive\CedeDrive2010\x64\Release (For Launcher)\*.exe"

erase "CedeDrive\CedeDrive\CedeDrive2010\Debug\*.res"
erase "CedeDrive\CedeDrive\CedeDrive2010\Debug\*.obj"
erase "CedeDrive\CedeDrive\CedeDrive2010\Debug\*.exe"

erase "CedeDrive\CedeDrive\CedeDrive2010\x64\Debug\*.res"
erase "CedeDrive\CedeDrive\CedeDrive2010\x64\Debug\*.obj"
erase "CedeDrive\CedeDrive\CedeDrive2010\x64\Debug\*.exe"

erase "CedeDrive\CedeDrive\Release\*.res"
erase "CedeDrive\CedeDrive\Release\*.obj"
erase "CedeDrive\CedeDrive\Release\*.exe"

erase "CedeDrive\CedeDrive\x64\Release\*.res"
erase "CedeDrive\CedeDrive\x64\Release\*.obj"
erase "CedeDrive\CedeDrive\x64\Release\*.exe"

erase "CedeDrive\CedeDrive\x64\Release (For Dev)\*.res"
erase "CedeDrive\CedeDrive\x64\Release (For Dev)\*.obj"
erase "CedeDrive\CedeDrive\x64\Release (For Dev)\*.exe"

erase "CedeDrive\CedeDrive\x64\Release (For Launcher)\*.res"
erase "CedeDrive\CedeDrive\x64\Release (For Launcher)\*.obj"
erase "CedeDrive\CedeDrive\x64\Release (For Launcher)\*.exe"

erase "CedeDrive\CedeDrive\x64\Release (Install-Dev)\*.res"
erase "CedeDrive\CedeDrive\x64\Release (Install-Dev)\*.obj"
erase "CedeDrive\CedeDrive\x64\Release (Install-Dev)\*.exe"

erase "CedeDrive\CedeDrive\Debug\*.res"
erase "CedeDrive\CedeDrive\Debug\*.obj"
erase "CedeDrive\CedeDrive\Debug\*.exe"

erase "CedeDrive\CedeDrive\Release (For Launcher)\*.res"
erase "CedeDrive\CedeDrive\Release (For Launcher)\*.obj"
erase "CedeDrive\CedeDrive\Release (For Launcher)\*.exe"

erase "CedeDriveService\CedeDriveService\Release\*.res"
erase "CedeDriveService\CedeDriveService\Release\*.obj"
erase "CedeDriveService\CedeDriveService\Release\*.exe"

erase "CedeDriveService\CedeDriveService\x64\Release\*.res"
erase "CedeDriveService\CedeDriveService\x64\Release\*.obj"
erase "CedeDriveService\CedeDriveService\x64\Release\*.exe"

erase "CedeDriveService\CedeDriveService\VS2010\CedeDriveService\Release\*.res"
erase "CedeDriveService\CedeDriveService\VS2010\CedeDriveService\Release\*.obj"
erase "CedeDriveService\CedeDriveService\VS2010\CedeDriveService\Release\*.exe"

erase "CedeDriveService\CedeDriveService\VS2010\CedeDriveService\x64\Release\*.res"
erase "CedeDriveService\CedeDriveService\VS2010\CedeDriveService\x64\Release\*.obj"
erase "CedeDriveService\CedeDriveService\VS2010\CedeDriveService\x64\Release\*.exe"

erase "PortableLauncher\CedeDriveLaunch\Release\*.obj"
erase "PortableLauncher\CedeDriveLaunch\Release\*.res"
erase "PortableLauncher\CedeDriveLaunch\Release\*.exe"

erase "PortableLauncher\CedeDriveLaunch\VS2010\CedeDriveLaunch\Release\*.obj"
erase "PortableLauncher\CedeDriveLaunch\VS2010\CedeDriveLaunch\Release\*.res"
erase "PortableLauncher\CedeDriveLaunch\VS2010\CedeDriveLaunch\Release\*.exe"

erase "ServiceDeployment\ServiceDeployment\Release\*.obj"
erase "ServiceDeployment\ServiceDeployment\Release\*.res"
erase "ServiceDeployment\ServiceDeployment\Release\*.exe"

erase "ServiceDeployment\ServiceDeployment\VS2010\ServiceDeployment\Release\*.obj"
erase "ServiceDeployment\ServiceDeployment\VS2010\ServiceDeployment\Release\*.res"
erase "ServiceDeployment\ServiceDeployment\VS2010\ServiceDeployment\Release\*.exe"
