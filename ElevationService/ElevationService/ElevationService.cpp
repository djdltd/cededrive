#include <windows.h>
#include <stdio.h>
#include "MemoryBuffer.h"

#define ID_TIMER 1000
#define SIZE_STRING	1024

const char g_szClassName[] = "myCedeDriveElevationServiceWindow";
static char g_szServiceDesc [] = "Elevates the CedeDrive process to allow the successful mounting of encrypted drives when a user is logged on without Administrator privileges.";

SERVICE_STATUS MyServiceStatus; 
SERVICE_STATUS_HANDLE MyServiceStatusHandle; 
HINSTANCE g_hInstance;
HANDLE m_hThread;

// Prototypes
void  WINAPI MyServiceStart (DWORD argc, LPTSTR *argv); 
void  WINAPI MyServiceCtrlHandler (DWORD opcode); 
DWORD MyServiceInitialization (DWORD argc, LPTSTR *argv, DWORD *specificError); 
VOID SvcDebugOut(LPSTR String, DWORD Status);
int ServiceMain ();
VOID InstallService (LPCTSTR lpszBinaryPathName);
VOID UninstallService ();

MemoryBuffer g_memComms;

// Implementation


DWORD WINAPI ThreadProc (PVOID pParam)
{
	MemoryBuffer memFile;
	HANDLE hIPC = g_memComms.GetSharedMemoryHandle ();

	int i = 0;

	for (;;) {
		WaitForSingleObject (hIPC, INFINITE);
		i++;
		char szText[SIZE_STRING];
		ZeroMemory (szText, SIZE_STRING);
		sprintf_s (szText, SIZE_STRING, "Cycle %i", i);	
		
		memFile.SetSize (strlen (szText));
		memFile.Write (szText, 0, strlen (szText));

		memFile.SaveToFile ("C:\\Temp\\CS\\ServiceFile.txt");

		Sleep (2000);

	}
	return 0;
}

void StartThread ()
{
	// Create the separate thread which converts the drive
	DWORD dwThreadID;
	m_hThread = CreateThread (NULL, 0, ThreadProc, NULL, 0, &dwThreadID);
}


bool LaunchApplication ()
{
	char szExepath[SIZE_STRING];
	ZeroMemory (szExepath, SIZE_STRING);
	strcpy_s (szExepath, SIZE_STRING, "C:\\Windows\\Notepad.exe");
	
    STARTUPINFO si;
    PROCESS_INFORMATION pi;

    ZeroMemory( &si, sizeof(si) );
    si.cb = sizeof(si);
    ZeroMemory( &pi, sizeof(pi) );

	if( !CreateProcess(NULL,   // No module name (use command line)
		szExepath,        // Command line
        NULL,           // Process handle not inheritable
        NULL,           // Thread handle not inheritable
        FALSE,          // Set handle inheritance to FALSE
        0,              // No creation flags
        NULL,           // Use parent's environment block
        NULL,           // Use parent's starting directory 
        &si,            // Pointer to STARTUPINFO structure
        &pi )           // Pointer to PROCESS_INFORMATION structure
    ) 
    {
		return false;
    }

	return true;
}


VOID WINAPI MyServiceCtrlHandler (DWORD Opcode)  { 
	DWORD status; 
 
	switch(Opcode) 
	{ 
		case SERVICE_CONTROL_PAUSE: 
		// Do whatever it takes to pause here. 
		MyServiceStatus.dwCurrentState = SERVICE_PAUSED; 
		break; 
 
		case SERVICE_CONTROL_CONTINUE: 
		// Do whatever it takes to continue here. 
		MyServiceStatus.dwCurrentState = SERVICE_RUNNING; 
		break; 
 
		case SERVICE_CONTROL_STOP: 
		// Do whatever it takes to stop here. 
		MyServiceStatus.dwWin32ExitCode = 0; 
		MyServiceStatus.dwCurrentState  = SERVICE_STOPPED; 
		MyServiceStatus.dwCheckPoint    = 0; 
		MyServiceStatus.dwWaitHint      = 0; 
 
		if (!SetServiceStatus (MyServiceStatusHandle,&MyServiceStatus)) { 
			status = GetLastError(); 
			SvcDebugOut(" [MY_SERVICE] SetServiceStatus error %ld\n",status); 
		} 
		SvcDebugOut(" [MY_SERVICE] Leaving MyService \n",0);
		return; 
 
		case SERVICE_CONTROL_INTERROGATE: 
		// Fall through to send current status. 
		break; 
		default: 
		SvcDebugOut(" [MY_SERVICE] Unrecognized opcode %ld\n", Opcode); 
	} 
 
	// Send current status. 
	if (!SetServiceStatus (MyServiceStatusHandle,  &MyServiceStatus)) { 
		status = GetLastError(); 
		SvcDebugOut(" [MY_SERVICE] SetServiceStatus error %ld\n",status); 
	} 
	return; 
} 

void WINAPI MyServiceStart (DWORD argc, LPTSTR *argv) { 
	DWORD status; 
	DWORD specificError; 
 
	MyServiceStatus.dwServiceType = SERVICE_WIN32; 
	MyServiceStatus.dwCurrentState = SERVICE_START_PENDING; 
	MyServiceStatus.dwControlsAccepted = SERVICE_ACCEPT_STOP | SERVICE_ACCEPT_PAUSE_CONTINUE; 
	MyServiceStatus.dwWin32ExitCode = 0; 
	MyServiceStatus.dwServiceSpecificExitCode = 0; 
	MyServiceStatus.dwCheckPoint = 0; 
	MyServiceStatus.dwWaitHint = 0; 
 
	MyServiceStatusHandle = RegisterServiceCtrlHandler("CedeDriveService", MyServiceCtrlHandler); 
 
	if (MyServiceStatusHandle == (SERVICE_STATUS_HANDLE)0) { 
		SvcDebugOut(" [MY_SERVICE] RegisterServiceCtrlHandler failed %d\n", GetLastError()); 
		return; 
	} 
 
	// Initialization code goes here. 
	status = MyServiceInitialization(argc,argv, &specificError); 
 
	// Handle error condition 
	if (status != NO_ERROR) { 
		MyServiceStatus.dwCurrentState = SERVICE_STOPPED; 
		MyServiceStatus.dwCheckPoint = 0; 
		MyServiceStatus.dwWaitHint = 0; 
		MyServiceStatus.dwWin32ExitCode = status; 
		MyServiceStatus.dwServiceSpecificExitCode = specificError; 
		SetServiceStatus (MyServiceStatusHandle, &MyServiceStatus); 
		return;
	}
 
	// Initialization complete - report running status. 
	MyServiceStatus.dwCurrentState = SERVICE_RUNNING;
	MyServiceStatus.dwCheckPoint = 0; 
	MyServiceStatus.dwWaitHint = 0; 
 
	if (!SetServiceStatus (MyServiceStatusHandle, &MyServiceStatus)) { 
		status = GetLastError(); 
		SvcDebugOut(" [MY_SERVICE] SetServiceStatus error %ld\n",status); 
	} 
 
	// This is where the service does its work. 
	ServiceMain ();
	SvcDebugOut(" [MY_SERVICE] Returning the Main Thread \n",0); 
	return; 
} 
 
// Stub initialization function. 
DWORD MyServiceInitialization(DWORD   argc, LPTSTR  *argv, DWORD *specificError) { 
    argv;
    argc; 
    specificError;
    return(0);
}
 
// The Window Procedure
LRESULT CALLBACK WndProc (HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
		case WM_CREATE:
		{
			//LaunchApplication ();
			SetTimer (hwnd, ID_TIMER, 400, NULL);
		}
		break;
		case WM_TIMER:
		{
			if (wParam == ID_TIMER) {
				Beep (500, 30);
			}
		}
		break;
		case WM_CLOSE:
			DestroyWindow (hwnd);
			break;
		case WM_DESTROY:
			PostQuitMessage (0);
			break;
		default:
			return DefWindowProc (hwnd, msg, wParam, lParam);
	}
	return 0;
}

int ServiceMain ()
{
	/*
	Beep (1000, 400);
	WNDCLASSEX wc;
	HWND hwnd;
	MSG Msg;

	// Register the window class
	wc.cbSize		= sizeof (WNDCLASSEX);
	wc.style		= 0;
	wc.lpfnWndProc	= WndProc;
	wc.cbClsExtra		= 0;
	wc.cbWndExtra		= 0;
	wc.hInstance		= g_hInstance;
	wc.hIcon		= LoadIcon (NULL, IDI_APPLICATION);
	wc.hCursor		= LoadCursor (NULL, IDC_ARROW);
	wc.hbrBackground	= (HBRUSH) (COLOR_WINDOW);
	wc.lpszMenuName 	= NULL;
	wc.lpszClassName 	= g_szClassName;
	wc.hIconSm		= LoadIcon (NULL, IDI_APPLICATION);

	if (!RegisterClassEx (&wc))
	{
		MessageBox (NULL, "Window Registration Failed!", "Error", MB_ICONEXCLAMATION | MB_OK);
		return 0;
	}

	// Create the Window

	hwnd = CreateWindowEx (WS_EX_WINDOWEDGE, g_szClassName, "CedeDrive Elevation Service Window", WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU, CW_USEDEFAULT, CW_USEDEFAULT, 400, 200, NULL, NULL, g_hInstance, NULL);

	if (hwnd == NULL)
	{
		MessageBox (NULL, "Window Creation Failed!", "Error!", MB_ICONEXCLAMATION | MB_OK);
		return 0;
	}

	//ShowWindow (hwnd, SW_SHOW);
	UpdateWindow (hwnd);

	// The Message Loop
	while (GetMessage (&Msg, NULL, 0, 0) > 0)
	{
		TranslateMessage (&Msg);
		DispatchMessage (&Msg);
	}

	return Msg.wParam;
	*/
	g_memComms.StartSharing ("CedeDriveServiceIPC", 1024000);

	StartThread ();

	return 0;
}

int WINAPI WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	char szCommand[SIZE_STRING];
	ZeroMemory (szCommand, SIZE_STRING);
	strncpy_s (szCommand, SIZE_STRING, lpCmdLine, 2);

	bool bUsingcommand = false;
	if (strcmp (szCommand, "/i") == 0) {
		// Install the service
		bUsingcommand = true;
		char szModulefilename[SIZE_STRING];
		ZeroMemory (szModulefilename, SIZE_STRING);
		GetModuleFileName(NULL, szModulefilename, SIZE_STRING);
		//strcat_s (szModulefilename, SIZE_STRING, " /s");

		InstallService (szModulefilename);
	}

	if (strcmp (szCommand, "/s") == 0) {
		// Start the service
		bUsingcommand = true;

	}

	if (strcmp (szCommand, "/d") == 0) {
		// Delete the service
		bUsingcommand = true;
		UninstallService ();
	}

	if (strcmp (szCommand, "/r") == 0) {
		// Run as application
		bUsingcommand = true;
		ServiceMain();
	}

	
	SERVICE_TABLE_ENTRY   DispatchTable[] = {{ TEXT("CedeDriveService"), MyServiceStart}, {NULL,NULL}}; 
 
	if (bUsingcommand == false) {
		Beep (2000, 400);
		if (!StartServiceCtrlDispatcher( DispatchTable))  { 
			SvcDebugOut(" [MY_SERVICE] StartServiceCtrlDispatcher error = %d\n", GetLastError()); 
		}
	}
}

VOID InstallService (LPCTSTR lpszBinaryPathName) {
	
	// The below is not supported in windows NT - we have to take this out.
	SERVICE_DESCRIPTION scDesc = {g_szServiceDesc};
	
	SC_HANDLE schSCManager = OpenSCManager(NULL, NULL,  SC_MANAGER_ALL_ACCESS);
	if (schSCManager == NULL) {
		MessageBox (NULL, "Could not open Service Manager.", "Error", MB_ICONEXCLAMATION | MB_OK);
		return;
	}

	SC_HANDLE schService = CreateService(
		schSCManager,              // SCManager database 
		"CedeDriveElevationService",        // name of service 
		"CedeDrive Elevation Service",           // service name to display 
		SERVICE_ALL_ACCESS,        // desired access 
		SERVICE_WIN32_OWN_PROCESS, // service type 
		SERVICE_AUTO_START,      // start type 
		SERVICE_ERROR_NORMAL,      // error control type 
		lpszBinaryPathName,        // service's binary 
		NULL,                      // no load ordering group 
		NULL,                      // no tag identifier 
		NULL,                      // no dependencies 
		NULL,                      // LocalSystem account 
		NULL);                     // no password 
 
	if (schService == NULL) {
		MessageBox (NULL, "Could not install Service.", "Error", MB_ICONEXCLAMATION | MB_OK);
	} else {
		if (ChangeServiceConfig2 (schService, SERVICE_CONFIG_DESCRIPTION, &scDesc) == 0) {
		//	MessageBox (NULL, "Could not set Description.", "Error", MB_OK | MB_ICONEXCLAMATION); // Not Supported on Windows NT.
		}
		//MessageBox (NULL, "COE Desktop Alert Service Installed Successfully.", "Info", MB_ICONINFORMATION | MB_OK);
	}
 
	CloseServiceHandle(schService); 
}

VOID UninstallService () {

	SC_HANDLE schSCManager = OpenSCManager(NULL, NULL,  SC_MANAGER_ALL_ACCESS);
	if (schSCManager == NULL) {
		MessageBox (NULL, "Could not open Service Manager.", "Error", MB_ICONEXCLAMATION | MB_OK);
		return;
	}

	SC_HANDLE schService = OpenService (schSCManager, "CedeDriveElevationService", STANDARD_RIGHTS_REQUIRED);
	
	if (schService == NULL) {
		MessageBox (NULL, "Could not open Service. (Possibly Service Not Found).", "Error", MB_ICONEXCLAMATION | MB_OK);
		return;
	}
	
	if (DeleteService (schService) == 0) {
		MessageBox (NULL, "Could not uninstall Service.", "Error", MB_ICONEXCLAMATION | MB_OK);
	} else {
		//MessageBox (NULL, "COE Desktop Alert Service Removed Successfully.", "Info", MB_ICONINFORMATION | MB_OK);
	}
	
	CloseServiceHandle(schService); 
	CloseServiceHandle(schSCManager); 
}

VOID SvcDebugOut(LPSTR String, DWORD Status) { 
	CHAR  Buffer[1024]; 
	if (strlen(String) < 1000) { 
		sprintf(Buffer, String, Status); 
		OutputDebugStringA(Buffer); 
	} 
}

