#include "DeploymentHandler.h"

DeploymentHandler::DeploymentHandler ()
{
}

DeploymentHandler::~DeploymentHandler ()
{
}

void DeploymentHandler::GetServicePath (char *szOutpath)
{
	strcpy_s (szOutpath, SIZE_STRING, m_szServicepath);
}

bool DeploymentHandler::LaunchServiceDeployment ()
{	
    STARTUPINFO si;
    PROCESS_INFORMATION pi;

    ZeroMemory( &si, sizeof(si) );
    si.cb = sizeof(si);
    ZeroMemory( &pi, sizeof(pi) );
	
	if( !CreateProcess(NULL,   // No module name (use command line)
		m_szServicepath,        // Command line
        NULL,           // Process handle not inheritable
        NULL,           // Thread handle not inheritable
        FALSE,          // Set handle inheritance to FALSE
        0,              // No creation flags
        NULL,           // Use parent's environment block
        NULL,           // Use parent's starting directory 
        &si,            // Pointer to STARTUPINFO structure
        &pi )           // Pointer to PROCESS_INFORMATION structure
    ) 
    {
		return false;
    }

	return true;
}

bool DeploymentHandler::LaunchApplication ()
{
	char szExepath[SIZE_STRING];
	ZeroMemory (szExepath, SIZE_STRING);
	
    STARTUPINFO si;
    PROCESS_INFORMATION pi;

    ZeroMemory( &si, sizeof(si) );
    si.cb = sizeof(si);
    ZeroMemory( &pi, sizeof(pi) );

	strcpy_s (szExepath, SIZE_STRING, m_szExepath32);
	
	if (m_bDiagnosticsmode == true) {
		strcat_s (szExepath, SIZE_STRING, " /diag");
	}

	if( !CreateProcess(NULL,   // No module name (use command line)
		szExepath,        // Command line
        NULL,           // Process handle not inheritable
        NULL,           // Thread handle not inheritable
        FALSE,          // Set handle inheritance to FALSE
        0,              // No creation flags
        NULL,           // Use parent's environment block
        NULL,           // Use parent's starting directory 
        &si,            // Pointer to STARTUPINFO structure
        &pi )           // Pointer to PROCESS_INFORMATION structure
    ) 
    {
		return false;
    }

	return true;
}

bool DeploymentHandler::PrepareApplicationPath (int ostype)
{
	int iRes = 0;
	char szAppData[SIZE_STRING];
	char szCompanyAppData[SIZE_STRING];
	char szProgramAppData[SIZE_STRING];
	char szTempScriptsDir[SIZE_STRING];
	char szTimedir[SIZE_STRING];

	ZeroMemory (szAppData, SIZE_STRING);
	ZeroMemory (szCompanyAppData, SIZE_STRING);
	ZeroMemory (szProgramAppData, SIZE_STRING);
	ZeroMemory (szTempScriptsDir, SIZE_STRING);
	ZeroMemory (szTimedir, SIZE_STRING);

	if (GetEnvironmentVariable ("APPDATA", szAppData, SIZE_STRING) == 0) {
		if (GetEnvironmentVariable ("TEMP", szAppData, SIZE_STRING) == 0) {
			return false;
		}
	}

	strcpy_s (szCompanyAppData, SIZE_STRING, szAppData);
	strcat_s (szCompanyAppData, SIZE_STRING, "\\CedeSoft");
	_mkdir (szCompanyAppData);

	strcpy_s (szProgramAppData, SIZE_STRING, szCompanyAppData);
	strcat_s (szProgramAppData, SIZE_STRING, "\\CedeDrive");
	_mkdir (szProgramAppData);

	strcpy_s (szTempScriptsDir, SIZE_STRING, szProgramAppData);
	strcat_s (szTempScriptsDir, SIZE_STRING, "\\Application");
	_mkdir (szTempScriptsDir);

	strcat_s (szTempScriptsDir, SIZE_STRING, "\\");

	if (ostype == OS32BIT) {
		strcat_s (szTempScriptsDir, SIZE_STRING, "32-bit");	
	}

	if (ostype == OS64BIT) {
		strcat_s (szTempScriptsDir, SIZE_STRING, "64-bit");
	}

	_mkdir (szTempScriptsDir);

	// Set our member variable which is used by the other functions
	ZeroMemory (m_szApplicationpath, SIZE_STRING);
	strcpy_s (m_szApplicationpath, SIZE_STRING, szTempScriptsDir);
	
	return true;
}


bool DeploymentHandler::DeployApplication ()
{
	MemoryBuffer mem;
	char szExepath[SIZE_STRING];
	char szServicepath[SIZE_STRING];

	ZeroMemory (szExepath, SIZE_STRING);
	ZeroMemory (szServicepath, SIZE_STRING);
	ZeroMemory (m_szServicepath, SIZE_STRING);


	// If this is a 32 bit OS
	if (PrepareApplicationPath (OS32BIT) == false) {
		SetLastErr ("Unable to prepare 32bit application path");
		return false;
	}

	strcpy_s (szExepath, SIZE_STRING, m_szApplicationpath);
	strcat_s (szExepath, SIZE_STRING, "\\CedeDrive32.exe");

	ZeroMemory (m_szExepath32, SIZE_STRING);
	strcpy_s (m_szExepath32, SIZE_STRING, szExepath);

	strcpy_s (szServicepath, SIZE_STRING, m_szApplicationpath);
	strcat_s (szServicepath, SIZE_STRING, "\\ServiceDeployment.exe");
	strcpy_s (m_szServicepath, SIZE_STRING, szServicepath);

	if (mem.ReadFromResource (MAKEINTRESOURCE(BIN_CEDEDRIVE32)) != 0) {
		SetLastErr ("Unable to read 32bit CedeDrive.exe resource");
		return false;
	}

	if (mem.SaveToFile (szExepath) == false) {
		SetLastErr ("Unable to save 32bit CedeDrive.exe resource");
		return false;
	}

	mem.Clear ();

	if (mem.ReadFromResource (MAKEINTRESOURCE(BIN_SERVICEDEPLOYMENT)) != 0) {
		SetLastErr ("Unable to read 32bit ServiceDeployment.exe resource");
		return false;
	}

	if (mem.SaveToFile (szServicepath) == false) {
		SetLastErr ("Unable to save 32bit ServiceDeployment.exe resource");
		return false;
	}

	mem.Clear ();

	return true;
	
}

void DeploymentHandler::SetLastErr (char *szErrortext)
{
	ZeroMemory (m_szLasterror, SIZE_STRING);
	strcpy_s (m_szLasterror, SIZE_STRING, szErrortext);
}