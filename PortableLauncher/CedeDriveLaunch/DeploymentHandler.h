#pragma once
#include <windows.h>
#include <malloc.h>
#include <stdio.h>
#include <io.h>
#include <direct.h>
#include "resource.h"
#include "MemoryBuffer.h"


#define SIZE_STRING		1024

class DeploymentHandler {
	
	public:
		DeploymentHandler ();
		~DeploymentHandler ();

		bool LaunchApplication ();
		bool LaunchServiceDeployment ();
		bool PrepareApplicationPath (int ostype);
		void GetServicePath (char *szOutpath);
		bool DeployApplication ();

		bool m_bwow64;
		char m_szLasterror[SIZE_STRING];

		bool m_bDiagnosticsmode;
	private:
		char m_szApplicationpath[SIZE_STRING];
		char m_szServicepath[SIZE_STRING];
		char m_szExepath64[SIZE_STRING];
		char m_szExepath32[SIZE_STRING];
		
		void SetLastErr (char *szErrortext);
};
