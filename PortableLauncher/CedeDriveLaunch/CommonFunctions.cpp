#include "CommonFunctions.h"

CommonFunctions::CommonFunctions ()
{
}

CommonFunctions::~CommonFunctions ()
{
}

void CommonFunctions::OutputDebugInt (LPTSTR szString, int iValue) {
	char szTemp[1024];
	char szInt [255];
	ZeroMemory (szInt, 255);
	ZeroMemory (szTemp, 1024);
	
	strcpy (szTemp, szString);
	sprintf (szInt, "%d", iValue);
	strcat (szTemp, szInt);
	
	OutputDebugString (szTemp);
}

void CommonFunctions::OutputDebugStringEx (LPTSTR szDescString, LPTSTR szString) {
	char szTemp[2048];
	ZeroMemory (szTemp, 2048);
	
	strcpy (szTemp, szDescString);
	strcat (szTemp, szString);
	OutputDebugString (szTemp);
}

void CommonFunctions::GetFilename (char *szFullPath, char *szOutName, BOOL bReturnExt) {
	// This function takes a full file path, and returns the name of the file giving the option to strip off the extension.
	char szTemp[255];
	int i = 0;
	int iFullstoploc = 0;
	BOOL bFullstopfound = FALSE;
	
	ZeroMemory (szTemp, 255);

	for (i=strlen (szFullPath);i>0;i--) {
		ZeroMemory (szTemp, 255);
		strncpy (szTemp, szFullPath+i, 1);
		if (bReturnExt == FALSE) {
			if (strcmp (szTemp, ".") == 0) {
				//OutputDebugInt ("Found . at: ", i);
				iFullstoploc = i;
				bFullstopfound = TRUE;
			}
		}
		
		if (strcmp (szTemp, "\\") == 0) {
			if (bFullstopfound == TRUE) { // We have already identified the location of the fullstop		
				// We have found the first backslash, so proceed to retrieve the filename
				ZeroMemory (szTemp, 255);
				strncpy (szTemp, szFullPath+i+1, iFullstoploc - (i+1));
				strcpy (szOutName, szTemp);
				return;
			} else {
				// This filepath does not have a full stop, or the user has chosen to return the ext so return the name anyway
				ZeroMemory (szTemp, 255);
				strcpy (szTemp, szFullPath+i+1);
				strcpy (szOutName, szTemp);
				return;
			}
		}
	}
}

BOOL CommonFunctions::CheckBackslashSuffix (char *szFullPath) {
	// This function will take a full path and check whether the end of the path
	// has a backslash, and returns a true if yes, and false if no - useful for checking if a user has already put one in
	// when asking for path names!
	if (strcmp (szFullPath+(strlen (szFullPath)-1), "\\") == 0) {
		return TRUE;
	} else {
		return FALSE;
	}
}

// DJD String manipulation function. Find a given string in another string, and return it's location in the string
int CommonFunctions::FindString (char *szStrtolook, char *szStrtofind) {
	char szTemp[2048];
	int i = 0;
	if (strlen (szStrtolook) >= strlen (szStrtofind)) {
		if (strlen (szStrtolook) > 0 && strlen (szStrtofind) > 0) {
			for (i=0;i<(strlen (szStrtolook) - strlen (szStrtofind))+1;i++) {
				ZeroMemory (szTemp, 2048);
				strncpy (szTemp, szStrtolook+i, strlen (szStrtofind));
				if (strcmp (szTemp, szStrtofind) == 0) {
					// The string has been found
					return i;
				}
			}
		}
	}
	return -1;
}

// DJD String manipulation function. Perform your usual search and replace on a string.
int CommonFunctions::ReplaceString (char *szStrtolook, char *szStrtosearch, char *szStrtoreplace) {
	char szTemp[2048];
	int l = 0;
	
	while (l>-1) {
		ZeroMemory (szTemp, 2048);
		l = FindString (szStrtolook, szStrtosearch);
		if (l>-1) {
			strncpy (szTemp, szStrtolook, l);
			strcat (szTemp, szStrtoreplace);
			strcat (szTemp, szStrtolook+l+strlen (szStrtosearch));
			strcpy (szStrtolook, szTemp);
		}
	}
}

// DJD String function. Given a string, filter a given string from the source string.
char *CommonFunctions::FilterString (char *szStrtolook, char *szStrtofilter) {
	int flen = strlen (szStrtofilter);
	int lpos = 0;
	int c;
	char szTemp[255];
	char szFinal[255];
	ZeroMemory (szTemp, 255);
	ZeroMemory (szFinal, 255);
	strncpy (szTemp, szStrtolook + lpos, flen);
	
	for (c=0;c<strlen (szStrtolook);c++) {
		if (strcmp (szStrtofilter, szTemp) == 0) {
			strncat (szFinal, szStrtolook, lpos);
			strcat (szFinal, szStrtolook + lpos + flen);
			
			return szFinal;
		} else {
			lpos++;
			strncpy (szTemp, szStrtolook + lpos, flen);
		}
	}
	return "";
}

BOOL CommonFunctions::CreateTree (char *szDirTree) {
	char szTempTree[2048];
	BOOL bDirSuccess = FALSE;
	int iseppointer = strlen (szDirTree);
	int i = 0;
	char szTempDirTree[2048];
	
	ZeroMemory (szTempDirTree, 2048);
	strcpy (szTempDirTree, szDirTree);
	if (strncmp (szDirTree+iseppointer-1, "\\", 1) != 0) {
		OutputDebugString ("CreateTree: Backslash Suffix Not found, adding...");
		strcat (szTempDirTree, "\\");
		iseppointer = strlen (szTempDirTree);
	}
	
	while (bDirSuccess == FALSE) {
		ZeroMemory (szTempTree, 2048);
		strncpy (szTempTree, szTempDirTree, iseppointer);		
		if (CreateDirectory (szTempTree, NULL) != 0) {
			bDirSuccess = TRUE;
		} else {
			if (GetLastError () == ERROR_PATH_NOT_FOUND) {
				if (strncmp (szTempDirTree+iseppointer, "\\", 1) == 0) {iseppointer--;}
				for (i=iseppointer;i>0;i--) {
					if (strncmp (szTempDirTree+i, "\\", 1) == 0) {
						iseppointer = i;
						break;
					}
				}
			} else {
				OutputDebugString ("CreateTree: Returning FALSE at scanback");
				return FALSE;
			}
		}
	}
	if (bDirSuccess == TRUE) {
		if (strncmp (szTempDirTree+iseppointer, "\\", 1) == 0) {iseppointer++;}
		for (i=iseppointer;i<strlen(szTempDirTree);i++) {
			if (strncmp (szTempDirTree+i, "\\", 1) == 0) {
				ZeroMemory (szTempTree, 2048);
				strncpy (szTempTree, szTempDirTree, i);
				if (CreateDirectory (szTempTree, NULL) == 0) {
					OutputDebugString ("CreateTree: Returning FALSE at scan forward");
					return FALSE;
				}
			}
		}
	}
	return TRUE;
}

BOOL CommonFunctions::GetModulePath (char *szOutString) {
	int i = 0;
	char szModulePath[2048];
	ZeroMemory (szModulePath, 2048);
	
	if (GetModuleFileName (NULL, szModulePath, 2048) == 0) {
		return FALSE;
	} else {
		for (i=strlen (szModulePath);i>0;i--) {
			if (strncmp (szModulePath+i, "\\", 1) == 0) {
				// Found the first backslash starting backwards in the string
				// so we return this
				strncpy (szOutString, szModulePath, i);
				return TRUE;
			}
		}
	}
}

// The ANSI version of my all famous Getdatapart function. Take an input line given some
// separation parameters, and output the part of the string you want given a delimiter, and a position.

void CommonFunctions::Getdatapart (char *szdataline, int idatapart, char *szsepchar, char *szresult)
{
	char sztempchar[8];
	ZeroMemory (sztempchar, 8);
	
	char szfinalstring[1024];
	ZeroMemory (szfinalstring, 1024);

	char sztdataline[1024];
	ZeroMemory (sztdataline, 1024);
	
	strcpy (sztdataline, szdataline);

	int datalen = 0;
	int c = 0;
	int sepcount = 0;
	
	strcat (sztdataline, szsepchar);
	datalen = strlen (sztdataline);

	for (c=0;c<datalen;c++) {
		strncpy (sztempchar, sztdataline+c, 1);
		if (strcmp (sztempchar, szsepchar) == 0) {
			sepcount++;
			if (sepcount == idatapart) {
				strcpy (szresult, szfinalstring);
				return;
			} else {
				ZeroMemory (szfinalstring, 1024);
			}
		} else {
			strcat (szfinalstring, sztempchar);
		}
	}
	strcpy (szresult, szfinalstring);
}