#pragma once
#include <windows.h>
#include <malloc.h>
#include <stdio.h>
#include <io.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <share.h>
#include "SingleDriveInfo.h"
#include "EVDFileHeader.h"

#define SIZE_STRING		1024

class EvdHandler {
	
	public:
		EvdHandler ();
		~EvdHandler ();		

		bool CheckEVDFile ();
		SingleDriveInfo m_localmodedrive;

	private:
		void GetPathOnly (char *szInpath, char *szOutpath);
		bool FileExists (char *FileName);
		bool GetFreeDriveLetter (char *szOutdriveletter);
		void NumberToDriveLetter (char *szOutdrive, int iNumber);
		int DriveLetterToNumber (char *szDriveletter);

		unsigned long GetVirtualDiskSizeMegs (char *szPath);

		
};
