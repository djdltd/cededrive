//#define _WIN32_WINNT 0x0501
#pragma once
#include <io.h>
#include <windows.h>
#include "MemoryBuffer.h"
#include "resource.h"

class CanvasHandler {
	
	public:
		CanvasHandler ();
		~CanvasHandler ();
		unsigned long GetWriteableOffset (MemoryBuffer *memBuffer);
		void WriteVolumeSerial (char *szDrive, MemoryBuffer *memBuffer);
		bool ReadVolumeSerial (MemoryBuffer *memBuffer, char *szDrive, char *szOutKey);
		void CreateCanvas (char *szFilename);
		bool IsVolumeSerialIdentical (char *szDrive, MemoryBuffer *memBuffer);

		unsigned long Raise (unsigned long lValue, unsigned long lPower);
		void Expand (char *szInval, char *szOutval);
		void RepeatExpand (char *szInval, int Length, char *szOutval);
		DWORD GetVolumeSerialNumber (char *szDrive);
		void EncBytes (char *szInstring, char *szKeyhive, MemoryBuffer *poutBuffer);
		void DecBytes (MemoryBuffer *pinBuffer, char *szKeyhive, char *szOutString);

	private:
		
};
