#include "CanvasHandler.h"

CanvasHandler::CanvasHandler ()
{

}

CanvasHandler::~CanvasHandler ()
{
}


unsigned long CanvasHandler::Raise (unsigned long lValue, unsigned long lPower)
{
	unsigned long lRes = lValue;
	for (int l=1;l<lPower;l++) {
		lRes = lRes * lValue;
	}
	return lRes;
}

void CanvasHandler::Expand (char *szInval, char *szOutval)
{
	//strcpy_s (szOutval, SIZE_STRING, "Output Test");
	char szExp1[SIZE_NAME];
	char szExp2[SIZE_NAME];
	unsigned long lExp1 = 0;
	unsigned long lExp2 = 0;

	unsigned long lPower = 0;

	char szCurResult[SIZE_STRING];

	for (int c=0;c<strlen(szInval);c++) {
		if (c>0) {

			ZeroMemory (szExp1, SIZE_NAME);
			ZeroMemory (szExp2, SIZE_NAME);
			strncpy_s (szExp1, SIZE_NAME, szInval+(c-1), 1);
			strncpy_s (szExp2, SIZE_NAME, szInval+c, 1);

			lExp1 = atol (szExp1);
			lExp2 = atol (szExp2);

			//OutputInt ("Long Exp1: ", lExp1);
			//OutputInt ("Long Exp2: ", lExp2);

			lPower = Raise (lExp1, lExp2);

			ZeroMemory (szCurResult, SIZE_STRING);
			ltoa (lPower, szCurResult, 10);

			//OutputText ("szCurResult: ", szCurResult);

			strcat_s (szOutval, SIZE_STRING, szCurResult);
		}
	}
}

void CanvasHandler::EncBytes (char *szInstring, char *szKeyhive, MemoryBuffer *poutBuffer)
{
	char szCurchar[SIZE_NAME];
	int iCurchar = 0;

	char szCurKey[SIZE_NAME];
	int curkey = 0;

	BYTE bEnc = 0;
	
	for (int c=0;c<strlen(szInstring);c++) {
		// Get the current byte value
		ZeroMemory (szCurchar, SIZE_NAME);
		strncpy_s (szCurchar, SIZE_NAME, szInstring+c, 1);
		iCurchar = szCurchar[0];

		// Get the current key value
		ZeroMemory (szCurKey, SIZE_NAME);
		strncpy_s (szCurKey, SIZE_NAME, szKeyhive+c, 3);
		curkey = atoi (szCurKey);

		bEnc = iCurchar+curkey;

		//OutputInt ("bEnc: ", bEnc);

		poutBuffer->SetByte (c, bEnc);
	}
}

void CanvasHandler::DecBytes (MemoryBuffer *pinBuffer, char *szKeyhive, char *szOutString)
{
	BYTE bCurEnc = 0;
	BYTE bDec = 0;

	char szCurKey[SIZE_NAME];
	int curkey = 0;

	char szCurDec[SIZE_NAME];

	for (int c=0;c<pinBuffer->GetSize ();c++) {
		// Get the current key value
		ZeroMemory (szCurKey, SIZE_NAME);
		strncpy_s (szCurKey, SIZE_NAME, szKeyhive+c, 3);
		curkey = atoi (szCurKey);
		
		bCurEnc = pinBuffer->GetByte (c);
		bDec = bCurEnc-curkey;

		ZeroMemory (szCurDec, SIZE_NAME);
		szCurDec[0] = bDec;

		strcat_s (szOutString, SIZE_STRING, szCurDec);
	}
}

void CanvasHandler::RepeatExpand (char *szInval, int Length, char *szOutval)
{
	char szResult[SIZE_STRING];
	ZeroMemory (szResult, SIZE_STRING);

	Expand (szInval, szResult);

	char szNewResult[SIZE_STRING];

	while (strlen(szResult) < Length)
	{
		ZeroMemory (szNewResult, SIZE_STRING);
		Expand (szResult, szNewResult);

		strcpy_s (szResult, SIZE_STRING, szNewResult);
	}

	strcpy_s (szOutval, SIZE_STRING, szResult);
}

DWORD CanvasHandler::GetVolumeSerialNumber (char *szDrive)
{
	char szVolname[SIZE_STRING];
	ZeroMemory (szVolname, SIZE_STRING);

	DWORD dwSerial = 0;

	DWORD dwMaxcomponent = 0;
	DWORD dwFlags = 0;
	
	char szFilesystem[SIZE_STRING];
	ZeroMemory (szFilesystem, SIZE_STRING);

	if (GetVolumeInformation (szDrive, szVolname, SIZE_STRING, &dwSerial, &dwMaxcomponent, &dwFlags, szFilesystem, SIZE_STRING) != 0) {
		
		return dwSerial;
	} else {
		return 0;
	}

	return 0;
}

void CanvasHandler::CreateCanvas (char *szFilename)
{
	unsigned long lk1 = 14777324;
	unsigned long lk2 = 77782415;
	unsigned long lk3 = 77777777;
	unsigned long lk4 = 44444444;
	unsigned long lk5 = 77777777;
	unsigned long lk6 = 11881188;
	unsigned long lk7 = 11881188;

	MemoryBuffer memFile;
	memFile.SetSize (50000);
	memFile.Append (&lk1, sizeof (unsigned long));
	memFile.Append (&lk2, sizeof (unsigned long));
	memFile.Append (&lk3, sizeof (unsigned long));
	memFile.Append (&lk4, sizeof (unsigned long));
	memFile.Append (&lk5, sizeof (unsigned long));
	memFile.Append (&lk6, sizeof (unsigned long));
	memFile.Append (&lk7, sizeof (unsigned long));

	memFile.SaveToFile (szFilename);

}

unsigned long CanvasHandler::GetWriteableOffset (MemoryBuffer *memBuffer)
{
	// This function scans the CedeCrypt portable executable and obtains the writeable offset
	// by scanning for the 7 magic numbers which are the headers for the cp canvas resource.
	// the writeable offset is then returned. It is then up to the calling function to specify
	// a sub-offset to actually write the data, the offset returned here will only return the
	// beginning of the resource

	unsigned long lk1 = 14777324;
	unsigned long lk2 = 77782415;
	unsigned long lk3 = 77777777;
	unsigned long lk4 = 44444444;
	unsigned long lk5 = 77777777;
	unsigned long lk6 = 11881188;
	unsigned long lk7 = 11881188;

	unsigned long lc1 = 0;
	unsigned long lc2 = 0;
	unsigned long lc3 = 0;
	unsigned long lc4 = 0;
	unsigned long lc5 = 0;
	unsigned long lc6 = 0;
	unsigned long lc7 = 0;

	unsigned int msize = sizeof (unsigned long) * 7;

	unsigned long b = 0;
	unsigned int ipointer = 0;
	
	if (memBuffer->GetSize () > 0) {
		for (b=0;b<memBuffer->GetSize ()-msize;b++) {
			ipointer = 0;

			memcpy (&lc1, (BYTE *) memBuffer->GetBuffer ()+b+ipointer, sizeof (unsigned long));
			ipointer+=sizeof (unsigned long);
			memcpy (&lc2, (BYTE *) memBuffer->GetBuffer ()+b+ipointer, sizeof (unsigned long));
			ipointer+=sizeof (unsigned long);
			memcpy (&lc3, (BYTE *) memBuffer->GetBuffer ()+b+ipointer, sizeof (unsigned long));
			ipointer+=sizeof (unsigned long);
			memcpy (&lc4, (BYTE *) memBuffer->GetBuffer ()+b+ipointer, sizeof (unsigned long));
			ipointer+=sizeof (unsigned long);
			memcpy (&lc5, (BYTE *) memBuffer->GetBuffer ()+b+ipointer, sizeof (unsigned long));
			ipointer+=sizeof (unsigned long);
			memcpy (&lc6, (BYTE *) memBuffer->GetBuffer ()+b+ipointer, sizeof (unsigned long));
			ipointer+=sizeof (unsigned long);
			memcpy (&lc7, (BYTE *) memBuffer->GetBuffer ()+b+ipointer, sizeof (unsigned long));
			ipointer+=sizeof (unsigned long);

			if (lc1 == lk1 && lc2 == lk2 && lc3 == lk3 && lc4 == lk4 && lc5 == lk5 && lc6 == lk6 && lc7 == lk7) {
				return b+msize+10;
			}
		}
	}
	
	return 0;
}

void CanvasHandler::WriteVolumeSerial (char *szDrive, MemoryBuffer *memBuffer)
{
	char szExpandedKey[SIZE_STRING];
	ZeroMemory (szExpandedKey, SIZE_STRING);
	RepeatExpand ("2347", 77, szExpandedKey);

	char szModule[SIZE_STRING];
	ZeroMemory (szModule, SIZE_STRING);
	GetModuleFileName (NULL, szModule, SIZE_STRING);

	unsigned long lwriteoffset = GetWriteableOffset (memBuffer);
	
	if (lwriteoffset > 100) { // Make sure the offset was found

		// Generate the machine key
		DWORD dwSerial = GetVolumeSerialNumber (szDrive);
		char szMachineID [SIZE_STRING];
		ZeroMemory (szMachineID, SIZE_STRING);
		//ReadMachine (szMachineID);
		sprintf_s (szMachineID, SIZE_STRING, "%X", dwSerial);

		// Encode the machine key
		MemoryBuffer memEncoded;
		memEncoded.SetSize (strlen (szMachineID));
		EncBytes (szMachineID, szExpandedKey, &memEncoded);

		// Write the size to the file
		int iencsize = memEncoded.GetSize ();
		memBuffer->Write (&iencsize, lwriteoffset+1700, sizeof (int));

		// Write the encoded key to the file
		memBuffer->Write (memEncoded.GetBuffer (), lwriteoffset+1900, memEncoded.GetSize ());
	}
}

bool CanvasHandler::ReadVolumeSerial (MemoryBuffer *memBuffer, char *szDrive, char *szOutKey)
{
	char szExpandedKey[SIZE_STRING];
	ZeroMemory (szExpandedKey, SIZE_STRING);
	RepeatExpand ("2347", 77, szExpandedKey);
	
	unsigned long lwriteoffset = GetWriteableOffset (memBuffer);

	if (lwriteoffset > 100) {

		// Get the size of the encoded key from the file
		int iencsize = 0;
		memcpy (&iencsize, (BYTE *) memBuffer->GetBuffer ()+lwriteoffset+1700, sizeof (int));

		// Now read the encoded machine key
		MemoryBuffer memEncoded;
		memEncoded.SetSize (iencsize);
		memEncoded.Write ((BYTE *) memBuffer->GetBuffer ()+lwriteoffset+1900, 0, iencsize);

		//Now decode the key
		DecBytes (&memEncoded, szExpandedKey, szOutKey);
		return true;

	} else {

		return false;
	}

	
}

bool CanvasHandler::IsVolumeSerialIdentical (char *szDrive, MemoryBuffer *memBuffer)
{
	// Get the machine key from disk
	char szDiskKey[SIZE_STRING];
	ZeroMemory (szDiskKey, SIZE_STRING);
	if (ReadVolumeSerial (memBuffer, szDrive, szDiskKey) == true) {
	//if (LoadMachineKey (szDiskKey) == true) {
		
		char m[SIZE_STRING];
		ZeroMemory (m, SIZE_STRING);
		
		// Generate the actual machine key machine key
		DWORD dwSerial = GetVolumeSerialNumber (szDrive);
		char szMachineID [SIZE_STRING];
		ZeroMemory (szMachineID, SIZE_STRING);
		sprintf_s (szMachineID, SIZE_STRING, "%X", dwSerial);

		
		sprintf_s (m, SIZE_STRING, "szDiskKey is: %s, szMachineID is: %s", szDiskKey, szMachineID);
		//MessageBox (NULL, m, "Diagnostics", MB_OK);


		// Check for a match
		if (strcmp (szDiskKey, szMachineID) == 0) {
			return true;
		} else {
			return false;
		}
	} else {
		return false;
	}

	return false;
}
