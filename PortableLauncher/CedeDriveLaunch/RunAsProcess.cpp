#include "RunAsProcess.h"

RunAsProcess::RunAsProcess ()
{
	uniCount = 0;
}

RunAsProcess::~RunAsProcess ()
{

}


int RunAsProcess::execAsUser (LPCSTR szUsername, LPCSTR szDomain, LPCSTR szPassword, LPCSTR szCommand)
{
	int iRet = -1;
		
	STARTUPINFO sInfo;
	ZeroMemory (&sInfo, sizeof (STARTUPINFO));
	sInfo.cb = sizeof (sInfo);
	sInfo.wShowWindow = SW_SHOWDEFAULT;
	sInfo.lpDesktop = NULL;
	sInfo.lpReserved = NULL;
	sInfo.cbReserved2 = 0;
	sInfo.lpReserved2 = NULL;		

	PROCESS_INFORMATION processInfo;

		
	char strUsername[255];
	ZeroMemory (strUsername, 255);
	strcpy_s (strUsername, 255, szUsername);

	char strDomain[255];
	ZeroMemory (strDomain, 255);
	strcpy_s (strDomain, 255, szDomain);
				
	char strPassword[255];
	ZeroMemory (strPassword, 255);
	strcpy_s (strPassword, 255, szPassword);

	char strCommand[255];
	ZeroMemory (strCommand, 255);
	strcpy_s (strCommand, 255, szCommand);

	// Convert strUsername to wusername Unicode format.
	strcpy_s (uniConvIn, 255, strUsername);
		
	iRet = -1;
	iRet = toUnicode ();
	if (iRet != 0) {
		return -1;
	}

	wchar_t *wusername;
	wusername = new wchar_t [uniCount];
	wcscpy (wusername, uniConvOut);
	ZeroMemory (uniConvIn, 255);

	// Convert strDomain to wdomain Unicode format.		

	strcpy_s (uniConvIn, 255, strDomain);
	iRet = -1;
	iRet = toUnicode ();
	if (iRet != 0) {
		return -1;
	}

	wchar_t *wdomain;
	wdomain = new wchar_t [uniCount];
	wcscpy (wdomain, uniConvOut);
	ZeroMemory (uniConvIn, 255);

	// Convert strPassword to wpassword Unicode format.

	strcpy_s (uniConvIn, 255, strPassword);
	iRet = -1;
	iRet = toUnicode ();
	if (iRet != 0) {
		return -1;
	}

	wchar_t *wpassword;
	wpassword = new wchar_t [uniCount];
	wcscpy (wpassword, uniConvOut);
	ZeroMemory (uniConvIn, 255);

	// Convert strCommand to wcommand Unicode format

	strcpy_s (uniConvIn, 255, strCommand);
	iRet = -1;
	iRet = toUnicode ();
	if (iRet != 0) {
		return -1;
	}		

	wchar_t *wcommand;
	wcommand = new wchar_t [uniCount];
	wcscpy (wcommand, uniConvOut);
	ZeroMemory (uniConvIn, 255);

	iRet = CreateProcessWithLogonW (wusername, 
						wdomain,
						wpassword,
						0,
						wcommand,
						NULL,
						NORMAL_PRIORITY_CLASS,
						NULL, 
						NULL,
						(LPSTARTUPINFOW) &sInfo, 
						&processInfo);

	if (iRet == 0) {
		int errCode = GetLastError ();
		char errorMsg [255];
		ZeroMemory (errorMsg, 255);

		sprintf (errorMsg, "CreateProcess Failed with error code %d.", errCode);
		//MessageBox (NULL, errorMsg, "CreateProcessAsUser", MB_OK);
		return errCode;
	} else {
		return 0;
	}
	
}


int RunAsProcess::toUnicode ()
{
	int iRet = 0;	
	
	iRet = MultiByteToWideChar (CP_ACP, MB_PRECOMPOSED, uniConvIn, 1 + strlen(uniConvIn), NULL, 0);
	if (iRet == 0) {
		int errCode = GetLastError ();
		char errorMsg [255];
		ZeroMemory (errorMsg, 255);
			
		sprintf_s (errorMsg, 255, "MultiByteToWideChar failed with code %d.", errCode);
		
		MessageBox (NULL, errorMsg, "MultiByteToWideChar", MB_OK);
		return -1;
	}
	uniConvOut = new wchar_t[iRet];
	uniCount = iRet;
	int finalRes = MultiByteToWideChar (CP_ACP, MB_PRECOMPOSED, uniConvIn, 1 + strlen(uniConvIn), uniConvOut, iRet);
	if (finalRes == 0) {
		int errCode = GetLastError ();
		char errorMsg [255];
		ZeroMemory (errorMsg, 255);
			
		sprintf_s (errorMsg, 255, "Second MultiByteToWideChar failed with code %d.", errCode);
			
		MessageBox (NULL, errorMsg, "MultiByteToWideChar", MB_OK);
		return -1;
	}
	return 0;
}
