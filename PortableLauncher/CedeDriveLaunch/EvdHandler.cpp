#include "EvdHandler.h"

EvdHandler::EvdHandler ()
{
}

EvdHandler::~EvdHandler ()
{
}

bool EvdHandler::FileExists (char *FileName)
{
	// Check if the file exists		
	struct _finddata_t c_file;
	long hFile;

	// Build the list of files in the source path
	if( (hFile = _findfirst(FileName, &c_file )) == -1L ) {
		return false;
	} else {
		return true;
	}

	return false;
}

void EvdHandler::GetPathOnly (char *szInpath, char *szOutpath)
{
	char szCurchar[SIZE_NAME];
	int seploc = 0;

	for (int c=strlen(szInpath);c>0;c--) {
		
		ZeroMemory (szCurchar, SIZE_NAME);
		strncpy_s (szCurchar, SIZE_NAME, szInpath+c, 1);

		if (strcmp (szCurchar, "\\") == 0) {
			seploc = c+1;
			break;
		}
	}

	strncpy_s (szOutpath, SIZE_STRING, szInpath, seploc);
}


bool EvdHandler::GetFreeDriveLetter (char *szOutdriveletter)
{
	char szDrives[SIZE_STRING];
	ZeroMemory (szDrives, SIZE_STRING);

	char szCurDrive[SIZE_STRING];
	char szLetter[SIZE_NAME];
	char szAvaildrive[SIZE_NAME];

	int a = 0;
	int l = 0;
	int len = 0;

	bool bAvaildrives[26];

	for (a=0;a<26;a++) {
		bAvaildrives[a] = true;
	}

	len = GetLogicalDriveStrings (SIZE_STRING, szDrives);
	
	for (a=0;a<len;a++) {
		if (szDrives[a] == 0) {

			ZeroMemory (szCurDrive, SIZE_STRING);
			strncpy (szCurDrive, szDrives+l, a-l);			
			l = a+1;

			ZeroMemory (szLetter, SIZE_NAME);
			strncpy_s (szLetter, SIZE_NAME, szCurDrive, 1);

			bAvaildrives[DriveLetterToNumber(szLetter)] = false;
		}
	}

	// Ensure drive A,  B and C are not chosen
	bAvaildrives[0] = false;
	bAvaildrives[1] = false;
	bAvaildrives[2] = false;


	for (a=0;a<26;a++) {

		if (bAvaildrives[a] == true) {
			
			ZeroMemory (szAvaildrive, SIZE_NAME);
			NumberToDriveLetter (szAvaildrive, a);

			ZeroMemory (szOutdriveletter, SIZE_NAME);
			strcpy_s (szOutdriveletter, SIZE_NAME, szAvaildrive);

			return true;
		}		
	}

	return false;
}

int EvdHandler::DriveLetterToNumber (char *szDriveletter)
{
	if (strcmp (szDriveletter, "A") == 0) {return 0;}
	if (strcmp (szDriveletter, "B") == 0) {return 1;}
	if (strcmp (szDriveletter, "C") == 0) {return 2;}
	if (strcmp (szDriveletter, "D") == 0) {return 3;}
	if (strcmp (szDriveletter, "E") == 0) {return 4;}
	if (strcmp (szDriveletter, "F") == 0) {return 5;}
	if (strcmp (szDriveletter, "G") == 0) {return 6;}
	if (strcmp (szDriveletter, "H") == 0) {return 7;}
	if (strcmp (szDriveletter, "I") == 0) {return 8;}
	if (strcmp (szDriveletter, "J") == 0) {return 9;}
	if (strcmp (szDriveletter, "K") == 0) {return 10;}
	if (strcmp (szDriveletter, "L") == 0) {return 11;}
	if (strcmp (szDriveletter, "M") == 0) {return 12;}
	if (strcmp (szDriveletter, "N") == 0) {return 13;}
	if (strcmp (szDriveletter, "O") == 0) {return 14;}
	if (strcmp (szDriveletter, "P") == 0) {return 15;}
	if (strcmp (szDriveletter, "Q") == 0) {return 16;}
	if (strcmp (szDriveletter, "R") == 0) {return 17;}
	if (strcmp (szDriveletter, "S") == 0) {return 18;}
	if (strcmp (szDriveletter, "T") == 0) {return 19;}
	if (strcmp (szDriveletter, "U") == 0) {return 20;}
	if (strcmp (szDriveletter, "V") == 0) {return 21;}
	if (strcmp (szDriveletter, "W") == 0) {return 22;}
	if (strcmp (szDriveletter, "X") == 0) {return 23;}
	if (strcmp (szDriveletter, "Y") == 0) {return 24;}
	if (strcmp (szDriveletter, "Z") == 0) {return 25;}
	
	return -1;
}

void EvdHandler::NumberToDriveLetter (char *szOutdrive, int iNumber)
{
	ZeroMemory (szOutdrive, SIZE_NAME);

	if (iNumber == 0) {strcpy_s (szOutdrive, SIZE_NAME, "A");}
	if (iNumber == 1) {strcpy_s (szOutdrive, SIZE_NAME, "B");}
	if (iNumber == 2) {strcpy_s (szOutdrive, SIZE_NAME, "C");}
	if (iNumber == 3) {strcpy_s (szOutdrive, SIZE_NAME, "D");}
	if (iNumber == 4) {strcpy_s (szOutdrive, SIZE_NAME, "E");}
	if (iNumber == 5) {strcpy_s (szOutdrive, SIZE_NAME, "F");}
	if (iNumber == 6) {strcpy_s (szOutdrive, SIZE_NAME, "G");}
	if (iNumber == 7) {strcpy_s (szOutdrive, SIZE_NAME, "H");}
	if (iNumber == 8) {strcpy_s (szOutdrive, SIZE_NAME, "I");}
	if (iNumber == 9) {strcpy_s (szOutdrive, SIZE_NAME, "J");}
	if (iNumber == 10) {strcpy_s (szOutdrive, SIZE_NAME, "K");}
	if (iNumber == 11) {strcpy_s (szOutdrive, SIZE_NAME, "L");}
	if (iNumber == 12) {strcpy_s (szOutdrive, SIZE_NAME, "M");}
	if (iNumber == 13) {strcpy_s (szOutdrive, SIZE_NAME, "N");}
	if (iNumber == 14) {strcpy_s (szOutdrive, SIZE_NAME, "O");}
	if (iNumber == 15) {strcpy_s (szOutdrive, SIZE_NAME, "P");}
	if (iNumber == 16) {strcpy_s (szOutdrive, SIZE_NAME, "Q");}
	if (iNumber == 17) {strcpy_s (szOutdrive, SIZE_NAME, "R");}
	if (iNumber == 18) {strcpy_s (szOutdrive, SIZE_NAME, "S");}
	if (iNumber == 19) {strcpy_s (szOutdrive, SIZE_NAME, "T");}
	if (iNumber == 20) {strcpy_s (szOutdrive, SIZE_NAME, "U");}
	if (iNumber == 21) {strcpy_s (szOutdrive, SIZE_NAME, "V");}
	if (iNumber == 22) {strcpy_s (szOutdrive, SIZE_NAME, "W");}
	if (iNumber == 23) {strcpy_s (szOutdrive, SIZE_NAME, "X");}
	if (iNumber == 24) {strcpy_s (szOutdrive, SIZE_NAME, "Y");}
	if (iNumber == 25) {strcpy_s (szOutdrive, SIZE_NAME, "Z");}
}

unsigned long EvdHandler::GetVirtualDiskSizeMegs (char *szPath)
{
	unsigned long lHeaderoffset = 0;
	EVDFileHeader fileheader;
	if (fileheader.SetFromFile (szPath) == true) {
		fileheader.Deserialise ();
		lHeaderoffset = fileheader.GetDataOffset ();
	} else {
		lHeaderoffset = 0;
	}


	HANDLE hFile;
	hFile = CreateFile(szPath, GENERIC_READ | GENERIC_WRITE,FILE_SHARE_READ,0,OPEN_ALWAYS,FILE_FLAG_NO_BUFFERING | FILE_FLAG_RANDOM_ACCESS,0);

    if(hFile == INVALID_HANDLE_VALUE)
    {
        return 0;
    }

	LARGE_INTEGER currentSize;
	LARGE_INTEGER newPointerlocation;
    //currentSize.LowPart = GetFileSize(g_hFile,(LPDWORD)&currentSize.HighPart);
    
	GetFileSizeEx (hFile, &currentSize);

	int chunksize = 1024 * 1024;

	unsigned long megs = ((currentSize.QuadPart-lHeaderoffset) / chunksize);

	CloseHandle (hFile);

	return megs;
}

bool EvdHandler::CheckEVDFile ()
{
	char szModulefilename[SIZE_STRING];
	ZeroMemory (szModulefilename, SIZE_STRING);
	GetModuleFileName (NULL, szModulefilename, SIZE_STRING);	

	char szPathonly[SIZE_STRING];
	ZeroMemory (szPathonly, SIZE_STRING);
	GetPathOnly (szModulefilename, szPathonly);
	strcat_s (szPathonly, SIZE_STRING, "EncryptedDisk.evd");

	char szFreeDriveletter[SIZE_NAME];
	ZeroMemory (szFreeDriveletter, SIZE_NAME);

	if (FileExists (szPathonly) == true) {

		if (GetFreeDriveLetter (szFreeDriveletter) == false) {
			// no available drive letter on this system, abort!
			MessageBox (NULL, "There are no available drive letters available on this system. Cannot mount encrypted disk.", "Insufficient drive letters", MB_OK | MB_ICONEXCLAMATION);
			PostQuitMessage (0);
			return false;
		}

		// if we got here then we have an available drive letter to use for later on
		// but we need to add a colon to it.
		strcat_s (szFreeDriveletter, SIZE_NAME, ":");
		
		ZeroMemory (m_localmodedrive.szName, SIZE_STRING);
		strcpy_s (m_localmodedrive.szName, SIZE_STRING, "Removable Encrypted Drive"); //m_localmodedrive
		strcat_s (m_localmodedrive.szName, SIZE_STRING, " (");
		strcat_s (m_localmodedrive.szName, SIZE_STRING, szFreeDriveletter);
		strcat_s (m_localmodedrive.szName, SIZE_STRING, ")");

		ZeroMemory (m_localmodedrive.szDescription, SIZE_STRING);
		strcpy_s (m_localmodedrive.szDescription, SIZE_STRING, "Standalone Encrypted disk for use with removable drives.");

		ZeroMemory (m_localmodedrive.szPath, SIZE_STRING);
		strcpy_s (m_localmodedrive.szPath, SIZE_STRING, szPathonly);

		ZeroMemory (m_localmodedrive.szDriveLetter, SIZE_NAME);
		strcpy_s (m_localmodedrive.szDriveLetter, SIZE_NAME, szFreeDriveletter);
		
		m_localmodedrive.bMountStartup = true;

		// Need to specify a disk size in megs!!!!
		unsigned long vdsize = GetVirtualDiskSizeMegs (szPathonly);
		m_localmodedrive.lDiskSizeMegs = vdsize;

	} else {
		return false;
	}

	return true;
}