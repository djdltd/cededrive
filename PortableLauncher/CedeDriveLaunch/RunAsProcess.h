#pragma once
#define _WIN32_WINNT 0x0501 // Change this to the appropriate value to target Windows Me or later.
#include <windows.h>
#include <stdio.h>
#include "resource.h"

class RunAsProcess {
	
	public:
		RunAsProcess ();
		~RunAsProcess ();	
		int execAsUser (LPCSTR szUsername, LPCSTR szDomain, LPCSTR szPassword, LPCSTR szCommand);
	private:
		int toUnicode ();

		char uniConvIn [255];
		wchar_t *uniConvOut;
		int uniCount;
};