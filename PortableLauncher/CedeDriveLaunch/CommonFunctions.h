#pragma once
#include <windows.h>
#include <io.h>
#include <stdio.h>

class CommonFunctions
{
	public:
		CommonFunctions ();
		~CommonFunctions ();
		void OutputDebugInt (LPTSTR szString, int iValue);
		void GetFilename (char *szFullPath, char *szOutName, BOOL bReturnExt);
		BOOL CheckBackslashSuffix (char *szFullPath);
		int FindString (char *szStrtolook, char *szStrtofind);
		int ReplaceString (char *szStrtolook, char *szStrtosearch, char *szStrtoreplace);
		char *FilterString (char *szStrtolook, char *szStrtofilter);
		BOOL CreateTree (char *szDirTree);
		BOOL GetModulePath (char *szOutString);
		void OutputDebugStringEx (LPTSTR szDescString, LPTSTR szString);
		void Getdatapart (char *szdataline, int idatapart, char *szsepchar, char *szresult);
	private:
};