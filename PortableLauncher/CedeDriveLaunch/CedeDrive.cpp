// Program to make a simple window

#include <windows.h>
#include <stdio.h>
#include <io.h>
#include <direct.h>
#include "SingleDriveInfo.h"
#include "DynList.h"
#include "MemoryBuffer.h"
#include "EvdHandler.h"
#include "ServicePipeClient.h"
#include "DeploymentHandler.h"
#include "RunAsProcess.h"
#include "CommonFunctions.h"
#include "SharedConfigFile.h"
#include "CanvasHandler.h"
#include "resource.h"

const char g_szClassName[] = "CedeDriveLaunchWindowClass";
const char g_mutexname [] = "CedeDriveEngine_Win32_Instance_Mutex";
const unsigned long g_maxsharemem = 1024000;

HBITMAP g_hbmBanner = NULL;
HWND g_txtpassword = NULL;
HWND g_txtusername = NULL;
HWND g_lblusername = NULL;
HWND g_lblpassword = NULL;
HWND g_lbltitle = NULL;
HWND g_btnok = NULL;
HWND g_btncancel = NULL;

HWND g_statichwnd;
HWND g_hwnd;
DeploymentHandler g_deployment;
CanvasHandler g_canvas;
SharedConfigFile g_config;
EvdHandler g_evd;
RunAsProcess g_runas;
bool g_bdebugmode = true;
MemoryBuffer g_meminfo;
bool g_ipcmessageregistered;
UINT g_ipcmessage;
UINT g_quitmessage; // Global quit message so that the cededrive process can tell us to exit when it's done with us
ServicePipeClient g_pipeclient;

typedef BOOL (WINAPI *LPFN_ISWOW64PROCESS) (HANDLE, PBOOL);
LPFN_ISWOW64PROCESS fnIsWow64Process = (LPFN_ISWOW64PROCESS)GetProcAddress(GetModuleHandle("kernel32"),"IsWow64Process");
 
BOOL IsWow64()
{
    BOOL bIsWow64 = FALSE;
 
    if (NULL != fnIsWow64Process)
    {
        if (!fnIsWow64Process(GetCurrentProcess(),&bIsWow64))
        {
            // handle error
        }
    }
    return bIsWow64;
}

void ShowMessage (char *szMessage)
{
	if (g_bdebugmode == true) {
		MessageBox (NULL, szMessage, "Message", MB_OK);
	}
}

bool IsServiceRunning ()
{
	InstructionResult result;
	result = g_pipeclient.RequestPing ();

	if (result.bSuccess == true) {
		return true;
	} else {
		//MessageBox (NULL, result.szDescription, "Success was false.", MB_OK);
		return false;
	}
}

bool IsUserAdmin ()
{

	char szSystemroot[SIZE_STRING];
	ZeroMemory (szSystemroot, SIZE_STRING);

	if (GetEnvironmentVariable ("SystemRoot", szSystemroot, SIZE_STRING) == 0) {
		return false;
	}

	strcat_s (szSystemroot, SIZE_STRING, "\\system32\\config\\systemprofile\\*.*");
	

	// Check if the file exists		
	struct _finddata_t c_file;
	long hFile;

	// Build the list of files in the source path
	if( (hFile = _findfirst(szSystemroot, &c_file )) == -1L ) {
		return false;
	} else {
		return true;
	}
}

bool IsInstanceRunning ()
{
	// Create the named mutex
	HANDLE mutex = CreateMutex(NULL, FALSE, g_mutexname);
	if (mutex == NULL)
		return true;

	// Check that the mutex didn't already exist
	if (GetLastError() == ERROR_ALREADY_EXISTS)
		return true;


	ReleaseMutex (mutex);
	CloseHandle (mutex);
	return false;
}


void ShowInt (int iInttoShow) {
	char szMsg[255];
	ZeroMemory (szMsg, 255);
	
	sprintf (szMsg, "Value of integer: %d", iInttoShow);
	MessageBox (NULL, szMsg, "ShowInt", MB_OK);
}

bool ShareDriveInfo ()
{
	DynList dlDrivelist;
	dlDrivelist.AddItem (&g_evd.m_localmodedrive, sizeof (SingleDriveInfo), false);

	// Put this 1 item list into our memory buffer
	dlDrivelist.ToMemoryBuffer (&g_meminfo);

	return g_config.Save (&g_meminfo);

	//return g_meminfo.Share ();
}

void RegisterIPCEvent (char *szEventname)
{
	if (g_ipcmessageregistered == false) {
		g_ipcmessage = RegisterWindowMessage (szEventname);
		g_ipcmessageregistered = true;
	}
}

void RegisterQuitEvent ()
{
	g_quitmessage = RegisterWindowMessage ("CedeDriveLaunchQuit");
}

void BroadcastIPCEvent (WPARAM wParam, LPARAM lParam)
{
	if (g_ipcmessageregistered == true) {
		SendMessage (HWND_BROADCAST, g_ipcmessage, wParam, lParam);
	}			
}


void ValidateInput ()
{
	CommonFunctions cmf;

	char szEUsername[SIZE_STRING];
	char szEPassword[SIZE_STRING];
	char szEDomain[SIZE_STRING];
	char szServicePath[SIZE_STRING];

	ZeroMemory (szEUsername, SIZE_STRING);
	ZeroMemory (szEPassword, SIZE_STRING);
	ZeroMemory (szEDomain, SIZE_STRING);

	GetDlgItemText (g_hwnd, IDC_TXTUSERNAME, szEUsername, SIZE_STRING);
	GetDlgItemText (g_hwnd, IDC_TXTPASSWORD, szEPassword, SIZE_STRING);

	if (strlen (szEUsername) == 0 || strlen (szEPassword) == 0) {
		MessageBox (NULL, "Please enter a network Username and Password.", "Input Error", MB_OK | MB_ICONEXCLAMATION);
		return;
	}

	if (cmf.FindString (szEUsername, "\\") == -1) {
		strcpy_s (szEDomain, SIZE_STRING, "localhost");
	} else {
		cmf.Getdatapart (szEUsername, 1, "\\", szEDomain);
		cmf.Getdatapart (szEUsername, 2, "\\", szEUsername);
	}

	char szSystemroot[SIZE_STRING];
	ZeroMemory (szSystemroot, SIZE_STRING);

	// Now time to validate the details the user has entered
	if (GetEnvironmentVariable ("SystemRoot", szSystemroot, SIZE_STRING) == 0) {
		 MessageBox (NULL, "Error getting environment settings.", "Environment Error.", MB_OK | MB_ICONEXCLAMATION);
	}
	strcat_s (szSystemroot, SIZE_STRING, "\\system32\\WinLogon.exe");

	if (g_runas.execAsUser (szEUsername, szEDomain, szEPassword, szSystemroot) != 0) {
		MessageBox (NULL, "Error Authenticating using entered account details. Please try again.", "Authentication Error", MB_OK | MB_ICONEXCLAMATION);
	} else {
		//strcpy_s (g_szUsername, SIZE_STRING, szEUsername);
		//strcpy_s (g_szPassword, SIZE_STRING, szEPassword);
		//strcpy_s (g_szDomain, SIZE_STRING, szEDomain);
		ZeroMemory (szServicePath, SIZE_STRING);
		g_deployment.GetServicePath (szServicePath);

		if (g_runas.execAsUser (szEUsername, szEDomain, szEPassword, szServicePath) != 0) {
			MessageBox (NULL, "Account details verified, but unable to launch Service deployment process.", "Service Error", MB_OK | MB_ICONEXCLAMATION);
		}

		if (g_deployment.LaunchApplication () == false) {
			MessageBox (NULL, "Fatal Error: Unable to launch CedeDrive core.", "CedeDrive Launch Error", MB_OK | MB_ICONEXCLAMATION);
			PostQuitMessage (0);
			return;
		}
		
		PostQuitMessage (0);
	}

	//strcpy_s (g_szUsername, SIZE_STRING, szEUsername);
	//strcpy_s (g_szPassword, SIZE_STRING, szEPassword);
	//strcpy_s (g_szDomain, SIZE_STRING, szEDomain);
}


bool AuthoriseExecution ()
{
	char szFilename[SIZE_STRING];
	ZeroMemory (szFilename, SIZE_STRING);
	GetModuleFileName (NULL, szFilename, SIZE_STRING);

	char szDrive[SIZE_NAME];
	ZeroMemory (szDrive, SIZE_NAME);
	strncpy_s (szDrive, SIZE_NAME, szFilename, 3);

	MemoryBuffer memModule;
	memModule.ReadFromFile (szFilename);

	if (g_canvas.IsVolumeSerialIdentical (szDrive, &memModule) == true) {
		memModule.Clear ();
		return true;
	} else {
		memModule.Clear ();
		return false;
	}
}


// Step4: The Windows Procedure - When windows calls us!
LRESULT CALLBACK WndProc (HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam) {
	
	if (msg == g_quitmessage) {
		// We've received a message to quit - so exit the launcher, we are not needed anymore
		PostQuitMessage (0);
	}
	
	switch (msg) {
		case WM_COMMAND:
		{
			switch (wParam)
			{
				case IDC_BTNOK:
				{
					ValidateInput ();
				}
				break;
				case IDC_BTNCANCEL:
				{
					PostQuitMessage (0);
				}
				break;
			}
		}
		break;
		case WM_CREATE:
		{
			g_hwnd = hwnd;
			RegisterQuitEvent (); // Register a global quit event so cede drive core can tell us to exit when we're not needed
			g_ipcmessageregistered = false;
			HFONT hfDefault;
			hfDefault = (HFONT) GetStockObject(DEFAULT_GUI_FONT);

			g_hbmBanner = LoadBitmap (GetModuleHandle (NULL), MAKEINTRESOURCE (IDB_BANNER));
			if (g_hbmBanner == NULL) {
				MessageBox (hwnd, "Could not load IDB_BANNER resource!", "Error", MB_OK | MB_ICONEXCLAMATION);
			}			

			// Create the main label control
			g_lbltitle = CreateWindow ("static", "Administrator privileges are required for first time usage. This request will only occur once on this computer, and is displayed because you do not have Administrator privileges.\n\nPlease enter account details with Administrator access on this machine:", WS_CHILD | WS_VISIBLE, 10, 70, 300, 100, hwnd, (HMENU) IDC_LBLUSERNAME, GetModuleHandle (NULL), NULL);			
			SendMessage(g_lbltitle, WM_SETFONT, (WPARAM)hfDefault, MAKELPARAM(FALSE, 0));

			int yoffset = 30;
			g_btnok = CreateWindow ("button", "Ok", WS_CHILD | WS_VISIBLE, 138, 215+yoffset, 74, 22, hwnd, (HMENU) IDC_BTNOK, GetModuleHandle (NULL), NULL);			
			SendMessage(g_btnok, WM_SETFONT, (WPARAM)hfDefault, MAKELPARAM(FALSE, 0));

			g_btncancel = CreateWindow ("button", "Cancel", WS_CHILD | WS_VISIBLE, 222, 215+yoffset, 74, 22, hwnd, (HMENU) IDC_BTNCANCEL, GetModuleHandle (NULL), NULL);			
			SendMessage(g_btncancel, WM_SETFONT, (WPARAM)hfDefault, MAKELPARAM(FALSE, 0));

			g_lblusername = CreateWindow ("static", "Username:", WS_CHILD | WS_VISIBLE, 10, 137+yoffset, 100, 24, hwnd, (HMENU) IDC_LBLUSERNAME, GetModuleHandle (NULL), NULL);			
			SendMessage(g_lblusername, WM_SETFONT, (WPARAM)hfDefault, MAKELPARAM(FALSE, 0));

			g_lblpassword = CreateWindow ("static", "Password:", WS_CHILD | WS_VISIBLE, 10, 167+yoffset, 100, 24, hwnd, (HMENU) IDC_LBLPASSWORD, GetModuleHandle (NULL), NULL);			
			SendMessage(g_lblpassword, WM_SETFONT, (WPARAM)hfDefault, MAKELPARAM(FALSE, 0));

			g_txtusername = CreateWindow ("edit", NULL, WS_CHILD | WS_VISIBLE | WS_BORDER | ES_AUTOHSCROLL, 98, 135+yoffset, 197, 20, hwnd, (HMENU) IDC_TXTUSERNAME, GetModuleHandle (NULL), NULL);			
			SendMessage(g_txtusername, WM_SETFONT, (WPARAM)hfDefault, MAKELPARAM(FALSE, 0));

			g_txtpassword = CreateWindow ("edit", NULL, WS_CHILD | WS_VISIBLE | WS_BORDER | ES_PASSWORD  | ES_AUTOHSCROLL, 98, 165+yoffset, 197, 20, hwnd, (HMENU) IDC_TXTPASSWORD, GetModuleHandle (NULL), NULL);			
			SendMessage(g_txtpassword, WM_SETFONT, (WPARAM)hfDefault, MAKELPARAM(FALSE, 0));

			SetFocus (g_txtusername);

			if (IsWow64() == TRUE) {
				g_deployment.m_bwow64 = true;
			} else {
				g_deployment.m_bwow64 = false;
			}

			// This is the CedeDrive Standalone launcher - it does many things, it checks for the
			// presence of an evd file because this is the most important thing it must do. If found
			// it proceeds to check if an instance of CedeDrive is already running. If an instance
			// of CedeDrive is already running it must inform this already running instance that a new
			// drive must be mounted. It does this using Shared Memory IPC because only one instance
			// of the Virtual Drive SDK can be active on any one computer.


			// Check for presence of local EVD file
			if (g_evd.CheckEVDFile () == true) { // THIS SHOULD BE CHECKING FOR TRUE!
				
				// If an evd file was found we will have a populated SingleDriveInfo structure in the evd
				// handler.
				
				// Start sharing memory, we need cededrive to be able to read the drive info structure
				// in shared memory				
				/*
				if (g_meminfo.StartSharing ("CedeDriveLauncher" , g_maxsharemem) == false) {
					MessageBox (NULL, "Fatal Error: Unable to create shared memory structure.", "CedeDrive Launch Error", MB_OK | MB_ICONEXCLAMATION);
					PostQuitMessage (0);
					return 0;
				}
				*/

				// Share the drive info structure with either an existing instance or a newly running
				// instance
				if (ShareDriveInfo () == false) {
					MessageBox (NULL, "Fatal Error: Unable to share relevant drive information.", "CedeDrive Launch Error", MB_OK | MB_ICONEXCLAMATION);
					PostQuitMessage (0);
					return 0;
				}

				// The next thing to do is check if an instance of CedeDrive is already running
				// if yes we must tell this instance to mount our drive, if not we deploy cededrive
				// to a temp directory, and tell this temporary version to mount our drive.
				if (IsInstanceRunning () == true) {
					// Use Shared Memory IPC and tell this already running instance that our
					// drive needs to be mounted.
					RegisterIPCEvent ("CedeDriveCoreEvent");
					BroadcastIPCEvent (0, 0);
					// We must be told to quit by the core because we won't know when the core
					// is done reading our shared memory info.


				} else {
					bool bOkToLaunchCedeDrive = false;

					// Deploy our own version of cededrive and use shared memory and tell
					// this version to mount our drive, also deploy the service deployment application
					if (g_deployment.DeployApplication () == false) {
						MessageBox (NULL, "Fatal Error: Unable to deploy temporary CedeDrive core.", "CedeDrive Launch Error", MB_OK | MB_ICONEXCLAMATION);
						MessageBox (NULL, g_deployment.m_szLasterror, "Failure reason", MB_OK | MB_ICONEXCLAMATION);
						PostQuitMessage (0);
						return 0;
					}

					// Check if the CedeDrive service is running, if it is not then attempt to deploy it
					if (IsServiceRunning () == false) {
						
						// Before we try to deploy the service, check if we are an admin first - admin rights are needed
						// to deploy the cededrive service
						if (IsUserAdmin () == false) {
							
							// This user is not an admin, so we need to show the password dialog so we can execute
							// a RunAs createprocess
							ShowWindow (hwnd, SW_SHOW);

						} else {
							g_deployment.LaunchServiceDeployment ();
							bOkToLaunchCedeDrive = true;
						}

					} else {
						bOkToLaunchCedeDrive = true;
					}


					if (bOkToLaunchCedeDrive == true) {
						// When the cededrive core launches, it will read the shared info structure we
						// are sharing. When it is done it will tell us to quit, as we are no longer needed.
						if (g_deployment.LaunchApplication () == false) {
							MessageBox (NULL, "Fatal Error: Unable to launch CedeDrive core.", "CedeDrive Launch Error", MB_OK | MB_ICONEXCLAMATION);
							PostQuitMessage (0);
							return 0;
						}
					}

				}

			} else {
				
				ShowMessage ("No EVD present. Now quitting...");
				PostQuitMessage (0);

			}			
		}
		break;		
		case WM_PAINT:
		{
			BITMAP bm;
			PAINTSTRUCT ps;
		
			HDC hdc = BeginPaint (hwnd, &ps);

			HDC hdcMem = CreateCompatibleDC (hdc);
			HBITMAP hbmOld = (HBITMAP) SelectObject (hdcMem, g_hbmBanner);

			GetObject (g_hbmBanner, sizeof (bm), &bm);
			BitBlt (hdc, 0, 0, bm.bmWidth, bm.bmHeight, hdcMem, 0, 0, SRCCOPY);

			SelectObject (hdcMem, hbmOld);
			DeleteDC (hdcMem);
			
			EndPaint (hwnd, &ps);
		}
		break;
		case WM_CLOSE:
			DestroyWindow (hwnd);
			break;
		case WM_DESTROY:
			PostQuitMessage(0);
			break;		
		default:
			return DefWindowProc (hwnd, msg, wParam, lParam);
	}
	return 0;
}

void ParseCommandLine (char *pszCmdline)
{
	//MessageBox (NULL, "Hello", "Info", MB_OK);
	//MessageBox (NULL, pszCmdline, "Command Line", MB_OK);
	char szExtendedAction[SIZE_STRING];
	ZeroMemory (szExtendedAction, SIZE_STRING);
	strncpy_s (szExtendedAction, SIZE_STRING, pszCmdline, 5);

	if (strcmp (szExtendedAction, "/diag") == 0) {
		g_deployment.m_bDiagnosticsmode = true;
	} else {
		g_deployment.m_bDiagnosticsmode = false;
	}
}

int WINAPI WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
	WNDCLASSEX wc;
	HWND hwnd;
	HWND hwnd2;
	MSG Msg;	
	
	if (AuthoriseExecution () == false) {
		MessageBox (NULL, "Please run CedeDrive from the storage device that was originally encrypted. Running CedeDrive from another location is not authorised.", "Unauthorised Execution", MB_OK | MB_ICONEXCLAMATION);
		return 2;
	}

	ParseCommandLine (lpCmdLine);

	// Step1: Registering the window class
	wc.cbSize = sizeof (WNDCLASSEX);
	wc.style = 0;
	wc.lpfnWndProc = WndProc;
	wc.cbClsExtra = 0L;
	wc.cbWndExtra = 0L;
	wc.hInstance = hInstance;
	wc.hIcon = LoadIcon (hInstance, IDI_APPLICATION);
	wc.hCursor = LoadCursor (NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH) CreateSolidBrush (GetSysColor (COLOR_BTNFACE));
	wc.lpszMenuName = NULL;
	wc.lpszClassName = g_szClassName;
	wc.hIconSm = LoadIcon (hInstance, IDI_APPLICATION);

	if (!RegisterClassEx (&wc)) {
		MessageBox (NULL, "Window Registration Failed!", "Error!", MB_ICONEXCLAMATION | MB_OK);
		return 0;
	}

	// Step2: Creating the window
	hwnd = CreateWindowEx (WS_EX_TOOLWINDOW, g_szClassName, "CedeDrive Launch", 
					WS_OVERLAPPEDWINDOW,  300, 300, 327, 310,
					NULL, NULL, hInstance, NULL);

	if (hwnd == NULL) {
		MessageBox (NULL, "Window creation failed!", "Error!", MB_ICONEXCLAMATION | MB_OK);
		return 0;
	}

	//ShowWindow (hwnd, nCmdShow);
	UpdateWindow (hwnd);

	// Step3: The message loop
	
	while (GetMessage (&Msg, NULL, 0, 0) > 0) {
		TranslateMessage (&Msg);
		DispatchMessage (&Msg);
	}
	return Msg.wParam;
}
