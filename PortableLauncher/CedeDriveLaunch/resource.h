#define APPICON			100
#define	OS64BIT			200
#define	OS32BIT			201

// Internal buffer sizes
#define SIZE_NAME			64
#define SIZE_STRING			1024
#define SIZE_LARGESTRING	10000
#define SIZE_INTEGER		32

#define IDC_STATIC_MAIN		400
#define IDB_BANNER				401

#define IDC_TXTPASSWORD 900
#define IDC_TXTUSERNAME 901
#define IDC_LBLUSERNAME 902
#define IDC_LBLPASSWORD 903
#define IDC_LBLTITLE	904
#define IDC_BTNOK		905
#define IDC_BTNCANCEL	906

#define BIN_CEDEDRIVE32		500
#define BIN_SERVICEDEPLOYMENT	502
