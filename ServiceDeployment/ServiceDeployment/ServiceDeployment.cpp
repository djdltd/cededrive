#include <windows.h>
#include <stdio.h>
#include <io.h>
#include <direct.h>
//#include "SingleDriveInfo.h"
//#include "DynList.h"
#include "MemoryBuffer.h"
//#include "EvdHandler.h"
#include "LogWriter.h"
#include "DeploymentHandler.h"
#include "resource.h"

// Global variables
const char g_szClassName[] = "CedeDriveServiceDeploymentWindowClass";
HWND g_statichwnd;
DeploymentHandler g_deployment;
bool g_bremoveservice = false;
LogWriter g_log;

typedef BOOL (WINAPI *LPFN_ISWOW64PROCESS) (HANDLE, PBOOL);
LPFN_ISWOW64PROCESS fnIsWow64Process = (LPFN_ISWOW64PROCESS)GetProcAddress(GetModuleHandle("kernel32"),"IsWow64Process");
 
// Implementation
BOOL IsWow64()
{
    BOOL bIsWow64 = FALSE;
 
    if (NULL != fnIsWow64Process)
    {
        if (!fnIsWow64Process(GetCurrentProcess(),&bIsWow64))
        {
            // handle error
        }
    }
    return bIsWow64;
}


// Step4: The Windows Procedure - When windows calls us!

LRESULT CALLBACK WndProc (HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam) {
		
	switch (msg) {
		
		case WM_CREATE:
		{
			g_log.Initialise ();
			g_deployment.SetLogWriter (&g_log);

			HFONT hfDefault;

			// Create the main label control
			g_statichwnd = CreateWindow ("static", "This is the CedeDrive service deployment application. \n\nThe CedeDrive management service is being deployed.", WS_CHILD | WS_VISIBLE, 10, 20, 270, 200, hwnd, (HMENU) IDC_STATIC_MAIN, GetModuleHandle (NULL), NULL);
			hfDefault = (HFONT) GetStockObject(DEFAULT_GUI_FONT);
			SendMessage (g_statichwnd, WM_SETFONT, (WPARAM)hfDefault, MAKELPARAM (FALSE, 0));

			
			if (IsWow64() == TRUE) {
				g_deployment.m_bwow64 = true;
			} else {
				g_deployment.m_bwow64 = false;
			}
		
			if (g_bremoveservice == true) {
				g_log.Appendtolog ("Installer is in removal mode.");
				g_deployment.Remove ();
			} else {
				g_log.Appendtolog ("Installer is in deployment mode.");
				g_deployment.Install ();
			}

			PostQuitMessage (0);

		}
		break;		
		case WM_CLOSE:
			DestroyWindow (hwnd);
			break;
		case WM_DESTROY:
			PostQuitMessage(0);
			break;		
		default:
			return DefWindowProc (hwnd, msg, wParam, lParam);
	}
	return 0;
}

int WINAPI WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
	WNDCLASSEX wc;
	HWND hwnd;
	MSG Msg;	
	
	char szCommand[SIZE_STRING];
	ZeroMemory (szCommand, SIZE_STRING);
	strncpy_s (szCommand, SIZE_STRING, lpCmdLine, 2);

	bool bUsingcommand = false;

	if (strcmp (szCommand, "/d") == 0) {
		// Delete the service
		bUsingcommand = true;
		g_bremoveservice = true;
	} else {
		g_bremoveservice = false;
	}

	// Step1: Registering the window class
	wc.cbSize = sizeof (WNDCLASSEX);
	wc.style = 0;
	wc.lpfnWndProc = WndProc;
	wc.cbClsExtra = 0L;
	wc.cbWndExtra = 0L;
	wc.hInstance = hInstance;
	wc.hIcon = LoadIcon (hInstance, IDI_APPLICATION);
	wc.hCursor = LoadCursor (NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH) CreateSolidBrush (GetSysColor (COLOR_BTNFACE));
	wc.lpszMenuName = NULL;
	wc.lpszClassName = g_szClassName;
	wc.hIconSm = LoadIcon (hInstance, IDI_APPLICATION);

	if (!RegisterClassEx (&wc)) {
		MessageBox (NULL, "Window Registration Failed!", "Error!", MB_ICONEXCLAMATION | MB_OK);
		return 0;
	}

	// Step2: Creating the window
	hwnd = CreateWindowEx (WS_EX_TOOLWINDOW, g_szClassName, "CedeDrive Service Deployment", 
					WS_OVERLAPPEDWINDOW, 300, 300, 444, 150,
					NULL, NULL, hInstance, NULL);

	if (hwnd == NULL) {
		MessageBox (NULL, "Window creation failed!", "Error!", MB_ICONEXCLAMATION | MB_OK);
		return 0;
	}

	//ShowWindow (hwnd, nCmdShow);
	UpdateWindow (hwnd);

	// Step3: The message loop
	
	while (GetMessage (&Msg, NULL, 0, 0) > 0) {
		TranslateMessage (&Msg);
		DispatchMessage (&Msg);
	}
	return 0;
}
