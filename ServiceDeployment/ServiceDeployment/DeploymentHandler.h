#pragma once
#include <windows.h>
#include <malloc.h>
#include <stdio.h>
#include <io.h>
#include <direct.h>
#include "resource.h"
#include "MemoryBuffer.h"
#include "LogWriter.h"

#define SIZE_STRING		1024

class DeploymentHandler {
	
	public:
		DeploymentHandler ();
		~DeploymentHandler ();

		bool LaunchService (bool bInstall);
		bool PrepareApplicationPath (bool bCreate);
		bool Install();
		bool Remove ();
		bool FileExists (char *FileName);
		bool DeployService ();
		void SetLogWriter (LogWriter *plog);

		bool m_bwow64;
		char m_szLasterror[SIZE_STRING];

	private:
		char m_szApplicationpath[SIZE_STRING];
		char m_szExepath64[SIZE_STRING];
		char m_szExepath32[SIZE_STRING];
		
		void SetLastErr (char *szErrortext);
		LogWriter *log;
};
