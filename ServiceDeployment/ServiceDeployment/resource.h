#define APPICON			100
#define	OS64BIT			200
#define	OS32BIT			201

// Internal buffer sizes
#define SIZE_NAME			64
#define SIZE_STRING			1024
#define SIZE_LARGESTRING	10000
#define SIZE_INTEGER		32

#define IDC_STATIC_MAIN		400
#define BIN_SERVICE32			500
#define BIN_SERVICE64		501
#define BIN_SDKDLL32			502
#define BIN_SDKDLL64			503
#define BIN_COREDLL32		504
#define BIN_COREDLL64		505
