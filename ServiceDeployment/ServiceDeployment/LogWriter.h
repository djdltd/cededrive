#pragma once
#include <windows.h>
#include <malloc.h>
#include <stdio.h>
#include <io.h>
#include <direct.h>
#include "resource.h"
#include "MemoryBuffer.h"

class LogWriter {
	
	public:
		LogWriter ();
		~LogWriter ();

		void Appendtolog (char *szLogtext, char *szLogtext2, char *szLogtext3);
		void Appendtolog (char *szLogtext, int iValue, char *szLogtext2);
		void Appendtolog (char *szLogtext, char *szLogtext2);
		void Appendtolog (char *szLogtext, int iValue);
		void Appendtolog (char *szLogtext);
		void Initialise ();

	private:

		void CloseOutFile();
		void WriteFileInt (int iValue);
		void WriteFileLine(LPCSTR strFileline);
		int OpenAppendFile (LPCSTR strFilename);
		bool PrepareLogPath ();

		BOOL m_bFileOpen;
		FILE *m_stream;
		char m_szLogpathname[SIZE_STRING];
		char m_szTemppath[SIZE_STRING];
		bool m_bInitialised;
};
