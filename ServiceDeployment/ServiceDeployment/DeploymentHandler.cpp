#include "DeploymentHandler.h"

DeploymentHandler::DeploymentHandler ()
{
}

DeploymentHandler::~DeploymentHandler ()
{
}

void DeploymentHandler::SetLogWriter (LogWriter *plog) {
	log = plog;
}

bool DeploymentHandler::FileExists (char *FileName)
{
	// Check if the file exists		
	struct _finddata_t c_file;
	long hFile;

	// Build the list of files in the source path
	if( (hFile = _findfirst(FileName, &c_file )) == -1L ) {
		return false;
	} else {
		return true;
	}

	return false;
}

bool DeploymentHandler::LaunchService (bool bInstall)
{
	// This function will install the service and start it
	// ready for the cededrive gui to start talking to it

	char szExepath[SIZE_STRING];
	ZeroMemory (szExepath, SIZE_STRING);
	
    STARTUPINFO si;
    PROCESS_INFORMATION pi;

    ZeroMemory( &si, sizeof(si) );
    si.cb = sizeof(si);
    ZeroMemory( &pi, sizeof(pi) );

	if (m_bwow64 == true) {
		log->Appendtolog ("LaunchService: 64-bit OS detected.");
		strcpy_s (szExepath, SIZE_STRING, m_szExepath64);
	} else {
		log->Appendtolog ("LaunchService: 32-bit OS detected.");
		strcpy_s (szExepath, SIZE_STRING, m_szExepath32);
	}
	
	if (bInstall == true) {
		// Install the service
		strcat_s (szExepath, SIZE_STRING, " /i");
	} else {
		// Remove the service
		strcat_s (szExepath, SIZE_STRING, " /d");
	}

	log->Appendtolog ("LaunchService: CreateProcess command line: ", szExepath);

	if( !CreateProcess(NULL,   // No module name (use command line)
		szExepath,        // Command line
        NULL,           // Process handle not inheritable
        NULL,           // Thread handle not inheritable
        FALSE,          // Set handle inheritance to FALSE
        0,              // No creation flags
        NULL,           // Use parent's environment block
        NULL,           // Use parent's starting directory 
        &si,            // Pointer to STARTUPINFO structure
        &pi )           // Pointer to PROCESS_INFORMATION structure
    ) 
    {
		log->Appendtolog ("LaunchService: Create process failed.");
		return false;
    }

	log->Appendtolog ("LaunchService: Create process succeeded.");
	return true;
}

bool DeploymentHandler::Install()
{
	char szExepath[SIZE_STRING];
	ZeroMemory (szExepath, SIZE_STRING);

	if (PrepareApplicationPath (false) == false) {
		log->Appendtolog ("Install: PrepareApplicationPath failed, aborting installation.");
		return false;
	}

	// Check if the service already exists on this system	
	strcpy_s (szExepath, SIZE_STRING, m_szApplicationpath);
	strcat_s (szExepath, SIZE_STRING, "\\CedeDriveService.exe");
	log->Appendtolog ("Install: Application path is ", szExepath);
	

	// If the service already exists, then upgrade it otherwise install it
	if (FileExists (szExepath) == true) {
		log->Appendtolog ("Application path already exists, now operating in Upgrade mode.");
		Remove ();
		DeployService ();
		LaunchService (true);
	} else {
		log->Appendtolog ("CedeDrive service not found on this system. Now operating in Install mode.");
		DeployService ();
		LaunchService (true);
	}

	return true;
}

bool DeploymentHandler::Remove ()
{
	char szExepath[SIZE_STRING];
	char szSdkpath[SIZE_STRING];
	char szCorepath[SIZE_STRING];

	ZeroMemory (szExepath, SIZE_STRING);
	ZeroMemory (szSdkpath, SIZE_STRING);
	ZeroMemory (szCorepath, SIZE_STRING);

	if (PrepareApplicationPath (false) == false) {
		log->Appendtolog ("PrepareApplicationPath failed, aborting removal.");
		return false;
	}

	// Check if the service already exists on this system	
	strcpy_s (szExepath, SIZE_STRING, m_szApplicationpath);
	strcat_s (szExepath, SIZE_STRING, "\\CedeDriveService.exe");
	log->Appendtolog ("Remove: Exepath is ", szExepath);

	strcpy_s (szSdkpath, SIZE_STRING, m_szApplicationpath);
	strcat_s (szSdkpath, SIZE_STRING, "\\VDSDKDll.dll");
	log->Appendtolog ("Remove: SDK path is ", szSdkpath);

	strcpy_s (szCorepath, SIZE_STRING, m_szApplicationpath);
	strcat_s (szCorepath, SIZE_STRING, "\\VDCore.dll");
	log->Appendtolog ("Remove: Core path is ", szCorepath);

	if (m_bwow64 == true) {
		ZeroMemory (m_szExepath64, SIZE_STRING);
		strcpy_s (m_szExepath64, SIZE_STRING, szExepath);
		log->Appendtolog ("DeployService: 64-bit Executable path is ", m_szExepath64);
	} else {
		ZeroMemory (m_szExepath32, SIZE_STRING);
		strcpy_s (m_szExepath32, SIZE_STRING, szExepath);
		log->Appendtolog ("DeployService: 32-bit Executable path is ", m_szExepath32);
	}
	

	LaunchService (false); // Remove the service
	DWORD dwErr;

	if (DeleteFile (szExepath) == FALSE) {
		dwErr = GetLastError ();
		log->Appendtolog ("Remove: Unable to delete: ErrCode: ", dwErr,  szExepath);
	} else {
		log->Appendtolog ("Remove: File deleted successfully: ", szExepath);
	}
	
	if (DeleteFile (szSdkpath) == FALSE) {
		dwErr = GetLastError ();
		log->Appendtolog ("Remove: Unable to delete: ErrCode: ", dwErr, szSdkpath);
	} else {
		log->Appendtolog ("Remove: File deleted successfully: ", szSdkpath);
	}

	if (DeleteFile (szCorepath) == FALSE) {
		dwErr = GetLastError ();
		log->Appendtolog ("Remove: Unable to delete: ErrCode: ", dwErr, szCorepath);
	} else {
		log->Appendtolog ("Remove: File deleted successfully: ", szCorepath);
	}
	
	
	return true;
}

bool DeploymentHandler::PrepareApplicationPath (bool bCreate)
{
	// Create the directories needed by the application

	int iRes = 0;
	char szProgramFiles[SIZE_STRING];
	char szCompany[SIZE_STRING];
	char szProgram[SIZE_STRING];

	ZeroMemory (szProgramFiles, SIZE_STRING);
	ZeroMemory (szCompany, SIZE_STRING);
	ZeroMemory (szProgram, SIZE_STRING);

	if (GetEnvironmentVariable ("ProgramFiles", szProgramFiles, SIZE_STRING) == 0) {
		return false;
	}

	strcpy_s (szCompany, SIZE_STRING, szProgramFiles);
	strcat_s (szCompany, SIZE_STRING, "\\CedeSoft");
	if (bCreate == true) {
		_mkdir (szCompany);
	}
	

	strcpy_s (szProgram, SIZE_STRING, szCompany);
	strcat_s (szProgram, SIZE_STRING, "\\CedeDriveService");
	if (bCreate == true) {
		_mkdir (szProgram);
	}

	// Set our member variable which is used by the other functions
	ZeroMemory (m_szApplicationpath, SIZE_STRING);
	strcpy_s (m_szApplicationpath, SIZE_STRING, szProgram);
	
	return true;
}


bool DeploymentHandler::DeployService ()
{
	// Save the executables (depending on 32-bit or 64-bit OS) in the prepared directories

	MemoryBuffer mem;
	char szExepath[SIZE_STRING];
	char szSdkpath[SIZE_STRING];
	char szCorepath[SIZE_STRING];

	ZeroMemory (szExepath, SIZE_STRING);
	ZeroMemory (szSdkpath, SIZE_STRING);
	ZeroMemory (szCorepath, SIZE_STRING);

	//m_szExepath64
	if (m_bwow64 == true) {
		// If this is a 64 bit OS
		if (PrepareApplicationPath (true) == false) {
			log->Appendtolog ("DeployService: Unable to prepare application path");
			SetLastErr ("Unable to prepare application path");
			return false;
		}

		strcpy_s (szExepath, SIZE_STRING, m_szApplicationpath);
		strcat_s (szExepath, SIZE_STRING, "\\CedeDriveService.exe");
		log->Appendtolog ("DeployService: Executable path is ", szExepath);

		ZeroMemory (m_szExepath64, SIZE_STRING);
		strcpy_s (m_szExepath64, SIZE_STRING, szExepath);
		log->Appendtolog ("DeployService: 64-bit Executable path is ", m_szExepath64);

		strcpy_s (szSdkpath, SIZE_STRING, m_szApplicationpath);
		strcat_s (szSdkpath, SIZE_STRING, "\\VDSDKDll.dll");
		log->Appendtolog ("DeployService: 64-bit SDK path is ", szSdkpath);

		strcpy_s (szCorepath, SIZE_STRING, m_szApplicationpath);
		strcat_s (szCorepath, SIZE_STRING, "\\VDCore.dll");
		log->Appendtolog ("DeployService: 64-bit Core path is ", szCorepath);


		if (mem.ReadFromResource (MAKEINTRESOURCE(BIN_SERVICE64)) != 0) {
			log->Appendtolog ("DeployService:  Unable to read 64bit CedeDriveService.exe resource");
			SetLastErr ("Unable to read 64bit CedeDriveService.exe resource");
			return false;
		}

		if (mem.SaveToFile (szExepath) == false) {
			log->Appendtolog ("DeployService:  Unable to save 64bit CedeDriveService.exe resource");
			SetLastErr ("Unable to save 64bit CedeDriveService.exe resource");
			return false;
		}

		mem.Clear ();

		if (mem.ReadFromResource (MAKEINTRESOURCE(BIN_SDKDLL64)) != 0) {
			log->Appendtolog ("DeployService:  Unable to read 64bit VDSDKDll.dll resource");
			SetLastErr ("Unable to read 64bit VDSDKDll.dll resource");
			return false;
		}

		if (mem.SaveToFile (szSdkpath) == false) {
			log->Appendtolog ("DeployService:  Unable to save 64bit VDSDKDll.dll resource");
			SetLastErr ("Unable to save 64bit VDSDKDll.dll resource");
			return false;
		}

		mem.Clear ();

		if (mem.ReadFromResource (MAKEINTRESOURCE(BIN_COREDLL64)) != 0) {
			log->Appendtolog ("DeployService:  Unable to read 64bit VDCore.dll resource");
			SetLastErr ("Unable to read 64bit VDCore.dll resource");
			return false;
		}

		if (mem.SaveToFile (szCorepath) == false) {
			log->Appendtolog ("DeployService:  Unable to save 64bit VDCore.dll resource");
			SetLastErr ("Unable to save 64bit VDCore.dll resource");
			return false;
		}
		
		log->Appendtolog ("DeployService: 64-bit Deployment successful.");
		return true;
	} else {
		// If this is a 32 bit OS
		if (PrepareApplicationPath (true) == false) {
			log->Appendtolog ("DeployService:  Unable to prepare application path");
			SetLastErr ("Unable to prepare application path");
			return false;
		}

		strcpy_s (szExepath, SIZE_STRING, m_szApplicationpath);
		strcat_s (szExepath, SIZE_STRING, "\\CedeDriveService.exe");
		log->Appendtolog ("DeployService:  32 bit szExepath is ", szExepath);

		ZeroMemory (m_szExepath32, SIZE_STRING);
		strcpy_s (m_szExepath32, SIZE_STRING, szExepath);
		log->Appendtolog ("DeployService:  32 bit szExepath is ", m_szExepath32);

		strcpy_s (szSdkpath, SIZE_STRING, m_szApplicationpath);
		strcat_s (szSdkpath, SIZE_STRING, "\\VDSDKDll.dll");
		log->Appendtolog ("DeployService:  32 bit Sdk path is ", szSdkpath);

		strcpy_s (szCorepath, SIZE_STRING, m_szApplicationpath);
		strcat_s (szCorepath, SIZE_STRING, "\\VDCore.dll");
		log->Appendtolog ("DeployService:  32 bit Corepath is ", szCorepath);


		if (mem.ReadFromResource (MAKEINTRESOURCE(BIN_SERVICE32)) != 0) {
			log->Appendtolog ("DeployService:  Unable to read 32bit CedeDrive.exe resource");
			SetLastErr ("Unable to read 32bit CedeDrive.exe resource");
			return false;
		}

		if (mem.SaveToFile (szExepath) == false) {
			log->Appendtolog ("DeployService:  Unable to save 32bit CedeDrive.exe resource");
			SetLastErr ("Unable to save 32bit CedeDrive.exe resource");
			return false;
		}

		mem.Clear ();

		if (mem.ReadFromResource (MAKEINTRESOURCE(BIN_SDKDLL32)) != 0) {
			log->Appendtolog ("DeployService:  Unable to read 32bit VDSDKDll.dll resource");
			SetLastErr ("Unable to read 32bit VDSDKDll.dll resource");
			return false;
		}

		if (mem.SaveToFile (szSdkpath) == false) {
			log->Appendtolog ("DeployService:  Unable to save 32bit VDSDKDll.dll resource");
			SetLastErr ("Unable to save 32bit VDSDKDll.dll resource");
			return false;
		}

		mem.Clear ();

		if (mem.ReadFromResource (MAKEINTRESOURCE(BIN_COREDLL32)) != 0) {
			log->Appendtolog ("DeployService:  Unable to read 32bit VDCore.dll resource");
			SetLastErr ("Unable to read 32bit VDCore.dll resource");
			return false;
		}

		if (mem.SaveToFile (szCorepath) == false) {
			log->Appendtolog ("DeployService:  Unable to save 32bit VDCore.dll resource");
			SetLastErr ("Unable to save 32bit VDCore.dll resource");
			return false;
		}

		log->Appendtolog ("DeployService: 32-bit Deployment successful.");
		return true;
	}
}


void DeploymentHandler::SetLastErr (char *szErrortext)
{
	ZeroMemory (m_szLasterror, SIZE_STRING);
	strcpy_s (m_szLasterror, SIZE_STRING, szErrortext);
}