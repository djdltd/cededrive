#include "LogWriter.h"

LogWriter::LogWriter ()
{
	m_bFileOpen = FALSE;
	m_bInitialised = false;
}

LogWriter::~LogWriter ()
{
}

void LogWriter::Initialise () {

	if (PrepareLogPath () == false) {
		return;
	}

	ZeroMemory (m_szLogpathname, SIZE_STRING);

	strcat_s (m_szTemppath, SIZE_STRING, "\\ServiceDeployment.log");
	strcpy_s (m_szLogpathname, SIZE_STRING, m_szTemppath);
	m_bInitialised = true;
}

bool LogWriter::PrepareLogPath ()
{
	// Create the directories needed by the application

	int iRes = 0;
	char szTemp[SIZE_STRING];
	char szCompany[SIZE_STRING];
	char szProgram[SIZE_STRING];

	ZeroMemory (szTemp, SIZE_STRING);
	ZeroMemory (szCompany, SIZE_STRING);
	ZeroMemory (szProgram, SIZE_STRING);

	if (GetEnvironmentVariable ("TEMP", szTemp, SIZE_STRING) == 0) {
		return false;
	}

	strcpy_s (szCompany, SIZE_STRING, szTemp);
	strcat_s (szCompany, SIZE_STRING, "\\CedeSoft");
	_mkdir (szCompany);

	

	strcpy_s (szProgram, SIZE_STRING, szCompany);
	strcat_s (szProgram, SIZE_STRING, "\\CedeDriveServiceDeployment");
	_mkdir (szProgram);
	

	// Set our member variable which is used by the other functions
	ZeroMemory (m_szTemppath, SIZE_STRING);
	strcpy_s (m_szTemppath, SIZE_STRING, szProgram);
	
	return true;
}

int LogWriter::OpenAppendFile (LPCSTR strFilename)
{
	if( (m_stream = fopen (strFilename, "a+t" )) != NULL ) {
		m_bFileOpen = TRUE;
		return 0;
	} else {
		return -1;
	}
}

void LogWriter::WriteFileLine(LPCSTR strFileline)
{

	if (m_bFileOpen == TRUE) {
		//strFileline += "\n";
		fwrite(strFileline, 1, strlen(strFileline), m_stream );
	} else {
		//MessageBox (NULL, "File is not open", "LiveDesktop", MB_ICONEXCLAMATION | MB_OK);
	}
}

void LogWriter::WriteFileInt (int iValue)
{
	char szMsg[32];
	
	ZeroMemory (szMsg, 32);
	sprintf_s (szMsg, 32, "%d", iValue);
	
	if (m_bFileOpen == TRUE) {
		//strFileline += "\n";
		fwrite(szMsg, 1, strlen(szMsg), m_stream );
	} else {
		//MessageBox (NULL, "File is not open", "LiveDesktop", MB_ICONEXCLAMATION | MB_OK);
	}
}

void LogWriter::CloseOutFile()
{
	if (m_bFileOpen == TRUE) {
		//if (fclose(stream) !=0 ) {
		//	MessageBox (NULL, "Error closing stream",NULL, NULL);
		//}
		_fcloseall ();
		m_bFileOpen = FALSE;
	}
}

void LogWriter::Appendtolog (char *szLogtext)
{
	if (m_bInitialised == false) {
		return;
	}

	SYSTEMTIME systemtime;
	GetLocalTime (&systemtime);
		
	int iDay = systemtime.wDay;
	int iMonth = systemtime.wMonth;
	int iYear = systemtime.wYear;
	int iHour = systemtime.wHour;
	int iMinute = systemtime.wMinute;
	int iSecond = systemtime.wSecond;
	
	if (OpenAppendFile (m_szLogpathname) == 0) {
		WriteFileInt (iDay);
		WriteFileLine("/");
		WriteFileInt (iMonth);
		WriteFileLine("/");
		WriteFileInt (iYear);
		WriteFileLine(" ");
		WriteFileInt (iHour);
		WriteFileLine(":");
		WriteFileInt (iMinute);
		WriteFileLine(":");
		WriteFileInt (iSecond);
		WriteFileLine(": ");
		WriteFileLine (szLogtext);
		WriteFileLine(".\n");
		CloseOutFile ();
	}
}

void LogWriter::Appendtolog (char *szLogtext, int iValue)
{
	if (m_bInitialised == false) {
		return;
	}

	SYSTEMTIME systemtime;
	GetLocalTime (&systemtime);
		
	int iDay = systemtime.wDay;
	int iMonth = systemtime.wMonth;
	int iYear = systemtime.wYear;
	int iHour = systemtime.wHour;
	int iMinute = systemtime.wMinute;
	int iSecond = systemtime.wSecond;
	
	if (OpenAppendFile (m_szLogpathname) == 0) {
		WriteFileInt (iDay);
		WriteFileLine("/");
		WriteFileInt (iMonth);
		WriteFileLine("/");
		WriteFileInt (iYear);
		WriteFileLine(" ");
		WriteFileInt (iHour);
		WriteFileLine(":");
		WriteFileInt (iMinute);
		WriteFileLine(":");
		WriteFileInt (iSecond);
		WriteFileLine(": ");
		WriteFileLine (szLogtext);
		WriteFileLine(" ");
		WriteFileInt (iValue);
		WriteFileLine(".\n");
		CloseOutFile ();
	}
}

void LogWriter::Appendtolog (char *szLogtext, char *szLogtext2)
{
	if (m_bInitialised == false) {
		return;
	}

	SYSTEMTIME systemtime;
	GetLocalTime (&systemtime);
		
	int iDay = systemtime.wDay;
	int iMonth = systemtime.wMonth;
	int iYear = systemtime.wYear;
	int iHour = systemtime.wHour;
	int iMinute = systemtime.wMinute;
	int iSecond = systemtime.wSecond;
	
	if (OpenAppendFile (m_szLogpathname) == 0) {
		WriteFileInt (iDay);
		WriteFileLine("/");
		WriteFileInt (iMonth);
		WriteFileLine("/");
		WriteFileInt (iYear);
		WriteFileLine(" ");
		WriteFileInt (iHour);
		WriteFileLine(":");
		WriteFileInt (iMinute);
		WriteFileLine(":");
		WriteFileInt (iSecond);
		WriteFileLine(": ");
		WriteFileLine (szLogtext);
		WriteFileLine(" ");
		WriteFileLine (szLogtext2);
		WriteFileLine(".\n");
		CloseOutFile ();
	}
}

void LogWriter::Appendtolog (char *szLogtext, int iValue, char *szLogtext2)
{
	if (m_bInitialised == false) {
		return;
	}
	SYSTEMTIME systemtime;
	GetLocalTime (&systemtime);
		
	int iDay = systemtime.wDay;
	int iMonth = systemtime.wMonth;
	int iYear = systemtime.wYear;
	int iHour = systemtime.wHour;
	int iMinute = systemtime.wMinute;
	int iSecond = systemtime.wSecond;
	
	if (OpenAppendFile (m_szLogpathname) == 0) {
		WriteFileInt (iDay);
		WriteFileLine("/");
		WriteFileInt (iMonth);
		WriteFileLine("/");
		WriteFileInt (iYear);
		WriteFileLine(" ");
		WriteFileInt (iHour);
		WriteFileLine(":");
		WriteFileInt (iMinute);
		WriteFileLine(":");
		WriteFileInt (iSecond);
		WriteFileLine(": ");
		WriteFileLine (szLogtext);
		WriteFileLine(" ");
		WriteFileInt (iValue);
		WriteFileLine(" ");
		WriteFileLine (szLogtext2);
		WriteFileLine(".\n");
		CloseOutFile ();
	}
}

void LogWriter::Appendtolog (char *szLogtext, char *szLogtext2, char *szLogtext3)
{
	if (m_bInitialised == false) {
		return;
	}
	SYSTEMTIME systemtime;
	GetLocalTime (&systemtime);
		
	int iDay = systemtime.wDay;
	int iMonth = systemtime.wMonth;
	int iYear = systemtime.wYear;
	int iHour = systemtime.wHour;
	int iMinute = systemtime.wMinute;
	int iSecond = systemtime.wSecond;
	
	if (OpenAppendFile (m_szLogpathname) == 0) {
		WriteFileInt (iDay);
		WriteFileLine("/");
		WriteFileInt (iMonth);
		WriteFileLine("/");
		WriteFileInt (iYear);
		WriteFileLine(" ");
		WriteFileInt (iHour);
		WriteFileLine(":");
		WriteFileInt (iMinute);
		WriteFileLine(":");
		WriteFileInt (iSecond);
		WriteFileLine(": ");
		WriteFileLine (szLogtext);
		WriteFileLine(" ");
		WriteFileLine (szLogtext2);
		WriteFileLine(" ");
		WriteFileLine (szLogtext3);
		WriteFileLine(".\n");
		CloseOutFile ();
	}
}
