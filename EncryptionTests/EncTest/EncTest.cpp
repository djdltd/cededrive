#include <windows.h>
#include "MemoryBuffer.h"
#include "StandardEncryption.h"

#define SIZE_STRING		1024

StandardEncryption m_enc;
MemoryBuffer m_memCipher;
char m_szPassword[SIZE_STRING];

int main(int argc, char* argv[])
{		

	strcpy_s (m_szPassword, SIZE_STRING, "penhorse34");
	m_memCipher.SetSize (1024*1024);

	if (m_enc.EncryptBuffer (&m_memCipher, m_szPassword, true) == true) {
		printf ("Encryption Successful - key has been created\n");
		
	} else {
		printf ("Encryption Failed!!!\n");
		
	}


}
