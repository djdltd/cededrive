#include <windows.h>
#include <stdio.h>
#include <vector>
#include <conio.h>
#include <iostream>

#include "VDSDKDll.h"
#include "MemoryBuffer.h"
#include "StandardEncryption.h"

/*

Last Date worked on: 18th April 2009 - Saturday
Encryption is fully working, and file spanning is working however now I am experiencing an issue when I copy files to the
encrypted drive and vista aborts with an error "Thread IO exited, could not complete operation". I've found this is because the WriteFile
command in the OnWriteCallback is taking too long to complete (over 10 seconds), and then the VDSDK terminates the operation so to not tie
up the OS. Must find out why the WriteFile command is taking so long sometimes, perhaps a few experiments with different flag settings.

Once this is sorted we are ready to move onto a fully fledged Win32 UI for it and integrating this into CedeCrypt portable.

*/

using namespace std;

#define DISK_SIZE 3000
#define SPLIT_SIZE 3000
#define MAXSPANNEDFILES	32000

StandardEncryption g_enc;
HANDLE g_hFile = INVALID_HANDLE_VALUE;

HANDLE g_hSpanfile[MAXSPANNEDFILES];

DRIVE_HANDLE g_hDrive = INVALID_DRIVE_HANDLE;

char g_szPassword[SIZE_STRING];
MemoryBuffer g_memCipher;

char g_szDrive[SIZE_NAME];
char g_szDrivePath[SIZE_NAME];

int CreateVirtualDisk();

int main(int argc, char* argv[])
{

	/*
	// Some Xor'ing tests for using AES as a stream cipher
	BYTE b1 = 137; // aes cipher byte
	BYTE b2 = 98; // plaintext

	BYTE b3; // xored result
	BYTE b4; // deciphered ciphertext

	b3 = b1 ^ b2;
	
	b4 = b1 ^ b3;

	printf ("B3: %i\n\n", b3);
	printf ("B4: %i\n\n", b4);
	*/


	//printf ("mod: %i", 25 % 23);


	//unsigned long long res = 899 / 300;

	//printf ("res: %I64u ", res);

	for (unsigned long l=0;l<MAXSPANNEDFILES;l++) {
		g_hSpanfile[l] = INVALID_HANDLE_VALUE;
	}
	

	//printf ("Sizeof Handle: %i", sizeof (HANDLE));

	//return 0;
	
	ZeroMemory (g_szPassword, SIZE_STRING);
	ZeroMemory (g_szDrive, SIZE_NAME);
	ZeroMemory (g_szDrivePath, SIZE_NAME);

	printf ("Virtual Encrypted Disk Password: ");

	cin >> g_szPassword;

	printf ("Drive Letter: ");
	cin >> g_szDrive;

	strcpy_s (g_szDrivePath, SIZE_NAME, g_szDrive);
	strcat_s (g_szDrivePath, SIZE_NAME, ":\\");

	
	g_memCipher.SetSize (1024*1024);
	if (g_enc.EncryptBuffer (&g_memCipher, g_szPassword, true) == true) {
		printf ("Encryption Successful\n");
	} else {				
		printf ("Encryption Failed!!!\n");
	}
	

	//printf ("You entered: %s\n\n", szString);

	//strcpy_s (szString, SIZE_STRING, "Hello this is a piece of plaintext data");

	//MemoryBuffer memTest;
	//memTest.SetSize (100000);
	
	//memTest.Write ((char *) szString, 0, strlen (szString));

	//if (g_enc.EncryptBuffer (&memTest, "password124", true) == true) {
	//	printf ("Encryption Successful\n");
	//	memTest.SaveToFile ("C:\\Temp\\TempEnc\\EncZero5.dat");
	//} else {
	//	printf ("Encryption Failed\n");
	//}
	

	//return 0;
    if(!InitializeVDSDK())
    {
        printf("Error: Can not initialize VDSDK\n");
        return 1;
    }

    // Activate VDSDK.
    // First parameter is activation key shipped with commercial license
    // In demo version this method always returns TRUE

    if(!ActivateVDSDK("+Gdd9LfTihfFs/DLL4XbkCDyBL3GQYD0IPIEvcZBgPQg8gS9xkGA9CDyBL3GQYD0IPIEvcZBgPQg8gS9xkGA9IJzlgZrh9vmbXLrR0L4CIUg8gS9xkGA9CDyBL3GQYD0IPIEvcZBgPQg8gS9xkGA9CDyBL3GQYD0IPIEvcZBgPSGjHphJVmb58EgDZk02prmIPIEvcZBgPQg8gS9xkGA9CDyBL3GQYD0IPIEvcZBgPQg8gS9xkGA9CDyBL3GQYD0uSfV5IlEc2JdnyPjffC4uliKy/LeIbBqIPIEvcZBgPQg8gS9xkGA9CDyBL3GQYD0IPIEvcZBgPSUAEPWwBV5b2X+fyEzUs9IfsMRLMlwaiJ2naPy83snBb7FBpDieoCyE+s0X/3LmL5Dv7ZiJKC1G5tvTM7c31P/oQyhIFj9TPvIWKh4YB2mT055xzRhjnYSyZmXSQvBpP/2JzuRv9hYDO0XDumWdgXapQnJxeJUifzkA7/yrCuSxLRqyRxeHyj/ZkQBMvXXDZKrF7OYNfGL7KMKBQmPYA7Fuh+JZskfc8O8NHQ10aVPw3NiEkz5XHjtotbR6eq+mzRsxbd1BF1tGlDQYQYKoSuNblh2Lj89w83qiZKfQcAAg83Km6R05NI1z3nG3NqLwLsqZX+kFyqdmN5Whdd2R2ahw3kr1wA/EdyUfsZ3y63pARaqydMrI9ui628ABC27CE572154FFEE62D6934BD756"))	
	{
        printf("ERROR: can not activate VDSDK\n");
        return 1;
    }

    CreateVirtualDisk();

    // Shutdown VDSDK
    ShutdownVDSDK(TRUE);

    return 0;
}

// ===========================================
// Virtual disk implementation
// ===========================================

BOOL _stdcall OnFormatCallback (ULONG Progress)
{
	printf ("Formatting...%i\n", Progress);

	if (Progress == 100) {
		ShellExecute (NULL, "open", g_szDrivePath, NULL, NULL, SW_SHOWNORMAL);
	}
	

	return true;
}


// read callback handler
BOOL _stdcall OnReadCallback
(
 DRIVE_HANDLE h,
 ULONGLONG ReadOffset,
 ULONG ReadSize,
 void* ReadBuffer,
 ULONG *BytesRead
 )
{
	// Regenerate the cipher if the size requested is greater than what we have
	//if (ReadSize > g_memCipher.GetSize ()) {
	//	g_memCipher.SetSize (ReadSize);
	//	if (g_enc.EncryptBuffer (&g_memCipher, g_szPassword, true) == true) {
	//		printf ("Encryption Successful\n");
	//	} else {				
	//		printf ("Encryption Failed!!!\n");
	//	}
	//}

    printf("OnReadCallback: size=%X\n",ReadSize);

	unsigned long bpos;
	unsigned long long encpos;	
	unsigned long long lciphersize = g_memCipher.GetSize ();

	unsigned long long lKb = 1024;
	unsigned long long lMeg = lKb * 1024;	    	
	unsigned long long lSplitMegs = (unsigned long long) SPLIT_SIZE;
	unsigned long long SplitSize = lSplitMegs * lMeg;

	unsigned long long lWhichfile = ReadOffset / SplitSize;
	unsigned long long lSplitfileoffset = ReadOffset % SplitSize;

	BYTE bp;
	BYTE bk;
	BYTE be;

    LARGE_INTEGER l;
    DWORD dw;
    //l.QuadPart = ReadOffset;
	printf ("OnReadCallback - Setting file pointer...\n");
	l.QuadPart = lSplitfileoffset;
    SetFilePointer(g_hSpanfile[lWhichfile],l.LowPart,&l.HighPart,FILE_BEGIN);

	printf ("OnReadCallback - Creating encryption buffer...\n");
	MemoryBuffer memEnc;
	memEnc.SetSize (ReadSize);

	//BOOL result = ReadFile(g_hFile,memEnc.GetBuffer (),ReadSize,&dw,NULL);
	BOOL result = ReadFile(g_hSpanfile[lWhichfile],memEnc.GetBuffer (),ReadSize,&dw,NULL);
	

	printf ("OnReadCallback - Beginning Encryption...\n");

	//memEnc.QuickCrypt (false);
	for (bpos=0;bpos<memEnc.GetSize ();bpos++) {
		
		encpos = (ReadOffset + bpos) % lciphersize;
		
		bp = memEnc.GetByte (bpos);
		bk = g_memCipher.GetByte (encpos);
		be = bp ^ bk;
		
		memEnc.SetByte (bpos, be);
	}

	printf ("OnReadCallback - Writing data...\n");

	memcpy (ReadBuffer, memEnc.GetBuffer (), ReadSize);

	memEnc.Clear ();

    //BOOL result = ReadFile(g_hFile,ReadBuffer,ReadSize,&dw,NULL);
    *BytesRead = dw;

	printf ("OnReadCallback - Exiting thread...\n");

    return result;
}

// write callback handler
BOOL _stdcall OnWriteCallback
(
 DRIVE_HANDLE h,
 ULONGLONG WriteOffset,
 ULONG WriteSize,
 const void* WriteBuffer,
 ULONG *BytesWritten
 )
{
    printf("OnWriteCallback: size=%X\n",WriteSize);

	// Regenerate the cipher if the size requested is greater than what we have
	//if (WriteSize > g_memCipher.GetSize ()) {
	//	g_memCipher.SetSize (WriteSize);
	//	if (g_enc.EncryptBuffer (&g_memCipher, g_szPassword, true) == true) {
	//		printf ("Encryption Successful\n");
	//	} else {				
	//		printf ("Encryption Failed!!!\n");
	//	}
	//}

	unsigned long long lKb = 1024;
	unsigned long long lMeg = lKb * 1024;	    	
	unsigned long long lSplitMegs = (unsigned long long) SPLIT_SIZE;
	unsigned long long SplitSize = lSplitMegs * lMeg;
	
	unsigned long long encpos;
	unsigned long long lciphersize = g_memCipher.GetSize ();
	unsigned long long lWhichfile = WriteOffset / SplitSize;
	unsigned long long lSplitfileoffset = WriteOffset % SplitSize;

	unsigned long bpos;
	BYTE bp;
	BYTE bk;
	BYTE be;

    LARGE_INTEGER l;
    DWORD dw;
    //l.QuadPart = WriteOffset;

	printf ("OnWriteCallback - Setting file pointer...\n");
	l.QuadPart = lSplitfileoffset;
    SetFilePointer(g_hSpanfile[lWhichfile],l.LowPart,&l.HighPart,FILE_BEGIN);

	printf ("OnWriteCallback - Creating encryption buffer...\n");
	MemoryBuffer memEnc;
	memEnc.SetSize (WriteSize);
	memEnc.Write ((BYTE *) WriteBuffer, 0, WriteSize);
	
	//memEnc.QuickCrypt (true);

	// Now XOR each byte in the memEnc buffer with the corresponding byte in our AES
	// cipher block

	printf ("OnWriteCallback - Beginning Encryption...\n");

	for (bpos=0;bpos<memEnc.GetSize ();bpos++) {
		
		encpos = (WriteOffset + bpos) % lciphersize;		

		bp = memEnc.GetByte (bpos);
		bk = g_memCipher.GetByte (encpos);
		be = bp ^ bk;
		
		memEnc.SetByte (bpos, be);
	}

    //BOOL result = WriteFile(g_hFile,WriteBuffer,WriteSize,&dw,NULL);
	
	printf ("OnWriteCallback - Writing data...on file %I64u\n", lWhichfile);
	BOOL result = WriteFile(g_hSpanfile[lWhichfile],memEnc.GetBuffer (),WriteSize,&dw,NULL);

	memEnc.Clear ();

    *BytesWritten = dw;

	printf ("OnWriteCallback - Exiting thread...\n");

    return result;
}


int AllocateDiskSpace()
{
	unsigned long f = 0;
	unsigned long long lKb = 1024;
	unsigned long long lMeg = lKb * 1024;
	unsigned long long lDiskMegs = (unsigned long long) DISK_SIZE;

    unsigned long long DiskSize = lDiskMegs * lMeg;
	
	unsigned long long lSplitMegs = (unsigned long long) SPLIT_SIZE;
	unsigned long long SplitSize = lSplitMegs * lMeg;

	unsigned long long SplitCount = (unsigned long long ) (DiskSize / SplitSize); // Number of files we need to create
	SplitCount++; // Increase the number of split files by one because divisions always round to nearest zero

    printf("Allocating disk space...\n");

    unsigned long long ChunkSize = 1024 * 1024; // 1 MB
    char* ZeroBuffer = new char[ChunkSize];

    //unsigned long long ChunksCount = (unsigned long long )(DiskSize / ChunkSize); // For non spanned disks
	unsigned long long ChunksCount = (unsigned long long )(SplitSize / ChunkSize); // For spanned disks across multiple image files	
	ChunksCount++;

	printf ("DISKSIZE: %I64u\n\n", lDiskMegs * lMeg);
	printf ("DiskSize: %I64u\n\n", DiskSize);
	printf ("Chunkscount: %i\n\n", ChunksCount);
	
	printf ("sizeof (unsigned long long) %i \n\n", sizeof (unsigned long long));


    DWORD dw;

    LARGE_INTEGER currentSize;
	unsigned long errcount = 0;

	for (unsigned long fs = 0;fs<SplitCount;fs++) {
		currentSize.LowPart = GetFileSize(g_hSpanfile[fs],(LPDWORD)&currentSize.HighPart);
		if(currentSize.QuadPart >= SplitSize)
		{
			// already allocated
			SetFilePointer(g_hSpanfile[fs],currentSize.LowPart,&currentSize.HighPart,FILE_BEGIN);
			SetEndOfFile(g_hSpanfile[fs]);
			//return;
		} else {
			errcount++;

			for(unsigned long i = 0;i<ChunksCount;i++)
			{
				printf("Allocating: %d%% of 100 (File %d)\n",i * 100 /ChunksCount, f);
				if(!WriteFile(g_hSpanfile[fs],ZeroBuffer,ChunkSize,&dw,NULL))
				{
					printf("ERROR\n");
				}
			}
		}
	}

	if (errcount == 0) {
		return 0; // All files already allocated
	}


	//return;

	//for (f = 0;f<SplitCount;f++) {


	//}

    delete[] ZeroBuffer;
    printf("Allocation completed\n");

	if (errcount == SplitCount) {
		return 1;
	} else {
		return 0;
	}
	
}

int CreateVirtualDisk()
{
    printf("Creating drive...\n");
	unsigned long long lKb = 1024;
	unsigned long long lMeg = lKb * 1024;
	unsigned long long lDiskMegs = (unsigned long long) DISK_SIZE;

    unsigned long long DiskSize = lDiskMegs * lMeg;

	unsigned long long lSplitMegs = (unsigned long long) SPLIT_SIZE;

	unsigned long long SplitSize = lSplitMegs * lMeg;
	unsigned long long SplitCount = (unsigned long long ) (DiskSize / SplitSize); // Number of files we need to create
	SplitCount++; // Increase the number of split files by one because divisions always round to nearest zero
	unsigned long lf = 0;


	char szModulepath[SIZE_STRING];
	ZeroMemory (szModulepath, SIZE_STRING);
	GetModuleFileName (NULL, szModulepath, SIZE_STRING);

	char szModuleDrive[SIZE_NAME];
	ZeroMemory (szModuleDrive, SIZE_NAME);
	strncpy_s (szModuleDrive, SIZE_NAME, szModulepath, 3);


	char szFilename[SIZE_STRING];
	ZeroMemory (szFilename, SIZE_STRING);
	//strcpy_s (szFilename, SIZE_STRING, szModuleDrive);
	strcpy_s (szFilename, SIZE_STRING, "D:\\");
	strcat_s (szFilename, SIZE_STRING, "EncryptedDisk"); // You can prefix a full path here if you like

	printf ("Filename is: %s\n\n", szFilename);

	//return 0;

	char szExt[SIZE_NAME];
	ZeroMemory (szExt, SIZE_NAME);
	strcpy_s (szExt, SIZE_NAME, ".dsk");

	char szFilenum[SIZE_NAME];
	char szActualfilename[SIZE_STRING];

	if (SplitCount >= MAXSPANNEDFILES) {
		printf ("ERROR: Number of files needed has exceeded MAXSPANNEDFILES!\n\n");
		printf ("Num files needed: %I64u\n\n", SplitCount);

		return 1;
	}	
	
	for (lf=0;lf<SplitCount;lf++) {

		ZeroMemory (szFilenum, SIZE_NAME);
		ultoa (lf, szFilenum, 10);

		ZeroMemory (szActualfilename, SIZE_STRING);
		strcpy_s (szActualfilename, SIZE_STRING, szFilename);		
		strcat_s (szActualfilename, SIZE_STRING, szFilenum);
		strcat_s (szActualfilename, SIZE_STRING, szExt);

		g_hSpanfile[lf] = CreateFile(szActualfilename,GENERIC_READ | GENERIC_WRITE,FILE_SHARE_READ,0,OPEN_ALWAYS,FILE_FLAG_NO_BUFFERING | FILE_FLAG_RANDOM_ACCESS,0);

		if(g_hSpanfile[lf] == INVALID_HANDLE_VALUE)
		{
			printf("ERROR: can not create / open disk image file\n");
			return 1;
		}
	}

	bool bFormatdrive = false;

    // allocate disk space
	if (AllocateDiskSpace() == 1) {
		bFormatdrive = true;
	}
	
	//return 1;

    // create virtual disk
    g_hDrive = CreateVirtualDrive(g_szDrive[0], DISK_SIZE,
        OnReadCallback,
        OnWriteCallback);

    if(g_hDrive == INVALID_DRIVE_HANDLE)
    {
        printf("ERROR: can not create virtual drive\n");
        return 1;
    }

	if (bFormatdrive == true) {
		printf ("Formatting Encrypted Drive - NTFS (Quick Format)...\n");
		FormatVirtualDrive (g_hDrive, TRUE, "NTFS", "CedeCrypted Drive", OnFormatCallback);
	}

	if (bFormatdrive == false) {
		ShellExecute (NULL, "open", g_szDrivePath, NULL, NULL, SW_SHOWNORMAL);
	}

    printf("Encrypted Drive created. Handle = %d\n",g_hDrive);
    printf("Press any key to dismount encrypted drive\n");

    // wait until any key pressed
    getch();

    // force dismount disk
    DestroyVirtualDrive(g_hDrive,TRUE);	

	for (lf=0;lf<SplitCount;lf++) {
		CloseHandle(g_hSpanfile[lf]);	
	}

    

    return 0;
}
