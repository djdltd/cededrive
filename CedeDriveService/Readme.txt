This is the CedeDrive Manager service. It is responsible for mounting and unmounting
encrypted virtual drives, but instead is doing it running as a service. The reason for
creating this service is to work around the problem whereby Administrator privileges are
required to mount and unmount drives. Services usually run under the Local System account
which is the highest available local privilege so will always have permission to mount
and unmount the drives. The CedeDrive GUI (whether this is the CedeDrive Installed App,
or the CedeDrivePortable app) communicates with this service using named pipes. Named
pipes are used to send requests to the service, and the service responds with an
Instruction Result containing a result code, description and a boolean value indicating
whether the request was successful.

