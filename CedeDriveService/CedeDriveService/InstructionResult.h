#pragma once
#include "resource.h"

class InstructionResult {
	
	public:
		InstructionResult ();
		~InstructionResult ();

		unsigned int iResult;
		bool bSuccess;
		char szDescription[SIZE_STRING];
	private:
};
