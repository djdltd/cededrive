#ifndef _VSDK_DLL_H_
#define _VSDK_DLL_H_

#include "VDSDKCommon.h"

#ifdef VSDK_DLL_EXPORTS
#define VDSDK_API extern "C" _declspec(dllexport)
//#define VDSDK_API
#else
#define VDSDK_API extern "C" _declspec(dllimport)
#endif


// drive flags (DriveFlags in CreateVirtualDriveEx)
enum
{
    DRIVE_FLAG_REMOVABLE = 1
};


// -------------------------------------------
// General purpose API functions
// -------------------------------------------

/*
Initialize Virtual Drive SDK
If driver is not running, it will be installed and started
@return result
*/
VDSDK_API
BOOL
_stdcall InitializeVDSDK();


/*
Uninitialize Virtual Drive SDK
@param bForceUnmountDisks forcibly dismount disks if they are used
*/
VDSDK_API
void
_stdcall ShutdownVDSDK(BOOL bForceUnmountDisks);


/*
Create virtual drive and register virtual drive callback handlers
@param DriveLetter drive letter (e.g. 'X')
@param DriveSize drive size, MB
@param pReadCallback pointer to read callback handler
@param pWriteCallback pointer to write callback handler
@return drive handle
*/
VDSDK_API
DRIVE_HANDLE
_stdcall CreateVirtualDrive(
           char DriveLetter,
           ULONG DriveSize,
           ON_READ_CALLBACK pReadCallback,
           ON_WRITE_CALLBACK pWriteCallback
           );


/*
Create virtual drive and register virtual drive callback handlers
@param DriveLetter drive letter (e.g. 'X')
@param DriveSize drive size, MB
@param pReadCallback pointer to read callback handler
@param pWriteCallback pointer to write callback handler
@param bReadOnly create read-only drive
@param DriveFlags: drive flags
@return drive handle
*/
VDSDK_API
DRIVE_HANDLE
_stdcall CreateVirtualDriveEx(
                   char DriveLetter,
                   ULONG DriveSize,
                   ON_READ_CALLBACK pReadCallback,
                   ON_WRITE_CALLBACK pWriteCallback,
                   BOOL bReadOnly,
                   ULONG DriveFlags
                   );



/*
Format drive
@param h drive handle
@param bQuickFormat quick or full format
@param FileSystem "FAT", "FAT32" or "NTFS"
@param VolumeLabel volume label
@param pCallbackHandler callback (format callback, can be NULL)
@return result
*/
VDSDK_API
BOOL
_stdcall FormatVirtualDrive(
                         DRIVE_HANDLE h, 
                         BOOL bQuickFormat, 
                         const char* FileSystem, 
                         const char* VolumeLabel, 
                         ON_FORMAT_CALLBACK pCallbackHandler);


/*
Destroy virtual drive by drive handle
@param h drive handle
@param bForce force dismount disk in case it is used
@return result
*/
VDSDK_API
BOOL
_stdcall DestroyVirtualDrive(DRIVE_HANDLE h, BOOL bForce);


/*
Get error code.
Error codes are defined in VDSDKErrors.h
@return result
*/
VDSDK_API
int
_stdcall GetVDSDKErrorCode();


/*
Register VDSDK notification handler
*/
VDSDK_API
void
_stdcall RegisterNotificationHandler(ON_VDSDK_NOTIFICATION pNotificationHandler);


// -------------------------------------------
// Additional kernel driver control functions
// -------------------------------------------

/*
Install kernel driver service
@return result
*/
VDSDK_API
BOOL
_stdcall InstallDriver();


/*
Start kernel driver service
@return result
*/
VDSDK_API
BOOL
_stdcall StartDriver();

/*
Stop kernel driver service
@return result
*/
VDSDK_API
BOOL
_stdcall StopDriver();

/*
Uninstall kernel driver service
@return result
*/
VDSDK_API
BOOL
_stdcall UninstallDriver();

/*
Query whether kernel driver is started
@return result
*/
VDSDK_API
BOOL
_stdcall IsDriverReady();

/*
Obtain kernel driver version
@param major (reference) Major version
@param minor (reference) Minor version
@return result
*/
VDSDK_API
BOOL
_stdcall GetVDSDKDriverVersion(int &major, int &minor);

/*
Activate VDSDK
@param ActivationKey Activation key that activates all features. 
                     It is shipped with commercial license.
@return result
*/
VDSDK_API
BOOL 
_stdcall ActivateVDSDK(const char* ActivationKey);

#endif
