#pragma once
#include "resource.h"
#include "EventDescriptions.h"
#include <windows.h>
#include <malloc.h>
#include <stdio.h>

class EventLogging {
	
	public:
		EventLogging ();
		~EventLogging ();		
		
		void SetEventsource (char *szEventsource);
		void ReportEventLog (DWORD dwEventID, char *szMessage, unsigned int iType);
		BOOL AddEventSource(LPTSTR pszLogName, LPTSTR pszSrcName, DWORD dwNum);
	private:
		
		bool bEventsourceset;
		char m_szEventsource[SIZE_STRING];
};
