#include "VirtualDriveManager.h"

VirtualDriveManager::VirtualDriveManager ()
{
	m_hAllocateFile = INVALID_HANDLE_VALUE;	

	for (int i=0;i<MAX_DRIVES;i++)
	{
		m_hDriveResource[i] = INVALID_DRIVE_HANDLE;
		m_bResourceBusy[i] = false;
		m_Offsetvalues[i] = 0;
	}

	m_diagnosticsset = false;
	m_binitialised = false;
	m_pinstance = (VirtualDriveManager *) this;
}

VirtualDriveManager::~VirtualDriveManager ()
{
}

void VirtualDriveManager::SetEventLogging (EventLogging *log)
{
	m_plog = log;
	m_diagnosticsset = true;
	m_enc.SetEventLogging (log);
}

void VirtualDriveManager::OutputInt (LPCSTR lpszText, int iValue, bool bError)
{
	if (m_diagnosticsset == true) {
		char szMsg[SIZE_STRING];
		ZeroMemory (szMsg, SIZE_STRING);
		sprintf_s (szMsg, SIZE_STRING, "%s %i", lpszText, iValue);

		if (bError == true) {
			m_plog->ReportEventLog (MSG_GENERICERROR, szMsg, ERR);
		} else {
			m_plog->ReportEventLog (MSG_GENERICINFO, szMsg, INFO);
		}
	}
}

void VirtualDriveManager::OutputText (LPCSTR lpszText, bool bError)
{
	if (m_diagnosticsset == true) {
		char szMsg[SIZE_STRING];
		ZeroMemory (szMsg, SIZE_STRING);
		sprintf_s (szMsg, SIZE_STRING, "%s", lpszText);

		if (bError == true) {
			m_plog->ReportEventLog (MSG_GENERICERROR, szMsg, ERR);
		} else {
			m_plog->ReportEventLog (MSG_GENERICINFO, szMsg, INFO);
		}
	}
}

void VirtualDriveManager::OutputText (LPCSTR lpszName, LPCSTR lpszValue, bool bError)
{
	if (m_diagnosticsset == true) {
		char szMsg[SIZE_STRING];
		ZeroMemory (szMsg, SIZE_STRING);
		sprintf_s (szMsg, SIZE_STRING, "%s %s", lpszName, lpszValue);

		if (bError == true) {
			m_plog->ReportEventLog (MSG_GENERICERROR, szMsg, ERR);
		} else {
			m_plog->ReportEventLog (MSG_GENERICINFO, szMsg, INFO);
		}
	}
}

void VirtualDriveManager::SetHWND (HWND hWnd)
{
	m_hwnd = hWnd;
}

void VirtualDriveManager::Initialise (char *szEncryptPassword)
{
	if (m_binitialised == true) {
		return;
	}

	ZeroMemory (m_szPassword, SIZE_STRING);
	strcpy_s (m_szPassword, SIZE_STRING, szEncryptPassword);
	m_memCipher.SetSize (1024*1024);

	if (m_enc.EncryptBuffer (&m_memCipher, m_szPassword, true) == true) {
		//printf ("Encryption Successful - key has been created\n");
		OutputText ("CedeDrive Encryption service was initialised successfully. ", false);
	} else {
		//printf ("Encryption Failed!!!\n");
		OutputText ("The CedeDrive Encryption service failed to initialise.", true);
	}

	//m_connectpanel.Hide ();
	if(!InitializeVDSDK())
    {
        //printf("Error: Can not initialize VDSDK\n");
		OutputText ("CedeDrive was unable to initialise the Virtual Drive Engine. ", true);
	} else {
		OutputText ("CedeDrive has initialised the Virtual Drive Engine successfully.", false);
	}

	if(!ActivateVDSDK("+Gdd9LfTihfFs/DLL4XbkCDyBL3GQYD0IPIEvcZBgPQg8gS9xkGA9CDyBL3GQYD0IPIEvcZBgPQg8gS9xkGA9IJzlgZrh9vmbXLrR0L4CIUg8gS9xkGA9CDyBL3GQYD0IPIEvcZBgPQg8gS9xkGA9CDyBL3GQYD0IPIEvcZBgPSGjHphJVmb58EgDZk02prmIPIEvcZBgPQg8gS9xkGA9CDyBL3GQYD0IPIEvcZBgPQg8gS9xkGA9CDyBL3GQYD0uSfV5IlEc2JdnyPjffC4uliKy/LeIbBqIPIEvcZBgPQg8gS9xkGA9CDyBL3GQYD0IPIEvcZBgPSUAEPWwBV5b2X+fyEzUs9IfsMRLMlwaiJ2naPy83snBb7FBpDieoCyE+s0X/3LmL5Dv7ZiJKC1G5tvTM7c31P/oQyhIFj9TPvIWKh4YB2mT055xzRhjnYSyZmXSQvBpP/2JzuRv9hYDO0XDumWdgXapQnJxeJUifzkA7/yrCuSxLRqyRxeHyj/ZkQBMvXXDZKrF7OYNfGL7KMKBQmPYA7Fuh+JZskfc8O8NHQ10aVPw3NiEkz5XHjtotbR6eq+mzRsxbd1BF1tGlDQYQYKoSuNblh2Lj89w83qiZKfQcAAg83Km6R05NI1z3nG3NqLwLsqZX+kFyqdmN5Whdd2R2ahw3kr1wA/EdyUfsZ3y63pARaqydMrI9ui628ABC27CE572154FFEE62D6934BD756"))
    {
        //printf("ERROR: can not activate VDSDK\n");
		OutputText ("CedeDrive was unable to activate the Virtual Drive Engine.", true);        
	} else {
		OutputText ("CedeDrive has activated the Virtual Drive Engine successfully.", false);
	}

	m_binitialised = true;
	//CreateVirtualDisk();
}

void VirtualDriveManager::GetMountError (char *szOutmessage)
{
	ZeroMemory (szOutmessage, SIZE_STRING);
	strcpy_s (szOutmessage, SIZE_STRING, m_szMounterrormessage);
}

DWORD WINAPI VirtualDriveManager::ThreadProc_AllocateDiskSpace (PVOID pParam)
{
	VirtualDriveManager *pwnd = (VirtualDriveManager *) pParam;

	unsigned long long userspecifiedsize = (unsigned long long) pwnd->currentAllocationInfo.lDiskSizeMegs;

	unsigned long long DiskSize = (unsigned long long) userspecifiedsize * 1024 * 1024;
	//unsigned long long DiskSize = 10485760000;
    DWORD dw;

    LARGE_INTEGER currentSize;
	LARGE_INTEGER newPointerlocation;
    //currentSize.LowPart = GetFileSize(g_hFile,(LPDWORD)&currentSize.HighPart);
    
	GetFileSizeEx (pwnd->m_hAllocateFile, &currentSize);
	
	if(currentSize.QuadPart >= DiskSize)
    {
        // already allocated
        //SetFilePointer(g_hFile,currentSize.LowPart,&currentSize.HighPart,FILE_BEGIN);
		
		SetFilePointerEx (pwnd->m_hAllocateFile, currentSize, &newPointerlocation, FILE_BEGIN);

        SetEndOfFile(pwnd->m_hAllocateFile);
		CloseHandle (pwnd->m_hAllocateFile);
        return 0;
    }
  

    const int ChunkSize = 1024 * 1024; // 1 MB
    char* ZeroBuffer = new char[ChunkSize];

    unsigned ChunksCount = (unsigned)(DiskSize / ChunkSize);
    for(unsigned i = 0;i<ChunksCount;++i)
    {        

		ZeroMemory (pwnd->m_szProgressLabel, SIZE_STRING);
		sprintf_s (pwnd->m_szProgressLabel, SIZE_STRING, "Creating encrypted virtual drive, %i %% complete (written chunk %i of %i)",  (i * 100 / ChunksCount), i, ChunksCount);
		
		pwnd->m_allocationpercent = (i * 100 / ChunksCount);
		pwnd->m_currentvalue = i;
		pwnd->m_maxvalues = ChunksCount;

		//pwnd->OutputInt ("Allocating: ", (i * 100 / ChunksCount));


        if(!WriteFile(pwnd->m_hAllocateFile,ZeroBuffer,ChunkSize,&dw,NULL))
        {            
			//pwnd->OutputText ("Error!");
        }
   }

    delete[] ZeroBuffer;    
	//pwnd->OutputText ("Allocation completed.");
	CloseHandle (pwnd->m_hAllocateFile);

	// Post a message to the main UI thread informing it that the Allocation has ended


	return 0;
}

void VirtualDriveManager::DoThread_AllocateDiskSpace ()
{
	// Spawns the thread that actually does the disk allocating as this
	// will take some time. Especially on large virtual disks.
	HANDLE hThread;
	DWORD dwThreadID;

	hThread = CreateThread (NULL, 0, ThreadProc_AllocateDiskSpace, (void *) this, 0, &dwThreadID);
}

bool VirtualDriveManager::FileExists (char *FileName)
{
	// Check if the file exists		
	struct _finddata_t c_file;
	long hFile;

	// Build the list of files in the source path
	if( (hFile = _findfirst(FileName, &c_file )) == -1L ) {
		return false;
	} else {
		return true;
	}

	return false;
}

bool VirtualDriveManager::IsDriveMounted (SingleDriveInfo driveinfo)
{
	for (int i=0;i<MAX_DRIVES;i++) {
		if (m_bResourceBusy[i] == true) {
			if (strcmp (driveinfo.szPath, m_Drivelist[i].szPath) == 0) {
				return true;
			}
		}
	}

	return false;
}

bool VirtualDriveManager::Replacestring (char *szStringtoreplace, char *szStringtosearchfor, char *szReplacement)
{
	char szFull[SIZE_STRING];
	char szSearch[SIZE_STRING];
	char szTemp[SIZE_STRING];
	int c = 0;

	char szFinal[SIZE_STRING];

	if (strlen (szStringtoreplace) > 0) {
		if (strlen (szStringtoreplace) >= strlen (szStringtosearchfor)) {
			
			ZeroMemory (szFull, SIZE_STRING);
			strcpy_s (szFull, SIZE_STRING, szStringtoreplace);

			ZeroMemory (szSearch, SIZE_STRING);
			strcpy_s (szSearch, SIZE_STRING, szStringtosearchfor);

			_strupr (szFull);
			_strupr (szSearch);

			for (c=0;c<(strlen (szFull)-strlen (szSearch));c++) {
				
				ZeroMemory (szTemp, SIZE_STRING);
				strncpy_s (szTemp, SIZE_STRING, szFull+c, strlen (szSearch));

				if (strcmp (szTemp, szSearch) == 0) {
					// We found the string

					ZeroMemory (szFinal, SIZE_STRING);
					
					// Copy the first part (before the found string) into the final string
					if (c>0) {
						strncpy_s (szFinal, SIZE_STRING, szStringtoreplace, c);
					}

					// Now append the replacement
					strcat_s (szFinal, SIZE_STRING, szReplacement);

					// now append the rest of the source string (after the found string)
					strcat_s (szFinal, SIZE_STRING, szStringtoreplace+c+strlen(szStringtosearchfor));

					ZeroMemory (szStringtoreplace, SIZE_STRING);
					strcpy_s (szStringtoreplace, SIZE_STRING, szFinal);

					//return true;
				}

			}

			// if we got here, then we didn't find anything
			return false;

		} else {
			return false;
		}
	} else {
		return false;
	}

	return false;
}

void VirtualDriveManager::GenFilename (char *szOutfilename, int iSize)
{
	SYSTEMTIME systime;
	GetSystemTime (&systime);
	ZeroMemory (szOutfilename, iSize);
	sprintf_s (szOutfilename, iSize, "%i-%i-%i-%i-%i-%iAutoFile.dat", systime.wDay, systime.wMonth, systime.wYear, systime.wHour, systime.wMinute, systime.wSecond);
}

int VirtualDriveManager::MountVirtualDisk (SingleDriveInfo driveinfo)
{
	ZeroMemory (m_szMounterrormessage, SIZE_STRING);

	// First check if this drive is not already mounted
	if (IsDriveMounted (driveinfo) == true) {
		sprintf_s (m_szMounterrormessage, "Unable to mount encrypted Drive '%s' because it is already mounted.", driveinfo.szName);
		OutputText (m_szMounterrormessage, true);
		return 0;
	}

	// Check if we free drive resources out of our maximum
	int iDriveresource = GetFreeDriveResource();

	if (iDriveresource != -1)
	{
		// First we need to make sure the file hosting the mounted disk is actually there
		if (FileExists (driveinfo.szPath) == false) {
			sprintf_s (m_szMounterrormessage, "Unable to mount encrypted Drive '%s' because the file %s could not be found. If this file has been deleted you will not gain access to your encrypted data.", driveinfo.szName, driveinfo.szPath);
			OutputText (m_szMounterrormessage, true);
			return 4;
		}

		// Now we need to make sure that the file attempting mount is an actual EVD file
		// and we need the header offset value so we don't overwrite the header with 
		// read and write information
		EVDFileHeader fileheader;
		fileheader.SetEventLogging (m_plog);

		if (fileheader.SetFromFile (driveinfo.szPath) == false) {
			char szError[SIZE_STRING];
			fileheader.GetLastErrorMessage (szError);
			sprintf_s (m_szMounterrormessage, "Unable to mount encrypted Drive '%s'. Reason: %s", driveinfo.szName, szError);
			OutputText (m_szMounterrormessage, true);
			return 5;
		}

		fileheader.Deserialise ();
		m_Offsetvalues[iDriveresource] = fileheader.GetDataOffset ();

		// We have a resource, now mark it as busy
		m_bResourceBusy[iDriveresource] = true; 

		m_Drivelist[iDriveresource] = driveinfo; // So we know which drive info corresponds to the resource we are using



		// First we need to open the file, and get a handle to it
		//OutputText ("Getting file handle...");

		m_hFileResource[iDriveresource] = CreateFile(driveinfo.szPath, GENERIC_READ | GENERIC_WRITE,FILE_SHARE_READ,0,
			OPEN_ALWAYS,FILE_FLAG_NO_BUFFERING | FILE_FLAG_RANDOM_ACCESS,0);

		if(m_hFileResource[iDriveresource] == INVALID_HANDLE_VALUE)
		{
			DWORD dwError = GetLastError ();
			//OutputText ("ERROR: can not create / open disk image file for allocation", true);
			m_bResourceBusy[iDriveresource] = false;
			//CloseHandle (m_hFileResource[iDriveresource]);

			sprintf_s (m_szMounterrormessage, "Unable to mount encrypted Drive '%s' because the file %s could not be opened. (%i)", driveinfo.szName, driveinfo.szPath, dwError);
			OutputText (m_szMounterrormessage, true);
			return 1;
		}
		
		//char szPath[SIZE_STRING];
		//ZeroMemory (szPath, SIZE_STRING);
		//strcpy_s (szPath, SIZE_STRING, "C:\\Temp\\");

		//char szEncmemfiletemp[SIZE_STRING];

		//GenFilename (szEncmemfiletemp, SIZE_STRING);

		//strcat_s (szPath, SIZE_STRING, szEncmemfiletemp);

		// Now check if we need to create a separate encryption buffer if a separate password has been specified for this
		// drive. 
		if (driveinfo.bUsingseperatepassword == true) {
			
			m_memIndividualcipher[iDriveresource].Clear ();
			m_memIndividualcipher[iDriveresource].SetSize (1024*1024);

			if (m_enc.EncryptBuffer (&m_memIndividualcipher[iDriveresource], driveinfo.szPassword, true) == true) {
				//printf ("Encryption Successful - key has been created\n");
				//OutputText ("CedeDrive has created a separate Encryption session with password: ", driveinfo.szPassword, false);
				OutputText ("CedeDrive has successfully created a separate Encryption session for ", driveinfo.szPath, false);

				// Save the encryption session for diagnostic purposes
				//m_memIndividualcipher[iDriveresource].SaveToFile (szPath);

			} else {
				//printf ("Encryption Failed!!!\n");
				OutputText ("CedeDrive was unable to create a separate Encryption session for ", driveinfo.szPath, true);
			}
		}

		// All looks good, so attempt to mount the drive
		// The worst bit, we have to check which drive resource we are using and call a different
		// Mount function because we also are using 30 different callbacks, therefore we need
		// to make sure we have the correct callback assigned.
		if (iDriveresource == 0) {m_hDriveResource[iDriveresource] = CreateVirtualDrive(driveinfo.szDriveLetter[0], driveinfo.lDiskSizeMegs, OnReadCallback0, OnWriteCallback0);}
		if (iDriveresource == 1) {m_hDriveResource[iDriveresource] = CreateVirtualDrive(driveinfo.szDriveLetter[0], driveinfo.lDiskSizeMegs, OnReadCallback1, OnWriteCallback1);}
		if (iDriveresource == 2) {m_hDriveResource[iDriveresource] = CreateVirtualDrive(driveinfo.szDriveLetter[0], driveinfo.lDiskSizeMegs, OnReadCallback2, OnWriteCallback2);}
		if (iDriveresource == 3) {m_hDriveResource[iDriveresource] = CreateVirtualDrive(driveinfo.szDriveLetter[0], driveinfo.lDiskSizeMegs, OnReadCallback3, OnWriteCallback3);}
		if (iDriveresource == 4) {m_hDriveResource[iDriveresource] = CreateVirtualDrive(driveinfo.szDriveLetter[0], driveinfo.lDiskSizeMegs, OnReadCallback4, OnWriteCallback4);}
		if (iDriveresource == 5) {m_hDriveResource[iDriveresource] = CreateVirtualDrive(driveinfo.szDriveLetter[0], driveinfo.lDiskSizeMegs, OnReadCallback5, OnWriteCallback5);}
		if (iDriveresource == 6) {m_hDriveResource[iDriveresource] = CreateVirtualDrive(driveinfo.szDriveLetter[0], driveinfo.lDiskSizeMegs, OnReadCallback6, OnWriteCallback6);}
		if (iDriveresource == 7) {m_hDriveResource[iDriveresource] = CreateVirtualDrive(driveinfo.szDriveLetter[0], driveinfo.lDiskSizeMegs, OnReadCallback7, OnWriteCallback7);}
		if (iDriveresource == 8) {m_hDriveResource[iDriveresource] = CreateVirtualDrive(driveinfo.szDriveLetter[0], driveinfo.lDiskSizeMegs, OnReadCallback8, OnWriteCallback8);}
		if (iDriveresource == 9) {m_hDriveResource[iDriveresource] = CreateVirtualDrive(driveinfo.szDriveLetter[0], driveinfo.lDiskSizeMegs, OnReadCallback9, OnWriteCallback9);}
		if (iDriveresource == 10) {m_hDriveResource[iDriveresource] = CreateVirtualDrive(driveinfo.szDriveLetter[0], driveinfo.lDiskSizeMegs, OnReadCallback10, OnWriteCallback10);}
		if (iDriveresource == 11) {m_hDriveResource[iDriveresource] = CreateVirtualDrive(driveinfo.szDriveLetter[0], driveinfo.lDiskSizeMegs, OnReadCallback11, OnWriteCallback11);}
		if (iDriveresource == 12) {m_hDriveResource[iDriveresource] = CreateVirtualDrive(driveinfo.szDriveLetter[0], driveinfo.lDiskSizeMegs, OnReadCallback12, OnWriteCallback12);}
		if (iDriveresource == 13) {m_hDriveResource[iDriveresource] = CreateVirtualDrive(driveinfo.szDriveLetter[0], driveinfo.lDiskSizeMegs, OnReadCallback13, OnWriteCallback13);}
		if (iDriveresource == 14) {m_hDriveResource[iDriveresource] = CreateVirtualDrive(driveinfo.szDriveLetter[0], driveinfo.lDiskSizeMegs, OnReadCallback14, OnWriteCallback14);}
		if (iDriveresource == 15) {m_hDriveResource[iDriveresource] = CreateVirtualDrive(driveinfo.szDriveLetter[0], driveinfo.lDiskSizeMegs, OnReadCallback15, OnWriteCallback15);}
		if (iDriveresource == 16) {m_hDriveResource[iDriveresource] = CreateVirtualDrive(driveinfo.szDriveLetter[0], driveinfo.lDiskSizeMegs, OnReadCallback16, OnWriteCallback16);}
		if (iDriveresource == 17) {m_hDriveResource[iDriveresource] = CreateVirtualDrive(driveinfo.szDriveLetter[0], driveinfo.lDiskSizeMegs, OnReadCallback17, OnWriteCallback17);}
		if (iDriveresource == 18) {m_hDriveResource[iDriveresource] = CreateVirtualDrive(driveinfo.szDriveLetter[0], driveinfo.lDiskSizeMegs, OnReadCallback18, OnWriteCallback18);}
		if (iDriveresource == 19) {m_hDriveResource[iDriveresource] = CreateVirtualDrive(driveinfo.szDriveLetter[0], driveinfo.lDiskSizeMegs, OnReadCallback19, OnWriteCallback19);}
		if (iDriveresource == 20) {m_hDriveResource[iDriveresource] = CreateVirtualDrive(driveinfo.szDriveLetter[0], driveinfo.lDiskSizeMegs, OnReadCallback20, OnWriteCallback20);}
		if (iDriveresource == 21) {m_hDriveResource[iDriveresource] = CreateVirtualDrive(driveinfo.szDriveLetter[0], driveinfo.lDiskSizeMegs, OnReadCallback21, OnWriteCallback21);}
		if (iDriveresource == 22) {m_hDriveResource[iDriveresource] = CreateVirtualDrive(driveinfo.szDriveLetter[0], driveinfo.lDiskSizeMegs, OnReadCallback22, OnWriteCallback22);}
		if (iDriveresource == 23) {m_hDriveResource[iDriveresource] = CreateVirtualDrive(driveinfo.szDriveLetter[0], driveinfo.lDiskSizeMegs, OnReadCallback23, OnWriteCallback23);}
		if (iDriveresource == 24) {m_hDriveResource[iDriveresource] = CreateVirtualDrive(driveinfo.szDriveLetter[0], driveinfo.lDiskSizeMegs, OnReadCallback24, OnWriteCallback24);}
		if (iDriveresource == 25) {m_hDriveResource[iDriveresource] = CreateVirtualDrive(driveinfo.szDriveLetter[0], driveinfo.lDiskSizeMegs, OnReadCallback25, OnWriteCallback25);}
		if (iDriveresource == 26) {m_hDriveResource[iDriveresource] = CreateVirtualDrive(driveinfo.szDriveLetter[0], driveinfo.lDiskSizeMegs, OnReadCallback26, OnWriteCallback26);}
		if (iDriveresource == 27) {m_hDriveResource[iDriveresource] = CreateVirtualDrive(driveinfo.szDriveLetter[0], driveinfo.lDiskSizeMegs, OnReadCallback27, OnWriteCallback27);}
		if (iDriveresource == 28) {m_hDriveResource[iDriveresource] = CreateVirtualDrive(driveinfo.szDriveLetter[0], driveinfo.lDiskSizeMegs, OnReadCallback28, OnWriteCallback28);}
		if (iDriveresource == 29) {m_hDriveResource[iDriveresource] = CreateVirtualDrive(driveinfo.szDriveLetter[0], driveinfo.lDiskSizeMegs, OnReadCallback29, OnWriteCallback29);}
		
		if(m_hDriveResource[iDriveresource] == INVALID_DRIVE_HANDLE)
		{
			//OutputInt ("ERROR: can not mount virtual drive resource ", iDriveresource, true);
			m_bResourceBusy[iDriveresource] = false;

			sprintf_s (m_szMounterrormessage, "Unable to mount encrypted Drive '%s'. If you are running on Windows Vista or Windows 7 please ensure you are running CedeDrive as an Administrator.", driveinfo.szName, driveinfo.szDriveLetter);
			CloseHandle (m_hFileResource[iDriveresource]);
			
			OutputText (m_szMounterrormessage, true);
			return 3;
		} else {
			char szMsg[SIZE_STRING];
			ZeroMemory (szMsg, SIZE_STRING);
			sprintf_s (szMsg, SIZE_STRING, "The Encrypted Virtual Drive %s located at %s has been mounted successfully as drive %s, using Virtual Drive resource %i, with Data Offset %i", driveinfo.szName, driveinfo.szPath, driveinfo.szDriveLetter, iDriveresource, m_Offsetvalues[iDriveresource]);
			OutputText (szMsg, false);

			if (driveinfo.bFormatdriveaftermount == true) {
			    // QuickFormat = TRUE, FileSystem=NTFS, VolumeLabel=VDSDK
				DriveFormatter formatter;
				

				if (strlen (driveinfo.szDriveLetter) > 0) {

					// we only want the first letter of the drive
					char szDriveletter[SIZE_NAME];
					ZeroMemory (szDriveletter, SIZE_NAME);
					strncpy_s (szDriveletter, driveinfo.szDriveLetter, 1);

					char szName[SIZE_STRING];
					ZeroMemory (szName, SIZE_STRING);
					strcpy_s (szName, SIZE_STRING, "Encrypted_Drive");

					if (strlen (driveinfo.szName) > 1) {
						ZeroMemory (szName, SIZE_STRING);
						strcpy_s (szName, SIZE_STRING, driveinfo.szName);

						Replacestring (szName, " ", "_");
					} else {
						
						if (strlen (driveinfo.szVolumename) > 1) {
							ZeroMemory (szName, SIZE_STRING);
							strcpy_s (szName, SIZE_STRING, driveinfo.szVolumename);

							Replacestring (szName, " ", "_");
						}
					}

					if (formatter.QuickFormatDrive (szDriveletter, szName) == false) {
						char szError[SIZE_STRING];
						formatter.GetErrorReason (szError, SIZE_STRING);
						OutputText ("Failed to Format Virtual Encrypted Drive, Reason: ", szError, true);

						char szScriptpath[SIZE_STRING];
						formatter.GetScriptPath (szScriptpath, SIZE_STRING);

						OutputText ("Format Script Path is: ", szScriptpath, false);
					} else {
						OutputText ("Drive Formatted Successfully.", false);

						char szScriptpath[SIZE_STRING];
						formatter.GetScriptPath (szScriptpath, SIZE_STRING);

						OutputText ("Format Script Path is: ", szScriptpath, false);
					}

					
				}
				
				/*
				Sleep (2000);
				
				if (!FormatVirtualDrive(m_hDriveResource[iDriveresource],TRUE,"NTFS",driveinfo.szVolumename, OnFormatCallbackHandler)) {
					Sleep (2000);
					if (!FormatVirtualDrive(m_hDriveResource[iDriveresource],TRUE,"NTFS",driveinfo.szVolumename, OnFormatCallbackHandler)) {
						sprintf_s (m_szMounterrormessage, "Unable to format new encrypted Drive '%s' (%s). However, the mounting was successful.", driveinfo.szName, driveinfo.szDriveLetter);
						OutputText (m_szMounterrormessage, true);
					}									
				}
				*/
				
			}

			return 0;
		}

	} else {
		//OutputText ("ERROR: No more free virtual drive resources!", true);	
		sprintf_s (m_szMounterrormessage, "Unable to mount encrypted Drive '%s' because you have exceeded the maximum number of 30 mounted drives. Try unmounting some drives you are not using and try again.", driveinfo.szName);
		OutputText (m_szMounterrormessage, true);
		return 2;
	}		    	
}

void VirtualDriveManager::UnmountVirtualDrive (SingleDriveInfo driveinfo)
{
	// Check which drive corresponds to the driveinfo object we are provided
	// when we find a match then unmount the drive.
	for (int i=0;i<MAX_DRIVES;i++)
	{
		if (m_bResourceBusy[i] == true) {
			if (strcmp (driveinfo.szPath, m_Drivelist[i].szPath) == 0) {
				DestroyVirtualDrive (m_hDriveResource[i], TRUE);
				CloseHandle(m_hFileResource[i]);
				m_bResourceBusy[i] = false; 
				m_Offsetvalues[i] = 0;
				if (driveinfo.bUsingseperatepassword == true) {
					m_memIndividualcipher[i].Clear();
				}
				char szMsg[SIZE_STRING];
				ZeroMemory (szMsg, SIZE_STRING);
				sprintf_s (szMsg, SIZE_STRING, "The Encrypted Virtual Drive %s located at %s with drive letter %s has been un-mounted.", driveinfo.szName, driveinfo.szPath, driveinfo.szDriveLetter);
				OutputText (szMsg, false);
			}
		}
	}
}

void VirtualDriveManager::UnmountAllVirtualDrives ()
{
	for (int i=0;i<MAX_DRIVES;i++)
	{
		if (m_bResourceBusy[i] == true)
		{			
			DestroyVirtualDrive (m_hDriveResource[i], TRUE);
			CloseHandle(m_hFileResource[i]);
			m_bResourceBusy[i] = false;				
			m_memIndividualcipher[i].Clear();				
		}
	}
	
	OutputText ("CedeDrive has unmounted all Encrypted Virtual Drives.", false);
}

bool VirtualDriveManager::DecryptBlock (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead, int iDriveresource)
{
	// This is called by any one of the 30 callback functions to decrypt a block given
	// a drive resource number, so it knows which drive handle and file to decrypt.
	
	unsigned long long lciphersize;
	unsigned long bpos;
	unsigned long long encpos;	
	BYTE bp;
	BYTE bk;
	BYTE be;

	//m_pinstance->OutputInt ("OnReadCallback, size: ", ReadSize);

    LARGE_INTEGER l;
	LARGE_INTEGER newpointer;
    DWORD dw;
    l.QuadPart = ReadOffset + m_Offsetvalues[iDriveresource];

	bool bseparateencryption = m_pinstance->m_Drivelist[iDriveresource].bUsingseperatepassword;

	if (bseparateencryption == true) {
		lciphersize = m_pinstance->m_memIndividualcipher[iDriveresource].GetSize (); // The buffer used as the key
	} else {
		lciphersize = m_pinstance->m_memCipher.GetSize (); // The buffer used as the key
	}

    //SetFilePointer(g_hFile,l.LowPart,&l.HighPart,FILE_BEGIN); // 32-bit Only
	SetFilePointerEx (m_pinstance->m_hFileResource[iDriveresource], l, &newpointer, FILE_BEGIN);
	
	MemoryBuffer memEnc;
	memEnc.SetSize (ReadSize);

	// Read the encrypted block from the file disk into our memory buffer
	if (!ReadFile(m_pinstance->m_hFileResource[iDriveresource],memEnc.GetBuffer (),ReadSize,&dw,NULL))
	{
		char szError[SIZE_STRING];
		ZeroMemory (szError, SIZE_STRING);
		sprintf_s (szError, SIZE_STRING, "Unable to Decrypt Block, due to ReadFile Error %i", GetLastError ());
		OutputText (szError, true);
	}
	
	

	// Decrypt the encrypted block
	for (bpos=0;bpos<memEnc.GetSize ();bpos++) {
		
		encpos = (ReadOffset + bpos) % lciphersize;
		
		bp = memEnc.GetByte (bpos);

		if (bseparateencryption == true) { // Check if this mounted drive is using an individual password, if so we need to use a different encryption key
			bk = m_pinstance->m_memIndividualcipher[iDriveresource].GetByte (encpos);
		} else {
			bk = m_pinstance->m_memCipher.GetByte (encpos);
		}
		
		be = bp ^ bk;
		
		memEnc.SetByte (bpos, be);
	}

	// Copy the decrypted block from the memory buffer (now decrypted) into the file disk read buffer
	memcpy (ReadBuffer, memEnc.GetBuffer (), ReadSize);
	memEnc.Clear ();

    //BOOL result = ReadFile(g_hFile,ReadBuffer,ReadSize,&dw,NULL); // Without decryption
    *BytesRead = dw;
    return true;
}


bool VirtualDriveManager::EncryptBlock (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten, int iDriveresource)
{
	
	unsigned long bpos;
	unsigned long long encpos;
	unsigned long long lciphersize; 
	BYTE bp;
	BYTE bk;
	BYTE be;
    
	//m_pinstance->OutputInt ("OnWriteCallback, Size: ", WriteSize);
	bool bseparateencryption = m_pinstance->m_Drivelist[iDriveresource].bUsingseperatepassword;

	if (bseparateencryption == true) {
		lciphersize = m_pinstance->m_memIndividualcipher[iDriveresource].GetSize();
	} else {
		lciphersize = m_pinstance->m_memCipher.GetSize ();
	}

	MemoryBuffer memEnc;
	memEnc.SetSize (WriteSize);
	// Copy the write buffer into our temporary buffer
	memEnc.Write ((BYTE *) WriteBuffer, 0, WriteSize); 
	
	// Encrypt our temporary buffer
	for (bpos=0;bpos<memEnc.GetSize ();bpos++) {
		
		encpos = (WriteOffset + bpos) % lciphersize;		

		bp = memEnc.GetByte (bpos);

		if (bseparateencryption == true) {
			bk = m_pinstance->m_memIndividualcipher[iDriveresource].GetByte (encpos);
		} else {
			bk = m_pinstance->m_memCipher.GetByte (encpos);
		}
		
		be = bp ^ bk;
		
		memEnc.SetByte (bpos, be);
	}


    LARGE_INTEGER l;
	LARGE_INTEGER newpointer;
    DWORD dw;
    l.QuadPart = WriteOffset + m_Offsetvalues[iDriveresource];
    //SetFilePointer(g_hFile,l.LowPart,&l.HighPart,FILE_BEGIN); // 32-bit only limited to 4GB
	SetFilePointerEx (m_pinstance->m_hFileResource[iDriveresource], l, &newpointer, FILE_BEGIN);
    //BOOL result = WriteFile(g_hFile,WriteBuffer,WriteSize,&dw,NULL); // Write without encryption

	// Write our encrypted buffer to the file disk
	if (!WriteFile (m_pinstance->m_hFileResource[iDriveresource], memEnc.GetBuffer(), WriteSize, &dw, NULL))
	{
		char szError[SIZE_STRING];
		ZeroMemory (szError, SIZE_STRING);
		sprintf_s (szError, SIZE_STRING, "Unable to Encrypt Block, due to WriteFile Error %i", GetLastError ());
		OutputText (szError, true);
	}

	memEnc.Clear ();

    *BytesWritten = dw;

    return true;
}


BOOL _stdcall VirtualDriveManager::OnReadCallback0 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead) {return m_pinstance->DecryptBlock (h, ReadOffset, ReadSize, ReadBuffer, BytesRead, 0);}
BOOL _stdcall VirtualDriveManager::OnReadCallback1 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead) {return m_pinstance->DecryptBlock (h, ReadOffset, ReadSize, ReadBuffer, BytesRead, 1);}
BOOL _stdcall VirtualDriveManager::OnReadCallback2 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead) {return m_pinstance->DecryptBlock (h, ReadOffset, ReadSize, ReadBuffer, BytesRead, 2);}
BOOL _stdcall VirtualDriveManager::OnReadCallback3 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead) {return m_pinstance->DecryptBlock (h, ReadOffset, ReadSize, ReadBuffer, BytesRead, 3);}
BOOL _stdcall VirtualDriveManager::OnReadCallback4 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead) {return m_pinstance->DecryptBlock (h, ReadOffset, ReadSize, ReadBuffer, BytesRead, 4);}
BOOL _stdcall VirtualDriveManager::OnReadCallback5 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead) {return m_pinstance->DecryptBlock (h, ReadOffset, ReadSize, ReadBuffer, BytesRead, 5);}
BOOL _stdcall VirtualDriveManager::OnReadCallback6 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead) {return m_pinstance->DecryptBlock (h, ReadOffset, ReadSize, ReadBuffer, BytesRead, 6);}
BOOL _stdcall VirtualDriveManager::OnReadCallback7 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead) {return m_pinstance->DecryptBlock (h, ReadOffset, ReadSize, ReadBuffer, BytesRead, 7);}
BOOL _stdcall VirtualDriveManager::OnReadCallback8 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead) {return m_pinstance->DecryptBlock (h, ReadOffset, ReadSize, ReadBuffer, BytesRead, 8);}
BOOL _stdcall VirtualDriveManager::OnReadCallback9 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead) {return m_pinstance->DecryptBlock (h, ReadOffset, ReadSize, ReadBuffer, BytesRead, 9);}
BOOL _stdcall VirtualDriveManager::OnReadCallback10 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead) {return m_pinstance->DecryptBlock (h, ReadOffset, ReadSize, ReadBuffer, BytesRead, 10);}
BOOL _stdcall VirtualDriveManager::OnReadCallback11 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead) {return m_pinstance->DecryptBlock (h, ReadOffset, ReadSize, ReadBuffer, BytesRead, 11);}
BOOL _stdcall VirtualDriveManager::OnReadCallback12 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead) {return m_pinstance->DecryptBlock (h, ReadOffset, ReadSize, ReadBuffer, BytesRead, 12);}
BOOL _stdcall VirtualDriveManager::OnReadCallback13 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead) {return m_pinstance->DecryptBlock (h, ReadOffset, ReadSize, ReadBuffer, BytesRead, 13);}
BOOL _stdcall VirtualDriveManager::OnReadCallback14 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead) {return m_pinstance->DecryptBlock (h, ReadOffset, ReadSize, ReadBuffer, BytesRead, 14);}
BOOL _stdcall VirtualDriveManager::OnReadCallback15 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead) {return m_pinstance->DecryptBlock (h, ReadOffset, ReadSize, ReadBuffer, BytesRead, 15);}
BOOL _stdcall VirtualDriveManager::OnReadCallback16 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead) {return m_pinstance->DecryptBlock (h, ReadOffset, ReadSize, ReadBuffer, BytesRead, 16);}
BOOL _stdcall VirtualDriveManager::OnReadCallback17 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead) {return m_pinstance->DecryptBlock (h, ReadOffset, ReadSize, ReadBuffer, BytesRead, 17);}
BOOL _stdcall VirtualDriveManager::OnReadCallback18 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead) {return m_pinstance->DecryptBlock (h, ReadOffset, ReadSize, ReadBuffer, BytesRead, 18);}
BOOL _stdcall VirtualDriveManager::OnReadCallback19 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead) {return m_pinstance->DecryptBlock (h, ReadOffset, ReadSize, ReadBuffer, BytesRead, 19);}
BOOL _stdcall VirtualDriveManager::OnReadCallback20 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead) {return m_pinstance->DecryptBlock (h, ReadOffset, ReadSize, ReadBuffer, BytesRead, 20);}
BOOL _stdcall VirtualDriveManager::OnReadCallback21 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead) {return m_pinstance->DecryptBlock (h, ReadOffset, ReadSize, ReadBuffer, BytesRead, 21);}
BOOL _stdcall VirtualDriveManager::OnReadCallback22 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead) {return m_pinstance->DecryptBlock (h, ReadOffset, ReadSize, ReadBuffer, BytesRead, 22);}
BOOL _stdcall VirtualDriveManager::OnReadCallback23 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead) {return m_pinstance->DecryptBlock (h, ReadOffset, ReadSize, ReadBuffer, BytesRead, 23);}
BOOL _stdcall VirtualDriveManager::OnReadCallback24 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead) {return m_pinstance->DecryptBlock (h, ReadOffset, ReadSize, ReadBuffer, BytesRead, 24);}
BOOL _stdcall VirtualDriveManager::OnReadCallback25 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead) {return m_pinstance->DecryptBlock (h, ReadOffset, ReadSize, ReadBuffer, BytesRead, 25);}
BOOL _stdcall VirtualDriveManager::OnReadCallback26 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead) {return m_pinstance->DecryptBlock (h, ReadOffset, ReadSize, ReadBuffer, BytesRead, 26);}
BOOL _stdcall VirtualDriveManager::OnReadCallback27 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead) {return m_pinstance->DecryptBlock (h, ReadOffset, ReadSize, ReadBuffer, BytesRead, 27);}
BOOL _stdcall VirtualDriveManager::OnReadCallback28 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead) {return m_pinstance->DecryptBlock (h, ReadOffset, ReadSize, ReadBuffer, BytesRead, 28);}
BOOL _stdcall VirtualDriveManager::OnReadCallback29 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead) {return m_pinstance->DecryptBlock (h, ReadOffset, ReadSize, ReadBuffer, BytesRead, 29);}


BOOL _stdcall VirtualDriveManager::OnWriteCallback0 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten) {return m_pinstance->EncryptBlock (h, WriteOffset, WriteSize, WriteBuffer, BytesWritten, 0);}
BOOL _stdcall VirtualDriveManager::OnWriteCallback1 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten) {return m_pinstance->EncryptBlock (h, WriteOffset, WriteSize, WriteBuffer, BytesWritten, 1);}
BOOL _stdcall VirtualDriveManager::OnWriteCallback2 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten) {return m_pinstance->EncryptBlock (h, WriteOffset, WriteSize, WriteBuffer, BytesWritten, 2);}
BOOL _stdcall VirtualDriveManager::OnWriteCallback3 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten) {return m_pinstance->EncryptBlock (h, WriteOffset, WriteSize, WriteBuffer, BytesWritten, 3);}
BOOL _stdcall VirtualDriveManager::OnWriteCallback4 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten) {return m_pinstance->EncryptBlock (h, WriteOffset, WriteSize, WriteBuffer, BytesWritten, 4);}
BOOL _stdcall VirtualDriveManager::OnWriteCallback5 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten) {return m_pinstance->EncryptBlock (h, WriteOffset, WriteSize, WriteBuffer, BytesWritten, 5);}
BOOL _stdcall VirtualDriveManager::OnWriteCallback6 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten) {return m_pinstance->EncryptBlock (h, WriteOffset, WriteSize, WriteBuffer, BytesWritten, 6);}
BOOL _stdcall VirtualDriveManager::OnWriteCallback7 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten) {return m_pinstance->EncryptBlock (h, WriteOffset, WriteSize, WriteBuffer, BytesWritten, 7);}
BOOL _stdcall VirtualDriveManager::OnWriteCallback8 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten) {return m_pinstance->EncryptBlock (h, WriteOffset, WriteSize, WriteBuffer, BytesWritten, 8);}
BOOL _stdcall VirtualDriveManager::OnWriteCallback9 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten) {return m_pinstance->EncryptBlock (h, WriteOffset, WriteSize, WriteBuffer, BytesWritten, 9);}
BOOL _stdcall VirtualDriveManager::OnWriteCallback10 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten) {return m_pinstance->EncryptBlock (h, WriteOffset, WriteSize, WriteBuffer, BytesWritten, 10);}
BOOL _stdcall VirtualDriveManager::OnWriteCallback11 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten) {return m_pinstance->EncryptBlock (h, WriteOffset, WriteSize, WriteBuffer, BytesWritten, 11);}
BOOL _stdcall VirtualDriveManager::OnWriteCallback12 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten) {return m_pinstance->EncryptBlock (h, WriteOffset, WriteSize, WriteBuffer, BytesWritten, 12);}
BOOL _stdcall VirtualDriveManager::OnWriteCallback13 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten) {return m_pinstance->EncryptBlock (h, WriteOffset, WriteSize, WriteBuffer, BytesWritten, 13);}
BOOL _stdcall VirtualDriveManager::OnWriteCallback14 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten) {return m_pinstance->EncryptBlock (h, WriteOffset, WriteSize, WriteBuffer, BytesWritten, 14);}
BOOL _stdcall VirtualDriveManager::OnWriteCallback15 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten) {return m_pinstance->EncryptBlock (h, WriteOffset, WriteSize, WriteBuffer, BytesWritten, 15);}
BOOL _stdcall VirtualDriveManager::OnWriteCallback16 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten) {return m_pinstance->EncryptBlock (h, WriteOffset, WriteSize, WriteBuffer, BytesWritten, 16);}
BOOL _stdcall VirtualDriveManager::OnWriteCallback17 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten) {return m_pinstance->EncryptBlock (h, WriteOffset, WriteSize, WriteBuffer, BytesWritten, 17);}
BOOL _stdcall VirtualDriveManager::OnWriteCallback18 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten) {return m_pinstance->EncryptBlock (h, WriteOffset, WriteSize, WriteBuffer, BytesWritten, 18);}
BOOL _stdcall VirtualDriveManager::OnWriteCallback19 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten) {return m_pinstance->EncryptBlock (h, WriteOffset, WriteSize, WriteBuffer, BytesWritten, 19);}
BOOL _stdcall VirtualDriveManager::OnWriteCallback20 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten) {return m_pinstance->EncryptBlock (h, WriteOffset, WriteSize, WriteBuffer, BytesWritten, 20);}
BOOL _stdcall VirtualDriveManager::OnWriteCallback21 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten) {return m_pinstance->EncryptBlock (h, WriteOffset, WriteSize, WriteBuffer, BytesWritten, 21);}
BOOL _stdcall VirtualDriveManager::OnWriteCallback22 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten) {return m_pinstance->EncryptBlock (h, WriteOffset, WriteSize, WriteBuffer, BytesWritten, 22);}
BOOL _stdcall VirtualDriveManager::OnWriteCallback23 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten) {return m_pinstance->EncryptBlock (h, WriteOffset, WriteSize, WriteBuffer, BytesWritten, 23);}
BOOL _stdcall VirtualDriveManager::OnWriteCallback24 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten) {return m_pinstance->EncryptBlock (h, WriteOffset, WriteSize, WriteBuffer, BytesWritten, 24);}
BOOL _stdcall VirtualDriveManager::OnWriteCallback25 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten) {return m_pinstance->EncryptBlock (h, WriteOffset, WriteSize, WriteBuffer, BytesWritten, 25);}
BOOL _stdcall VirtualDriveManager::OnWriteCallback26 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten) {return m_pinstance->EncryptBlock (h, WriteOffset, WriteSize, WriteBuffer, BytesWritten, 26);}
BOOL _stdcall VirtualDriveManager::OnWriteCallback27 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten) {return m_pinstance->EncryptBlock (h, WriteOffset, WriteSize, WriteBuffer, BytesWritten, 27);}
BOOL _stdcall VirtualDriveManager::OnWriteCallback28 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten) {return m_pinstance->EncryptBlock (h, WriteOffset, WriteSize, WriteBuffer, BytesWritten, 28);}
BOOL _stdcall VirtualDriveManager::OnWriteCallback29 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten) {return m_pinstance->EncryptBlock (h, WriteOffset, WriteSize, WriteBuffer, BytesWritten, 29);}

BOOL _stdcall VirtualDriveManager::OnFormatCallbackHandler(ULONG Progress)
{
	return TRUE;
}

int VirtualDriveManager::GetFreeDriveResource ()
{
	for (int i=0;i<MAX_DRIVES;i++) 
	{
		if (m_bResourceBusy[i] == false) {
			return i;
		}
	}

	// No free drive resource available - this must mean all the virtual drives
	// are currently in use.
	return -1;
}

unsigned long VirtualDriveManager::GetVirtualDiskSizeMegs (char *szPath)
{
	HANDLE hFile;
	hFile = CreateFile(szPath, GENERIC_READ | GENERIC_WRITE,FILE_SHARE_READ,0,OPEN_ALWAYS,FILE_FLAG_NO_BUFFERING | FILE_FLAG_RANDOM_ACCESS,0);

    if(hFile == INVALID_HANDLE_VALUE)
    {
		OutputText ("ERROR: can not create / open disk image file for getting file size", true);
        return 0;
    }

	LARGE_INTEGER currentSize;
	LARGE_INTEGER newPointerlocation;
    //currentSize.LowPart = GetFileSize(g_hFile,(LPDWORD)&currentSize.HighPart);
    
	GetFileSizeEx (hFile, &currentSize);

	int chunksize = 1024 * 1024;

	unsigned long megs = (currentSize.QuadPart / chunksize);

	CloseHandle (hFile);

	return megs;
}

void VirtualDriveManager::AllocateVirtualDisk (SingleDriveInfo driveinfo)
{
	OutputText ("Allocating drive...", false);

	m_hAllocateFile = CreateFile(driveinfo.szPath, GENERIC_READ | GENERIC_WRITE,FILE_SHARE_READ,0,
        OPEN_ALWAYS,FILE_FLAG_NO_BUFFERING | FILE_FLAG_RANDOM_ACCESS,0);

    if(m_hAllocateFile == INVALID_HANDLE_VALUE)
    {
		OutputText ("ERROR: can not create / open disk image file for allocation", true);
        return;
    }

	currentAllocationInfo = driveinfo;

    // allocate disk space
    //AllocateDiskSpace();
	DoThread_AllocateDiskSpace();
}

bool VirtualDriveManager::AllocateStandaloneVirtualDisk (SingleDriveInfo driveinfo)
{
	OutputText ("Allocating drive...", false);

	m_hAllocateFile = CreateFile(driveinfo.szPath, GENERIC_READ | GENERIC_WRITE,FILE_SHARE_READ,0,
        OPEN_ALWAYS,FILE_FLAG_NO_BUFFERING | FILE_FLAG_RANDOM_ACCESS,0);

    if(m_hAllocateFile == INVALID_HANDLE_VALUE)
    {
		OutputText ("ERROR: can not create / open disk image file for allocation", true);
        return false;
    }

	unsigned long long userspecifiedsize = (unsigned long long) driveinfo.lDiskSizeMegs;

	unsigned long long DiskSize = (unsigned long long) userspecifiedsize * 1024 * 1024;
	//unsigned long long DiskSize = 10485760000;
    DWORD dw;

    LARGE_INTEGER currentSize;
	LARGE_INTEGER newPointerlocation;
    //currentSize.LowPart = GetFileSize(g_hFile,(LPDWORD)&currentSize.HighPart);
    
	GetFileSizeEx (m_hAllocateFile, &currentSize);
	
	if(currentSize.QuadPart >= DiskSize)
    {
        // already allocated
        //SetFilePointer(g_hFile,currentSize.LowPart,&currentSize.HighPart,FILE_BEGIN);
		
		SetFilePointerEx (m_hAllocateFile, currentSize, &newPointerlocation, FILE_BEGIN);

        SetEndOfFile(m_hAllocateFile);
		//PostMessage (m_hwnd, WM_UICOMMAND, VD_ALLOCATIONCOMPLETE, 0);
		CloseHandle (m_hAllocateFile);
        return true;
    }
    
	OutputText ("Allocating disk space...", false);

    const int ChunkSize = 1024 * 1024; // 1 MB
    char* ZeroBuffer = new char[ChunkSize];

    unsigned ChunksCount = (unsigned)(DiskSize / ChunkSize);
    for(unsigned i = 0;i<ChunksCount;++i)
    {        

		//ZeroMemory (pwnd->m_szProgressLabel, SIZE_STRING);
		//sprintf_s (pwnd->m_szProgressLabel, SIZE_STRING, "Creating encrypted virtual drive, %i %% complete (written chunk %i of %i)",  (i * 100 / ChunksCount), i, ChunksCount);
		
		m_allocationpercent = (i * 100 / ChunksCount);
		m_currentvalue = i;
		m_maxvalues = ChunksCount;

		//pwnd->OutputInt ("Allocating: ", (i * 100 / ChunksCount));
		//PostMessage (pwnd->m_hwnd, WM_UICOMMAND, VD_ALLOCATIONPROGRESS, 0);


        if(!WriteFile(m_hAllocateFile,ZeroBuffer,ChunkSize,&dw,NULL))
        {            
			//OutputText ("Error!");
        }
   }

    delete[] ZeroBuffer;    
	OutputText ("Allocation completed.", false);
	CloseHandle (m_hAllocateFile);

	// Post a message to the main UI thread informing it that the Allocation has ended
	//PostMessage (pwnd->m_hwnd, WM_UICOMMAND, VD_ALLOCATIONCOMPLETE, 0);

	return true;
}

