
#define _WIN32_WINNT 0x0501 // Change this to the appropriate value to target Windows Me or later.


#include <windows.h>
#include <stdio.h>
#include <Sddl.h>
#include "resource.h"
#include "DynList.h"
#include "SingleDriveInfo.h"
#include "MemoryBuffer.h"
#include "EventDescriptions.h"
#include "EventLogging.h"
#include "VirtualDriveManager.h"
#include "InstructionResult.h"

#define ID_TIMER 1000
#define SIZE_STRING	1024

#define DRIVE_MOUNT					100
#define DRIVE_UNMOUNT			101
#define DRIVE_UNMOUNTALL		102
#define SERVICEPING					103

#define BUFSIZE 384000
 
// Service string constants
static char g_szFriendlyName[] = "CedeDrive Manager Service";
static char g_szServiceName[] = "CedeDriveManagerService";
static char g_szServiceID[] = "CedeDriveService";
static char g_szServiceDesc [] = "Manages the mounting and unmounting of Virtual Encrypted drives used by CedeDrive.";
static char g_szEventsource[] = "CedeDrive";

/*
REMEMBER! EventDescriptions.mc contains all the descriptions of the possible events this service can generate for the
windows event viewer. After you have added or modified events, you must recompile this file using the Message Compiler MC.
e.g. mc EventDescriptions.mc    - You must use the Visual Studio Command Line to do this. This is so that event viewer knows where
to look for the decriptions of your events, because every time you log and event, you log it using it's ID which is stored in the event descriptions
file. I don't see why Microsoft couldn't have just provided a simple ReportEvent function taking in a string, but no they want us to create a whole
separate event file, add a source to the registry and only then will event viewer see the descriptions.

This is now a pre build event in the visual studio project, so manual compiling should not be necessary
ALWAYS edit EventDescriptions.mc - do NOT edit EventDescriptions.h or EventDescriptions.rc. These are modified by the message compiler

*/
VirtualDriveManager* VirtualDriveManager::m_pinstance = NULL;

static char g_szPipename[] = "\\\\.\\pipe\\CedeDriveServicePipe";

SERVICE_STATUS MyServiceStatus; 
SERVICE_STATUS_HANDLE MyServiceStatusHandle; 
HINSTANCE g_hInstance;
EventLogging g_log;
VirtualDriveManager g_vdmanager;
SingleDriveInfo g_driveinfo;

// Prototypes
void  WINAPI MyServiceStart (DWORD argc, LPTSTR *argv); 
void  WINAPI MyServiceCtrlHandler (DWORD opcode);
DWORD MyServiceInitialization (DWORD argc, LPTSTR *argv, DWORD *specificError); 
VOID SvcDebugOut(LPSTR String, DWORD Status);
int ServiceMain ();
VOID InstallService (LPCTSTR lpszBinaryPathName);
VOID UninstallService ();
VOID ClientInstanceThread(LPVOID); 

// Implementation
#pragma region Service control handling

VOID WINAPI MyServiceCtrlHandler (DWORD Opcode)  { 
	DWORD status; 
 
	switch(Opcode) 
	{ 
		case SERVICE_CONTROL_PAUSE: 
		// Do whatever it takes to pause here. 
		MyServiceStatus.dwCurrentState = SERVICE_PAUSED; 
		break; 
 
		case SERVICE_CONTROL_CONTINUE: 
		// Do whatever it takes to continue here. 
		MyServiceStatus.dwCurrentState = SERVICE_RUNNING; 
		break; 
 
		case SERVICE_CONTROL_STOP: 
		// Do whatever it takes to stop here. 
		MyServiceStatus.dwWin32ExitCode = 0; 
		MyServiceStatus.dwCurrentState  = SERVICE_STOPPED; 
		MyServiceStatus.dwCheckPoint    = 0; 
		MyServiceStatus.dwWaitHint      = 0; 
		g_log.ReportEventLog (MSG_SERVICE_STOPPED, "",  INFO);
		g_vdmanager.UnmountAllVirtualDrives ();
		if (!SetServiceStatus (MyServiceStatusHandle,&MyServiceStatus)) { 
			status = GetLastError(); 
		} 
		return; 
 
		case SERVICE_CONTROL_INTERROGATE:
			{
			}
			break;

		default:
			{
			}
			break;
	} 
 
	// Send current status. 
	if (!SetServiceStatus (MyServiceStatusHandle,  &MyServiceStatus)) { 
		status = GetLastError(); 
	} 
	return; 
} 

void WINAPI MyServiceStart (DWORD argc, LPTSTR *argv) { 
	DWORD status; 
	DWORD specificError; 
 
	MyServiceStatus.dwServiceType = SERVICE_WIN32; 
	MyServiceStatus.dwCurrentState = SERVICE_START_PENDING; 
	MyServiceStatus.dwControlsAccepted = SERVICE_ACCEPT_STOP | SERVICE_ACCEPT_PAUSE_CONTINUE; 
	MyServiceStatus.dwWin32ExitCode = 0; 
	MyServiceStatus.dwServiceSpecificExitCode = 0; 
	MyServiceStatus.dwCheckPoint = 0; 
	MyServiceStatus.dwWaitHint = 0; 
 
	MyServiceStatusHandle = RegisterServiceCtrlHandler(g_szServiceID, MyServiceCtrlHandler); 
 
	if (MyServiceStatusHandle == (SERVICE_STATUS_HANDLE)0) { 
		return; 
	} 
 
	// Initialization code goes here. 
	status = MyServiceInitialization(argc,argv, &specificError); 
 
	// Handle error condition 
	if (status != NO_ERROR) { 
		MyServiceStatus.dwCurrentState = SERVICE_STOPPED; 
		MyServiceStatus.dwCheckPoint = 0; 
		MyServiceStatus.dwWaitHint = 0; 
		MyServiceStatus.dwWin32ExitCode = status; 
		MyServiceStatus.dwServiceSpecificExitCode = specificError; 
		SetServiceStatus (MyServiceStatusHandle, &MyServiceStatus); 
		return;
	}
 
	// Initialization complete - report running status. 
	MyServiceStatus.dwCurrentState = SERVICE_RUNNING;
	MyServiceStatus.dwCheckPoint = 0; 
	MyServiceStatus.dwWaitHint = 0; 
 
	if (!SetServiceStatus (MyServiceStatusHandle, &MyServiceStatus)) { 
		status = GetLastError(); 
	} 
 
	// This is where the service does its work. 
	ServiceMain ();
	return; 
} 
 
// Stub initialization function. 
DWORD MyServiceInitialization(DWORD   argc, LPTSTR  *argv, DWORD *specificError) { 
    argv;
    argc; 
    specificError;
    return(0);
}
 
#pragma endregion


int ServiceMain ()
{

	// Set up the Event Logging
	g_log.SetEventsource (g_szEventsource);
	g_log.AddEventSource ("Application", g_szEventsource, 0); // Add the event log source to the registry - needed so the windows event log can view event descriptions.
	g_log.ReportEventLog (MSG_GENERICINFO, "The CedeDrive service was started successfully.", INFO); // Report the service started event
	
	// Set up the Virtual Drive Engine (VDSDK), and give it the pointer to the Event Logging class
	g_vdmanager.SetEventLogging (&g_log);
	g_vdmanager.Initialise ("password"); // The password supplied here is not used as each drive will be mounted with it's own password

	BOOL fConnected; 
	DWORD dwThreadId; 
	HANDLE hPipe, hThread; 
	LPTSTR lpszPipename = TEXT(g_szPipename); 
 
	SECURITY_ATTRIBUTES sa;
	TCHAR * szSD = TEXT("D:")       // Discretionary ACL
	TEXT("(A;OICI;GA;;;S-1-1-0)");    // Allow full control to everyone

	sa.nLength = sizeof(SECURITY_ATTRIBUTES);
	sa.bInheritHandle = FALSE;
	ConvertStringSecurityDescriptorToSecurityDescriptor (szSD, SDDL_REVISION_1, &(sa.lpSecurityDescriptor), NULL);

	// The main loop creates an instance of the named pipe and 
	// then waits for a client to connect to it. When the client 
	// connects, a thread is created to handle communications 
	// with that client, and the loop is repeated. 
 
   for (;;) 
   { 
      hPipe = CreateNamedPipe( 
          lpszPipename,             // pipe name 
          PIPE_ACCESS_DUPLEX,       // read/write access 
          PIPE_TYPE_MESSAGE |       // message type pipe 
          PIPE_READMODE_MESSAGE |   // message-read mode 
          PIPE_WAIT,                // blocking mode 
          PIPE_UNLIMITED_INSTANCES, // max. instances  
          BUFSIZE,                  // output buffer size 
          BUFSIZE,                  // input buffer size 
          NMPWAIT_USE_DEFAULT_WAIT, // client time-out 
          &sa);                    // default security attribute 

      if (hPipe == INVALID_HANDLE_VALUE) 
      {
          g_log.ReportEventLog (MSG_PIPECREATION_FAILED, "", ERR); 
          return 0;
      }
 
      // Wait for the client to connect; if it succeeds, 
      // the function returns a nonzero value. If the function
      // returns zero, GetLastError returns ERROR_PIPE_CONNECTED. 
 
      fConnected = ConnectNamedPipe(hPipe, NULL) ? 
         TRUE : (GetLastError() == ERROR_PIPE_CONNECTED); 
 
      if (fConnected) 
      { 
      // Create a thread for this client. 
         hThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE) ClientInstanceThread, (LPVOID) hPipe, 0, &dwThreadId);

         if (hThread == NULL) 
         {
            g_log.ReportEventLog (MSG_CLIENTTHREAD_FAILED, "", ERR); 
            return 0;
         }
         else CloseHandle(hThread); 
       } 
      else 
        // The client could not connect, so close the pipe. 
         CloseHandle(hPipe); 
   } 
   return 1;
}

VOID ClientInstanceThread(LPVOID lpvParam) 
{
	// Everytime a named pipe client connects to this service, a new thread is created, and this method is called within that new thread
	// which services the request. The request is going to be stuff like MOUNT this drive, or UNMOUNT this drive. The reason we're doing it
	// in this service /  getting a service to control mounting and unmounting is because services run as the Local System account (normally)
	// and this will allow a User without admin privileges to mount virtual drives, which normally requires admin rights.

	// Some info: I got into a bit of an issue when I was allocating more than 1MB of memory within this thread. After I did that I was unable
	// to write to SingleDriveInfo structures that had been newly created. After some research it appears that windows threads have a default
	// stack size of 1MB which can be changed. Instead I've reduced the amount of memory that is used in a single thread.

	DWORD cbBytesRead, cbReplyBytes, cbWritten; 
	BOOL fSuccess; 
	HANDLE hPipe; 
	SingleDriveInfo driveinfo;
	InstructionResult result;

	MemoryBuffer memRcv;
	memRcv.SetSize (BUFSIZE);

	// The thread's parameter is a handle to a pipe instance. 
	hPipe = (HANDLE) lpvParam; 

	// Read client requests from the pipe. 
	fSuccess = ReadFile (hPipe, memRcv.GetBuffer (), BUFSIZE, &cbBytesRead, NULL);			

	if (cbBytesRead > 0) {
		// Copy what we've received into a separate memory buffer with the correct size
		MemoryBuffer memList;
		memList.SetSize (cbBytesRead);
		memList.Write (memRcv.GetBuffer(), 0, cbBytesRead);
		memRcv.Clear ();

		// Deserialise back to a dynlist;
		DynList dlRequestlist;
		dlRequestlist.FromMemoryBuffer (&memList);

		// Print out the list
		SingleDriveInfo *pinfo;

		// When we receive a communication from the named pipe, it will contain a DynList of SingleDriveInfo objects
		// each SingleDriveInfo object contains the instruction for that drive, such as MOUNT or UNMOUNT.

		for (unsigned int i=0;i<dlRequestlist.GetNumItems ();i++) {
			pinfo = (SingleDriveInfo *) dlRequestlist.GetItem (i);

			// This is the point at which we read the instruction from each drive info structure we've been given
			// and then depending on the instruction we either mount or unmount the drives.
			
			ZeroMemory (&driveinfo, sizeof (SingleDriveInfo));
			memcpy (&driveinfo, (SingleDriveInfo *) pinfo, sizeof (SingleDriveInfo));
			
			// Process the instruction to mount a drive
			if (pinfo->iInstruction == DRIVE_MOUNT) {
				int ires = g_vdmanager.MountVirtualDisk (driveinfo);

				ZeroMemory (&result, sizeof (InstructionResult));
				if (ires != 0) {
					result.iResult = ires;
					g_vdmanager.GetMountError (result.szDescription);
					result.bSuccess = false;
				} else {
					result.iResult = 0;
					strcpy_s (result.szDescription, SIZE_STRING, "The mount operation was successful.");
					result.bSuccess = true;
				}
			}

			// Process the instruction to unmount a drive
			if (pinfo->iInstruction == DRIVE_UNMOUNT) {
				g_vdmanager.UnmountVirtualDrive (driveinfo);

				ZeroMemory (&result, sizeof (InstructionResult));
				result.iResult = 0;
				strcpy_s (result.szDescription, SIZE_STRING, "The un-mount operation was successful.");
				result.bSuccess = true;
			}

			// Process the instruction to unmount all drives
			if (pinfo->iInstruction == DRIVE_UNMOUNTALL) {
				g_vdmanager.UnmountAllVirtualDrives ();

				ZeroMemory (&result, sizeof (InstructionResult));
				result.iResult = 0;
				strcpy_s (result.szDescription, SIZE_STRING, "The un-mount operation of all drives was successful.");
				result.bSuccess = true;
			}

			if (pinfo->iInstruction == SERVICEPING) {
				// This is just a ping message, so basically we return success. Clients use this
				// instruction to test if we're here and responding.
				ZeroMemory (&result, sizeof (InstructionResult));
				result.iResult = 0;
				strcpy_s (result.szDescription, SIZE_STRING, "The CedeDrive Manager service is up and running.");
				result.bSuccess = true;
			}
		}
	}

	// Write the result back to the caller.
	MemoryBuffer memResult;
	memResult.SetSize (sizeof (InstructionResult));
	memResult.Write (&result, 0, sizeof (InstructionResult)); 

	// Write the reply to the pipe. 
	fSuccess = WriteFile(hPipe, memResult.GetBuffer (), memResult.GetSize (), &cbWritten, NULL);
 
	FlushFileBuffers(hPipe); 
	DisconnectNamedPipe(hPipe); 
	CloseHandle(hPipe); 
}


#pragma region Service installation and WinMain

int WINAPI WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	char szCommand[SIZE_STRING];
	ZeroMemory (szCommand, SIZE_STRING);
	strncpy_s (szCommand, SIZE_STRING, lpCmdLine, 2);

	bool bUsingcommand = false;
	if (strcmp (szCommand, "/i") == 0) {
		// Install the service
		bUsingcommand = true;
		char szModulefilename[SIZE_STRING];
		ZeroMemory (szModulefilename, SIZE_STRING);
		GetModuleFileName(NULL, szModulefilename, SIZE_STRING);
		//strcat_s (szModulefilename, SIZE_STRING, " /s");

		InstallService (szModulefilename);
	}

	if (strcmp (szCommand, "/s") == 0) {
		// Start the service
		bUsingcommand = true;

	}

	if (strcmp (szCommand, "/d") == 0) {
		// Delete the service
		bUsingcommand = true;
		UninstallService ();
	}

	if (strcmp (szCommand, "/r") == 0) {
		// Run as application
		bUsingcommand = true;
		ServiceMain();
	}

	
	SERVICE_TABLE_ENTRY   DispatchTable[] = {{ TEXT(g_szServiceID), MyServiceStart}, {NULL,NULL}}; 
 
	if (bUsingcommand == false) {
		if (!StartServiceCtrlDispatcher( DispatchTable))  { 
			
		}
	}
}

VOID InstallService (LPCTSTR lpszBinaryPathName) {
	
	// The below is not supported in windows NT - we have to take this out.
	SERVICE_DESCRIPTION scDesc = {g_szServiceDesc};
	
	SC_HANDLE schSCManager = OpenSCManager(NULL, NULL,  SC_MANAGER_ALL_ACCESS);
	if (schSCManager == NULL) {
		return;
	}

	SC_HANDLE schService = CreateService(
		schSCManager,              // SCManager database 
		g_szServiceName,        // name of service 
		g_szFriendlyName,           // service name to display 
		SERVICE_ALL_ACCESS,        // desired access 
		SERVICE_WIN32_OWN_PROCESS, // service type 
		SERVICE_AUTO_START,      // start type 
		SERVICE_ERROR_NORMAL,      // error control type 
		lpszBinaryPathName,        // service's binary 
		NULL,                      // no load ordering group 
		NULL,                      // no tag identifier 
		NULL,                      // no dependencies 
		NULL,                      // LocalSystem account 
		NULL);                     // no password 
 
	if (schService == NULL) {
	
	} else {
		if (ChangeServiceConfig2 (schService, SERVICE_CONFIG_DESCRIPTION, &scDesc) == 0) {
		//	MessageBox (NULL, "Could not set Description.", "Error", MB_OK | MB_ICONEXCLAMATION); // Not Supported on Windows NT.
		}
		//MessageBox (NULL, "COE Desktop Alert Service Installed Successfully.", "Info", MB_ICONINFORMATION | MB_OK);

		StartService (schService, 0, NULL);
	}
 
	CloseServiceHandle(schService); 
}

void StopService () {
	SERVICE_STATUS ssStatus;

	SC_HANDLE schSCManager = OpenSCManager(NULL, NULL,  SC_MANAGER_ALL_ACCESS);
	if (schSCManager == NULL) {
		return;
	}

	SC_HANDLE schService = OpenService (schSCManager, g_szServiceName, SERVICE_STOP);
	
	if (schService == NULL) {
		return;
	}
	
	if (ControlService (schService, SERVICE_CONTROL_STOP, &ssStatus) == 0) {
		DWORD dwError = GetLastError ();

		char szMsg[SIZE_STRING];
		ZeroMemory (szMsg, SIZE_STRING);
		sprintf_s (szMsg, SIZE_STRING, "Unable to stop service, error %i", dwError);
		
	}
	
	CloseServiceHandle(schService); 
	CloseServiceHandle(schSCManager);
}

VOID UninstallService () {
	SERVICE_STATUS ssStatus;

	SC_HANDLE schSCManager = OpenSCManager(NULL, NULL,  SC_MANAGER_ALL_ACCESS);
	if (schSCManager == NULL) {
		return;
	}

	SC_HANDLE schService = OpenService (schSCManager, g_szServiceName, STANDARD_RIGHTS_REQUIRED);
	
	if (schService == NULL) {
		return;
	}
	
	StopService ();

	if (DeleteService (schService) == 0) {
		
	} else {
		//MessageBox (NULL, "COE Desktop Alert Service Removed Successfully.", "Info", MB_ICONINFORMATION | MB_OK);
	}
	
	CloseServiceHandle(schService); 
	CloseServiceHandle(schSCManager); 
}

VOID SvcDebugOut(LPSTR String, DWORD Status) { 
	CHAR  Buffer[1024]; 
	if (strlen(String) < 1000) { 
		sprintf(Buffer, String, Status); 
		OutputDebugStringA(Buffer); 
	} 
}

#pragma endregion
