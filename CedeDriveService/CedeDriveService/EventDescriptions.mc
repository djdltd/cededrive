; // ***** EventDescriptions.mc *****
; // This is the header section.

MessageIdTypedef=DWORD

SeverityNames=(Success=0x0:STATUS_SEVERITY_SUCCESS
	Informational=0x1:STATUS_SEVERITY_INFORMATIONAL
	Warning=0x2:STATUS_SEVERITY_WARNING
	Error=0x3:STATUS_SEVERITY_ERROR
	)

FacilityNames=(System=0x0:FACILITY_SYSTEM
	Runtime=0x2:FACILITY_RUNTIME
	Stubs=0x3:FACILITY_STUBS
	Io=0x4:FACILITY_IO_ERROR_CODE
	)

LanguageNames=(English=0x409:MSG00409)

; // The following are message definitions
MessageId=0x100
Severity=Informational
Facility=Runtime
SymbolicName=MSG_SERVICE_STARTED
Language=English
The CedeDrive service was started %1 successfully.
.
MessageId=0x101
Severity=Informational
Facility=Runtime
SymbolicName=MSG_SERVICE_STOPPED
Language=English
The CedeDrive service was stopped.
.
MessageId=0x102
Severity=Error
Facility=Runtime
SymbolicName=MSG_PIPECREATION_FAILED
Language=English
CedeDrive was unable to create the named pipe for incoming requests.
.
MessageId=0x103
Severity=Error
Facility=Runtime
SymbolicName=MSG_CLIENTTHREAD_FAILED
Language=English
CedeDrive was unable to create a new thread to service an incoming client.
.
MessageId=0x104
Severity=Error
Facility=Runtime
SymbolicName=MSG_GENERICERROR
Language=English
%1
.
MessageId=0x105
Severity=Informational
Facility=Runtime
SymbolicName=MSG_GENERICINFO
Language=English
%1
.
