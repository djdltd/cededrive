 // ***** EventDescriptions.mc *****
 // This is the header section.
 // The following are message definitions
//
//  Values are 32 bit values laid out as follows:
//
//   3 3 2 2 2 2 2 2 2 2 2 2 1 1 1 1 1 1 1 1 1 1
//   1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0
//  +---+-+-+-----------------------+-------------------------------+
//  |Sev|C|R|     Facility          |               Code            |
//  +---+-+-+-----------------------+-------------------------------+
//
//  where
//
//      Sev - is the severity code
//
//          00 - Success
//          01 - Informational
//          10 - Warning
//          11 - Error
//
//      C - is the Customer code flag
//
//      R - is a reserved bit
//
//      Facility - is the facility code
//
//      Code - is the facility's status code
//
//
// Define the facility codes
//
#define FACILITY_SYSTEM                  0x0
#define FACILITY_STUBS                   0x3
#define FACILITY_RUNTIME                 0x2
#define FACILITY_IO_ERROR_CODE           0x4


//
// Define the severity codes
//
#define STATUS_SEVERITY_WARNING          0x2
#define STATUS_SEVERITY_SUCCESS          0x0
#define STATUS_SEVERITY_INFORMATIONAL    0x1
#define STATUS_SEVERITY_ERROR            0x3


//
// MessageId: MSG_SERVICE_STARTED
//
// MessageText:
//
// The CedeDrive service was started %1 successfully.
//
#define MSG_SERVICE_STARTED              ((DWORD)0x40020100L)

//
// MessageId: MSG_SERVICE_STOPPED
//
// MessageText:
//
// The CedeDrive service was stopped.
//
#define MSG_SERVICE_STOPPED              ((DWORD)0x40020101L)

//
// MessageId: MSG_PIPECREATION_FAILED
//
// MessageText:
//
// CedeDrive was unable to create the named pipe for incoming requests.
//
#define MSG_PIPECREATION_FAILED          ((DWORD)0xC0020102L)

//
// MessageId: MSG_CLIENTTHREAD_FAILED
//
// MessageText:
//
// CedeDrive was unable to create a new thread to service an incoming client.
//
#define MSG_CLIENTTHREAD_FAILED          ((DWORD)0xC0020103L)

//
// MessageId: MSG_GENERICERROR
//
// MessageText:
//
// %1
//
#define MSG_GENERICERROR                 ((DWORD)0xC0020104L)

//
// MessageId: MSG_GENERICINFO
//
// MessageText:
//
// %1
//
#define MSG_GENERICINFO                  ((DWORD)0x40020105L)

