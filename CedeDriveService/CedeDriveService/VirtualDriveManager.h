#include "resource.h"
#include "EventLogging.h"
#include "SingleDriveInfo.h"
#include "StandardEncryption.h"
#include "EVDFileHeader.h"
#include "DriveFormatter.h"
#include "VDSDKDll.h"

#define DISK_SIZE 100
#define MAX_DRIVES	30

class VirtualDriveManager {
	
	public:
		VirtualDriveManager ();
		~VirtualDriveManager ();		


		static DWORD WINAPI ThreadProc_AllocateDiskSpace (PVOID pParam);
		void DoThread_AllocateDiskSpace ();

		static BOOL _stdcall OnReadCallback0 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead);
		static BOOL _stdcall OnReadCallback1 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead);
		static BOOL _stdcall OnReadCallback2 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead);
		static BOOL _stdcall OnReadCallback3 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead);
		static BOOL _stdcall OnReadCallback4 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead);
		static BOOL _stdcall OnReadCallback5 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead);
		static BOOL _stdcall OnReadCallback6 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead);
		static BOOL _stdcall OnReadCallback7 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead);
		static BOOL _stdcall OnReadCallback8 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead);
		static BOOL _stdcall OnReadCallback9 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead);
		static BOOL _stdcall OnReadCallback10 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead);
		static BOOL _stdcall OnReadCallback11 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead);
		static BOOL _stdcall OnReadCallback12 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead);
		static BOOL _stdcall OnReadCallback13 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead);
		static BOOL _stdcall OnReadCallback14 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead);
		static BOOL _stdcall OnReadCallback15 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead);
		static BOOL _stdcall OnReadCallback16 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead);
		static BOOL _stdcall OnReadCallback17 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead);
		static BOOL _stdcall OnReadCallback18 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead);
		static BOOL _stdcall OnReadCallback19 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead);
		static BOOL _stdcall OnReadCallback20 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead);
		static BOOL _stdcall OnReadCallback21 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead);
		static BOOL _stdcall OnReadCallback22 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead);
		static BOOL _stdcall OnReadCallback23 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead);
		static BOOL _stdcall OnReadCallback24 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead);
		static BOOL _stdcall OnReadCallback25 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead);
		static BOOL _stdcall OnReadCallback26 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead);
		static BOOL _stdcall OnReadCallback27 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead);
		static BOOL _stdcall OnReadCallback28 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead);
		static BOOL _stdcall OnReadCallback29 (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead);
		
		static BOOL _stdcall OnWriteCallback0 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten);
		static BOOL _stdcall OnWriteCallback1 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten);
		static BOOL _stdcall OnWriteCallback2 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten);
		static BOOL _stdcall OnWriteCallback3 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten);
		static BOOL _stdcall OnWriteCallback4 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten);
		static BOOL _stdcall OnWriteCallback5 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten);
		static BOOL _stdcall OnWriteCallback6 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten);
		static BOOL _stdcall OnWriteCallback7 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten);
		static BOOL _stdcall OnWriteCallback8 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten);
		static BOOL _stdcall OnWriteCallback9 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten);
		static BOOL _stdcall OnWriteCallback10 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten);
		static BOOL _stdcall OnWriteCallback11 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten);
		static BOOL _stdcall OnWriteCallback12 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten);
		static BOOL _stdcall OnWriteCallback13 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten);
		static BOOL _stdcall OnWriteCallback14 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten);
		static BOOL _stdcall OnWriteCallback15 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten);
		static BOOL _stdcall OnWriteCallback16 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten);
		static BOOL _stdcall OnWriteCallback17 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten);
		static BOOL _stdcall OnWriteCallback18 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten);
		static BOOL _stdcall OnWriteCallback19 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten);
		static BOOL _stdcall OnWriteCallback20 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten);
		static BOOL _stdcall OnWriteCallback21 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten);
		static BOOL _stdcall OnWriteCallback22 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten);
		static BOOL _stdcall OnWriteCallback23 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten);
		static BOOL _stdcall OnWriteCallback24 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten);
		static BOOL _stdcall OnWriteCallback25 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten);
		static BOOL _stdcall OnWriteCallback26 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten);
		static BOOL _stdcall OnWriteCallback27 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten);
		static BOOL _stdcall OnWriteCallback28 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten);
		static BOOL _stdcall OnWriteCallback29 (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten);
		static BOOL _stdcall OnFormatCallbackHandler(ULONG Progress);

		bool DecryptBlock (DRIVE_HANDLE h, ULONGLONG ReadOffset, ULONG ReadSize, void* ReadBuffer, ULONG *BytesRead, int iDriveresource);
		bool EncryptBlock (DRIVE_HANDLE h, ULONGLONG WriteOffset, ULONG WriteSize, const void* WriteBuffer, ULONG *BytesWritten, int iDriveresource);
		bool FileExists (char *FileName);

		// External useable methods
		void SetHWND (HWND hWnd);
		int MountVirtualDisk (SingleDriveInfo driveinfo);
		void UnmountVirtualDrive (SingleDriveInfo driveinfo);
		void UnmountAllVirtualDrives ();
		bool IsDriveMounted (SingleDriveInfo driveinfo);

		void GenFilename (char *szOutfilename, int iSize);
		void AllocateVirtualDisk (SingleDriveInfo driveinfo);
		bool AllocateStandaloneVirtualDisk (SingleDriveInfo driveinfo);
		void Initialise (char *szEncryptPassword);
		int GetFreeDriveResource ();
		void GetMountError (char *szOutmessage);
		unsigned long GetVirtualDiskSizeMegs (char *szPath);
		void SetEventLogging (EventLogging *log);
		void OutputInt (LPCSTR lpszText, int iValue, bool bError);
		void OutputText (LPCSTR lpszText, bool bError);
		void OutputText (LPCSTR lpszName, LPCSTR lpszValue, bool bError);
		bool Replacestring (char *szStringtoreplace, char *szStringtosearchfor, char *szReplacement);

		// Virtual Drive vars - these have to be public because they are accessed from
		// static methods through a static pointer.
		HANDLE m_hAllocateFile; // File handle used when allocating virtual disks.
		
		// Allocation progress variables
		char m_szProgressLabel[SIZE_STRING];
		int m_allocationpercent;
		int m_currentvalue;
		int m_maxvalues;
		
		// Virtual Drive resources - these are limited in number and represent
		// the maximum number of virtual drives that can be mounted at once
		// I'll limit these to 30

		DRIVE_HANDLE m_hDriveResource[MAX_DRIVES];
		HANDLE m_hFileResource[MAX_DRIVES];
		bool m_bResourceBusy[MAX_DRIVES];
		SingleDriveInfo m_Drivelist[MAX_DRIVES];
		MemoryBuffer m_memIndividualcipher[MAX_DRIVES];
		unsigned long m_Offsetvalues[MAX_DRIVES];

		StandardEncryption m_enc;
		char m_szPassword[SIZE_STRING];
		MemoryBuffer m_memCipher;
		char m_szMounterrormessage[SIZE_STRING];

		static VirtualDriveManager* m_pinstance;
		HWND m_hwnd;

		SingleDriveInfo currentAllocationInfo;
		bool m_binitialised;
		EventLogging *m_plog;

	private:
		
		bool m_diagnosticsset;
};