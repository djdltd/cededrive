#include "EventLogging.h"

EventLogging::EventLogging ()
{
	bEventsourceset = false;
}

EventLogging::~EventLogging ()
{
	
}

void EventLogging::SetEventsource (char *szEventsource)
{
	ZeroMemory (m_szEventsource, SIZE_STRING);
	strcpy_s (m_szEventsource, SIZE_STRING, szEventsource);
	bEventsourceset = true;
}

void EventLogging::ReportEventLog (DWORD dwEventID, char *szMessage, unsigned int iType)
{
	if (bEventsourceset == false) {
		return;
	}

	PCTSTR aInsertions[] = { szMessage };
    HANDLE h; 

    // Get a handle to the event log.

    h = RegisterEventSource(NULL, m_szEventsource);
    if (h == NULL) 
    {
        return;
    }

    // Report the event.
	WORD wType = EVENTLOG_INFORMATION_TYPE;

	if (iType == ERR) {
		wType = EVENTLOG_ERROR_TYPE;
	}

	if (iType == WARNING) {
		wType = EVENTLOG_WARNING_TYPE;
	}

	if (iType == INFO) {
		wType = EVENTLOG_INFORMATION_TYPE;
	}

    if (!ReportEvent(h,           // event log handle 
            wType,  // event type 
            0,            // event category  
            dwEventID,            // event identifier 
            NULL,                 // no user security identifier 
            1,             // number of substitution strings 
            0,                    // no data 
            aInsertions,                // pointer to strings 
            NULL))                // no data 
    {
        //printf("Could not report the event."); 
    }
 
    DeregisterEventSource(h); 
    return;
}

BOOL EventLogging::AddEventSource(LPTSTR pszLogName, LPTSTR pszSrcName, DWORD dwNum)
{
   HKEY hk; 
   DWORD dwData, dwDisp; 
   TCHAR szBuf[MAX_PATH];
   char pszMsgDLL[SIZE_STRING];

   ZeroMemory (pszMsgDLL, SIZE_STRING);
   GetModuleFileName (NULL, pszMsgDLL, SIZE_STRING);

   // Create the event source as a subkey of the log. 
 
   wsprintf(szBuf, 
      "SYSTEM\\CurrentControlSet\\Services\\EventLog\\%s\\%s",
      pszLogName, pszSrcName); 
 
   if (RegCreateKeyEx(HKEY_LOCAL_MACHINE, szBuf, 
          0, NULL, REG_OPTION_NON_VOLATILE,
          KEY_WRITE, NULL, &hk, &dwDisp)) 
   {
      printf("Could not create the registry key."); 
      return FALSE;
   }
 
   // Set the name of the message file. 
 
   if (RegSetValueEx(hk,              // subkey handle 
           "EventMessageFile",        // value name 
           0,                         // must be zero 
           REG_EXPAND_SZ,             // value type 
           (LPBYTE) pszMsgDLL,        // pointer to value data 
           (DWORD) lstrlen(pszMsgDLL)+1)) // length of value data 
   {
      printf("Could not set the event message file."); 
      RegCloseKey(hk); 
      return FALSE;
   }
 
   // Set the supported event types. 
 
   dwData = EVENTLOG_ERROR_TYPE | EVENTLOG_WARNING_TYPE | 
        EVENTLOG_INFORMATION_TYPE; 
 
   if (RegSetValueEx(hk,      // subkey handle 
           "TypesSupported",  // value name 
           0,                 // must be zero 
           REG_DWORD,         // value type 
           (LPBYTE) &dwData,  // pointer to value data 
           sizeof(DWORD)))    // length of value data 
   {
      printf("Could not set the supported types."); 
      RegCloseKey(hk); 
      return FALSE;
   }
 
   // Set the category message file and number of categories.

   if (RegSetValueEx(hk,              // subkey handle 
           "CategoryMessageFile",     // value name 
           0,                         // must be zero 
           REG_EXPAND_SZ,             // value type 
           (LPBYTE) pszMsgDLL,        // pointer to value data 
           (DWORD) lstrlen(pszMsgDLL)+1)) // length of value data 
   {
      printf("Could not set the category message file."); 
      RegCloseKey(hk); 
      return FALSE;
   }
 
   if (RegSetValueEx(hk,      // subkey handle 
           "CategoryCount",   // value name 
           0,                 // must be zero 
           REG_DWORD,         // value type 
           (LPBYTE) &dwNum,   // pointer to value data 
           sizeof(DWORD)))    // length of value data 
   {
      printf("Could not set the category count."); 
      RegCloseKey(hk); 
      return FALSE;
   }

   RegCloseKey(hk); 
   return TRUE;
}