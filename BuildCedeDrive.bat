echo off

echo Cleaning...

erase Installer\CedeDriveSetup-32bit.exe
erase Installer\CedeDriveSetup-64bit.exe
erase Installer\32bit\CedeDrive.exe
erase Installer\32bit\CedeDriveSetup-32bit.exe
erase Installer\32bit\ServiceDeployment.exe
erase Installer\64bit\CedeDrive.exe
erase Installer\64bit\CedeDriveSetup-64bit.exe
erase Installer\64bit\ServiceDeployment.exe
erase Installer\CedeDriveLaunch.exe

echo Building the CedeDrive Manager Service...

"C:\Program Files (x86)\Microsoft Visual Studio 8\Common7\IDE\devenv.exe" /build "Release|Win32" CedeDriveService\CedeDriveService\CedeDriveService.sln

"C:\Program Files (x86)\Microsoft Visual Studio 8\Common7\IDE\devenv.exe" /build "Release|x64" CedeDriveService\CedeDriveService\CedeDriveService.sln

echo Building the CedeDrive Service Deployment installer...

"C:\Program Files (x86)\Microsoft Visual Studio 8\Common7\IDE\devenv.exe" /build "Release|Win32" ServiceDeployment\ServiceDeployment\ServiceDeployment.sln

echo Building minimised version for the Portable Launcher...

"C:\Program Files (x86)\Microsoft Visual Studio 8\Common7\IDE\devenv.exe" /build "Release (For Launcher)|Win32" CedeDrive\CedeDrive\CedeDrive.sln

"C:\Program Files (x86)\Microsoft Visual Studio 8\Common7\IDE\devenv.exe" /build "Release (For Launcher)|x64" CedeDrive\CedeDrive\CedeDrive.sln

echo Building the Launcher...

"C:\Program Files (x86)\Microsoft Visual Studio 8\Common7\IDE\devenv.exe" /rebuild "Release|Win32" PortableLauncher\CedeDriveLaunch\CedeDriveLaunch.sln


echo Building CedeDrive normal version for installation...

"C:\Program Files (x86)\Microsoft Visual Studio 8\Common7\IDE\devenv.exe" /build "Release (For Install)|Win32" CedeDrive\CedeDrive\CedeDrive.sln

"C:\Program Files (x86)\Microsoft Visual Studio 8\Common7\IDE\devenv.exe" /build "Release (For Install)|x64" CedeDrive\CedeDrive\CedeDrive.sln

echo Copying built executables to installer directory...
copy CedeDrive\CedeDrive\Release\CedeDrive.exe Installer\32bit\CedeDrive.exe /Y
copy CedeDrive\CedeDrive\x64\Release\CedeDrive.exe Installer\64bit\CedeDrive.exe /Y
copy ServiceDeployment\ServiceDeployment\Release\ServiceDeployment.exe Installer\32bit\ServiceDeployment.exe /Y
copy ServiceDeployment\ServiceDeployment\Release\ServiceDeployment.exe Installer\64bit\ServiceDeployment.exe /Y
copy PortableLauncher\CedeDriveLaunch\Release\CedeDriveLaunch.exe Installer\CedeDriveLaunch.exe /Y

echo Building installer...
"C:\Program Files (x86)\NSIS\makensis.exe" Installer\32bit\CedeDriveSetup-32bit.nsi
"C:\Program Files (x86)\NSIS\makensis.exe" Installer\64bit\CedeDriveSetup-64bit.nsi

echo Copying setup files to output directory
copy Installer\32bit\CedeDriveSetup-32bit.exe Installer\CedeDriveSetup-32bit.exe
copy Installer\64bit\CedeDriveSetup-64bit.exe Installer\CedeDriveSetup-64bit.exe

explorer Installer
