echo off

echo Cleaning...

erase Installer\CedeDriveSetup-32bit.exe
erase Installer\CedeDriveSetup-64bit.exe
erase Installer\32bit\CedeDrive.exe
erase Installer\32bit\CedeDriveSetup-32bit.exe
erase Installer\32bit\ServiceDeployment.exe
erase Installer\64bit\CedeDrive.exe
erase Installer\64bit\CedeDriveSetup-64bit.exe
erase Installer\64bit\ServiceDeployment.exe
erase Installer\CedeDriveLaunch.exe
erase Installer\CedeDriveSetup-32bit.msi
erase Installer\CedeDriveSetup-64bit.msi

erase Installer\32bit\WiXProject\WixProject1\CedeDrive.exe
erase Installer\64bit\WiXProject\WixProject1\CedeDrive.exe
erase Installer\32bit\WiXProject\WixProject1\ServiceDeployment.exe
erase Installer\64bit\WiXProject\WixProject1\ServiceDeployment.exe
erase CedeDriveService\CedeDriveService\x64\Release\CedeDriveService.exe
erase CedeDriveService\CedeDriveService\Release\CedeDriveService.exe

erase Installer\32bit\WiXProject\WixProject1\CedeDriveService.exe
erase Installer\32bit\WiXProject\WixProject1\CedeDriveUserGuide.pdf
erase Installer\32bit\WiXProject\WixProject1\VDSDKDll.dll
erase Installer\32bit\WiXProject\WixProject1\VDCore.dll

erase Installer\64bit\WiXProject\WixProject1\CedeDriveService.exe
erase Installer\64bit\WiXProject\WixProject1\CedeDriveUserGuide.pdf
erase Installer\64bit\WiXProject\WixProject1\VDSDKDll.dll
erase Installer\64bit\WiXProject\WixProject1\VDCore.dll


echo Building the CedeDrive Manager Service...

"C:\Program Files (x86)\Microsoft Visual Studio 8\Common7\IDE\devenv.exe" /rebuild "Release|Win32" CedeDriveService\CedeDriveService\CedeDriveService.sln

"C:\Program Files (x86)\Microsoft Visual Studio 8\Common7\IDE\devenv.exe" /rebuild "Release|x64" CedeDriveService\CedeDriveService\CedeDriveService.sln

echo Building the CedeDrive Service Deployment installer...

"C:\Program Files (x86)\Microsoft Visual Studio 8\Common7\IDE\devenv.exe" /rebuild "Release|Win32" ServiceDeployment\ServiceDeployment\ServiceDeployment.sln

echo Building minimised version for the Portable Launcher...

"C:\Program Files (x86)\Microsoft Visual Studio 8\Common7\IDE\devenv.exe" /rebuild "Release (For Launcher)|Win32" CedeDrive\CedeDrive\CedeDrive.sln

"C:\Program Files (x86)\Microsoft Visual Studio 8\Common7\IDE\devenv.exe" /rebuild "Release (For Launcher)|x64" CedeDrive\CedeDrive\CedeDrive.sln

echo Building the Launcher...

"C:\Program Files (x86)\Microsoft Visual Studio 8\Common7\IDE\devenv.exe" /rebuild "Release|Win32" PortableLauncher\CedeDriveLaunch\CedeDriveLaunch.sln


echo Building CedeDrive normal version for installation...

"C:\Program Files (x86)\Microsoft Visual Studio 8\Common7\IDE\devenv.exe" /rebuild "Release (For Install)|Win32" CedeDrive\CedeDrive\CedeDrive.sln

"C:\Program Files (x86)\Microsoft Visual Studio 8\Common7\IDE\devenv.exe" /rebuild "Release (For Install)|x64" CedeDrive\CedeDrive\CedeDrive.sln

echo Copying built executables to installer directory...
copy CedeDrive\CedeDrive\Release\CedeDrive.exe Installer\32bit\CedeDrive.exe /Y
copy CedeDrive\CedeDrive\Release\CedeDrive.exe Installer\32bit\WiXProject\WixProject1\CedeDrive.exe /Y

copy CedeDrive\CedeDrive\x64\Release\CedeDrive.exe Installer\64bit\CedeDrive.exe /Y
copy CedeDrive\CedeDrive\x64\Release\CedeDrive.exe Installer\64bit\WiXProject\WixProject1\CedeDrive.exe /Y

copy ServiceDeployment\ServiceDeployment\Release\ServiceDeployment.exe Installer\32bit\ServiceDeployment.exe /Y
copy ServiceDeployment\ServiceDeployment\Release\ServiceDeployment.exe Installer\32bit\WiXProject\WixProject1\ServiceDeployment.exe /Y

copy ServiceDeployment\ServiceDeployment\Release\ServiceDeployment.exe Installer\64bit\ServiceDeployment.exe /Y
copy ServiceDeployment\ServiceDeployment\Release\ServiceDeployment.exe Installer\64bit\WiXProject\WixProject1\ServiceDeployment.exe /Y

copy PortableLauncher\CedeDriveLaunch\Release\CedeDriveLaunch.exe Installer\CedeDriveLaunch.exe /Y

copy CedeDriveUserGuide.pdf Installer\32bit\WiXProject\WixProject1\CedeDriveUserGuide.pdf /Y
copy CedeDriveService\CedeDriveService\Release\CedeDriveService.exe Installer\32bit\WiXProject\WixProject1\CedeDriveService.exe /Y
copy CedeDriveService\CedeDriveService\Release\VDSDKDll.dll Installer\32bit\WiXProject\WixProject1\VDSDKDll.dll /Y
copy CedeDriveService\CedeDriveService\Release\VDCore.dll Installer\32bit\WiXProject\WixProject1\VDCore.dll /Y

copy CedeDriveUserGuide.pdf Installer\64bit\WiXProject\WixProject1\CedeDriveUserGuide.pdf /Y
copy CedeDriveService\CedeDriveService\x64\Release\CedeDriveService.exe Installer\64bit\WiXProject\WixProject1\CedeDriveService.exe /Y
copy CedeDriveService\CedeDriveService\x64\Release\VDSDKDll.dll Installer\64bit\WiXProject\WixProject1\VDSDKDll.dll /Y
copy CedeDriveService\CedeDriveService\x64\Release\VDCore.dll Installer\64bit\WiXProject\WixProject1\VDCore.dll /Y


echo Building installer...
"C:\Program Files (x86)\NSIS\makensis.exe" Installer\32bit\CedeDriveSetup-32bit.nsi
"C:\Program Files (x86)\NSIS\makensis.exe" Installer\64bit\CedeDriveSetup-64bit.nsi
"C:\Program Files (x86)\Microsoft Visual Studio 8\Common7\IDE\devenv.exe" /rebuild "Release|x86" Installer\32bit\WiXProject\WixProject1.sln
"C:\Program Files (x86)\Microsoft Visual Studio 8\Common7\IDE\devenv.exe" /rebuild "Release|x86" Installer\64bit\WiXProject\WixProject1.sln

echo Copying setup files to output directory
copy Installer\32bit\CedeDriveSetup-32bit.exe Installer\CedeDriveSetup-32bit.exe
copy Installer\64bit\CedeDriveSetup-64bit.exe Installer\CedeDriveSetup-64bit.exe

copy Installer\32bit\WiXProject\WixProject1\bin\Release\CedeDriveSetup-32bit.msi Installer\CedeDriveSetup-32bit.msi
copy Installer\64bit\WiXProject\WixProject1\bin\Release\CedeDriveSetup-64bit.msi Installer\CedeDriveSetup-64bit.msi

explorer Installer
