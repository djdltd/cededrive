#include <windows.h> 
#include <stdio.h>
#include <conio.h>
#include <tchar.h>
#include "MemoryBuffer.h"
#include "DynList.h"
#include "SingleDriveInfo.h"
#include "InstructionResult.h"

#define BUFSIZE 512
#define RESULTBUF		10000
#define DRIVE_MOUNT					100
#define DRIVE_UNMOUNT			101
#define DRIVE_UNMOUNTALL		102

int _tmain(int argc, TCHAR *argv[]) 
{ 
	printf("*** THIS IS THE PIPE CLIENT ***");

   HANDLE hPipe; 
   LPTSTR lpvMessage=TEXT("Default message from client"); 
   TCHAR chBuf[BUFSIZE]; 
   BOOL fSuccess; 
   DWORD cbRead, cbWritten, dwMode; 
   LPTSTR lpszPipename = TEXT("\\\\.\\pipe\\mynamedpipe"); 


   // Create a temporary drive list structure for transmission across
   // the named pipe
	SingleDriveInfo drive1;
	SingleDriveInfo drive2;

	ZeroMemory (&drive1, sizeof (SingleDriveInfo));
	//ZeroMemory (&drive2, sizeof (SingleDriveInfo));


	drive1.iInstruction = DRIVE_MOUNT;
	strcpy_s (drive1.szName, SIZE_STRING, "Test Drive");
	strcpy_s (drive1.szDescription, SIZE_STRING, "Test Drive Description");
	strcpy_s (drive1.szPath, SIZE_STRING, "C:\\Data\\EncryptedDisk.evd");
	strcpy_s (drive1.szPassword, SIZE_STRING, "penhorse34");
	drive1.bUsingseperatepassword = true;
	strcpy_s (drive1.szDriveLetter, SIZE_NAME, "D:");
	drive1.lDiskSizeMegs = 120;
	

	//strcpy_s (drive2.szName, SIZE_STRING, "Drive 2 Name");
	//strcpy_s (drive2.szDescription, SIZE_STRING, "Drive 2 Description which is long");

	DynList dlDrivelist;
	dlDrivelist.AddItem (&drive1, sizeof (SingleDriveInfo), false);
	//dlDrivelist.AddItem (&drive2, sizeof (SingleDriveInfo), false);
	
	// Serialise to memory buffer
	MemoryBuffer memList;
	dlDrivelist.ToMemoryBuffer (&memList);

	printf ("MemList size is: %i", memList.GetSize ());

   if( argc > 1 )
      lpvMessage = argv[1];
 
	// Try to open a named pipe; wait for it, if necessary.  
   while (1) 
   { 
      hPipe = CreateFile( 
         lpszPipename,   // pipe name 
         GENERIC_READ |  // read and write access 
         GENERIC_WRITE, 
         0,              // no sharing 
         NULL,           // default security attributes
         OPEN_EXISTING,  // opens existing pipe 
         0,              // default attributes 
         NULL);          // no template file 
 
		// Break if the pipe handle is valid. 
		if (hPipe != INVALID_HANDLE_VALUE) 
         break; 
 
      // Exit if an error other than ERROR_PIPE_BUSY occurs. 
      if (GetLastError() != ERROR_PIPE_BUSY) 
      {
         printf("Could not open pipe"); 
         return 0;
      }
 
      // All pipe instances are busy, so wait for 20 seconds. 
      if (!WaitNamedPipe(lpszPipename, 20000)) 
      { 
         printf("Could not open pipe"); 
         return 0;
      } 
   } 
 
	// The pipe connected; change to message-read mode. 
   dwMode = PIPE_READMODE_MESSAGE; 
   fSuccess = SetNamedPipeHandleState( 
      hPipe,    // pipe handle 
      &dwMode,  // new pipe mode 
      NULL,     // don't set maximum bytes 
      NULL);    // don't set maximum time 
   if (!fSuccess) 
   {
      printf("SetNamedPipeHandleState failed"); 
      return 0;
   }
 
	// Send a message to the pipe server. 
   //for (;;) {

   /*
   fSuccess = WriteFile( 
      hPipe,                  // pipe handle 
      lpvMessage,             // message 
      (lstrlen(lpvMessage)+1)*sizeof(TCHAR), // message length 
      &cbWritten,             // bytes written 
      NULL);                  // not overlapped
  */
	
   fSuccess = WriteFile (hPipe, memList.GetBuffer(), memList.GetSize(), &cbWritten, NULL);

   if (!fSuccess) 
   {
      printf("WriteFile failed"); 
      return 0;
   }
 
   do 
   { 
	// Read from the pipe. 
	MemoryBuffer memResult;
	memResult.SetSize (RESULTBUF);


	printf ("Reading from pipe...\n");
      fSuccess = ReadFile( 
         hPipe,    // pipe handle 
         memResult.GetBuffer (),    // buffer to receive reply 
         RESULTBUF,  // size of buffer 
         &cbRead,  // number of bytes read 
         NULL);    // not overlapped 
 
      if (! fSuccess && GetLastError() != ERROR_MORE_DATA) 
         break; 
 
	  MemoryBuffer memAnswer;
	  memAnswer.SetSize (cbRead);
	  memAnswer.Write (memResult.GetBuffer (), 0, cbRead);
	  memResult.Clear ();

	  InstructionResult irResult;
	  memcpy (&irResult, memAnswer.GetBuffer (), sizeof (InstructionResult));

	  _tprintf ("ResultCode: %i\nDescription: %s\nSuccess: %i\n", irResult.iResult, irResult.szDescription, irResult.bSuccess);

      //_tprintf( TEXT("%s\n"), chBuf ); 
	} while (!fSuccess);  // repeat loop if ERROR_MORE_DATA 
		//Sleep (500);
//}
   //getch();
 
   CloseHandle(hPipe);  
   return 0; 
}
