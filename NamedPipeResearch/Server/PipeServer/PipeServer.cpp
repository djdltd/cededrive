#include <windows.h> 
#include <stdio.h>
#include <conio.h>
#include <tchar.h>
#include "MemoryBuffer.h"
#include "DynList.h"
#include "SingleDriveInfo.h"

#define BUFSIZE 1024000
 
VOID ClientInstanceThread(LPVOID); 
VOID GetAnswerToRequest(LPTSTR, LPTSTR, LPDWORD); 
 
int _tmain(VOID) 
{ 
	printf ("*** THIS IS THE PIPE SERVER ***");

   BOOL fConnected; 
   DWORD dwThreadId; 
   HANDLE hPipe, hThread; 
   LPTSTR lpszPipename = TEXT("\\\\.\\pipe\\mynamedpipe"); 
 
	// The main loop creates an instance of the named pipe and 
	// then waits for a client to connect to it. When the client 
	// connects, a thread is created to handle communications 
	// with that client, and the loop is repeated. 
 
   for (;;) 
   { 
      hPipe = CreateNamedPipe( 
          lpszPipename,             // pipe name 
          PIPE_ACCESS_DUPLEX,       // read/write access 
          PIPE_TYPE_MESSAGE |       // message type pipe 
          PIPE_READMODE_MESSAGE |   // message-read mode 
          PIPE_WAIT,                // blocking mode 
          PIPE_UNLIMITED_INSTANCES, // max. instances  
          BUFSIZE,                  // output buffer size 
          BUFSIZE,                  // input buffer size 
          NMPWAIT_USE_DEFAULT_WAIT, // client time-out 
          NULL);                    // default security attribute 

      if (hPipe == INVALID_HANDLE_VALUE) 
      {
          printf("CreatePipe failed"); 
          return 0;
      }
 
      // Wait for the client to connect; if it succeeds, 
      // the function returns a nonzero value. If the function
      // returns zero, GetLastError returns ERROR_PIPE_CONNECTED. 
 
      fConnected = ConnectNamedPipe(hPipe, NULL) ? 
         TRUE : (GetLastError() == ERROR_PIPE_CONNECTED); 
 
      if (fConnected) 
      { 
      // Create a thread for this client. 
         hThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE) ClientInstanceThread, (LPVOID) hPipe, 0, &dwThreadId);

         if (hThread == NULL) 
         {
            printf("CreateThread failed"); 
            return 0;
         }
         else CloseHandle(hThread); 
       } 
      else 
        // The client could not connect, so close the pipe. 
         CloseHandle(hPipe); 
   } 
   return 1; 
} 
 
VOID ClientInstanceThread(LPVOID lpvParam) 
{
   TCHAR chRequest[BUFSIZE]; 
   TCHAR chReply[BUFSIZE]; 
   DWORD cbBytesRead, cbReplyBytes, cbWritten; 
   BOOL fSuccess; 
   HANDLE hPipe; 
 
   MemoryBuffer memRcv;
	memRcv.SetSize (BUFSIZE);

	// The thread's parameter is a handle to a pipe instance. 
   hPipe = (HANDLE) lpvParam; 
 
   while (1) 
   { 
		// Read client requests from the pipe. 
	   fSuccess = ReadFile (hPipe, memRcv.GetBuffer (), 1024000, &cbBytesRead, NULL);			

      if (! fSuccess || cbBytesRead == 0) 
         break;


		if (cbBytesRead > 0) {
			// Copy what we've received into a separate memory buffer with the correct size
			MemoryBuffer memList;
			memList.SetSize (cbBytesRead);
			memList.Write (memRcv.GetBuffer(), 0, cbBytesRead);

			// Deserialise back to a dynlist;
			DynList dlRequestlist;
			dlRequestlist.FromMemoryBuffer (&memList);

			// Print out the list
			SingleDriveInfo *pinfo;

			for (unsigned int i=0;i<dlRequestlist.GetNumItems ();i++) {
				pinfo = (SingleDriveInfo *) dlRequestlist.GetItem (i);
				printf ("Item %i, Drive Name: %s, Description: %s", i, pinfo->szName, pinfo->szDescription);

			}
		}

      GetAnswerToRequest(chRequest, chReply, &cbReplyBytes); 

		// Write the reply to the pipe. 
      fSuccess = WriteFile(hPipe, chReply, cbReplyBytes, &cbWritten, NULL);

      if (! fSuccess || cbReplyBytes != cbWritten) break; 
  }
 
	// Flush the pipe to allow the client to read the pipe's contents 
	// before disconnecting. Then disconnect the pipe, and close the 
	// handle to this pipe instance. 
 
   FlushFileBuffers(hPipe); 
   DisconnectNamedPipe(hPipe); 
   CloseHandle(hPipe); 
}

VOID GetAnswerToRequest(LPTSTR chRequest, LPTSTR chReply, LPDWORD pchBytes)
{
   //_tprintf( TEXT("%s\n"), chRequest );
   lstrcpy( chReply, TEXT("Default answer from server") );
   *pchBytes = (lstrlen(chReply)+1)*sizeof(TCHAR);
}
