#pragma once
#include "resource.h"

class SingleDriveInfo {
	
	public:
		SingleDriveInfo ();
		~SingleDriveInfo ();

		unsigned int iInstruction;
		char szName[SIZE_STRING];
		char szDescription[SIZE_STRING];
		char szPath[SIZE_STRING];
		char szDriveLetter[SIZE_NAME];
		unsigned long lDiskSizeMegs;
		bool bMountStartup;
		bool bUsingseperatepassword;
		char szPassword[SIZE_STRING];

		bool bFormatdriveaftermount;
		char szVolumename[SIZE_STRING];

	private:
		
		
};