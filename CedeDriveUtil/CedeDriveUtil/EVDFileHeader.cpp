#include "EVDFileHeader.h"

EVDFileHeader::EVDFileHeader ()
{

}

EVDFileHeader::~EVDFileHeader ()
{
}

void EVDFileHeader::GetLastErrorMessage (char *szOutmessage)
{
	ZeroMemory (szOutmessage, SIZE_STRING);
	strcpy_s (szOutmessage, SIZE_STRING, m_szLasterror);
}

unsigned long EVDFileHeader::GetSectorMultiple (unsigned long lValue)
{
	// Takes a value and returns a value that is the nearest sector-aligned multiple
	// of the supplied value
	unsigned long currentMultiple = 1024;
	if (lValue > 0) {

		while (lValue > currentMultiple) {
			currentMultiple = currentMultiple * 2;
		}

		return currentMultiple;

	} else {
		return 0;
	}
}

// FILESIGNATURE | TOTALHEADERSIZE | FILEVERSION | USER_RECVR_KEY_SIZE | USER_RECVRKEY_DATA | 
//   ulong * 4     ulong             ulong         ulong                 byte *

//     MFTR_RECVR_KEY_SIZE | MFTR_RECVRKEY_DATA | NAME_SIZE | NAME_DATA | DESC_SIZE | DESC_DATA
//     ulong			     byte *				  ulong       char *      ulong       char *

void EVDFileHeader::SetName (char *szName)
{
	ZeroMemory (m_szName, SIZE_STRING);
	strcpy_s (m_szName, SIZE_STRING, szName);
}

void EVDFileHeader::SetDescription (char *szDesc)
{
	ZeroMemory (m_szDescription, SIZE_STRING);
	strcpy_s (m_szDescription, SIZE_STRING, szDesc);
}

void EVDFileHeader::SetUserKey (MemoryBuffer *memKey)
{
	m_memUserkey.SetSize (memKey->GetSize ());
	m_memUserkey.Write (memKey->GetBuffer (), 0, memKey->GetSize ());
}

void EVDFileHeader::SetMFKey (MemoryBuffer *memKey)
{
	m_memMfkey.SetSize (memKey->GetSize ());
	m_memMfkey.Write (memKey->GetBuffer (), 0, memKey->GetSize ());
}

void EVDFileHeader::Serialise ()
{
	// Serialise an EVD file header taking all the individual bits of
	// info and putting it into one memorybuffer containing header information
	unsigned long fsignature = 1577047;
	unsigned long fileversion = 1001;
	unsigned long userkeysize = m_memUserkey.GetSize ();
	unsigned long mfkeysize = m_memMfkey.GetSize ();
	unsigned long namesize = strlen (m_szName);
	unsigned long descsize = strlen (m_szDescription);

	// Total header size starts from and includes File Version
	unsigned long ltotalheadersize = sizeof (unsigned long) + sizeof (unsigned long) + m_memUserkey.GetSize () + sizeof (unsigned long) + m_memMfkey.GetSize () + 
		sizeof (unsigned long) + namesize + sizeof (unsigned long) + descsize;

	
	m_memHeader.SetSize (ltotalheadersize + (sizeof (unsigned long) *2));
	
	m_memHeader.Append (&fsignature, sizeof (unsigned long));
	m_memHeader.Append (&ltotalheadersize, sizeof (unsigned long));
	m_memHeader.Append (&fileversion, sizeof (unsigned long));
	m_memHeader.Append (&userkeysize, sizeof (unsigned long));
	m_memHeader.Append (m_memUserkey.GetBuffer (), m_memUserkey.GetSize ());
	m_memHeader.Append (&mfkeysize, sizeof (unsigned long));
	m_memHeader.Append (m_memMfkey.GetBuffer (), m_memMfkey.GetSize ());
	m_memHeader.Append (&namesize, sizeof (unsigned long));
	m_memHeader.Append ((char *) m_szName, namesize);
	m_memHeader.Append (&descsize, sizeof (unsigned long));
	m_memHeader.Append ((char *) m_szDescription, descsize);

}

// FILESIGNATURE | TOTALHEADERSIZE | FILEVERSION | USER_RECVR_KEY_SIZE | USER_RECVRKEY_DATA | 
//   ulong * 4     ulong             ulong         ulong                 byte *

//     MFTR_RECVR_KEY_SIZE | MFTR_RECVRKEY_DATA | NAME_SIZE | NAME_DATA | DESC_SIZE | DESC_DATA
//     ulong			     byte *				  ulong       char *      ulong       char *

MemoryBuffer *EVDFileHeader::GetEVDHeader ()
{
	return (MemoryBuffer *) &m_memHeader;
}

void EVDFileHeader::SetEVDFileHeader (MemoryBuffer *memHeader)
{
	m_memHeader.SetSize (memHeader->GetSize ());
	m_memHeader.Write (memHeader->GetBuffer (), 0, memHeader->GetSize ());
}

bool EVDFileHeader::SetFromFile (char *szFilename)
{
	// Populates the EVD File header memory buffer from a file, but it does it
	// a bit differently than normal by just reading the beginning of the file instead
	// of reading in the whole file just to get the header, as EVD files will be massive.

	HANDLE hFile = CreateFile(szFilename, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ,0, OPEN_ALWAYS, FILE_FLAG_NO_BUFFERING | FILE_FLAG_RANDOM_ACCESS,0);

    if(hFile == INVALID_HANDLE_VALUE)
    {
		ZeroMemory (m_szLasterror, SIZE_STRING);
		sprintf_s (m_szLasterror, SIZE_STRING, "Cannot open file %s, due to Error %i", szFilename, GetLastError ());
        return false;
    }

	LARGE_INTEGER currentSize;
	GetFileSizeEx (hFile, &currentSize);

	if (currentSize.QuadPart > (sizeof (unsigned long) * 5)) {
		// Ok to proceed - file is big enough
		// Firstly read the first 2 unsigned longs as they contain the first 2 important parts, the signature and the total header size;
		
		DWORD initialheadersize = GetSectorMultiple (sizeof (unsigned long) *2);
		MemoryBuffer memTinyheader;
		memTinyheader.SetSize (initialheadersize);
		DWORD dwBytesread = 0;

		if (ReadFile (hFile, memTinyheader.GetBuffer(), initialheadersize, &dwBytesread, NULL) == 0) {
			memTinyheader.Clear ();
			CloseHandle (hFile);
			ZeroMemory (m_szLasterror, SIZE_STRING);
			sprintf_s (m_szLasterror, SIZE_STRING, "Cannot read tiny header from file %s, due to Error %i", szFilename, GetLastError ());
			return false;

		} else {	

			// Tiny header read ok, now reset the file pointer back to zero
			SetFilePointer (hFile, 0, NULL, FILE_BEGIN);

			// Check the signature now
			unsigned long fsignature = 0;
			unsigned long ltotalsize = 0;
			unsigned long ipointer = 0;

			memcpy (&fsignature, (BYTE *) memTinyheader.GetBuffer ()+ipointer, sizeof (unsigned long));
			ipointer += sizeof (unsigned long);

			if (fsignature == 1577047) {
				// Signature is ok, read the rest
				memcpy (&ltotalsize, (BYTE *) memTinyheader.GetBuffer ()+ipointer, sizeof (unsigned long));
				ipointer += sizeof (unsigned long);

				// Now we have the total size, we know how big the header is, so read all of it now
				unsigned long totalheadersize = ltotalsize + (sizeof (unsigned long) *2);
				DWORD dwHeaderbytesread = 0;
				DWORD dwalignedsize = GetSectorMultiple (totalheadersize);

				m_memHeader.SetSize (dwalignedsize);
				if (ReadFile (hFile, m_memHeader.GetBuffer (), dwalignedsize, &dwHeaderbytesread, NULL) == 0) {
					m_memHeader.Clear ();
					memTinyheader.Clear ();
					CloseHandle (hFile);
					ZeroMemory (m_szLasterror, SIZE_STRING);
					sprintf_s (m_szLasterror, SIZE_STRING, "Cannot read file header from file %s, due to Error %i", szFilename, GetLastError ());
					return false;
				} else {					
					// Header was read ok, so close the file and return success;
					memTinyheader.Clear ();
					CloseHandle (hFile);
					return true;
				}
			} else {
				memTinyheader.Clear ();
				CloseHandle (hFile);	
				ZeroMemory (m_szLasterror, SIZE_STRING);
				sprintf_s (m_szLasterror, SIZE_STRING, "The header was not read because file %s is not an EVD file (bad signature).", szFilename);
				return false;
			}

		}

	} else {
		CloseHandle (hFile);	
		ZeroMemory (m_szLasterror, SIZE_STRING);
		sprintf_s (m_szLasterror, SIZE_STRING, "Unable to read file %s, because it does not meet the minimum file size.", szFilename);
		return false;
	}

	// Close our file handle when we are done
	CloseHandle (hFile);
}

MemoryBuffer *EVDFileHeader::GetUserKey ()
{
	return (MemoryBuffer *) &m_memUserkey;
}

MemoryBuffer *EVDFileHeader::GetMFKey ()
{
	return (MemoryBuffer *) &m_memMfkey;
}

void EVDFileHeader::GetName (char *szOutname)
{
	ZeroMemory (szOutname, SIZE_STRING);
	strcpy_s (szOutname, SIZE_STRING, m_szName);
}

void EVDFileHeader::GetDescription (char *szOutdescription)
{
	ZeroMemory (szOutdescription, SIZE_STRING);
	strcpy_s (szOutdescription, SIZE_STRING, m_szDescription);
}

unsigned long EVDFileHeader::GetDataOffset ()
{
	// The data offset is used by the data read and write functions in the virtual drive manager
	// help in the cededrive service. This value is needed so that when data is read or written, it
	// knows where to start reading and writing the data, as it does not care about the file header
	return GetSectorMultiple (m_lEVDdataoffset);
}

void EVDFileHeader::GetHeaderInfo (char *szOutstring)
{
	ZeroMemory (szOutstring, SIZE_LARGESTRING);
	strcpy_s (szOutstring, SIZE_LARGESTRING, m_szFileheader);
}

void EVDFileHeader::Deserialise ()
{
	// Deserialises and EVD File header, and sets all the component parts given the deserialised data
	unsigned long fsignature = 0;
	unsigned long fileversion = 0;
	unsigned long userkeysize = 0;
	unsigned long mfkeysize = 0;
	unsigned long namesize = 0;
	unsigned long descsize = 0;

	unsigned long ltotalheadersize = 0;

	unsigned long ipointer = 0;

	ZeroMemory (m_szFileheader, SIZE_LARGESTRING);

	if (m_memHeader.GetSize () > (sizeof (unsigned long) *5)) {
		
		memcpy (&fsignature, (BYTE *) m_memHeader.GetBuffer()+ipointer, sizeof (unsigned long));
		ipointer += sizeof (unsigned long);

		memcpy (&ltotalheadersize, (BYTE *) m_memHeader.GetBuffer()+ipointer, sizeof (unsigned long));
		ipointer += sizeof (unsigned long);

		memcpy (&fileversion, (BYTE *) m_memHeader.GetBuffer()+ipointer, sizeof (unsigned long));
		ipointer += sizeof (unsigned long);

		memcpy (&userkeysize, (BYTE *) m_memHeader.GetBuffer()+ipointer, sizeof (unsigned long));
		ipointer += sizeof (unsigned long);

		m_memUserkey.SetSize (userkeysize);
		m_memUserkey.Write ((BYTE *) m_memHeader.GetBuffer ()+ipointer, 0, userkeysize);
		ipointer += userkeysize;

		memcpy (&mfkeysize, (BYTE *) m_memHeader.GetBuffer()+ipointer, sizeof (unsigned long));
		ipointer += sizeof (unsigned long);

		m_memMfkey.SetSize (mfkeysize);
		m_memMfkey.Write ((BYTE *) m_memHeader.GetBuffer ()+ipointer, 0, mfkeysize);
		ipointer += mfkeysize;

		memcpy (&namesize, (BYTE *) m_memHeader.GetBuffer()+ipointer, sizeof (unsigned long));
		ipointer += sizeof (unsigned long);

		ZeroMemory (m_szName, SIZE_STRING);
		memcpy (m_szName, (char *) m_memHeader.GetBuffer ()+ipointer, namesize);
		ipointer += namesize;

		memcpy (&descsize, (BYTE *) m_memHeader.GetBuffer()+ipointer, sizeof (unsigned long));
		ipointer += sizeof (unsigned long);

		ZeroMemory (m_szDescription, SIZE_STRING);
		memcpy (m_szDescription, (char *) m_memHeader.GetBuffer ()+ipointer, descsize);
		ipointer += descsize;

		// Now set the EVD data offset
		m_lEVDdataoffset = ltotalheadersize + (sizeof (unsigned long) * 2);

		sprintf_s (m_szFileheader, SIZE_LARGESTRING, "File Signature: %i\nTotal Header Size: %i\nFile Version: %i\nUser Key Size: %i\nMF Key Size: %i\nName Size: %i\nName: %s\nDesc Size: %i\nDesc: %s\n",
			fsignature, ltotalheadersize, fileversion, userkeysize, mfkeysize, namesize, m_szName, descsize, m_szDescription);

				

	}
}