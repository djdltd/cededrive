// CedeDriveUtil.cpp : Defines the entry point for the console application.
//

#include <windows.h>
#include <stdio.h>
#include "MemoryBuffer.h"
#include "EVDFileHeader.h"



int main(int argc, char **argv)
{
	
	printf ("Number of Arguments supplied: %i\n", argc);

	if (argc == 1) {
		printf ("Incorrect number of arguments supplied!\n");
		printf ("Available commands: \n");
		printf (" /showheader\n");
		printf (" /comparefiles\n");

		return 1;
	}

	char szCommand[SIZE_STRING];
	ZeroMemory (szCommand, SIZE_STRING);
	strcpy_s (szCommand, SIZE_STRING, argv[1]);
	bool bcommandrecognised = false;

	if (strcmp (szCommand, "/showheader") == 0) {
	
		bcommandrecognised = true;

		if (argc != 3) {
			printf ("Incorrect number of arguments supplied!\n");
			return 1;
		}

		char szEVDfile[SIZE_STRING];
		ZeroMemory (szEVDfile, SIZE_STRING);
		strcpy_s (szEVDfile, SIZE_STRING, argv[2]);

		printf ("\nShowing file header for: %s\n", szEVDfile);

		EVDFileHeader fileheader;
		if (fileheader.SetFromFile (szEVDfile) == true) {
			
			fileheader.Deserialise ();

			char szHeader[SIZE_LARGESTRING];
			fileheader.GetHeaderInfo (szHeader);

			printf (szHeader);

			printf ("\nData Offset: %i\n", fileheader.GetDataOffset ());

		} else {
			char szError[SIZE_STRING];
			fileheader.GetLastErrorMessage (szError);
			printf ("Unable to show header - %s\n", szError);
		}
	}
	

	if (strcmp (szCommand, "/comparefiles") == 0) {
	
		bcommandrecognised = true;

		if (argc != 4) {
			printf ("Incorrect number of arguments supplied!\n");
			return 1;
		}

		char szFileone[SIZE_STRING];
		ZeroMemory (szFileone, SIZE_STRING);
		strcpy_s (szFileone, SIZE_STRING, argv[2]);

		char szFiletwo[SIZE_STRING];
		ZeroMemory (szFiletwo, SIZE_STRING);
		strcpy_s (szFiletwo, SIZE_STRING, argv[3]);

		MemoryBuffer mem1;
		MemoryBuffer mem2;

		bool bres1 = mem1.ReadFromFile (szFileone);
		bool bres2 = mem2.ReadFromFile (szFiletwo);

		if (bres1 == false) {
			printf ("Unable to read from %s\n", szFileone);
			return 1;
		}

		if (bres2 == false) {
			printf ("Unable to read from %s\n", szFiletwo);
			return 1;
		}

		printf ("Size of file 1: %i\n", mem1.GetSize ());
		printf ("Size of file 2: %i\n", mem2.GetSize ());

		if (mem1.GetSize () == mem2.GetSize ()) {

			BYTE b1;
			BYTE b2;

			unsigned long diffs = 0;
			unsigned long l;

			printf("Comparing....\n");
			for (l=0;l<mem1.GetSize ();l++)
			{
				b1 = mem1.GetByte (l);
				b2 = mem2.GetByte (l);

				if (b1 != b2) {
					diffs++;
				}
			}
			printf("Finished comparing.\n");

			printf ("Number of byte differences: %i\n", diffs);

			if (diffs == 0) {
				printf ("Files are identical.\n");		
			}

		} else {
			printf ("Unable to compare - the file sizes are not the same!");
		}
	}

	if (bcommandrecognised == false) {
		printf("Command not recognised.\n");
	}

	return 0;
}

