#pragma once
#include "resource.h"
#include <io.h>
#include <stdio.h>
#include <windows.h>
#include <direct.h>
#include "MemoryBuffer.h"

class EVDFileHeader {
	
	public:
		EVDFileHeader ();
		~EVDFileHeader ();

		void SetName (char *szName);
		void SetDescription (char *szDesc);
		void SetUserKey (MemoryBuffer *memKey);
		void SetMFKey (MemoryBuffer *memKey);
		
		MemoryBuffer *GetEVDHeader ();
		void SetEVDFileHeader (MemoryBuffer *memHeader);
		MemoryBuffer *GetMFKey ();
		MemoryBuffer *GetUserKey ();
		void GetName (char *szOutname);
		void GetDescription (char *szOutdescription);
		void Serialise ();
		void Deserialise ();
		bool SetFromFile (char *szFilename);
		void GetLastErrorMessage (char *szOutmessage);
		unsigned long GetDataOffset ();
		unsigned long GetSectorMultiple (unsigned long lValue);
		void GetHeaderInfo (char *szOutstring);
	private:


		char m_szLasterror[SIZE_STRING];

		char m_szName[SIZE_STRING];
		char m_szDescription[SIZE_STRING];
		MemoryBuffer m_memUserkey;
		MemoryBuffer m_memMfkey;
		unsigned long m_lEVDdataoffset;

		MemoryBuffer m_memHeader;
		char m_szFileheader[SIZE_LARGESTRING];

};