echo off

echo Cleaning...

erase Installer\CedeDriveSetup-32bit.exe
erase Installer\CedeDriveSetup-64bit.exe
erase Installer\32bit\CedeDrive.exe
erase Installer\32bit\CedeDriveSetup-32bit.exe
erase Installer\32bit\ServiceDeployment.exe
erase Installer\64bit\CedeDrive.exe
erase Installer\64bit\CedeDriveSetup-64bit.exe
erase Installer\64bit\ServiceDeployment.exe
erase Installer\CedeDriveLaunch.exe
erase Installer\CedeDriveSetup-32bit.msi
erase Installer\CedeDriveSetup-64bit.msi
erase Installer\CedeDriveSetup.exe


erase Installer\32bit\CedeDriveInstaller32\CedeDriveInstaller\ServiceDeployment.exe
erase Installer\32bit\CedeDriveInstaller32\CedeDriveInstaller\CedeDrive.exe
erase Installer\32bit\CedeDriveInstaller32\CedeDriveInstaller\CedeDriveLHI.dll
erase Installer\32bit\CedeDriveInstaller32\CedeDriveInstaller\CedeDriveService.exe
erase Installer\32bit\CedeDriveInstaller32\CedeDriveInstaller\CedeDriveUserGuide.pdf
erase Installer\32bit\CedeDriveInstaller32\CedeDriveInstaller\VDSDKDll.dll
erase Installer\32bit\CedeDriveInstaller32\CedeDriveInstaller\VDCore.dll
erase Installer\32bit\CedeDriveInstaller32\CedeDriveInstaller\bin\Release\*.msi

erase Installer\64bit\CedeDriveInstaller64\CedeDriveInstaller64\ServiceDeployment.exe
erase Installer\64bit\CedeDriveInstaller64\CedeDriveInstaller64\CedeDrive.exe
erase Installer\64bit\CedeDriveInstaller64\CedeDriveInstaller64\CedeDriveLHI.dll
erase Installer\64bit\CedeDriveInstaller64\CedeDriveInstaller64\CedeDriveService.exe
erase Installer\64bit\CedeDriveInstaller64\CedeDriveInstaller64\CedeDriveUserGuide.pdf
erase Installer\64bit\CedeDriveInstaller64\CedeDriveInstaller64\VDSDKDll.dll
erase Installer\64bit\CedeDriveInstaller64\CedeDriveInstaller64\VDCore.dll
erase Installer\64bit\CedeDriveInstaller64\CedeDriveInstaller64\bin\Release\*.msi


erase Installer\32bit\WiXProject\WixProject1\ServiceDeployment.exe
erase Installer\32bit\WiXProject\WixProject1\CedeDrive.exe
erase Installer\32bit\WiXProject\WixProject1\CedeDriveService.exe
erase Installer\32bit\WiXProject\WixProject1\CedeDriveUserGuide.pdf
erase Installer\32bit\WiXProject\WixProject1\VDSDKDll.dll
erase Installer\32bit\WiXProject\WixProject1\VDCore.dll
erase Installer\32bit\WiXProject\WixProject1\bin\Release\*.msi


erase Installer\64bit\WiXProject\WixProject1\ServiceDeployment.exe
erase Installer\64bit\WiXProject\WixProject1\CedeDrive.exe
erase Installer\64bit\WiXProject\WixProject1\CedeDriveService.exe
erase Installer\64bit\WiXProject\WixProject1\CedeDriveUserGuide.pdf
erase Installer\64bit\WiXProject\WixProject1\VDSDKDll.dll
erase Installer\64bit\WiXProject\WixProject1\VDCore.dll


erase "CedeDrive\CedeDrive\CedeDrive2010\Release\*.res"
erase "CedeDrive\CedeDrive\CedeDrive2010\Release\*.obj"
erase "CedeDrive\CedeDrive\CedeDrive2010\Release\*.exe"

erase "CedeDrive\CedeDrive\CedeDrive2010\x64\Release\*.res"
erase "CedeDrive\CedeDrive\CedeDrive2010\x64\Release\*.obj"
erase "CedeDrive\CedeDrive\CedeDrive2010\x64\Release\*.exe"


erase "CedeDriveLHI\CedeDriveLHI\Release\*.res"
erase "CedeDriveLHI\CedeDriveLHI\Release\*.obj"
erase "CedeDriveLHI\CedeDriveLHI\Release\*.dll"

erase "CedeDriveLHI\CedeDriveLHI\x64\Release\*.res"
erase "CedeDriveLHI\CedeDriveLHI\x64\Release\*.obj"
erase "CedeDriveLHI\CedeDriveLHI\x64\Release\*.dll"

erase "RemovableBootstrapper\CedeDriveLaunch\Release\*.res"
erase "RemovableBootstrapper\CedeDriveLaunch\Release\*.obj"
erase "RemovableBootstrapper\CedeDriveLaunch\Release\*.exe"

erase "MSIBootstrapper\CedeDriveSetup\Release\*.res"
erase "MSIBootstrapper\CedeDriveSetup\Release\*.obj"
erase "MSIBootstrapper\CedeDriveSetup\Release\*.exe"

erase "CedeDrive\CedeDrive\CedeDrive2010\Release (For Launcher)\*.res"
erase "CedeDrive\CedeDrive\CedeDrive2010\Release (For Launcher)\*.obj"
erase "CedeDrive\CedeDrive\CedeDrive2010\Release (For Launcher)\*.exe"

erase "CedeDrive\CedeDrive\CedeDrive2010\x64\Release (For Launcher)\*.res"
erase "CedeDrive\CedeDrive\CedeDrive2010\x64\Release (For Launcher)\*.obj"
erase "CedeDrive\CedeDrive\CedeDrive2010\x64\Release (For Launcher)\*.exe"

erase "CedeDrive\CedeDrive\CedeDrive2010\Debug\*.res"
erase "CedeDrive\CedeDrive\CedeDrive2010\Debug\*.obj"
erase "CedeDrive\CedeDrive\CedeDrive2010\Debug\*.exe"

erase "CedeDrive\CedeDrive\CedeDrive2010\x64\Debug\*.res"
erase "CedeDrive\CedeDrive\CedeDrive2010\x64\Debug\*.obj"
erase "CedeDrive\CedeDrive\CedeDrive2010\x64\Debug\*.exe"

erase "CedeDrive\CedeDrive\Release\*.res"
erase "CedeDrive\CedeDrive\Release\*.obj"
erase "CedeDrive\CedeDrive\Release\*.exe"

erase "CedeDrive\CedeDrive\x64\Release\*.res"
erase "CedeDrive\CedeDrive\x64\Release\*.obj"
erase "CedeDrive\CedeDrive\x64\Release\*.exe"

erase "CedeDrive\CedeDrive\x64\Release (For Dev)\*.res"
erase "CedeDrive\CedeDrive\x64\Release (For Dev)\*.obj"
erase "CedeDrive\CedeDrive\x64\Release (For Dev)\*.exe"

erase "CedeDrive\CedeDrive\x64\Release (For Launcher)\*.res"
erase "CedeDrive\CedeDrive\x64\Release (For Launcher)\*.obj"
erase "CedeDrive\CedeDrive\x64\Release (For Launcher)\*.exe"

erase "CedeDrive\CedeDrive\x64\Release (Install-Dev)\*.res"
erase "CedeDrive\CedeDrive\x64\Release (Install-Dev)\*.obj"
erase "CedeDrive\CedeDrive\x64\Release (Install-Dev)\*.exe"

erase "CedeDrive\CedeDrive\Debug\*.res"
erase "CedeDrive\CedeDrive\Debug\*.obj"
erase "CedeDrive\CedeDrive\Debug\*.exe"

erase "CedeDrive\CedeDrive\Release (For Launcher)\*.res"
erase "CedeDrive\CedeDrive\Release (For Launcher)\*.obj"
erase "CedeDrive\CedeDrive\Release (For Launcher)\*.exe"

erase "CedeDriveService\CedeDriveService\Release\*.res"
erase "CedeDriveService\CedeDriveService\Release\*.obj"
erase "CedeDriveService\CedeDriveService\Release\*.exe"

erase "CedeDriveService\CedeDriveService\x64\Release\*.res"
erase "CedeDriveService\CedeDriveService\x64\Release\*.obj"
erase "CedeDriveService\CedeDriveService\x64\Release\*.exe"

erase "CedeDriveService\CedeDriveService\VS2010\CedeDriveService\Release\*.res"
erase "CedeDriveService\CedeDriveService\VS2010\CedeDriveService\Release\*.obj"
erase "CedeDriveService\CedeDriveService\VS2010\CedeDriveService\Release\*.exe"

erase "CedeDriveService\CedeDriveService\VS2010\CedeDriveService\x64\Release\*.res"
erase "CedeDriveService\CedeDriveService\VS2010\CedeDriveService\x64\Release\*.obj"
erase "CedeDriveService\CedeDriveService\VS2010\CedeDriveService\x64\Release\*.exe"

erase "PortableLauncher\CedeDriveLaunch\Release\*.obj"
erase "PortableLauncher\CedeDriveLaunch\Release\*.res"
erase "PortableLauncher\CedeDriveLaunch\Release\*.exe"

erase "PortableLauncher\CedeDriveLaunch\VS2010\CedeDriveLaunch\Release\*.obj"
erase "PortableLauncher\CedeDriveLaunch\VS2010\CedeDriveLaunch\Release\*.res"
erase "PortableLauncher\CedeDriveLaunch\VS2010\CedeDriveLaunch\Release\*.exe"

erase "ServiceDeployment\ServiceDeployment\Release\*.obj"
erase "ServiceDeployment\ServiceDeployment\Release\*.res"
erase "ServiceDeployment\ServiceDeployment\Release\*.exe"

erase "ServiceDeployment\ServiceDeployment\VS2010\ServiceDeployment\Release\*.obj"
erase "ServiceDeployment\ServiceDeployment\VS2010\ServiceDeployment\Release\*.res"
erase "ServiceDeployment\ServiceDeployment\VS2010\ServiceDeployment\Release\*.exe"






echo Building the CedeDrive Manager Service...

"C:\Program Files (x86)\Microsoft Visual Studio 10.0\Common7\IDE\devenv.exe" /rebuild "Release|Win32" CedeDriveService\CedeDriveService\VS2010\CedeDriveService\CedeDriveService.sln

"C:\Program Files (x86)\Microsoft Visual Studio 10.0\Common7\IDE\devenv.exe" /rebuild "Release|x64" CedeDriveService\CedeDriveService\VS2010\CedeDriveService\CedeDriveService.sln


echo Building the CedeDrive Service Deployment installer...

"C:\Program Files (x86)\Microsoft Visual Studio 10.0\Common7\IDE\devenv.exe" /rebuild "Release|Win32" ServiceDeployment\ServiceDeployment\VS2010\ServiceDeployment\ServiceDeployment.sln


echo Building the Login Hook Interceptor DLL...

"C:\Program Files (x86)\Microsoft Visual Studio 10.0\Common7\IDE\devenv.exe" /rebuild "Release|Win32" CedeDriveLHI\CedeDriveLHI\CedeDriveLHI.sln

"C:\Program Files (x86)\Microsoft Visual Studio 10.0\Common7\IDE\devenv.exe" /rebuild "Release|x64" CedeDriveLHI\CedeDriveLHI\CedeDriveLHI.sln


echo Building minimised version for the Portable Launcher...

"C:\Program Files (x86)\Microsoft Visual Studio 10.0\Common7\IDE\devenv.exe" /rebuild "Release (For Launcher)|Win32" CedeDrive\CedeDrive\CedeDrive2010\CedeDrive2010.sln

"C:\Program Files (x86)\Microsoft Visual Studio 10.0\Common7\IDE\devenv.exe" /rebuild "Release (For Launcher)|x64" CedeDrive\CedeDrive\CedeDrive2010\CedeDrive2010.sln


echo Building the Launcher (CDLDR)...

"C:\Program Files (x86)\Microsoft Visual Studio 10.0\Common7\IDE\devenv.exe" /rebuild "Release|Win32" PortableLauncher\CedeDriveLaunch\VS2010\CedeDriveLaunch\CedeDriveLaunch.sln


echo Building the Elevated Launcher...

"C:\Program Files (x86)\Microsoft Visual Studio 10.0\Common7\IDE\devenv.exe" /rebuild "Release|Win32" RemovableBootstrapper\CedeDriveLaunch\CedeDriveLaunch.sln

echo Signing the Elevated Launcher...

call signfile RemovableBootstrapper\CedeDriveLaunch\Release\CedeDriveLaunch.exe


echo Building CedeDrive normal version for installation...

"C:\Program Files (x86)\Microsoft Visual Studio 10.0\Common7\IDE\devenv.exe" /rebuild "Release (For Install)|Win32" CedeDrive\CedeDrive\CedeDrive2010\CedeDrive2010.sln

"C:\Program Files (x86)\Microsoft Visual Studio 10.0\Common7\IDE\devenv.exe" /rebuild "Release (For Install)|x64" CedeDrive\CedeDrive\CedeDrive2010\CedeDrive2010.sln


echo Copying built executables to installer directory...

copy CedeDrive\CedeDrive\CedeDrive2010\Release\CedeDrive2010.exe Installer\32bit\CedeDriveInstaller32\CedeDriveInstaller\CedeDrive.exe /Y
copy CedeDrive\CedeDrive\CedeDrive2010\x64\Release\CedeDrive2010.exe Installer\64bit\CedeDriveInstaller64\CedeDriveInstaller64\CedeDrive.exe /Y

copy CedeDriveLHI\CedeDriveLHI\Release\CedeDriveLHI.dll Installer\32bit\CedeDriveInstaller32\CedeDriveInstaller\CedeDriveLHI.dll /Y
copy CedeDriveLHI\CedeDriveLHI\x64\Release\CedeDriveLHI.dll Installer\64bit\CedeDriveInstaller64\CedeDriveInstaller64\CedeDriveLHI.dll /Y

copy ServiceDeployment\ServiceDeployment\VS2010\ServiceDeployment\Release\ServiceDeployment.exe Installer\32bit\CedeDriveInstaller32\CedeDriveInstaller\ServiceDeployment.exe /Y
copy ServiceDeployment\ServiceDeployment\VS2010\ServiceDeployment\Release\ServiceDeployment.exe Installer\64bit\CedeDriveInstaller64\CedeDriveInstaller64\ServiceDeployment.exe /Y

copy PortableLauncher\CedeDriveLaunch\VS2010\CedeDriveLaunch\Release\CedeDriveLaunch.exe Installer\CedeDriveLaunch.exe /Y

copy CedeDriveUserGuide.pdf Installer\32bit\CedeDriveInstaller32\CedeDriveInstaller\CedeDriveUserGuide.pdf /Y
copy CedeDriveService\CedeDriveService\VS2010\CedeDriveService\Release\CedeDriveService.exe Installer\32bit\CedeDriveInstaller32\CedeDriveInstaller\CedeDriveService.exe /Y
copy CedeDriveService\CedeDriveService\VS2010\CedeDriveService\Release\VDSDKDll.dll Installer\32bit\CedeDriveInstaller32\CedeDriveInstaller\VDSDKDll.dll /Y
copy CedeDriveService\CedeDriveService\VS2010\CedeDriveService\Release\VDCore.dll Installer\32bit\CedeDriveInstaller32\CedeDriveInstaller\VDCore.dll /Y

copy CedeDriveUserGuide.pdf Installer\64bit\CedeDriveInstaller64\CedeDriveInstaller64\CedeDriveUserGuide.pdf /Y
copy CedeDriveService\CedeDriveService\VS2010\CedeDriveService\x64\Release\CedeDriveService.exe Installer\64bit\CedeDriveInstaller64\CedeDriveInstaller64\CedeDriveService.exe /Y
copy CedeDriveService\CedeDriveService\VS2010\CedeDriveService\x64\Release\VDSDKDll.dll Installer\64bit\CedeDriveInstaller64\CedeDriveInstaller64\VDSDKDll.dll /Y
copy CedeDriveService\CedeDriveService\VS2010\CedeDriveService\x64\Release\VDCore.dll Installer\64bit\CedeDriveInstaller64\CedeDriveInstaller64\VDCore.dll /Y

echo Signing built executables...

call signfile Installer\32bit\CedeDriveInstaller32\CedeDriveInstaller\CedeDrive.exe
call signfile Installer\64bit\CedeDriveInstaller64\CedeDriveInstaller64\CedeDrive.exe


echo Building installer...
"C:\Program Files (x86)\Microsoft Visual Studio 10.0\Common7\IDE\devenv.exe" /rebuild "Release|x86" Installer\32bit\CedeDriveInstaller32\CedeDriveInstaller\CedeDriveInstaller.sln
"C:\Program Files (x86)\Microsoft Visual Studio 10.0\Common7\IDE\devenv.exe" /rebuild "Release|x86" Installer\64bit\CedeDriveInstaller64\CedeDriveInstaller64\CedeDriveInstaller64.sln

echo Copying setup files to output directory

copy Installer\32bit\CedeDriveInstaller32\CedeDriveInstaller\bin\Release\CedeDriveInstaller.msi Installer\CedeDriveSetup-32bit.msi
copy Installer\64bit\CedeDriveInstaller64\CedeDriveInstaller64\bin\Release\CedeDriveInstaller64.msi Installer\CedeDriveSetup-64bit.msi

echo Signing Built MSI files...

call signfile Installer\CedeDriveSetup-32bit.msi
call signfile Installer\CedeDriveSetup-64bit.msi

echo Now building the MSI Bootstrapper...

"C:\Program Files (x86)\Microsoft Visual Studio 10.0\Common7\IDE\devenv.exe" /rebuild "Release|Win32" MSIBootstrapper\CedeDriveSetup\CedeDriveSetup.sln

echo Copying built Setup.exe to Installer dir...
copy MSIBootstrapper\CedeDriveSetup\Release\CedeDriveSetup.exe Installer\CedeDriveSetup.exe /Y

echo Signing Setup Executable...

call signfile Installer\CedeDriveSetup.exe


explorer Installer
