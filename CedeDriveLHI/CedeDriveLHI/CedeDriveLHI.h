/*----------------------
   CEDEDRIVELHI.H header file Library header for CedeDrive Secure Login Window
   v1.00 - Created by CedeSoft Ltd.
  ----------------------*/

#ifdef __cplusplus
#define EXPORT extern "C" __declspec (dllexport)
#else
#define EXPORT __declspec (dllexport)
#endif

// DLL Library export routines, used by the main CedeDrive process
EXPORT BOOL SetMouseHook (HWND hwnd, UINT UpdateMsg);
EXPORT BOOL SetKeybHook (HWND hwnd, UINT UpdateMsg);
EXPORT BOOL UnsetHooks (HWND hwnd);
EXPORT BOOL SetKeyboardDisabled (BOOL bState);
EXPORT BOOL SetMouseDisabled (BOOL bState);
EXPORT BOOL DisableKeyboardShortcuts ();
EXPORT BOOL EnableKeyboardShortcuts ();