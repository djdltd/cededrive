/*-------------------------------------------------
   CTHOOK.CPP -- CedeTracker Agent v1.00 Library (CTHook.DLL) to insert low level hooks into the OS so that we can monitor mouse and keyboard input.
   Created by CedeSoft Ltd. Copyright (2008).
  -------------------------------------------------*/
//#ifndef _WIN32_WINDOWS        // Allow use of features specific to Windows 98 or later.
#define _WIN32_WINDOWS 0x0501 // Change this to the appropriate value to target Windows Me or later.
#define _WIN32_WINNT 0x0501
//#endif
#include <stdio.h>
#include  <windows.h>
#include "CedeDriveLHI.h"

// *************************************************************
//			GLOBAL VARIABLES
// *************************************************************

HINSTANCE g_hInstance; // Global handle to the instance of the Library
HWND g_MasterHWND; // Window handle of our parent (Main virtuajump process)
UINT g_MouseUpdateMsg; // mouse update message id
UINT g_KeybUpdateMsg; // keyboard update message id
HHOOK hMouseHook; // Handle to mouse hooks
HHOOK hKeybHook; // Handle to keyboard hooks
BOOL g_KeyboardDisabled = FALSE;
BOOL g_MouseDisabled = FALSE;
BOOL g_shortcutsdisabled = FALSE;

// *************************************************************
//			FUNCTION PROTOTYPES
// *************************************************************

LRESULT CALLBACK LowLevelMouseProc (int nCode, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK LowLevelKeyboardProc (int nCode, WPARAM wParam, LPARAM lParam);

// *************************************************************
//			IMPLEMENTATION
// *************************************************************

int WINAPI DllMain (HINSTANCE hInstance, DWORD fdwReason, PVOID pvReserved)
{
	
	switch (fdwReason)
	{
		case DLL_PROCESS_ATTACH:
			// Save the instance handle to a global variable
			g_hInstance = (HINSTANCE) hInstance;
			
			return TRUE;
		case DLL_PROCESS_DETACH:
			// The process has detached from the libary, so clean up after ourselves.
			UnhookWindowsHookEx (hMouseHook);
			UnhookWindowsHookEx (hKeybHook);
			hKeybHook = NULL;
			hMouseHook = NULL;
			return TRUE;
		default:
			return TRUE;
	}
}

EXPORT BOOL DisableKeyboardShortcuts ()
{
	g_shortcutsdisabled = TRUE;
	return TRUE;
}

EXPORT BOOL EnableKeyboardShortcuts ()
{
	g_shortcutsdisabled = FALSE;
	return FALSE;
}

EXPORT BOOL SetMouseHook (HWND hwnd, UINT UpdateMsg)
{
	// Don't add the hook if the window handle is null
	if (hwnd == NULL)
		return FALSE;
	
	// Hook into low level mouse instructions
	hMouseHook = SetWindowsHookEx (WH_MOUSE_LL, (HOOKPROC) LowLevelMouseProc, g_hInstance, 0L);
	
	// Check that we were successful
	if (hMouseHook != NULL) {
		g_MasterHWND = hwnd;
		g_MouseUpdateMsg = UpdateMsg;
		return TRUE;
	} else {
		UnhookWindowsHookEx (hMouseHook);
		return FALSE;
	}
}

EXPORT BOOL SetKeybHook (HWND hwnd, UINT UpdateMsg)
{
	// Don't add the hook if the window handle is null
	if (hwnd == NULL)
		return FALSE;
	
	// Hook into low level keyboard instructions
	hKeybHook = SetWindowsHookEx (WH_KEYBOARD_LL, (HOOKPROC) LowLevelKeyboardProc, g_hInstance, 0L);
	
	// Check that we were successful
	if (hKeybHook != NULL) {
		g_MasterHWND = hwnd;
		g_KeybUpdateMsg = UpdateMsg;
		return TRUE;
	} else {
		UnhookWindowsHookEx (hKeybHook);
		return FALSE;
	}
}

EXPORT BOOL UnsetHooks (HWND hwnd)
{
	// The process has detached from the libary, so clean up after ourselves.
	UnhookWindowsHookEx (hMouseHook);
	UnhookWindowsHookEx (hKeybHook);
	hKeybHook = NULL;
	hMouseHook = NULL;
	return TRUE;
}

EXPORT BOOL SetKeyboardDisabled (BOOL bState) {
	g_KeyboardDisabled = bState;
	return TRUE;
}

EXPORT BOOL SetMouseDisabled (BOOL bState) {
	g_MouseDisabled = bState;
	return TRUE;
}

LRESULT CALLBACK LowLevelMouseProc (int nCode, WPARAM wParam, LPARAM lParam)
{
	// Do we have to handle this message?
	if (nCode >= 0)
	{
		// Pass the mouse information to our parent (VirtuaJump main process)
		SendMessage (g_MasterHWND, g_MouseUpdateMsg, wParam, lParam);
	}
	
	if (g_MouseDisabled == TRUE) {
		if (wParam == WM_LBUTTONDOWN || wParam == WM_LBUTTONUP || wParam == WM_RBUTTONDOWN || wParam == WM_RBUTTONUP || wParam == WM_MOUSEWHEEL) {
			return 1;
		} else {
			return CallNextHookEx (hMouseHook, nCode, wParam, lParam);
		}
	} else {
		return CallNextHookEx (hMouseHook, nCode, wParam, lParam);
	}
}

void ShowInt (int iInttoShow) {
	char szMsg[255];
	ZeroMemory (szMsg, 255);
	
	sprintf (szMsg, "Value of integer: %d", iInttoShow);
	MessageBox (NULL, szMsg, "ShowInt", MB_OK);
}

LRESULT CALLBACK LowLevelKeyboardProc (int nCode, WPARAM wParam, LPARAM lParam)
{
	/*
	// Do we have to handle this message?
	if (nCode >= 0)
	{
		// Pass the keyboard information to our parent (VirtuaJump main process)
		SendMessage (g_MasterHWND, g_KeybUpdateMsg, wParam, lParam);
		Beep (2000, 100);
	}
	
	if (g_KeyboardDisabled == TRUE) {
		return 1;
	} else {
		return CallNextHookEx (hKeybHook, nCode, wParam, lParam);
	}
	*/

	// By returning a non-zero value from the hook procedure, the
    // message does not get passed to the target window
    KBDLLHOOKSTRUCT *pkbhs = (KBDLLHOOKSTRUCT *) lParam;
    BOOL bControlKeyDown = 0;
	BOOL bWindowsLKeyDown = 0;
	BOOL bWindowsRKeyDown = 0;	
	BOOL bDeleteKeyDown = 0;

	if (g_shortcutsdisabled == TRUE) {

		switch (nCode)
		{
			case HC_ACTION:
			{
				// Check to see if the CTRL key is pressed
				bControlKeyDown = GetAsyncKeyState (VK_CONTROL) >> ((sizeof(SHORT) * 8) - 1);
				bWindowsLKeyDown = GetAsyncKeyState (VK_LWIN) >> ((sizeof (SHORT) * 8) - 1);
				bWindowsRKeyDown = GetAsyncKeyState (VK_RWIN) >> ((sizeof (SHORT) * 8) - 1);
				bDeleteKeyDown = GetAsyncKeyState (VK_DELETE) >> ((sizeof (SHORT) * 8) - 1);

				//SendMessage (g_MasterHWND, g_MouseUpdateMsg, wParam, lParam);
				// Disable CTRL+ESC
				if (pkbhs->vkCode == VK_ESCAPE && bControlKeyDown)
				{
					SendMessage (g_MasterHWND, g_KeybUpdateMsg, wParam, lParam);
					return 1;
				}
				// Disable ALT+TAB
				if (pkbhs->vkCode == VK_TAB && pkbhs->flags & LLKHF_ALTDOWN)
				{
					SendMessage (g_MasterHWND, g_KeybUpdateMsg, wParam, lParam);
					return 1;
				}
				// Disable ALT+ESC
				if (pkbhs->vkCode == VK_ESCAPE && pkbhs->flags & LLKHF_ALTDOWN)
				{
					SendMessage (g_MasterHWND, g_KeybUpdateMsg, wParam, lParam);
					return 1;
				}
				// Disable Left-Win + TAB
				if (pkbhs->vkCode == VK_TAB && bWindowsLKeyDown)
				{
					SendMessage (g_MasterHWND, g_KeybUpdateMsg, wParam, lParam);
					return 1;
				}
				// Disable Right-Win + TAB
				if (pkbhs->vkCode == VK_TAB && bWindowsRKeyDown)
				{
					SendMessage (g_MasterHWND, g_KeybUpdateMsg, wParam, lParam);
					return 1;
				}
				// Disable Right-Win
				if (pkbhs->vkCode == VK_RWIN)
				{
					//SendMessage (g_MasterHWND, g_KeybUpdateMsg, wParam, lParam);
					//UnsetHooks (g_MasterHWND);
					//SetKeybHook (g_MasterHWND, g_KeybUpdateMsg);
					//CallNextHookEx (hKeybHook, nCode, wParam, lParam);

					return 1;
				}
				// Disable Left-Win
				if (pkbhs->vkCode == VK_LWIN)
				{
					//SendMessage (g_MasterHWND, g_KeybUpdateMsg, wParam, lParam);
					//UnsetHooks (g_MasterHWND);
					//SetKeybHook (g_MasterHWND, g_KeybUpdateMsg);
					//CallNextHookEx (hKeybHook, nCode, wParam, lParam);

					return 1;
				}
				// Try to disable ctrl-alt-delete
				//if (bControlKeyDown && bDeleteKeyDown && pkbhs->flags & LLKHF_ALTDOWN)
					//return 1;

				break;
			}

			default:
				break;
		}

	}


    return CallNextHookEx (hKeybHook, nCode, wParam, lParam);

}
