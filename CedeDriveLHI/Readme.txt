This contains the solution for the CedeDrive Login Hook Interceptor (LHI)

Basically, when a computer comes out of standby and needs to present a password dialog to the user, a new secure password window is shown which temporarily disables all of the task switching hot keys, like Ctrl Alt, and Ctrl-Escape. In order to disable these hot keys on Windows NT, you have to create a keyboard hook and intercept the keyboard hotkeys and stop them from being passed on to the OS. The only way to create a global hook such as this is in a DLL, and that is what this DLL does.

