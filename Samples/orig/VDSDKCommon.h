#ifndef _VSDK_COMMON_DECL_H_
#define _VSDK_COMMON_DECL_H_

#include <windows.h>

typedef long DRIVE_HANDLE;
#define INVALID_DRIVE_HANDLE -1

// -------------------------------------------
// Callback handlers definitions
// -------------------------------------------

/*
Read callback typedef
@param h drive handle
@param ReadOffset read data offset
@param ReadSize size of data to read
@param ReadBuffer pointer to the destination buffer
@param BytesRead actually read bytes count
@return result
*/
typedef BOOL (_stdcall *ON_READ_CALLBACK)(
    DRIVE_HANDLE h,
    ULONGLONG ReadOffset,
    ULONG ReadSize,
    void* ReadBuffer,
    ULONG *BytesRead
    );

/*
Write callback typedef
@param h drive handle
@param Writeffset write data offset
@param WriteSize size of data to write
@param WriteBuffer pointer to the destination buffer
@param BytesWritten actually written bytes count
@return  result
*/
typedef BOOL (_stdcall *ON_WRITE_CALLBACK)(
    DRIVE_HANDLE h,
    ULONGLONG WriteOffset,
    ULONG WriteSize,
    const void* WriteBuffer,
    ULONG *BytesWritten
    );





// VDSDK notifications
enum
{
    VDSDK_OnUnmountRemovableMedia = 0, // removable media is being unmounted
    VDSDK_OnUnmountedDrive = 1, // drive has been unmounted
};

/*
VDSDK notifications handler typedef
@param h drive handle
@param NotificationCode notification code
@param Reserved ignored
@return  result
*/
typedef BOOL (_stdcall *ON_VDSDK_NOTIFICATION)(
    DRIVE_HANDLE h,
    ULONG NotificationCode,
    ULONG Reserved
    );




// -------------------------------------------
// Callback handlers for drive formatting
// -------------------------------------------

// Format callback
// @param Progress: Progress in percents
// @return ignored
typedef BOOL (_stdcall *ON_FORMAT_CALLBACK)(ULONG Progress);


#endif
