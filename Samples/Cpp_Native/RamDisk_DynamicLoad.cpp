#include <windows.h>
#include <stdio.h>
#include <vector>
#include <conio.h>

#define _VDSDK_DECLARE_FUNCTION_POINTERS_
#include "VDSDKDynamicLoad.h"



#define DISK_SIZE 16
std::vector<char> g_memory;
DRIVE_HANDLE g_hDrive = INVALID_DRIVE_HANDLE;

int CreateVirtualDisk();

int main(int argc, char* argv[])
{
    if(!InitializeVDSDKFunctionPointers())
    {
        printf("Error: Can not initialize VDSDK function pointers\n");
        return 1;
    }

    if(!InitializeVDSDK())
    {
        printf("Error: Can not initialize VDSDK\n");
        return 1;
    }

    // Activate VDSDK.
    // First parameter is activation key shipped with commercial license
    // In demo version this method always returns TRUE
    if(!ActivateVDSDK(""))
    {
        printf("ERROR: can not activate VDSDK\n");
        return 1;
    }


    CreateVirtualDisk();

    // Shutdown VDSDK
    ShutdownVDSDK(TRUE);

    return 0;
}


// ===========================================
// Virtual disk implementation
// ===========================================

// read callback handler
BOOL _stdcall OnReadCallback
(
 DRIVE_HANDLE h,
 ULONGLONG ReadOffset,
 ULONG ReadSize,
 void* ReadBuffer,
 ULONG *BytesRead
 )
{
    printf("OnReadCallback: size=%X\n",ReadSize);

    memcpy(ReadBuffer
        ,&g_memory[0] + ReadOffset,
        ReadSize);

    *BytesRead = ReadSize;
    return TRUE;
}


// write callback handler
BOOL _stdcall OnWriteCallback
(
 DRIVE_HANDLE h,
 ULONGLONG WriteOffset,
 ULONG WriteSize,
 const void* WriteBuffer,
 ULONG *BytesWritten
 )
{
    printf("OnWriteCallback: size=%X\n",WriteSize);

    memcpy(&g_memory[0] + WriteOffset,
        WriteBuffer,
        WriteSize);

    *BytesWritten = WriteSize;

    return TRUE;
}


int CreateVirtualDisk()
{
    printf("Creating drive...\n");

    // prepare memory buffer
    g_memory.resize(DISK_SIZE * 1024 * 1024);

    // create virtual disk
    g_hDrive = CreateVirtualDrive('Z',DISK_SIZE,
        OnReadCallback,
        OnWriteCallback);

    if(g_hDrive == INVALID_DRIVE_HANDLE)
    {
        printf("ERROR: can not create virtual drive\n");
        return 1;
    }

    printf("VirtualDrive created. Handle = %d\n",g_hDrive);
    printf("Press any key to quit\n");

    // wait until any key pressed
    getch();

    // dismount disk
    if(!DestroyVirtualDrive(g_hDrive,FALSE))
    {
        printf("Dismount failed. Forcing dismount operation...\n");
        // force dismount...
        DestroyVirtualDrive(g_hDrive,TRUE);
    }

    return 0;
}
