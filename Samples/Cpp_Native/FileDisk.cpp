#include <windows.h>
#include <stdio.h>
#include <vector>
#include <conio.h>

#include "VDSDKDll.h"
#include "MemoryBuffer.h"
#include "StandardEncryption.h"


#define DISK_SIZE 100
HANDLE g_hFile = INVALID_HANDLE_VALUE;
DRIVE_HANDLE g_hDrive = INVALID_DRIVE_HANDLE;
StandardEncryption g_enc;
char g_szPassword[SIZE_STRING];
MemoryBuffer g_memCipher;
bool g_useencryption = false;
int CreateVirtualDisk();

int main(int argc, char* argv[])
{

	if (g_useencryption == true) {
		strcpy_s (g_szPassword, SIZE_STRING, "password123");
		g_memCipher.SetSize (1024*1024);
		if (g_enc.EncryptBuffer (&g_memCipher, g_szPassword, true) == true) {
			printf ("Encryption Successful\n");
		} else {				
			printf ("Encryption Failed!!!\n");
		}
	}


    if(!InitializeVDSDK())
    {
        printf("Error: Can not initialize VDSDK\n");
        return 1;
    }

    // Activate VDSDK.
    // First parameter is activation key shipped with commercial license
    // In demo version this method always returns TRUE
    if(!ActivateVDSDK("+Gdd9LfTihfFs/DLL4XbkCDyBL3GQYD0IPIEvcZBgPQg8gS9xkGA9CDyBL3GQYD0IPIEvcZBgPQg8gS9xkGA9IJzlgZrh9vmbXLrR0L4CIUg8gS9xkGA9CDyBL3GQYD0IPIEvcZBgPQg8gS9xkGA9CDyBL3GQYD0IPIEvcZBgPSGjHphJVmb58EgDZk02prmIPIEvcZBgPQg8gS9xkGA9CDyBL3GQYD0IPIEvcZBgPQg8gS9xkGA9CDyBL3GQYD0uSfV5IlEc2JdnyPjffC4uliKy/LeIbBqIPIEvcZBgPQg8gS9xkGA9CDyBL3GQYD0IPIEvcZBgPSUAEPWwBV5b2X+fyEzUs9IfsMRLMlwaiJ2naPy83snBb7FBpDieoCyE+s0X/3LmL5Dv7ZiJKC1G5tvTM7c31P/oQyhIFj9TPvIWKh4YB2mT055xzRhjnYSyZmXSQvBpP/2JzuRv9hYDO0XDumWdgXapQnJxeJUifzkA7/yrCuSxLRqyRxeHyj/ZkQBMvXXDZKrF7OYNfGL7KMKBQmPYA7Fuh+JZskfc8O8NHQ10aVPw3NiEkz5XHjtotbR6eq+mzRsxbd1BF1tGlDQYQYKoSuNblh2Lj89w83qiZKfQcAAg83Km6R05NI1z3nG3NqLwLsqZX+kFyqdmN5Whdd2R2ahw3kr1wA/EdyUfsZ3y63pARaqydMrI9ui628ABC27CE572154FFEE62D6934BD756"))
    {
        printf("ERROR: can not activate VDSDK\n");
        return 1;
    }


    CreateVirtualDisk();

    // Shutdown VDSDK
    ShutdownVDSDK(TRUE);

    return 0;
}


// ===========================================
// Virtual disk implementation
// ===========================================

// read callback handler
BOOL _stdcall OnReadCallback
(
 DRIVE_HANDLE h,
 ULONGLONG ReadOffset,
 ULONG ReadSize,
 void* ReadBuffer,
 ULONG *BytesRead
 )
{
	BOOL result;

	if (g_useencryption == true) {

		unsigned long long lciphersize = g_memCipher.GetSize ();
		unsigned long bpos;
		unsigned long long encpos;	
		BYTE bp;
		BYTE bk;
		BYTE be;

		printf("OnReadCallback: size=%X\n",ReadSize);

		LARGE_INTEGER l;
		LARGE_INTEGER newpointer;
		DWORD dw;
		l.QuadPart = ReadOffset;

		//SetFilePointer(g_hFile,l.LowPart,&l.HighPart,FILE_BEGIN); // 32-bit Only
		SetFilePointerEx (g_hFile, l, &newpointer, FILE_BEGIN);


		printf ("OnReadCallback - Creating encryption buffer...\n");
		MemoryBuffer memEnc;
		memEnc.SetSize (ReadSize);

		result = ReadFile(g_hFile,memEnc.GetBuffer (),ReadSize,&dw,NULL);

		printf ("OnReadCallback - Beginning Decryption...\n");

		//memEnc.QuickCrypt (false);
		for (bpos=0;bpos<memEnc.GetSize ();bpos++) {
			
			encpos = (ReadOffset + bpos) % lciphersize;
			
			bp = memEnc.GetByte (bpos);
			bk = g_memCipher.GetByte (encpos);
			be = bp ^ bk;
			
			memEnc.SetByte (bpos, be);
		}

		printf ("OnReadCallback - Writing data...\n");

		memcpy (ReadBuffer, memEnc.GetBuffer (), ReadSize);

		memEnc.Clear ();


		//BOOL result = ReadFile(g_hFile,ReadBuffer,ReadSize,&dw,NULL); // Without decryption
		*BytesRead = dw;
	
	} else {
		
		printf("OnReadCallback: size=%X\n",ReadSize);

		LARGE_INTEGER l;
		LARGE_INTEGER newpointer;
		DWORD dw;
		l.QuadPart = ReadOffset;
		//SetFilePointer(g_hFile,l.LowPart,&l.HighPart,FILE_BEGIN);
		SetFilePointerEx (g_hFile, l, &newpointer, FILE_BEGIN);
		result = ReadFile(g_hFile,ReadBuffer,ReadSize,&dw,NULL);
		*BytesRead = dw;

	}

    return result;
}


// write callback handler
BOOL _stdcall OnWriteCallback
(
 DRIVE_HANDLE h,
 ULONGLONG WriteOffset,
 ULONG WriteSize,
 const void* WriteBuffer,
 ULONG *BytesWritten
 )
{
	BOOL result;

	if (g_useencryption == true) {
		unsigned long bpos;
		unsigned long long encpos;
		unsigned long long lciphersize = g_memCipher.GetSize ();
		BYTE bp;
		BYTE bk;
		BYTE be;

		printf("OnWriteCallback: size=%X\n",WriteSize);


		printf ("OnWriteCallback - Creating encryption buffer...\n");
		MemoryBuffer memEnc;
		memEnc.SetSize (WriteSize);
		memEnc.Write ((BYTE *) WriteBuffer, 0, WriteSize);



		printf ("OnWriteCallback - Beginning Encryption...\n");
		
		for (bpos=0;bpos<memEnc.GetSize ();bpos++) {
			
			encpos = (WriteOffset + bpos) % lciphersize;		

			bp = memEnc.GetByte (bpos);
			bk = g_memCipher.GetByte (encpos);
			be = bp ^ bk;
			
			memEnc.SetByte (bpos, be);
		}

		LARGE_INTEGER l;
		LARGE_INTEGER newpointer;
		DWORD dw;
		l.QuadPart = WriteOffset;
		//SetFilePointer(g_hFile,l.LowPart,&l.HighPart,FILE_BEGIN); // 32-bit only limited to 4GB
		SetFilePointerEx (g_hFile, l, &newpointer, FILE_BEGIN);
		//BOOL result = WriteFile(g_hFile,WriteBuffer,WriteSize,&dw,NULL); // Write without encryption

		result = WriteFile (g_hFile, memEnc.GetBuffer(), WriteSize, &dw, NULL); // Write with encryption

		memEnc.Clear ();


		*BytesWritten = dw;
	} else {
	
	    printf("OnWriteCallback: size=%X\n",WriteSize);

		LARGE_INTEGER l;
		LARGE_INTEGER newpointer;
		DWORD dw;
		l.QuadPart = WriteOffset;
		
		//SetFilePointer(g_hFile,l.LowPart,&l.HighPart,FILE_BEGIN);
		SetFilePointerEx (g_hFile, l, &newpointer, FILE_BEGIN);

		result = WriteFile(g_hFile,WriteBuffer,WriteSize,&dw,NULL);

		*BytesWritten = dw;
	}

    return result;
}



void AllocateDiskSpace()
{
    unsigned long long DiskSize = (unsigned long long) DISK_SIZE * 1024 * 1024;
	//unsigned long long DiskSize = 10485760000;
    DWORD dw;

    LARGE_INTEGER currentSize;
	LARGE_INTEGER newPointerlocation;
    //currentSize.LowPart = GetFileSize(g_hFile,(LPDWORD)&currentSize.HighPart);
    
	GetFileSizeEx (g_hFile, &currentSize);
	
	if(currentSize.QuadPart >= DiskSize)
    {
        // already allocated
        //SetFilePointer(g_hFile,currentSize.LowPart,&currentSize.HighPart,FILE_BEGIN);
		
		SetFilePointerEx (g_hFile, currentSize, &newPointerlocation, FILE_BEGIN);

        SetEndOfFile(g_hFile);
        return;
    }


    printf("Allocating disk space...\n");

    const int ChunkSize = 1024 * 1024; // 1 MB
    char* ZeroBuffer = new char[ChunkSize];

    unsigned ChunksCount = (unsigned)(DiskSize / ChunkSize);
    for(unsigned i = 0;i<ChunksCount;++i)
    {
        printf("Allocating: %d%% of 100%\n",i * 100 /ChunksCount);
        if(!WriteFile(g_hFile,ZeroBuffer,ChunkSize,&dw,NULL))
        {
            printf("ERROR\n");
        }
   }

    delete[] ZeroBuffer;
    printf("Allocation completed\n");
}


int CreateVirtualDisk()
{
    printf("Creating drive...\n");

	g_hFile = CreateFile("C:\\Temp\\disktest.dat",GENERIC_READ | GENERIC_WRITE,FILE_SHARE_READ,0,
        OPEN_ALWAYS,FILE_FLAG_NO_BUFFERING | FILE_FLAG_RANDOM_ACCESS,0);

    if(g_hFile == INVALID_HANDLE_VALUE)
    {
        printf("ERROR: can not create / open disk image file\n");
        return 1;
    }

    // allocate disk space
    AllocateDiskSpace();
	

    // create virtual disk
    g_hDrive = CreateVirtualDrive('Z',DISK_SIZE,
        OnReadCallback,
        OnWriteCallback);

    if(g_hDrive == INVALID_DRIVE_HANDLE)
    {
        printf("ERROR: can not create virtual drive\n");
        return 1;
    }

    printf("VirtualDrive created. Handle = %d\n",g_hDrive);
    printf("Press any key to quit\n");

    // wait until any key pressed
    getch();

    // force dismount disk
    DestroyVirtualDrive(g_hDrive,TRUE);

    CloseHandle(g_hFile);

    return 0;
}
