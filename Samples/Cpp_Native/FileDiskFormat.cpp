#include <windows.h>
#include <stdio.h>
#include <vector>
#include <conio.h>

#include "VDSDKDll.h"


#define DISK_SIZE 256
HANDLE g_hFile = INVALID_HANDLE_VALUE;
DRIVE_HANDLE g_hDrive = INVALID_DRIVE_HANDLE;
BOOL g_bLogReadWriteCallbacks = FALSE;

int CreateVirtualDisk();

int main(int argc, char* argv[])
{
    if(!InitializeVDSDK())
    {
        printf("Error: Can not initialize VDSDK\n");
        return 1;
    }

    // Activate VDSDK.
    // First parameter is activation key shipped with commercial license
    // In demo version this method always returns TRUE
    if(!ActivateVDSDK(""))
    {
        printf("ERROR: can not activate VDSDK\n");
        return 1;
    }


    CreateVirtualDisk();

    // Shutdown VDSDK
    ShutdownVDSDK(TRUE);

    return 0;
}


// ===========================================
// Virtual disk implementation
// ===========================================

// read callback handler
BOOL _stdcall OnReadCallback
(
 DRIVE_HANDLE h,
 ULONGLONG ReadOffset,
 ULONG ReadSize,
 void* ReadBuffer,
 ULONG *BytesRead
 )
{
    if(g_bLogReadWriteCallbacks)
        printf("OnReadCallback: size=%X\n",ReadSize);

    LARGE_INTEGER l;
    DWORD dw;
    l.QuadPart = ReadOffset;
    SetFilePointer(g_hFile,l.LowPart,&l.HighPart,FILE_BEGIN);
    BOOL result = ReadFile(g_hFile,ReadBuffer,ReadSize,&dw,NULL);
    *BytesRead = dw;
    return result;
}


// write callback handler
BOOL _stdcall OnWriteCallback
(
 DRIVE_HANDLE h,
 ULONGLONG WriteOffset,
 ULONG WriteSize,
 const void* WriteBuffer,
 ULONG *BytesWritten
 )
{
    if(g_bLogReadWriteCallbacks)
        printf("OnWriteCallback: size=%X\n",WriteSize);

    LARGE_INTEGER l;
    DWORD dw;
    l.QuadPart = WriteOffset;
    SetFilePointer(g_hFile,l.LowPart,&l.HighPart,FILE_BEGIN);
    BOOL result = WriteFile(g_hFile,WriteBuffer,WriteSize,&dw,NULL);

    *BytesWritten = dw;

    return result;
}

BOOL _stdcall OnFormatCallbackHandler(ULONG Progress)
{
    printf("Formatting: %d%% of 100%% completed\n",Progress);
    return TRUE;
}


void AllocateDiskSpace()
{
    LONGLONG DiskSize = DISK_SIZE * 1024 * 1024;
    DWORD dw;

    LARGE_INTEGER currentSize;
    currentSize.LowPart = GetFileSize(g_hFile,(LPDWORD)&currentSize.HighPart);
    if(currentSize.QuadPart >= DiskSize)
    {
        // already allocated
        SetFilePointer(g_hFile,currentSize.LowPart,&currentSize.HighPart,FILE_BEGIN);
        SetEndOfFile(g_hFile);
        return;
    }

    printf("Allocating disk space...\n");

    const int ChunkSize = 1024 * 1024; // 1 MB
    char* ZeroBuffer = new char[ChunkSize];

    unsigned ChunksCount = (unsigned)(DiskSize / ChunkSize);
    for(unsigned i = 0;i<ChunksCount;++i)
    {
        printf("Allocating: %d%% of 100%\n",i * 100 /ChunksCount);
        if(!WriteFile(g_hFile,ZeroBuffer,ChunkSize,&dw,NULL))
        {
            printf("ERROR\n");
        }
    }

    delete[] ZeroBuffer;
    printf("Allocation completed\n");
}


int CreateVirtualDisk()
{
    printf("Creating drive...\n");

    g_hFile = CreateFile("disk.dat",GENERIC_READ | GENERIC_WRITE,FILE_SHARE_READ,0,
        OPEN_ALWAYS,FILE_FLAG_NO_BUFFERING | FILE_FLAG_RANDOM_ACCESS,0);

    if(g_hFile == INVALID_HANDLE_VALUE)
    {
        printf("ERROR: can not create / open disk image file\n");
        return 1;
    }

    // allocate disk size
    AllocateDiskSpace();

    // create virtual disk
    g_hDrive = CreateVirtualDrive('Z',DISK_SIZE,
        OnReadCallback,
        OnWriteCallback);

    if(g_hDrive == INVALID_DRIVE_HANDLE)
    {
        printf("ERROR: can not create virtual drive\n");
        return 1;
    }

    printf("VirtualDrive created. Handle = %d\n",g_hDrive);


    // formatting
    printf("Formatting virtual drive...\n");

    // QuickFormat = FALSE, FileSystem=NTFS, VolumeLabel=VDSDK
    if(!FormatVirtualDrive(g_hDrive,FALSE,"NTFS","VDSDK", OnFormatCallbackHandler))
    {
        printf("Formatting error\n");
    }
    printf("Formatting completed\n");
    printf("Press any key to quit\n");

    // enable read/write logging
    g_bLogReadWriteCallbacks = TRUE;

    // wait until any key pressed
    getch();

    // force dismount disk
    DestroyVirtualDrive(g_hDrive,TRUE);

    CloseHandle(g_hFile);

    return 0;
}
