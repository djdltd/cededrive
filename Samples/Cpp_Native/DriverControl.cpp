#include <windows.h>
#include <stdio.h>
#include <vector>
#include <conio.h>

#include "VDSDKDll.h"


void print_usage()
{
    printf("Usage:\n");
    printf("----- driver commands ------\n");
    printf("-install\n");
    printf("-uninstall\n");
    printf("-start\n");
    printf("-stop\n");
}


int main(int argc, char* argv[])
{
    BOOL b;

    if(argc != 1 && argc != 2)
    {
        print_usage();
        return -1;
    }

    const char *command = argv[1];

    if(argc == 2)
    {
        if(!strcmp(command,"-install"))
        {
            printf("Installing driver...\n");
            b = InstallDriver();
            printf("%s\n",b ? "OK\n" : "FAILED\n");
            return 0;
        }
        if(!strcmp(command,"-start"))
        {
            printf("Starting driver...\n");
            b = StartDriver();
            printf("%s\n",b ? "OK\n" : "FAILED\n");
            return 0;
        }
        if(!strcmp(command,"-stop"))
        {
            printf("Stopping driver...\n");
            b = StopDriver();
            printf("%s\n",b ? "OK\n" : "FAILED\n");
            return 0;
        }
        if(!strcmp(command,"-uninstall"))
        {
            printf("Uninstalling driver...\n");
            b = UninstallDriver();
            printf("%s\n",b ? "OK\n" : "FAILED\n");
            return 0;
        }
    }

    int major,minor;
    if(GetVDSDKDriverVersion(major,minor))
    {
        printf("Detected VDSDK kernel driver %d.%03d\n",major,minor);
    }
    else
    {
        printf("VDSDK kernel driver is not found\n");
    }

    return 0;
}

