#ifndef _VSDK_DYNAMIC_LOAD_H_
#define _VSDK_DYNAMIC_LOAD_H_

/*
    Dynamic VDSDKDll.dll loading support.
    You should define _VDSDK_DECLARE_FUNCTION_POINTERS_ macro
    and include this file once in your code.
*/

#include "VDSDKCommon.h"

// ===================================
// Declare VDSDK functions prototypes
// ===================================
typedef BOOL (_stdcall *Proc_InitializeVDSDK)();
typedef void (_stdcall *Proc_ShutdownVDSDK)(BOOL bForceUnmountDisks);
typedef DRIVE_HANDLE (_stdcall *Proc_CreateVirtualDrive)(
    char DriveLetter,
    ULONG DriveSize,
    ON_READ_CALLBACK pReadCallback,
    ON_WRITE_CALLBACK pWriteCallback
    );

typedef DRIVE_HANDLE (_stdcall *Proc_CreateVirtualDriveEx)(
    char DriveLetter,
    ULONG DriveSize,
    ON_READ_CALLBACK pReadCallback,
    ON_WRITE_CALLBACK pWriteCallback,
    BOOL bReadOnly,
    ULONG DriveFlags
    );

typedef BOOL (_stdcall *Proc_DestroyVirtualDrive)(DRIVE_HANDLE h, BOOL bForce);
typedef int (_stdcall *Proc_GetVDSDKErrorCode)();
typedef BOOL (_stdcall *Proc_InstallDriver)();
typedef BOOL (_stdcall *Proc_StartDriver)();
typedef BOOL (_stdcall *Proc_StopDriver)();
typedef BOOL (_stdcall *Proc_UninstallDriver)();
typedef BOOL (_stdcall *Proc_IsDriverReady)();
typedef BOOL (_stdcall *Proc_GetVDSDKDriverVersion)(int &major, int &minor);
typedef BOOL (_stdcall *Proc_ActivateVDSDK)(const char* ActivationKey);
typedef BOOL (_stdcall *Proc_FormatVirtualDrive)(DRIVE_HANDLE h, BOOL bQuickFormat, const char* FileSystem, const char* VolumeLabel, ON_FORMAT_CALLBACK pCallback);
typedef void (_stdcall *Proc_RegisterNotificationHandler)(ON_VDSDK_NOTIFICATION pNotificationHandler);


// declaration of the function to correctly initialize pointers
bool InitializeVDSDKFunctionPointers(const char* DllPath = "VDSDKDll.dll");


#ifdef _VDSDK_DECLARE_FUNCTION_POINTERS_
// ===================================
// Declare pointers to VDSDK functions 
// ===================================
Proc_InitializeVDSDK InitializeVDSDK = NULL;
Proc_ShutdownVDSDK ShutdownVDSDK = NULL;
Proc_CreateVirtualDrive CreateVirtualDrive = NULL;
Proc_CreateVirtualDriveEx CreateVirtualDriveEx = NULL;
Proc_DestroyVirtualDrive DestroyVirtualDrive = NULL;
Proc_GetVDSDKErrorCode GetVDSDKErrorCode = NULL;
Proc_InstallDriver InstallDriver = NULL;
Proc_StartDriver StartDriver = NULL;
Proc_StopDriver StopDriver = NULL;
Proc_UninstallDriver UninstallDriver = NULL;
Proc_IsDriverReady IsDriverReady = NULL;
Proc_GetVDSDKDriverVersion GetVDSDKDriverVersion = NULL;
Proc_ActivateVDSDK ActivateVDSDK = NULL;
Proc_FormatVirtualDrive FormatVirtualDrive = NULL;
Proc_RegisterNotificationHandler RegisterNotificationHandler = NULL;

// ===================================
// Initialize function pointers
// ===================================
bool InitializeVDSDKFunctionPointers(const char* DllPath)
{
    HMODULE h = LoadLibrary(DllPath);
    if(h == NULL)
        return false;

    if((InitializeVDSDK = (Proc_InitializeVDSDK)GetProcAddress(h, "InitializeVDSDK")) == NULL)return false;
    if((ShutdownVDSDK = (Proc_ShutdownVDSDK)GetProcAddress(h, "ShutdownVDSDK")) == NULL)return false;
    if((CreateVirtualDrive = (Proc_CreateVirtualDrive)GetProcAddress(h, "CreateVirtualDrive")) == NULL)return false;
    if((CreateVirtualDriveEx = (Proc_CreateVirtualDriveEx)GetProcAddress(h, "CreateVirtualDriveEx")) == NULL)return false;
    if((DestroyVirtualDrive = (Proc_DestroyVirtualDrive)GetProcAddress(h, "DestroyVirtualDrive")) == NULL)return false;
    if((GetVDSDKErrorCode = (Proc_GetVDSDKErrorCode)GetProcAddress(h, "GetVDSDKErrorCode")) == NULL)return false;
    if((InstallDriver = (Proc_InstallDriver)GetProcAddress(h, "InstallDriver")) == NULL)return false;
    if((StartDriver = (Proc_StartDriver)GetProcAddress(h, "StartDriver")) == NULL)return false;
    if((StopDriver = (Proc_StopDriver)GetProcAddress(h, "StopDriver")) == NULL)return false;
    if((UninstallDriver = (Proc_UninstallDriver)GetProcAddress(h, "UninstallDriver")) == NULL)return false;
    if((IsDriverReady = (Proc_IsDriverReady)GetProcAddress(h, "IsDriverReady")) == NULL)return false;
    if((GetVDSDKDriverVersion = (Proc_GetVDSDKDriverVersion)GetProcAddress(h, "GetVDSDKDriverVersion")) == NULL)return false;
    if((ActivateVDSDK = (Proc_ActivateVDSDK)GetProcAddress(h, "ActivateVDSDK")) == NULL)return false;
    if((FormatVirtualDrive = (Proc_FormatVirtualDrive)GetProcAddress(h, "FormatVirtualDrive")) == NULL)return false;
    if((RegisterNotificationHandler = (Proc_RegisterNotificationHandler)GetProcAddress(h, "RegisterNotificationHandler")) == NULL)return false;

    return true;
}

#endif


#endif