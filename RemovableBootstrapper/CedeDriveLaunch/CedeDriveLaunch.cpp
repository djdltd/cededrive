#include "resource.h"
#include <Windows.h>



void GetPathOnly (char *szInpath, char *szOutpath)
{
	char szCurchar[SIZE_NAME];
	int seploc = 0;

	for (int c=strlen(szInpath);c>0;c--) {
		
		ZeroMemory (szCurchar, SIZE_NAME);
		strncpy_s (szCurchar, SIZE_NAME, szInpath+c, 1);

		if (strcmp (szCurchar, "\\") == 0) {
			seploc = c+1;
			break;
		}
	}

	strncpy_s (szOutpath, SIZE_STRING, szInpath, seploc);
}

int WINAPI WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {

	char szModulefilename[SIZE_STRING];
	ZeroMemory (szModulefilename, SIZE_STRING);
	GetModuleFileName (NULL, szModulefilename, SIZE_STRING);

	char szPathonly[SIZE_STRING];
	ZeroMemory (szPathonly, SIZE_STRING);

	GetPathOnly (szModulefilename, szPathonly);

	//MessageBox (NULL, szPathonly, "Module Filename", MB_OK);

	strcat_s (szPathonly, SIZE_STRING, "cdldr");

	STARTUPINFO si;
    PROCESS_INFORMATION pi;

    ZeroMemory( &si, sizeof(si) );
    si.cb = sizeof(si);
    ZeroMemory( &pi, sizeof(pi) );

    // Start the child process. 
	if( !CreateProcess(NULL,   // No module name (use command line)
		szPathonly,        // Command line
        NULL,           // Process handle not inheritable
        NULL,           // Thread handle not inheritable
        FALSE,          // Set handle inheritance to FALSE
        0,              // No creation flags
        NULL,           // Use parent's environment block
        NULL,           // Use parent's starting directory 
        &si,            // Pointer to STARTUPINFO structure
        &pi )           // Pointer to PROCESS_INFORMATION structure
    ) 
    {
		MessageBox (NULL, "Unable to Launch CDLDR. Please ensure CDLDR is present on this removable device.", "Unable to Launch", MB_OK | MB_ICONEXCLAMATION);	
		return -1;
    }


	return 0;

}