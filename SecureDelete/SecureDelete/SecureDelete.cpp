// SecureDelete.cpp : Defines the entry point for the console application.
//
#include <windows.h>
#include <stdio.h>
#include "resource.h"
#include "FileShredder.h"

int main(int argc, char* argv[])
{
	if (argc != 2) {
	
		printf ("\n\nSecure Delete v1.1\nCopyright (c) 2010 CedeSoft Ltd.\nSecure Deletion of files using DoD standards.\n\n");
		return 0;
	
	} 
	
	char szFilepath[SIZE_STRING];
	ZeroMemory (szFilepath, SIZE_STRING);
	strcpy_s (szFilepath, SIZE_STRING, argv[1]);

	printf ("Shredding File ");
	printf (szFilepath);
	printf ("....\n");

	FileShredder shredder;
	
	shredder.InitRandom ();
	shredder.SecureDelete (szFilepath);

	return 0;
}

