#pragma once
#define _CRT_RAND_S

#include <windows.h>
#include <malloc.h>
#include <stdio.h>
#include "MemoryBuffer.h"
#include "resource.h"


#define RAND_SIZE	10000

class FileShredder {
	
	public:
		FileShredder ();
		~FileShredder ();		
		

		//**************************************
		void SecureDelete (char *pszFilename);
		void OverwriteFile (char *pszFilename, BYTE bByte, bool bComplement, bool bRandom, unsigned long lSize);
		void FillBuffer (BYTE bByte, bool bComplement, bool bRandom, unsigned long lBufSize);
		int GetRand (int iMax);
		unsigned long GetFileSize (char *pszFilename);
		void InitRandom ();
		void GetPathOnly (char *szInpath, char *szOutpath);
		void GenFileRandomName (char *szOutname);
		BYTE GetRandomAlphaNum ();
		void NameScramble (char *pszFilename);
	private:

		MemoryBuffer m_writebuffer;
		MemoryBuffer m_randombuffer;


		//************************************


};