#include "resource.h"
#include <Windows.h>
#include "MemoryBuffer.h"


typedef BOOL (WINAPI *LPFN_ISWOW64PROCESS) (HANDLE, PBOOL);
LPFN_ISWOW64PROCESS fnIsWow64Process = (LPFN_ISWOW64PROCESS)GetProcAddress(GetModuleHandle("kernel32"),"IsWow64Process");

MemoryBuffer g_mem;

BOOL IsWow64()
{
    BOOL bIsWow64 = FALSE;
 
    if (NULL != fnIsWow64Process)
    {
        if (!fnIsWow64Process(GetCurrentProcess(),&bIsWow64))
        {
            // handle error
        }
    }
    return bIsWow64;
}


int WINAPI WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {

	char szTempdir[SIZE_STRING];
	ZeroMemory (szTempdir, SIZE_STRING);

	if (GetEnvironmentVariable ("TEMP", szTempdir, SIZE_STRING) == 0) {
		MessageBox (NULL, "CedeDrive Setup was unable to deploy the MSI to %TEMP% because retrieval of the TEMP environment variable failed.", "Unable to deploy.", MB_OK | MB_ICONEXCLAMATION);
		return -1;
	}

	char szFullpath[SIZE_STRING];
	ZeroMemory (szFullpath, SIZE_STRING);

	if (IsWow64 () == TRUE)
	{
		// 64bit OS mode
		strcpy_s (szFullpath, SIZE_STRING, szTempdir);
		strcat_s (szFullpath, SIZE_STRING, "\\");
		strcat_s (szFullpath, SIZE_STRING, "CedeDriveSetup-64bit.msi");

		DeleteFile (szFullpath);

		//MessageBox (NULL, "This is a 64-bit OS", "64", MB_OK);
		if (g_mem.ReadFromResource (MAKEINTRESOURCE (BIN_MSI64)) == 0) {
			
			if (g_mem.SaveToFile (szFullpath) == true) {
			
				// Everything was good, go go go!!!
				ShellExecute (NULL, "open", szFullpath, NULL, NULL, SW_SHOWNORMAL);
				return 0;
			} else {
				MessageBox (NULL, "CedeDrive Setup was unable to deploy the MSI to %TEMP% because the Bootstrapper was unable to save the 64-bit resource to %TEMP%", "Unable to deploy.", MB_OK | MB_ICONEXCLAMATION);
				return -1;
			}

		} else {
			MessageBox (NULL, "CedeDrive Setup was unable to deploy the MSI to %TEMP% because the Bootstrapper was unable to read the 64-bit resource.", "Unable to deploy.", MB_OK | MB_ICONEXCLAMATION);
			return -1;
		}

	} else {
		// 32bit OS mode
		strcpy_s (szFullpath, SIZE_STRING, szTempdir);
		strcat_s (szFullpath, SIZE_STRING, "\\");
		strcat_s (szFullpath, SIZE_STRING, "CedeDriveSetup-32bit.msi");

		DeleteFile (szFullpath);

		if (g_mem.ReadFromResource (MAKEINTRESOURCE (BIN_MSI32)) == 0) {
			
			if (g_mem.SaveToFile (szFullpath) == true) {
			
				// Everything was good, go go go!!!
				ShellExecute (NULL, "open", szFullpath, NULL, NULL, SW_SHOWNORMAL);
				return 0;
			} else {
				MessageBox (NULL, "CedeDrive Setup was unable to deploy the MSI to %TEMP% because the Bootstrapper was unable to save the 32-bit resource to %TEMP%", "Unable to deploy.", MB_OK | MB_ICONEXCLAMATION);
				return -1;
			}

		} else {
			MessageBox (NULL, "CedeDrive Setup was unable to deploy the MSI to %TEMP% because the Bootstrapper was unable to read the 32-bit resource.", "Unable to deploy.", MB_OK | MB_ICONEXCLAMATION);
			return -1;
		}

		//MessageBox (NULL, "This is a 32-bit OS", "64", MB_OK);
	}

	return 0;
}
